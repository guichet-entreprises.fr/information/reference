﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP127" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Ergothérapeute" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="ergotherapeute" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/ergotherapeute.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="ergotherapeute" -->
<!-- var(translation)="None" -->

# Ergothérapeute

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L'ergothérapeute est un professionnel de la santé dont l'activité consiste, par le biais d'activités et d'apprentissages, à permettre aux personnes handicapées d'adapter leur mode de vie en vue d'acquérir ou de retrouver leur autonomie après une blessure, une maladie ou toute déficience physique.

Il tient compte des problèmes physiques rencontrés par son patient mais également des facteurs psychosociaux et environnementaux liés.

*Pour aller plus loin* : article L. 4331-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer la profession d'ergothérapeute, le professionnel doit :

- être titulaire d'un diplôme d’État d'ergothérapeute ;
- procéder à l'enregistrement de son diplôme (cf. infra « 5°. a. Obligation de procéder à l'enregistrement de son diplôme »).

**À noter**

Peuvent également exercer l'activité d'ergothérapeute les professionnels qui :

- ont exercé la profession d'infirmier ou infirmière dans un emploi d'ergothérapeute avant le 11 avril 1983 au sein d'un établissement public de santé accueillant des personnes atteintes de troubles mentaux ;
- justifient d'une expérience professionnelle d'au mois trois ans en qualité d'ergothérapeute, au cours des dix années précédant la date du 23 novembre 1986 et ayant satisfait à un contrôle de connaissances dans les trois ans suivants cette date.

*Pour aller plus loin* : articles L. 4331-2, L. 4331-5 et L. 4333-1 du Code de la santé publique.

#### Formation

##### Diplôme d’État (DE) d'ergothérapeute

Cette formation d'une durée de trois ans est composée :

- d'une formation théorique de 2000 heures, sous forme de cours magistraux (794 heures) et de travaux dirigés (1206 heures) ;
- d'une formation clinique et situationnelle (1260 heures).

Les modalités et le contenu de la formation sont fixées à l'[annexe III](http://solidarites-sante.gouv.fr/fichiers/bo/2010/10-07/ste_20100007_0001_p000.pdf) de l'arrêté du 5 juillet 2010.

Le DE d'ergothérapeute est délivré par le préfet de région aux candidats ayant subi avec succès l'examen prévu en fin de formation.

Une fois le diplôme obtenu, le professionnel est tenu de se faire enregistrer auprès de l'Agence régionale de santé (ARS) (cf. infra « 5°. a. Obligation de procéder à l'enregistrement du diplôme »).

*Pour aller plus loin* : article D. 4331-2 et suivants ; arrêté du 5 juillet 2010 relatif au diplôme d’État d'ergothérapeute.

##### Coûts associés à la qualification

La formation en vue d'acquérir le DE d'ergothérapeute est payante et son coût varie selon le cursus envisagé. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité d'ergothérapeute peut, à titre temporaire et occasionnel, exercer la même activité en France.

Lorsque l’État ne réglemente ni l'accès, ni l'exercice de la profession, le ressortissant doit justifier avoir exercé cette activité dans un ou plusieurs État(s) membres pendant au moins un an au cours des dix dernières années.

Pour cela, le professionnel doit avant sa première prestation en France, effectuer une déclaration préalable auprès du préfet de la région au sein de laquelle il souhaite la réaliser (cf. infra « 5°. b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel en France (LPS) »).

Le ressortissant est tenu au respect des règles professionnelles régissant l'exercice de la profession en France.

En outre, en cas de différences substantielles entre la formation du ressortissant et celle requise pour l'exercice de l'activité d'ergothérapeute en France, de nature à nuire à la santé publique, le préfet de région peut décider de le soumettre à une épreuve d'aptitude.

*Pour aller plus loin* : article L. 4331-6 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Etablissement (LE))

Tout ressortissant d'un État membre de l'UE, légalement établi et exerçant l'activité d'ergothérapeute, peut exercer la même activité à titre permanent, en France.

Pour cela, il doit être titulaire soit :

- d'un titre de formation délivré par un État membre qui réglemente l'accès à la profession d'ergothérapeute ;
- lorsque l’État ne réglemente ni l'accès, ni l'exercice de la profession, un titre de formation justifiant que le ressortissant a bénéficié d'une formation en vue d'exercer cette activité et d'une attestation certifiant qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- d'un titre de formation délivré par un État tiers mais reconnu par un État membre et permettant d'exercer l'activité d'ergothérapeute.

Lorsqu'il existe des différences substantielles entre la formation reçue par le ressortissant et celle requise pour l'exercice de la profession d'ergothérapeute en France, le préfet de région peut décider de le soumettre à une mesure de compensation (cf. infra « 5°. c. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent en France (LE) »).

En outre, le ressortissant doit posséder les connaissances linguistiques nécessaires à l'exercice de sa profession en France.

*Pour aller plus loin* : articles L.4331-4 et L. 4333-1 du Code de la santé publique.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

#### Respect de la vie privée de ses patients et secret professionnel

L'ergothérapeute, en qualité de professionnel de santé, est tenu au respect de la vie privée de ses patients et au secret professionnel. En outre, il doit informer ses patients de leur état de santé et notamment sur :

- les différents traitements proposés ;
- les risques encourus ;
- les alternatives possibles en cas de refus.

*Pour aller plus loin* : articles L. 1110-4 et L. 1111-2 du Code de la santé publique.

#### Code de déontologie

Les règles déontologiques applicables à la profession d'ergothérapeute ne sont pas codifiées. Toutefois les professionnels respectent le Code d'éthique international disponible sur le [site de la Fédération mondiale des ergothérapeutes](http://www.wfot.org/) (World Federation of Occupational Therapists (WFOT)).

Pour plus d'informations, vous pouvez consulter le [site de l'Association nationale française des ergothérapeutes](http://www.anfe.fr) (ANFE).

## 4°. Assurances et sanctions

#### Assurance

L'ergothérapeute en tant que professionnel de santé, est tenu de souscrire une assurance de responsabilité civile professionnelle pour les risques encourus au cours de l'exercice de son activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

#### Sanctions pénales

L'exercice illégal de la profession d'ergothérapeute, de même que l'usage du titre d'ergothérapeute, sans être qualifié professionnellement (usurpation de titres) est puni d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : article L. 4334-2 du Code de la santé publique ; article 433-17 du Code pénal.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Obligation de procéder à l'enregistrement de son diplôme

#### Autorité compétente

Le professionnel titulaire d'un diplôme d’État d'ergothérapeute ou d'une autorisation d'exercice doit adresser une demande à l'ARS en vue de procéder à son enregistrement.

#### Pièces justificatives

Pour cela, il doit transmettre les informations suivantes, réputées validées et certifiées par l'organisme ayant délivré son diplôme, son titre de formation ou son autorisation d'exercice :

- l'état civil du titulaire du diplôme et toutes les données permettant de l'identifier ;
- les nom et adresse de l'établissement ayant délivré la formation ;
- l'intitulé de sa formation.

#### Issue de la procédure

Après vérifications des pièces, l'ARS procède à l'enregistrement du diplôme.

**À noter**

L'inscription n'est possible que pour un seul département, toutefois si le professionnel souhaite exercer dans plusieurs départements, il sera inscrit sur la liste du département dans lequel se trouve le lieu principal de son activité.

#### Coût

Gratuit.

*Pour aller plus loin* : articles D. 4333-1 à D. 4333-6-1 du Code de la santé publique.

### b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel en France (LPS)

#### Autorité compétente

Le ressortissant adresse sa demande au préfet de la région au sein de laquelle il souhaite exercer sa prestation de services.

#### Pièces justificatives

Sa demande doit comporter :

- le formulaire de déclaration fixé à l'annexe de l'arrêté du 8 décembre 2017 ;
- une copie de sa pièce d'identité en cours de validité et un document attestant de la nationalité du demandeur ;
- une copie de son titre de formation permettant l'exercice de la fonction d'ergothérapeute et le cas échéant, pour les infirmiers une copie de son titre de formation de spécialiste ;
- une attestation datant de moins de trois mois de l'autorité compétente de l’État membre, attestant que le professionnel n'encourt aucune interdiction d'exercer la profession ;
- lorsque l’État membre ne réglemente ni l'accès à la profession, ni son exercice, toute pièce justifiant qu'il a exercé la profession dans cet État pendant un an au cours des dix dernières années ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État membre, la reconnaissance de ce titre ainsi qu'un justificatif certifiant qu'il a exercé cette activité pendant trois ans, et le cas échéant, une copie de cette déclaration et de la première déclaration.

#### Délai et procédure

Le préfet accuse réception de la demande dans un délai d'un mois et informe le demandeur soit :

- qu'il peut débuter sa prestation de services ;
- qu'il ne peut pas débuter sa prestation de services ;
- qu'en raison d'une différence substantielle entre sa formation et celle requise pour exercer l'activité d'ergothérapeute en France, qu'il doit se soumettre à une épreuve d'aptitude en vue de démontrer qu'il possède les connaissances et compétences nécessaires pour exercer en France.

*Pour aller plus loin* : arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### c. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent en France (LE)

#### Autorité compétente

Le ressortissant doit adresser sa demande en double exemplaires par lettre recommandée avec avis de réception, au secrétariat de la commission des ergothérapeutes composée :

- du directeur régional de la jeunesse, des sports et de la cohésion sociale ;
- du directeur général de l'ARS ;
- d'un médecin ;
- de deux ergothérapeutes dont l'un exerçant en institut de formation.

#### Pièces justificatives

Sa demande doit comporter les éléments suivants, le cas échéant, assortis de leur traduction certifiée en français :

- le [formulaire de demande d'autorisation](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=73C160E5135B9F63FAC92C921782D8AC.tplgfr33s_2idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180412) complété et signé ;
- une photocopie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation permettant l'exercice de la profession, le cas échéant, pour les infirmiers, une copie du titre de formation de spécialiste ;
- le cas échéant, une copie des diplômes complémentaires ;
- tout document justifiant de formation continue, d'expérience ou de compétence acquises au cours de son exercice professionnel ;
- une déclaration de l'autorité compétente de l’État membre attestant que le ressortissant ne fait l'objet d'aucune sanction ;
- une attestation délivrée par l’État membre détaillant la formation reçue (volume horaire et enseignements suivis) ;
- lorsque l’État ne réglemente ni l'accès à l'activité, ni son exercice, la preuve qu'il a exercé cette activité pendant au moins deux ans au cours des dix dernières années ;
- lorsque le ressortissant a obtenu son titre de formation au sein d'un État tiers mais reconnu au sein d'un État membre, la reconnaissance de son titre de formation.

#### Délai et procédure

Le préfet de région délivre après avis de la commission des ergothérapeutes, l'autorisation d'exercice. Il accuse réception de la demande dans un délai d'un mois à compter de sa réception. Toutefois, le silence gardé par le préfet au delà d'un délai de quatre mois, vaut acceptation de la demande. Cependant, il peut également prévoir que le ressortissant devra se soumettre à une mesure de compensation (cf. ci-après).

**À noter**

Une fois titulaire d'une autorisation d'exercice, le professionnel est tenu de procéder à son enregistrement auprès de l'ARS (cf. supra « 5°. a. Obligation de procéder à l'enregistrement de son diplôme »).

*Pour aller plus loin* : arrêté du 20 janvier 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de conseiller en génétique, infirmier, masseur-kinésithérapeute, pédicure-podologue, ergothérapeute, manipulateur en électroradiologie médicale et diététicien.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel et celle requise pour l'exercice de la profession d'ergothérapeute en France, le préfet de région, peut décider de le soumettre au choix, à une épreuve d'aptitude sous forme d'un examen écrit, ou à un stage d'adaptation. Ce dernier doit être effectué au sein d'un établissement de santé public ou privé agréé par l'ARS et sous la responsabilité d'un professionnel exerçant l'activité d'ergothérapeute depuis au moins trois ans.

*Pour aller plus loin* : arrêté du 24 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de conseiller en génétique, masseur-kinésithérapeute, pédicure-podologue, ergothérapeute, manipulateur d'électroradiologie médicale et diététicien par des ressortissants des États membres de l'Union européenne ou partie à l'accord sur l'Espace économique européen.

### d. Voies de recours

#### Centre d’assistance français
 
Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).