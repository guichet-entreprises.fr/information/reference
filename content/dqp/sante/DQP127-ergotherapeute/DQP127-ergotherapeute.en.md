﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP127" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Occupational therapist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="occupational-therapist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/occupational-therapist.html" -->
<!-- var(last-update)="2020-04-15 17:21:29" -->
<!-- var(url-name)="occupational-therapist" -->
<!-- var(translation)="Auto" -->


Occupational therapist
======================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:29<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The occupational therapist is a health professional whose activity consists, through activities and learning, of enabling people with disabilities to adapt their lifestyle in order to acquire or regain their independence after an injury, illness or any physical impairment.

It takes into account the physical problems encountered by his patient but also the psychosocial and environmental related factors.

*For further information*: Article L. 4331-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice the profession of occupational therapist, the professional must:

- Hold a state diploma as an occupational therapist
- to register his diploma (see infra "5°). a. Obligation to register his diploma").

**Please note**

Professionals who:

- worked as a nurse in an occupational therapist job prior to April 11, 1983 in a public health facility for people with mental disorders;
- justify a three-year work experience as an occupational therapist, in the ten years prior to November 23, 1986, and having completed a knowledge check within three years of that date.

*For further information*: Articles L. 4331-2, L. 4331-5 and L. 4333-1 of the Public Health Code.

#### Training

**State Diploma (DE) occupational therapist**

This three-year course consists of:

- 2000 hours of theoretical training, in the form of lectures (794 hours) and directed work (1206 hours);
- clinical and situational training (1260 hours).

The terms and content of the training are set at the[Appendix III](http://solidarites-sante.gouv.fr/fichiers/bo/2010/10-07/ste_20100007_0001_p000.pdf) 5 July 2010.

The occupational therapist's ED is issued by the regional prefect to candidates who have successfully completed the exam at the end of the training.

Once the diploma has been obtained, the professional is required to register with the Regional Health Agency (ARS) (see infra "5°). a. Obligation to register the diploma").

*For further information*: Article D. 4331-2 and beyond; 5 July 2010 on the state occupational therapist's diploma.

#### Costs associated with qualification

Training to acquire occupational therapist ED is paid for and the cost varies depending on the course envisaged. It is advisable to get close to the institutions concerned for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may, on a temporary and casual basis, engage in the same activity in France.

Where the state does not regulate access or the practice of the profession, the national must justify having carried out this activity in one or more member states for at least one year in the last ten years.

To do this, the professional must, before his first performance in France, make a prior declaration with the prefect of the region in which he wishes to carry it out (see infra "5°. b. Pre-declaration for the national for a temporary and casual exercise in France (LPS)").

The national is bound by the professional rules governing the practice of the profession in France.

In addition, in the event of substantial differences between the training of the national and that required for the exercise of occupational therapist activity in France, likely to harm public health, the regional prefect may decide to subject him to a test aptitude.

*For further information*: Article L. 4331-6 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU Member State, legally established and acting as an occupational therapist, may carry out the same activity on a permanent basis in France.

For this, it must be the holder either:

- a training certificate issued by a Member State that regulates access to the profession of occupational therapist;
- where the State does not regulate access or the practice of the profession, a training document justifying that the national has received training to carry out this activity and a certificate certifying that he has been engaged in this activity for a period of time less than a year in the last ten years;
- a training certificate issued by a third state but recognised by a Member State and allowing the work of occupational therapist to be practised.

Where there are substantial differences between the training received by the national and that required for the profession of occupational therapist in France, the regional prefect may decide to subject him to a compensation measure (see infra "5 . c. Application for authorization of practice for the national for a permanent exercise in France (LE) ").

In addition, the national must have the language skills necessary to practice his profession in France.

*For further information*: Articles L.4331-4 and L. 4333-1 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Patient privacy and professional confidentiality**

The occupational therapist, as a health professional, is bound to respect the privacy of his patients and professional secrecy. In addition, he must inform his patients of their state of health and in particular on:

- The different treatments on offer
- The risks involved
- possible alternatives in the event of a refusal.

*For further information*: Articles L. 1110-4 and L. 1111-2 of the Public Health Code.

**Code of Ethics**

The ethical rules applicable to the occupational therapist profession are not codified. However, professionals comply with the International Code of Ethics available on the world federation of Occupational Therapists ([WFOT](http://www.wfot.org/ResourceCentre.aspx#)).

For more information, you can see[website of the French National Association of Occupational Therapists](http://www.anfe.fr) (ANFE).

4°. Insurance and sanctions
-----------------------------------------------

**Insurance**

The occupational therapist, as a health professional, is required to take out professional liability insurance for the risks incurred during the course of his activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

**Criminal sanctions**

The illegal practice of the profession of occupational therapist, as well as the use of the title of occupational therapist, without being qualified professionally (usurpation of titles) is punishable by one year's imprisonment and a fine of 15,000 euros.

*For further information*: Article L. 4334-2 of the Public Health Code; Article 433-17 of the Penal Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obligation to register his diploma

#### Competent authority

The professional with a state occupational therapist or exercise authorization must apply to the LRA for registration.

#### Supporting documents

To do so, it must pass on the following information, deemed validated and certified by the organization that has issued its diploma, training certificate or exercise authorization:

- The civil status of the holder of the diploma and all the data to identify it;
- The name and address of the institution that delivered the training;
- title of his training.

#### Outcome of the procedure

After checking the exhibits, the LRA registers the diploma.

**Please note**

Registration is only possible for one department, however if the professional wishes to practice in several departments, he will be placed on the list of the department in which the main place of his activity is located.

#### Cost

Free.

*For further information*: Articles D. 4333-1 to D. 4333-6-1 of the Public Health Code.

### b. Pre-declaration for the national for a temporary and casual exercise in France (LPS)

#### Competent authority

The national sends his application to the prefect of the region in which he wishes to perform his services.

#### Supporting documents

His application must include:

- The[declaration form](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000036171877&dateTexte=20180412) attached to the annex of the december 8, 2017 decree;
- A copy of his valid ID and a document attesting to the applicant's nationality;
- A copy of his training title allowing the exercise of the function of occupational therapist and, if necessary, for nurses a copy of his training certificate as a specialist;
- a certificate less than three months old from the competent authority of the Member State, certifying that the professional is not prohibited from practising the profession;
- where the Member State does not regulate access to the profession or its exercise, any evidence that it has practised in that state for one year in the last ten years;
- where the training document has been issued by a third state and recognised in a Member State, the recognition of that title and a proof certifying that it has been engaged in this activity for three years, and if so, a copy of that declaration and the first statement.

#### Time and procedure

The prefect acknowledges receipt of the request within one month and informs the applicant:

- That he can begin his service delivery;
- He cannot begin his service delivery;
- that due to a substantial difference between his training and that required to work as an occupational therapist in France, he must submit to an aptitude test in order to demonstrate that he has the necessary knowledge and skills to practice in France.

*For further information*: Order of 8 December 2017 relating to the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### c. Application for authorisation to exercise for the national for a permanent exercise in France (LE)

#### Competent authority

The national must submit his application in double copies by letter recommended with notice of receipt, to the secretariat of the committee of occupational therapists composed:

- Regional Director of Youth, Sports and Social Cohesion;
- Director General of the LRA;
- A doctor
- two occupational therapists, one of whom is practising in a training institute.

#### Supporting documents

Its application must include the following, if any, with their certified translation into French:

- The[Authorization application form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=73C160E5135B9F63FAC92C921782D8AC.tplgfr33s_2idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180412) Completed and signed;
- A photocopy of his valid ID
- A copy of his training title allowing the practice of the profession, if any, for nurses, a copy of the specialist training title;
- If necessary, a copy of the additional diplomas;
- any documentation justifying continuing education, experience or competence acquired during his professional practice;
- a statement by the competent authority of the Member State stating that the national is not subject to any sanction;
- a certificate issued by the Member State detailing the training received (hourly volume and instructions followed);
- where the state does not regulate access to the activity or its exercise, proof that it has been engaged in this activity for at least two years in the last ten years;
- when the national has obtained his training designation in a third state but recognised within a Member State, the recognition of his training title.

#### Time and procedure

The regional prefect issues the authorisation to practice after notice of the commission of occupational therapists. It acknowledges receipt of the request within one month of receipt. However, the silence kept by the prefect beyond a four-month period is worth accepting the application. However, it may also provide that the national will have to submit to a compensation measure (see below).

**Please note**

Once the professional is licensed to practice, he is required to register with the ARS (see supra "5°). a. Obligation to register his diploma").

*For further information*: decree of 20 January 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the exercise in France of the professions of genetic counsellor, nurse, masseur-kinesitherapist, pedicure-podologist, occupational therapist, manipulator in medical electroradiology and dietician.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the professional and that required for the profession of occupational therapist in France, the regional prefect may decide to submit it to the choice, to an aptitude test under a written exam, or an adjustment course. The latter must be carried out in a public or private health facility accredited by the ARS and under the responsibility of a professional who has been an occupational therapist for at least three years.

*For further information*: decree of 24 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice in France of the professions of genetic counsellor, massage therapist, pedicure-podologist, occupational therapist, manipulator medical electroradiology and dietitian by nationals of the Member States of the European Union or party to the agreement on the European Economic Area.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

