﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP042" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Audioprothésiste" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="audioprothesiste" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/audioprothesiste.html" -->
<!-- var(last-update)="2020-04-15 17:21:14" -->
<!-- var(url-name)="audioprothesiste" -->
<!-- var(translation)="None" -->

# Audioprothésiste

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:14<!-- end-var -->

## 1°. Définition de l’activité

L'audioprothésiste est un professionnel de santé dont l'activité consiste à fournir un système de prothèses auditives aux personnes présentant des défaillances de l'ouïe. Cette prestation ne peut se faire qu'après un examen otologique et audiométrique tonal et vocal et sur prescription médicale d'un oto-rhino-laryngologiste.

*Pour aller plus loin* : article L. 4361-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Le professionnel qui souhaite exercer l'activité d'audioprothésiste en France doit :

- être qualifié professionnellement (cf. infra « 2°. a. Formation ») ;
- avoir fait l'objet d'un enregistrement auprès de l'agence régionale de santé (cf. infra « 5°. a. Procédure d'enregistrement ») ;
- exercer son activité dans un local réservé et aménagé à cet effet.

*Pour aller plus loin* : articles L. 4361-2 et L. 4361-6 du Code de la santé publique.

#### Formation

Pour être reconnu comme étant qualifié professionnellement, l'intéressé doit être titulaire soit :

- d'un diplôme d’État d'audioprothésiste ;
- de tout autre diplôme, certificat ou titre permettant l'exercice de la médecine en France.

##### Diplôme d’État d'audioprothésiste

Ce diplôme est accessible sur concours, aux candidats titulaires du baccalauréat ou d'une équivalence, et ayant réussi l'ensemble des épreuves d'admission.

La formation d'une durée de trois ans comprend :

- des enseignements théoriques ;
- des enseignements dirigés ;
- des enseignements pratiques ;
- la réalisation de stages en audiologie au sein de pôles d'activités hospitaliers d'oto-rhino-laryngologie ou au sein d'établissements agréés par le conseil de formation et de recherche ;
- la soutenance d'un mémoire de recherche au cours de la dernière année de formation.

Plusieurs établissements sont agréés pour délivrer cette formation :

- l'[université](http://www.univ-tlse3.fr/) de Toulouse ;
- l'unité de formation des sciences médicales de l'[université de Bordeaux](http://www.univ-bordeauxsegalen.fr/) ;
- l'unité de formation et de recherche (UFR) de sciences pharmaceutiques et biologiques de l'[université de Montpellier](http://www.umontpellier.fr/) ;
- l'UFR de sciences pharmaceutiques et biologiques de l'[université de Lorraine](http://pharma.univ-lorraine.fr/) ;
- l'Institut des sciences et techniques de la réadaptation ([ISTR](https://istr.univ-lyon1.fr/)) de l'université de Lyon ;
- le centre de préparation au diplôme d’État d'audioprothésiste ([CPDA](http://cpda.cnam.fr/)) au Conservatoire national des arts et métiers de Paris.

**À noter**

L'accès à cette formation fait l'objet d'un numerus clausus.

*Pour aller plus loin* : articles D. 636-1 à D. 636-17 du Code de l'éducation ; articles L. 4361-1 et D. 4361-1 du Code de la santé publique.

##### Autres diplômes

Peuvent également exercer l'activité d'audioprothésiste, sans procéder à leur enregistrement, les professionnels :

- titulaires d'un certificat d'études techniques d'acoustique portant sur l'appareillage de prothèses auditives, certificat délivré par les facultés de médecine, de pharmacie ou mixtes ;
- justifiant d'une expérience professionnelle en tant qu'audioprothésiste d'au moins cinq ans avant le 4 janvier 1967 après avoir été autorisé par une commission nationale de qualification ;
- ayant réussi un examen professionnel probatoire dès lors qu'ils n'ont pas reçu l'autorisation susvisée ou bénéficient d'une expérience professionnelle inférieure à cinq ans avant le 4 janvier 1967.

*Pour aller plus loin* : articles L. 4361-2 et L. 4361-5 du Code de la santé publique.

#### Coûts associés à la qualification

Le coût de la formation menant à la profession d'audioprothésiste varie selon le cursus envisagé. Pour plus de renseignements, il convient de se rapprocher des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité d'audioprothésiste peut exercer, à titre temporaire et occasionnel, la même activité en France, et ce sans effectuer la procédure d'enregistrement.

Il doit pour cela effectuer au préalable une déclaration (cf. infra « 5°. b. Demande de déclaration en vue d'une prestation de services »).

Lorsque la profession n'est réglementée ni dans le cadre de l'activité ni dans celui de la formation dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an au cours des dix années précédant la prestation, dans un ou plusieurs États membres de l'UE.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, l'autorité compétente peut exiger que l'intéressé se soumette à une épreuve d'aptitude.

*Pour aller plus loin* : article L. 4361-9 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre établissement)

Pour exercer l’activité d'audioprothésiste en France à titre permanent, le ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°.Qualifications professionnelles ») ;
- être titulaire d'une autorisation d'exercice (cf. infra « 5°. c. Demande d'autorisation d'exercice en vue d'un LE »).

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, l'autorité compétente peut exiger que l'intéressé se soumette à un stage d'adaptation ou une épreuve d'aptitude.

*Pour aller plus loin* : article L. 4361-4 du Code de la santé publique ; arrêté du 30 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste, opticien-lunetier par des ressortissants des États membres de l'UE ou parties à l'accord sur l'EEE.

## 3°. Règles déontologiques, éthique

L’audioprothésiste est soumis, durant l'exercice de son activité, au respect :

- du secret professionnel ;
- du Code de déontologie européen de la profession des audioprothésistes et notamment des règles éthiques suivantes :
  - assister son patient et agir uniquement dans son intérêt,
  - respecter les exigences en matière de publicité,
  - agir dans le respect de sa profession.

*Pour aller plus loin* : le [Code de déontologie](http://www.unsaf.org/site/audioprothesiste/deontologie.html) est disponible sur le site de l'Unsaf (Syndicat national des audioprothésistes).

## 4°. Sanctions pénales et assurances

### Sanctions pénales

Le professionnel qui exerce illégalement la profession d'audioprothésiste encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende ainsi que l'une des peines complémentaires suivantes :

- l'affichage ou la diffusion de la condamnation ;
- la confiscation de ce qui a permis de commettre l'infraction ;
- l'interdiction d'exercer la profession pour une durée de cinq ans ou de manière définitive.

*Pour aller plus loin* : article L. 4363-1 à L. 4363-3 du Code de la santé publique.

### Assurance

En qualité de professionnel de santé exerçant une activité de prévention, de diagnostic ou de soins, l'audioprothésiste doit, s'il exerce à titre libéral, souscrire une assurance de responsabilité professionnelle.

S'il exerce en tant que salarié, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de cette activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Procédure d'enregistrement

Avant l'exercice de son activité, le professionnel doit procéder à son enregistrement.

#### Autorité compétente

L'agence régionale de santé du lieu d'exercice du professionnel est compétente pour vérifier l'ensemble des informations fournies par le professionnel en vue de son enregistrement.

#### Pièces justificatives

Les organismes ayant délivré le titre de formation au professionnel doivent remettre à l'autorité compétente les informations suivantes :

- l'état civil et toutes les informations relatives à l'identité du professionnel ;
- l'adresse et le libellé de l'établissement ayant dispensé la formation ;
- l'intitulé de la formation.

#### Coût

Gratuit

*Pour aller plus loin* : articles L. 4361-2-2, D. 4365-1 et D. 4333-1 du Code de la santé publique.

### b. Demande de déclaration en vue d'une prestation de services

#### Autorité compétente

La demande de déclaration doit être adressée avant la première prestation de services, au préfet de la région de l'établissement du professionnel.

#### Pièces justificatives

La demande de déclaration doit comporter les éléments suivants, le cas échéant traduits en français :

- le [formulaire](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7F200D263B3CF713E481471475EE0CFD.tplgfr26s_3?cidTexte=JORFTEXT000036171877) de déclaration complété et signé ;
- une pièce d'identité en cours de validité ou tout document attestant de la nationalité du demandeur ;
- la copie du titre de formation permettant l'exercice de l'activité d'audioprothésiste dans l’État membre ;
- une attestation de moins de trois mois certifiant que le demandeur est légalement établi dans l’État de l'UE ou l'EEE et qu'il n'encourt aucune interdiction d'exercer de manière temporaire ou définitive ;
- lorsque le titre de formation a été acquis dans un État tiers et reconnu dans un État membre :
  - la reconnaissance du titre de formation par l’État membre,
  - un justificatif que le demandeur a exercé la profession d'audioprothésiste dans cet État pendant au moins trois ans ;
- le cas échéant, une copie de sa précédente déclaration et de la première effectuée ;
- une déclaration justifiant que le demandeur possède les connaissances linguistiques nécessaires à l'exercice de sa profession en France.

#### Issue de la procédure

Dans un délai d'un mois à compter de la réception du dossier du demandeur, le préfet l'informe soit :

- qu'il peut débuter la prestation de services sans vérification préalable de ses qualifications professionnelles ;
- que suite à la vérification de ses qualifications professionnelles, le professionnel doit se soumettre à une épreuve d'aptitude ;
- qu'il ne peut débuter la prestation de services.

Le préfet enregistre le professionnel sur une liste particulière et lui adresse un récépissé mentionnant un numéro d'enregistrement. En l'absence de réponse de la part du préfet dans un délai d'un mois, la prestation de services peut débuter.

**À noter**

La déclaration est renouvelable tous les ans.

*Pour aller plus loin* : articles L. 4361-9, R. 4361-16 et R. 4331-12 à R. 4331-15 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### c. Demande d'autorisation d'exercice en vue d'un LE

#### Autorité compétente

La demande doit être adressée en double exemplaire par lettre recommandée avec avis de réception au secrétariat de la commission des audioprothésistes composée de professionnels.

#### Pièces justificatives

La demande de l'intéressé doit comporter les éléments suivants :

- le [formulaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DDB2C307150C1EBD51D563B436CD57D5.tplgfr40s_3?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180221)de demande complété et signé ;
- une copie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation permettant l'exercice de la profession dans son pays d'obtention et, le cas échéant, ses diplômes complémentaires ainsi qu'une attestation mentionnant le niveau et le détail de la formation ;
- tout justificatif de son expérience professionnelle, des formations continues suivies dans un État membre de l'UE ou l'EEE ;
- tout document justifiant qu'il ne fait l'objet d'aucune sanction ;
- lorsque l’État membre ne réglemente ni l'accès à la profession ni son exercice, un justificatif attestant que le professionnel a exercé l'activité d'audioprothésiste pendant un an au cours des dix dernières années ;
- lorsque le demandeur est titulaire d'un titre de formation délivré par un État tiers et reconnu par un État membre, la reconnaissance du titre de formation par ce dernier.

#### Issue de la procédure

Le préfet de la région du lieu d'établissement du professionnel accuse réception du dossier dans un délai d'un mois à compter de sa réception et délivre l'autorisation d'exercice après avis de la commission susvisée.

**À noter**

Le silence gardé par le préfet au delà de quatre mois à compter de la réception du dossier complet vaut rejet de la demande.

*Pour aller plus loin* : articles R. 4361-13 à R. 4361-15 du Code de la santé publique ; arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste et opticien-lunetier.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).