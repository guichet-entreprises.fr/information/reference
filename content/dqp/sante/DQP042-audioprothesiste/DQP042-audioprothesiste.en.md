﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP042" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Hearing aid dispenser" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="hearing-aid-dispenser" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/hearing-aid-dispenser.html" -->
<!-- var(last-update)="2020-04-15 17:21:15" -->
<!-- var(url-name)="hearing-aid-dispenser" -->
<!-- var(translation)="Auto" -->


Hearing aid dispenser
===========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The audioprosthetist is a health professional whose activity is to provide a hearing aid system to people with hearing failures. This service can only be done after a tonal and vocal otological and audiometric examination and on medical prescription of an otolaryngologist.

*For further information*: Article L. 4361-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The professional who wishes to work as an audioprosthetist in France must:

- be professionally qualified (see infra "2.2). a. Training");
- have been registered with the regional health agency (see infra "5°. a. Registration procedure");
- operate in a purpose-built space.

*For further information*: Articles L. 4361-2 and L. 4361-6 of the Public Health Code.

#### Training

To be recognized as a professionally qualified person, the person must be a holder of either:

- a state degree as an audioprosthetist;
- any other diploma, certificate or title allowing the practice of medicine in France.

**State Diploma of Audioprosthetist**

This diploma is available by competition, to candidates with a bachelor's degree or equivalency, and having passed all the entrance exams.

The three-year training includes:

- theoretical teachings;
- directed teachings;
- Practical lessons
- conducting audiology internships in otolaryngology hospital clusters or in institutions accredited by the training and research council;
- support for a research dissertation in the last year of training.

Several institutions are accredited to deliver this training:

- The[University](http://www.univ-tlse3.fr/) Toulouse;
- the medical science training unit of the[université de Bordeaux](http://www.univ-bordeauxsegalen.fr/) ;
- The Pharmaceutical and Biological Sciences Training and Research Unit (UFR) of the[université de Montpellier](http://www.umontpellier.fr/) ;
- UFR of Pharmaceutical and Biological Sciences[université de Lorraine](http://pharma.univ-lorraine.fr/) ;
- Institute of Rehabilitation Science and Technology ([ISTR](https://istr.univ-lyon1.fr/)) at the University of Lyon;
- the State Diploma Preparation Centre for Audioprosthetist ([CPDA](http://cpda.cnam.fr/)) at the National Conservatory of Arts and Crafts in Paris.

**Please note**

Access to this training is the subject of a numerus clausus.

*For further information*: Articles D. 636-1 to D. 636-17 of the Education Code; Sections L. 4361-1 and D. 4361-1 of the Public Health Code.

**Other diplomas**

Professionals can also carry out the activity of audioprosthetist, without recording them:

- holders of a certificate of technical studies in acoustics on the device of hearing aids, certificate issued by medical, pharmacy or mixed schools;
- justifying professional experience as an audioprosthetist at least five years before 4 January 1967 after being authorised by a national qualification commission;
- having passed a probationary professional examination if they have not received the above authorization or have work experience of less than five years before 4 January 1967.

*For further information*: Articles L. 4361-2 and L. 4361-5 of the Public Health Code.

#### Costs associated with qualification

The cost of training leading to the profession of audioprosthetist varies according to the course envisaged. For more information, it is important to get closer to the institutions involved.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may engage in the same temporary and occasional activity in France, without carrying out the registration procedure.

In order to do so, it must make a declaration (see infra "5°. b. Request for a declaration for service delivery").

Where the profession is not regulated in the context of activity or training in the country in which the professional is legally established, he must have carried out this activity for at least one year in the ten years prior to one or more EU Member States.

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent authority may require that the person concerned submit to an aptitude test.

*For further information*: Article L. 4361-9 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

In order to carry out the activity of audioprosthetist in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above: "2. Professional qualifications");
- licensed to practice (see infra "5°). c. Application for authorization to exercise for an LE").

**Good to know: compensation measures**

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent authority may require that the person submit to an adjustment course or an aptitude test.

*For further information*: Article L. 4361-4 of the Public Health Code;[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022050408&dateTexte=20180221) 30 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist, optician-lunetier by nationals of the EU member states or parties to the EEA agreement.

3°.Ethical rules, ethics
------------------------------------

The audioprosthetist is subject to respect during the exercise of his activity:

- Professional secrecy
- European Code of Ethics for the Profession of Audioprosthetists, including the following ethical rules:- assisting the patient and acting only in his or her best interests,
  - Comply with advertising requirements,
  - act with respect for his profession.

*For further information*: In:[Code of Ethics](http://www.unsaf.org/site/audioprothesiste/deontologie.html) is available on the website of unsaf (National Union of Audioprosthetists).

4°. Criminal sanctions and insurance
--------------------------------------------------------

**Criminal sanctions**

A professional who illegally practises as an audioprosthetist faces a one-year prison sentence and a fine of 15,000 euros, as well as one of the following additional penalties:

- Posting or disseminating the conviction
- confiscation of what led to the commission of the offence;
- a five-year ban on practising the profession or permanently.

*For further information*: Article L. 4363-1 to L. 4363-3 of the Public Health Code.

**Insurance**

As a health professional engaged in prevention, diagnosis or care, the audioprosthetist must, if he practices in a liberal capacity, take out professional liability insurance.

If he exercises as an employee, it is up to the employer to take out such insurance for his employees for the acts carried out during this activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Registration procedure

Before carrying out his activity, the professional must proceed with his registration.

**Competent authority**

The regional health agency of the professional's place of practice is competent to verify all the information provided by the professional for registration.

**Supporting documents**

Organizations that have issued the professional training designation must provide the relevant authority with the following information:

- Marital status and all information about the identity of the professional;
- The address and wording of the institution that provided the training;
- the title of the training.

**Cost**

Free

*For further information*: Articles L. 4361-2-2, D. 4365-1 and D. 4333-1 of the Public Health Code.

### b. Request for reporting for service delivery

**Competent authority**

The request for a declaration must be sent before the first service is provided to the prefect of the professional establishment area.

**Supporting documents**

The request for declaration must include the following, if any, translated into French:

- The[Form](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7F200D263B3CF713E481471475EE0CFD.tplgfr26s_3?cidTexte=JORFTEXT000036171877) Completed and signed declaration;
- valid identification or any document attesting to the applicant's nationality;
- Copying the training title to carry out the activity of audioprosthetist in the Member State;
- a certificate of less than three months certifying that the applicant is legally established in the EU State or the EEA and that he is not prohibited from practising temporarily or permanently;
- where the training title has been acquired in a third state and recognised in a Member State:- Recognition of the training title by the Member State,
  - proof that the applicant has practised as an audioprosthetist in that state for at least three years;
- If so, a copy of his previous statement and the first one made;
- a statement justifying that the applicant has the language skills necessary to practice his profession in France.

**Outcome of the procedure**

Within one month of receiving the applicant's file, the prefect informs him:

- that he can begin the provision of services without prior verification of his professional qualifications;
- that following the verification of his professional qualifications, the professional must submit to an aptitude test;
- that it cannot begin service delivery.

The prefect registers the professional on a particular list and sends him a receipt stating a registration number. In the absence of a response from the prefect within one month, service delivery may begin.

**Please note**

The declaration is renewable every year.

*For further information*: Articles L. 4361-9, R. 4361-16 and R. 4331-12 to R. 4331-15 of the Public Health Code; December 8, 2017 order on the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### c. Application for authorization to practice for an LE

**Competent authority**

The request must be sent in double copy by letter recommended with notice of receipt to the secretariat of the committee of audioprosthetists composed of professionals.

**Supporting documents**

The applicant's application must include:

- The[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DDB2C307150C1EBD51D563B436CD57D5.tplgfr40s_3?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180221)Completed and signed application;
- A copy of his valid ID
- A copy of his training title allowing the practice of the profession in his country of obtainment and, if necessary, his additional diplomas as well as a certificate mentioning the level and detail of the training;
- any proof of professional experience, continuing training in an EU Member State or the EEA;
- any document justifying that it is not subject to any sanction;
- where the Member State does not regulate access to the profession or its exercise, a proof that the professional has been an audio-prosthetist for one year in the last ten years;
- where the applicant holds a training certificate issued by a third state and recognised by a Member State, the recognition of the training title by the Member State.

**Outcome of the procedure**

The prefect of the region of the place of establishment of the professional acknowledges receipt of the file within one month of its receipt and issues the authorization of exercise after notice of the aforementioned commission.

**Please note**

The silence kept by the prefect after four months from the receipt of the full file is worth rejecting the application.

*For further information*: Articles R. 4361-13 to R. 4361-15 of the Public Health Code; decree of 25 February 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist and optician-lunetier.

### d. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

