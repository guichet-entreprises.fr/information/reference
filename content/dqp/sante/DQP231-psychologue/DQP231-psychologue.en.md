﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP231" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Psychologist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="psychologist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/psychologist.html" -->
<!-- var(last-update)="2020-04-15 17:22:04" -->
<!-- var(url-name)="psychologist" -->
<!-- var(translation)="Auto" -->


Psychologist
============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:04<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The psychologist is a professional who listens, accompanies and helps people with moral or psychological disorders, punctual or chronic.

He performs psychological evaluations, makes diagnoses and implements individual or collective therapies tailored to his patients.

The psychologist also works with medical, social and educational professionals by raising awareness and informing them about the psychological aspect.

2°. Professional qualifications
----------------------------------------

### a. National requirements

### National legislation

According to section 44 of Act 85-772 of 25 July 1985, "the professional use of the title of psychologist, accompanied or not by a qualifier, is reserved for holders of a diploma, certificate or title sanctioning a basic and applied university education of a high level in psychology preparing for professional life and listed by decree in the Council of State or holders of a recognized foreign diploma equivalent to the national diplomas required."

Under article 5 of Decree 90-255 of 22 March 1990, which establishes the list of diplomas for professional use of the title of psychologist, the recognition of the equivalent character of foreign degrees is the responsibility of the Minister responsible for higher education after the advice of a commission whose composition is fixed by decree of that minister.

Furthermore, Article II of Article 44 of the Act of 25 July 1985 provides that holders of a diploma, certificate or title allowing the practice of the profession in another state may be allowed to make professional use of the title of psychologist when that allows the practice of the profession in that state.

*For further information*: Article 1 of the decree of 22 March 1990 and article 44 of the Act of 25 July 1985.

### Training

A bachelor's degree in psychology and a master's degree in psychology with a professional internship of at least 500 hours are required.

During the internship, completed on an ongoing or in fractional periods, the student is placed under the responsibility of a qualified practitioner-referent psychologist who has been practising for at least three years and a trainee teacher who is one of the teacher-researchers of the training leading to the master's degree. The internship must be completed no later than one year after the theoretical training provided as part of the master's degree in psychology.

At the end of the internship, the person concerned submits an internship report and supports it before a jury composed of the course managers and a psychology teacher-researcher appointed by the head of the master's psychology mention.

The validation of the internship results in the issuance of a certificate that must be in accordance with the model attached to the order of 19 May 2006.

**Please note**

The usurpation of the title of psychologist is a crime punishable by the penalties provided by Article 433-17 of the Penal Code, i.e. one year's imprisonment and a fine of 15,000 euros.

*For further information*: Article 433 of the Penal Code; May 16, 2006.

### Related costs

Training leading to the degree of psychologist is paid for. Its cost varies depending on the institutions that provide the teachings.

### b. EU and EEU nationals: for temporary or casual exercise (Free Service)

There are no regulations for a national of a member of the European Union (EU) or part of the European Economic Area (EEA) wishing to practise as a psychologist in France, either on a temporary or casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals (see below "5. "Qualification Recognition Process and Formalities") will find to apply.

### c. EU and EEA nationals: for a permanent exercise (Freedom of establishment)

As the profession of psychologist is not part of the automatic diploma recognition scheme, any person holding diplomas from another EU Member State or a party to the EEA agreement wishing to exercise permanent profession of psychologist in France must obtain a license to practice issued by the Minister responsible for higher education (cf. infra "5. Qualification process and formalities").

Where there is a substantial difference between the training provided by the applicant and that required in France, he may be subject to compensation measures (see infra "Good to know: compensation measures") provided for by Decree No. 2003-1073 of November 14 2003 amended.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethics and ethics

Although not regulated, there are ethical and ethical obligations on psychologists, including:

- Respect professional secrecy
- Ensure that the dignity of patients is maintained;
- act honestly and with integrity.

The unregulated Code of Ethics dates back to 1996. It was revised in 2012.

### b. Cumulative activities

The psychologist working in the hospital function may engage in another professional activity. The combination of the two activities must be consistent with the dignity and quality required by its professional practice and that it be in compliance with current regulations.

*For further information*: Articles 5 to 12 of the January 27, 2017 decree.

4°. Social legislation and insurance
--------------------------------------------------------

In the event of a liberal exercise, the psychologist is obliged to take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during this activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obtaining permission to practise for EU or EEA nationals for a permanent exercise (LE)

The holder of diplomas issued in another EU Member State or party to the EEA agreement must apply for authorisation to practise.

**Competent authority**

Permission to practise as a psychologist is issued by the Minister responsible for higher education, after the advice of a committee of experts composed of teacher-researchers and representatives of professional organisations.

**Procedure**

In support of their application for authorisation, the holder of diplomas issued in another EU Member State or party to the EEA agreement must send a complete file to the Ministry for Higher Education, Research and Innovation (MESRI) transmitted by dematerialized means. Cases are heard in order of arrival and up to 80 cases per commission.

**Supporting documents**

The application for leave to practice must include:

- The application form for the authorization of the profession completed. This form is available online on the[MESRI website](http://www.enseignementsup-recherche.gouv.fr/cid66177/psychologue-une-profession-reglementee-en-france.html) ;
- ID
- A resume
- A copy of all degrees, certificates or titles in psychology obtained;
- A copy of the certificates of the authorities issuing the training certificate, specifying the level of training and the detail, year by year, of the lessons taken, their hourly volume and the content and duration of the validated internships;
- If necessary, a certificate from the competent authority of the Member State justifying the practice of the profession of psychologist with an indication of the duration;
- for those who have worked in an EU Member State or a party to the EEA agreement which does not regulate access or the practice of the profession: a certificate justifying the duration of the exercise in that state with the corresponding dates or a record of internships may be followed with an indication of their duration.

Information on the procedure for recognising foreign diplomas to practise as a psychologist, as well as a FAQ, is available[on the MESRI website](http://www.enseignementsup-recherche.gouv.fr/pid24843-cid66177/psychologue-une-profession-reglementee-en-france.html).

**What to know**

Supporting documents, if any, must be translated into French by a sworn translator.

**Outcome of the procedure**

The Committee of Experts gives an opinion, the Minister responsible for higher education is the only one competent to make a reasoned decision.

Therefore, the ministerial decision takes the form of:

- or a favourable decision establishing the recognition of the diploma as equivalent to those required in France for the practice of the profession of psychologist;
- or an unfavourable decision: if necessary, the individual may have to make a choice between an aptitude test and an accommodation course (see below "Good to know: compensation measures").
- or a decision reserved for the sending of additional parts

**Please note**

The silence kept by the Minister responsible for higher education for more than four months on the application for authorisation is worth the decision to reject.

*To go further:* Decree No. 2003-1073 of November 14, 2003.

**Remedy**

In the event of an adverse decision, the applicant may challenge the decision.

It has two months after notification of the decision to form, at the choice of:

- a graceful appeal to MESRI;
- a legal challenge to the administrative court in Paris.

**Cost**

Free.

**Good to know: compensation measures**

Upon receipt of the ministerial decision, the individual must, if necessary, indicate by e-mail his choice between an aptitude test and an accommodation course.

He must file with the president of the requested university a file that includes an application for registration and a copy of the ministerial decision.

**The aptitude test**

It includes written and oral questions and practical exercises, or only one of these control modalities.

**The adaptation course**

It can take place on several training grounds, in institutions in agreement with the organising university.

The trainee has been under the responsibility of a qualified professional who has been practising for at least three years.

At the end of the internship, which may be accompanied by additional theoretical training, the trainee submits a report and will support him before a jury whose president and members are appointed by the president of the university.

The organising university sends the official notification of success to the adaptation course or the aptitude test for the higher education directorate. After reviewing the application, the decision taken by the Minister responsible for higher education is notified to the applicant.

*For further information*: Articles 2 to 7 of the November 18, 2003 order.

### b. Registration with the regional health agency (ARS) on the Adeli directory

**Competent authority**

The registration on the Adeli directory is done with the regional health agency (ARS) of the place of exercise.

**Timeframe**

The application for registration must be submitted before taking office, regardless of the mode of practice (liberal, salaried, mixed).

**Supporting documents**

In support of his application for registration, the psychologist must:

- the favourable decision issued by the Minister for Higher Education establishing the recognition of the diploma as equivalent to those required in France for the practice of the profession of psychologist;
- ID
- Form Cerfa 12269Completed, dated and signed.

**Outcome of the procedure**

An Adeli number is issued, it is mentioned on the receipt of the file, issued by the ARS.

**Cost**

Free.

### c. Remedies

#### National

In the event of an adverse decision, the applicant may challenge the decision.

It has two months after notification of the decision, to form the choice of:

- a graceful appeal to the Minister for Higher Education;
- a legal challenge to the administrative court in Paris.

##### Cost

Free.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a one-week indicative period to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT tries to find an individual and pragmatic solution amicably within an indicative period of ten weeks from the day the case is taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

After SOLVIT processes the file, it presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

