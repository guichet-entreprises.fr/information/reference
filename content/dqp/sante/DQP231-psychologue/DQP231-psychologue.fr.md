﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP231" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Psychologue" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="psychologue" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/psychologue.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="psychologue" -->
<!-- var(translation)="None" -->

# Psychologue

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l‘activité

Le psychologue est un professionnel qui écoute, accompagne et aide les personnes ayant des troubles moraux ou psychiques, ponctuels ou chroniques.

Il réalise des évaluations psychologiques, pose des diagnostics et met en place des thérapies individuelles ou collectives adaptées à ses patients.

Le psychologue intervient également auprès des professionnels médicaux, sociaux et éducatifs en les sensibilisant et les informant sur l’aspect psychologique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Aux termes de l’article 44 de la loi n° 85-772 du 25 juillet 1985 portant diverses dispositions d'ordre social, « l'usage professionnel du titre de psychologue, accompagné ou non d'un qualificatif, est réservé aux titulaires d'un diplôme, certificat ou titre sanctionnant une formation universitaire fondamentale et appliquée de haut niveau en psychologie préparant à la vie professionnelle et figurant sur une liste fixée par décret en Conseil d'État ou aux titulaires d'un diplôme étranger reconnu équivalent aux diplômes nationaux exigés ».

En application du 5° de l’article 1er du décret n° 90-255 du 22 mars 1990 fixant la liste des diplômes permettant de faire usage professionnel du titre de psychologue, la reconnaissance du caractère équivalent de diplômes étrangers relève du ministre chargé de l’enseignement supérieur après avis d’une commission dont la composition est fixée par arrêté de ce ministre.

Par ailleurs, le II de l’article 44 de la loi du 25 juillet 1985 prévoit que les titulaires d'un diplôme, certificat ou titre permettant l'exercice de la profession dans un autre Etat peuvent être autorisés à faire usage professionnel du titre de psychologue lorsque ce titre permet l'exercice de la profession dans cet Etat.

*Pour aller plus loin* : article 1er du décret du 22 mars 1990 et article 44 de la loi du 25 juillet 1985.

#### Formation

Une licence en psychologie et un master mention psychologie comportant un stage professionnel d’au moins 500 heures sont requis.

Lors du stage, accompli de façon continue ou par périodes fractionnées, l’étudiant est placé sous la responsabilité d’un psychologue praticien-référent qualifié exerçant depuis au moins trois ans et d’un maître de stage qui est un des enseignants-chercheurs de la formation conduisant au diplôme de master. Le stage doit être achevé au plus tard un an après la formation théorique dispensée dans le cadre du master en psychologie.

À l’issue du stage, l’intéressé remet un rapport de stage et le soutient devant un jury composé des responsables du stage et d’un enseignant-chercheur en psychologie désigné par le responsable de la mention psychologie du master. 

La validation du stage donne lieu à la délivrance d’une attestation qui doit être conforme au modèle annexé à l’arrêté du 19 mai 2006.

**À noter**

L’usurpation du titre de psychologue est un délit puni des peines prévues par l’article 433-17 du Code pénal, soit un an d'emprisonnement et 15 000 euros d'amende.

*Pour aller plus loin* : article 433 du Code pénal ; arrêté du 16 mai 2006.

#### Coûts associés 

La formation menant à l’obtention du diplôme de psychologue est payante. Son coût varie selon les établissements qui dispensent les enseignements. 

### b. Ressortissants UE et UEE : en vue d’un exercice temporaire ou occasionnel (Libre Prestation de Service)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un membre de l’Union Européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer la profession de psychologue en France, à titre temporaire ou occasionnel.

Dès lors, seules les mesures prises pour le libre établissement des ressortissants de l'UE ou de l'EEE (cf. infra « 5. Démarche et formalités de reconnaissance de qualification ») trouveront à s'appliquer.

### c. Ressortissants UE et EEE : en vue d’un exercice permanent (Libre Établissement)

La profession de psychologue n’entrant pas dans le cadre du régime de reconnaissance automatique des diplômes, toute personne titulaire de diplômes délivrés dans un autre État membre de l’UE ou partie à l’accord sur l’EEE souhaitant exercer à titre permanent la profession de psychologue en France doit obtenir une autorisation d’exercer délivrée par le ministre chargé de l’enseignement supérieur (cf. infra « 5. Démarche et formalités de reconnaissance de qualification »).

Lorsqu’il existe une différence substantielle entre la formation suivie par le demandeur et celle requise en France, il peut est soumis à des mesures de compensation (cf. infra « Bon à savoir : mesures de compensation ») prévues par le décret n° 2003-1073 du 14 novembre 2003 modifié.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### a. Déontologie et éthique

Bien que non réglementée, des obligations déontologiques et éthiques incombent aux psychologues, et notamment :

- respecter le secret professionnel ;
- veiller au maintien de la dignité des patients ;
- agir de manière honnête et intègre.

Le Code de déontologie non réglementé date de 1996. Il a été révisé en 2012.

### b. Cumul d'activités

Le psychologue exerçant au sein de la fonction hospitalière peut exercer une autre activité professionnelle. Le cumul des deux activités devra être compatible avec la dignité et la qualité qu’exige son exercice professionnel et qu’il soit en conformité avec la réglementation en vigueur.

*Pour aller plus loin* : articles 5 à 12 du décret du 27 janvier 2017.

## 4°. Législation sociale et assurances

En cas d’exercice libéral, le psychologue a l’obligation de souscrire une assurance de responsabilité professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. Dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Obtenir une autorisation d'exercer pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE)

Le titulaire de diplômes délivrés dans un autre État membre de l’UE ou partie à l’accord sur l’EEE doit solliciter une autorisation d’exercer.

#### Autorité compétente

L’autorisation d’exercer la profession de psychologue est délivrée par le ministre chargé de l’enseignement supérieur, après avis d’une commission d’experts composée d’enseignants-chercheurs et de représentants des organisations professionnelles.

#### Procédure

À l’appui de sa demande d’autorisation, le titulaire de diplômes délivrés dans un autre État membre de l’UE ou partie à l’accord sur l’EEE doit adresser au ministère chargé de l’enseignement supérieur, de la recherche et de l'innovation (MESRI) un dossier complet transmis par voie dématérialisée. Les dossiers sont instruits par ordre d’arrivée et dans la limite de 80 dossiers par commission.

#### Pièces justificatives

Le dossier de demande d’autorisation d’exercice doit contenir :

- le formulaire de demande d’autorisation d’exercice de la profession complété. Ce formulaire est disponible en ligne sur le [site du MESRI](http://www.enseignementsup-recherche.gouv.fr/cid66177/psychologue-une-profession-reglementee-en-france.html) ;
- une pièce d’identité ;
- un curriculum vitae ;
- une copie de tous les diplômes, certificats ou titres en psychologie obtenus ;
- une copie des attestations des autorités ayant délivré le titre de formation, précisant le niveau de formation et le détail, année par année, des enseignements suivis, leur volume horaires ainsi que le contenu et la durée des stages validés ;
- le cas échéant, une attestation de l’autorité compétente de l'État membre justifiant l'exercice de la profession de psychologue avec indication de la durée ;
- pour les intéressés qui ont exercé dans un État membre de l’UE ou partie à l'accord sur l’EEE qui ne réglemente ni l’accès ni l’exercice de la profession : une attestation justifiant la durée de l’exercice dans cet État avec les dates correspondantes ou un relevé des stages éventuellement suivis avec une indication de leur durée.

Les informations sur la procédure de reconnaissance des diplômes étrangers pour exercer la profession de psychologue, ainsi qu’une FAQ, sont disponibles [sur le site du MESRI](http://www.enseignementsup-recherche.gouv.fr/pid24843-cid66177/psychologue-une-profession-reglementee-en-france.html).

**À savoir**

Les pièces justificatives, le cas échéant, doivent être traduites en français par un traducteur assermenté.

#### Issue de la procédure

La commission d’experts rend un avis, le ministre chargé de l’enseignement supérieur est seul compétent pour prendre une décision motivée. 

Dès lors, la décision ministérielle prend la forme :

- soit d’une décision favorable établissant la reconnaissance du diplôme comme équivalent à ceux exigés en France pour l’exercice de la profession de psychologue ;
- soit d’une décision défavorable : le cas échéant, l’intéressé peut être amené à effectuer un choix entre une épreuve d’aptitude et un stage d’adaptation (cf. infra « Bon à savoir : mesures de compensation »).
- soit d’une décision réservée à l’envoi de pièces complémentaires 

**À noter**

Le silence gardé par le ministre chargé de l’enseignement supérieur pendant plus de quatre mois sur la demande d’autorisation vaut décision de rejet.

*Pour aller plus loin* : décret n° 2003-1073 du 14 novembre 2003.

#### Voie de recours

En cas de décision défavorable, le demandeur peut contester la décision.

Il dispose d’un délai de deux mois après la notification de la décision pour former, au choix :

- un recours gracieux auprès du MESRI ;
- un recours contentieux auprès du tribunal administratif de Paris.

#### Coût

Gratuit.

#### Bon à savoir : mesures de compensation

À la réception de la décision ministérielle, l’intéressé doit, le cas échéant, indiquer par courriel son choix entre une épreuve d’aptitude et un stage d’adaptation.

Il doit déposer auprès du président de l’université sollicitée, un dossier comprenant une demande d’inscription et une copie de la décision ministérielle.

##### L’épreuve d’aptitude

Elle comporte des interrogations écrites et orales et des exercices pratiques, ou une seule de ces modalités de contrôle.

##### Le stage d’adaptation

Il peut se dérouler sur plusieurs terrains de stage, dans des établissements en convention avec l’université organisatrice.

Le stagiaire est placé sous la responsabilité d'un professionnel qualifié exerçant depuis au moins trois ans.

À l'issue du stage, qui peut être accompagné d'une formation théorique complémentaire, le stagiaire remet un rapport et le soutiendra devant un jury dont le président et les membres sont désignés par le président de l'université.

L’université organisatrice transmet la notification officielle de la réussite au stage d’adaptation ou à l’épreuve d’aptitude à la direction de l’enseignement supérieur. Après réexamen de la demande,  la décision prise par le ministre chargé de l’enseignement supérieur est notifiée au demandeur.

*Pour aller plus loin* : articles 2 à 7 de l’arrêté du 18 novembre 2003.

### b. Enregistrement auprès de l’agence régionale de santé (ARS) sur le répertoire Adeli

#### Autorité compétente

L’enregistrement sur le répertoire Adeli se fait auprès de l’agence régionale de santé (ARS) du lieu d’exercice.

#### Délai

La demande d’enregistrement doit être présentée avant la prise de fonction, quel que soit le mode d’exercice (libéral, salarié, mixte).

#### Pièces justificatives

À l'appui de sa demande d'enregistrement, le psychologue doit :

- la décision favorable délivrée par le ministre chargé de l’enseignement supérieur établissant la reconnaissance du diplôme comme équivalent à ceux exigés en France pour l’exercice de la profession de psychologue ;
- une pièce d’identité ;
- le formulaire Cerfa 12269*02 complété, daté et signé.

#### Issue de la procédure

Un numéro Adeli est délivré, il est mentionné sur le récépissé du dossier, délivré par l’ARS.

#### Coût

Gratuit.

### c. Voies de recours

#### Nationales

En cas de décision défavorable, le demandeur peut contester la décision.

Il dispose d’un délai de deux mois après la notification de la décision, pour former au choix :

- un recours gracieux auprès du ministre chargé de l’enseignement supérieur ;
- un recours contentieux auprès du tribunal administratif de Paris.

##### Coût

Gratuit.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai indicatif d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT tente de trouver une solution individuelle et pragmatique à l'amiable dans un délai indicatif de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du traitement du dossier par SOLVIT, ce dernier présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).