﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP224" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Pharmacy technician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="pharmacy-technician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/pharmacy-technician.html" -->
<!-- var(last-update)="2020-04-15 17:22:02" -->
<!-- var(url-name)="pharmacy-technician" -->
<!-- var(translation)="Auto" -->


Pharmacy technician
=================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:02<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The pharmacy preparer is a health professional responsible for preparing medical prescriptions and managing stocks (supply, storage, control of deliveries, etc.) of medicines of the dispensary, under the responsibility of the pharmacist.

Within the dispensary, he also plays a role as a trader, as he must be able to advise and sell products to customers.

*For further information*: Article L. 4241-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of pharmacy preparer is reserved:

- Professional pharmacy preparer certificate holders;
- persons who have received an exercise permit issued by the Minister of Health.

*For further information*: Articles L. 4241-4 and L. 4241-6 of the Public Health Code.

#### Training

The professional pharmacy preparer certificate is a diploma available after a two-year S or STL (laboratory science and technology) degree.

The student will follow a theoretical teaching of 800 hours in a training centre while alternating with a practical teaching directly followed in the dispensary.

*To go more law*n: Section D. 4241-3 of the Public Health Code.

#### Costs associated with qualification

Training pays off. For more information, it is advisable to get closer to the dispensing establishments.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) or European Economic Area (EEA) state, who is legally practising as a pharmacy preparer in one of these states, may use his or her professional title in France, either temporarily or temporarily. Casual.

He will have to apply for it, before his first performance, by declaration addressed to the prefect of the region in which he wishes to make the delivery (see infra "4°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional will have to justify having carried out it in one or more EU or EEA states for at least one year, during the course of ten years before the performance.

*For further information*: Article L. 4241-11 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state who is established and legally practises pharmacy preparation in that state may carry out the same activity in France on a permanent basis if he:

- holds a training certificate issued by a competent authority in another EU or EEA state, which regulates access to or exercise of the profession;
- has worked full-time or part-time for two years in the last ten years in another EU or EEA state that does not regulate training or practising;
- holds a title issued by a competent authority of a third state and recognised by an EU or EEA state, and justifies having exercised the activity or profession for at least three years in the EU or EEA state.

Once the national fulfils one of these conditions, he or she will be able to apply for an individual authorisation to practise from the prefect of the region in which he wishes to practice his profession (see infra "4°. b. Request an exercise permit for the EU or EEA national for permanent activity (LE) ").

*For further information*: Article L. 4241-7 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Although they do not have the same prerogatives as pharmacists, pharmacy preparers are concerned by the rules of the Pharmacists' Code of Ethics. Thus, they must respect the rules concerning the operation of the dispensary in advertising and communication, professional secrecy, life and the human person.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The prefect of the region is competent to decide on the request for a prior declaration of activity.

**Supporting documents**

The application is made by filing a file that includes the following documents:

- A copy of a valid ID
- A copy of the training title allowing the profession to be practised in the state of obtainment;
- a certificate, less than three months old, from the competent authority of the EU State or the EEA, certifying that the person concerned is legally established in that state and that, when the certificate is issued, there is no prohibition, even temporary, Exercise
- any evidence justifying that the national has practised the profession in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise;
- where the training certificate has been issued by a third state and recognised in an EU or EEA state other than France:- recognition of the training title established by the state authorities that have recognised this title,
  - any evidence justifying that the national has practiced the profession in that state for three years;
- If so, a copy of the previous statement as well as the first statement made;
- a certificate of professional civil liability.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Timeframe**

Upon receipt of the file, the prefect of the region will have one month to decide on the application and will inform the national:

- He can start the performance.
- that he will be subject to a compensation measure if there are substantial differences between the training or professional experience of the national and those required in France;
- he will not be able to start the performance;
- any difficulty that could delay its decision. In the latter case, the regional prefect will be able to make his decision within two months of the resolution of this difficulty, and no later than three months after notification to the national.

The prefect's silence within these deadlines will be worth accepting the request for declaration.

**Please note**

The return is renewable every year or at each change in the applicant's situation.

*For further information*: Article R. 4241-13 of the Public Health Code; December 8, 2017 order on the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### b. Requesting an exercise permit for the EU or EEA national for permanent activity (LE)

**Competent authority**

The authorisation to exercise is issued by the prefect of the region, after advice from the committee of pharmacy preparers.

**Supporting documents**

The application for authorization is made by filing a file in two copies, including all the following documents:

- The [application form for authorization to practice](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021862830&dateTexte=20180319) Completed and signed;
- A photocopy of a valid ID
- A copy of his training title allowing him to work as a pharmacy preparer and, if necessary, a photocopy of additional diplomas;
- all the documents to justify his continuous training and professional experience acquired in a Member State;
- a declaration by the EU State or the EEA that the national is not subject to any sanctions;
- A copy of all of his certificates mentioning the level of training received, and the details of the hours and volume of teachings followed;
- where neither access to training nor its exercise is regulated in the Member State, any documentation to justify that it has been a pharmacy preparer for two years in the last ten years;
- where the training certificate has been issued by a third state but recognised in a Member State, the Recognition of the Training Title by the Member State.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The prefect acknowledges receipt of the file within one month and will decide after having the opinion of the committee of pharmacy preparers. The latter is responsible for examining the knowledge and skills of the national acquired during his training or during his professional experience. It may subject the national to a compensation measure.

The silence kept by the prefect of the region within four months is worth rejecting the application for authorisation.

*For further information*: Articles R. 4241-9 to 4241-12 of the Public Health Code; decree of 19 February 2010 setting out the composition of the file to be provided to the competent authorisation commission for the examination of applications submitted for the practice in France of the profession of pharmacy preparer and pharmacy preparer Hospital.

**Good to know: compensation measures**

If the examination of the professional qualifications attested by the training qualifications and the professional experience shows substantial differences with the qualifications required for access and exercise in France, the person concerned must submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority may either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- require an adjustment course and/or aptitude test.

*For further information*: decree of 24 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice in France of pharmacy preparer and hospital pharmacy preparer by nationals of the Member States European Union or party to the European Economic Area agreement.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

