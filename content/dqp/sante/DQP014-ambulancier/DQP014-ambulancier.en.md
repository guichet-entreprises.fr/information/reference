﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP014" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Emergency medical technician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="emergency-medical-technician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/emergency-medical-technician.html" -->
<!-- var(last-update)="2020-04-15 17:21:13" -->
<!-- var(url-name)="emergency-medical-technician" -->
<!-- var(translation)="Auto" -->


Emergency medical technician
=========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:13<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The paramedic provides, on medical prescription or in case of emergency, the care and transport of sick or injured persons in medical transport vehicles adapted for**reasons for care or diagnosis**. It ensures the comfort of the transported, installs them during the journey and can intervene in case of emergency to provide first aid.

*To go further* Appendix III of the January 26, 2006 order on the training conditions of the paramedic and the ambulance diploma.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of ambulance, requires the completion of one of the following diplomas or titles:

- State ambulance diploma;
- Ambulance capacity certificate
- Paramedic diploma.

*To go further* Article L. 4393-2 of the Public Health Code.

#### Training

The ambulance diploma is issued by the prefect of the region to persons who have completed, except partial exemption, the entire training leading to this diploma and pass the certification tests.

#### Conditions for access to training leading to ambulance graduation

To obtain an ambulance diploma, the conditions of access are:

- pass the selection tests to join a training institute;
- Provide prefectural certification of fitness to drive a ambulance after medical examination;
- provide a medical certificate of non-contradictory to the ambulance profession, issued by a licensed physician (which attests to the absence of musculoskeletal, psychological problems, a disability incompatible with the profession: visual, hearing impairment, amputation of a limb, etc.) ;
- Provide a medical certificate of vaccination in accordance with current regulations;
- Have a driver's licence outside the probationary period in accordance with the law and valid with a prefectural certificate of fitness to drive an ambulance;
- provide a certificate of training in emergency care and action.

**What to know**

No prior degree or age requirements are required.

Training for the State Ambulance Diploma takes about five months. For more details, it is advisable to get closer to the training institute in question.

*To go further* 26 January 2006.

#### Costs associated with qualification

Training for the State Ambulance Diploma pays off. Its cost varies according to the training institutes (from about 4,000 to 5,000 euros, for example). For more details, it is advisable to get closer to the training institute in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

The national of a Member State of the European Union (EU)*or party to the European Economic Area (EEA) agreement*, legally established in one of these states may carry out the same activity in France on a temporary and occasional basis to have sent a prior declaration of activity to the prefect of the department of the delivery site.

If neither access nor training is regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

The provider's professional qualifications are checked before the first service is provided. In the event of a substantial difference between the claimant's qualifications and the training required in France, which is likely to harm public health, the competent authority asks the claimant to prove that he has acquired the knowledge and skills compensation measures (see infra "Good to know: compensation measures").

In all cases, the European national wishing to practise in France on a temporary or occasional basis must possess the necessary language skills to carry out the activity and master the weight and measurement systems used in France. France.

*To go further* Article L. 4393-5 and L. 4393-6 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

EU nationals wishing to carry out a permanent ambulance activity in France must obtain a licence to practise.

EU or EEA nationals may be allowed to practise in France if they have successfully completed a post-secondary education cycle and are holders, of their choice:

- a training certificate issued by an EU or EEA state in which access or practice of the profession is regulated and which allows legal practice to be practised;
- a training certificate issued by an EU or EEA state in which neither access nor the practice of the profession is regulated, accompanied by a certificate justifying the exercise of this activity in that state for at least the equivalent of two years at the end of the full-time over the past ten years;
- a training certificate, issued by a third state, recognised in an EU or EEA state, other than France, allowing the profession to be legally practised there.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected, depending on his choice, to an test (see below: "Good to know: compensation measures").

*To go further* Article L. 4393-3 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Although not codified, the ethical and ethical obligations rest with the paramedics. In particular, these professionals must respect professional confidentiality and ensure the dignity of patients.

4°. Insurance
---------------------------------

In the event of a liberal exercise, the ambulance service is required to take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out on occasion.

5°. Qualification recognition process and formalities
---------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally performs ambulance activities in an EU or EEA state may exercise in France on a temporary and occasional basis provided they are a prior declaration.

If neither access nor the exercise of this activity is regulated in the EU or EEA State of practice, the person concerned must provide evidence of a professional exercise of at least two years full-time in the last ten years.

The person's professional qualifications are checked before the first service is provided. In the event of substantial differences between the person's qualifications and the training required in France, the claimant may be required to prove that he has acquired the missing knowledge and skills, including through compensation (see below: "Good to know: compensation measures").

#### Competent authority

The prior declaration of activity must be addressed, before the first performance, to the Regional Directorate of Youth, Sports and Social Cohesion (DRJSCS) in Normandy.

#### Renewal of pre-declaration

The advance declaration must be renewed once a year if the claimant wishes to make a new benefit in France.

#### Procedure

The Minister for Health speaks after the advice of the Normandy Ambulance Commission. Within one month of receiving the statement, the Minister informs the individual that he or she may or may not begin the provision of services or that he must provide proof that he has acquired the missing knowledge and skills, including through compensation measures.

The Minister may also inform within one month of receiving the declaration of the need for further information. In this case, the Minister has two months from receiving additional information to inform the person concerned whether or not he can begin his service delivery.

In the absence of a response from the Minister responsible for health within the aforementioned timeframe, service delivery may begin.

#### Delivery of receipt

The Minister responsible for health lists the health care provider on a particular list and sends him a receipt with his registration number. The claimant must then inform the relevant national insurance agency of its benefit by sending a copy of the receipt provided by the Minister responsible for health.

#### Supporting documents

The supporting documents to be provided are:

- The completed declaration form, the model of which is presented as an appendix to the March 24, 2010 order;
- Photocopying of an ID, to be completed with a certificate of nationality if necessary;
- photocopying of the training title (translated into French by a certified translator);
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and does not incur any prohibition on practising (translated into French by a certified translator).

#### Cost

Free.

*To go further* Articles L. 4393-5, R. 4393-5, R. 4331-12 to R. 4331-15 of the Public Health Code, Order of March 24, 2010 relating to the prior declaration of service delivery for the practice of the professions of caregiver, child care assistant and ambulance and an order of 11 August 2010 appointing regional commissions to advise on declarations of free provision of services for medical assistants, nursing assistants, childcare assistants and paramedics.

### b. Formalities for EU nationals for a permanent exercise (LE)

#### Asking for permission to practice

A national of an EU or EEA state must obtain an authorisation in order to be able to practise as an ambulance driver on French territory on a permanent basis.

#### Competent authority

The request must be addressed to the regional prefect of the person's place of settlement. The prefect issues the authorisation to exercise, after advice from the ambulance board.

#### Procedure

The applicant must submit his application in duplicate by letter recommended with notice of receipt accompanied by a complete file to the Regional Directorate of Youth, Sports and Social Cohesion (DRJSCS) of the place where he wishes to practice.

#### Time

The regional prefect must acknowledge receipt within one month of receipt of the application and the full file. In the event of an incomplete file, the prefect must indicate the missing documents and the time frame within which they must be communicated.

If the prefect remains silent for four months after the application is received, the application for leave is considered rejected.

#### Supporting documents

The supporting documents to be provided are:

- The application form for authorisation to practice the profession, the model of which is provided as an appendix to the order of March 24, 2010;
- A photocopy of a valid ID
- A copy of the training title allowing the profession to be practised in the country of obtainment;
- A copy of the additional diplomas if necessary;
- all useful documents justifying continuous training, experience and skills acquired during work experience in an EU state, EEA or third state;
- a statement by the competent authority of the EU State or the Establishment EEA, less than one year old, attesting to the absence of sanctions against the applicant;
- A copy of the certificates of the authorities issuing the training certificate specifying the level of training, the details of the lessons year by year and the content and duration of the internships carried out;
- for those who have worked in an EU or EEA state that does not regulate access or the practice of the profession, all documents justifying an exercise in that state for the equivalent of two full-time years in the last ten years;
- for those persons holding a training certificate issued by a third state and recognised in an EU or EEA state, other than France, the recognition of the training certificate established by the state authorities having recognised that title.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

#### Remedies

If the application for leave is rejected, the applicant may initiate:

- a graceful appeal to the regional prefect who made the decision within two months of the decision (implicit or express) of rejection;
- a hierarchical appeal to the Minister responsible for health within two months of the decision (implicit or express) of rejection;
- a legal appeal before the territorially competent administrative court within two months of the decision (implicit or express) of rejection.

**Good to know: compensation measures**

In order to obtain permission to practise, the person concerned may be required to undergo an aptitude test or an adjustment course, if it appears that the professional qualifications and experience he uses are substantially different. those required for the practice of the profession in France.

If compensation measures are deemed necessary, the regional prefect responsible for issuing the exercise authorization indicates to the person concerned that he has two months to choose between the aptitude test and the adjustment course.

#### The aptitude test

The DRJSCS, which organises the aptitude tests, must summon the person by recommended letter with notice of receipt at least one month before the start of the tests. This summons mentions the day, time and place of the trial.

The aptitude test may take the form of written or oral questions noted on 20 on each of the subjects that were not initially taught or not acquired during the professional experience.

Admission is pronounced on the condition that the individual has achieved a minimum average of 10 out of 20, with no score less than 8 out of 20. The results of the test are notified to the person concerned by the regional prefect.

If successful, the regional prefect authorizes the person concerned to practise the profession.

#### The adaptation course

It is carried out in a public or private health facility approved by the regional health agency (ARS). The trainee is placed under the pedagogical responsibility of a qualified professional who has been practising the profession for at least three years and who establishes an evaluation report.

The internship, which eventually includes additional theoretical training, is validated by the head of the reception structure on the proposal of the qualified professional evaluating the trainee.

The results of the internship are notified to the person concerned by the regional prefect.

The application for leave to practice is then made after further notice from the Nursing Committee.

*To go further* Articles L. 4391-2 and the following and D. 4391-2 and following of the Public Health Code and the order of March 24, 2010, above.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

#### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

#### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

#### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

#### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

#### Cost

Free.

#### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

#### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris,[official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

