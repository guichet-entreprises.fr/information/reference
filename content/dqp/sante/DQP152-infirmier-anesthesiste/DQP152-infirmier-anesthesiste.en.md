﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP152" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Nurse anaesthetist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="nurse-anaesthetist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/nurse-anaesthetist.html" -->
<!-- var(last-update)="2020-04-15 17:21:36" -->
<!-- var(url-name)="nurse-anaesthetist" -->
<!-- var(translation)="Auto" -->


Nurse anaesthetist
==================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:36<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The anaesthetist nurse performs specific care and technical gestures in the fields of anesthesia-resuscitation, emergency medicine and pain management. It intervenes in the peri-intervention period and participates in diagnosis, treatment and research. The nurse anaesthetist works collaboratively and under the responsibility of anaesthetist-resuscitators.

*For further information*: Article R. 4311-12 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practise as an anaesthetist nurse, the person must hold the state diploma of nurse anaesthetist (also known as a state-certified anaesthetist nurse: IADE).

It is the regional prefect who issues this diploma to persons holding the state diploma of nurse (or other title allowing the practice of this profession) or midwifery (or another title allowing the practice of this profession).

*For further information*: Article D. 4311-45 of the Public Health Code.

#### Training

The training takes place in a specialized school.

**Conditions for access to an anaesthetist nurse training school:**

- hold the state nursing diploma (or some other title for the practice of the profession mentioned in Articles L. 4311-3 and L. 4311-12 of the Public Health Code) or the State Midwifery Diploma (or some other title for the exercise of midwifery) This profession);
- justify two years of full-time work experience as a nurse;
- pass the entrance exams for training in preparation for the state anaesthetist diploma.

Within the limit of 10% of admissions, persons with a foreign degree of nurse or midwife not validated for the exercise in France can be admitted on condition:

- justify a twenty-four-month professional exercise appreciated in full-time equivalent;
- meet professional-level tests
- to pass a test to assess their command of the French language.

Once admitted to the school of nurse anaesthetist, the training lasts twenty-four months.

*For further information*: Articles D. 4311-45 to D. 4311-48 of the Public Health Code and the July 23, 2012 Order on Training leading to the State Diploma of Nurse Anaesthetist.

#### Costs associated with qualification

Training for the anaesthetist's degree is paid (approximately 10,000 euros). Costs vary from training organization to training organization. For more details, it is advisable to get closer to the organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

The national of a European Union (EU) state*European Economic Area (EEA)*, established and legally carrying out the activities of anesthesiologist in one of these states, may carry out the same activity in France on a temporary and occasional basis on the condition that he has referred the delivery site to the prefect of the department. a prior declaration of activity.

If the training qualification is not recognised in France, the qualifications of the national of an EU or EEA state are checked before his first performance. In the event of substantial differences between the claimant's qualifications and the training required in France, the national must provide proof that he has acquired the missing knowledge and skills, including by submitting to compensation (see below: "Good to know: compensation measures").

In all cases, the European national wishing to practise in France on a temporary or occasional basis must possess the necessary language skills to carry out the activity and master the weight and measurement systems used in France. France.

*For further information*: Article L. 4311-22 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Nurse anaesthetists are under the Special Exercise Authorization regime.

A national of an EU or EEA state with a diploma to perform the duties of an anaesthetist, must obtain an individual authorisation to practise in France. This authorization is issued, if necessary, by the prefect of the region, after the opinion of the Committee of Nurses whose composition is modified to take into account the speciality in question.

In case of substantial differences between the professional qualifications attested by all training titles and professional experience on the one hand, and the qualifications required for access to the profession and its exercise in France on the other hand, the person concerned must submit to a compensation measure consisting of the choice of an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

To practise in France, any EU or EEA national must have the language skills necessary to carry out the activity in France and master the weight and measurement systems used in France.

*For further information*: Article L. 4311-4 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

All general duties imposed on nurses apply to nationals practising in France.

As such, nurses must respect the principles of dignity, non-discrimination or independence. They are subject to the conditions of practice of the profession, the professional rules applicable in France and the competent disciplinary court.

*For further information*: Articles R. 4312-1 and the following from the Public Health Code.

### a. Cumulative activities

The nurse may engage in another professional activity provided that this combination is consistent with the dignity and quality required by his professional practice and that it is in accordance with the regulations in force.

*For further information*: Article R. 4312-20 of the Public Health Code.

### b. Conditions of honorability

In order to practice, the anaesthetist nurse does not have to:

- be subject to a temporary or permanent ban on practising in France or abroad;
- be suspended because of the serious danger to patients by the exercise of the activity.

*For further information*: Articles L. 4311-16 and L. 4311-26 of the Public Health Code.

### c. Obligation for continuous professional development

Nurse anaesthetists must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of training (present, mixed or non-presential) in analysis, evaluation, and practice improvement and risk management. All training is recorded in a personal document containing training certificates.

*For further information*: Articles L. 4021-1 and R. 4382-1 of the Public Health Code.

### d. Physical aptitude

An anaesthetist must not have a disability or a medical condition that makes the practice of the profession dangerous.

*For further information*: Article L. 4311-18 of the Public Health Code.

4°. Social legislation and insurance
--------------------------------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a nurse practising on a liberal basis must take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the self-sustaining pension and pension fund of medical assistants (CARPIMKO)

Nurses practising in a liberal capacity, even incidentally, must join the pension and pension fund of nurses, massage therapists, pedicures-podiatrists, speech therapists (CARPIMKO).

#### Supporting documents

The person concerned must contact CARPIMKO as soon as possible:

- The affiliate questionnaire [downloadable from the CARPIMKO website](http://www.carpimko.com/document/pdf/affiliation_declaration.pdf) or a letter mentioning the start date of liberal activity;
- Photocopying of the state diploma;
- a photocopy of the registration number in the "automation of lists" (ADELI) directory of the diploma issued by the regional health agency (ARS) or a photocopy of the back of the diploma.

5°. Qualifications process and formalities
----------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

#### Competent authority

The prior declaration of activity must be addressed to the National Council of the Order of Nurses (CNOI), prior to the first provision of services.

#### Renewal of pre-declaration

The advance declaration must be renewed once a year if the provider wishes to re-provide services in France.

#### Supporting documents

The supporting documents to be provided are:

- Complete the activity advance declaration form, the model of which is defined as an appendix to the January 20, 2010 order;
- produce the certificate of subscription to a professional liability insurance for acts performed on French territory;
- provide a photocopy of a piece of identification attesting to the applicant's nationality;
- provide a photocopy of the training title or titles
- certifying the competent authority of the State of The Establishment, member of the EU or the EEA, certifying that the person concerned is legally established there and that, when the certificate is issued, there is no prohibition, even temporary, from practising .

**What to know**

The supporting documents, with the exception of the photocopying of the ID, must be translated into French by a translator authorised with the French courts or empowered to intervene with the judicial or administrative authorities of a state MEMBER of the EU or the EEA.

#### Time

Within one month of receiving the advance declaration, the CNOI informs the claimant of the result of the examination of his qualifications;

Within this time, the CNOI may request additional information. In this case, the initial one-month period is extended by one month.

At the end of this procedure, the CNOI informs the claimant, as the case may be:

- That it can begin service delivery
- It cannot begin service delivery;
- that he must prove that he has acquired the missing knowledge and skills if his training shows substantial differences with the training required in France (notably through a compensation measure).

In the absence of a response from the CNOI on time, service delivery may begin.

#### Delivery of receipt

The CNOI registers the service provider on a particular list and then sends the provider a registration receipt. The claimant must then inform the relevant national health insurance agency of his benefit and provide him with his registration number.

**What to know**

The casual and temporary claimant is not required to register with the ADELI directory and is not liable for the ordinal contribution.

#### Cost

Free.

*For further information*: Article L. 4311-22 and following and R. 4311-38 and following of the Public Health Code and the order of January 20, 2010 mentioned above.

### b. Formalities for EU nationals for a permanent exercise (LE)

#### Request a special license to practice

##### Competent authority

The application for permission to practise is addressed to the regional prefect of the person's place of settlement. The latter issues, if necessary, the authorisation to practice, after advice from the Committee of Nurses.

##### Procedure

The application for authorization to exercise must be addressed to the DRJSCS of the proposed location of practice, by letter recommended with notice of receipt. The regional prefect acknowledges receipt of the request within one month of receiving the file.

##### Supporting documents

The application for leave to practice must be set in two copies and contain:

- an application form for authorization to practice the profession completed and showing the specialty in which the applicant files his application (nurse anaesthetist). This form is available online on the DRJSCS website under review;
- A photocopy of a valid ID
- A copy of the training title allowing the practice of the profession in the country of obtaining as well as, if necessary, the specialized training title;
- If necessary, a copy of the additional diplomas;
- all useful documents justifying continuous training, experience and skills acquired during the professional year in an EU or EEA Member State or a third state;
- a statement, less than a year old, from the competent authority of the EU State or the EEA attesting to the absence of sanctions;
- A copy of the certificates of the authorities issuing the training certificate, specifying the level of training and the detail, year by year, of the lessons taken and their hourly volumes as well as the content and duration of the validated internships;
- for those who have worked in an EU or EEA state that does not regulate access or the practice of the profession: all the documents justifying their exercise in that state the equivalent of two full-time years in the last ten years;
- for those who hold a training certificate issued by a third state and recognised in an EU or EEA state, other than France: the recognition of the training certificate and, if necessary, the specialist designation established by the state of EU or EEA having recognised these titles.

**Good to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

In order to obtain this authorization, the person concerned may be required to carry out compensation measures (fitness test or accommodation training) if it turns out that the qualifications and work experience he uses are substantially different from those required for the practice of the profession in France.

If compensation measures are deemed necessary, the regional prefect responsible for issuing the exercise authorization indicates to the person concerned that he has two months to choose between the aptitude test and the adjustment course (see below: "Good to know: compensation measures").

##### Outcome of the procedure

The silence kept by the regional prefect at the end of a period of four months from the receipt of the full file is worth the decision to reject the application. If, on the contrary, permission to practise is granted, it applies only to the profession of anaesthetist nurse.

##### Remedies

If the application for leave to practice is refused (implicit or expressly), the applicant may challenge that decision. It can thus within two months of notification of the refusal decision, form at the choice:

- a graceful appeal to the regional prefect;
- a hierarchical appeal to the Minister for Health;
- legal action before the territorially competent administrative court.

##### Cost

Free

**Good to know: compensation measures**

**The aptitude test**

The DRJSCS, the organizer of the aptitude tests, must summon the person by recommended letter with notice of receipt, at least one month before the start of the tests. This summons mentions the day, time and place of the trial. The aptitude test may take the form of written or oral questions noted on 20 on each of the subjects that were not initially taught or not acquired during the professional experience.

Admission is pronounced on the condition that the individual has achieved a minimum average of 10 out of 20, with no score less than 8 out of 20. The results of the test are notified to the person concerned by the regional prefect.

If successful, the regional prefect authorizes the person concerned to practise the profession.

##### The adaptation course

It is carried out in a public or private health facility accredited by the ARS. The trainee is placed under the pedagogical responsibility of a qualified professional who has been practising the profession for at least three years and who establishes an evaluation report.

The internship, which eventually includes additional theoretical training, is validated by the head of the reception structure on the proposal of the qualified professional evaluating the trainee.

The results of the internship are notified to the person concerned by the regional prefect.

If necessary, the decision to authorize the exercise is then taken, after a new opinion of the commission referred to in Article L. 4311-4 of the Public Health Code.

*For further information*: Articles L. 4311-4, R. 4311-34 and following of the Public Health Code and the order of January 20, 2010 above.

#### Ask for inclusion on the nurses' order table

To practice the profession of anaesthetist nurse, it is mandatory to register on the list of nurses. This inscription makes the practice of the profession lawful on French territory.

##### Competent authority

The application for registration must be made to the departmental or interdepartmental council of the Order of Nurses (CDOI or CIOI) in which the person concerned wishes to practice, preferably by letter recommended with notice of receipt.

##### Procedure

Upon receipt of the full application file, the President of the CDOI or CIOI acknowledges receipt of the application within one month. The CDOI or CIOI has a maximum of three months to study and decide on the application. The decision is made known to the person concerned by recommended letter with notice of receipt up to one week after the deliberation of the CDOI or CIOI.

##### Supporting documents

The supporting documents to be provided are:

- The application form on the order's list to be withdrawn from the CDOI or CIOI considered or downloaded from the [national order of nurses website](http://www.ordre-infirmiers.fr/lordre-et-les-conseils-ordinaux/inscription-a-lordre.html) ;
- a photocopy of a valid piece of identification accompanied, if necessary, by a certificate of nationality issued by a competent authority;
- a copy of the nursing diploma (translated by a certified translator, if applicable). This copy must be accompanied by:- or a certificate from the State of the issuance of the diploma certifying training in accordance with European obligations,
  - certification that the individual has completed the equivalent of two years in full-time, in the ten years prior to the application, the nursing profession including full programming, organization and administration of care nursing patients;
- evidence of knowledge of the French language and the system of weights and measures used in the country;
- proof of character: either a criminal record extract, less than three months old, issued by a competent authority of the State of origin or origin, or a certificate of morality or honourability issued by the council of the order or The competent authority of the EU Member State less than three months old;
- a statement on the honour of the person certifying that he or she is not the subject of a case that could give rise to a conviction or sanction that could have an impact on the inscription on the board;
- a certificate of registration or registration, issued by the authority with which it was previously registered or registered. Failing that, the person must file a declaration on honour certifying that he or she has never been registered or, failing that, a certificate of registration or registration in an EU or EEA Member State;
- a resume.

Other supporting documents may be required according to cdOI or CIDI: for more details, it is advisable to get closer to the competent authority.

If the person wishes to practice in the form of a company, he must attach, in addition to the documents mentioned above:

- a copy of the statutes and, if any, a copy of the internal regulations and a copy or shipment of the constitution;
- A certificate of registration on the list of the order of each of the partners or, if they are not yet registered, the proof of the application for registration;
- for a liberal exercise(SEL) in-society:- a certificate from the Commercial Court or High Court Registry finding that the company's application for registration is submitted to the registry of the Commercial and Corporate Register,
  - a certificate from the partners specifying the nature and evaluation of the contributions, the amount of the share capital, the number, the minimum amount and the distribution of shares or shares, the affirmation of the total or partial release of the contributions.

##### Remedies

Any appeal must be brought before the regional or inter-regional council in which the CDOI or CIOI is located, which has ruled within 30 days of notification of the decision.

##### Cost

The registration on the order's list is free, but it creates the obligation to submit to the compulsory ordinal dues, the amount of which is set annually by the National Council of the Order.

*For further information*: Articles L. 4311-15 and L. 4311-16, L. 4312-7, R. 4112-1 and beyond, applicable by articles R. 4311-52, R. 4113-4, R. 4113-28 of the Public Health Code.

#### In the case of exercise in the form of a professional civil society (SCP) or an SEL, request the inclusion of the company on the board of the College of Nurses

If the person wishes to practice in the form of a CPS or an SEL, he must put that company on the list of the place where the head office is established.

##### Competent authority

The application for registration must be made to the CDOI or THE CIOI in which the person wishes to practice, preferably by recommended letter with notice of receipt.

##### Procedure

Upon receipt of the full application file, the President of the CDOI or CIOI acknowledges receipt of the application within one month. The CDOI or CIOI has a maximum of three months to study and decide on the application. The decision is made known to the person concerned by recommended letter with notice of receipt up to one week after the deliberation of the CDOI or CIOI.

##### Supporting documents

The supporting documents to be provided are:

- a copy of the statutes and, if any, a copy of the internal regulations and a copy or shipment of the constitution;
- A certificate of registration on the list of the order of each of the partners or, if they are not yet registered, the proof of the application for registration;
- for an EXERCISE in SEL:
- a certificate from the Commercial Court or High Court Registry finding that the company's application for registration is submitted to the registry of the Commercial and Corporate Register,
- a certificate from the partners specifying the nature and evaluation of the contributions, the amount of the share capital, the number, the minimum amount and the distribution of shares or shares, the affirmation of the total or partial release of the contributions.

##### Remedies

Any appeal must be brought before the regional or inter-regional council in which the CDOI or CIOI, which has ruled, is located, within 30 days of notification of the decision.

##### Cost

The registration on the order's list is free, but it creates the obligation to submit to the compulsory ordinal dues, the amount of which is set annually by the National Council of the Order.

#### Request registration of diploma or authorization to practice (ADELI)

Nurses are required to register their training certificate or the required authorization for the practice of the profession.

##### Competent authority

The registration of the diploma or the authorization to exercise must be registered within the ADELI directory ("automation of lists") with the ARS of the place of practice.

##### Time

The application for registration must be submitted within one month of taking office, regardless of the mode of exercise (liberal, salaried, mixed). The receipt issued by the ARS mentions the ADELI number. The LRA then sends the applicant an application form for the award of the professional health card.

###### Supporting documents

The supporting documents to be provided are:

- The original diploma (translated into French by a certified translator);
- ID
- Proof of registration to the order of the practice department;
- form CERFA 10906Completed, dated and signed.

This list may vary from region to region. For more details, it is advisable to get closer to the ARS concerned.

###### Cost

Free.

*For further information*: Article L. 4311-15 of the Public Health Code.

#### Apply for affiliation with health insurance

Registering with health insurance allows the health insurance company to take care of the care performed. In addition, this registration triggers the acquisition of the Health Professional Card (CPS) and care sheets on behalf of the nurse.

###### Competent authority

This is done in the health care professionals' department of the primary health insurance fund (CPAM) at the place of practice.

###### Supporting documents

The supporting documents to be provided are:

- Copying the diploma
- ADELI
- The Professional Health Card (CPS) application form
- A bank identity statement
- vital card and vital card certification.

Other supporting documents may be requested according to the different CPAMs. For more information, it is advisable to get closer to the relevant CPAM.

### c. European Professional Card

The European professional card is an electronic procedure for 500-member professional qualifications in another EU state.

The EPC procedure can be used both when the national wishes to operate in another EU state:

- temporary and occasional;
- on a permanent basis.

The EPC is valid:

- indefinitely in the event of a long-term settlement;
- 18 months in principle for temporary service delivery (or 12 months for occupations that may have an impact on public health or safety).

#### Application for a European business card

To request an EPC:

- you need to create a user account on the European Commission's ECAS authentication service
- you must then complete your EPC profile (identity, contact information, etc.)
- Finally, it is possible to create an EPC application by downloading the scanned supporting documents.

#### Cost

For each CPR application, authorities in the host country and country of origin may charge a file review fee that varies depending on the situation.

#### Time

**For an EPC application for temporary and occasional activity**

Within a week, the country of origin authority acknowledges receipt of the EPC application, reports if documents are missing and informs of any costs. Then, the host country's authorities check the case.

- If no verification is required with the host country, the country of origin authority reviews the application and makes a final decision within three weeks.
- If verifications are required within the host country, the country of origin authority has one month to review the application and forward it to the host country. The host country then makes a final decision within three months.

**For an EPC application for a permanent activity**

Within a week, the country of origin authority acknowledges receipt of the EPC application, reports if documents are missing and informs of any costs. The country of origin then has one month to review the application and forward it to the host country. The latter makes the final decision within three months.

If the host country authorities believe that the level of education or training or work experience is below the standards required in that country, they may apply to take an aptitude test or to perform an adjustment course. .

From the EPC application:

- If the application for CPR is granted, then an online CPR certificate can be obtained.
- If the host country's authorities do not make a decision within the allotted time, qualifications are tacitly recognised and a CPE is issued. It is then possible to obtain a CPR certificate from your online account.
- If the application for cPR is rejected, the decision to refuse must be reasoned and apply for action to challenge the refusal.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information** : SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris,[official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

