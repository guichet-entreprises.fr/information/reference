﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP171" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="General practitioner" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="general-practitioner" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/general-practitioner.html" -->
<!-- var(last-update)="2020-04-15 17:21:39" -->
<!-- var(url-name)="general-practitioner" -->
<!-- var(translation)="Pro" -->


# General practitioner

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:39<!-- end-var -->


<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of the profession

The general practitioner is a primary care or local doctor: he or she is normally the point of first medical contact within the health care system, providing open and unlimited access to its users, dealing with all health problems regardless of the age, sex, or any other characteristic of the person concerned.

General practice covers a large field of activities determined by the needs and wants of patients. Practitioners develop a person-centred approach, orientated to the individual, his/her family and community, with emphasis on building a long-term relationship between the patient and the practitioner.

General practitioners contribute to the provision of outpatient care, by offering prevention, screening, diagnosis, treatment and follow-up of diseases as well as health education for their patients.

They administer and coordinate pain relief care in conjunction, where appropriate, with specialised pain management structures. They ensure customised application of protocols and recommendations for conditions requiring long-term care and contribute to the follow-up of chronic diseases, in cooperation with other professionals involved in the patient's care.

They refer patients, according to their needs, to other professionals in the health care system and the medico-social sector, ensuring coordination of the care necessary for patients and synthesising information transmitted by various health professionals.

For more information on the definition of general practice, you can also consult [the website of the European Society of General Practice - Family Medicine (WONCA Europe)](http://www.woncaeurope.org/sites/default/files/documents/Definition%203rd%20ed%202011%20with%20revised%20wonca%20tree.pdf), which is the European branch of the World Organisation of General Practitioners (WONCA).

**Useful tip**

As part of the coordinated care process, the general practitioner can be chosen as the attending physician. This will affect reimbursement for healthcare costs by the national health insurance fund. For more information about attending physicians, you can consult the <a href="http://www.ameli.fr/assures/soins-et-remboursements/comment-etre-rembourse/le-parcours-de-soins-coordonnes/choisir-et-declarer-votre-medecin-traitant.php">National Health Insurance website</a>.

For more information, please refer to Article L. 4130-1 of the French Public Health Code.

## 2°. Professional qualifications

### a. National requirements

#### National legislation

Pursuant to Article L. 4111-1 of the Public Health Code, to lawfully practise the profession of physician in France, the individual concerned must fulfil all three of the following conditions. He or she must:

* Hold a French state diploma (*diplôme d’État*, DE) as a doctor of medicine, or a diploma, certificate or other evidence of formal qualification listed in Article L. 4131-1 of the Public Health Code (see below “Useful tip: automatic recognition of a diploma”)
* Be a French national, an Andorran citizen or a citizen of a European Union Member State (EU)\* or a European Economic Area Member State (EEA)\* or Morocco, subject to the application of the rules set out in the Public Health Code or in international commitments. This condition does not apply, however, to a physician holding a French *diplôme d’État* as a doctor of medicine
* Unless otherwise specified, be registered with one of the *département* boards of the French Medical Association (*Ordre des médecins*) (see below “5°. a. Applying for registration with the French Medical Association”)

Nevertheless, individuals who do not fulfil the conditions as to their diploma or nationality may be authorised to practise the profession of physician by individual order of the Minister of Health (see below “5°. c. Applying for a personal authorisation to practise, if necessary”).

For more information, please refer to Articles L. 4111-1, L. 4112-6, L. 4112-7 and L. 4131-1 of the Public Health Code.

**Please note:** If all these conditions are not met, practising the profession of physician is unlawful and punishable by two years' imprisonment and a fine of 30,000 euros. For more information, please refer to Articles L. 4161-1 and L. 4161-5 of the Public Health Code.

**Useful tip: Automatic recognition of a diploma**

Pursuant to Article L. 4131-1 of the Public Health Code, EU or EEA citizens may practise the profession of physician if they hold one of the following qualifications:

* Evidence of formal qualifications as a doctor issued by an EU or EEA Member State in accordance with Community obligations and appearing on the list set out in the Annex to [the Order of 13 July 2009 establishing the lists and conditions for the recognition of evidence of formal qualifications as a physician or specialised physician issued by EU Member States or parties to the EEA Agreement referred to in Article L. 4131-1(2°) of the Public Health Code](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020930148&fastPos=1&fastReqId=1774860260&categorieLien=cid&oldAction=rechTexte)
* Evidence of formal qualifications as a physician issued by an EU or EEA Member State in accordance with Community obligations that does not appear in the aforementioned list, if it is accompanied by a certificate from that Member State stating that the qualifications were awarded following a training in accordance with the obligations and that the State considers such qualifications to be the equivalent of qualifications that appear in the list ;
* Evidence of formal qualifications as a physician issued by an EU or EEA Member State awarded following medical training begun in that Member State prior to the dates set out in the aforementioned Order and not in accordance with Community obligations, if accompanied by a statement from one of those Member States certifying that the holder of the evidence of formal qualifications in that State has effectively and lawfully practised the profession of physician in the specialty in question for at least three consecutive years during the five years preceding the issue of the certificate
* Evidence of formal qualifications as a doctor issued by the former Czechoslovakia, the former Soviet Union or the former Yugoslavia or that were awarded following training begun before the dates of independence of the Czech Republic, Slovakia, Estonia, Latvia, Lithuania or Slovenia, if accompanied by a statement from the competent authorities of one of these States stating that this evidence has the same legal validity as evidence of formal qualifications issued by that State. This shall be accompanied by a certificate issued by the same authorities stating that the holder has effectively and lawfully practised in that State the profession of physician in the specialty concerned for at least three consecutive years during the five years preceding the issue of the certificate
* Evidence of formal qualifications as a physician issued by an EU or EEA Member State that does not appear in the aforementioned list if accompanied by a statement issued by the competent authorities of that Member State certifying that the holder of the evidence of formal qualifications was established within its territory as of the date set out in the aforementioned Order and that he or she has earned the right to pursue the activities of a general practitioner under its national social security system
* Evidence of formal qualifications as a physician issued by an EU or EEA Member State that was awarded following medical training begun in that Member State prior to the dates set out in the aforementioned Order and not in accordance with Community obligations but enabling the profession of physician to be lawfully practised in the Member State that issued them, if the physician can prove that he or she has, during the previous five years, performed three consecutive years of full-time hospital duties in France in the specialty that corresponds to the evidence of formal qualifications, as an *attaché associé*, a *praticien attaché associé* or an *assistant associé*, or has performed duties as *chef de clinique associé des universités* or *assistant associé des universités*, provided that he or she performed hospital duties at the same time
* Evidence of formal qualifications as a specialist physician appearing in the aforementioned list awarded by Italy following specialist medical training begun in Italy after 31 December 1983 and prior to 1 January 1991, if accompanied by a certificate issued by the Italian authorities stating that the holder has effectively and lawfully practised the profession of specialist physician in the area in question for at least seven consecutive years during the ten years preceding the issue of the certificate

For more information, please refer to Article L. 4131-1 of the Public Health Code; Order of 13 July 2009 establishing the lists and conditions for the recognition of evidence of formal qualifications as a physician or specialised physician issued by EU Member States or parties to the EEA Agreement referred to in paragraph 2 of Article L. 4131-1 of the Public Health Code.

#### Training

Medical studies in France consist of three cycles lasting a total of between nine and eleven years, depending on the chosen field of study.

The training, which is university-based, includes a number of internships as well as two competitive examinations:

* The first examination is given at the end of the first year of study; known as the “First Common Core Year for Healthcare Studies” (*Première année commune aux études de santé* – PACES). Students of medicine, pharmacy, dentistry, physiotherapy and midwifery all receive the same instruction. At the end of the first examination, students are ranked according to their results. Those with the highest scores are allowed to continue their studies (subject to a *numerus clausus*) and to opt, where appropriate, to continue training leading to the practice of medicine.
* The second examination is the National Ranking Tests (*épreuves classantes nationales* – ECN), formerly referred to as the “*internat*”. They are given at the end of the 2nd cycle (i.e. after the sixth year of study). At the end of this competition, students choose, according to their ranking, their specialty and/or their city of assignment. The length of their subsequent studies depends on the specialty they have chosen.

To obtain their *diplôme d’État* (DE) as a doctor of medicine, students must have successfully completed all of their internships, been awarded a specialist post-graduate diploma (the *diplôme d’études spécialisées* – DES) and successfully defended their thesis.

For more information, please refer to Article L. 632-1 of the Education Code.

**Useful tip**

Medical students must be vaccinated.

For further details, please refer to Article R. 3112-1 of the Public Health Code.

##### Diplôme de formation générale en sciences médicales

The General Diploma in Medical Sciences (*Diplôme de formation générale en sciences médicales*) is awarded to students completing the first cycle, which lasts three years and is the equivalent of an undergraduate degree. The first year is known as the “First Common Core Year for Healthcare Studies” (*Première Année Commune aux Etudes de Santé* – PACES).

At the end of the first cycle, students are expected to have:

* Acquired the basic scientific knowledge that is required to subsequently master the knowledge and skills necessary to exercise a medical profession. This basic knowledge is broad and encompasses biology, some aspects of the exact sciences, and several disciplines of the humanities and social sciences.
* A fundamental approach to healthy and sick individuals, including a full understanding of symptomatology.

The cycle includes theoretical, methodological, applied and practical training as well as a series of internships, including a four-week introductory care internship in a hospital.

For more information, please refer to the Order of 22 March 2011 on the course of study for the General Diploma in Medical Sciences.

##### Diplôme de formation approfondie en sciences médicales

The Advanced Diploma in Medical Sciences (*Diplôme de formation approfondie en sciences médicales*) is awarded to students completing the second cycle, which lasts three years and is the equivalent of a master's degree. 

The goal is to give students the standard skills that will enable them to subsequently carry out the duties required in the third cycle, in both hospital and outpatient settings, and for them to acquire the professional skills for the training for their chosen specialty.

Students are expected to learn to communicate and cooperate, and to acquire the skills required to become a clinician, a member of a multi-disciplinary healthcare team, a public health professional, a scientist and a role model in terms of ethical behaviour. Students must also be capable of reflective thinking.

Lectures primarily focus on frequent or serious illnesses, public health problems, as well as clinical best practices.

At the end of the second cycle, students are expected to have:

* Acquired knowledge relating to physiopathology, pathology, therapeutics and prevention, complementing and deepening knowledge acquired in the previous cycle
* Been trained in the scientific approach
* Learned clinical reasoning
* Acquired the standard skills preparing them for the third cycle

In addition to theoretical and practical training, the training includes 36 months of internships and 25 stints of night or weekend duty (known as *la garde*).

For more information, please refer to the Order of 8 April 2013 on the course of study for the first and second cycles of medical studies.

##### DES de médecine

Entrance to the third cycle is determined by the National Ranking Tests (*épreuves classantes nationales* – ECN). To be able to practise medicine as a general practitioner, a medical student must earn a *DES de médecine générale*. The training leading to this degree lasts three years.

**Please note**

Students are not allowed to sit the ECN more than twice.

The training includes:

* Approximately 200 hours of courses, both general and specific to the chosen specialty. For more information on these courses, please refer to Appendix V of the [Order of 22 September 2004 establishing the list and rules governing the *DES de médecine*](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT00000000807238&fastPos=1&fastReqId=1299214311&categoryLink=cid&oldAction=rechTexte)
* Practical training:
  * Two mandatory general medicine internships in hospitals accredited to provide such internships: six months in adult medicine (internal medicine, geriatrics, etc.) and six months in emergency medicine
  * Two mandatory general medicine internships in settings accredited to provide such internships, including six months in paediatrics or gynaecology
  * Six months with a general practitioner who is a *praticien agréé-maître de stage des universités*
  * Six months in the final year of the third cycle, spent – depending on the intern's professional project – either in ambulatory medicine or in an accredited medical setting as part of a personal project validated by the general medicine coordinator

Throughout the cycle, training in providing psychological and psychiatric care for patients is mandatory. This takes place during internships carried out in health services and settings accredited to provide training for interns and authorised to do so.

For more information, please refer to Article 1 and Appendix V of the Order of 22 September 2004 establishing the list of *DES de médecine*.

#### Costs of qualifying as a physician

The training leading to the *DES de médecine* is fee-based. Its cost varies according to the universities providing the courses. For more details, candidates are advised to contact the university in question.

### b. EU and EEA citizens: temporary and occasional practice (Freedom to Provide Services)

Physicians who are citizens of an EU or EEA Member State and who are legally established and practising medicine in one of these Member States may – on a temporary and occasional basis – practise their profession in France, provided that they previously submitted a declaration to the French Medical Association’s national board (known as the French Medical Council, *Conseil national de l’Ordre des médecins*) (see below “5°. b. Filing a prior declaration for EU or EEA citizens carrying on a temporary or occasional activity”).

**Useful tip**

Registration with the French Medical Council is not required for physicians under the freedom to provide services (FPS), and they are not required to pay dues. They are simply recorded on a specific list maintained by the Council.

The prior declaration must be accompanied by an attestation concerning the language skills required to perform the service. Testing of language proficiency must be proportionate to the service provided and the test must be administered after the professional qualification has been recognised.

In cases where evidence of formal qualifications is not automatically recognised (see above “2°. a. National legislation”), the provider's professional qualifications shall be checked before services are provided. In the event that substantial discrepancies between the qualifications of the individual concerned and the training required in France are such that they may post a threat to public health, the service provider shall be subjected to a test.

Physicians under the freedom to provide services (FPS) are required to comply with professional rules applicable in France, in particular ethical rules (see below “3°. c. Conditions of good repute and ethical rules”). They shall be subject to the disciplinary jurisdiction of the French Medical Council.

**Please note**

The service is provided under the French professional title of physician (*médecin*). However, where evidence of formal qualifications is not recognised and where the qualifications have not been verified, the service shall be provided under the professional title of the Member State of establishment, so as to avoid any confusion with the French professional title.

For more information, please refer to Article L. 4112-7 of the Public Health Code.

### c. EU and EEA citizens: permanent practice (Freedom of Establishment)

#### Automatic recognition of a diploma awarded in another EU Member State

Article L. 4131-1 of the Public Health Code sets out the system for automatically recognising in France certain diplomas or titles, where applicable, accompanied by certificates, obtained in an EU or EEA Member State (see above “2.a. National legislation”).

The French Medical Association’s relevant *département* board is responsible for verifying that any diplomas, titles, certificates and attestations are in order, for allowing automatic recognition of them and for ruling on applications for registration with the Council.

For more information, please refer to Article L. 4131-1 of the Public Health Code; Order of 13 July 2009 establishing the lists and conditions for the recognition of evidence of formal qualifications as a physician or specialised physician issued by EU Member States or parties to the EEA Agreement referred to in paragraph 2 of Article L. 4131-1 of the Public Health Code.

#### Derogations and prior authorisation

If EU or EEA citizens do not fulfil the conditions for the automatic recognition of their qualifications, the authorisation to practise scheme has jurisdiction over their cases (see below “5°. c. Application for an individual authorisation to practise, where appropriate”).

Individuals who do not benefit from automatic recognition but who hold evidence of formal qualifications enabling them to lawfully practise the profession of physician may be authorised by the Minister of Health, and on an individual basis, to practise the specialty in question, following consultation of a board of professionals.

If the assessment of an individual's professional qualifications, as attested to by the evidence of formal qualifications and professional experience, reveals substantial discrepancies with the qualifications required for admission to the profession in the speciality in questions and its practice in France, the individual must agree to compensatory measures.

Depending on the level of qualification required in France and that held by the individual in question, the competent authority may:

* offer the applicant a choice between completing an adaptation period and taking an aptitude test
* make an adaptation period or aptitude test mandatory
* make an adaptation period and aptitude test mandatory

For more information, please refer to Article L. 4131-1-1-1 of the Public Health Code.

## 3°. Conditions of good repute and ethical rules

### a. Compliance with the Code of Medical Ethics

The provisions of the Code of Medical Ethics are binding on all physicians practising in France, whether they are registered with the French Medical Council or are exempt from this obligation (see below “5°. a. Applying for registration with the French Medical Association”).

**Useful tip**

All the provisions of the Code of Medical Ethics are codified in Articles R. 4127-1 to R. 4127-112 of the Public Health Code.

As such, physicians must respect the principles of morality, probity and dedication essential to the practice of medicine. They must also respect patient confidentiality and act independently.

For more information, please refer to Articles R. 4127-1 to R. 4127-112 of the Public Health Code.

### b. Multiple activities

Physicians may only engage in another activity if doing so is compatible with the principles of professional independence and dignity they are obliged to observe. Multiple activities must not allow them to derive benefit from their medical prescriptions or advice.

Thus, physicians cannot combine the practice of medicine with another activity in the health care field. In particular, they are prohibited from practising as opticians, ambulance drivers or managers of ambulance companies, manufacturers or sellers of medical devices, owners or managers of convalescence establishments, sports halls, spas or massage practices.

Similarly, physicians who hold elective or administrative positions are prohibited from using them to expand their practices.

For more information, please refer to Articles R. 4127-26 and R. 4127-27 of the Public Health Code.

### c. Conditions of good repute

To be able to practise medicine, a physician must certify that no proceedings are underway against him/her that could give rise to a conviction or a penalty that would likely affect his/her registration with the French Medical Council.

For more information, please refer to Article R. 4112-1 of the Public Health Code.

### d. Duty of ongoing professional development

Physicians must take part in a multi-year professional development programme. The goals of such a programme include assessing professional practices, developing skills, improving the quality and safety of care, and maintaining and keeping knowledge and skills up-to-date.

All actions by physicians in fulfilment of this duty are recorded in a specific document attesting to their compliance.

For more information, please refer to Articles L. 4021-1 *et seq.* and R. 4021-4 *et seq.* of the Public Health Code.

### e. Physical aptitude

Doctors must not present any infirmity or pathology that is incompatible with the practice of medicine (see below “5°. a. Applying for registration with the French Medical Association”).

For more information, please refer to Article R. 4112-2 of the Public Health Code.

## 4°. Social legislation and insurance

### a. Obligation to take out professional liability insurance

As a healthcare professional, physicians practising in a private capacity must take out professional civil liability insurance.

Conversely, for physicians who are employees, such insurance is optional. In this case, it is up to the employer to take out insurance for its employees to cover actions carried out in the course of their professional duties.

For more information, please refer to Article L. 1142-2 of the Public Health Code.

### b. Obligation to sign up with the French Medical Pension Fund (*Caisse Autonome de Retraite des Médecins de France* – CARMF)

Any physician registered with the French Medical Council and practising as a professional (even part-time and even if he/she is employed elsewhere) is required to sign up with the CARMF.

#### Timeframe

Individuals must begin their affiliation with the CARMF within one month of entering medical practice.

#### How to sign up

Individuals must sign, date and return the application form, which must be countersigned by the instance of the French Medical Council in their département. This form can be downloaded from the [CARMF website](http://www.carmf.fr/doc/formulaires/cotisants/declaration-en-vue.pdf).

**Useful tip**

In the event that a physician is practising medicine within a *Société d’Exercice Libéral* (company formed by self-employed practitioners), CARMF affiliation is also mandatory for all of the company's partners.

### c. Obligation to report one's practice to the French Health Insurance Fund

Once physicians who are self-employed practitioners are registered with the French Medical Council, they must declare their practice to the Primary Health Insurance Fund (*Caisse primaire d’assurance maladie* – CPAM).

#### How to sign up

Signing up with the CPAM can be done online at the [official CPAM website](https://installation-medecin.ameli.fr/installation_medecin/).

#### Supporting documents

Those signing up must provide a complete application file, which includes:

* A copy of a valid piece of identification
* Details of one's professional bank account
* If applicable, supporting documentation allowing the individual to register as a “Secteur 2” physician (physicians registered under this category are allowed to charge higher fees).

For more information, please refer to the [section of the CPAM website devoted to affiliation of physicians](http://www.ameli.fr/professionnels-de-sante/medecins/gerer-votre-activite/votre-installation-en-liberal/vous-vous-installez-en-liberal.php).

## 5°. Process and procedures for recognition of professional qualifications

### a. Applying for registration with the French Medical Association (*Ordre des médecins*)

In order to be able to lawfully practise as a doctor in France, registration is mandatory.

The following are not obliged to register:

* EU and EEA citizens who are based, and who lawfully practise as a doctor, in a Member State or party to the EEA Agreement, when they carry on professional activities in France on a temporary and occasional basis (see above “2°. b. EU and EEA citizens: temporary and occasional practice”)
* Doctors who are in the armed forces medical staff
* Doctors who, having the status of civil servant at central government level or established civil servant with a local authority, do not practise medicine as part of their duties

For more information, please refer to Articles L. 4112-5 to L. 4112-7 of the French Public Health Code.

**Please note**

Registration with the French Medical Association enables the health professionals card (*carte de professionnel de santé* - CPS) to be issued automatically and free-of-charge. The CPS is an electronic professional ID card, protected by a confidential code and containing details of the doctor’s identity, profession and specialist field. Further information can be obtained from [government website of the French Digital Health Agency](http://esante.gouv.fr/services/espace-cps/cartes-professionnelles-de-sante).

#### Competent authority

Registration applications should be sent to the Chair of the French Medical Association’s board for the *département* in which the applicant wishes to set up his/her professional practice.

Applications may be filed directly with the relevant board or sent to it by registered letter with acknowledgment of receipt.

For more information, please refer to Article R. 4112-1 of the Public Health Code.

**Useful tip**

If the professional practice is transferred outside the *département*, the doctor must apply for deregistration with the Medical Association of the *département* where he/she practised and for registration with the Medical Association of the new *département* of residence. For more information, please refer to Article R. 4112-3 of the Public Health Code.

#### Steps

When it receives the application, the *département* board appoints a rapporteur to examine the application and draft a report. 

The board checks the applicant’s qualifications and asks to be sent bulletin no. 2 of his/her criminal record. It also verifies that the applicant:

* complies with requirements in terms of ethics and independence (see above “3°. c. Conditions of good repute”)
* fulfils requirements as regards competency
* does not have an infirmity or medical condition that is incompatible with carrying on the profession (see above “3°. e. Physical aptitude”).

In the event of serious concerns regarding the applicant’s professional competency or the existence of an infirmity or medical condition that is incompatible with carrying on the profession, the *département* board refers the matter to the regional or inter-regional board which commissions an expert appraisal. If the ensuing report finds that there is professional incompetence that makes carrying on the profession dangerous, the *département* board rejects the registration application and specifies the applicant’s obligations in terms of training.

Rejection decisions may only be made once the applicant has been given the opportunity, with at least fifteen days’ notice by registered letter with acknowledgment of receipt, to appear before the board to present his/her case.

During the following week, the board sends its decision to the applicant, the French Medical Association’s National Board and the Regional Health Agency (ARS) by registered letter with acknowledgment of receipt.

The notice mentions the remedies against rejection decisions which must be justified.

For more information, please refer to Articles R. 4112-2 and R. 4112-4 of the Public Health Code.

#### Timeframe

The Chair acknowledges receipt of the full application file within a month. 

The French Medical Association’s *département* board must decide on the registration application within three months of receipt of the full application file.  If no reply is received within this timeframe, the application is deemed to have been refused.

The timeframe is extended to six months for citizens of third countries when investigations outside mainland France are required. The applicant is duly informed of such investigations.

It may also be extended by up to two months by the *département* board when an expert appraisal has been commissioned.

For more information, please refer to Articles L. 4112-3 and R. 4112-1 of the Public Health Code.

#### Supporting documents

The applicant must send a full registration application file comprising:

* Two copies of the standardised questionnaire duly filled in, dated and signed, together with an ID photograph. The questionnaire is available from the *département* boards or as a download from [official website of the French Medical Association’s National Board](https://www.conseil-national.medecin.fr/sites/default/files/questionnaireinscriptionordremedecins.pdf)
* A photocopy of a valid ID document or, where applicable, a nationality certificate issued by a competent authority
* Where applicable, a photocopy of a valid French residence permit (*carte de séjour*) belonging to a member of the family of an EU citizen, of a valid EU long-term residence permit or of a valid residence permit mentioning the status of refugee
* Where applicable, a photocopy of a valid EU Blue Card
* A copy of formal qualifications (and a French translation by a sworn translator, if necessary), together with:
  * the necessary certificate(s)(see above “2°. a. National requirements”) when the applicant is an EU or EEA citizen   
  * if the applicant has a personal authorisation to practise (see above “2°. c. EU and EEA citizens: permanent practice”), a copy of this authorisation
  * when the applicant provides a diploma issued by a foreign country whose validity is recognised in France, a copy of the formal qualifications which need to be held for this recognition to be granted
* for citizens of a foreign country, a criminal record certificate or an equivalent document dating back less than three months and issued by a competent authority of the home country. For EU or EEA citizens who require proof of ethics and good repute to carry on the profession of doctor, this document may be replaced by a certificate, dating back less than three months, from the competent authority of the home country and attesting to the fact that ethical and good repute requirements have been met
* a sworn declaration from the applicant certifying that no proceedings which may lead to sentencing or a sanction that may affect registration with the French Medical Association are ongoing against him/her
* a certificate of deregistration or registration issued by the authority with which the applicant was previously enrolled or registered or, otherwise, a sworn declaration from the applicant that he/she has never been enrolled or registered or, failing that, a certificate of enrolment or registration in an EU or EEA Member State
* any evidence to establish the fact that the applicant has a knowledge of languages necessary for practice of the profession
* a curriculum vitae
* contracts and riders covering practice of the profession and those concerning use of equipment and the premises in which the applicant practises
* if the business is carried on in the form of a professional practice (SEL) or professional partnership (SCP), this company’s articles of association and any riders thereto
* if the applicant is a civil servant or public official, the appointment order
* if the applicant is a university professor – hospital practitioner (PU-PH), university lecturer – hospital practitioner (MCU-PH) or  hospital practitioner (PH), the appointment order as hospital practitioner and, where applicable, the appointment decree or order as university professor or university lecturer

For additional information, we advise you to refer to the [official website of the French Medical Council](https://www.conseil-national.medecin.fr/l-inscription-au-tableau-1233).

For more information, please refer to Articles L. 4113-9 and R. 4112-1 of the Public Health Code.

#### Remedies

The applicant or the French Medical Council may challenge the registration acceptance or rejection decision within 30 days of its notification or the implicit rejection decision. Appeals are brought before the regional board with territorial jurisdiction.

The *département* board has two months as from receipt of the challenge to make a decision. Should it fail to do so, the challenge is deemed to have been rejected.

The *département* board’s decision may also be appealed against within 30 days to the French Medical Council. Its decision may be challenged in turn before the French Supreme Administrative Court (*Conseil d’État*).

For more information, please refer to Articles L. 4112-4 and R. 4112-5 of the Public Health Code.

#### Cost

Registration with the French Medical Association is free-of-charge but it carries the requirement to pay the mandatory membership subscription (*cotisation ordinale obligatoire*) whose amount is set on a yearly basis. The subscription must be paid during the first quarter of the then-current calendar year. Payment may be made online on the [official website of the French Medical Council](https://paiements.ordre.medecin.fr/). For information, the subscription cost was €333 in 2017.

For more information, please refer to Article L. 4122-2 of the Public Health Code.

### b. Filing a prior declaration for EU or EEA citizens carrying on a temporary or occasional activity (freedom to provide services, FPS).

Any EU or EEA citizen who is based and lawfully carries on the profession of doctor in one of these countries may practise in France on a temporary or occasional basis if he/she files a prior declaration (see above “2°. b. EU and EEA citizens: temporary and occasional practice”).

A prior declaration must be submitted every year.

**Please note**

The applicant must report any changes to his/her circumstances in the same manner.

For more information, please refer to Articles L. 4112-7 and R. 4112-9-2 of the Public Health Code.

#### Competent authority

The declaration must be filed with the French Medical Council before any services are provided.

For more information, please refer to Article R. 4112-9 of the Public Health Code.

#### Conditions for the declaration and receipt

The declaration may be sent by mail or filed online on the [official website of the French Medical Council](https://sve.ordre.medecin.fr/loc_fr/default/?__CSRFTOKEN__=3924ffb9-b716-467f-bd6c-27f3fffb03fe).

Once the French Medical Council has received the declaration and all the required supporting documents, it sends the service provider a receipt containing his/her registration number and details of the relevant discipline.

**Please note**

The service provider informs the competent French health insurance organisation that he/she is about to provide a service beforehand by sending a copy of this receipt or by any other means.

For more information, please refer to Articles R. 4112-9-2 and R. 4112-11 of the Public Health Code.

#### Timeframe

Within one month of receipt of the declaration, the French Medical Council informs the applicant:

* whether or not he/she can begin to provide the services
* when an examination of the professional qualifications reveals a significant disparity with the training required in France, that he/she must prove that he/she possesses the missing knowledge and skills by taking an aptitude test. If he/she passes this test, he/she will be informed within a month that he/she may begin to provide the services
* when it is evident from the file that there is a problem requiring further information, of the reasons for the delay in processing the declaration. In such cases, he/she has one month to obtain the information requested. After re-examining the file, the French Medical Council reverts to the service provider within two months of receipt of this information :
  * whether or not he/she can begin to provide the services
  * when an examination of the professional qualifications of the service provider reveals a significant disparity with the training required in France, that he/she must prove that he/she possesses the missing knowledge and skills by taking an aptitude test. 

In the latter case, if he/she passes this test, he/she will be informed within a month that he/she may begin to provide the services. Otherwise, he/she is advised that he/she may not begin to provide the services. 

Should the French Medical Council fail to reply within these timeframes, the services may be provided.

For more information, please refer to Article R. 4112-9-1 of the Public Health Code.

#### Supporting documents

The prior declaration must be accompanied by another declaration relating to a knowledge of languages necessary for providing the services as well as the following supporting documents:

* the prior service declaration form, the template for which is available in the Appendix to [the Order of 20 January 2010 on the prior service declaration to practise the professions of doctor, dental surgeon and midwife](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021776754&dateTexte=20170209), filled in, dated and signed. The information requested covers:
  * the applicant’s identity
  * the relevant profession
  * professional liability insurance
  * for renewals, the service periods and professional activities carried on
* a copy of a valid ID document or a document providing proof of the applicant’s nationality
* a copy of the formal qualification(s) and, where applicable, the translation by a sworn translator
* a certificate from the competent authority of the EU or EEA Member State in which he/she is established, attesting to the fact that the applicant is lawfully established in that country and that he/she is not subject to any ban on practising together, where applicable, with a French translation by a sworn translator

**Please note**

Checking language skills must be proportional to the activity to be carried on and take place once the professional qualifications have been recognised.

For more information, please refer to Article L. 4112-7 of the Public Health Code; Order of 20 January 2010 on the prior service declaration to practise the professions of doctor, dental surgeon and midwife.

#### Coût

Free-of-charge.

### c. Applying for a personal authorisation to practise, if necessary

#### For EU or EEA citizens

EU or EEA citizens holding a formal qualification may apply for a personal authorisation to practise:

* issued by one of these countries in which recognition is not automatic (see above “2°. c. EU and EEA citizens: permanent practice”)
* issued by a third country but recognised by an EU or EEA Member State, provided they provide proof of having carried on the profession of doctor in the specialist field, full-time and for three years, in said Member State

A practice authorisation board (CAE) assesses the applicant’s training and professional experience.

This board may suggest compensatory measures:

* when the training is at least one year less than for the French state diploma (DE), when it covers significantly different subjects, or when one or more constituent elements of the professional activity, which can only be carried on with the aforementioned qualification, does/do not exist in the corresponding profession in the home Member State or was/were not taught in that country
* and when the applicant’s training and experience are not adequate to compensate for these differences

Depending on the level of qualification required in France and that held by the individual in question, the competent authority may:

* offer the applicant a choice between completing an adaptation period and taking an aptitude test
* make an adaptation period or aptitude test mandatory
* make an adaptation period and aptitude test mandatory

The purpose of the **aptitude test** is to check, through written and oral examinations, the applicant’s aptitude to carry on the profession of doctor in the relevant specialist field. It concerns subjects that are not covered by the applicant’s formal qualifications or professional experience.

The purpose of the **adaptation period** is to enable applicants to acquire the skills required for practice of the profession of doctor. It is carried out under the supervision of a doctor and may be supplemented by optional theoretical training. The adaptation period may last a maximum of three years and may be completed on a part-time basis.

For more information, please refer to Articles L. 4111-2 II, L. 4131-1-1, R. 4111-17 to R. 4111-20 and R. 4131-29 of the Public Health Code.

#### For third country citizens

These persons may apply for a personal authorisation to practise, provided they can prove that they are sufficiently fluent in French and if they hold a formal qualification:

* issued by an EU or EEA Member State with the experience being certified by any means
* issued by a third country allowing the profession of doctor to be carried on in the country in which the qualification was obtained
  * and if they pass anonymous tests of basic and practical knowledge. For further information on these tests, refer to the [official website of the National Management Centre (CNG)](http://www.cng.sante.fr/Epreuves-de-verification-des.html),
  * and if they can present proof of having worked for three years in a department or organisation authorised to train interns 

**Please note:** Doctors holding a specialist post-graduate diploma obtained as part of a foreign internship are deemed to have passed the knowledge verification tests.

For more information, please refer to Articles L. 4111-2 (I and I bis), D. 4111-1, D. 4111-6 and R. 4111-16-2 of the Public Health Code.

#### Competent authority

Two copies of applications are sent by registered letter with acknowledgment of receipt to the unit responsible for the practice authorisation boards of the National Management Centre.

Following an opinion from the practice authorisation board, the authorisation to practise is issued by the Minister for Health.

For more information, please refer to Articles R. 4111-14 and R. 4131-29 of the Public Health Code; Order of 25 February 2010 listing the documents required for the file to be sent to the competent practice authorisation boards for assessment of applications submitted for carrying on the professions of doctor, dental surgeon, midwife and pharmacist in France.

#### Timeframe

The National Management Centre acknowledges receipt of the application within one month.

Failure to reply during a certain period as from receipt of the file constitutes a rejection of the application. This timeframe is:

* four months for applications filed by EU or EEA citizens holding qualifications issued in one of these countries
* six months for applications submitted by citizens of third countries holding qualifications issued by an EU or EEA Member State
* one year for other applications. This time-frame may be extended by two months, by a decision from the ministerial authority that is notified at least one month prior to its expiry date, in the event of serious problems in assessing the applicant’s professional experience.

For more information, please refer to Articles R. 4111-2, R. 4111-14 and R. 4131-29 of the Public Health Code.

#### Supporting documents

The application file must contain:

* an application form for authorisation to practise the profession, the template for which is available in Appendix 1 to the Order of 25 February 2010, filled-in, dated and signed, and showing, where applicable, the specialist field for which the applicant is filing his/her application
* a photocopy of a valid ID document
* a copy of the formal qualification authorising the profession to be carried on in the country in which it was obtained and, where applicable, a copy of the qualification in the specialist field
* where applicable, a copy of additional qualifications
* any relevant documents attesting to in-service training, experience and skills acquired during professional practice in an EU or EEA Member State, or in a third country (certificates covering duties, activity review, operations report, etc.)
* in respect of the responsibilities held in a country other than France, a declaration from this country’s competent authority, dating back less than a year, certifying that no sanctions have been handed down against the applicant

Depending on the applicant’s circumstances, additional supporting documents may be required. For further information, refer to the [official website of the National Management Centre](http://www.cng.sante.fr/Liste-des-pieces-a-fournir.html).

**Useful tip**

The supporting documents must be in French or be translated by a sworn translator.

For more information, please refer to the Order of 25 February 2010 listing the documents required for the file to be sent to the competent practice authorisation boards for assessment of applications submitted for carrying on the professions of doctor, dental surgeon, midwife and pharmacist in France; government instruction of 17 November 2014 no.  DGOS/RH1/RH2/RH4/2014/318.

### c. Remedies

#### French support centre

The ENIC-NARIC is the French centre for information on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided by the national administration in each EU and EEA country. It aims to find solutions to problems between an EU citizen and the government of another of these countries. SOLVIT is heavily involved in the recognition of professional qualifications.

##### Conditions

Applicants may use SOLVIT’s services if they are able to prove:

* that the public authorities of an EU Member State have breached their EU rights as a citizen or business of another EU Member State
* that they have not yet taken the case to court (administrative appeals do not count)

##### Procedure

The citizen must complete an [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once the form has been submitted, SOLVIT contacts him/her within one week to ask for further information, if necessary, and to check whether the problem falls within its remit.

##### Supporting documents

To refer a problem to SOLVIT, the citizen must provide:

* his/her full contact details
* a detailed description of the problem
* all relevant evidence(e.g. correspondence and decisions received from the administrative authority in question)

##### Timeframe

SOLVIT aims to find solutions within 10 weeks – starting on the day the case is taken on by the SOLVIT centre in the country where the problem occurred.

##### Cost

Free-of-charge.

##### End of the procedure

At the end of these 10 weeks, SOLVIT puts forward a solution:

* If the solution resolves the problem concerning the application of EU law, it is accepted and the case is closed
* If the problem is not settled, the case is closed as unresolved and referred to the European Commission

##### Further information

SOLVIT in France: Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris, [official website of the Secretariat General for European Affairs](https://sgae.gouv.fr/sites/SGAE/accueil.html).