﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP203" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Orthoptiste" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="orthoptiste" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/orthoptiste.html" -->
<!-- var(last-update)="2020-04-15 17:21:53" -->
<!-- var(url-name)="orthoptiste" -->
<!-- var(translation)="None" -->

# Orthoptiste

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:53<!-- end-var -->

## 1°. Définition de l'activité

L'orthoptiste est un professionnel de santé spécialisé dans la rééducation visuelle.

À ce titre, il a vocation à dépister les troubles visuels et à assurer les actes de rééducation et de réadaptation de la vue de ses patients, à tous les âges, sur prescription médicale.

Travaillant en étroite collaboration avec les ophtalmologues, l'orthoptiste met en place des protocoles organisationnels lui permettant d'assurer la prise en charge des patients. Cela peut concerner la préparation de l'examen médical par l'ophtalmologue ou encore le suivi par l'orthoptiste d'un patient dont la pathologie visuelle est diagnostiquée.

En l'absence de médecin, il pourra également être habilité à accomplir les premiers actes de soins nécessaires en orthoptie.

*Pour aller plus loin* : article L. 4342-1 du Code de la santé publique ; décret n° 2016-1670 du 5 décembre 2016 relatif à la définition des actes d’orthoptie et aux modalités d’exercice de la profession d’orthoptiste.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'exercice de la profession d'orthoptiste est réservé aux titulaires du certificat de capacité d'orthoptiste.

*Pour aller plus loin* : article L. 4342-3 du Code de la santé publique.

#### Formation

La formation menant au certificat de capacité d'orthoptie est accessible aux titulaires :

- du baccalauréat ;
- d'un diplôme d'accès aux études universitaires ;
- d'une qualification ou d'une expérience jugées suffisantes.

L'accès à la formation se fait après avoir réussi un concours d'admission (avec numerus clausus) dans l'une des [quatorze écoles](https://supexam.fr/prepa-paramedical/orthoptiste/ecoles/) qui dispensent la formation d'orthoptie en France, dont la durée est de trois années.

**À savoir**

L'étudiant devra suivre plusieurs stages au cours de sa formation qu'il consignera dans un carnet.

À chaque fin d'année, l'étudiant doit passer avec succès des examens écrits et oraux, en obtenant la moyenne générale des points à l'ensemble des épreuves. La dernière année, celui-ci devra réussir des épreuve écrites et orales, une démonstration d'un appareil d'examen ou de traitement orthoptique, et rendre un mémoire de fin d'études.

*Pour aller plus loin* : arrêté du 20 octobre 2014 relatif aux études en vue du certificat de capacité d'orthoptiste.

#### Coûts associés à la qualification

La formation est payante. Pour plus d'informations, il est conseillé de se rapprocher des établissements la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’UE ou de l’EEE exerçant légalement l’activité d'orthoptiste dans l’un de ces États peut faire usage de son titre professionnel en France, à titre temporaire et occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet de la région dans laquelle il souhaite faire la prestation (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins un an au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : article L. 4342-5 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité d'orthoptiste dans cet État peut exercer la même activité en France de manière permanente s’il :

- est titulaire d'un titre de formation délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession, à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation ni l'exercice de la profession ;
- est titulaire d’un diplôme, titre ou certificat acquis dans un État tiers mais reconnu et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité d'orthoptiste dans l’État qui a admis l’équivalence.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une autorisation individuelle d'exercice auprès du préfet de la région dans laquelle il souhaite exercer sa profession (cf. infra « 5°. b. Obtenir une autorisation individuelle d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

*Pour aller plus loin* : article L. 4342-4 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Bien que non codifiées, des règles de déontologie incombent à l'intéressé qui souhaite exercer la profession d'orthoptiste, et notamment :

- d'offrir la meilleure prise en charge possible à ses patients ;
- d'entretenir et développer leurs connaissances scientifiques du domaine ;
- de se comporter avec loyauté et respect vis-à-vis des personnes qu'ils contactent dans leur exercice professionnel.

En outre, l'orthoptiste est tenu de respecter le secret professionnel lié à la pratique de son activité. Le non-respect de cette obligation est puni d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : article L. 4344-2 du Code de la santé publique.

## 4°. Législation sociale et assurances

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, l'orthoptiste exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans cette hypothèse, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

### b. Obligation d'affiliation à la caisse autonome de retraite et de prévoyance des auxiliaires médicaux (CARPIMKO)

Tout orthoptiste exerçant sous la forme libérale, même en tant que remplaçant, doit adhérer à la CARPIMKO, dans le mois qui suit le début de son activité.

L'intéressé devra transmettre le [formulaire](https://www.carpimko.com/document/pdf/affiliation_declaration.pdf) d'affiliation, rempli, daté et signé, et joindre à sa demande une photocopie de son titre de formation ainsi que son numéro d'enregistrement Adeli (cf. infra « 5°. c. Enregistrement auprès du répertoire Adeli »).

### c. Obligation de déclaration auprès de l'Assurance maladie

L'orthoptiste exerçant sous la forme libérale doit déclarer son activité auprès de la Caisse primaire d'assurance maladie (CPAM).

L'inscription peut être réalisée en ligne sur le [site officiel](https://www.ameli.fr/orthoptiste) de l'Assurance maladie.

L'intéressé devra joindre à sa demande :

- une copie d'une pièce d'identité en cours de validité ;
- un relevé d'identité bancaire professionnel ;
- le cas échéant, le ou les titres justificatifs permettant l'accès au secteur 2.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet de région est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de la profession dans l’État d'obtention ;
- une attestation datant de moins de trois mois de l'autorité compétente de l’État de l'UE ou de l'EEE certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- toutes pièces justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation ni l'accès à la profession demandée ou son exercice ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État de l'UE ou de l'EEE autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l’État ayant reconnu ce titre,
  - toutes pièces justifiant que le ressortissant a exercé la profession dans cet État pendant trois ans ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée ;
- une attestation de responsabilité civile professionnelle.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

À réception du dossier, le préfet de région disposera d'un délai d'un mois pour se prononcer sur la demande et informera le ressortissant :

- qu'il peut débuter la prestation. Dès lors, le préfet enregistrera le demandeur au répertoire Adeli ;
- qu'il sera soumis à une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France ;
- qu'il ne pourra pas débuter la prestation ;
- de toute difficulté pouvant retarder sa décision. Dans ce dernier cas, le préfet pourra rendre sa décision dans les deux mois suivant la résolution de cette difficulté, et au plus tard dans les trois mois de sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaudra acceptation de la demande de déclaration.

**À noter**

La déclaration est renouvelable tous les ans ou à chaque changement de situation du demandeur.

*Pour aller plus loin* : articles R. 4342-13, et R. 4331-12 à R. 4331-15 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### b. Obtenir une autorisation individuelle d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

#### Autorité compétente

L'autorisation d'exercice est délivrée par le préfet de région, après avis de la commission des orthoptistes.

#### Pièces justificatives

La demande d'autorisation s'effectue par le dépôt d'un dossier comprenant l'ensemble des documents suivants :

- le [formulaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021906912&fastPos=2&fastReqId=1698185741&categorieLien=cid&oldAction=rechTexte) de demande d'autorisation d'exercice ;
- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation ;
- le cas échéant, une copie des diplômes complémentaires ;
- toute pièce justifiant des formations continues, des expériences et des compétences acquises dans l’État de l'UE ou de l'EEE ;
- une déclaration de l'autorité compétente de l'État de l'UE ou de l'EEE justifiant l'absence de sanction à l'encontre du ressortissant ;
- une copie des attestations des autorités spécifiant le niveau de formation, le détail et le volume horaire des enseignements suivis ainsi que le contenu et la durée des stages validés ;
- tout document justifiant que le ressortissant a exercé l'activité d'orthoptiste pendant un an au cours des dix dernières années dans un État de l'UE ou de l'EEE lorsque ni l'accès ni l'exercice ne sont réglementés dans cet État.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet accuse réception du dossier dans le délai d'un mois et se prononcera après avoir eu l'avis de la commission des orthoptistes. Cette dernière est chargée d'examiner les connaissances et les compétences du ressortissant acquises lors de sa formation ou au cours de son expérience professionnelle. Elle pourra soumettre le ressortissant à une mesure de compensation.

Le silence gardé du préfet de région dans un délai de quatre mois vaut décision de rejet de la demande d'autorisation.

*Pour aller plus loin* : articles R. 4342-10 à R. 4342-12 du Code de la santé publique ; arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste et opticien-lunetier.

#### Bon à savoir : mesures de compensation

Si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession d'orthoptiste et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation qui peut être un stage d’adaptation ou une épreuve d’aptitude.

L'épreuve d'aptitude prend la forme d'un examen écrit ou oral noté sur 20. Sa validation est prononcée lorsque le ressortissant a obtenu une note moyenne égale ou supérieure à dix sur vingt, et ce, sans note inférieure à huit sur vingt. En cas de réussite à l'épreuve, le ressortissant sera autorisé à utiliser le titre d'orthoptiste.

Le stage d'adaptation se fait dans un établissement de santé public ou privé, ou chez un professionnel, et ne doit pas durer plus de trois ans. Il comprend également une formation théorique qui sera validée par le responsable du stage.

*Pour aller plus loin* : arrêté du 30 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste, opticien-lunetier par des ressortissants des États membres de l'Union européenne ou parties à l'accord sur l'Espace économique européen.

### c. Enregistrement auprès du répertoire Adeli

Le ressortissant souhaitant exercer la profession d'orthoptiste en France est tenu de faire enregistrer son autorisation d'exercer sur le répertoire Adeli (« Automatisation Des Listes »).

#### Autorité compétente

L’enregistrement au répertoire Adeli se fait auprès de l’agence régionale de santé (ARS) du lieu d’exercice.

#### Délai

La demande d’enregistrement est présentée dans le mois suivant la prise de fonction du ressortissant, quel que soit le mode d’exercice (libéral, salarié, mixte).

#### Pièces justificatives

À l'appui de sa demande d'enregistrement, l'orthoptiste doit fournir un dossier comportant :

- le diplôme original ou titre attestant la formation d'orthoptiste délivré par l'État de l'UE ou de l'EEE (traduit en français par un traducteur agréé, le cas échéant) ;
- une pièce d’identité ;
- le formulaire Cerfa 10906-06 complété, daté et signé.

#### Issue de la procédure

Le numéro Adeli du ressortissant sera directement mentionné sur le récépissé du dossier, délivré par l'ARS.

#### Coût

Gratuit.

*Pour aller plus loin* : article L. 4342-2 du Code de la santé publique.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L'intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).