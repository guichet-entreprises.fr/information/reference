﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP200" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Orthopédiste-orthésiste" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="orthopediste-orthesiste" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/orthopediste-orthesiste.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="orthopediste-orthesiste" -->
<!-- var(translation)="None" -->

# Orthopédiste-orthésiste

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L'orthopédiste ou orthésiste est un professionnel, auxiliaire médical, dont l'activité consiste à réaliser, sur prescription médicale, un appareillage dans le but de traiter l'ensemble des affections liées à l'appareil locomoteur (musculo-squelettique) des personnes handicapées.

À ce titre, le professionnel procède, à la prise de mesure, la conception et la fabrication de l'outil d'appareillage ainsi qu'à la délivrance, l'adaptation et la réparation du produit.

Les dispositifs médicaux d'appareillages concernés sont les suivants :

- les ceintures médico-chirurgicales de soutien ou de maintien ;
- les corsets orthopédiques d'immobilisation du rachis ;
- les bandages herniaires ;
- les orthèses élastiques de contention ;
- les vêtements compressifs sur mesure.

*Pour aller plus loin* : articles L. 4364-1 et D. 4364-6 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité d'orthopédiste - orthésiste, le professionnel doit :

- être qualifié professionnellement ;
- procéder à l'enregistrement de son diplôme (cf. infra « 5°. a. Demande d'enregistrement de son diplôme »).

#### Formation

Pour être reconnu comme étant qualifié professionnellement, le professionnel doit être titulaire soit :

- d'un titre professionnel d'orthopédiste - orthésiste ;
- du brevet de technicien supérieur (BTS) de prothésiste - orthésiste (bac +2) ;
- pour les pharmaciens, d'un diplôme universitaire ou interuniversitaire en orthopédie ;
- pour les professionnels non-pharmaciens et non-orthoprothésistes à compter du 25 février 2007, d'un titre ou certificat reconnu par décision du ministre chargé de la santé et validant une formation en « Orthopédiste-orthésiste » enregistrée au Répertoire national des certifications professionnelles ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ;
- pour les professionnels non-pharmaciens et non-orthoprothésistes avant le 25 février 2007, l'un des diplômes suivants :
  - un certificat de technicien bandagiste orthopédiste-orthésiste délivré par l'école de Marseille, les chambres des métiers d'Alsace et de Moselle, ou le centre de formation Ecotev à Vienne, 
  - un certificat de technicien supérieur en orthèses délivré par la chambre des métiers de Paris et la Chambre syndicale nationale des podo-orthésistes,
  - un titre d'enseignement en orthèses délivré par l'école d'orthopédie de Poissy entre 1996 et 2000 ;
- d'une reconnaissance de ses qualifications professionnelle délivrée par les organismes d'assurance maladie et le ministère chargé des anciens combattants et victimes de guerre.

*Pour aller plus loin* : article D. 4364-7 du Code de la santé publique et article 7 de l'arrêté du 1er février 2011 relatif aux professions de prothésiste, orthésiste pour l'appareillage des personnes handicapées.

#### Coûts associés à la qualification

Le coût de la formation menant à la profession d'orthopédiste - orthésiste est payante et son coût varie selon le cursus envisagé. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant l'activité d'orthopédiste - orthésiste, peut, exercer à titre temporaire et occasionnel, la même activité en France.

Lorsque ni l'accès à l'activité, ni son exercice ne sont réglementés dans cet État membre, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an au cours des dix dernières années dans un ou plusieurs État(s) membres ;

Préalablement à sa prestation de services, le professionnel est tenu d'effectuer une déclaration auprès du préfet de région (cf. infra « 5°. b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS) »).

En outre, le professionnel doit justifier être titulaire des connaissances linguistiques nécessaires à l'exercice de son activité.

*Pour aller plus loin* : articles L. 4364-6 et L. 4364-7 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Etablissement (LE))

Tout ressortissant de l'UE ou de l'EEE, légalement établi et exerçant l'activité d'orthopédiste - orthésiste peut exercer, à titre permanent, la même activité, en France.

Pour cela, le professionnel doit être titulaire soit :

- d'un titre de formation délivré par un État membre qui réglemente l'exercice et l'accès à cette profession ;
- lorsque l’État membre ne réglemente ni l'accès, ni l'exercice à la profession, un titre de formation attestant qu'il a suivi une préparation en vue d'exercer l'activité d'orthopédiste - orthésiste ainsi qu'une attestation certifiant qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- un titre de formation délivré par un État tiers mais reconnu par un État membre autre que la France, permettant d'exercer cette activité, ainsi qu'un justificatif d'exercice de cette activité pendant au moins trois ans.

En cas de différences substantielles entre la formation reçue par le professionnel et celle requise pour exercer l'activité d'orthopédiste - orthésiste en France, le préfet peut décider de soumettre le ressortissant à une mesure de compensation (cf. infra « 5°. c. Bon à savoir : mesures de compensation »).

En outre, le professionnel doit justifier être titulaire des connaissances linguistiques nécessaires à l'exercice de son activité.

*Pour aller plus loin* : article L. 4364-5 du Code de la santé publique.

## 3°. Règles professionnelles

### Respect du secret professionnel

L'orthopédiste, en tant que professionnel de la santé, est tenu au respect du secret professionnel.

*Pour aller plus loin* : article D. 4364-12 du Code de la santé publique.

### Règles de bonne pratique

En outre, le professionnel est tenu au respect des règles de bonne pratique qui incombent à sa profession.

À ce titre, il doit notamment :

- exercer dans des locaux équipés et adaptés aux besoins de l'exercice de son activité, et lui permettre de recevoir ses patients dans de bonnes conditions d'isolement (phonique et visuel) ;
- tenir et mettre à jour un dossier pour chacun de ses patients comprenant l'ensemble des informations le concernant et un descriptif des appareils nécessaires et des éventuelles interventions effectuées.

*Pour aller plus loin* : article D. 4364-13 du Code de la santé publique; articles 13 à 26 de l'arrêté du 1er février 2011 précité.

## 4°. Assurances

L'orthopédiste - orthésiste en tant que professionnel de la santé est tenu de souscrire une assurance de responsabilité civile professionnelle pour les risques encourus au cours de l'exercice de son activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Procéder à l'enregistrement de son diplôme

#### Autorité compétente

Le demandeur doit adresser une demande à l'agence régionale de santé (ARS) en vue de procéder à son enregistrement.

#### Pièces justificatives

Il doit, pour cela, transmettre les informations suivantes, réputées validées et certifiées par l'organisme ayant délivré le diplôme ou le titre de formation :

- l'état civil du titulaire du diplôme et toutes les données permettant d'identifier le demandeur ;
- les nom et adresse de l'établissement ayant délivré la formation ;
- l'intitulé de la formation.

#### Issue de la procédure

Après vérifications des pièces, l'ARS procède à l'enregistrement du diplôme.

**À noter**

L'inscription n'est possible que pour un seul département, toutefois si le professionnel souhaite exercer dans plusieurs départements, il sera inscrit sur la liste du département dans lequel se trouve le lieu principal de son activité.

#### Coût

Gratuit.

*Pour aller plus loin* : article D. 4364-18 du Code de la santé publique ; articles D. 4333-1 à D. 4333-6-1 du Code de la santé publique.

### b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande au préfet de la région où il souhaite réaliser sa prestation de services.

#### Pièces justificatives

Sa demande doit comporter :

- le formulaire de déclaration fixé à l'annexe de l'arrêté du 19 février 2010 complété et signé ;
- l'ensemble des pièces justificatives mentionnées au sein de ce formulaire.

#### Délai et procédure

Le préfet de région, informe le demandeur dans un délai d'un mois suivant la réception de son dossier, soit :

- qu'il peut débuter sa prestation de services ;
- qu'il ne peut pas débuter sa prestation de services ;
- qu'il doit faire l'objet d'une vérification préalable de ses qualifications professionnelles. Le cas échéant, lorsqu'il existe une différence substantielle entre la formation reçue par le ressortissant et celle exigée 

**À noter**

Pour exercer cette activité en France, le préfet peut décider de le soumettre à mesure de compensation (cf. infra « 5°. c. Bon à savoir : mesures de compensation »).

En l'absence de réponse de la part du préfet dans un délai d'un mois, le ressortissant peut débuter sa prestation de services.

**À noter**

Le ressortissant sera inscrit par le préfet sur une liste particulière qui lui fournira le cas échéant, un récépissé de sa déclaration mentionnant son numéro d'enregistrement.

Cette déclaration doit faire l'objet d'un renouvellement annuel dans les mêmes conditions.

*Pour aller plus loin* : articles R. 4364-11-3, R. 4331-12 à R. 4331-15 et D. 4364-11-9 du Code de la santé publique; arrêté du 19 février 2010 relatif à la déclaration préalable de prestation de services pour l'exercice des professions d'orthoprothésiste, podo-orthésiste, oculariste, épithésiste, orthopédiste-orthésiste.

### c. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant doit adresser sa demande en double exemplaire, par lettre recommandée avec avis de réception, à la commission des orthésistes.

#### Pièces justificatives

La demande doit comporter le [formulaire de demande d'autorisation](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DB4E58672EA257FEDF6B78008E6B179E.tplgfr24s_1?idArticle=LEGIARTI000021777548&cidTexte=LEGITEXT000021777545&dateTexte=20180412) complété et signé.

En outre, il doit fournir les pièces justificatives suivantes, le cas échéant, assorties de leur traduction certifiée en français :

- une photocopie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation permettant l'exercice de l'activité d'orthopédiste - orthésiste dans cet État membre et le cas échéant, de ses diplômes complémentaires ;
- tout document justifiant d'une formation continue ou d'une expérience professionnelle du demandeur ;
- une attestation de l’État membre certifiant que le demandeur ne fait l'objet d'aucune sanction ;
- une copie des attestations délivrées par les autorités mentionnant le niveau de formation du demandeur ainsi que le détail de la formation (volume horaire, enseignements suivis) ;
- lorsque l’État ne réglemente ni l'accès à la profession, ni son exercice, la preuve qu'il a exercé cette activité pendant au moins deux ans au cours des dix dernières années ;
- lorsque le ressortissant a acquis son titre de formation dans un État tiers mais reconnu par un État membre, la reconnaissance de son titre de formation.

#### Délai et procédure

Le préfet de région accuse réception de la demande dans un délai d'un mois. En l'absence de réponse dans un délai de quatre mois à compter de la réception de la demande, celle-ci est réputée refusée.

*Pour aller plus loin* : articles R. 4364-11 à R. 4364-11-2 du Code de la santé publique; arrêté du 20 janvier 2010 fixant la composition du dossier à fournir à la commission d'autorisation d'exercice compétente pour l'examen des demandes présentées en vue de l'exercice en France des professions d'orthoprothésiste, podo-orthésiste, oculariste, épithésiste, orthopédiste-orthésiste.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel, et celle requise pour exercer l'activité d'othopédiste - orthésiste en France, l'autorité compétente peut décider de le soumettre au choix, soit à un stage adaptation, soit à une épreuve d'aptitude.

L'épreuve d'aptitude doit être effectuée dans un délai de six mois à compter de la notification au demandeur. Elle a pour objet de vérifier les connaissances et aptitudes du ressortissant en vue d'exercer en France. Le stage d'adaptation effectué auprès d'un professionnel qualifié, a pour objectif de permettre au ressortissant d'acquérir les connaissances nécessaires à l'exercice de cette activité.

*Pour aller plus loin* : articles R. 4311-35 et R. 4311-36 du Code de la santé publique; arrêté du 24 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions d'orthoprothésiste, podo-orthésiste, oculariste, épithésiste, orthopédiste-orthésiste par des ressortissants des Etats membres de l'Union européenne ou partie à l'accord sur l'Espace économique européen.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).