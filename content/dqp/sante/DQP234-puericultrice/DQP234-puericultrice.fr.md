﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP234" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Puéricultrice" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="puericultrice" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/puericultrice.html" -->
<!-- var(last-update)="2020-04-15 17:22:06" -->
<!-- var(url-name)="puericultrice" -->
<!-- var(translation)="None" -->

# Puéricultrice

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:06<!-- end-var -->

## 1°. Définition de l'activité

La puéricultrice (ou infirmière puéricultrice) est une infirmière professionnelle, spécialiste des soins de la petite enfance.

L'infirmière puéricultrice a pour missions, notamment :

- d'assurer le suivi et de prodiguer les soins nécessaires aux nouveau-nés, nourrissons et aux enfants ;
- de surveiller le régime alimentaire des nouveau-nés et des enfants placés en incubateur ou sous photothérapie.

La puéricultrice peut exercer son activité au sein de maternités, services pédiatriques des hôpitaux ou au sein de centres de protection maternelle et infantile.

*Pour aller plus loin* : article R. 4311-13 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer la profession de puéricultrice, le professionnel doit être titulaire des diplômes suivants :

- un diplôme d'infirmier/ère ou de sage-femme ;
- un diplôme d’État de puéricultrice.

*Pour aller plus loin* : article D. 4311-49 du Code de la santé publique.

#### Formation

Pour plus d'informations sur les formations menant aux diplômes d'infirmier/ère et de sage-femme, il est conseillé de se reporter aux fiches « Infirmier/ère » et « Sage-femme ».

##### Diplôme d’État de puéricultrice

Le diplôme d’État de puéricultrice est délivré au candidat :

- titulaire du diplôme d’État d'infirmier/ère ou de sage-femme ;
- ayant réussi le concours d'admission au sein d'une école préparant au diplôme ;
- ayant subi les vaccinations obligatoires (cf. article L. 3111-1 et suivants du Code de la santé publique).

La candidat doit, pour se présenter au concours d'admission, adresser un dossier comprenant :

- une demande manuscrite d'inscription ;
- une fiche d'état civil ;
- un curriculum vitae ;
- la copie de l'ensemble de ses diplômes ou titres de formation ;
- un justificatif de versement des droits d'inscription au concours d'admission.

Le concours d'admission se compose de deux épreuves écrites d'admissibilité et d'une épreuve orale d'admission.

La formation, d'une durée de douze mois, se compose :

- d'enseignements théoriques d'une durée de 650 heures ;
- d'enseignements cliniques d'une durée de 710 heures ;
- de travaux dirigés et d'évaluation d'une durée de 140 heures ;
- de stages réalisés au sein de centre hospitaliers et d'établissements conventionnés par l'école.

Le diplôme d’État de puéricultrice est délivré au candidat ayant réussi les évaluations de connaissances et des capacités professionnelles au cours de sa formation.

*Pour aller plus loin* : arrêté du 12 décembre 1990 relatif à la scolarité, au diplôme d’État de puéricultrice et au fonctionnement des écoles.

#### Coûts associés à la qualification

La formation menant à l'obtention du diplôme de puéricultrice est payante. Pour plus d'informations, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union européenne (UE) ou de l’Espace économique européen (EEE), établi et exerçant légalement les activités d’infirmière puéricultrice dans un de ces États, peut exercer la même activité en France de manière temporaire et occasionnelle à la condition d’avoir adressé au préfet de département du lieu d’exécution de la prestation une déclaration préalable d’activité.

Si le titre de formation n’est pas reconnu en France, les qualifications du ressortissant d’un État de l’UE ou de l’EEE sont vérifiées avant sa première prestation. En cas de différences substantielles entre les qualifications du prestataire et la formation exigée en France, le ressortissant doit apporter la preuve qu’il a acquis les connaissances et compétences manquantes, notamment en se soumettant à des mesures de compensation (cf. infra « Bon à savoir : mesures de compensation »).

Dans tous les cas, le ressortissant européen désireux d’exercer en France de manière temporaire ou occasionnelle doit posséder les connaissances linguistiques nécessaires à l’exercice de l’activité et maîtriser les systèmes de poids et mesures utilisés en France.

*Pour aller plus loin* : article L. 4311-22 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Les ressortissants d’un État de l’UE ou de l’EEE souhaitant exercer à titre permanent en France relèvent de deux régimes distincts. Cependant, dans les deux cas, le ressortissant doit posséder les connaissances linguistiques nécessaires à l’exercice de l’activité en France et maîtriser les systèmes de poids et de mesures utilisés en France.

#### Le régime de reconnaissance automatique du diplôme

Tous les titulaires d’un titre d’infirmier/ère puéricultrice délivré par l’un des États de l’UE ou de l’EEE figurant à l’arrêté du 10 juin 2004 peuvent bénéficier d’une reconnaissance automatique de leur diplôme. De plus, les ressortissants qui ne sont pas titulaires de l'un des titres listés à l’annexe I du décret du 10 juin 2004 peuvent bénéficier d’une reconnaissance automatique à la condition qu’ils disposent de titres de formation dont le respect est certifié par une attestation de l’État d’obtention. Pour connaître la liste exhaustive de ces titres, il est conseillé de se référer à l’arrêté du 10 juin 2004.

Dans tous les cas, les personnes concernées n’ont pas à demander d’autorisation d’exercer. En revanche, elles doivent s’inscrire au conseil départemental de l’Ordre des infirmiers du lieu où elles comptent exercer et demander leur inscription au répertoire« automatisation des listes », Adeli (cf. infra « Demander l’enregistrement du diplôme ou de l ’autorisation d’exercer (n° Adeli) »).

#### Le régime de l’autorisation individuelle d’exercice

Les ressortissants ne bénéficiant pas du régime de reconnaissance automatique doivent, pour pouvoir exercer de façon permanente en France, obtenir une autorisation individuelle d’exercice délivrée par le préfet de région. Ils doivent en faire la demande en adressant un dossier complet à la direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS) du lieu où ils envisagent de s’installer.

En cas de différence substantielle entre les qualifications professionnelles acquises et celles requises en France, les ressortissants peuvent être soumis à des mesures de compensation (cf. infra « Bon à savoir : mesures de compensation ») prévues par l’arrêté du 24 mars 2010.

*Pour aller plus loin* : articles L. 4311-4 et suivants et R. 4311-34 et suivants du Code de la santé publique, arrêté du 10 juin 2004 fixant la liste des diplômes, certificats et autres titres d’infirmier/ère puéricultrice délivrés par les États membres de l’UE ou autres États partis à l’accord sur l’EEE, mentionnée à l’article L. 4311-3 du Code de la santé publique, l’arrêté du 20 janvier 2010 relatif à la déclaration préalable de prestation de services pour l’exercice des professions d’infirmier (…), et l’arrêté du 24 mars 2010 fixant les modalités d’organisation de l’épreuve d’aptitude et du stage d’adaptation pour l’exercice en France de la profession d’infirmier par les ressortissants des États membres de l’UE ou partie à l’accord sur l’EEE.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

L’ensemble des devoirs généraux s’imposant aux infirmières puéricultrices françaises s’applique aux ressortissants exerçant en France.

À ce titre, les infirmiers/ères puéricultrices doivent notamment respecter les principes de dignité, de non-discrimination ou d’indépendance. Ils sont soumis aux conditions d’exercice de la profession, aux règles professionnelles applicables en France et à la juridiction disciplinaire compétente.

*Pour aller plus loin* : articles R. 4312-1 et suivants du Code de la santé publique.

### a. Cumul d’activités

L’infirmier/ère puéricultrice peut exercer une autre activité professionnelle à la condition que ce cumul soit compatible avec la dignité et la qualité qu’exige son exercice professionnel et qu’elle soit en conformité avec la réglementation en vigueur.

*Pour aller plus loin* : article R. 4312-20 du Code de la santé publique.

### b. Conditions d’honorabilité

Pour pouvoir exercer, l’infirmier/ère puéricultrice ne doit pas :

- être frappé d’une interdiction temporaire ou définitive d’exercer la profession en France ou à l’étranger ;
- être frappé d’une suspension prononcée en raison du danger grave auquel l’exercice de l’activité expose les patients.

*Pour aller plus loin* : articles L. 4311-16 et L. 4311-26 du Code de la santé publique.

### c. Obligation de développement professionnel continu

Les infirmiers/ères puéricultrices doivent participer annuellement à un programme de développement professionnel continu. Ce programme vise à maintenir et actualiser leurs connaissances et leurs compétences ainsi qu’à améliorer leurs pratiques professionnelles.

À ce titre, le professionnel de santé (salarié ou libéral) doit justifier de son engagement dans une démarche de développement professionnel. Le programme se présente sous la forme de formations (présentielles, mixtes ou non-présentielles) d’analyse, d’évaluation, et d’amélioration des pratiques et de gestion des risques. L’ensemble des formations suivies est consigné dans un document personnel contenant les attestations de formation.

*Pour aller plus loin* : articles L. 4021-1 et R. 4382-1 du Code de la santé publique.

### d. Aptitude physique

L’infirmier/ère puéricultrice ne doit pas être atteint d’une infirmité ou d’un état pathologique qui rend dangereux l’exercice de la profession.

*Pour aller plus loin* : article L. 4311-18 du Code de la santé publique.

## 4°. Assurance

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, l’infirmier/ère puéricultrice exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

### b. Obligation d’affiliation à la caisse autonome de retraite et de prévoyance des auxiliaires médicaux (Carpimko)

Les infirmiers/ères puéricultrices exerçant à titre libéral, même accessoirement, doivent s’affilier à la caisse de retraite et de prévoyance des infirmiers, des masseurs kinésithérapeutes, des pédicures-podologues, des orthophonistes (Carpimko).

#### Pièces justificatives

L’intéressé doit adresser à la Carpimko au plus tôt :

- le questionnaire d’affiliation [téléchargeable sur le site de la Carpimko](http://www.Carpimko.com/document/pdf/affiliation_declaration.pdf) ou un courrier mentionnant la date de début d’activité libérale ;
- la photocopie du diplôme d’État ;
- la photocopie du numéro d’enregistrement (n° Adeli) du diplôme délivré par l’agence régionale de santé (ARS) ou la photocopie du verso du diplôme.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

La déclaration préalable d’activité doit être adressée au Conseil national de l’Ordre des infirmiers (CNOI), avant la première prestation de services.

#### Renouvellement de la déclaration préalable

La déclaration préalable doit être renouvelée une fois par an si le prestataire souhaite effectuer une nouvelle prestation de services en France.

#### Pièces justificatives

Cette demande est accompagnée des pièces justificatives suivantes :

- le [formulaire de déclaration](https://www.legifrance.gouv.fr/affichTexte.docidTexte=JORFTEXT000036171877&dateTexte=20180129) complété et signé ;
- l’attestation de souscription d' une assurance de responsabilité civile professionnelle pour les actes pratiqués sur le territoire français ;
- la photocopie d’une pièce d’identité attestant de la nationalité du demandeur ;
- la photocopie du ou des titre(s) de formation ;
- l’attestation de l’autorité compétente de l’État d’établissement, membre de l’UE ou de l’EEE, certifiant que l’intéressé y est légalement établi et qu’il n’encourt, lorsque l’attestation est délivrée, aucune interdiction, même temporaire, d’exercer.

**À savoir**

Les pièces justificatives, à l’exception de la photocopie de la pièce d’identité, doivent être traduites en français par un traducteur agréé auprès des tribunaux français ou habilité à intervenir auprès des autorités judiciaires ou administratives d’un État membre de l’UE ou de l’EEE.

#### Délais

Dans un délai d’un mois après la réception de la déclaration préalable, le CNOI informe le prestataire du résultat de l’examen de ses qualifications.

Dans ce délai, le CNOI peut demander un complément d’information. Dans ce cas, le délai initial d’un mois est prorogé d’un mois.

À l’issue de cette procédure, le CNOI informe le prestataire, selon le cas :

- qu’il peut débuter la prestation de services ;
- qu’il ne peut pas débuter la prestation de services ;
- qu’il doit apporter la preuve qu’il a acquis les connaissances et compétences manquantes si sa formation fait apparaître des différences substantielles avec la formation exigée en Franc (notamment au moyen d’une mesure de compensation).

En l’absence de réponse du CNOI dans les délais impartis, la prestation de services peut débuter.

#### Délivrance du récépissé

Le CNOI enregistre le prestataire de services sur une liste particulière, puis il adresse au prestataire un récépissé d’enregistrement. Le prestataire doit alors informer l’organisme national d’assurance maladie compétent de sa prestation et lui communiquer son numéro d’enregistrement.

**À savoir**

Le prestataire occasionnel et temporaire n’est pas soumis à l’obligation de s’enregistrer au répertoire Adeli et il n’est pas redevable de la cotisation ordinale.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 4311-22 et suivants et R. 4311-38 et suivants du Code de la santé publique et l’arrêté du 20 janvier 2010 précité.

### b. Formalités pour les ressortissants de l’UE en vue d’un exercice permanent (LE)

#### Le cas échéant, demander une autorisation d’exercer

Si le ressortissant ne relève pas du régime de reconnaissance automatique de son diplôme, il doit solliciter une autorisation d’exercer.

##### Autorité compétente

La demande d’autorisation d’exercer est adressée au préfet de région du lieu d’établissement de l’intéressé. Ce dernier délivre, le cas échéant, l’autorisation d’exercice après avis de la commission des infirmiers.

##### Procédure

La demande d’autorisation d’exercice doit être adressée à la direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS) du lieu d’exercice envisagé. Le préfet de région accuse réception de la demande dans le délai d’un mois suivant la réception du dossier.

##### Pièces justificatives

Le dossier de demande d’autorisation d’exercice doit être établi en deux exemplaires et contenir :

- le formulaire de demande d’autorisation d’exercice de la profession complété. Ce formulaire est disponible en ligne sur le site de la DRJSCS considérée ;
- une photocopie d’une pièce d’identité en cours de validité ;
- une copie du titre de formation permettant l’exercice de la profession dans le pays d’obtention ;
- le cas échéant, une copie des diplômes complémentaires ;
- toutes les pièces utiles justifiant des formations continues, de l’expérience et des compétences acquises au cours de l’exercice professionnel dans un État membre de l’UE ou de l’EEE ou un État tiers ;
- une déclaration, datant de moins d’un an, de l’autorité compétente de l’État de l’UE ou de l’EEE attestant de l’absence de sanction ;
- une copie des attestations des autorités ayant délivré le titre de formation spécifiant l niveau de la formation et le détail, année par année, des enseignements suivis, leur volume horaire, leurs contenus et la durée des stages validés ;
- pour les intéressés qui ont exercé dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de la profession, toutes les pièces justifiant qu’ils ont exercé dans cet État l’équivalent de deux ans à temps complet au cours des dix dernières années ;
- pour les intéressés titulaires d’un titre de formation délivré par un État tiers et reconnu dans un État de l’UE ou de l’EEE, autre que la France, la reconnaissance du titre de formation et, le cas échéant, du titre de spécialiste établie par les autorités de l’État de l’UE ou de l’EEE ayant reconnu ces titres.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Pour obtenir cette autorisation, l’intéressé peut être amené à effectue des mesures de compensation (épreuve d’aptitude ou stage d’adaptation s’il s’avère que les qualifications et l’expérience professionnelle don il se prévaut sont substantiellement différentes de celles requises pou l’exercice de la profession en France.

Si des mesures de compensation sont jugées nécessaires, le préfet d région compétent pour délivrer l’autorisation d’exercice indique l’intéressé qu’il dispose d’un délai de deux mois pour choisir entre l’épreuve d’aptitude et le stage d’adaptation (cf. infra « Bon savoir : mesures de compensation »).

##### Issue de la procédure

Le silence gardé par le préfet de région à l’expiration d’un délai d quatre mois à compter de la réception du dossier complet vaut décision de rejet de la demande.

##### Voies de recours

En cas de refus (implicite ou exprès) de la demande d’autorisatio d’exercer, le demandeur peut contester cette décision. Il peut ainsi, dans les deux mois suivant la notification de la décision de refus, former au choix :

- un recours gracieux auprès du préfet de région ;
- un recours hiérarchique auprès du ministre chargé de la santé ;
- un recours contentieux auprès du tribunal administratif territorialement compétent.

##### Coût

Gratuit.

##### Bon à savoir : mesures de compensation

###### L’épreuve d’aptitude

La DRJSCS organisatrice des épreuves d’aptitude doit convoquer l’intéressé par lettre recommandée avec avis de réception au moins u mois avant le début des épreuves. Cette convocation mentionne le jour l’heure et le lieu de l’épreuve. L’épreuve d’aptitude peut prendre l forme d’interrogations écrites ou orales notées sur vingt portant su chacune des matières qui n’ont pas été enseignées initialement ou no acquises au cours de l’expérience professionnelle.

L’admission est prononcée à la condition que l’intéressé ait obtenu un moyenne minimale de dix sur vingt et ce, sans note inférieure à huit su vingt. Les résultats de l’épreuve sont notifiés à l’intéressé par le préfet de région.

En cas de réussite, le préfet de région autorise l’intéressé à exercer la profession.

###### Le stage d’adaptation

Il s’effectue dans un établissement de santé public ou privé agréé pa l’ARS. Le stagiaire est placé sous la responsabilité pédagogique d’un professionnel qualifié exerçant la profession depuis au moins trois ans et qui établit un rapport d’évaluation.

Le stage, qui comprend éventuellement une formation théorique complémentaire, est validé par le responsable de la structure d’accueil sur proposition du professionnel qualifié évaluant le stagiaire.

Les résultats du stage sont notifiés à l’intéressé par le préfet de région.

Le cas échéant, la décision d’autorisation d’exercice est ensuite prise, après un nouvel avis de la commission mentionnée à l’article L. 4311-4 du Code de la santé publique.

*Pour aller plus loin* : articles L. 4311-4, R. 4311-34 et suivants du Code de la santé publique et l’arrêté du 20 janvier 2010 précité.

#### Demander son inscription au tableau de l’Ordre des infirmiers

Pour exercer la profession d’infirmier/ère puéricultrice, il est obligatoire de s’inscrire au tableau de l’Ordre des infirmiers. Cette inscription rend licite l’exercice de la profession sur le territoire français.

##### Autorité compétente

La demande d’inscription doit être formulée auprès du conseil départemental ou interdépartemental de l’Ordre des infirmiers (CDOI ou CIOI) dans lequel l’intéressé souhaite exercer, de préférence par lettre recommandée avec avis de réception.

##### Procédure

À réception du dossier complet de demande d’inscription, le président du CDOI ou du CIOI accuse réception de la demande dans un délai d’un mois. Le CDOI ou le CIOI dispose d’un délai maximal de trois mois pour étudier et statuer sur la demande. La décision est portée à la connaissance de l’intéressé par lettre recommandée avec avis de réception une semaine maximum après la délibération du CDOI ou CIOI.

##### Pièces justificatives

- le formulaire de demande d’inscription au tableau de l’Ordre à retirer auprès du CDOI ou du CIOI considéré ou à télécharger sur le [site de l’Ordre national des infirmiers](http://www.ordre-infirmiers.fr/lordre-et-les-conseils-ordinaux/inscription-a-lordre.html) ;
- la photocopie d’une pièce d’identité en cours de validité accompagnée, le cas échéant, d’une attestation de nationalité délivrée par une autorité compétente ;
- la copie du diplôme d’infirmier/ère puéricultrice, traduite par un traducteur agréé, le cas échéant. Cette copie doit être accompagnée :
  - soit d’une attestation de l’État de délivrance du diplôme certifiant une formation conforme aux obligations européennes,
  - soit d’une attestation certifiant que l’intéressé a exercé l’équivalent de deux années à temps complet, au cours des dix années précédant la demande, la profession d’infirmier/ère puéricultrice incluant la pleine programmation, l’organisation et l’administration des soins infirmiers aux patients ;
- les preuves des connaissances de la langue française et du système de poids et de mesures utilisés sur le territoire national ;
- les preuves de moralité :
  - soit un extrait de casier judiciaire, datant de moins de trois mois, délivré par une autorité compétente de l’État d’origine ou de provenance,
  - soit une attestation de moralité ou d’honorabilité délivrée par le conseil de l’Ordre ou l’autorité compétente de l’État membre de l’UE datant de moins de trois mois ;
- une déclaration sur l’honneur de l’intéressé certifiant ne pas faire l’objet d’une instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur l’inscription au tableau ;
- un certificat de radiation, d’inscription ou d’enregistrement, délivré par l’autorité auprès de laquelle il était antérieurement inscrit ou enregistré. À défaut, l’intéressé devra produire une déclaration sur l’honneur certifiant n’avoir jamais été inscrit ou enregistré ou, à défaut, un certificat de radiation d’inscription ou d’enregistrement dans un État membre de l’UE ou de l’EEE ;
- un curriculum vitae.

D’autres pièces justificatives peuvent être exigées selon les CDOI ou CIDI. Pour plus de précisions, il est conseillé de se rapprocher du conseil considéré.

Si l’intéressé souhaite exercer sous la forme d’une société, il doit joindre, en plus des pièces mentionnées ci-dessus :

- un exemplaire des statuts et, s’il en existe, un exemplaire du règlement intérieur et une copie ou expédition de l’acte constitutif ;
- un certificat d’inscription au tableau de l’Ordre de chacun des associés ou, s’ils ne sont pas encore inscrits, le justificatif de la demande d’inscription ;
- pour un exercice en société d’exercice libéral (SEL) :
  - une attestation du greffe du tribunal de commerce ou du tribunal de grande instance constatant le dépôt au greffe de la demande d’immatriculation de la société au registre du commerce et des sociétés,
  - une attestation des associés précisant la nature et l’évaluation des apports, le montant du capital social, le nombre, le montant minimal et la répartition des parts sociales ou des actions, l’affirmation de la libération totale ou partielle des apports.

##### Voies de recours

Tout recours doit être porté devant le conseil régional ou interrégional dans le ressort duquel se trouve le CDOI ou CIOI qui a statué dans un délai de trente jours à compter de la notification de la décision.

##### Coût

L’inscription au tableau de l’Ordre est gratuite mais elle engendre l’obligation de se soumettre à la cotisation ordinale obligatoire dont le montant est fixé tous les ans par le Conseil national de l’Ordre.

*Pour aller plus loin* : articles L. 4311-15, L. 4311-16, L. 4312-7, R. 4112- et suivants rendus applicables par les articles R. 4311-52, R. 4113-4, R. 4113-28 du Code de la santé publique.

#### En cas d’exercice sous la forme d’une société civile professionnelle (SCP) ou d’une SEL, demander l’inscription de la société au tableau de l’Ordre des infirmiers

Si l’intéressé souhaite exercer sous forme d’une SCP ou d’une SEL, il doit inscrire cette société au tableau de l’Ordre du lieu d’établissement du siège social.

##### Autorité compétente

La demande d’inscription doit être formulée auprès du CDOI ou du CIOI dans lequel l’intéressé souhaite exercer, de préférence par lettre recommandée avec avis de réception.

##### Procédure

À réception du dossier complet de demande d’inscription, le président du CDOI ou du CIOI accuse réception de la demande dans un délai d’un mois. Le CDOI ou CIOI dispose d’un délai maximal de trois mois pour étudier et statuer sur la demande. La décision est portée à la connaissance de l’intéressé par lettre recommandée avec avis de réception une semaine maximum après la délibération du CDOI ou CIOI.

##### Pièces justificatives

- un exemplaire des statuts et, s’il en existe, un exemplaire du règlement intérieur et une copie ou expédition de l’acte constitutif ;
- un certificat d’inscription au tableau de l’Ordre de chacun des associés ou, s’ils ne sont pas encore inscrits, le justificatif de la demande d’inscription ;
- pour un exercice en SEL :
  - une attestation du greffe du tribunal de commerce ou du tribunal de grande instance constatant le dépôt au greffe de la demande d’immatriculation de la société au registre du commerce et des sociétés,
  - une attestation des associés précisant la nature et l’évaluation des apports, le montant du capital social, le nombre, le montant minimal et la répartition des parts sociales ou des actions, l’affirmation de la libération totale ou partielle des apports.

##### Voies de recours

Tout recours doit être porté devant le conseil régional ou interrégional dans le ressort duquel se trouve le CDOI ou CIOI qui a statué, dans un délai de 30 jours à compter de la notification de la décision.

##### Coût

L’inscription au tableau de l’Ordre est gratuite mais elle engendre l’obligation de se soumettre à la cotisation ordinale obligatoire dont le montant est fixé tous les ans par le conseil national de l’Ordre.

#### Demander l’enregistrement du diplôme ou de l’autorisation d’exercer (n° Adeli)

Les infirmiers/ères puéricultrices sont tenus de faire enregistrer leur titre de formation ou l’autorisation requise pour l’exercice de la profession.

##### Autorité compétente

L’enregistrement du diplôme ou de l’autorisation d’exercice doit être enregistré au sein du répertoire Adeli (« automatisation des listes ») auprès de l’ARS du lieu d’exercice.

##### Délai

La demande d’enregistrement doit être présentée dans le mois suivant la prise de fonction, quel que soit le mode d’exercice (libéral, salarié, mixte). Le récépissé qui est délivré par l’ARS mentionne le numéro Adeli. L’ARS adresse ensuite au demandeur un formulaire de demande d’attribution de la carte professionnel de santé.

##### Pièces justificatives

Les pièces justificatives à fournir sont les suivantes :

- le diplôme original (traduit en français par un traducteur agréé, le cas échéant) ;
- une pièce d’identité ;
- le justificatif d’inscription à l’Ordre du département d’exercice ;
- le formulaire Cerfa 10906*06 complété, daté et signé.

Cette liste peut varier d’une région à l’autre. Pour plus de précisions, il est conseillé de se rapprocher de l’ARS concernée.

##### Coût

Gratuit.

*Pour aller plus loin* : article L. 4311-15 du Code de la santé publique.

#### Effectuer une demande d’affiliation auprès de l’assurance maladie

L’enregistrement auprès de l’assurance maladie permet à cette dernière de prendre en charge les soins effectués. De plus, cet enregistrement déclenche l’obtention de la carte de professionnel de santé (CPS) et de feuilles de soins au nom de l’infirmier/ère puéricultrice.

##### Autorité compétente

La démarche s’effectue au service des relations avec les professionnels de santé de la caisse primaire d’assurance maladie (CPAM) du lieu d’exercice.

##### Pièces justificatives

Les pièces justificatives à fournir sont les suivantes :

- la copie du diplôme ;
- la fiche Adeli ;
- le formulaire de demande de carte professionnel de santé (CPS) ;
- un relevé d’identité bancaire ;
- la carte vitale et l’attestation de carte vitale.

D’autres pièces justificatives peuvent être demandées selon les différentes CPAM. Pour plus de précisions, il est conseillé de se rapprocher de la CPAM compétente.

### c. Carte professionnelle européenne (CPE)

La carte professionnelle européenne est une procédure électronique permettant de faire reconnaître des qualifications professionnelles dans un autre État de l’UE.

La procédure CPE peut être utilisée à la fois lorsque le ressortissant souhaite exercer son activité dans un autre État de l’UE :

- à titre temporaire et occasionnel ;
- à titre permanent.

La CPE est valide :

- indéfiniment en cas d’établissement à long terme ;
- dix-huit mois pour la prestation de services à titre temporaire.

#### Demande de carte professionnelle européenne

Pour demander une CPE, le ressortissant doit :

- créer un compte utilisateur sur le [service d’authentification de la Commission Européenne](https://webgate.ec.europa.eu/cas) ;
- remplir ensuite son profil CPE (identité, coordonnées, etc.).

**À noter**

Il est également possible de créer une demande CPE en téléchargeant les pièces justificatives scannées.

#### Coût

Pour chaque demande de CPE, les autorités du pays d’accueil et du pays d’origine peuvent prélever des frais d’examen de dossier dont le montant varie selon les situations.

#### Délais

Pour une demande de CPE dans le cadre d' une activité temporaire et occasionnelle : dans un délai d’une semaine, l’autorité du pays d’origine accuse réception de la demande de CPE, signale s’il manque des documents et informe des frais éventuels. Puis, les autorités du pays d’accueil contrôlent le dossier.

Si aucune vérification n’est nécessaire auprès du pays d’accueil, l’autorité du pays d’origine examine la demande et prend une décision finale dans un délai de trois semaines.

Si des vérifications sont nécessaires au sein du pays d’accueil, l’autorité du pays d’origine a un mois pour examiner la demande et la transmettre au pays d’accueil. Le pays d’accueil prend alors une décision finale dans un délai de trois mois.

Pour une demande de CPE dans le cadre d' une activité permanente : dans un délai d’une semaine, l’autorité du pays d’origine accuse réception de la demande de CPE, signale s’il manque des documents et informe des frais éventuels. Le pays d’origine dispose ensuite d’un délai d’un mois pour examiner la demande et la transmettre au pays d’accueil. Ce dernier prend la décision finale dans un délai de trois mois.

Si les autorités du pays d’accueil estiment que le niveau d’éducation ou de formation ou que l’expérience professionnelle sont inférieurs aux normes exigées dans ce pays, elles peuvent demander au demandeur de passer une épreuve d’aptitude ou d’effectuer un stage d’adaptation.

#### Issue de la demande de CPE

Si la demande d’obtention de CPE est accordée, il est alors possible d’obtenir un certificat de CPE en ligne.

Si les autorités du pays d’accueil ne prennent pas de décision dans les délais impartis, les qualifications sont tacitement reconnues et une CPE est délivrée. Il est alors possible d’obtenir un certificat de CPE à partir de son compte en ligne.

Si la demande d’obtention de CPE est rejetée, la décision de refus doit être motivée et présenter les voies de recours pour contester ce refus.

*Pour aller plus loin* : articles R. 4311-41-4 et suivants du Code de la santé publique.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines compter du jour de la prise en charge du dossier par le centre SOLVIT de pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général de affaires européennes, 68 rue de Bellechasse, 75700, Paris, ([siteofficiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).