﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP234" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Paediatric nurse" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="paediatric-nurse" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/paediatric-nurse.html" -->
<!-- var(last-update)="2020-04-15 17:22:06" -->
<!-- var(url-name)="paediatric-nurse" -->
<!-- var(translation)="Auto" -->


Paediatric nurse
=========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:06<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The childminder is a professional nurse who specializes in early childhood care.

The childminder's duties include:

- follow-up and care for newborns, infants and children;
- monitor the diets of newborns and children in incubators or on phototherapy.

The childminder can work in maternity wards, paediatric wards of hospitals or in maternal and child protection centres.

*For further information*: Article R. 4311-13 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice the profession of childminder, the professional must have the following degrees:

- a nursing/midwife degree
- a state diploma as a childminder.

*For further information*: Article D. 4311-49 of the Public Health Code.

#### Training

For more information on training for nursing and midwifery degrees, please refer to the "Nurse/Era" and "Midwife" cards.

**State diploma of childcare**

The state diploma of childminder is awarded to the candidate:

- Holder of the state diploma of nurse/midwife;
- having passed the entrance exam at a school preparing for the diploma;
- (see Section L. 3111-1 and following from the Public Health Code).

To enter the admission competition, the applicant must submit a file that includes:

- A handwritten application for registration
- A civil registration card
- A resume
- Copying all of his diplomas or training titles;
- proof of payment of the entrance fee for the admission competition.

The admissions competition consists of two written eligibility tests and an oral entrance exam.

The 12-month training consists of:

- 650-hour theoretical lessons;
- 710-hour clinical teaching;
- directed work and evaluation of 140 hours;
- internships in hospitals and institutions contracted by the school.

The State Diploma of Childminder is awarded to a candidate who has passed the assessments of knowledge and professional abilities during his training.

*For further information*: decree of 12 December 1990 relating to schooling, the state diploma of childcare and the functioning of schools.

#### Costs associated with qualification

Training leading to the graduation of a childminder pays off. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The national of a European Union (EU) or European Economic Area (EEA) state, established and legally practising the activities of a childcare nurse in one of these states, may carry out the same activity in France on a temporary and occasionally on the condition that a prior declaration of activity has been sent to the prefect of the department of the delivery site.

If the training qualification is not recognised in France, the qualifications of the national of an EU or EEA state are checked before his first performance. In the event of substantial differences between the claimant's qualifications and the training required in France, the national must provide proof that he has acquired the missing knowledge and skills, including by submitting to compensation (see infra "Good to know: compensation measures").

In all cases, the European national wishing to practise in France on a temporary or occasional basis must possess the necessary language skills to carry out the activity and master the weight and measurement systems used in France. France.

*For further information*: Article L. 4311-22 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Nationals of an EU or EEA state wishing to practice on a permanent basis in France fall under two separate regimes. However, in both cases, the national must have the language skills necessary to carry out the activity in France and master the weight and measurement systems used in France.

#### The automatic diploma recognition scheme

All holders of a childcare nurse certificate issued by one of the EU or EEA states on the 10 June 2004 decree are entitled to automatic recognition of their diploma. In addition, nationals who do not hold one of the titles listed in Schedule I of the decree of 10 June 2004 may be granted automatic recognition provided that they have training credentials whose compliance is certified by a certificate from the State of obtaining. To find out the full list of these titles, it is advisable to refer to the decree of 10 June 2004.

In all cases, the persons concerned do not have to apply for permission to practice. On the other hand, they must register with the Departmental Council of the College of Nurses of the place where they intend to practice and apply for their registration in the directory "automation of lists", Adeli (see below: "Request the registration of the diploma or the permission to practice (No. Adeli)").

#### The individual exercise authorization regime

Nationals who do not benefit from the automatic recognition scheme must obtain an individual authorisation to exercise in France in order to be able to exercise permanently in France. They must apply for it by sending a complete file to the Regional Directorate of Youth, Sports and Social Cohesion (DRJSCS) of the place where they plan to settle.

In the event of a substantial difference between the professional qualifications acquired and those required in France, nationals may be subject to compensation measures (see below: "Good to know: compensation measures") provided by the decree of the March 24, 2010.

*For further information*: Articles L. 4311-4 and following and R. 4311-34 and following articles of the Public Health Code, decree of 10 June 2004 setting out the list of diplomas, certificates and other nursing/nursery titles issued by EU Member States or other States parties to the EEA agreement, article L. 4311-3 of the Public Health Code, the January 20, 2010 order on the prior declaration of the provision of services for the practice of nursing professions (...), and the order of March 24, 2010 setting out the terms and conditions organisation of the aptitude test and the adaptation course for the practice of nursing in France by nationals of EU Member States or party to the EEA agreement.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

All the general duties imposed on French childcare nurses apply to nationals practising in France.

As such, childminder nurses must respect the principles of dignity, non-discrimination or independence. They are subject to the conditions of practice of the profession, the professional rules applicable in France and the competent disciplinary court.

*For further information*: Articles R. 4312-1 and the following of the Public Health Code.

### a. Cumulative activities

The nurse/nursery nurse may engage in another professional activity provided that this combination is consistent with the dignity and quality required by his professional practice and that it is in accordance with the regulations in force.

*For further information*: Article R. 4312-20 of the Public Health Code.

### b. Conditions of honorability

In order to practice, the nurse/childminder must not:

- be subject to a temporary or permanent ban on practising in France or abroad;
- be suspended because of the serious danger to patients by the exercise of the activity.

*For further information*: Articles L. 4311-16 and L. 4311-26 of the Public Health Code.

### c. Obligation for continuous professional development

Nursery nurses must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of training (present, mixed or non-presential) in analysis, evaluation, and practice improvement and risk management. All training is recorded in a personal document containing training certificates.

*For further information*: Articles L. 4021-1 and R. 4382-1 of the Public Health Code.

### d. Physical aptitude

The nursery nurse must not be afflicted with a disability or a medical condition that makes the practice of the profession dangerous.

*For further information*: Article L. 4311-18 of the Public Health Code.

4°. Insurance
---------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a liberal nurse/nursery nurse must take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the self-sustaining pension and pension fund of medical assistants (Carpimko)

Nurse nurse-in-law practising in a liberal capacity, even incidentally, must join the pension and pension fund of nurses, massage therapists, pedicures-podologists, speech therapists (Carpimko).

**Supporting documents**

The person concerned must address carpimko as soon as possible:

- The affiliate questionnaire [downloadable from the Carpimko website](http://www.Carpimko.com/document/pdf/affiliation_declaration.pdf) or a letter mentioning the start date of liberal activity;
- Photocopying of the state diploma;
- a photocopy of the registration number (Adeli) of the diploma issued by the regional health agency (ARS) or a photocopy of the back of the diploma.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The prior declaration of activity must be addressed to the National Council of the College of Nurses (CNOI), prior to the first delivery of services.

**Renewal of pre-declaration**

The advance declaration must be renewed once a year if the provider wishes to re-provide services in France.

**Supporting documents**

This request is accompanied by the following supporting documents:

- The[declaration form](https://www.legifrance.gouv.fr/affichTexte.docidTexte=JORFTEXT000036171877&dateTexte=20180129) Completed and signed;
- certificate of subscription of professional liability insurance for acts performed on French territory;
- A photocopy of a piece of identification attesting to the applicant's nationality;
- Photocopying the training title or titles
- the certificate of the competent authority of the State of Establishment, member of the EU or the EEA, certifying that the person concerned is legally established there and that he does not incur, when the certificate is issued, no prohibition, even temporary, to practise.

**What to know**

The supporting documents, with the exception of the photocopying of the ID, must be translated into French by a translator authorised with the French courts or empowered to intervene with the judicial or administrative authorities of a state MEMBER of the EU or the EEA.

**Timeframe**

Within one month of receiving the advance declaration, the CNOI informs the claimant of the result of the examination of his qualifications.

Within this time, the CNOI may request further information. In this case, the initial one-month period is extended by one month.

At the end of this procedure, the CNOI informs the claimant, as the case may be:

- That it can begin service delivery
- It cannot begin service delivery;
- that he must prove that he has acquired the missing knowledge and skills if his training shows substantial differences with the training required in Franc (including through a compensation measure).

In the absence of a response from the CNOI on time, service delivery may begin.

**Delivery of receipt**

The CNOI registers the service provider on a particular list and then sends the provider a registration receipt. The claimant must then inform the relevant national health insurance agency of his benefit and provide him with his registration number.

**What to know**

The casual and temporary claimant is not required to register with the Adeli directory and is not liable for the ordinal contribution.

**Cost**

Free.

*For further information*: Articles L. 4311-22 and following and R. 4311-38 and following of the Public Health Code and the order of January 20, 2010 mentioned above.

### b. Formalities for EU nationals for a permanent exercise (LE)

#### If necessary, seek permission to practice

If the national is not under the automatic recognition scheme, he must apply for a licence to practise.

**Competent authority**

The application for permission to practise is addressed to the regional prefect of the person's place of settlement. The latter issues, if necessary, the authorisation to practice after the advice of the Committee of Nurses.

**Procedure**

The application for authorisation to exercise must be addressed to the Regional Directorate of Youth, Sports and Social Cohesion (DRJSCS) of the proposed place of practice. The regional prefect acknowledges receipt of the request within one month of receiving the file.

**Supporting documents**

The application for leave to practice must be set in two copies and contain:

- The application form for the authorization of the profession completed. This form is available online on the DRJSCS website under review;
- A photocopy of a valid ID
- A copy of the training title allowing the profession to be practised in the country of obtainment;
- If necessary, a copy of the additional diplomas;
- all useful documents justifying continuous training, experience and skills acquired during the professional year in an EU or EEA Member State or a third state;
- a statement, less than a year old, from the competent authority of the EU State or the EEA attesting to the absence of sanctions;
- A copy of the certificates of the authorities that issued the training title specifying the level of training and the detail, year by year, of the lessons taken, their hourly volume, their content and the duration of the validated internships;
- for those who have worked in an EU or EEA state that does not regulate access or the practice of the profession, all the documents justifying that they have exercised the equivalent of two full-time years in that state in the last ten years;
- for those who hold a training certificate issued by a third state and recognised in an EU or EEA state, other than France, the recognition of the training certificate and, if necessary, the specialist designation established by the state of EU or EEA having recognised these titles.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

In order to obtain this authorization, the person concerned may be required to carry out compensation measures (fitness test or accommodation if it turns out that the qualifications and professional experience he uses are substantially different from those required for the practice of the profession in France.

If compensation measures are deemed necessary, the prefect of the region responsible for issuing the exercise authorization indicates that he has two months to choose between the aptitude test and the adaptation course (see below " Good knowledge: compensation measures").

**Outcome of the procedure**

The silence kept by the regional prefect at the end of a period of four months from the receipt of the full file is worth the decision to reject the application.

**Remedies**

If the application for an application for licence to practise is refused (implicit or expressly), the applicant may challenge that decision. It can thus, within two months of notification of the refusal decision, form the choice of:

- a graceful appeal to the regional prefect;
- a hierarchical appeal to the Minister for Health;
- legal action before the territorially competent administrative court.

**Cost**

Free.

**Good to know: compensation measures**

**The aptitude test**

The DRJSCS organising the aptitude tests must summon the person by recommended letter with notice of receipt at least a month before the start of the tests. This summons mentions the day and place of the trial. The aptitude test may take the form of written or oral questions noted on twenty, each of the subjects that were not initially taught or acquired during the professional experience.

Admission is pronounced on the condition that the person has achieved a minimum average of ten out of twenty, with no score below eight over twenty. The results of the test are notified to the person concerned by the regional prefect.

If successful, the regional prefect authorizes the person concerned to practise the profession.

**The adaptation course**

It is carried out in a public or private health facility accredited by the ARS. The trainee is placed under the pedagogical responsibility of a qualified professional who has been practising the profession for at least three years and who establishes an evaluation report.

The internship, which eventually includes additional theoretical training, is validated by the head of the reception structure on the proposal of the qualified professional evaluating the trainee.

The results of the internship are notified to the person concerned by the regional prefect.

If so, the decision to authorize the exercise is then made, following a new opinion of the commission referred to in Section L. 4311-4 of the Public Health Code.

*For further information*: Articles L. 4311-4, R. 4311-34 and following of the Public Health Code and the order of January 20, 2010 above.

#### Ask for inclusion on the College of Nurses

To practice the profession of nurse/child care, it is mandatory to register on the board of the College of Nurses. This inscription makes the practice of the profession lawful on French territory.

**Competent authority**

The application for registration must be made to the departmental or interdepartmental council of the College of Nurses (CDOI or CIOI) in which the person concerned wishes to practice, preferably by letter recommended with notice of receipt.

**Procedure**

Upon receipt of the full application file, the President of the CDOI or CIOI acknowledges receipt of the application within one month. The CDOI or CIOI has a maximum of three months to study and decide on the application. The decision is made known to the person concerned by recommended letter with notice of receipt up to one week after the deliberation of the CDOI or CIOI.

**Supporting documents**

- The application form for registration on the Order's board to be withdrawn from the CDOI or CIOI considered or downloaded from the [national College of Nurses website](http://www.ordre-infirmiers.fr/lordre-et-les-conseils-ordinaux/inscription-a-lordre.html) ;
- a photocopy of a valid piece of identification accompanied, if necessary, by a certificate of nationality issued by a competent authority;
- a copy of the nursing/childmed diploma, translated by a certified translator, if necessary. This copy must be accompanied by:- or a certificate from the State of the issuance of the diploma certifying training in accordance with European obligations,
  - or a certificate certifying that the individual has worked the equivalent of two years full-time, in the ten years prior to the application, the profession of nurse/childcare profession including full programming, organization and administration of nursing to patients;
- evidence of knowledge of the French language and the system of weights and measures used in the country;
- proof of morality:- or a criminal record extract, less than three months old, issued by a competent authority of the State of origin or origin,
  - either a certificate of morality or honour from the Council of the Order or the competent authority of the EU Member State less than three months old;
- a statement on the honour of the person certifying that he or she is not the subject of a case that could give rise to a conviction or sanction that could have an impact on the inscription on the board;
- a certificate of delisting, registration or registration, issued by the authority with which it was previously registered or registered. Failing that, the person concerned will have to file a declaration on honour certifying that he or she has never been registered or, failing that, a certificate of registration or registration in an EU or EEA Member State;
- a resume.

Other supporting documents may be required according to CDOI or CIDI. For more details, it is advisable to get closer to the board in question.

If the person wishes to practice in the form of a company, he must attach, in addition to the documents mentioned above:

- a copy of the statutes and, if any, a copy of the internal regulations and a copy or shipment of the constitution;
- A certificate of registration on the order of each of the partners or, if they are not yet registered, the proof of the application for registration;
- for a liberal exercise(SEL) in-society:- a certificate from the Commercial Court or High Court Registry finding that the company's application for registration is submitted to the registry of the Commercial and Corporate Register,
  - a certificate from the partners specifying the nature and evaluation of the contributions, the amount of the share capital, the number, the minimum amount and the distribution of shares or shares, the affirmation of the total or partial release of the contributions.

**Remedies**

Any appeal must be brought before the regional or inter-regional council in which the CDOI or CIOI is located, which has ruled within thirty days of notification of the decision.

**Cost**

Registration on the Order's board is free, but it creates an obligation to submit to the compulsory ordinal dues, the amount of which is set annually by the National Council of the Order.

*For further information*: Articles L. 4311-15, L. 4311-16, L. 4312-7, R. 4112- and the following made applicable by articles R. 4311-52, R. 4113-4, R. 4113-28 of the Public Health Code.

#### In the case of exercise in the form of a professional civil society (SCP) or an SEL, request the inclusion of the company on the board of the College of Nurses

If the person wishes to practice in the form of a CPS or an SEL, he must list the company on the order of the place of establishment of the head office.

**Competent authority**

The application for registration must be made to the CDOI or THE CIOI in which the person wishes to practice, preferably by recommended letter with notice of receipt.

**Procedure**

Upon receipt of the full application file, the President of the CDOI or CIOI acknowledges receipt of the application within one month. The CDOI or CIOI has a maximum of three months to study and decide on the application. The decision is made known to the person concerned by recommended letter with notice of receipt up to one week after the deliberation of the CDOI or CIOI.

**Supporting documents**

- a copy of the statutes and, if any, a copy of the internal regulations and a copy or shipment of the constitution;
- A certificate of registration on the order of each of the partners or, if they are not yet registered, the proof of the application for registration;
- for an EXERCISE in SEL:- a certificate from the Commercial Court or High Court Registry finding that the company's application for registration is submitted to the registry of the Commercial and Corporate Register,
  - a certificate from the partners specifying the nature and evaluation of the contributions, the amount of the share capital, the number, the minimum amount and the distribution of shares or shares, the affirmation of the total or partial release of the contributions.

**Remedies**

Any appeal must be brought before the regional or inter-regional council in which the CDOI or CIOI, which has ruled, is located, within 30 days of notification of the decision.

**Cost**

Registration on the Order's board is free, but it creates an obligation to submit to the compulsory ordinal dues, the amount of which is set annually by the National Council of the Order.

#### Request registration of diploma or authorization to practice (Adeli)

Nursery nurses are required to register their training certificate or the required authorization for the practice of the profession.

**Competent authority**

The registration of the diploma or the authorization to exercise must be registered within the Adeli directory ("automation of lists") with the ARS of the place of practice.

**Timeframe**

The application for registration must be submitted within one month of taking office, regardless of the mode of exercise (liberal, salaried, mixed). The receipt issued by the ARS mentions the Adeli number. The LRA then sends the applicant an application form for the award of the professional health card.

**Supporting documents**

The supporting documents to be provided are:

- The original diploma (translated into French by a certified translator, if applicable);
- ID
- Proof of registration to the Order of the Exercise Department;
- Form Cerfa 10906Completed, dated and signed.

This list may vary from region to region. For more details, it is advisable to get closer to the ARS concerned.

**Cost**

Free.

*For further information*: Article L. 4311-15 of the Public Health Code.

#### Apply for affiliation with health insurance

Registering with health insurance allows the health insurance company to take care of the care performed. In addition, this registration triggers the acquisition of the Health Professional Card (CPS) and care sheets on behalf of the nurse/child care provider.

**Competent authority**

This is done in the health care professionals' department of the primary health insurance fund (CPAM) at the place of practice.

**Supporting documents**

The supporting documents to be provided are:

- Copying the diploma
- Adeli's card;
- The Professional Health Card (CPS) application form
- A bank identity statement
- vital card and vital card certification.

Other supporting documents may be requested according to the different CPAMs. For more details, it is advisable to get closer to the competent CPAM.

### c. European Professional Card (CPE)

The European Professional Card is an electronic procedure for recognising professional qualifications in another EU state.

The CPE procedure can be used both when the national wishes to operate in another EU state:

- temporary and occasional;
- on a permanent basis.

The CPE is valid:

- indefinitely in the event of a long-term settlement;
- 18 months for the provision of services on a temporary basis.

**Application for a European business card**

To apply for a CPE, the national must:

- Create a user account on the[Authentication service of the European Commission](https://webgate.ec.europa.eu/cas) ;
- then complete his CPE profile (identity, contact information, etc.).

**Please note**

It is also possible to create a CPE request by downloading the scanned supporting documents.

**Cost**

For each CPE application, the authorities of the host country and the country of origin may charge a file review fee, the amount of which varies depending on the situation.

**Timeframe**

For a CPE application in the context of a temporary and occasional activity: within one week, the authority of the country of origin acknowledges receipt of the CPE request, reports if documents are missing and informs of any costs. Then, the host country's authorities check the case.

If no verification is required with the host country, the country of origin authority reviews the application and makes a final decision within three weeks.

If verifications are required within the host country, the country of origin authority has one month to review the application and forward it to the host country. The host country then makes a final decision within three months.

For a CPE application as part of a permanent activity: within one week, the authority of the country of origin acknowledges receipt of the CPE request, reports if it is missing documents and informs of any costs. The country of origin then has one month to review the application and forward it to the host country. The latter makes the final decision within three months.

If the host country authorities believe that the level of education or training or work experience is below the standards required in that country, they may ask the applicant to take an aptitude test or to complete an internship adaptation.

**Issue of CPE application**

If the application for CPE is granted, then it is possible to obtain a CPE certificate online.

If the host country's authorities do not make a decision within the allotted time, qualifications are tacitly recognised and a CPE is issued. It is then possible to obtain a CPE certificate from your online account.

If the application for CPE is rejected, the decision to refuse must be justified and bring the remedies to challenge that refusal.

*For further information*: Articles R. 4311-41-4 and following of the Public Health Code.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the SOLVIT centre in the country where the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris, ([website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

