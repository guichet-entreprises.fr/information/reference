﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP248" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Midwife" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="midwife" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/midwife.html" -->
<!-- var(last-update)="2020-04-15 17:22:10" -->
<!-- var(url-name)="midwife" -->
<!-- var(translation)="Auto" -->


Midwife
=======

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:10<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

Midwifery is about helping women give birth. This health professional accompanies his patients throughout their pregnancy, performs ultrasounds, makes diagnoses and prescribes medical tests and examinations.

They may be required to prescribe vaccines to mothers and newborns, to participate with the doctor in reproductive medical assistance activities, or to carry out contraceptive consultations and preventive gynaecological follow-up.

*For further information*: Articles L. 4151-1 to L. 4151-4, and R. 4127-318 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Under Article L. 4111-1 of the Public Health Code, in order to legally practise as a midwife in France, those concerned must meet cumulatively the following three conditions:

- hold the French state diploma of midwifery or a diploma, certificate or other title mentioned in Article L. 4151-5 of the Code of Public Health (see infra "Good to know: automatic recognition of diploma");
- be of French nationality, Andorran citizenship or national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) or Morocco, subject to the application of the rules derived from the health code public or international commitments;
- with exceptions, be listed on the Board of the College of Midwives (see infra "5.0). b. Ask for inclusion on the College of Midwives list").

*For further information*: Articles L. 4111-1 and L. 4151-5 of the Public Health Code.

**Good to know: automatic diploma recognition**

Under Article L. 4151-5 of the Public Health Code, EU or EEA nationals may practise as a midwife if they hold one of the following titles:

- a midwifery training certificate issued by one of these States in accordance with EU obligations and listed on the decree of 13 February 2007;
- a midwifery training certificate issued by an EU or EEA state, in accordance with EU obligations, not on the list of the decree of 13 February 2007, if it is accompanied by a certificate from that State certifying that it is sanctioning a training in accordance with these obligations and a certificate indicating the type of training taken, supplemented if necessary by a professional practice, and that it is assimilated, by him, to the diplomas, certificates and titles listed on this list;
- a midwifery training certificate issued by one of these States in accordance with Community obligations, not on the list of the decree of 13 February 2007 and unaccompanied by the certificate of professional practice mentioned in the decree of 13 February 2007, if an EU or EEA state certifies that the person has effectively and lawfully devoted himself to midwifery activities for at least two consecutive years in the five years prior to the issuance of this certificate;
- a midwifery training certificate issued by an EU or EEA state sanctioning midwifery training started in that state prior to the dates of the 13 February 2007 decree and not in accordance with EU obligations, if it is accompanied by a certificate from one of these states certifying that the holder of the training certificate has effectively and lawfully devoted himself to midwifery activities in that state for at least three consecutive years in the five years prior to issuing this certificate;
- a midwifery training certificate issued by the former Czechoslovakia, the former Soviet Union or the former Yugoslavia or which sanctions training begun before the independence date of the Czech Republic, Slovakia, Estonia, Latvia, Lithuania or Slovenia, if accompanied by a certificate from the competent authorities of the Czech Republic or Slovakia for training documents issued by the former Czechoslovakia, Estonia, Latvia or the Czech Republic Lithuania for training documents issued by the former Soviet Union, Slovenia for training documents issued by the former Yugoslavia, certifying that they have the same legal validity as training documents issued by state. This certificate is accompanied by a certificate issued by the same authorities indicating that the holder has exercised the midwifery profession in that state, effectively and lawfully, during the at least three consecutive years in the five years prior to the issuance of the certificate;
- a midwifery training designation sanctioning training begun in Romania prior to the dates set out in the order of 13 February 2007 and not in accordance with Community obligations, if that State certifies that the person has exercised in that state, effectively and lawfully, the profession of midwifery during periods set by the minister in charge of health.
- a midwifery training certificate issued in Poland to professionals who completed their training before 1 May 2004 and not in accordance with Community obligations if that State certifies that the person has exercised in that State, effectively and lawfully, the midwifery for periods set by the Minister for Health's decree or if the training document includes a special revaluation programme allowing it to be equated with a title on the list of the decree of 13 February 2007;
- midwifery training certificates issued by an EU or EEA state sanctioning training that began before 18 January 2016.

*For further information*: Article L. 4151-5; decree of 13 February 2007 setting out the list of diplomas, certificates and other midwifery titles issued by the Member States of the European Union, the States Parties to the Agreement on the European Economic Area and the Swiss Confederation, referred to in Article L. 4151-5 (2 degrees) of the public health code.

#### Training

Midwifery studies consist of two cycles with a total duration of five
Years.

The training is punctuated:

- a competition at the end of the first year at university. This year of study, called the "first year of common health studies" (PACES) is common to students in medicine, pharmacy, odontology, physiotherapy and midwives. At the end of this first competition, students are ranked according to their results. Those in a useful rank under the numerus clausus are allowed to continue their studies and to choose, if necessary, to continue training leading to midwifery;
- the graduation of a general training in maieutic sciences at the end of the third year;
- at the end of the fifth year of training in one of the 35 midwifery schools.

**Good to know**

Midwifery students must carry out compulsory vaccinations. For more information, please refer to Section R. 3112-1 of the Public Health Code.

**General Education Diploma in Maieuse Sciences (DFGSMa)**

The first cycle is sanctioned by the DFGSMa. It consists of six semesters and corresponds to the license level. The first two semesters take place at the University of Medicine and correspond to the PACES.

The aim of the training is to:

- acquiring the scientific knowledge essential to mastering the knowledge and know-how necessary to practice the midwifery profession complementing and deepening those acquired during the previous cycle;
- acquiring practical knowledge and skills during clinical training and internships;
- The scientific approach
- Learning to work as a multi-professional team and acquiring communication techniques essential to professional practice;
- awareness of ongoing professional development including the evaluation of professional practices and the continuous deepening of knowledge.

It includes a common core and a personalized course during which the student can deepen or supplement his knowledge either:

- in the field of maieutics and perinatal health;
- for a research orientation, as part of a research course, accompanied by a minimum four-week internship;
- in a particular area other than maieutics.

**State Midwifery Diploma**

The second cycle of midwifery studies is sanctioned by the state diploma and includes four semesters corresponding to the master's level.

This cycle includes theoretical, methodological, applied, practical and clinical teachings as well as the completion of internships.

During their internships, students must write a dissertation that will result in a public defence before a jury.

Finally, a certificate of clinical and therapeutic synthesis is organised during the last semester of training and should be used to verify the skills acquired during the second cycle.

The state diploma is issued to the student as soon as he or she has:

- validated the certificate of clinical and therapeutic synthesis;
- supported his memoir successfully.

*For further information*: decree of 11 March 2013 relating to the scheme of education for the state diploma of midwifery.

#### Costs associated with qualification

The training leading to the midwifery diploma is paid for and the cost varies depending on the institution chosen. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The professional who is a member of an EU or EEA state who is established and legally practises in one of these states may, on a temporary and occasional basis in France, carry out the same activity on a temporary and occasional basis, without being included in the Midwives.

To do this, the professional must make a prior declaration, as well as a declaration justifying that he has the necessary language skills to practice in France (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

**What to know**

Registration on the College of Midwives is not required for the free-service professional (LPS). It is therefore not required to pay ordinal dues. The midwife is simply registered on a specific list maintained by the National Council of the College of Midwives.

The pre-declaration must be accompanied by a statement regarding the language skills necessary to carry out the service. In this case, the control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

When training titles do not receive automatic recognition (see supra "2.0). a. National legislation"), the provider's professional qualifications are checked before the first service is provided. In the event of substantial differences between the qualifications of the person concerned and the training required in France that could harm public health, the claimant is subjected to an aptitude test.

The midwife in LPS situation is obliged to respect the professional rules applicable in France, including all ethical rules (See infra "3°. Conditions of honorability, ethical rules, ethics"). It is subject to the disciplinary jurisdiction of the College of Midwives.

**Please note**

The performance is performed under the French professional title of midwife. However, where training qualifications are not recognised and qualifications have not been verified, the performance is carried out under the professional title of the State of Establishment, in order to avoid confusion With the French professional title.

*For further information*: Article L. 4112-7 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

**The automatic diploma recognition scheme**

Article L. 4151-5 of the Public Health Code creates a regime for automatic recognition in France of certain diplomas or titles, if any, accompanied by certificates, obtained in an EU or EEA state (see supra "2'a. National Legislation").

It is up to the National Council of the College of Midwives to verify the regularity of diplomas, titles, certificates and certificates, to grant automatic recognition and then to rule on the application for inclusion on the Order's list.

*For further information*: Article L. 4151-5 of the Public Health Code.

**The regime of individual authorisation to practise**

If the EU or EEA national does not qualify for the automatic recognition of his or her credentials, he or she falls under an authorisation scheme (see below "5o). b. If necessary, apply for an individual exercise authorization).

Persons who do not receive automatic recognition but who hold a training designation to legally practise as a midwife may be individually authorised to practise in France, by the Minister of health care, after advice from a commission made up of professionals in particular.

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession and its exercise in France, the person must submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- impose an adjustment course or aptitude test
- impose an adjustment course and an ordeal.

*For further information*: Articles L. 4151-5-1, D. 4111-8, R. 4111-17 to R. 4111-20, and R. 4151-19 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Compliance with the Midwifery Code of Ethics

The provisions of the Code of Ethics are imposed on all midwives practising in France, whether they are on the Order's board or exempt from this obligation (see above: "5. b. Ask for inclusion on the College of Midwives list").

**What to know**

All provisions of the Code of Ethics are codified in sections R. 4127-301 to R. 4127-367 of the Public Health Code.

As such, midwives must respect the principles of dignity, non-discrimination, professional secrecy and independence.

*For further information*: Articles R. 4127-301 to R. 4127-367 of the Public Health Code.

### b. Cumulative activities

The midwife may only engage in any other activity if such a combination is compatible with the principles of professional independence and dignity imposed on her. The accumulation of activities should not allow him to take advantage of his prescriptions or his medical advice.

A midwife who serves an elective or administrative mandate is also prohibited from using it to increase her clientele.

*For further information*: Articles R. 4127-322 and R. 4127-323 of the Public Health Code.

### c. Conditions of honorability

In order to practice, the midwife must certify that no proceedings that could give rise to a conviction or sanction that could affect her inclusion on the board are against her.

*For further information*: Article R. 4112-1 of the Public Health Code.

### d. Obligation for continuous professional development

Midwives must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of training (present, mixed or non-presential) in analysis, evaluation, and practice improvement and risk management. All training is recorded in a personal document containing training certificates.

*For further information*: Articles L. 4021-1 and R. 4382-1 of the Public Health Code.

### e. Physical aptitude

Midwives must not present with infirmity or pathology incompatible with the practice of the profession (see infra "5°. a. Request inclusion on the College of Midwives list").

*For further information*: Article R. 4112-2 of the Public Health Code.

4°. Insurance
---------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a liberal midwife must take out professional liability insurance.

On the other hand, if she exercises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the Independent Retirement Fund of Dental Surgeons and Midwives (CARCDSF)

Any midwife who is on the College of Midwives and practises in the liberal form (even part-time and even if she is also employed) has an obligation to join the CARCDSF.

The individual must report to carCDSF within one month of the start of his Liberal activity.

*For further information*: Article R. 643-1 of the Social Security Code; the site of the[CARCDSF](http://www.carcdsf.fr/).

### c. Health Insurance Reporting Obligation

Once on the Order's list, the midwife practising in liberal form must declare her activity with the Primary Health Insurance Fund (CPAM).

**Terms**

Registration with the CPAM can be made online on the official website of Medicare.

**Supporting documents**

The registrant must provide a complete file including:

- Copying a valid piece of identification
- a professional bank identity statement (RIB)
- if necessary, the title (s) to allow access to Sector 2.

For more information, please refer to the section on the liberal installation of midwives on the Medicare website.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally practises midwifery activities in one of these states may exercise in France on a temporary or occasional basis if he makes the prior declaration (see supra "2." b. EU and EEA nationals: for a temporary and casual exercise (Freedom to provide services)").

The advance declaration must be renewed every year.

**Please note**

Any change in the applicant's situation must be notified under the same conditions.

**Competent authority**

The declaration must be addressed, prior to the first service delivery, to the National Council of the College of Midwives.

**Terms of reporting and receipt**

The declaration can be sent by mail or directly made online on the official website of the College of Midwives.

When the National Council of the Order receives the declaration and all the necessary supporting documents, it sends the claimant a receipt specifying its registration number as well as the discipline exercised.

**Please note**

The service provider informs the relevant national health insurance agency of its provision of services by sending a copy of the receipt or by any other means.

**Timeframe**

Within one month of receiving the declaration, the National Council of the Order informs the applicant:

- Whether or not he can start delivering services;
- when the verification of professional qualifications shows a substantial difference with the training required in France, he must prove to have acquired the missing knowledge and skills by submitting to an ordeal aptitude. If he meets this check, he is informed within one month that he can begin the provision of services;
- when the file review highlights a difficulty requiring further information, the reasons for the delay in reviewing the file. He then has one month to obtain the requested additional information. In this case, before the end of the 2nd month from the receipt of this information, the National Council informs the claimant, after reviewing his file:- whether or not it can begin service delivery,
  - when the verification of the claimant's professional qualifications shows a substantial difference with the training required in France, he must demonstrate that he has acquired the missing knowledge and skills, including subjecting to an aptitude test.

In the latter case, if he meets this control, he is informed within one month that he can begin the provision of services. Otherwise, he is informed that he cannot begin the delivery of services. In the absence of a response from the National Council of the Order within these timeframes, service delivery may begin.

**Supporting documents**

The pre-declaration must be accompanied by a statement regarding the language skills required to carry out the service and the following supporting documents:

- The[Advance Service Delivery Reporting Form](http://www.ordre-sages-femmes.fr/wp-content/uploads/2015/10/Formulaire-de-d%C3%A9claration-LPS.pdf) ;
- Copying a valid piece of identification or a document attesting to the applicant's nationality;
- Copying the training document or titles, accompanied, if necessary, by a translation by a certified translator;
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and that he is not prohibited from practising, accompanied, if necessary, by a french translation established by a certified translator.

**Please note**

The control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

**Cost**

Free.

*For further information*: Articles L. 4112-7, R. 4112-9 and the following of the Public Health Code; January 20, 2010 order on the prior declaration of the provision of services for the practice of physician, dental surgeon and midwife.

### b. Formalities for EU or EEA nationals for a permanent exercise (LE)

#### If necessary, seek individual authorisation to exercise

If the national is not under the automatic recognition scheme, he must apply for a licence to practise.

**Competent authority**

The request is addressed in two copies, by letter recommended with request for notice of receipt to the unit responsible for the commissions of exercise authorization (CAE) of the National Management Centre (NMC).

**Supporting documents**

The application file must contain all of the following supporting documents:

- The[application form for authorization to practice the profession](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E876A52D33A89FE000860F290423BADF.tplgfr36s_1?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180117) ;
- A photocopy of a valid ID
- A copy of the training title allowing the practice of the profession in the state of obtaining as well as, if necessary, a copy of the specialist training title;
- If necessary, a copy of the additional diplomas;
- any useful evidence justifying continuous training, experience and skills acquired during the professional exercise in an EU or EEA state, or in a third state (certificates of functions, activity report, operational assessment, etc. ) ;
- in the context of functions performed in a state other than France, a declaration by the competent authority of that State, less than one year old, attesting to the absence of sanctions against the applicant.

Depending on the applicant's situation, additional supporting documentation is required. For more information, please refer to the official website of the[Cng](http://www.cng.sante.fr/).

**What to know**

Supporting documents must be written in French or translated by a certified translator.

**Timeframe**

The NMC acknowledges receipt of the request within one month of receipt.

The silence kept for a certain period of time from the receipt of the full file is worth the decision to dismiss the application. This delay is increased to:

- four months for applications from EU or EEA nationals with a degree from one of these states;
- six months for applications from third-party nationals with a diploma from an EU or EEA state;
- one year for other applications.

This period may be extended by two months, by decision of the ministerial authority notified no later than one month before the expiry of the latter, in the event of a serious difficulty in assessing the candidate's professional experience.

*For further information*: decree of 25 February 2010 setting out the composition of the file to be provided to the competent authorisation commissions for the examination of applications submitted for the exercise in France of the professions of doctor, dental surgeon, midwife and Pharmacist.

**Good to know: compensation measures**

Where there are substantial differences between the training and work experience of the national and those required to practise in France, the NMC may decide either:

- Suggest that the applicant choose between an adjustment course or an aptitude test;
- to impose an adjustment course and/or an aptitude test.

**The aptitude test** is intended to verify, through written or oral tests or practical exercises, the applicant's ability to practise as a midwife in the specialty concerned. It deals with subjects that are not covered by the applicant's training or training credentials or professional experience.

**The adaptation course** is intended to enable interested parties to acquire the skills necessary to practice the midwifery profession. It is carried out under the responsibility of a midwife and can be accompanied by optional additional theoretical training. The duration of the internship does not exceed three years. It can be done part-time.

*For further information*: Articles R. 4111-17 to R. 4111-20 of the Public Health Code.

#### Ask for inclusion on the College of Midwives' table

Registration on the Order's board is mandatory to legally carry out midwifery activity in France.

The registration does not apply:

- EU or EEA nationals who are established and who are legally engaged in midwifery activity in an EU or EEA state, when they perform acts of their profession in France on a temporary and occasional basis (see supra "2." b. EU and EEA nationals: for temporary and occasional exercise);
- Midwives belonging to active military health service executives;
- midwives who, having the status of public servant or a holding officer of a local community, are not called upon, in the course of their duties, to practice medicine.

**Please note**

The registration on the Order's board allows the automatic and free issuance of the health professional card. The health professional card is an electronic business identity card. It is protected by a confidential code and contains, among other things, the midwife's identification data (identity, occupation, specialty). For more information, it is recommended to refer to the government website of the French Digital Health Agency.

**Competent authority**

The application for registration is addressed to the Chairman of the Board of the College of Midwives of the department in which the person concerned wishes to establish his professional residence.

The application can be submitted directly to the departmental council of the Order concerned or sent to it by registered mail with request for notice of receipt.

**What to know**

In the event of a transfer of his professional residence outside the department, the practitioner is required to request his removal from the order of the department where he was practising and his registration on the order of his new professional residence.

**Procedure**

Upon receipt of the request, the county council appoints a rapporteur who conducts the application and makes a written report. The board verifies the candidate's titles and requests disclosure of bulletin 2 of the applicant's criminal record. In particular, it verifies that the candidate:

- fulfils the necessary conditions of morality and independence (see supra "3.3. c. Conditions of honorability");
- meets the necessary competency requirements;
- does not present a disability or pathological condition incompatible with the practice of the profession (see supra "3." e. Physical aptitude").

In case of serious doubt about the applicant's professional competence or the existence of a disability or pathological condition incompatible with the practice of the profession, the county council refers the matter to the regional or inter-regional council expertise. If, in the opinion of the expert report, there is a professional inadequacy that makes the practice of the profession dangerous, the departmental council refuses registration and specifies the training obligations of the practitioner.

No decision to refuse registration can be made without the person being invited at least a fortnight in advance by a recommended letter requesting notice of receipt to appear before the Board to explain.

The decision of the College Council is notified within a week to the individual, the National Council of the College of Midwives and the Director General of the Regional Health Agency (ARS). The notification is by recommended letter with request for notice of receipt.

The notification mentions remedies against the decision. The decision to refuse must be justified.

**Timeframe**

The Chair acknowledges receipt of the full file within one month of its registration.

The College's departmental council must decide on the application for registration within three months of receipt of the full application file. If a response is not answered within this time frame, the application for registration is deemed rejected.

This period is increased to six months for nationals of third countries when an investigation is to be carried out outside metropolitan France. The person concerned is then notified.

It may also be extended for a period of no more than two months by the Departmental Council when an expert opinion has been ordered.

**Supporting documents**

The applicant must submit a complete application file including:

- two copies of the standardized questionnaire with a completed, dated and signed photo ID, available in the College's Departmental Councils or directly downloadable from the official website of the National Council of the College of Midwives;
- A photocopy of a valid ID or, if necessary, a certificate of nationality issued by a competent authority;
- If applicable, a photocopy of a valid EU citizen's family residence card, the valid long-term resident-EC card or the resident card with valid refugee status;
- If so, a photocopy of a valid nationality certificate;
- a copy, accompanied if necessary by a translation, made by a certified translator, of the training titles to which are attached:- where the applicant is an EU or EEA national, the certificate or certificates provided (see above "2. a. National requirements"),
  - applicant is granted an individual exercise permit (see supra "2. v. EU and EEA nationals: for a permanent exercise"), copying this authorisation,
  - When the applicant presents a diploma issued in a foreign state whose validity is recognized on French territory, the copy of the titles to which that recognition may be subordinated;
- for nationals of a foreign state, a criminal record extract or an equivalent document less than three months old, issued by a competent authority of the State of origin. This part can be replaced, for EU or EEA nationals who require proof of morality or honourability for access to midwifery activity, by a certificate, less than three months old, from the competent authority of the State. certifying that these moral or honourability conditions are met;
- a statement on the applicant's honour certifying that no proceedings that could give rise to a conviction or sanction that could affect the listing on the board are against him;
- a certificate of registration or registration issued by the authority with which the applicant was previously registered or registered or, failing that, a declaration of honour from the applicant certifying that he or she was never registered or registered or, failing that, a certificate of registration or registration in an EU or EEA state;
- all the evidence that the applicant has the language skills necessary to practice the profession;
- a resume.

**Remedies**

The applicant or the National Council of the College of Midwives may challenge the decision to register or refuse registration within 30 days of notification of the decision or the implied decision to reject it. The appeal is brought before the territorially competent regional council.

The regional council must decide within two months of receiving the application. In the absence of a decision within this period, the appeal is deemed dismissed.

The decision of the regional council is also subject to appeal, within 30 days, to the National Council of the College of Midwives. The decision itself can be appealed to the Council of State.

**Cost**

Registration on the College's board is free, but it creates an obligation to pay the mandatory ordinal dues, the amount of which is set annually and which must be paid in the first quarter of the current calendar year. Payment can be made online on the official website of the National Council of the College of Midwives. As an indication, in 2018, the amount of this contribution amounts to 148 euros.

*To go further*Article R. 4112-1 and the following of the Public Health Code.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

