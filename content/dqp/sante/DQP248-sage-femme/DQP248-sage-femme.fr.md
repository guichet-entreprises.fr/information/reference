﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP248" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Sage-femme" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="sage-femme" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/sage-femme.html" -->
<!-- var(last-update)="2020-04-15 17:22:10" -->
<!-- var(url-name)="sage-femme" -->
<!-- var(translation)="None" -->

# Sage-femme

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:10<!-- end-var -->

## 1°. Définition de l'activité

Le métier de sage-femme consiste à aider les femmes à accoucher. Ce professionnel de la santé accompagne ses patientes tout au long de leur grossesse, pratique les échographies, établit les diagnostics et prescrit les analyses et les examens médicaux.

Il peut être amené à prescrire les vaccins aux mères et aux nouveau-nés, à participer avec le médecin aux activités d'assistance médicale à la procréation, ou encore à réaliser des consultations de contraception et de suivi gynécologique de prévention.

*Pour aller plus loin* : articles L. 4151-1 à L. 4151-4, et R. 4127-318 du Code la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

En application de l’article L. 4111-1 du Code de la santé publique, pour exercer légalement la profession de sage-femme en France, les intéressés doivent remplir cumulativement les trois conditions suivantes :

- être titulaire du diplôme français d’État de sage-femme ou d’un diplôme, certificat ou autre titre mentionnés à l’article L. 4151-5 du Code de la santé publique (cf. infra « Bon à savoir : la reconnaissance automatique de diplôme ») ;
- être de nationalité française, de citoyenneté andorrane ou ressortissant d’un État membre de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE) ou du Maroc, sous réserve de l’application des règles issues du Code de la santé publique ou d’engagements internationaux ;
- sauf exception, être inscrit au tableau du Conseil de l’Ordre des sages-femmes (cf. infra « 5°. b. Demander son inscription au tableau de l’Ordre des sages-femmes »).

*Pour aller plus loin* : articles L. 4111-1 et L. 4151-5 du Code de la santé publique.

#### Bon à savoir : la reconnaissance automatique de diplôme

En application de l’article L. 4151-5 du Code de la santé publique, les ressortissants de l’UE ou de l’EEE peuvent exercer la profession de sage-femme s’ils sont titulaires d’un des titres suivants :

- un titre de formation de sage-femme délivré par l'un de ces États conformément aux obligations communautaires et figurant sur la liste de l'arrêté du 13 février 2007 ;
- un titre de formation de sage-femme délivré par un État de l'UE ou de l'EEE, conformément aux obligations communautaires, ne figurant pas sur la liste de l'arrêté du 13 février 2007, s'il est accompagné d'une attestation de cet État certifiant qu'il sanctionne une formation conforme à ces obligations et d'une attestation indiquant le type de formation suivie, complétée le cas échéant par une pratique professionnelle, et qu'il est assimilé, par lui, aux diplômes, certificats et titres figurant sur cette liste ;
- un titre de formation de sage-femme délivré par l'un de ces États conformément aux obligations communautaires, ne figurant pas sur la liste de l'arrêté du 13 février 2007 et non accompagné de l'attestation de pratique professionnelle mentionnée à l'arrêté du 13 février 2007, si un Etat de l'UE ou de l'EEE atteste que l'intéressé s'est consacré de façon effective et licite aux activités de sage-femme pendant au moins deux années consécutives au cours des cinq années précédant la délivrance de cette attestation ;
- un titre de formation de sage-femme délivré par un État de l'UE ou de l'EEE sanctionnant une formation de sage-femme commencée dans cet État antérieurement aux dates figurant à l'arrêté du 13 février 2007 et non conforme aux obligations communautaires, s'il est accompagné d'une attestation de l'un de ces États certifiant que le titulaire du titre de formation s'est consacré dans cet État de façon effective et licite aux activités de sage-femme pendant au moins trois années consécutives au cours des cinq années précédant la délivrance de cette attestation ;
- un titre de formation de sage-femme délivré par l'ancienne Tchécoslovaquie, l'ancienne Union soviétique ou l'ancienne Yougoslavie ou qui sanctionne une formation commencée avant la date d'indépendance de la République tchèque, de la Slovaquie, de l'Estonie, de la Lettonie, de la Lituanie ou de la Slovénie, s'il est accompagné d'une attestation des autorités compétentes de la République tchèque ou de la Slovaquie pour les titres de formation délivrés par l'ancienne Tchécoslovaquie, de l'Estonie, de la Lettonie ou de la Lituanie pour les titres de formation délivrés par l'ancienne Union soviétique, de la Slovénie pour les titres de formation délivrés par l'ancienne Yougoslavie, certifiant qu'ils ont la même validité sur le plan juridique que les titres de formation délivrés par cet État. Cette attestation est accompagnée d'un certificat délivré par ces mêmes autorités indiquant que son titulaire a exercé dans cet État, de façon effective et licite, la profession de sage-femme pendant au moins trois années consécutives au cours des cinq années précédant la délivrance du certificat ;
- un titre de formation de sage-femme sanctionnant une formation commencée en Roumanie antérieurement aux dates fixées dans l'arrêté du 13 février 2007 et non conforme aux obligations communautaires, si cet État atteste que l'intéressé a exercé dans cet État, de façon effective et licite, la profession de sage-femme pendant des périodes fixées par arrêté du ministre chargé de la santé ;
- un titre de formation de sage-femme délivré en Pologne aux professionnels ayant achevé leur formation avant le 1er mai 2004 et non conforme aux obligations communautaires si cet État atteste que l'intéressé a exercé dans cet État, de façon effective et licite, la profession de sage-femme pendant des périodes fixées par arrêté du ministre chargé de la santé ou si le titre de formation comporte un programme spécial de revalorisation lui permettant d'être assimilé à un titre figurant sur la liste de l'arrêté du 13 février 2007 ;
- les titres de formation de sage-femme délivrés par un État de l'UE ou de l'EEE sanctionnant une formation débutée avant le 18 janvier 2016.

*Pour aller plus loin* : article L. 4151-5 ; arrêté du 13 février 2007 fixant la liste des diplômes, certificats et autres titres de sage-femme délivrés par les États membres de l'Union européenne, les États parties à l'accord sur l'Espace économique européen et la Confédération suisse, visée à l'article L. 4151-5 (2°) du Code de la santé publique.

#### Formation

Les études de sage-femme se composent de deux cycles d'une durée totale de cinq ans.

La formation est ponctuée :

- d'un concours à l'issue de la première année en université. Cette année d’étude, appelée « première année commune aux études de santé » (PACES) est commune aux étudiants en médecine, pharmacie, odontologie, kinésithérapie et sages-femmes. À l’issue de ce premier concours, les étudiants sont classés selon leurs résultats. Ceux figurant en rang utile au regard du numerus clausus sont admis à poursuivre leurs études et à choisir, le cas échéant, de continuer une formation menant à l’exercice de sage-femme ;
- de la remise du diplôme de formation générale en sciences maïeutiques à la fin de la troisième année ;
- de la remise du diplôme d’État à l'issue de la cinquième année de formation dans l'une des 35 écoles de sages-femmes.

**Bon à savoir**

Les étudiants sages-femmes doivent procéder à des vaccinations obligatoires. Pour plus de précisions, il est conseillé de se reporter à l’article R. 3112-1 du Code de la santé publique.

##### Diplôme de formation générale en sciences maïeutiques (DFGSMa)

Le premier cycle est sanctionné par le DFGSMa. Il comprend six semestres et correspond au niveau licence. Les deux premiers semestres se passent à l'université de médecine et correspondent à la PACES.

La formation a pour objectif :

- l'acquisition des connaissances scientifiques indispensables à la maîtrise des savoirs et des savoir-faire nécessaires à l'exercice de la profession de sage-femme complétant et approfondissant celles acquises au cours du cycle précédent ;
- l'acquisition de connaissances pratiques et de compétences au cours de la formation clinique et des stages ;
- la démarche scientifique ;
- l'apprentissage du travail en équipe pluriprofessionnelle et l'acquisition des techniques de communication indispensables à l'exercice professionnel ;
- la sensibilisation au développement professionnel continu comprenant l'évaluation des pratiques professionnelles et l'approfondissement continu des connaissances.

Elle comprend un tronc commun et un parcours personnalisé au cours duquel l'étudiant pourra approfondir ou compléter ses connaissances soit :

- dans un domaine de la maïeutique et de la santé périnatale ;
- en vue d'une orientation vers la recherche, dans le cadre d'un parcours recherche, accompagné d'un stage de quatre semaines minimum ;
- dans un domaine particulier autre que la maïeutique.

##### Diplôme d’État de sage-femme

Le deuxième cycle des études de sage-femme est sanctionné par le diplôme d’État et comprend quatre semestres correspondant au niveau master.

Ce cycle comprend des enseignements théoriques, méthodologiques, appliqués, pratiques et cliniques ainsi que l'accomplissement de stages.

Au cours de leurs stages, les étudiants doivent rédiger un mémoire qui donnera lieu à une soutenance publique devant un jury.

Enfin, un certificat de synthèse clinique et thérapeutique est organisé lors du dernier semestre de formation et doit permettre de vérifier les compétences acquises lors du second cycle.

Le diplôme d’État est délivré à l'étudiant dès lors qu'il a :

- validé le certificat de synthèse clinique et thérapeutique ;
- soutenu son mémoire avec succès.

*Pour aller plus loin* : arrêté du 11 mars 2013 relatif au régime des études en vue du diplôme d’État de sage-femme.

#### Coûts associés à la qualification

La formation menant à l'obtention du diplôme de sage-femme est payante et son coût varie selon l'établissement choisi. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le professionnel ressortissant d’un État de l'UE ou de l'EEE qui est établi et exerce légalement son activité dans l’un de ces États, peut, exercer en France, de manière temporaire et occasionnelle, la même activité et ce, sans être inscrit au tableau de l'ordre des sages-femmes.

Pour cela, le professionnel doit effectuer une déclaration préalable, ainsi qu'une déclaration justifiant qu'il possède les connaissances linguistiques nécessaires pour exercer en France (cf. infra « 5°. a. Effectuer une déclaration préalable d'activité pour les ressortissants de l'UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

**À savoir**

L’inscription au tableau de l’Ordre des sages-femmes n’est pas requise pour le professionnel en situation de libre prestation de services (LPS). Il n'est donc pas tenu de s’acquitter des cotisations ordinales. La sage-femme est simplement enregistrée sur une liste spécifique tenue par le Conseil national de l’Ordre des sages-femmes.

La déclaration préalable doit être accompagnée d’une déclaration concernant les connaissances linguistiques nécessaires à la réalisation de la prestation. Dans cette hypothèse, le contrôle de la maîtrise de la langue doit être proportionné à l’activité à exercer et réalisé une fois la qualification professionnelle reconnue.

Lorsque les titres de formation ne bénéficient pas d’une reconnaissance automatique (cf. supra « 2°. a. Législation nationale »), les qualifications professionnelles du prestataire sont vérifiées avant la première prestation de services. En cas de différences substantielles entre les qualifications de l’intéressé et la formation exigée en France de nature à nuire à la santé publique, le prestataire est soumis à une épreuve d'aptitude.

La sage-femme en situation de LPS est tenue de respecter les règles professionnelles applicables en France, notamment l’ensemble des règles déontologiques (cf. infra « 3°. Conditions d’honorabilité, règles déontologiques, éthique »). Elle est soumise à la juridiction disciplinaire de l’Ordre des sages-femmes.

**À noter**

La prestation est réalisée sous le titre professionnel français de sage-femme. Toutefois, lorsque les titres de formation ne bénéficient pas d’une reconnaissance et dans le cas où les qualifications n’ont pas été vérifiées, la prestation est réalisée sous le titre professionnel de l’État d’établissement, de manière à éviter toute confusion avec le titre professionnel français.

*Pour aller plus loin* : article L. 4112-7 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

#### Le régime de reconnaissance automatique du diplôme

L’article L. 4151-5 du Code de la santé publique crée un régime de reconnaissance automatique en France de certains diplômes ou titres, le cas échéant, accompagnés de certificats, obtenus dans un État de l’UE ou de l’EEE (cf. supra « 2°a. Législation nationale »).

Il appartient au conseil national de l’Ordre des sages-femmes de vérifier la régularité des diplômes, titres, certificats et attestations, d’en accorder la reconnaissance automatique puis de statuer sur la demande d’inscription au tableau de l’Ordre.

*Pour aller plus loin* : article L. 4151-5 du Code de la santé publique.

#### Le régime de l'autorisation individuelle d'exercer

Si le ressortissant de l’UE ou de l’EEE ne remplit pas les conditions pour bénéficier du régime de reconnaissance automatique de ses titres ou diplômes, il relève d’un régime d’autorisation d’exercice (cf. infra « 5°. b. Le cas échéant, demander une autorisation individuelle d’exercice).

Les personnes qui ne bénéficient pas de la reconnaissance automatique mais qui sont titulaires d’un titre de formation permettant d’exercer légalement la profession de sage-femme, peuvent être individuellement autorisées à exercer en France, par le ministre de la santé, après avis d’une commission composée notamment de professionnels.

Si l’examen des qualifications professionnelles attestés par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé doit se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente peut soit :

- proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation et une épreuve.

*Pour aller plus loin* : articles L. 4151-5-1, D. 4111-8, R. 4111-17 à R. 4111-20, et R. 4151-19 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Respect du Code de déontologie des sages-femmes

Les dispositions du Code de déontologie s’imposent à toutes les sages-femmes exerçant en France, qu'elles soient inscrites au tableau de l’Ordre ou qu’elles soient dispensées de cette obligation (cf. supra « 5°. b. Demander son inscription au tableau de l’Ordre des sages-femmes »).

**À savoir**

L’ensemble des dispositions du Code de déontologie est codifié aux articles R. 4127-301 à R. 4127-367 du Code de la santé publique.

À ce titre, les sages-femmes doivent notamment respecter les principes de dignité, de non-discrimination, de secret professionnel ou encore d'indépendance.

*Pour aller plus loin* : articles R. 4127-301 à R. 4127-367 du Code de la santé publique.

### b. Cumul d'activités

La sage-femme ne peut exercer une autre activité que si un tel cumul est compatible avec les principes d’indépendances et de dignité professionnelles qui s’imposent à elle. Le cumul d’activités ne doit pas lui permettre de tirer profit de ses prescriptions ou de ses conseils médicaux.

Il est également interdit à une sage-femme qui remplit un mandat électif ou une fonction administrative d’en user pour accroître sa clientèle.

*Pour aller plus loin* : articles R. 4127-322 et R. 4127-323 du Code de la santé publique.

### c. Conditions d'honorabilité

Pour pouvoir exercer, la sage-femme doit certifier qu'aucune instance pouvant donner lieu à une condamnation ou une sanction susceptible d'avoir des conséquences sur son inscription au tableau n'est en cours à son encontre.

*Pour aller plus loin* : article R. 4112-1 du Code de la santé publique.

### d. Obligation de développement professionnel continu

Les sages-femmes doivent participer annuellement à un programme de développement professionnel continu. Ce programme vise à maintenir et actualiser leurs connaissances et leurs compétences ainsi qu'à améliorer leurs pratiques professionnelles.

À ce titre, le professionnel de santé (salarié ou libéral) doit justifier de son engagement dans une démarche de développement professionnel. Le programme se présente sous la forme de formations (présentielles, mixtes ou non-présentielles) d’analyse, d’évaluation, et d’amélioration des pratiques et de gestion des risques. L’ensemble des formations suivies est consigné dans un document personnel contenant les attestations de formation.

*Pour aller plus loin* : articles L. 4021-1 et R. 4382-1 du Code de la santé publique.

### e. Aptitude physique

Les sages-femmes ne doivent pas présenter d’infirmité ou de pathologie incompatible avec l’exercice de la profession (cf. infra « 5°. a. Demander son inscription au tableau de l’Ordre des sages-femmes »).

*Pour aller plus loin* : article R. 4112-2 du Code de la santé publique.

## 4°. Assurance

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, la sage-femme exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, si elle exerce en tant que salariée, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

### b. Obligation d’affiliation à la caisse autonome de retraite des chirurgiens-dentistes et des sages-femmes (CARCDSF)

Toute sage-femme inscrite au tableau de l'Ordre des sages-femmes et exerçant sous la forme libérale (même à temps partiel et même si elle exerce par ailleurs une activité salariée) a l’obligation d’adhérer à la CARCDSF.

L’intéressé doit se déclarer à la CARCDSF dans le mois suivant le début de son activité libérale.

*Pour aller plus loin* : article R. 643-1 du Code de la sécurité sociale ; site de la [CARCDSF](http://www.carcdsf.fr/).

### c. Obligation de déclaration auprès de l’Assurance maladie

Une fois inscrite au tableau de l’Ordre, la sage-femme exerçant sous forme libérale doit déclarer son activité auprès de la Caisse primaire d’assurance maladie (CPAM).

#### Modalités

L’inscription auprès de la CPAM peut être réalisée en ligne sur le site officiel de l’Assurance maladie.

#### Pièces justificatives

Le déclarant doit communiquer un dossier complet comprenant :

- la copie d’une pièce d’identité en cours de validité ;
- un relevé d’identité bancaire (RIB) professionnel 
- le cas échéant, le(s) titre(s) justificatif(s) permettant l’accès au secteur 2.

Pour plus d’informations, il est conseillé de se reporter à la rubrique consacrée à l’installation en libéral des sages-femmes du site de l’Assurance maladie.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d'activité pour les ressortissants de l'UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

Tout ressortissant de l’UE ou de l’EEE qui est établi et exerce légalement les activités de sage-femme dans l’un de ces États peut exercer en France de manière temporaire ou occasionnelle s’il en fait la déclaration préalable (cf. supra « 2°. b. Ressortissants UE et EEE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Service) »).

La déclaration préalable doit être renouvelée tous les ans.

**À noter**

Tout changement de situation du demandeur doit être notifié dans les mêmes conditions.

#### Autorité compétente

La déclaration doit être adressée, avant la première prestation de services, au Conseil national de l’Ordre des sages-femmes.

#### Modalités de la déclaration et récépissé

La déclaration peut être envoyée par courrier ou directement effectuée en ligne sur le site officiel de l’Ordre des sages-femmes.

Lorsque le Conseil national de l’Ordre reçoit la déclaration et l’ensemble des pièces justificatives nécessaires, il adresse au prestataire un récépissé précisant son numéro d’enregistrement ainsi que la discipline exercée.

**À noter**

Le prestataire de services informe préalablement l’organisme national d’assurance maladie compétent de sa prestation de services par l’envoi d’une copie de ce récépissé ou par tout autre moyen.

#### Délai

Dans un délai d’un mois à compter de la réception de la déclaration, le Conseil national de l’Ordre informe le demandeur :

- qu’il peut ou non débuter la prestation de services ;
- lorsque la vérification des qualifications professionnelles met en évidence une différence substantielle avec la formation exigée en France, qu’il doit prouver avoir acquis les connaissances et les compétences manquantes en se soumettant à une épreuve d’aptitude. S’il satisfait à ce contrôle, il est informé dans un délai d’un mois qu’il peut débuter la prestation de services ;
- lorsque l’examen du dossier met en évidence une difficulté nécessitant un complément d’informations, des raisons du retard pris dans l’examen de son dossier. Il dispose alors d’un délai d’un mois pour obtenir les compléments d’informations demandés. Dans ce cas, avant la fin du deuxième mois à compter de la réception de ces informations, le Conseil national informe le prestataire, après réexamen de son dossier :
  - qu’il peut ou non débuter la prestation de services,
  - lorsque la vérification des qualifications professionnelles du prestataire met en évidence une différence substantielle avec la formation exigée en France, qu’il doit démontrer qu’il a acquis les connaissances et compétences manquantes, notamment en se soumettant à une épreuve d’aptitude.

Dans cette dernière hypothèse, s’il satisfait à ce contrôle, il est informé dans le délai d’un mois qu’il peut débuter la prestation de services. Dans le cas contraire, il est informé qu’il ne peut pas débuter la prestation de services. En l’absence de réponse du Conseil national de l’Ordre dans ces délais, la prestation de services peut débuter.

#### Pièces justificatives

La déclaration préalable doit être accompagnée d’une déclaration concernant les connaissances linguistiques nécessaires à la réalisation de la prestation et des pièces justificatives suivantes :

- le [formulaire de déclaration préalable de prestation de services](http://www.ordre-sages-femmes.fr/wp-content/uploads/2015/10/Formulaire-de-d%C3%A9claration-LPS.pdf) ;
- la copie d’une pièce d’identité en cours de validité ou d’un document attestant la nationalité du demandeur ;
- la copie du ou des titres de formation, accompagnée, le cas échéant, d’une traduction par un traducteur agréé ;
- une attestation de l’autorité compétente de l’État d’établissement de l’UE ou de l’EEE certifiant que l’intéressé est légalement établi dans cet État et qu’il n’encourt aucune interdiction d’exercer, accompagnée, le cas échéant, d’une traduction en français établie par un traducteur agréé.

**À noter**

Le contrôle de la maîtrise de la langue doit être proportionné à l’activité à exercer et réalisé une fois la qualification professionnelle reconnue.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 4112-7, R. 4112-9 et suivants du Code de la santé publique ; arrêté du 20 janvier 2010 relatif à la déclaration préalable de prestation de services pour l’exercice des professions de médecin, chirurgien-dentiste et sage-femme.

### b. Formalités pour les ressortissants de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Le cas échéant, demander une autorisation individuelle d’exercice

Si le ressortissant ne relève pas du régime de reconnaissance automatique de son diplôme, il doit solliciter une autorisation d'exercer.

##### Autorité compétente

La demande est adressée en deux exemplaires, par lettre recommandée avec demande d’avis de réception à la cellule chargée des commissions d’autorisation d’exercice (CAE) du Centre national de gestion (CNG).

##### Pièces justificatives

Le dossier de demande doit contenir l'ensemble des pièces justificatives suivantes :

- le [formulaire de demande d’autorisation d’exercice de la profession](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E876A52D33A89FE000860F290423BADF.tplgfr36s_1?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180117) ;
- une photocopie d’une pièce d’identité en cours de validité ;
- une copie du titre de formation permettant l’exercice de la profession dans l’État d’obtention ainsi que, le cas échéant, une copie du titre de formation de spécialiste ;
- le cas échéant, une copie des diplômes complémentaires ;
- toutes pièces utiles justifiant des formations continues, de l’expérience et des compétences acquises au cours de l’exercice professionnel dans un État de l’UE ou de l’EEE, ou dans un État tiers (attestations de fonctions, bilan d’activité, bilan opératoire, etc.) ;
- dans le cadre de fonctions exercées dans un État autre que la France, une déclaration de l’autorité compétente de cet État, datant de moins d’un an, attestant de l’absence de sanctions à l’égard du demandeur.

Selon la situation du demandeur, d’autres pièces justificatives sont exigées. Pour plus d’informations, il est conseillé de se reporter au site officiel du [CNG](http://www.cng.sante.fr/).

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé.

##### Délai

Le CNG accuse réception de la demande dans le délai d’un mois à compter de sa réception.

Le silence gardé pendant un certain délai à compter de la réception du dossier complet vaut décision de rejet de la demande. Ce délai est porté à :

- quatre mois pour les demandes présentées par les ressortissants de l’UE ou de l’EEE titulaires d’un diplôme délivré dans l’un de ces États ;
- six mois pour les demandes présentées par les ressortissants d’États tiers titulaires d’un diplôme délivré par un État de l’UE ou de l’EEE ;
- un an pour les autres demandes.

Ce délai peut être prolongé de deux mois, par décision de l’autorité ministérielle notifiée au plus tard un mois avant l’expiration de celui-ci, en cas de difficulté sérieuse portant sur l’appréciation de l’expérience professionnelle du candidat.

*Pour aller plus loin* : arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de médecin, chirurgien-dentiste, sage-femme et pharmacien.

##### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation et l'expérience professionnelle du ressortissant et celles requises pour exercer en France, la CNG peut décider soit :

- de proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- d'imposer un stage d’adaptation et/ou une épreuve d’aptitude.

**L’épreuve d’aptitude** a pour objet de vérifier, par des épreuves écrites ou orales ou par des exercices pratiques, l’aptitude du demandeur à exercer la profession de sage-femme dans la spécialité concernée. Elle porte sur les matières qui ne sont pas couvertes par le ou les titres de formation du demandeur ou son expérience professionnelle.

**Le stage d’adaptation** a pour objet de permettre aux intéressés d’acquérir les compétences nécessaires à l’exercice de la profession de sage-femme. Il est accompli sous la responsabilité d’une sage-femme et peut être accompagné d’une formation théorique complémentaire facultative. La durée du stage n’excède pas trois ans. Il peut être effectué à temps partiel.

*Pour aller plus loin* : articles R. 4111-17 à R. 4111-20 du Code de la santé publique.

#### Demander son inscription au tableau de l'Ordre des sages-femmes

L'inscription au tableau de l'Ordre est obligatoire pour exercer légalement l'activité de sage-femme en France.

L'inscription ne s'applique pas :

- aux ressortissants de l’UE ou de l’EEE qui sont établis et qui exercent légalement l’activité de sage-femme dans un État de l'UE ou de l'EEE, lorsqu’ils exécutent en France, de manière temporaire et occasionnelle, des actes de leur profession (cf. supra « 2°. b. Ressortissants UE et EEE : en vue d’un exercice temporaire et occasionnel ») ;
- aux sages-femmes appartenant aux cadres actifs du service de santé des armées ;
- aux sages-femmes qui, ayant la qualité de fonctionnaire de l’État ou d’agent titulaire d’une collectivité locale, ne sont pas appelés, dans l’exercice de leurs fonctions, à exercer la médecine.

**À noter**

L’inscription au tableau de l’Ordre permet la délivrance automatique et gratuite de la carte de professionnel de santé.. La carte de professionnel de santé est une carte d’identité professionnelle électronique. Elle est protégée par un code confidentiel et contient notamment les données d’identification de la sage-femme (identité, profession, spécialité). Pour plus d’informations, il est recommandé de se reporter au site gouvernemental de l’Agence française de la santé numérique.

##### Autorité compétente

La demande d’inscription est adressée au président du conseil de l’Ordre des sages-femmes du département dans lequel l’intéressé souhaite établir sa résidence professionnelle.

La demande peut être directement déposée au conseil départemental de l’Ordre concerné ou lui être adressée par courrier recommandé avec demande d’avis de réception.

**À savoir**

En cas de transfert de sa résidence professionnelle hors du département, le praticien est tenu de demander sa radiation du tableau de l’Ordre du département où il exerçait et son inscription au tableau de l’Ordre de sa nouvelle résidence professionnelle.

##### Procédure

A la réception de la demande, le conseil départemental désigne un rapporteur qui procède à l’instruction de la demande et fait un rapport écrit. Le conseil vérifie les titres du candidat et demande communication du bulletin n° 2 du casier judiciaire de l’intéressé. Il vérifie notamment que le candidat :

- remplit les conditions nécessaires de moralité et d’indépendance (cf. supra « 3°. c. Conditions d’honorabilité ») ;
- remplit les conditions nécessaires de compétence ;
- ne présente pas une infirmité ou un état pathologique incompatible avec l’exercice de la profession (cf. supra « 3°. e. Aptitude physique »).

En cas de doute sérieux sur la compétence professionnelle du demandeur ou sur l’existence d’une infirmité ou d’un état pathologique incompatible avec l’exercice de la profession, le conseil départemental saisit le conseil régional ou interrégional qui diligente une expertise. S’il est constaté, au vu du rapport d’expertise, une insuffisance professionnelle rendant dangereux l’exercice de la profession, le conseil départemental refuse l’inscription et précise les obligations de formation du praticien.

Aucune décision de refus d’inscription ne peut être prise sans que l’intéressé ait été invité quinze jours au moins à l’avance par lettre recommandée avec demande d’avis de réception à comparaître devant le conseil pour y présenter ses explications.

La décision du conseil de l’Ordre est notifiée dans la semaine qui suit à l’intéressé, au Conseil national de l’Ordre des sages-femmes et au directeur général de l’agence régionale de santé (ARS). La notification se fait par lettre recommandée avec demande d’avis de réception.

La notification mentionne les voies de recours contre la décision. La décision de refus doit être motivée.

##### Délai

Le président accuse réception du dossier complet dans un délai d’un mois à compter de son enregistrement.

Le conseil départemental de l’Ordre doit statuer sur la demande d’inscription dans un délai maximum de trois mois à compter de la réception du dossier complet de demande. À défaut de réponse dans ce délai, la demande d’inscription est réputée rejetée.

Ce délai est porté à six mois pour les ressortissants des États tiers lorsqu’il y a lieu de procéder à une enquête hors de la France métropolitaine. L’intéressé en est alors avisé.

Il peut également être prorogé d’une durée qui ne peut excéder deux mois par le Conseil départemental lorsqu’une expertise a été ordonnée.

##### Pièces justificatives

L’intéressé doit adresser un dossier complet de demande d’inscription comprenant :

- deux exemplaires du questionnaire normalisé avec une photo d’identité rempli, daté et signé, disponible dans les Conseils départementaux de l’Ordre ou directement téléchargeable sur le site officiel du Conseil national de l’Ordre des sages-femmes ;
- une photocopie d’une pièce d’identité en cours de validité ou, le cas échéant, une attestation de nationalité délivrée par une autorité compétente ;
- le cas échéant, une photocopie de la carte de séjour de membre de la famille d’un citoyen de l’UE en cours de validité, de la carte de résident de longue durée-CE en cours de validité ou de la carte de résident portant mention du statut de réfugié en cours de validité ;
- le cas échéant, une photocopie d’une attestation de nationalité en cours de validité ;
- une copie, accompagnée le cas échéant d’une traduction, faite par un traducteur agréé, des titres de formation à laquelle sont joints :
  - lorsque le demandeur est un ressortissant de l’UE ou de l’EEE, la ou les attestations prévues (cf. supra « 2°. a. Exigences nationales »),
  - lorsque le demandeur bénéficie d’une autorisation d’exercice individuelle (cf. supra « 2°. c. Ressortissants UE et EEE : en vue d’un exercice permanent »), la copie de cette autorisation,
  - lorsque le demandeur présente un diplôme délivré dans un État étranger dont la validité est reconnue sur le territoire français, la copie des titres à la possession desquels cette reconnaissance peut être subordonnée ;
- pour les ressortissants d’un État étranger, un extrait de casier judiciaire ou un document équivalent datant de moins de trois mois, délivré par une autorité compétente de l’État d’origine. Cette pièce peut être remplacée, pour les ressortissants des États de l’UE ou de l’EEE qui exigent une preuve de moralité ou d’honorabilité pour l’accès à l’activité de sage-femme, par une attestation, datant de moins de trois mois, de l’autorité compétente de l’État d’origine certifiant que ces conditions de moralité ou d’honorabilité sont remplies ;
- une déclaration sur l’honneur du demandeur certifiant qu’aucune instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur l’inscription au tableau n’est en cours à son encontre ;
- un certificat de radiation d’inscription ou d’enregistrement délivré par l’autorité auprès de laquelle le demandeur était antérieurement inscrit ou enregistré ou, à défaut, une déclaration sur l’honneur du demandeur certifiant qu’il n’a jamais été inscrit ou enregistré ou, à défaut, un certificat d’inscription ou d’enregistrement dans un État de l’UE ou de l’EEE ;
- tous les éléments de nature à établir que le demandeur possède les connaissances linguistiques nécessaires à l’exercice de la profession ;
- un curriculum vitae.

##### Voies de recours

Le demandeur ou le Conseil national de l’Ordre des sages-femmes peuvent contester la décision d’inscription ou de refus d’inscription dans un délai de 30 jours à compter de la notification de la décision ou de la décision implicite de rejet. L’appel est porté devant le conseil régional territorialement compétent.

Le conseil régional doit statuer dans un délai de deux mois à compter de la réception de la demande. À défaut de décision dans ce délai, le recours est réputé rejeté.

La décision du conseil régional est également susceptible d’appel, dans les 30 jours, auprès du Conseil national de l’Ordre des sages-femmes. La décision ainsi rendue peut elle-même faire l’objet d’un recours devant le Conseil d’État.

##### Coût

L’inscription au tableau de l’Ordre est gratuite mais elle engendre l’obligation de s’acquitter de la cotisation ordinale obligatoire dont le montant est fixé annuellement et qui doit être versée au cours du premier trimestre de l’année civile en cours. Le paiement peut s’effectuer en ligne sur le site officiel du Conseil national de l’Ordre des sages-femmes. À titre indicatif, en 2018, le montant de cette cotisation s’élève à 148 euros.

*Pour aller plus loin*: article R. 4112-1 et suivants du Code de la santé publique.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).