﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP237" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Radiophysicien" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="radiophysicien" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/radiophysicien.html" -->
<!-- var(last-update)="2020-04-15 17:22:08" -->
<!-- var(url-name)="radiophysicien" -->
<!-- var(translation)="None" -->

# Radiophysicien

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:08<!-- end-var -->

## 1°. Définition de l’activité

Le radiophysicien ou physicien médical est un professionnel de santé exerçant au sein d'une équipe pluridisciplinaire dont l'activité consiste à mettre en œuvre ses connaissances en matière de rayonnements dans le domaine médical. Il est notamment chargé de la qualité des images, de la dosimétrie (étude quantitative de la radiation) et s'assure de l'optimisation de l'utilisation des équipements à rayonnements à des fins de diagnostiques ou thérapeutiques du patient.

Le radiophysicien apporte son expertise en matière de :

- radiothérapie ;
- médecine nucléaire ;
- imagerie médicale.

**À noter**

L'exercice illégal de la profession est puni de deux ans d'emprisonnement et de 30 000 euros d'amende 

*Pour aller plus loin* : articles L. 4251-1 et L. 4252-2 du Code de la santé publique (Code de la santé publique).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de radiophysicien, le professionnel doit :

- être titulaire de l'un des diplômes ou titres de formation suivants :
  - un diplôme en physique radiologique et médicale,
  - un diplôme de physicien médical,
  - d'un agrément en tant que radiophysicien délivré avant le 28 novembre 2004 ;
- procéder à son enregistrement.

**À noter**

Cette procédure d'enregistrement est gratuite et doit s'effectuer avant le début de l'exercice de la profession de radiophysicien auprès de l'organisme compétent.

*Pour aller plus loin* : article L. 4251-2 et L. 4251-3 du Code de la santé publique.

#### Formation

##### Diplômes prérequis

Pour être reconnu comme étant qualifié professionnellement, l'intéressé doit être titulaire d'un diplôme de niveau master (bac+5) en physique radiologique ou en physicien médical. Les modalités d'accès et la formation dispensée sont propres aux universités délivrant ces diplômes.

Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

##### Diplôme de qualification en physique radiologique et médicale (DQPRM)

En vue de se spécialiser, le professionnel doit effectuer une formation portant sur la radiothérapie, la curiethérapie, la radiologie et médecine nucléaire et de radioprotection des patients.

Cette formation est accessible sur concours. Peuvent se présenter, les candidats titulaires d'un diplôme de niveau master (bac+5) ou d'un niveau équivalent dans les domaines de la physique radiologique et médicale, admis comme prérequis pour l'inscription aux épreuves du concours d'entrée.

Pour s'inscrire aux épreuves de sélection de la formation spécialisée, le diplôme du candidat doit répondre aux exigences de contenu, fixées à l'annexe II de l'arrêté du 6 décembre 2011 relatif à la formation et aux missions de la personne spécialisée en radiophysique médicale et à la reconnaissance des qualifications professionnelles des ressortissants étrangers pour l'exercice de ces missions en France.

La formation spécialisée est d'une durée d'un an minimum et comporte :

- un enseignement théorique d'au moins 180 heures ;
- un stage au sein d'un ou plusieurs établissements de santé ou des cabinets libéraux sous la responsabilité d'un professionnel spécialisé en radiophysique. Ce stage se décompose comme suit :
  - 36 semaines minimum en radiothérapie ;
  - 10 semaines en médecine nucléaire ;
  - 6 semaines en radiologie.

À l'issue du stage le candidat doit subir des épreuves de contrôle de connaissances devant un jury professionnel et en cas de succès, obtient un DQPRM par l'organisme ayant délivré la formation.

*Pour aller plus loin* : articles R. 1333-59 et suivants du Code de la santé publique ; arrêté du 6 décembre 2011 susvisé.

#### Coûts associés à la qualification

La formation menant au diplôme de physique radiologique et médicale ou de physicien médical est payante et son coût varie selon l'établissement délivrant la formation. Pour plus d'informations, il est conseillé de se rapprocher de l'établissement considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union Européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de radiophysicien dans cet État peut, exercer en France à titre temporaire et occasionnel la même activité.

Lorsque l’État membre ne réglemente ni la formation ni son exercice, l'intéressé doit justifier avoir exercé en tant que radiophysicien pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, le professionnel doit procéder à une déclaration préalable avant sa première prestation en France (cf. infra « 4°. b. Déclaration préalable pour un ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

**À noter**

Lorsque des différences substantielles existent entre la formation et l'expérience professionnelles du ressortissant et celles exigées pour exercer en France, le préfet de département pourra décider de le soumettre à une épreuve d'aptitude ou un stage d'adaptation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

En outre, le ressortissant doit justifier avoir les connaissances nécessaires à l'exercice de la profession de radiophysicien en France.

*Pour aller plus loin* : article L. 4251-6 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant de l’ UE légalement établi dans un État membre pour y exercer l'activité de radiophysicien peut exercer en France, la même activité dès lors qu'il est titulaire :

- d'un titre de formation délivré par un ou plusieurs États membres qui réglementent l'activité de radiophysicien et lui permettent d'exercer légalement cette activité ;
- lorsque l’État ne réglemente ni l'accès à la profession ni son exercice, d'un titre attestant qu'il a reçu une formation préparant à l'exercice de cette profession et un document justifiant qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- d'un titre de formation délivré par un État tiers et reconnu par un État membre autre que la France lui permettant d'exercer légalement sa profession. Le cas échéant, l'intéressé devra justifié avoir exercé l'activité pendant trois ans au sein de cet État membre.

**À noter**

En cas de différences substantielles entre la formation du professionnel et celle exigée pour exercer la profession de radiophysicien en France, le préfet pourra le soumettre à une mesure de compensation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : article L. 4251-5 du Code de la santé publique.

## 3°. Assurances

En qualité de professionnel de santé, le radiophysicien doit, s'il exerce à titre libéral, souscrire une assurance de responsabilité professionnelle.

S'il exerce en tant que salarié, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de cette activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable pour un ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande avant sa première prestation de services au préfet de département de son choix.

#### Pièces justificatives

La demande doit comporter :

- le [formulaire de déclaration](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=FDBFFCF534F0D849F090355DB5B55EE8.tplgfr25s_2?idArticle=LEGIARTI000024986947&cidTexte=LEGITEXT000024986922&dateTexte=20180313) complété et signé ;
- l'ensemble des informations relatives à l'état civil, la nationalité et la légalité d'établissement du ressortissant dans l’État membre d'origine.

#### Délais

Dans un délai d'un mois à compter de la réception de la demande du ressortissant, le préfet l'informe soit :

- qu'il peut débuter la prestation de services ;
- qu'il ne peut pas débuter la prestation de services ;
- qu'il doit faire l'objet d'une mesure de compensation en vue de démontrer qu'il possède les connaissances nécessaires à l'exercice de son activité en France.

L'absence de réponse du préfet au-delà d'un délai d'un mois vaut rejet de la demande.

#### Issue de la procédure

Le préfet de région enregistre le ressortissant sur une liste particulière et lui adresse un récépissé mentionnant son numéro d'enregistrement et précisant l'organisme d'assurance maladie compétent.

**À noter**

La déclaration préalable permet au ressortissant d'exercer sur l'ensemble du territoire français et doit être renouvelée tous les ans selon les mêmes modalités.

*Pour aller plus loin* : articles 12 à 16 et annexe VII de l'arrêté du 6 décembre 2011 relatif à la formation et aux missions de la personne spécialisée en radiophysique médicale et à la reconnaissance des qualifications professionnelles des ressortissants étrangers pour l'exercice de ces missions en France.

### b. Demande d'autorisation d'exercice pour le ressortissant UE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant doit adresser une demande en deux exemplaires par lettre recommandée avec avis de réception au préfet du département au sein duquel il souhaite exercer.

#### Pièces justificatives

Sa demande doit contenir les documents suivants, le cas échéant assortis de leur traduction en français :

- le formulaire d'autorisation d'exercice fixé à l'annexe V de l'arrêté du 6 décembre 2011 complété et signé ;
- une photocopie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation permettant l'exercice de l'activité de radiophysicien et le cas échéant ses diplômes complémentaires ;
- tout document justifiant de ses formations ou expériences professionnelles ;
- une déclaration de l'autorité compétente de son État d'établissement datant de moins d'un an, certifiant qu'il ne fait l'objet d'aucune sanction ;
- une copie des attestations précisant le niveau de formation ainsi que leur contenu et la durée des stages validés par le demandeur ;
- selon les cas :
  - lorsque l’État ne réglemente ni l'accès ni l'exercice de la profession, toute pièce justifiant qu'il a exercé des fonctions de spécialiste en radiophysique médicale pendant au moins un an au cours des dix dernières années,
  - lorsque le professionnel a acquis son titre de formation au sein d'un État tiers mais reconnu par un État membre, la reconnaissance du titre de formation par l’État l'ayant reconnu.

#### Délais et issue de la procédure

Le préfet accuse réception de la demande dans un délai d'un mois à compter de sa réception.

L'absence de réponse au-delà d'un délai de quatre mois vaut rejet de la demande. Dès lors que la demande est acceptée, le ressortissant se voit remettre une carte professionnelle de santé et peut exercer la profession de radiophysicien dans les mêmes conditions que le ressortissant français.

*Pour aller plus loin* : articles 6 à 11 de l'arrêté du 6 décembre 2011 susvisé.

#### Bon à savoir : mesures de compensation

Lorsque l'examen des qualifications professionnelles du ressortissant fait apparaître des différences substantielles entre sa formation et celle requise pour exercer l'activité de radiophysicien en France ou dès lors que la formation du professionnel est inférieure d'au moins un an à celle du diplôme français, le préfet de département peut, après avis d'une commission décider de soumettre le professionnel à une épreuve d'aptitude ou à un stage d'adaptation.

L'épreuve d'aptitude consiste en un examen écrit et oral permettant de vérifier les connaissances du ressortissant. Le stage d'adaptation quant à lui, d'une durée maximale de trois ans, doit être réalisé auprès d'un radiophysicien exerçant depuis au moins trois ans pour les stages en médecine nucléaire et en radiologie et depuis au moins cinq ans pour les stages en radiothérapie.

Les modalités d'organisation des mesures de compensation sont fixées à l'annexe VI de l'arrêté du 6 décembre 2011 susvisé.

*Pour aller plus loin* : article L. 4251-5 du Code de la santé publique.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris [site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html).