﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP237" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Radiophysicist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="radiophysicist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/radiophysicist.html" -->
<!-- var(last-update)="2020-04-15 17:22:08" -->
<!-- var(url-name)="radiophysicist" -->
<!-- var(translation)="Auto" -->


Radiophysicist
==============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:08<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The radiophysicist or medical physicist is a health professional working within a multidisciplinary team whose activity is to implement his knowledge of radiation in the medical field. In particular, he is responsible for the quality of the images, the dosimetry (quantitative study of radiation) and ensures the optimization of the use of radiation equipment for diagnostic or therapeutic purposes of the patient.

The radiophysicist brings his expertise in:

- radiation therapy;
- Nuclear medicine;
- medical imaging.

**Please note**

The illegal practice of the profession is punishable by two years' imprisonment and a fine of 30,000 euros

*For further information*: Articles L. 4251-1 and L. 4252-2 of the Public Health Code (Public Health Code).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of radiophysicist, the professional must:

- hold one of the following diplomas or training titles:- a degree in radiological and medical physics,
  - a medical physicist's degree,
  - accreditation as a radiophysicist issued before November 28, 2004;
- to register it.

**Please note**

This registration procedure is free of charge and must be carried out before the start of the profession of radiophysicist with the relevant body.

*For further information*: Article L. 4251-2 and L. 4251-3 of the Public Health Code.

#### Training

**Pre-requis honorary degrees**

To be recognized as a professionally qualified person, the person must have a master's degree (B.A.5) degree in radiological physics or medical physicist. The access arrangements and training provided are specific to the universities issuing these diplomas.

It is advisable to get close to the institutions concerned for more information.

**Qualification Diploma in Radiological and Medical Physics (DQPRM)**

In order to specialize, the professional must conduct training in radiotherapy, brachytherapy, radiology and nuclear medicine and radiation protection of patients.

This training is available by competition. Candidates with a master's degree (B.A. 5) or an equivalent level in the fields of radiological and medical physics may apply, admitted as a prerequisite for entry into the entrance exams.

In order to register for the specialized training selection tests, the candidate's diploma must meet the content requirements set out in Schedule II of the December 6, 2011 order on the training and missions of the person medical radiophysics and the recognition of the professional qualifications of foreign nationals for the performance of these missions in France.

The specialized training is for a minimum of one year and includes:

- theoretical instruction of at least 180 hours
- an internship at one or more health facilities or liberal practices under the responsibility of a radiophysical professional. This internship breaks down as follows:- 36 weeks minimum in radiotherapy;
  - 10 weeks in nuclear medicine;
  - Six weeks in radiology.

At the end of the internship, the candidate must undergo knowledge tests before a professional jury and, if successful, obtains a DQPRM by the organization that issued the training.

*For further information*: Articles R. 1333-59 and the following of the Public Health Code; December 6, 2011.

#### Costs associated with qualification

Training for a degree in radiological and medical physics or medical physicist is paid for and the cost varies depending on the institution providing the training. For more information, it is advisable to get closer to the establishment in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the agreement on the European Economic Area (EEA) legally established and practising the activity of radiophysicist in that state may, exercise in France on a temporary and casual basis the same activity.

Where the Member State does not regulate training or exercise, the individual must justify having practised as a radiophysicist for at least one year in the last ten years.

Once the professional fulfils these conditions, he must make a prior declaration before his first performance in France (see infra "4°. b. Pre-declaration for an EU national for a temporary and casual exercise (LPS)").

**Please note**

Where there are substantial differences between the national's professional training and experience and those required to practise in France, the department prefect may decide to submit him to an aptitude test or an internship. adaptation (see infra "5.00). b. Good to know: compensation measures").

In addition, the national must justify having the necessary knowledge to practice the profession of radiophysicist in France.

*For further information*: Article L. 4251-6 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any EU national legally established in a Member State to work as a radiophysicist can carry out the same activity in France, as long as he owns:

- a training certificate issued by one or more Member States that regulate the activity of radiophysicist and allow him to carry out this activity legally;
- where the state does not regulate access to or exercise of the profession, a title attesting that it has received training in preparation for the practice of that profession and a document justifying that it has been engaged in this activity for at least one year in the last ten years Years
- a training certificate issued by a third state and recognised by a Member State other than France allowing it to legally practise its profession. If so, the person concerned must have justified having been active for three years in that Member State.

**Please note**

In the event of substantial differences between the training of the professional and that required to practise as a radiophysicist in France, the prefect may subject him to a compensation measure (see infra "5°. b. Good to know: compensation measures").

*For further information*: Article L. 4251-5 of the Public Health Code.

3°. Insurance
------------------------

As a health professional, the radiophysicist must, if he practises in a liberal capacity, take out professional liability insurance.

If he exercises as an employee, it is up to the employer to take out such insurance for his employees for the acts carried out during this activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Pre-declaration for an EU national for a temporary and casual exercise (LPS)

**Competent authority**

The national must apply before his first service delivery to the prefect of the department of his choice.

**Supporting documents**

The application must include:

- The[declaration form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=FDBFFCF534F0D849F090355DB5B55EE8.tplgfr25s_2?idArticle=LEGIARTI000024986947&cidTexte=LEGITEXT000024986922&dateTexte=20180313) Completed and signed;
- all information relating to the civil status, nationality and legality of the national's establishment in the Member State of origin.

**Timeframe**

Within one month of receiving the national's request, the prefect informs him:

- That it can begin service delivery
- It cannot begin service delivery;
- that he must be compensated in order to demonstrate that he has the necessary knowledge to carry out his activity in France.

The prefect's failure to respond beyond a one-month delay is worth rejecting the application.

**Outcome of the procedure**

The regional prefect registers the national on a particular list and sends him a receipt stating his registration number and specifying the relevant health insurance agency.

**Please note**

The prior declaration allows the national to practise throughout the French territory and must be renewed every year on the same terms.

*For further information*: Articles 12 to 16 and Appendix VII of the December 6, 2011 order on the training and missions of the medical radiophysics specialist and the recognition of the professional qualifications of foreign nationals for the exercise of these missions to France.

### b. Application for authorisation to exercise for the EU national for a permanent exercise (LE)

**Competent authority**

The national must submit an application in two copies by letter recommended with notice of receipt to the prefect of the department in which he wishes to practice.

**Supporting documents**

His application must contain the following documents, if any, with their translation into French:

- The exercise authorization form set out in Schedule V of the December 6, 2011 order completed and signed;
- A photocopy of his valid ID
- A copy of his training title allowing the exercise of the activity of radiophysicist and if necessary his additional diplomas;
- any documents justifying his training or professional experience;
- a statement by the competent authority of its State of establishment less than one year old, certifying that it is not subject to any sanction;
- A copy of the certificates detailing the level of training and the content and duration of the internships validated by the applicant;
- depending on the case:- where the state does not regulate access or practice of the profession, any evidence justifying that it has held the position of medical radiophysics specialist for at least one year in the past ten years,
  - where the professional has acquired his training title in a third state but recognised by a Member State, the recognition of the training title by the State has recognized him.

**Delays and outcome of the procedure**

The prefect acknowledges receipt of the request within one month of receipt.

Failure to respond beyond four months is a reason to reject the application. Once the application is accepted, the national is given a professional health card and can practise as a radiophysicist under the same conditions as the French national.

*For further information*: Articles 6 to 11 of the order of December 6, 2011 above.

**Good to know: compensation measures**

When the examination of the professional qualifications of the national reveals substantial differences between his training and that required to carry out the activity of radiophysicist in France or as long as the training of the professional is less than one year less than that of the French diploma, the prefect of department may, after notice of a commission, decide to subject the professional to an aptitude test or an adjustment course.

The aptitude test consists of a written and oral examination to verify the knowledge of the national. The adaptation course, for a maximum of three years, must be carried out with a radiophysicist who has been practising for at least three years for internships in nuclear medicine and radiology and for at least five years for radiotherapy internships.

The modalities for the organisation of compensation measures are set out in Annex VI of the aforementioned decree of 6 December 2011.

*For further information*: Article L. 4251-5 of the Public Health Code.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris[official website](https://sgae.gouv.fr/sites/SGAE/accueil.html).

