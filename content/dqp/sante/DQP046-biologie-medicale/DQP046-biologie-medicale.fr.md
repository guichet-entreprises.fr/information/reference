﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP046" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Biologie médicale" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="biologie-medicale" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/biologie-medicale.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="biologie-medicale" -->
<!-- var(translation)="None" -->

# Biologie médicale

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

La biologie médicale est une discipline médicale qui consiste, grâce à un examen médical spécifique, à diagnostiquer et prévenir les pathologies des patients et assurer leur suivi thérapeutique.

Le médecin spécialiste (appelé biologiste médical) réalise cet examen au sein d'un laboratoire de biologie médicale.

*Pour aller plus loin* : article L. 6211-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de biologiste médical, le professionnel doit :

- être qualifié professionnellement pour exercer la profession de médecin ou de pharmacien et être titulaire soit :
  - d'un diplôme de spécialité en biologie médicale (cf. ci-après « Diplôme d'études spécialisées en biologie médicale (DESBM) »),
  - d'une qualification en biologie médicale délivrée par l'Ordre national des médecins, l'Ordre national des pharmaciens ou le ministre chargé de la défense pour les praticiens des armées,
  - d'une autorisation individuelle d'exercice délivrée par le ministre chargé de la santé dans les conditions prévues à l'article L. 4111-2 du Code de la santé publique ;
- être inscrit au tableau de l'Ordre des médecins ou des pharmaciens.

Pour connaître les modalités de formation en vue de devenir pharmacien ou médecin, et les modalités d'inscription au tableau de l'Ordre de ces professions, il est conseillé de se reporter aux fiches « Pharmacien » et « Médecin qualifié en médecine générale ».

En outre, certains professionnels peuvent faire reconnaître leur domaine de spécialisation (cf. infra « Reconnaissance d'un domaine de spécialisation »).

*Pour aller plus loin* : articles L. 6213-1 à L. 6213-6-1 du Code de la santé publique.

#### Formation

##### Diplôme d'études spécialisées en biologie médicale (DESBM)

Pour exercer en tant que biologiste médical, le professionnel doit être titulaire du DES « Biologie médicale ».

Cette formation est accessible aux étudiants en médecine ayant passé avec succès les épreuves classantes nationales (ECN) et aux étudiants en pharmacie ayant réussi le concours de l'internat.

Ce diplôme permet au futur spécialiste d'acquérir les connaissances et la pratique nécessaires à l'exercice de ses fonctions.

Cette formation se compose de huit semestres dont au moins trois dans un lieu de stage avec encadrement universitaire et au moins un dans un lieu de stage sans encadrement universitaire.

**À noter**

Dans le cadre de son projet professionnel, le futur biologiste médical peut candidater à une formation spécialisée transversale (FST) parmi les suivantes :

- bio-informatique médicale ;
- génétique et médecine moléculaire bioclinique ;
- hématologie bioclinique ;
- hygiène - prévention de l'infection, résistances ;
- médecine et biologie de la reproduction - andrologie ;
- nutrition appliquée ;
- pharmacologie médicale/thérapeutique ;
- thérapie cellulaire/transfusion.

La formation du DES se décompose en trois phases : la phase socle, la phase d'approfondissement et la phase de consolidation.

###### La phase socle

D'une durée de quatre semestres, elle permet au candidat d'acquérir les connaissances de base de la spécialité choisie et d'effectuer :

- trois stages dans un lieu agréé à titre principal en biologie médicale et doit couvrir les domaines de la biochimie-biologie moléculaire, l'hématologie et la bactériologie-virologie ;
- un stage dans un lieu agréé à titre principal en biologie médicale et dans un domaine différent du précédent stage.

###### La phase d'approfondissement

D'une durée de deux semestres, elle permet au candidat d'acquérir les connaissances et compétences nécessaires à l'exercice de la spécialité choisie et d'effectuer deux stages dans un lieu agréé à titre principal en biologie médicale et bénéficiant d'un agrément pour ce domaine.

###### La phase de consolidation

D'une durée de deux semestres, elle permet au candidat de consolider ses connaissances et d'effectuer :

- un stage d'un semestre dans un lieu agréé à titre principal en biologie médicale ;
- un stage d'un semestre dans un lieu bénéficiant d'un agrément fonctionnel en biologie médicale ou agréé à titre principal dans une autre spécialité et à titre secondaire en biologie médicale.

*Pour aller plus loin* : arrêté du 27 novembre 2017 modifiant l’arrêté du 12 avril 2017 relatif à l’organisation du troisième cycle des études de médecine et l’arrêté du 21 avril 2017 relatif aux connaissances, aux compétences et aux maquettes de formation des diplômes d’études spécialisées et fixant la liste de ces diplômes et des options et formations spécialisées transversales du troisième cycle des études de médecine.

##### Reconnaissance d'un domaine de spécialisation

Peut également exercer en tant que biologiste médical, le professionnel qui, à compter du 30 mai 2013 :

- a pratiqué la biologie médicale dans un établissement de santé pendant au moins deux ans au cours des dix dernières années, ou qui a commencé à exercer entre le 13 janvier 2008 et le 13 janvier 2010 et a exercé cette activité pendant deux ans avant le 13 janvier 2012. Toutefois le professionnel qui a exercé la biologie médicale dans un domaine de spécialisation ne peut qu'exercer au sein de ce domaine ;
- a exercé la profession de vétérinaire et qui, ayant commencé une spécialisation en biologie médicale avant le 13 janvier 2010, a obtenu sa spécialisation au plus tard le 13 janvier 2016 ;
- a exercé les fonctions de directeur ou directeur adjoint d'un Centre national pour la lutte contre les maladies transmissibles et a reçu l'autorisation d'exercer la biologie médicale par le ministre chargé de la santé ;
- a été autorisé par le ministre chargé de la santé dans un domaine de spécialisation de son laboratoire ;
- a exercé pendant au moins trois ans les fonctions de biologiste médical dans une structure ou un laboratoire de biologie médicale.

Le cas échéant, le professionnel devra effectuer une demande de reconnaissance de son domaine de spécialisation (cf. infra « 5°. a. Demande de reconnaissance de spécialisation »).

En outre, le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles devra également effectuer, une demande d'autorisation d'exercice (cf. infra « 5°. b. Demande d'autorisation d'exercice pour le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles »).

*Pour aller plus loin* : articles L. 6213-2 et L. 6213-2-1 du Code de la santé publique ; ordonnance n° 2010-49 du 13 janvier 2010 relative à la biologie médicale.

#### Coûts associés à la qualification

La formation menant à l'obtention du diplôme de médecin est payante et son coût varie selon l'établissement choisi. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d'un État de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de médecin ou de pharmacien peut exercer en France, de manière temporaire et occasionnelle, la même activité, et ce, sans être inscrit au tableau de l'Ordre des médecins ou des pharmaciens.

Pour cela il devra effectuer une déclaration préalable dont la nature dépend de sa qualification professionnelle et une déclaration justifiant qu'il possède les connaissances linguistiques nécessaires pour exercer en France.

Pour connaître les modalités de ces déclarations préalables, il est conseillé de se reporter aux paragraphes « 5°. Démarches et formalités de reconnaissance de qualification » des fiches « Pharmacien » et « Médecin qualifié en médecine générale ».

### c. Ressortissants UE : en vue d’un exercice permanent (LE)

Le ressortissant d'un État de l'UE ou de l'EEE légalement établi et exerçant en qualité de médecin ou de pharmacien peut exercer la même activité à titre permanent en France.

À ce titre, le ressortissant peut bénéficier, selon la nature de sa qualification professionnelle, soit :

- du régime de la reconnaissance automatique de son diplôme de médecin ou de pharmacien. Le cas échéant, il appartiendra à l'Ordre de vérifier la régularité de son diplôme, titre ou certification et d'en accorder la reconnaissance automatique en vue de l'inscrire au tableau de l'Ordre de la profession concernée ;
- du régime de l'autorisation individuelle d'exercer. Le cas échéant, le professionnel devra effectuer une demande d'autorisation. Il est conseillé de se reporter aux fiches « Pharmacien » et « Médecin qualifié en médecine générale » pour de plus amples informations.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### a. Respect du Code de déontologie des médecins ou des pharmaciens

Le biologiste médical est soumis au respect des règles professionnelles applicables aux professions de médecin et de pharmacien.

L'ensemble des dispositions des Codes de déontologie des médecins et des pharmaciens est codifié aux articles R. 4127-1 à R. 4127-112 et R. 4235-1 à R. 4235-77 du Code de la santé publique.

À ce titre, le professionnel doit notamment respecter les principes de dignité, de non-discrimination, de secret professionnel ou encore d'indépendance.

*Pour aller plus loin* : article L. 6213-7 du Code de la santé publique.

### b. Cumul d’activités

Le professionnel ne peut exercer une autre activité que si un tel cumul est compatible avec les principes d'indépendance et de dignité professionnelles qui s'imposent à lui.

À ce titre, il ne peut cumuler ses fonctions avec une autre activité voisine du domaine de la santé.

Il lui est également interdit d'exercer une fonction administrative et d'en user pour accroître sa clientèle.

*Pour aller plus loin* : articles R. 4127-26 à R. 4127-27 et R. 4235-4 à R. 4235-23 du Code de la santé publique.

### c. Obligation de développement professionnel continu

Le biologiste médical en tant que professionnel de la santé doit suivre un programme de développement professionnel continu. Ce programme permet d'améliorer la qualité et la sécurité des soins, et de maintenir et d'actualiser ses connaissances.

L'ensemble des formations suivies ou des actions réalisées doivent être retranscrites dans un document personnel contenant les attestations de formation.

*Pour aller plus loin* : décret n° 2016-942 du 8 juillet 2016 relatif à l'organisation du développement professionnel continu des professionnels de santé.

## 4°. Assurances et sanctions

### a. Obligation d'assurance

#### Obligation de souscrire une assurance de responsabilité civile** professionnelle

En qualité de professionnel de santé, le biologiste médical exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

#### Obligation d'affiliation à une caisse d'assurance vieillesse ou de retraite

Le professionnel est tenu soit :

- d'adhérer à la Caisse autonome de retraite des médecins de France (CARMF), s'il exerce en qualité de médecin et est inscrit au tableau de l'Ordre des médecins ;
- de s'affilier à la Caisse d'assurance vieillesse des pharmaciens (CAVP), s'il est inscrit au tableau de l'Ordre des pharmaciens.

Pour connaître les modalités de ces démarches, vous pouvez consulter le [site de la CAVP](https://www.cavp.fr/) et [le site de la CARMF](http://www.carmf.fr/).

#### Obligation de déclaration après de l'Assurance Maladie

Une fois inscrit au tableau de l'Ordre de sa profession, le professionnel exerçant sous forme libérale doit déclarer son activité auprès de la Caisse primaire d'assurance maladie (CPAM).

#### Modalités

L’inscription auprès de la CPAM peut être réalisée en ligne sur le site officiel de l’Assurance maladie.

#### Pièces justificatives

Le déclarant doit communiquer un dossier complet comprenant :

- la copie d’une pièce d’identité en cours de validité ;
- un relevé d’identité bancaire (RIB) professionnel ;
- le cas échéant, le(s) titre(s) justificatif(s) permettant l’accès au secteur 2.

Pour plus d’informations, il est conseillé de se reporter à la rubrique consacrée à l’installation en libéral des médecins du site de l’Assurance maladie.

### b. Sanctions

#### Sanctions disciplinaires

Le professionnel est tenu, en vue de son inscription au tableau de l'ordre de sa profession, de communiquer au conseil départemental de l'ordre dont il relève, l'ensemble des contrats et avenants relatifs à sa profession ainsi que ceux relatifs à l'usage du matériel et des locaux au sein desquels il exerce, s'il n'en est pas le propriétaire.

En cas de manquement à cette obligation, le professionnel encourt l'une des sanctions disciplinaires suivantes :

- un avertissement ;
- un blâme ;
- une interdiction d'exercer de manière temporaire ou définitive ;
- la radiation du tableau de l'ordre.

*Pour aller plus loin* : articles L. 6241-1 ; L. 4124-6 et L. 4234-6 du Code de la santé publique.

#### Sanctions pénales

Le professionnel qui exerce l'activité de biologiste médical sans être qualifié professionnellement encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende pour usurpation de titre.

En outre, l'exercice illégal de la profession de biologiste médical est puni d'une peine de deux ans d'emprisonnement et de 30 000 euros d'amende.

*Pour aller plus loin* : articles L. 6242-1 et L. 6242-2 du Code de la santé publique.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande de reconnaissance de spécialisation

#### Autorité compétente

Le professionnel doit adresser sa demande au Centre national de gestion par lettre recommandée avec avis de réception à l'adresse suivante :

Centre national de gestion, département concours, autorisations d'exercice, mobilité-développement professionnel
21 B, rue Leblanc, 75737 Paris Cedex 15

#### Pièces justificatives

Sa demande doit comporter les documents suivants :

- une lettre de demande motivée et mentionnant le domaine de spécialisation envisagé ;
- une photocopie d'une pièce d'identité en cours de validité ;
- un curriculum vitae détaillé ainsi que les éventuelles publications du demandeur ;
- une copie de l'ensemble de ses diplômes ;
- une attestation justifiant que le ressortissant a exercé la biologie médicale pendant au moins deux ans au cours des dix dernières années et avant le 13 janvier 2012 ;
- pour le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles, son doctorat d'exercice ou d'université, ou son diplôme d'ingénieur en lien avec la biologie médicale ;
- pour le médecin ou le pharmacien exerçant au sein d'un centre hospitalier ou universitaire, un document justifiant qu'il exerce bien au sein d'un tel établissement depuis au moins trois ans et mentionnant son statut.

#### Délai et procédure

Le centre national de gestion accuse réception de la demande dans un délai d'un mois à compter de sa réception et la transmet à la Commission nationale de biologie médicale. En l'absence de réponse dans un délai de quatre mois à compter de la réception du dossier complet, la reconnaissance est accordée au demandeur.

*Pour aller plus loin* : article R. 6213-2 du Code de la santé publique ; arrêté du 14 février 2017 fixant la composition du dossier à fournir à la Commission nationale de biologie médicale prévue à l'article L. 6213-12 du Code de la santé publique et définissant les domaines de spécialisation mentionnés à l'article R. 6213-1 du même Code.

### b. Demande d'autorisation d'exercice pour le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles

#### Autorité compétente

Le professionnel doit adresser sa demande au Centre national de gestion par lettre recommandée avec avis de réception.

#### Pièces justificatives

Sa demande doit comporter :

- une lettre motivée de demande ;
- une photocopie d'une pièce d'identité en cours de validité ;
- un curriculum vitae décrivant son cursus professionnel et, le cas échéant, l'ensemble de ses publications ;
- une copie de sa demande d'autorisation d'exercice des fonctions de directeur ou directeur adjoint de laboratoire ;
- une photocopie de l'ensemble de ses diplômes, titres, ou certifications dans le domaine de la biologie médicale.

#### Délai et procédure

Le Centre national de gestion accuse réception de la demande dans un délai d'un mois à compter de sa réception. L'absence de réponse du ministre chargé de la santé au delà d'un délai de quatre mois vaut rejet de la demande d'autorisation.

*Pour aller plus loin* : R. 6213-5 à R. 6213-7 du Code de la santé publique ; arrêté du 14 février 2017 susvisé ; V de l'article 9 de l'ordonnance n° 2010-49 du 13 janvier 2010 relative à la biologie médicale.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).