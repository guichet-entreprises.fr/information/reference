﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP046" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Clinical pathologist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="clinical-pathologist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/clinical-pathologist.html" -->
<!-- var(last-update)="2020-04-15 17:21:16" -->
<!-- var(url-name)="clinical-pathologist" -->
<!-- var(translation)="Auto" -->


Clinical pathologist
=================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

Medical biology is a medical discipline that consists, through a specific medical examination, of diagnosing and preventing patients' pathologies and ensuring their therapeutic follow-up.

The medical specialist (called a medical biologist) performs this examination in a medical biology laboratory.

*For further information*: Article L. 6211-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To be a medical biologist, the professional must:

- be professionally qualified to practise as a doctor or pharmacist and hold either:- a specialty degree in medical biology (see below "Specialized Diploma in Medical Biology (DESBM)"),
  - a qualification in medical biology issued by the National College of Physicians, the National College of Pharmacists or the Minister for Defence for Military Practitioners,
  - an individual exercise authorization issued by the Minister responsible for health under the conditions of Article L. 4111-2 of the Public Health Code;
- be listed on the College of Physicians or Pharmacists.

To find out how to train to become a pharmacist or a doctor, and how to register on the College of These Professions, it is advisable to refer to the "Pharmacist" and "Qualified Physician in General Medicine" cards.

In addition, some professionals may have their area of specialization recognized (see infra "Recognition of a specialization area").

*For further information*: Articles L. 6213-1 to L. 6213-6-1 of the Public Health Code.

#### Training

**Diploma in Medical Biology (DESBM)**

To practice as a medical biologist, the professional must hold the "Medical Biology" DES.

This training is available to medical students who have successfully passed the National Classant Tests (NCA) and to pharmacy students who have passed the internship competition.

This diploma allows the future specialist to acquire the knowledge and practice necessary to carry out his duties.

This training consists of eight semesters, of which at least three are in an internship with university supervision and at least one in an internship place without university supervision.

**Please note**

As part of his professional project, the future medical biologist may apply for a specialized cross-sectional training (TSF) among the following:

- medical bioinformatics;
- genetics and bioclinical molecular medicine;
- bioclinical hematology;
- hygiene - infection prevention, resistance;
- medicine and reproductive biology - andrology;
- Applied nutrition
- Medical/therapeutic pharmacology;
- cell therapy/transfusion.

The formation of the DES is divided into three phases: the base phase, the deepening phase and the consolidation phase.

**The base phase**

Lasting four semesters, it allows the candidate to acquire the basic knowledge of the chosen specialty and to perform:

- three internships in a senior accredited location in medical biology and must cover the fields of molecular biochemistry, hematology and bacteriology-virology;
- an internship in a senior licensed location in medical biology and in a field different from the previous internship.

**The deepening phase**

Lasting two semesters, it allows the candidate to acquire the knowledge and skills necessary to practice the chosen specialty and to complete two internships in a place approved as a principal in medical biology and benefiting from accreditation. for this area.

**The consolidation phase**

Lasting two semesters, it allows the candidate to consolidate his knowledge and perform:

- a one-semester internship in a senior licensed location in medical biology;
- a one-semester internship in a place with functional accreditation in medical biology or a principally accredited in another specialty and secondary in medical biology.

*For further information*: Decree of 27 November 2017 amending the decree of 12 April 2017 relating to the organisation of the third cycle of medical studies and the decree of 21 April 2017 relating to the knowledge, skills and training models of the diplomas of study and establishing the list of these diplomas and the cross-sectional specialized options and training of the postgraduate medical studies.

**Recognition of a specialty area**

Can also practice as a medical biologist, the professional who, as of May 30, 2013:

- practiced medical biology in a health facility for at least two years over the past 10 years, or who began practising between January 13, 2008 and January 13, 2010 and practiced this activity for two years prior to January 13, 2012. However, the professional who has practiced medical biology in a field of specialization can only practice in this field;
- practiced as a veterinarian and who, having begun a specialization in medical biology before January 13, 2010, was awarded his specialization by January 13, 2016;
- served as Director or Deputy Director of a National Centre for communicable Diseases and was authorized to practice medical biology by the Minister responsible for health;
- has been authorised by the Minister responsible for health in an area of specialisation of his laboratory;
- has served for at least three years as a medical biologist in a medical biology structure or laboratory.

If necessary, the professional will have to apply for recognition of his area of specialization (see infra "5°. a. Application for specialization recognition").

In addition, the director or deputy director of a national centre for the control of communicable diseases will also have to apply for an exercise authorization (see infra "5°. b. Application for authorization to practice for the director or deputy director of a national communicable disease centre").

*For further information*: Articles L. 6213-2 and L. 6213-2-1 of the Public Health Code;[Ordinance No. 2010-49](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021683301&dateTexte=20180409) January 13, 2010 on Medical Biology.

#### Costs associated with qualification

The training leading to the doctor's degree is paid for and the cost varies depending on the institution chosen. For more information, it is advisable to check with the institutions concerned.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) state or a state party to the legally established European Economic Area (EEA) agreement may practice the same temporary and occasional practice in France. activity, without being listed on the College of Physicians or Pharmacists.

In order to do so, he will have to make a prior declaration, the nature of which depends on his professional qualification and a declaration justifying that he has the necessary language skills to practice in France.

To find out the terms of these advance declarations, it is advisable to refer to paragraphs "5." Procedures and formalities of recognition of qualification" of the "Pharmacist" and "Qualified Physician in General Medicine" cards.

### c. EU nationals: for a permanent exercise (LE)

A national of a legally established EU state or EEA who practises as a doctor or pharmacist may carry out the same activity on a permanent basis in France.

As such, the national may benefit, depending on the nature of his professional qualification, i.e.:

- automatic recognition of his or her doctor's or pharmacist's diploma. If necessary, it will be up to the College to verify the regularity of its diploma, title or certification and to grant automatic recognition in order to register it in the Order of the Profession in question;
- individual authorization to practise. If necessary, the professional will have to apply for permission. It is advisable to refer to the "Pharmacist" and "Qualified Physician in General Medicine" cards for more information.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Compliance with the code of ethics of doctors or pharmacists

The medical biologist is subject to respect for the professional rules applicable to the professions of doctor and pharmacist.

All provisions of the Physicians' and Pharmacists Codes of Ethics are codified in sections R. 4127-1 at R. 4127-112 and R. 4235-1 at R. 4235-77 of the Public Health Code.

As such, the professional must respect the principles of dignity, non-discrimination, professional secrecy or independence.

*For further information*: Article L. 6213-7 of the Public Health Code.

### b. Cumulative activities

The professional can only engage in any other activity if such a combination is compatible with the principles of professional independence and dignity imposed on him.

As such, he cannot combine his duties with any other activity close to the health field.

It is also prohibited from performing an administrative function and using it to increase its clientele.

*For further information*: Articles R. 4127-26 to R. 4127-27 and R. 4235-4 to R. 4235-23 of the Public Health Code.

### c. Obligation for continuous professional development

The medical biologist as a health professional must follow a continuous professional development program. This program improves the quality and safety of care, and maintains and updates knowledge.

All training or actions must be transcribed in a personal document containing the training certificates.

*For further information*: Decree No. 2016-942 of July 8, 2016 on the organization of the continuous professional development of health professionals.

4°. Insurance and sanctions
-----------------------------------------------

### a. Insurance obligation

**Obligation to take out liability insurance** Professional**

As a health professional, a medical biologist practising on a liberal basis must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

**Obligation to join an old-age or pension insurance fund**

The professional is required to:

- to join the Autonomous Pension Fund of Physicians of France (CARMF), if he practises as a doctor and is listed on the board of the College of Physicians;
- to join the Pharmacists' Old Age Insurance Fund (CAVP), if it is listed on the College of Pharmacists' table.

To find out how these procedures are, you can consult the[CAVP website](https://www.cavp.fr/) And[CARMF website](http://www.carmf.fr/).

**Taxing after Health Insurance**

Once registered on the Order of His profession, the professional practising in liberal form must declare his activity with the Primary Health Insurance Fund (CPAM).

**Terms**

Registration with the CPAM can be made online on the official website of Medicare.

**Supporting documents**

The registrant must provide a complete file including:

- Copying a valid piece of identification
- a professional bank identity statement (RIB)
- if necessary, the title (s) to allow access to Sector 2.

For more information, please refer to the section on the installation of doctors on the Health Insurance website.

### b. Sanctions

**Disciplinary sanctions**

The professional is obliged, for his inclusion on the list of the order of his profession, to communicate to the departmental council of the order under which he belongs, all contracts and endorsements relating to his profession as well as those relating to the use of the material and the premises in which he operates, if he is not the owner.

In the event of a breach of this obligation, the professional incurs one of the following disciplinary sanctions:

- A warning
- Blame
- a temporary or permanent ban on practising;
- delisting from the order chart.

*For further information*: Articles L. 6241-1; L. 4124-6 and L. 4234-6 of the Public Health Code.

**Criminal sanctions**

A professional who works as a medical biologist without being professionally qualified faces a one-year prison sentence and a fine of 15,000 euros for usurpation of title.

In addition, the illegal practice of the profession of medical biologist is punishable by a two-year prison sentence and a fine of 30,000 euros.

*For further information*: Articles L. 6242-1 and L. 6242-2 of the Public Health Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for specialization recognition

**Competent authority**

The professional must submit his application to the National Management Centre by recommended letter with notice of receipt at:

National Management Centre, Competition Department, Exercise Authorizations, Mobility and Professional Development
21 B, rue Leblanc, 75737 Paris Cedex 15

**Supporting documents**

His application must include the following documents:

- A reasoned letter of request outseating the area of specialization envisaged;
- A photocopy of a valid ID
- A detailed resume and any applicant's publications
- A copy of all his diplomas
- a certificate justifying that the national has practiced medical biology for at least two years in the last ten years and before 13 January 2012;
- for the deputy director of a national centre for communicable diseases, his phD in practice or university, or his engineering degree in connection with medical biology;
- for the doctor or pharmacist practising in a hospital or university, a document justifying that he has been practising well in such a institution for at least three years and mentioning his status.

**Time and procedure**

The National Management Centre acknowledges receipt of the application within one month of receipt and sends it to the National Medical Biology Commission. If the applicant is not answered within four months of receipt of the full file, the applicant is granted recognition.

*For further information*: Article R. 6213-2 of the Public Health Code; order of February 14, 2017 setting out the composition of the file to be provided to the National Medical Biology Commission under Article L. 6213-12 of the Public Health Code and defining the areas of specialization mentioned in Article R. 6213-1 of the same Code.

### b. Application for authorization to practice for the director or deputy director of a national communicable disease centre

**Competent authority**

The professional must apply to the National Management Centre by recommended letter with notice of receipt.

**Supporting documents**

His application must include:

- A reasoned letter of request
- A photocopy of a valid ID
- A resume describing his professional background and, if necessary, all of his publications;
- A copy of his application for permission to perform the duties of director or assistant director of laboratory;
- a photocopy of all his diplomas, titles, or certifications in the field of medical biology.

**Time and procedure**

The National Management Centre acknowledges receipt of the application within one month of receipt. Failure to respond to the Minister of Health beyond four months is the reason for the rejection of the application for leave.

*For further information*: R. 6213-5 to R. 6213-7 of the Public Health Code; ordered from 14 February 2017 above; V of Section 9 of Ordinance No. 2010-49 of January 13, 2010 relating to medical biology.

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

