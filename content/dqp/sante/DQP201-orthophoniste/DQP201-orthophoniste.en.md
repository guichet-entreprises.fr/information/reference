﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP201" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Speech therapist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="speech-therapist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/speech-therapist.html" -->
<!-- var(last-update)="2020-04-15 17:21:52" -->
<!-- var(url-name)="speech-therapist" -->
<!-- var(translation)="Auto" -->


Speech therapist
================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The speech therapist is a health professional whose activity consists of carrying out a speech-language check-in of the patient, with the aim of preventing and treating through rehabilitation all the language, communication and speech disorders that he can Present.

The professional can only exercise on a medical prescription except in case of emergency and in the absence of a doctor.

**Please note**

A professional who illegally practises the profession of speech-language pathologist faces a one-year prison sentence and a fine of 15,000 euros for usurping titles.

*For further information*: Articles L. 4341-1 and R. 4341-1 and following of the Public Health Code, Section 433-17 of the Penal Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of speech therapist the professional must:

- Be professionally qualified
- register his diploma with the regional health agency[Ars](https://www.ars.sante.fr/) ;
- to do an internship with a licensed speech therapist (see infra "2 degrees). Internship with a practitioner").

*For further information*: Article L. 4341-2 of the Public Health Code.

#### Training

To be recognized as a professionally qualified person, the individual must hold one of the following qualifications, diplomas or certificates:

- A certificate of speech-language pathologist ability
- a diploma or certificate of speech therapy studies prior to the creation of the above certificate.

**Speech-language pathologist's ability certificate**

Access to training is available through competition at speech therapy training centres. For more information, please visit the official website of universities offering speech therapy training.

Can access the training to obtain the certificate of speech-language ability, the incumbent candidates:

- A bachelor's degree
- A degree in access to university education
- French or foreign degree admitted as a bachelor's degree.

In addition, the training is also accessible to the candidate through the Experience Validation (VAE) procedure. For more information it is advisable to refer to the official website of the[Vae](http://www.vae.gouv.fr/).

The candidate can also benefit from the certification with dispensation as long as he holds:

- a certificate of fitness to teach children with hearing loss, recognized by the Minister of Health;
- a diploma as a specialist teacher for deaf children, recognized by the Minister for Education;
- a re-educator of dyslexics, recognized by one or the other of the ministers above.

The ten-semester course consists of two six- and four-semester cycles that allow the future professional to acquire the knowledge necessary for the practice of speech therapy, namely:

- during the first cycle, all the general knowledge relating to science, public health and human approach and the pathology and pathophysiology of the patient;
- during the second cycle, all practical knowledge of clinical reasoning and therapeutic intervention.

The certificate is issued to the candidate who:

- successfully completed all the tests of the two cycles and carried out the internships planned during its training;
- obtained the Certificate of Clinical Skills issued at the end of the last semester of training;
- supported a submission before a professional jury over the past six months.

*For further information*: Article L. 4341-3 of the Public Health Code; Decree No. 2013-798 of August 30, 2013 relating to the education regime for the speech-language certificate.

**Internship with a practitioner**

The future professional must, once the certificate of speech-language pathology, complete an internship with a licensed practitioner who has been practising for at least three years on a liberal basis or in a public or private health care institution.

*For further information*: Articles D. 4341-6 to D. 4341-10 of the Public Health Code.

#### Costs associated with qualification

Training for the Speech Language Ability Certificate is paid for and the cost varies depending on the institution issuing the training. For more information it is advisable to get closer to the establishment in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may engage in the same temporary and occasional activity in France without having to register with the ARS.

In order to do so, the person must justify work experience as a speech therapist for at least one year in the last ten years, when neither the profession nor the training is regulated in the Member State where he is legally established.

As soon as he fulfils these conditions, he must, before his first performance in France, make a prior declaration in order to practice his profession in France (see infra "5°. b. Request for pre-reporting for the EU national for a temporary and casual exercise (LPS)").

**Please note**

Where there are substantial differences between the national's professional training and experience and those required to carry out the speech-language pathology activity in France, the regional prefect may decide to subject him to an ordeal. adaptability or an adjustment course (see infra "5°). Good to know: compensation measures").

*For further information*: Article L. 4341-7 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any legally established EU national practising speech-language pathologists in a Member State may carry out the same activity on a permanent basis in France.

In order to do so, the person concerned must be the holder:

- a training certificate allowing it to carry out the activity of speech-language pathologist, issued by a Member State as long as that State regulates access to the profession or its exercise;
- where neither the profession nor its training is regulated in the Member State, a training document attesting to the preparation for the profession of speech-language pathologist and a certificate justifying that the professional has performed this function for at least one year in the last ten years;
- a training certificate issued by a third state and recognised in an EU Member State other than France and allowing the profession of speech-language pathologist to be legally practised, as well as a certificate justifying that the professional has carried out this activity for at least three years in that Member State or party.

As for the national wishing to practice on a temporary and casual basis, the prefect may decide to submit him to an aptitude test or an adjustment course (see infra "5°. Good to know: compensation measures").

Once the professional meets these requirements, he or she will have to apply for recognition of his professional qualifications (see below "5°). c. Application for authorisation to exercise for the European national for a permanent exercise (LE)).

*For further information*: Article L. 4341-4 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Professional rules**

The speech therapist must exercise his specialty independently and independently and respect the professional secrecy that is incumbent on the practice of his profession.

*For further information*: Articles L. 4341-1 and L. 4344-2 of the Public Health Code.

**Bans**

The health care professional or student destined for such an activity may not receive any benefit in cash or in kind, directly or indirectly from companies producing or marketing products supported by security Social.

In addition, the professional cannot collect a health professional's fees if he or she does not meet all the requirements for one of these occupations.

If there are breaches of these provisions, the professional faces a two-year prison sentence and a fine of 75,000 euros. A temporary ban on practising for up to ten years may also be imposed on a complementary basis in the event of a conviction.

*For further information*: Article L. 4113-5, L. 4113-6 and L. 4113-8 of the Public Health Code.

**Incompatibilities**

No one may work as a speech therapist if they:

- is in a medical condition that makes it dangerous to practice his profession;
- is subject to a temporary or permanent ban on practising in France or abroad.

If necessary, the Director General of the LRA may refuse the professional's registration. However, where the person is subject to such a ban in a third state in the EU or the EEA, the Director General of the ARS may authorise him to practise.

*For further information*: Article L. 4343-3 of the Public Health Code.

4°. Insurance and guarantee funds
-----------------------------------------------------

### Liability insurance

The speech therapist as a professional is required to take out insurance to cover him against the risks incurred during his activity.

**Please note**

If the professional exercises as an employee, it is up to the employer to take out such insurance for his employees for the acts carried out during their professional activity.

### Guarantee fund

In addition, the individual is required to make an annual lump sum contribution to the fund to guarantee damages resulting from preventive, diagnostic or care by health professionals. The amount of this contribution is set annually by the ministers responsible for health and the economy and varies between 15 and 25 euros per year.

*For further information*: Articles L. 1142-2 of the Public Health Code and L. 426-1 of the Insurance Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Apply for registration

**Competent authority and supporting documents**

The professional must provide the ARS with the following information:

- his marital status;
- The name and address of the institution or organization that has issued its diploma or training title to it;
- title of his training title.

In addition, the organization that has issued the training designation to the professional, forwards it to the ARS.

**Cost**

Free.

The ARS registers the professional's training title and issues a professional health card (CPS).

*For further information*: Articles D. 4343-1 and D. 4333-1 to D. 4333-6-1 of the Public Health Code.

### b. Request for pre-reporting of the EU national for a temporary and casual exercise (LPS)

**Competent authority**

The national must apply to the prefect of the region in which he wishes to perform his performance.

**Supporting documents**

The national's request must include the following documents with, if any, their translation into French:

- The declaration form attached to the schedule of the Order of 8 December 2017 referred to below, completed and signed;
- A copy of his valid ID
- A copy of his training title allowing the exercise of speech-language pathology activity within the country of obtaining;
- a certificate less than three months old certifying that the professional is legally established in that Member State and does not incur any prohibition on doing so;
- where the state does not regulate access to the profession or its exercise, a document justifying that the professional has been engaged in this activity for at least one year in the last ten years;
- where the training certificate has been issued by a third state and recognised by a Member State other than France:- Recognition of the training title by the Member State,
  - any document justifying that the professional has been active for at least three years in that state,
  - copy of the first and previous statements.

**Delays and outcome of the procedure**

The prefect informs the applicant within one month of receiving his application:

- That he can begin his service delivery;
- that he must be subject to an audit of his professional skills and, in the event of a substantial difference between his training and that required for the exercise of this activity in France, that he must submit to a compensation measure (see below " 5°. c. Good to know: compensation measures");
- that it cannot begin its service delivery.

The silence kept by the prefect beyond a one-month period is worth permission to perform the provision of services.

The prefect registers the national on a particular list and sends him a receipt with his registration number.

**Please note**

The declaration must be renewed every year under the same conditions. In the event of a change in circumstances, the national must inform the regional prefect.

*For further information*: Articles L. 4341-17, R. 4331-12 to R. 4331-15 of the Public Health Code; December 8, 2017 order on the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### c. Application for authorisation to exercise for the European national for a permanent exercise (LE)

**Competent authority**

The request must be sent in a double copy by letter recommended with notice of receipt, to the Regional Directorate of Youth, Sports and Social Cohesion (DRJSCS), assuring the secretariat of the Committee of Speech-language pathologists whose composition Section R. 4341-17 of the Public Health Code.

**Supporting documents**

The application must include the following, if any, with their translation into French:

- The[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DDB2C307150C1EBD51D563B436CD57D5.tplgfr40s_3?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180221) Completed and signed application;
- A copy of his valid ID
- A copy of his training title allowing the practice of the profession in his country of obtainment and if necessary, his additional diplomas as well as a certificate mentioning the level and detail of the training;
- any proof of professional experience, continuing training in an EU Member State or the EEA;
- any document justifying that it is not subject to any sanction;
- where the Member State does not regulate access to the profession or its exercise, a proof that the professional has been a speech therapist for at least one year in the last ten years;
- where the applicant holds a training certificate issued by a third state and recognised by a Member State, the recognition of the training title by the Member State.

**Outcome of the procedure**

The prefect of the region of the place of establishment of the professional acknowledges receipt of the file within one month of its receipt and issues the authorization of exercise after notice of the aforementioned commission. This authorisation allows the professional to obtain a professional card in order to carry out the activity of speech therapist in France under the same conditions as the French national.

**Please note**

The silence kept by the prefect after four months from the receipt of the full file is worth rejecting the application.

**Cost**

Free.

*For further information*: Articles R. 4341-13 to R. 4341-15 of the Public Health Code; decree of 25 February 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist and optician-lunetier.

**Good to know: compensation measures**

The regional prefect may, in order to verify his professional skills, subject the national to an aptitude test or an adjustment course.

The aptitude test consists of an examination that is the subject of written and oral questions on subjects not taught to the national. The adaptation course, on the other hand, must be practiced in a public or private health care institution or in a professional.

*For further information*: Article L. 4341-4 of the Public Health Code; decree of 30 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist, optician-lunetier by European Union Member States or parties to the European Economic Area Agreement.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

