﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP201" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Orthophoniste" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="orthophoniste" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/orthophoniste.html" -->
<!-- var(last-update)="2020-04-15 17:21:52" -->
<!-- var(url-name)="orthophoniste" -->
<!-- var(translation)="None" -->

# Orthophoniste

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:52<!-- end-var -->

## 1°. Définition de l’activité

L'orthophoniste est un professionnel de la santé dont l'activité consiste à réaliser un bilan orthophonique du patient, dans le but de prévenir et traiter par une rééducation, l'ensemble des troubles du langage, de la communication et de la parole qu'il peut présenter.

Le professionnel ne peut exercer que sur prescription médicale hormis en cas d'urgence et en l'absence de médecin.

**À noter** 

Le professionnel qui exerce illégalement la profession d'orthophoniste encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende au titre d'usurpation de titres.

*Pour aller plus loin* : articles L. 4341-1 et R. 4341-1 et suivants du Code de la santé publique, article 433-17 du Code pénal.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité d'orthophoniste le professionnel doit :

- être qualifié professionnellement ;
- procéder à l'enregistrement de son diplôme auprès de l'agence régionale de santé [ARS](https://www.ars.sante.fr/) ;
- effectuer un stage auprès d'un orthophoniste agréé (cf. infra « 2°. Stage auprès d'un praticien »).

*Pour aller plus loin* : article L. 4341-2 du Code de la santé publique.

#### Formation

Pour être reconnu comme étant qualifié professionnellement, l'intéressé doit être titulaire de l'un des titres, diplômes ou certificats suivants :

- un certificat de capacité d'orthophoniste ;
- un diplôme ou attestation d'études d'orthophonie antérieur à la création du certificat susvisé.

##### Certificat de capacité d'orthophoniste

L'accès à la formation se fait sur concours auprès des centres de formation en orthophonie. Pour plus d'informations, il est conseillé de se reporter au site officiel des universités proposant une formation en orthophonie.

Peuvent accéder à la formation en vue d'obtenir le certificat de capacité d'orthophoniste, les candidats titulaires :

- d'un baccalauréat ;
- d'un diplôme d'accès aux études universitaires ;
- d'un diplôme français ou étranger admis en tant que dispense du baccalauréat.

En outre, la formation est également accessible au candidat par la procédure de Validation des acquis de l'expérience (VAE). Pour plus d'informations il est conseillé de se reporter au site officiel de la [VAE](http://www.vae.gouv.fr/).

Le candidat peut également bénéficier de la certification avec dispense dès lors qu'il est titulaire :

- d'un certificat d'aptitude à l'enseignement des enfants atteints de déficience auditive, reconnu par le ministre chargé de la santé ;
- d'un diplôme d'instituteur spécialisé pour les enfant sourds, reconnu par le ministre chargé de l'éducation ;
- d'un titre de rééducateur des dyslexiques, reconnu par l'un ou l'autre des ministres susvisés.

La formation d'une durée de dix semestres se compose de deux cycles de six et quatre semestres permettant au futur professionnel d'acquérir les connaissances nécessaires à l'exercice de l'orthophonie, à savoir :

- au cours du premier cycle, l'ensemble des connaissances générales relatives aux sciences, la santé publique et à l'approche de l'être humain et la pathologie et physiopathologie du patient ;
- au cours du second cycle, l'ensemble des connaissances pratiques relatives au raisonnement clinique et à l'intervention thérapeutique.

Le certificat est délivré au candidat ayant :

- accompli avec succès l'ensemble des épreuves des deux cycles et effectué les stages prévus au cours de sa formation ;
- obtenu le certificat de compétences cliniques délivré à l'issue du dernier semestre de formation ;
- soutenu un mémoire devant un jury professionnel au cours du dernier semestre.

*Pour aller plus loin* : article L. 4341-3 du Code de la santé publique ; décret n° 2013-798 du 30 août 2013 relatif au régime des études en vue du certificat de capacité orthophoniste.

##### Stage auprès d'un praticien

Le futur professionnel doit, une fois titulaire du certificat de capacité d'orthophoniste, effectuer un stage auprès d'un praticien agréé et exerçant depuis au moins trois ans à titre libéral ou au sein d'un établissement de santé public ou privé.

*Pour aller plus loin* : articles D. 4341-6 à D. 4341-10 du Code de la santé publique.

#### Coûts associés à la qualification

La formation menant au certificat de capacité d'orthophoniste est payante et son coût varie selon l'établissement délivrant la formation. Pour plus d'informations il est conseillé de se rapprocher de l'établissement considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité d'orthophoniste peut exercer à titre temporaire et occasionnel la même activité en France sans avoir à procéder à son enregistrement auprès de l'ARS.

Pour cela, l'intéressé doit justifier d'une expérience professionnelle en qualité d'orthophoniste d'au moins un an au cours des dix dernières années, lorsque ni la profession ni la formation ne sont réglementées dans l’État membre où il est légalement établi.

Dès lors qu'il remplit ces conditions il doit, avant sa première prestation en France, effectuer une déclaration préalable en vue d'exercer sa profession en France (cf. infra « 5°. b. Demande de déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

**À noter** 

Lorsque des différences substantielles existent entre la formation et l'expérience professionnelles du ressortissant et celles exigées pour exercer l'activité d'orthophoniste en France, le préfet de région pourra décider de le soumettre à une épreuve d'aptitude ou un stage d'adaptation (cf. infra « 5°. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : article L. 4341-7 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant de l'UE légalement établi et exerçant l'activité d'orthophoniste dans un État membre, peut exercer la même activité à titre permanent en France.

Pour cela, l'intéressé doit être titulaire :

- d'un titre de formation lui permettant d'exercer l'activité d'orthophoniste, délivré par un État membre dès lors que cet État réglemente l'accès à la profession ou son exercice ;
- lorsque ni la profession ni sa formation ne sont réglementées dans l’État membre, un titre de formation attestant de la préparation à l'exercice de la profession d'orthophoniste ainsi qu'une attestation justifiant que le professionnel a exercé cette fonction pendant au moins un an au cours des dix dernières années ;
- d'un titre de formation délivré par un État tiers et reconnu dans un État membre de l'UE autre que la France et permettant d'exercer légalement la profession d'orthophoniste, ainsi que d'une attestation justifiant que le professionnel à exercé cette activité pendant au moins trois ans dans cet État membre ou partie.

De même que pour le ressortissant souhaitant exercer à titre temporaire et occasionnel, le préfet pourra décider de le soumettre à une épreuve d'aptitude ou un stage d'adaptation (cf. infra « 5°. Bon à savoir : mesures de compensation »).

Dès lors qu'il remplit ces conditions, le professionnel devra effectuer une demande de reconnaissance de ses qualifications professionnelles (cf. infra « 5°. c. Demande d'autorisation d'exercice pour le ressortissant européen en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : article L. 4341-4 du Code de la santé publique.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Règles professionnelles

L'orthophoniste doit exercer sa spécialité en toute indépendance et autonomie et respecter le secret professionnel qui incombe à l'exercice de sa profession.

*Pour aller plus loin* : articles L. 4341-1 et L. 4344-2 du Code de la santé publique.

### Interdictions

Le professionnel de la santé ou l'étudiant se destinant à une telle activité ne peut recevoir d'avantage en espèces ou en nature, de manière directe ou indirecte de la part d'entreprises produisant ou commercialisant des produits pris en charge par la sécurité sociale.

En outre, le professionnel ne peut percevoir les honoraires d'un professionnel de santé s'il ne remplit pas l'ensemble des conditions requises à l'exercice de l'une de ces professions.

En cas de manquements à ces dispositions, le professionnel encourt une peine de deux ans d'emprisonnement et 75 000 euros d'amende. Une interdiction temporaire d'exercice pendant une période maximale de dix ans peut également être prononcée à titre complémentaire en cas de condamnation.

*Pour aller plus loin* : article L. 4113-5, L. 4113-6 et L. 4113-8 du Code de la santé publique.

### Incompatibilités

Nul ne peut exercer l'activité d'orthophoniste s'il :

- se trouve dans un état pathologique rendant dangereux l'exercice de sa profession ;
- fait l'objet d'une interdiction temporaire ou définitive d'exercer en France ou à l'étranger.

Le cas échéant le directeur général de l'ARS pourra refuser l'inscription du professionnel. Toutefois, lorsque l'intéressé fait l'objet d'une telle interdiction dans un État tiers à l'UE ou l'EEE, le directeur général de l'ARS peut l'autoriser à exercer.

*Pour aller plus loin* : article L. 4343-3 du Code de la santé publique.

## 4°. Assurances et fonds de garantie

### Assurance de responsabilité civile

L'orthophoniste en qualité de professionnel est tenu de souscrire une assurance permettant de le couvrir contre les risques encourus lors de son activité.

**À noter** 

Si le professionnel exerce en tant que salarié, c'est à l'employeur de souscrire une telle assurance pour ses salariés pour les actes effectués à l'occasion de leur activité professionnelle.

### Fonds de garantie

En outre, l'intéressé est tenu de verser une contribution forfaitaire annuelle au fonds de garantie des dommages consécutifs à des actes de prévention, de diagnostic ou de soins dispensés par des professionnels de santé. Le montant de cette contribution est fixé chaque année par les ministres chargés de la santé et de l'économie et varie entre 15 et 25 euros par an.

*Pour aller plus loin* : articles L. 1142-2 du Code de la santé publique et L. 426-1 du Code des assurances.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une demande d'enregistrement

#### Autorité compétente et pièces justificatives

Le professionnel doit transmettre à l'ARS les informations suivantes :

- son état civil ;
- le nom et adresse de l'établissement ou organisme lui ayant délivré son diplôme ou titre de formation ;
- l'intitulé de son titre de formation.

En outre, l'organisme ayant délivré le titre de formation au professionnel, le transmet à l'ARS.

#### Coût

Gratuit.

L'ARS procède à l'enregistrement du titre de formation du professionnel et lui délivre une carte professionnelle de santé (CPS).

*Pour aller plus loin* : articles D. 4343-1 et D. 4333-1 à D. 4333-6-1 du Code de la santé publique.

### b. Demande de déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser une demande au préfet de la région au sein de laquelle il souhaite exercer sa prestation.

#### Pièces justificatives

La demande du ressortissant doit comprendre les documents suivants assortis, le cas échéant, de leur traduction en français :

- le formulaire de déclaration fixé à l'annexe de l'arrêté du 8 décembre 2017 visé ci-après, complété et signé ;
- une copie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation permettant l'exercice de l'activité d'orthophoniste au sein du pays d'obtention ;
- une attestation datant de moins de trois mois certifiant que le professionnel est légalement établi dans cet État membre et n'encourt aucune interdiction d'exercer cette activité ;
- lorsque l’État ne réglemente ni l'accès à la profession ni son exercice, un document justifiant que le professionnel a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu par un État membre autre que la France :
  - la reconnaissance du titre de formation par l’État membre,
  - tout document justifiant que le professionnel a exercé l'activité pendant au moins trois ans au sein de cet État,
  - le cas échéant, la copie de la première et de la déclaration précédente.

#### Délais et issue de la procédure

Le préfet informe le demandeur dans un délai d'un mois à compter de la réception de sa demande soit :

- qu'il peut débuter sa prestation de services ;
- qu'il doit faire l'objet d'une vérification de ses compétences professionnelles et, en cas de différence substantielle entre sa formation et celle requise pour l'exercice de cette activité en France, qu'il doit se soumettre à une mesure de compensation (cf. infra « 5°. c. Bon à savoir : mesures de compensation ») ;
- qu'il ne peut débuter sa prestation de services.

Le silence gardé par le préfet au delà d'un délai d'un mois vaut autorisation d'exercer la prestation de services.

Le préfet enregistre le ressortissant sur une liste particulière et lui adresse un récépissé comportant son numéro d'enregistrement.

**À noter**

La déclaration doit être renouvelée tous les ans dans les mêmes conditions. En cas de changement de situation le ressortissant doit en informer le préfet de région.

*Pour aller plus loin* : articles L. 4341-17, R. 4331-12 à R. 4331-15 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### c. Demande d'autorisation d'exercice pour le ressortissant européen en vue d'un exercice permanent (LE)

#### Autorité compétente

La demande doit être adressée en double exemplaire par lettre recommandée avec avis de réception, à la direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS), assurant le secrétariat de la commission des orthophonistes dont la composition est fixée à l'article R. 4341-17 du Code de la santé publique.

#### Pièces justificatives

La demande doit comporter les éléments suivants le cas échéant assortis de leur traduction en français :

- le [formulaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DDB2C307150C1EBD51D563B436CD57D5.tplgfr40s_3?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180221) de demande complété et signé ;
- une copie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation permettant l'exercice de la profession dans son pays d'obtention et le cas échéant, ses diplômes complémentaires ainsi qu'une attestation mentionnant le niveau et le détail de la formation ;
- tout justificatif de son expérience professionnelle, des formations continues suivies dans un État membre de l'UE ou l'EEE ;
- tout document justifiant qu'il ne fait l'objet d'aucune sanction ;
- lorsque l’État membre ne réglemente ni l'accès à la profession ni son exercice, un justificatif que le professionnel a exercé l'activité d'orthophoniste pendant au moins un an au cours des dix dernières années ;
- lorsque le demandeur est titulaire d'un titre de formation délivré par un État tiers et reconnu par un État membre, la reconnaissance du titre de formation par ce dernier.

#### Issue de la procédure

Le préfet de la région du lieu d'établissement du professionnel accuse réception du dossier dans un délai d'un mois à compter de sa réception et délivre l'autorisation d'exercice après avis de la commission susvisée. Cette autorisation permet au professionnel d'obtenir une carte professionnelle en vue d'exercer l'activité d'orthophoniste en France dans les mêmes conditions que le ressortissant français.

**À noter**

Le silence gardé par le préfet au delà de quatre mois à compter de la réception du dossier complet, vaut rejet de la demande.

#### Coût

Gratuit.

*Pour aller plus loin* : articles R. 4341-13 à R. 4341-15 du Code de la santé publique ; arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste et opticien-lunetier.

#### Bon à savoir : mesures de compensation

Le préfet de région peut, en vue de vérifier ses compétences professionnelles, soumettre le ressortissant à une épreuve d'aptitude ou un stage d'adaptation.

L'épreuve d'aptitude consiste en un examen faisant l'objet d'interrogations écrites et orales portant sur les matières non enseignées au ressortissant. Le stage d'adaptation quant à lui, doit être pratiqué au sein d'un établissement de santé public ou privé ou chez un professionnel.

*Pour aller plus loin* : article L. 4341-4 du Code de la santé publique ; arrêté du 30 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste, opticien-lunetier par des ressortissants des États membres de l'Union européenne ou parties à l'accord sur l'Espace économique européen.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).