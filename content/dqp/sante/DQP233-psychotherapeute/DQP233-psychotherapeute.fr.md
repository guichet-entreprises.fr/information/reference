﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP233" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Psychothérapeute" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="psychotherapeute" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/psychotherapeute.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="psychotherapeute" -->
<!-- var(translation)="None" -->

# Psychothérapeute

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le psychothérapeute est un professionnel dont l'activité consiste à analyser l'ensemble des troubles sociaux, psychologiques et psychosomatiques de ses patients en vue de les traiter et d'améliorer leur bien-être.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour pouvoir exercer, le psychothérapeute doit être :

- titulaire du titre de psychothérapeute ;
- inscrit sur le registre national des psychothérapeutes.

#### Formation

##### Titre de psychothérapeute

Pour pouvoir exercer et être inscrit au registre national des psychothérapeutes, le professionnel doit :

- être titulaire de l'un des diplômes suivants :
  - un diplôme d’État de médecin de niveau doctorat,
  - un diplôme de niveau master spécialisé en psychologie ou en psychanalyse ;
- suivre une formation en psychopathologie clinique d'au moins 400 heures, en vue d'acquérir les connaissances relatives aux :
  - développement, au fonctionnement et au processus psychiques,
  - critères de discernement des grandes pathologies psychiatriques,
  - principales approches utilisée au sein de la profession.

En plus de cette formation, le futur professionnel est tenu de suivre un stage pratique de cinq mois minimum à temps plein, partiel ou fractionné, au sein d'un établissement public ou privé accrédité.

**À noter**

Peut également être reconnu comme étant qualifié professionnellement, le professionnel qui justifie avoir exercé la psychothérapie pendant au moins cinq ans à compter de la publication du décret du 20 mai 2010. Pour cela, il doit adresser un [formulaire de demande d'autorisation d'inscription](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DD83A8F3F417C19CE560EA85D606ADF6.tplgfr33s_1?idArticle=LEGIARTI000022338740&cidTexte=LEGITEXT000022338729&dateTexte=20180413) complété et signé au directeur de l'ARS.

Une fois titulaire de son titre, le professionnel peut faire procéder à son enregistrement au registre national des psychothérapeutes (cf. infra « 5°. a. Inscription au registre national des psychothérapeutes »).

*Pour aller plus loin* : décret n° 2010-534 du 20 mai 2010 relatif à l'usage du titre de psychothérapeute ; arrêté du 9 juin 2010 relatif aux demandes d'inscription au registre national des psychothérapeutes.

#### Coûts associés à la qualification

La formation menant au titre de psychothérapeute est payante et son coût varie selon le cursus envisagé. Il est conseillé de se renseigner auprès des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de psychothérapeute peut exercer, à titre temporaire et occasionnel, la même activité en France.

Lorsque l’État membre ne réglemente ni l'accès à la formation, ni son exercice, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, il est tenu d'effectuer une déclaration préalable auprès de l'agence régionale de santé (ARS) avant de débuter sa prestation de services.

En outre, le professionnel doit justifier être titulaire des connaissances linguistiques nécessaires à l'exercice de sa profession en France.

*Pour aller plus loin* : article 52 de la loi n° 2004-806 du 9 août 2004 relative à la politique de santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, légalement établi et exerçant l'activité de psychothérapeute, peut exercer à titre permanent, la même activité en France.

Pour cela, il doit être titulaire :

- d'un titre de formation délivré par un État membre qui réglemente l'accès et l'exercice de cette profession ;
- lorsque l’État membre ne réglemente ni l'accès à la profession, ni son exercice, d'un titre de formation délivré par un État membre attestant de sa préparation à l'exercice de la profession ainsi qu'une attestation justifiant qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- d'un titre de formation délivré par un État tiers mais reconnu par un État membre de l'UE permettant d'exercer cette activité. En outre, il doit justifier avoir exercé cette activité pendant au moins trois ans.

Dès lors qu'il remplit ces conditions, le ressortissant doit effectuer une demande en vue d'obtenir une autorisation d'user de son titre professionnel (cf. infra « 5°. c. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent »).

En outre, le professionnel doit justifier être titulaire des connaissances linguistiques nécessaires pour exercer son activité en France.

*Pour aller plus loin* : article 52 de la loi n° 2004-806 du 9 août 2004 relative à la politique de santé publique.

## 3°. Règles déontologiques

### Code de déontologie

Les règles déontologiques applicables à la profession de psychothérapeute ne sont pas réglementées. Toutefois, les professionnels doivent suivre la charte mondiale de déontologie des membres de cette profession.

À ce titre, le professionnel s'engage notamment à respecter :

- le droit à l'information, au respect de la vie privée et à la dignité de leurs patients, 
- le secret professionnel.

Vous pouvez consulter la rubrique Code de déontologie, sur le [site de la Fédération française de psychothérapie et psychanalyse](http://www.ff2p.fr/fichiers_site/la_ff2p/la_ff2p.html#code) (FF2P).

## 4°. Assurances

Le psychothérapeute en qualité de professionnel est tenu de souscrire une assurance permettant de le couvrir contre les risques encourus lors de son activité.

**À noter**

Si le professionnel exerce en tant que salarié, c'est à l'employeur de souscrire une telle assurance pour ses salariés, pour les actes effectués à l'occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Inscription au registre national des psychothérapeutes

#### Autorité compétente

Le professionnel doit, en vue de son inscription, adresser sa demande auprès de l'agence régionale de santé (ARS) de la région où se trouve sa résidence professionnelle principale.

#### Pièces justificatives

Sa demande doit comporter :

- une copie de sa pièce d'identité ;
- une attestation justifiant de l'obtention de son diplôme d’État de médecin ou d'un diplôme de niveau master ;
- une attestation de suivi de la formation en psychopathologie clinique ;
- le cas échéant, l'attestation d'enregistrement pour les professionnels de la médecine ;
- le professionnel titulaire d'un doctorat en médecine, d'un titre de psychologue ou d'un titre de psychanalyste doivent en outre fournir soit :
  - une attestation de l'obtention de son diplôme,
  - une attestation de son enregistrement dans un annuaire d'association de psychanalystes.

#### Délai et procédure

Le directeur de l'ARS accuse réception de sa demande dès la réception de son dossier. L'absence de réponse au delà d'un délai de deux mois vaut rejet de sa demande.

#### Coût

Gratuit

**À noter**

Si le professionnel exerce son activité au sein de plusieurs sites, il doit déclarer l'ensemble des adresses concernées.

*Pour aller plus loin* : articles 7 à 9 du décret du 20 mai 2010.

### b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant adresse sa demande au directeur général de l'ARS de la région au sein de laquelle il souhaite effectuer sa prestation de services.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant, assortis de leur traduction, par un traducteur agréé en français :

- le formulaire de déclaration préalable fixé à l'annexe 6 de la circulaire du 24 décembre 2012 ;
- une photocopie de sa pièce d'identité ainsi qu'un document attestant de sa nationalité, si sa pièce d'identité ne la mentionne pas ;
- une photocopie de l'ensemble de ses titres de formation ;
- une attestation de l'autorité compétente de l’État membre, certifiant que le ressortissant est légalement établi pour exercer l'activité de psychothérapeute et qu'il n'encourt aucune interdiction d'exercice.

#### Délai et procédure

Le directeur général accuse réception de la demande du ressortissant et l'informe dans un délai d'un mois soit :

- qu'il peut débuter sa prestation de services ;
- qu'il ne peut débuter sa prestation de services ;
- qu'il doit faire l'objet d'une vérification de ses compétences et le cas échéant, se soumettre à une épreuve d'aptitude dès lors que l'examen de ses qualifications fait apparaître une différence substantielle entre sa formation et celle requise pour l'exercice de la profession en France.

**À noter**

Cette déclaration doit être renouvelée tous les ans, dans les mêmes conditions.

*Pour aller plus loin* : annexes 2 et 3 de la circulaire n° DGOS/RH2/2012/431 du 24 décembre 2012 relative aux conditions d’usage du titre de psychothérapeute par les titulaires de diplômes délivrés par des États membres de l’Union européenne, de l’Espace économique européen et de la Confédération suisse.

### c. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant adresse sa demande au directeur général de l'ARS de la région au sein de laquelle le professionnel souhaite établir sa résidence professionnelle.

#### Pièces justificatives

Sa demande doit comporter :

- le formulaire de demande d'autorisation d'user du titre de psychothérapeute, dont le modèle est fixé à l'annexe 3 de la circulaire du 24 décembre 2012 ;
- une photocopie de sa pièce d'identité en cours de validité ;
- une copie de l'ensemble de ses diplômes, certificats ou titres obtenus ;
- toute pièce justifiant du suivi d'une formation continue, d'expérience ou de compétences acquises au sein d'un État membre ;
- une déclaration de moins d'un an, de l'autorité compétente de l’État membre certifiant qu'il ne fait l'objet d'aucune sanction ;
- une copie des attestations délivrées par les États membres spécifiant le niveau de formation ainsi que le volume horaire des enseignements suivis ;
- lorsque l’État ne réglemente ni l'accès à la profession, ni son exercice, toute pièce justifiant qu'il a exercé l'activité de psychothérapeute pendant au moins un an au cours des dix dernières années ;
- lorsque le professionnel a acquis sa qualification dans un État tiers mais reconnu par un État membre de l'UE, l'attestation de reconnaissance du titre de formation ainsi que toute pièce justifiant qu'il a exercé cette activité pendant au moins trois ans dans cet État membre.

#### Délai et procédure

Le directeur général accuse réception de la demande dans un délai d'un mois à compter de sa réception. En l'absence de réponse au delà d'un délai de quatre mois à compter de la réception de la demande, la demande est réputée refusée.

*Pour aller plus loin* : article 14 du décret n° 2017-1520 du 2 novembre 2017 relatif à la reconnaissance des qualifications professionnelles dans le domaine de la santé.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel et celle requise pour exercer la profession de psychothérapeute, le directeur général peut décider de le soumettre soit à une épreuve d'aptitude prenant la forme d'un examen écrit réalisé dans les six mois suivant la décision, soit à un stage d'adaptation.

Ce dernier doit être effectué au sein d'un établissement de santé, un établissement social ou un établissement médico-social sous la responsabilité d'un professionnel.

*Pour aller plus loin* : circulaire n° DGOS/RH2/2012/431 du 24 décembre 2012 relative aux conditions d’usage du titre de psychothérapeute par les titulaires de diplômes délivrés par des États membres de l’Union européenne, de l’Espace économique européen et de la Confédération suisse.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.
 
##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).