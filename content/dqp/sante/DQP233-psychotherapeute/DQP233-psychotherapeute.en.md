﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP233" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Psychotherapist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="psychotherapist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/psychotherapist.html" -->
<!-- var(last-update)="2020-04-15 17:22:05" -->
<!-- var(url-name)="psychotherapist" -->
<!-- var(translation)="Auto" -->


Psychotherapist
===============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:05<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The psychotherapist is a professional whose activity consists of analyzing all the social, psychological and psychosomatic disorders of his patients in order to treat them and improve their well-being.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to practice, the psychotherapist must be:

- Holder of the title of psychotherapist;
- registered on the National Registry of Psychotherapists.

#### Training

**Title of psychotherapist**

In order to practice and be registered in the National Register of Psychotherapists, the professional must:

- hold one of the following degrees:- a state-of-the-art doctoral doctor' degree,
  - a master's degree specializing in psychology or psychoanalysis;
- undergo at least 400 hours of clinical psychopathology training to gain knowledge of:- development, mental functioning and processes,
  - criteria for discernment of major psychiatric pathologies,
  - main approaches used within the profession.

In addition to this training, the future professional is required to take a practical internship of at least five months full-time, part-time or split, in a public or private accredited institution.

**Please note**

The professional who justifies having exercised psychotherapy for at least five years from the publication of the decree of 20 May 2010 may 20 can also be recognized as a professionally qualified professional. In order to do so, it must address a[application form for registration authorization](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DD83A8F3F417C19CE560EA85D606ADF6.tplgfr33s_1?idArticle=LEGIARTI000022338740&cidTexte=LEGITEXT000022338729&dateTexte=20180413) completed and signed to the director of the LRA.

Once the professional holds his title, he can have his registration made to the national register of psychotherapists (see infra "5°. a. Registration in the National Registry of Psychotherapists").

*For further information*: Decree No. 2010-534 of May 20, 2010 on the use of the title of psychotherapist; Order of 9 June 2010 relating to applications for registration in the National Register of Psychotherapists.

#### Costs associated with qualification

Training for the title of psychotherapist is paid for and the cost varies according to the course envisaged. It is advisable to check with the institutions concerned for more information.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a European Union (EU) state or a state party to the legally established European Economic Area (EEA) agreement may perform the same activity in France on a temporary and casual basis. .

Where the Member State does not regulate access to training or its exercise, the national must justify having carried out this activity for at least one year in the last ten years.

Once it fulfils these conditions, it is required to make a prior declaration with the regional health agency (ARS) before beginning its service delivery.

In addition, the professional must justify having the language skills necessary to practice his profession in France.

*For further information*: Article 52 of Public Health Policy Act 2004-806 of 9 August 2004.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, legally established and practising as a psychotherapist, may carry out the same activity in France on a permanent basis.

To do so, he must be the holder:

- a training certificate issued by a Member State that regulates access and practice of that profession;
- where the Member State does not regulate access to or exercise of a training document issued by a Member State attesting to its preparation for the practice of the profession, as well as a certificate justifying that it has carried out this activity during the less than a year in the last ten years;
- a training certificate issued by a third state but recognised by an EU Member State to carry out this activity. In addition, he must justify having been engaged in this activity for at least three years.

Once the national meets these conditions, he must apply for permission to use his professional title (see infra "5°. c. Application for authorization of practice for the national for a permanent exercise").

In addition, the professional must justify having the necessary language skills to carry out his activity in France.

*For further information*: Article 52 of Public Health Policy Act 2004-806 of 9 August 2004.

3°.Ethical rules
----------------------------

**Code of Ethics**

The ethical rules applicable to the profession of psychotherapist are not regulated. However, professionals must follow the global charter of ethics of members of this profession.

As such, the professional is committed to:

- the right to information, privacy and the dignity of their patients,
- professional secrecy.

You can see the code of ethics section, on the[website of the French Federation of Psychotherapy and Psychoanalysis](http://www.ff2p.fr/fichiers_site/la_ff2p/la_ff2p.html#code) (FF2P).

4°. Insurance
---------------------------------

The psychotherapist as a professional is required to take out insurance to cover him against the risks incurred during his activity.

**Please note**

If the professional exercises as an employee, it is up to the employer to take out such insurance for his employees, for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Enrollment in the National Registry of Psychotherapists

**Competent authority**

The professional must apply to the regional health agency (ARS) in the region where his main professional residence is located for registration.

**Supporting documents**

His application must include:

- A copy of his ID
- a certificate justifying obtaining a state diploma as a doctor or a master's degree;
- a certificate of follow-up training in clinical psychopathology;
- If necessary, the registration certificate for medical professionals;
- the professional with a doctorate in medicine, a title of psychologist or a psychoanalyst designation must also provide either:- a certificate of graduation,
  - a certificate of its registration in an association directory of psychoanalysts.

**Time and procedure**

The Director of the LRA acknowledges receipt of his request as soon as his file is received. Failure to respond beyond two months is a case for rejecting his application.

**Cost**

Free

**Please note**

If the professional operates within several sites, he must declare all the addresses concerned.

*For further information*: Articles 7 to 9 of the May 20, 2010 decree.

### b. Pre-declaration for the national for a temporary and casual exercise (LPS)

**Competent authority**

The national sends his application to the Director General of the LRA in the region in which he wishes to carry out his service.

**Supporting documents**

Its application must include the following documents, if any, with their translation, by a certified translator in French:

- The pre-reporting form set out in Schedule 6 of the circular of 24 December 2012;
- A photocopy of his ID and a document attesting to his nationality, if his ID does not mention it;
- A photocopy of all of his training credentials;
- a certificate from the competent authority of the Member State, certifying that the national is legally established to carry out the activity of psychotherapist and that he is not prohibited from practising.

**Time and procedure**

The Director General acknowledges receipt of the national's request and informs him within one month:

- That he can begin his service delivery;
- That he cannot begin his service delivery;
- that he must be subject to a competency check and, if necessary, submit to an aptitude test if the examination of his qualifications shows a substantial difference between his training and that required for the exercise of profession in France.

**Please note**

This declaration must be renewed every year, under the same conditions.

*For further information*: Appendix 2 and 3 of Circular DGOS/RH2/2012/431 of 24 December 2012 on the conditions for the use of the psychotherapist designation by holders of diplomas issued by Member States of the European Union, the European Economic Area and the Swiss Confederation.

### c. Application for authorization of exercise for the national for a permanent exercise (LE)

**Competent authority**

The national sends his application to the director general of the LRA in the region in which the professional wishes to establish his professional residence.

**Supporting documents**

His application must include:

- The application form for authorisation to use the title of psychotherapist, the model of which is set out in Schedule 3 of the circular of 24 December 2012;
- A photocopy of his valid ID
- A copy of all of his diplomas, certificates or titles obtained;
- any evidence justifying the follow-up of continuing education, experience or skills acquired within a Member State;
- a declaration of less than one year from the competent authority of the Member State certifying that it is not subject to any sanction;
- A copy of the certificates issued by the Member States specifying the level of training and the hourly volume of the lessons taken;
- where the state does not regulate access to the profession or its exercise, any evidence to justify that it has been a psychotherapist for at least one year in the last ten years;
- where the professional has acquired his qualification in a third state but recognised by an EU Member State, the certificate of recognition of the training certificate as well as any evidence justifying that he has carried out this activity for at least three years in that state Member.

**Time and procedure**

The General Manager acknowledges receipt of the request within one month of receipt. If the application is not answered beyond four months of receipt, the application is deemed to be denied.

*For further information*: Article 14 of Decree No. 2017-1520 of November 2, 2017 on the recognition of professional qualifications in the health field.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the professional and that required to practise as a psychotherapist, the Director General may decide to submit it to either an aptitude test in the form of a exam completed within six months of the decision, i.e. at an accommodation course.

The latter must be carried out in a health facility, a social institution or a medical-social institution under the responsibility of a professional.

*For further information*: Circular DGOS/RH2/2012/431 of 24 December 2012 on the conditions for the use of the psychotherapist designation by holders of diplomas issued by Member States of the European Union, the European Economic Area and the Swiss Confederation.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

