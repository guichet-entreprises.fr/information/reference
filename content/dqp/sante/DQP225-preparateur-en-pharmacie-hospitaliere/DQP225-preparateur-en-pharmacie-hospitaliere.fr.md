﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP225" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Préparateur en pharmacie hospitalière" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="preparateur-en-pharmacie-hospitaliere" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/preparateur-en-pharmacie-hospitaliere.html" -->
<!-- var(last-update)="2020-04-15 17:22:02" -->
<!-- var(url-name)="preparateur-en-pharmacie-hospitaliere" -->
<!-- var(translation)="None" -->

# Préparateur en pharmacie hospitalière

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:02<!-- end-var -->

## 1°. Définition de l'activité

Le préparateur en pharmacie hospitalière est un professionnel de santé qui travaille au sein d'un hôpital, sous la responsabilité et le contrôle d'un pharmacien. Ses missions sont de préparer et de distribuer les médicaments aux patients, ainsi que de gérer les stocks de produits et de matériels.

Il est également amené à préparer les dispositifs médicaux stériles.

*Pour aller plus loin* : article L. 4241-13 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession de préparateur en pharmacie hospitalière est réservée aux titulaires du diplôme de préparateur en pharmacie hospitalière.

*Pour aller plus loin* : article L. 4241-13 du Code de la santé publique.

#### Formation

La formation menant au diplôme de préparateur en pharmacie hospitalière est accessible aux personnes ayant obtenu le brevet professionnel de préparateur en pharmacie.

Le diplôme peut quant à lui être obtenu par la voie :

- d'une formation initiale ;
- de l'apprentissage ;
- d'une formation professionnelle continue ;
- de la validation des acquis de l'expérience.

L'accès à la formation initiale et à la formation professionnelle continue n'est ouverte qu'aux candidats ayant réussi des épreuves de sélection, organisées annuellement pas des centres de formation. La sélection est constituée d'une première épreuve écrite d'admissibilité notée sur 20, puis d'une épreuve orale d'admission de trente minutes, notée également sur 20. Le candidat ayant obtenu la moyenne aux deux épreuves d'affilée pourra intégrer la formation à la préparation en pharmacie hospitalière.

Celle-ci se déroule sur 42 semaines comprenant 660 heures d'enseignement théorique et 700 heures de stage. Une évaluation des connaissances de l'élève est organisée tout au long de la formation. La validation de l'ensemble des compétences permettra à l'élève d'être reçu au diplôme.

*Pour aller plus loin* : arrêté du 2 août 2006 relatif à la formation conduisant au diplôme de préparateur en pharmacie hospitalière.

#### Coûts associés à la qualification

La formation est payante. Pour plus d'informations, il est conseillé de se rapprocher des établissements la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE) exerçant légalement l’activité de préparateur en pharmacie hospitalière dans l’un de ces États peut faire usage de son titre professionnel en France à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet de la région dans laquelle il souhaite faire la prestation (cf. infra « 4°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États de l'UE ou de l'EEE pendant au moins un an, au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : article L. 4241-16 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité de préparateur en pharmacie hospitalière dans cet État, peut exercer la même activité en France de manière permanente s'il :

- est titulaire d'un titre de formation délivré par une autorité compétente d'un autre État de l'UE ou de l'EEE, qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession à temps plein ou à temps partiel, pendant deux ans au cours des dix dernières années dans un autre État de l'UE ou de l'EEE qui ne réglemente ni la formation, ni l'exercice de la profession ;
- est titulaire d'un titre délivré par une autorité compétente d'un État tiers et reconnu par un État de l'UE ou de l'EEE, et qu'il justifie avoir exercé l'activité la profession pendant au moins trois ans dans l’État de l'UE ou de l'EEE.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une autorisation individuelle d'exercice auprès du préfet de la région dans laquelle il souhaite exercer sa profession (cf. infra « 4°. b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

*Pour aller plus loin* : article L. 4241-14 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Bien que non codifiées, des règles s'imposent aux préparateurs en pharmacie hospitalière, notamment, de respecter le secret professionnel et de veiller au bien-être des patients.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet de la région est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de la profession dans l’État d'obtention ;
- une attestation, datant de moins de trois mois, de l'autorité compétente de l’État de l'UE ou de l'EEE, certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- toutes pièces justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État de l'UE ou de l'EEE autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l’État ayant reconnu ce titre,
  - toutes pièces justifiant que le ressortissant a exercé la profession dans cet État pendant trois ans ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée ;
- une attestation de responsabilité civile professionnelle.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

À réception du dossier, le préfet de la région disposera d'un délai d'un mois pour se prononcer sur la demande et informera le ressortissant :

- qu'il peut débuter la prestation ;
- qu'il sera soumis à une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France ;
- qu'il ne pourra pas débuter la prestation ;
- de toute difficulté pouvant retarder sa décision. Dans ce dernier cas, le préfet pourra rendre sa décision dans les deux mois suivant la résolution de cette difficulté, et au plus tard dans les trois mois de sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaudra acceptation de la demande de déclaration.

**À noter **

La déclaration est renouvelable tous les ans ou à chaque changement de situation du demandeur.

*Pour aller plus loin* : article R. 4241-13 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

#### Autorité compétente

L'autorisation d'exercice est délivrée par le préfet de la région, après avis de la commission des préparateurs en pharmacie hospitalière.

#### Pièces justificatives

La demande d'autorisation s'effectue par le dépôt d'un dossier en deux exemplaires, comprenant l'ensemble des documents suivants :

- le [formulaire de demande d'autorisation d'exercice](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021862830&dateTexte=20180319) complété et signé ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une copie de son titre de formation lui permettant d'exercer l'activité de préparateur en pharmacie hospitalière et le cas échéant la photocopie de diplômes complémentaires ;
- l'ensemble des pièces permettant de justifier de ses formations continues et expériences professionnelles acquises dans un État membre ;
- une déclaration de l’État de l'UE ou de l'EEE attestant que le ressortissant ne fait l'objet d'aucune sanction ;
- une copie de l'ensemble de ses attestations mentionnant le niveau de la formation reçue, et le détail des heures et volume des enseignements suivis ;
- lorsque ni l'accès à la formation, ni son exercice ne sont réglementés dans l’État membre, tout document permettant de justifier qu'il a exercé l'activité de préparateur en pharmacie hospitalière pendant deux ans au cours des dix dernières années ;
- lorsque le titre de formation a été délivré par un État tiers mais reconnu dans un État membre, la reconnaissance du titre de formation par l’État membre.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet accuse réception du dossier dans le délai d'un mois et se prononcera après avoir eu l'avis de la commission des préparateurs en pharmacie. Cette dernière est chargée d'examiner les connaissances et les compétences du ressortissant acquises lors de sa formation ou au cours de son expérience professionnelle. Elle pourra soumettre le ressortissant à une mesure de compensation.

L'absence de réponse du préfet de région dans un délai de quatre mois vaut rejet de la demande d'autorisation.

*Pour aller plus loin* : article R. 4241-9 à 4241-12 du Code de la santé publique ; arrêté du 19 février 2010 fixant la composition du dossier à fournir à la commission d'autorisation d'exercice compétente pour l'examen des demandes présentées en vue de l'exercice en France de la profession de préparateur en pharmacie et préparateur en pharmacie hospitalière.

#### Bon à savoir : mesures de compensation

Si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession de préparateur en pharmacie et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente pourra soit :

- proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation et/ou une épreuve d’aptitude.

*Pour aller plus loin* : arrêté du 24 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de préparateur en pharmacie et préparateur en pharmacie hospitalière par des ressortissants des États membres de l'Union européenne ou partie à l'accord sur l'Espace économique européen.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).