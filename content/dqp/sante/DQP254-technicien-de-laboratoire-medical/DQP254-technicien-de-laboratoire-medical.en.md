﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP254" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Medical laboratory technician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="medical-laboratory-technician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/medical-laboratory-technician.html" -->
<!-- var(last-update)="2020-04-15 17:22:13" -->
<!-- var(url-name)="medical-laboratory-technician" -->
<!-- var(translation)="Auto" -->


Medical laboratory technician
=============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:13<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The medical laboratory technician is a medical assistant whose activity consists of carrying out, with the help of a medical biologist and/or a specialist physician, an examination of pathological medical biology, anatomy and cytology (analysis of biological cells and fluids).

In addition, the medical laboratory technician may be required to participate in teaching, research and therapeutic education programs for the patient.

*For further information*: Article L. 4352-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of medical laboratory technician, the professional must be the holder of either:

- a state diploma (DE) as a medical laboratory technician;
- a training title whose training programme is equivalent to that of the ED and whose list is set at the[Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=68603435EE703FB09BF93D2EF66DB326.tplgfr41s_1?idArticle=LEGIARTI000036674386&cidTexte=LEGITEXT000006072818&dateTexte=20180524) of the order of 4 November 1976 setting out the list of titles or diplomas required of persons employed as technicians in a medical biology laboratory.

Professionals who:

- justify having performed technical duties in a medical laboratory for at least six months prior to 8 November 1976;
- were performing as a medical laboratory technician on the date of the enactment of Law No. 2013-442 of May 30, 2013, without holding a diploma or training designation to practice this profession.

The professional professional is required to register with the regional health agency (ARS) at the place of his professional practice.

*For further information*: Articles L. 4352-2 and the following from the Public Health Code.

#### Training

**State Medical Laboratory Technician Diploma**

To obtain this state diploma, the professional must have completed a three-year course with theoretical, practical teaching and internships.

This diploma is issued by the regional prefect to candidates who have:

- completed the three years of training and carried out the compulsory internships;
- obtained a certificate of training in emergency gestures and care at level 2;
- successfully passed the state degree tests:- a four-hour written synthesis test covering the entire training programme,
  - two practical tests lasting three hours each.

*For further information*: Articles D. 4352-1 to D. 4352-6 of the Public Health Code; order of 21 August 1996 relating to preparatory studies for the State Diploma of Medical Laboratory Technician.

**Certificate of specific capacity for blood samples**

The professional practising as a medical laboratory technician may obtain a certificate of capacity to take blood samples as long as he justifies having undergoen training whose program is fixed at the[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=190B672931637BDEAB273D9AACC931B4.tplgfr27s_2?idArticle=LEGIARTI000026318006&cidTexte=LEGITEXT000026318008&dateTexte=20180528) of the following order, and successfully passed:

- A theoretical test
- a training course in emergency management and care, and the completion of forty venous or hair samples over a period of up to three months;
- a practical test of samples in the presence of a jury.

To obtain this certificate, the applicant files an application with the LRA of his place of residence including:

- An application to register for the exam
- A photocopy of his ID
- a copy of all of his titles or diplomas.

*For further information*: order of 13 March 2006 setting out the conditions for issuing the certificate of capacity to take blood samples for medical biology examinations.

#### Costs associated with qualification

Training leading to the State Medical Laboratory Technician Diploma is paid for and the cost varies depending on the institution providing the teachings. For more information, it is advisable to get closer to the establishment in question.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may perform the work of a medical laboratory technician on a temporary and casual basis same activity in France.

Where the Member State does not regulate access to the activity or its exercise, the national must justify having carried out this activity for at least one year in the last ten years.

If they meet these conditions, the person concerned must make a prior declaration to the relevant regional prefect prior to his first provision of services (see below " Make a prior declaration for the EU national for a temporary and casual exercise (LPS)").

The temporary and casual national is not subject to the registration requirement for the French national.

In addition, he must justify having the language skills necessary to practice the profession of medical laboratory technician in France.

*For further information*: Article L. 4352-7 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of a legally established EU or EEA Member State who practises as a medical laboratory technician may perform the same activity in France on a temporary basis.

In order to do so, the person concerned must be the holder of:

- a training certificate issued by a Member State that regulates access to this profession and allows it to carry out this activity;
- where the State does not regulate access to the activity or its exercise, a training document certifying that it has been preparing for the practice of this occupation, as well as a certificate certifying that it has been engaged in this activity for at least one year in the Over the past decade;
- a training document allowing the exercise of this activity issued by a third state and recognised in an EU or EEA Member State, as well as a certificate certifying that it has carried out this activity for three years in that Member State.

When the examination of the national's professional qualifications shows a substantial difference between his training and the qualification required to carry out the activity of laboratory technician in France, the regional prefect may decide submit it to either an aptitude test or an adjustment course.

In addition, as in the context of the SPA, the national must justify having the language skills necessary to practice the profession in France.

As soon as the national fulfils these conditions, he must apply for an authorisation to practise from the relevant regional prefect (see below "Request for authorisation to practise for the EU national for a permanent exercise").

*For further information*: Articles L. 4352-6 and L. 4352-8 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The medical laboratory technician, as a health professional, is required to comply with the provisions of the Code of Medical Ethics.

As such, it is required to:

- Professional secrecy;
- Respect for the patient's privacy
- to exercise independently;
- respect the principles of morality, probity and devotion.

*For further information*: Articles R. 4127-1 and the following of the Public Health Code.

4°. Insurance and criminal sanctions
--------------------------------------------------------

**Insurance**

The medical laboratory technician as a health professional is required to take out professional liability insurance for the risks incurred during the course of his activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

**Sanctions**

The professional faces a one-year prison sentence and a fine of 15,000 euros if he practises:

- illegally the profession of medical laboratory technician;
- without right, the quality of laboratory technician of medical biology or a title or diploma allowing the exercise of this activity is punished with penalties provided for the offence of usurpation of title.

*For further information*: Articles L. 4353-1 and L. 4353-2 of the Public Health Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Proceed to register

**Competent authority**

The professional must submit his request to the regional health agency at the place of his professional practice.

**Supporting documents**

His file must include the following supporting documents:

- A copy of his ID
- a copy of his training title or authorization to practice, as well as certified information provided by the organization that issued this title or certification:- the civil status data of the holder of the training title and all the information to identify the applicant,
  - The wording and address of the organization that delivered the training,
  - the title of the training received.

**Time and procedure**

The ARS registers the professional after checking all supporting documents.

**Cost**

Free.

**Please note**

Professionals who have graduated or qualified for training less than three years but do not work as a laboratory technician must register with the LRA in their home or any other LSA.

*For further information*: Articles L. 4352-4 and D. 4354-1 to R. 4354-11 of the Public Health Code.

### b. Make a pre-declaration for the EU national for a temporary and casual exercise (LPS)

**Competent authority**

The national must make a request to the prefect of the region in which he wishes to exercise it before his first service delivery.

**Supporting documents**

The application must include the following documents, if any, with their translation into French:

- the service delivery reporting form attached to the schedule of the December 8, 2017 order on the prior declaration of service delivery for genetic counsellors, medical physicists and pharmacy and pharmacy preparers hospital pharmacy, as well as for occupations in Book III of Part IV of the Public Health Code;
- A copy of his valid ID
- A copy of his training certificate allowing him to carry out the activity of medical laboratory technician in his state of establishment;
- a certificate less than three months old certifying that it is legally established in a Member State and does not incur any prohibition on practising;
- Where neither access to the activity nor its exercise is regulated in the Member State, any documentation justifying the professional's work as a medical laboratory technician for one year in the last ten years;
- where the training designation has been established by a third state and recognised by a Member State:- Recognition of the training title established by the Member State;
  - any evidence justifying the professional's work as a medical laboratory technician for at least three years;
  - in the event of a renewal, a copy of the previous statement and the first statement.

**Delays and procedures**

The prefect informs the applicant within one month:

- that he can start providing services without checking his professional qualifications;
- where there is a substantial difference between the training received by the applicant and that required in France to carry out the activity of medical laboratory technician, that he submit to an aptitude test in order to demonstrate that he owns the whole The knowledge needed to practice
- that it cannot begin service delivery.

The lack of response by the prefect of the region beyond a period of one month, the professional can begin his service delivery. The professional is on a particular list and receives a receipt and registration number.

**Please note**

The declaration must be renewed every year under the same conditions.

*For further information*: Articles R. 4361-16; R. 4332-12 and R. 4331-12 to R. 4331-15 of the Public Health Code; December 8, 2017 aforementioned.

### c. Application for authorisation to exercise for the EU national for a permanent exercise

**Competent authority**

The national must submit his application in two copies by letter recommended with notice of receipt to the secretariat of the commission of authorisation to exercise.

**Supporting documents**

Its application must include the following documents, if any, with their translation into French, certified:

- The application form for authorisation to exercise as set out in the schedule of the order of 24 March 2010 cited below;
- A photocopy of a valid ID
- A copy of the training title allowing the exercise of the activity of medical laboratory technician and, if necessary, of his additional diplomas;
- any evidence justifying the national's continuing training and professional experience;
- A declaration less than a year old and certifying that it is not subject to any sanction;
- a copy of the certificates of the organizations that issued him his training title and stating:- The level of training
  - The detail and hourly volume of the teachings followed,
  - The duration and content of the internships carried out;
- where the Member State does not regulate access to the activity or its exercise, any document justifying that it has been engaged in this activity for at least two years in the last ten years.

**Time and procedure**

The regional prefect acknowledges receipt of the request within one month of receipt and issues the authorization to exercise after advice from the Medical Laboratory Technicians Committee.

In the absence of a response from the prefect within four months of receipt of the file, is worth accepting his application for authorization to exercise.

*For further information*: Articles R. 4352-7 to R. 4352-9 of the Public Health Code; decree of 24 March 2010 setting out the composition of the file to be provided to the competent exercise authorisation commissions for the examination of applications submitted for the exercise in France of the profession of medical laboratory technician.

**Good to know: compensation measures**

In case of substantial differences between the training received by the professional and that required for the exercise in France of the activity of medical laboratory technician, the regional prefect may decide to submit it to a compensation measure.

The aptitude test takes the form of written or oral questions about subjects not taught during his training or not acquired during his professional experience.

The adaptation course takes place in a public or private health facility approved by the regional health agency (ARS) and under the pedagogical responsibility of a professional who has been working as a medical laboratory technician for at least three Years.

*For further information*: decree of 24 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice of medical laboratory technician in France by nationals of the Member States of the European Union or party to agreement on the European Economic Area.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

