﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP254" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Technicien de laboratoire médical" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="technicien-de-laboratoire-medical" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/technicien-de-laboratoire-medical.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="technicien-de-laboratoire-medical" -->
<!-- var(translation)="None" -->

# Technicien de laboratoire médical

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le technicien de laboratoire médical est un auxiliaire médical dont l'activité consiste à réaliser, avec l'aide d'un biologiste médical et/ou d'un médecin spécialiste, un examen de biologie médicale, d'anatomie et de cytologie pathologiques (analyse des cellules et des liquides biologiques).

En outre, le technicien de laboratoire médical peut être amené à participer à des missions d'enseignement, de recherche et aux programmes d'éducation thérapeutique du patient.

*Pour aller plus loin* : article L. 4352-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de technicien de laboratoire médical, le professionnel doit être titulaire soit :

- d'un diplôme d’État (DE) de technicien de laboratoire médical ;
- d'un titre de formation dont le programme de formation est équivalent à celui du DE et dont la liste est fixée à l'article 1er de l'arrêté du 4 novembre 1976 fixant la liste des titres ou diplômes exigés des personnes employées en qualité de technicien dans un laboratoire de biologie médicale.

Peuvent également exercer la profession de technicien de laboratoire médical les professionnels qui :

- justifient avoir exercé des fonctions techniques dans un laboratoire médical pendant au moins six mois avant le 8 novembre 1976 ;
- exerçaient des fonctions de technicien de laboratoire médical à la date de promulgation de la loi n° 2013-442 du 30 mai 2013, sans être titulaire d'un diplôme ou d'un titre de formation permettant l'exercice de cette profession.

Le professionnel qualifié professionnellement est tenu de se faire enregistrer auprès de l'agence régionale de santé (ARS) du lieu de son exercice professionnel.

*Pour aller plus loin* : articles L. 4352-2 et suivants du Code de la santé publique.

#### Formation

##### Diplôme d’État de technicien de laboratoire médical

Pour obtenir ce diplôme d’État, le professionnel doit avoir suivi une formation d'une durée de trois ans comportant des enseignements théoriques, pratiques et des stages.

Ce diplôme est délivré par le préfet de région aux candidats ayant :

- suivi les trois années de formation et effectué les stages obligatoires ;
- obtenu une attestation de formation aux gestes et soins d'urgence de niveau 2 ;
- passé avec succès les épreuves du diplôme d’État à savoir :
  - une épreuve écrite de synthèse d'une durée de quatre heures et portant sur l'ensemble du programme de la formation,
  - deux épreuves pratiques d'une durée de trois heures chacune.

*Pour aller plus loin* : articles D. 4352-1 à D. 4352-6 du Code de la santé publique ; arrêté du 21 août 1996 relatif aux études préparatoires au diplôme d’État de technicien de laboratoire médical.

##### Certificat de capacité spécifique en vue d'effectuer des prélèvements sanguins

Le professionnel exerçant l'activité de technicien de laboratoire médical peut obtenir un certificat de capacité pour effectuer des prélèvements sanguins dès lors qu'il justifie avoir suivi une formation dont le programme est fixé à l'annexe de l'arrêté visé ci-après, et passé avec succès :

- une épreuve théorique ;
- un stage de formation aux gestions et soins d'urgence, et de réalisation de quarante prélèvements de sang veineux ou capillaire sur une période de trois mois maximum ;
- une épreuve pratique de prélèvements en présence d'un jury.

Pour obtenir ce certificat, le candidat dépose une demande auprès de l'ARS de son lieu de résidence comprenant :

- une demande d'inscription à l'examen ;
- une photocopie de sa pièce d'identité ;
- une copie de l'ensemble de ses titres ou diplômes.

*Pour aller plus loin* : arrêté du 13 mars 2006 fixant les conditions de délivrance du certificat de capacité pour effectuer des prélèvements sanguins en vue d'examens de biologie médicale.

#### Coûts associés à la qualification

La formation menant au diplôme d’État de technicien de laboratoire médical est payante et son coût varie selon l'établissement dispensant les enseignements. Pour de plus amples informations, il est conseillé de se rapprocher de l'établissement considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de technicien de laboratoire médical peut exercer à titre temporaire et occasionnel la même activité en France.

Lorsque l’État membre ne réglemente ni l'accès à l'activité ni son exercice, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, l'intéressé devra effectuer avant sa première prestation de services une déclaration préalable auprès du préfet de région compétent (cf. infra « Effectuer une déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS) »).

Le ressortissant exerçant à titre temporaire et occasionnel n'est pas soumis à l'obligation d'enregistrement prévue pour le ressortissant français.

En outre, il doit justifier avoir les connaissances linguistiques nécessaires à l'exercice de la profession de technicien de laboratoire médical en France.

*Pour aller plus loin* : article L. 4352-7 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'UE ou de l'EEE légalement établi et exerçant l'activité de technicien de laboratoire médical peut exercer en France, à titre temporaire, la même activité.

Pour cela, l'intéressé doit être titulaire soit :

- d'un titre de formation délivré par un État membre qui réglemente l'accès à cette profession et lui permettant d'exercer cette activité ;
- lorsque l’État ne réglemente ni l'accès à l'activité ni son exercice, d'un titre de formation attestant qu'il a suivi une préparation en vue de l'exercice de cette profession, ainsi qu'une attestation certifiant qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- d'un titre de formation permettant l'exercice de cette activité délivré par un État tiers et reconnu dans un État membre de l'UE ou de l'EEE, ainsi qu'une attestation certifiant qu'il a exercé cette activité pendant trois ans dans cet État membre.

Lorsque l'examen des qualifications professionnelles du ressortissant fait apparaître une différence substantielle entre sa formation et la qualification requise pour exercer en France l'activité de technicien de laboratoire, le préfet de région peut décider de le soumettre soit à une épreuve d'aptitude, soit à un stage d'adaptation.

En outre, comme dans le cadre de la LPS, le ressortissant doit justifier être titulaire des connaissances linguistiques nécessaires à l'exercice de la profession en France.

Dès lors qu'il remplit ces conditions, le ressortissant doit solliciter une demande d'autorisation d'exercice auprès du préfet de région compétent (cf. infra « Demande d'autorisation d'exercice pour le ressortissant de l'UE en vue d'un exercice permanent »).

*Pour aller plus loin* : articles L. 4352-6 et L. 4352-8 du Code de la santé publique.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le technicien de laboratoire médical, en tant que professionnel de la santé, est tenu au respect des dispositions du Code de déontologie médicale.

À ce titre, il est tenu :

- au secret professionnel ;
- au respect de la vie privée du patient ;
- d'exercer en toute indépendance ;
- de respecter les principe de moralité, de probité et de dévouement.

*Pour aller plus loin* : articles R. 4127-1 et suivants du Code de la santé publique.

## 4°. Assurances et sanctions pénales

### Assurance

Le technicien de laboratoire médical en tant que professionnel de santé est tenu de souscrire une assurance de responsabilité civile professionnelle pour les risques encourus au cours de l'exercice de son activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

### Sanctions

Le professionnel encourt une peine d'un an d'emprisonnement et 15 000 euros d'amende dès lors qu'il exerce :

- illégalement la profession de technicien de laboratoire médical ;
- sans droit, de la qualité de technicien de laboratoire de biologie médical ou d'un titre ou diplôme permettant l'exercice de cette activité est puni des peines prévues pour le délit d'usurpation de titre.

*Pour aller plus loin* : articles L. 4353-1 et L. 4353-2 du Code de la santé publique.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Procéder à son enregistrement

#### Autorité compétente

Le professionnel doit adresser sa demande à l'agence régionale de santé du lieu de son exercice professionnel.

#### Pièces justificatives

Son dossier doit comporter les pièces justificatives suivantes :

- une copie de sa pièce d'identité ;
- une copie de son titre de formation ou de son autorisation d'exercice, ainsi que les informations certifiées fournies par l'organisme ayant délivré ce titre ou cette attestation :
  - les données d'état civil du titulaire du titre de formation et l'ensemble des informations permettant d'identifier le demandeur,
  - le libellé et l'adresse de l'organisme ayant délivré la formation,
  - l'intitulé de la formation reçue.

#### Délai et procédure

L'ARS procède à l'enregistrement du professionnel après vérification de l'ensemble des pièces justificatives.

#### Coût

Gratuit.

**À noter**

Les professionnels ayant obtenu leur diplôme ou titre de formation depuis moins de trois ans mais n'exerçant pas l'activité de technicien de laboratoire doivent procéder à leur enregistrement auprès de l'ARS de leur domicile ou de toute autre ARS.

*Pour aller plus loin* : articles L. 4352-4 et D. 4354-1 à R. 4354-11 du Code de la santé publique.

### b. Effectuer une déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser avant sa première prestation de services, un demande au préfet de la région au sein de laquelle il souhaite l'exercer.

#### Pièces justificatives

La demande doit comporter les documents suivants, le cas échéant assortis de leur traduction en français :

- le formulaire de déclaration de prestation de services fixé à l'annexe de l'arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique ;
- une copie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation lui permettant d'exercer l'activité de technicien de laboratoire médical dans son État d'établissement ;
- une attestation datant de moins de trois mois certifiant qu'il est légalement établi dans un État membre et n'encourt aucune interdiction d'exercer ;
- lorsque ni l'accès à l'activité ni son exercice ne sont réglementés dans l’État membre, tout document justifiant que le professionnel a exercé l'activité de technicien de laboratoire médical pendant un an au cours des dix dernières années ;
- lorsque le titre de formation a été établi par un État tiers et reconnu par un État membre :
  - la reconnaissance du titre de formation établie par l’État membre ;
  - toute pièce justifiant que le professionnel a exercé pendant au moins trois ans l'activité de technicien de laboratoire médical ;
  - en cas de renouvellement, une copie de la déclaration précédente et de la première déclaration.

#### Délais et procédure

Le préfet informe le demandeur dans un délai d'un mois soit :

- qu'il peut débuter la prestation de services sans vérifications de ses qualifications professionnelles ;
- lorsqu'il existe une différence substantielle entre la formation reçue par le demandeur et celle exigée en France pour exercer l'activité de technicien de laboratoire médical, qu'il se soumette à une épreuve d'aptitude en vue de démontrer qu'il possède l'ensemble des connaissances nécessaires pour exercer ;
- qu'il ne peut débuter la prestation de services.

L'absence de réponse par le préfet de région au delà d'un délai d'un mois, le professionnel peut débuter sa prestation de services. Le professionnel est inscrit sur une liste particulière et reçoit un récépissé et un numéro d'inscription.

**À noter**

La déclaration doit être renouvelée tous les ans dans les mêmes conditions.

*Pour aller plus loin* : articles R. 4361-16 ; R. 4332-12 et R. 4331-12 à R. 4331-15 du Code de la santé publique ; arrêté du 8 décembre 2017 susvisé.

### c. Demande d'autorisation d'exercice pour le ressortissant de l'UE en vue d'un exercice permanent

#### Autorité compétente

Le ressortissant doit adresser sa demande en deux exemplaires par lettre recommandée avec avis de réception au secrétariat de la commission d'autorisation d'exercice.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant, assortis de leur traduction en français, certifiée :

- le formulaire de demande d'autorisation d'exercice fixé à l'annexe de l'arrêté du 24 mars 2010 cité ci-après ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de l'activité de technicien de laboratoire médical et le cas échéant, de ses diplômes complémentaires ;
- toute pièce justifiant des formations continues et des expériences professionnelles du ressortissant ;
- une déclaration datant de moins d'un an et certifiant qu'il ne fait l'objet d'aucune sanction ;
- une copie des attestations des organismes lui ayant délivré son titre de formation et mentionnant :
  - le niveau de la formation,
  - le détail et le volume horaire des enseignements suivis,
  - la durée et le contenu des stages effectués ;
- lorsque l’État membre ne réglemente ni l'accès à l'activité ni son exercice, tout document justifiant qu'il a exercé cette activité pendant au moins deux ans au cours des dix dernières années.

#### Délai et procédure

Le préfet de région accuse réception de la demande dans un délai d'un mois à compter de sa réception et délivre l'autorisation d'exercice après avis de la commission des techniciens de laboratoire médical.

En absence de réponse de la part du préfet dans un délai de quatre mois à compter de la réception du dossier, vaut acceptation de sa demande d'autorisation d'exercice.

*Pour aller plus loin* : articles R. 4352-7 à R. 4352-9 du Code de la santé publique ; arrêté du 24 mars 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France de la profession de technicien de laboratoire médical.

#### Bon à savoir : mesures de compensation

En cas de différences substantielles entre la formation reçue par le professionnel et celle exigée pour l'exercice en France de l'activité de technicien de laboratoire médical, le préfet de région peut décider de le soumettre à une mesure de compensation.

L'épreuve d'aptitude prend la forme d'interrogations écrites ou orales portant sur les matières non enseignées au cours de sa formation ou non acquises au cours de son expérience professionnelle.

Le stage d'adaptation s'effectue dans un établissement de santé public ou privé agréé par l'agence régionale de santé (ARS) et sous la responsabilité pédagogique d'un professionnel exerçant l'activité de technicien de laboratoire médical depuis au moins trois ans.

*Pour aller plus loin* : arrêté du 24 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France de la profession de technicien de laboratoire médical par des ressortissants des États membres de l'Union européenne ou partie à l'accord sur l'Espace économique européen.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).