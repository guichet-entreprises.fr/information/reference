﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP199" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Opticien lunetier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="opticien-lunetier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/opticien-lunetier.html" -->
<!-- var(last-update)="2020-04-15 17:21:51" -->
<!-- var(url-name)="opticien-lunetier" -->
<!-- var(translation)="None" -->

# Opticien lunetier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:51<!-- end-var -->

## 1°. Définition de l'activité

L'opticien-lunetier est un professionnel de santé spécialisé dans la conception, la fabrication et la vente de lunettes, de verres correcteurs et de lentilles de contact.

S'appuyant sur une prescription d'un ophtalmologiste (médecin spécialiste), il peut néanmoins lui-même évaluer la vue de son client en faisant un examen optométrique.

Une fois que le client aura choisi la monture, l'opticien réalisera le taillage, le calibrage et le montage des verres.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession d'opticien-lunetier est réservée au titulaire du brevet de technicien supérieur (BTS) d'opticien-lunetier.

*Pour aller plus loin* : article D. 4362-1 du Code de la santé publique.

#### Formation

Le BTS est un diplôme de niveau III accessible après l'obtention du baccalauréat. Le diplôme se prépare en deux ans dans des lycées technologiques, des écoles privées ou dans le cadre de l'apprentissage dans des centres de formation d'apprentis.

#### Coûts associés à la qualification

La formation pour devenir opticien-lunetier est payante et dépendra du type d'enseignement choisi. Pour plus d'informations, il est conseillé de se rapprocher des établissements concernés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE), exerçant légalement l’activité d'opticien-lunetier dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet de la région dans laquelle il souhaite faire la prestation (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États de l'UE ou de l'EEE pendant au moins un an, au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : article L. 4362-7 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité d'opticien-lunetier dans cet État, peut exercer la même activité en France de manière permanente s'il :

- est titulaire d'un titre de formation délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession ;
- est titulaire d'un titre délivré par une autorité compétente d'un État tiers et reconnu par un État de l'UE ou de l'EEE, et qu'il justifie avoir exercé l'activité la profession pendant au moins trois ans dans l’État de l'UE ou de l'EEE.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une autorisation individuelle d'exercice auprès du préfet de la région dans laquelle il souhaite exercer sa profession (cf. infra « 5°. b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

*Pour aller plus loin* : article L. 4362-3 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

L'opticien lunetier n'est soumis à aucune règle déontologique. Toutefois, ce dernier devra respecter la réglementation en matière de délivrance des verres correcteurs et précisée de articles L. 4362-9 à L. 4362-12 du Code de la santé publique.

## 4°. Assurance

En qualité de professionnel de santé, l'opticien-lunetier exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet de la région est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de la profession dans l’État d'obtention ;
- une attestation, datant de moins de trois mois, de l'autorité compétente de l’État de l'UE ou de l'EEE, certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- toutes pièces justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État de l'UE ou de l'EEE autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l’État ayant reconnu ce titre,
  - toutes pièces justifiant que le ressortissant a exercé la profession dans cet État pendant trois ans ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée ;
- une attestation de responsabilité civile professionnelle.

**À savoir** 

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

A réception du dossier, le préfet de la région disposera d'un délai d'un mois pour se prononcer sur la demande et informera le ressortissant :

- qu'il peut débuter la prestation. Dès lors, le préfet enregistrera le demandeur au répertoire Adeli ;
- qu'il sera soumis à une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France ;
- qu'il ne pourra pas débuter la prestation ;
- de toute difficulté pouvant retarder sa décision. Dans ce dernier cas, le préfet pourra rendre sa décision dans les deux mois suivant la résolution de cette difficulté, et au plus tard dans les trois mois de sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaudra acceptation de la demande de déclaration.

**À noter**

La déclaration est renouvelable tous les ans ou à chaque changement de situation du demandeur.

*Pour aller plus loin* : arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

#### Autorité compétente

L'autorisation d'exercice est délivrée par le préfet de la région, après avis de la commission des opticiens-lunetiers.

#### Pièces justificatives

La demande d'autorisation s'effectue par le dépôt d'un dossier comprenant l'ensemble des documents suivants :

- le formulaire de demande d'autorisation d'exercice complété et signé ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une copie de son titre de formation lui permettant d'exercer l'activité d'opticien-lunetier et le cas échéant la photocopie de diplômes complémentaires ;
- l'ensemble des pièces permettant de justifier de ses formations continues et expériences professionnelles acquises dans un État membre ;
- une déclaration de l’État membre attestant que le ressortissant ne fait l'objet d'aucune sanction ;
- une copie de l'ensemble de ses attestations mentionnant le niveau de la formation reçue, et le détail des heures et volume des enseignements suivis ;
- lorsque ni l'accès à la formation ni son exercice ne sont réglementés dans l’État membre, tout document permettant de justifier qu'il a exercé l'activité d'opticien-lunetier pendant un an au cours des dix dernières années ;
- lorsque le titre de formation a été délivré par un État tiers mais reconnu dans un État membre, la reconnaissance du titre de formation par l’État membre.

**À savoir** 

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet accuse réception du dossier dans le délai d'un mois et se prononcera après avoir eu l'avis de la commission des opticiens-lunetiers. Cette dernière est chargée d'examiner les connaissances et les compétences du ressortissant acquises lors de sa formation ou au cours de son expérience professionnelle. Elle pourra soumettre le ressortissant à une mesure de compensation.

Le silence gardé du préfet de région dans un délai de quatre mois vaut rejet de la demande d'autorisation.

*Pour aller plus loin* : articles R. 4362-2 à R. 4362-4 du Code de la santé publique ; arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste et opticien-lunetier.

#### Bon à savoir : mesures de compensation

Si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession d'opticien-lunetier et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente pourra soit :

- proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation et/ou une épreuve d’aptitude ;

*Pour aller plus loin* : article L. 4362-3 du Code de la santé publique ; arrêté du 30 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste, opticien-lunetier par des ressortissants des États membres de l'Union européenne ou parties à l'accord sur l'Espace économique européen.

### c. Enregistrement auprès du répertoire Adeli

Le ressortissant souhaitant exercer la profession d'opticien-lunetier en France est tenu de faire enregistrer son autorisation d'exercer sur le répertoire Adeli (« Automatisation des listes »).

#### Autorité compétente

L’enregistrement au répertoire Adeli se fait auprès de l’agence régionale de santé (ARS) du lieu d’exercice.

#### Délai

La demande d’enregistrement est présentée dans le mois suivant la prise de fonction du ressortissant, quel que soit le mode d’exercice (libéral, salarié, mixte).

#### Pièces justificatives

A l'appui de sa demande d'enregistrement, l'opticien-lunetier doit fournir un dossier comportant :

- le diplôme original ou titre attestant de la formation d'opticien-lunetier délivrée par l'État de l'UE ou de l'EEE (traduit en français par un traducteur agréé, le cas échéant) ;
- une pièce d’identité ;
- le formulaire Cerfa 10906-06 complété, daté et signé.

#### Issue de la procédure

Le numéro Adeli du ressortissant sera directement mentionné sur le récépissé du dossier, délivré par l'ARS.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 4362-1 et D. 4364-18 du Code de la santé publique.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).