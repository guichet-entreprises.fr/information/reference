﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP199" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Dispensing optician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="dispensing-optician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/dispensing-optician.html" -->
<!-- var(last-update)="2020-04-15 17:21:51" -->
<!-- var(url-name)="dispensing-optician" -->
<!-- var(translation)="Auto" -->


Dispensing optician
==================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:51<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The optician is a healthcare professional specializing in the design, manufacture and sale of glasses, corrective lenses and contact lenses.

Based on a prescription from an ophthalmologist (specialist physician), he can nevertheless assess his client's eyesight by doing an optometric examination.

Once the client has chosen the frame, the optician will perform the cutting, calibration and assembly of the lenses.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of optician-lunetier is reserved for the holder of the certificate of senior technician (BTS) of optician-lunetier.

*To go further* Article D. 4362-1 of the Public Health Code.

#### Training

The BTS is a Level III diploma available after graduation. The diploma is prepared in two years in technological high schools, private schools or as part of apprenticeships in training centers for apprentices.

#### Costs associated with qualification

Training to become an optician-lunetier pays off and will depend on the type of education chosen. For more information, it is advisable to get closer to the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

A national of a European Union (EU) or European Economic Area (EEA) state, legally practising optician-lunetier activity in one of these states, may use his or her professional title in France on a temporary or casual basis.

He will have to apply for it, before his first performance, by declaration addressed to the prefect of the region in which he wishes to make the delivery (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional will have to justify having carried out it in one or more EU or EEA states for at least one year, during the course of ten years before the performance.

*To go further* Article L. 4362-7 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, who is established and legally practises optician-lunetier activity in that state, may carry out the same activity in France on a permanent basis if he:

- holds a training certificate issued by a competent authority in another Member State, which regulates access to or exercise of the profession;
- has worked full-time or part-time for one year in the last ten years in another Member State that does not regulate training or the practice of the profession;
- holds a title issued by a competent authority of a third state and recognised by an EU or EEA state, and that it justifies having exercised the profession for at least three years in the EU or EEA State.

Once the national fulfils one of these conditions, he or she will be able to apply for an individual authorisation to practise from the prefect of the region in which he wishes to practice his profession (see infra "5o). b. Request an exercise permit for the EU or EEA national for permanent activity (LE) ").

*To go further* Article L. 4362-3 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The eyewear optician is not subject to any ethical rules. However, the latter will have to comply with the regulations regarding the issuance of corrective lenses and specified from sections L. 4362-9 to L. 4362-12 of the Public Health Code.

4°. Insurance
---------------------------------

As a health professional, a liberal optician-lunetier must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The prefect of the region is competent to decide on the request for a prior declaration of activity.

**Supporting documents**

The application is made by filing a file that includes the following documents:

- A copy of a valid ID
- A copy of the training title allowing the profession to be practised in the state of obtainment;
- a certificate, less than three months old, from the competent authority of the EU State or the EEA, certifying that the person concerned is legally established in that state and that, when the certificate is issued, there is no prohibition, even temporary, Exercise
- any evidence justifying that the national has practised the profession in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise;
- where the training certificate has been issued by a third state and recognised in an EU or EEA state other than France:- recognition of the training title established by the state authorities that have recognised this title,
  - any evidence justifying that the national has practiced the profession in that state for three years;
- If so, a copy of the previous statement as well as the first statement made;
- a certificate of professional civil liability.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Timeframe**

Upon receipt of the file, the prefect of the region will have one month to decide on the application and will inform the national:

- that he can start the performance. From then on, the prefect will register the applicant in the Adeli directory;
- that he will be subject to a compensation measure if there are substantial differences between the training or professional experience of the national and those required in France;
- he will not be able to start the performance;
- any difficulty that could delay its decision. In the latter case, the prefect will be able to make his decision within two months of the resolution of this difficulty, and no later than three months of notification to the national.

The prefect's silence within these deadlines will be worth accepting the request for declaration.

**Note that **

The return is renewable every year or at each change in the applicant's situation.

*To go further* : Order of 8 December 2017 relating to the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### b. Requesting an exercise permit for the EU or EEA national for permanent activity (LE)

**Competent authority**

The authorisation to exercise is issued by the prefect of the region, after advice from the committee of opticians-lunetiers.

**Supporting documents**

The application for authorization is made by filing a file containing all of the following documents:

- The completed and signed application form for the exercise authorization;
- A photocopy of a valid ID
- A copy of his training title allowing him to carry out the activity of optician-lunetier and, if necessary, a photocopy of additional diplomas;
- all the documents to justify his continuous training and professional experience acquired in a Member State;
- a statement from the Member State stating that the national is not subject to any sanction;
- A copy of all of his certificates mentioning the level of training received, and the details of the hours and volume of the teachings followed;
- where neither access to training nor its exercise is regulated in the Member State, any documentation to justify that it has been an optician-lunetier for one year in the last ten years;
- where the training certificate has been issued by a third state but recognised in a Member State, the Recognition of the Training Title by the Member State.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The prefect acknowledges receipt of the file within one month and will decide after having the opinion of the committee of opticians-lunetiers. The latter is responsible for examining the knowledge and skills of the national acquired during his training or during his professional experience. It may subject the national to a compensation measure.

The silence kept by the prefect of the region within four months is worth rejecting the application for authorisation.

*To go further* Articles R. 4362-2 to R. 4362-4 of the Public Health Code; decree of 25 February 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist and optician-lunetier.

**Good to know: compensation measures**

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession of optician-lunetier and its in France, the person concerned will have to submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority may either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- impose an adjustment course and/or aptitude test;

*To go further* Article L. 4362-3 of the Public Health Code; decree of 30 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist, optician-lunetier by European Union Member States or parties to the European Economic Area Agreement.

### c. Registration with the Adeli directory

A national wishing to practise as an optician-lunetier in France is required to register his authorisation to practice on the Adeli directory ("Automation of lists").

**Competent authority**

Registration in the Adeli directory is done with the regional health agency (ARS) of the place of practice.

**Timeframe**

The application for registration is submitted within one month of taking office of the national, regardless of the mode of practice (liberal, salaried, mixed).

**Supporting documents**

In support of his application for registration, the optician-lunetier must provide a file containing:

- the original diploma or title attesting to the training of optician-lunetier issued by the EU State or the EEA (translated into French by a certified translator, if applicable);
- ID
- Deer 10906-06 form completed, dated and signed.

**Outcome of the procedure**

The national's Adeli number will be directly mentioned on the receipt of the file, issued by the ARS.

**Cost**

Free.

*To go further* Articles L. 4362-1 and D. 4364-18 of the Public Health Code.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

