﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP202" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Prosthetist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="prosthetist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/prosthetist.html" -->
<!-- var(last-update)="2020-04-15 17:21:53" -->
<!-- var(url-name)="prosthetist" -->
<!-- var(translation)="Auto" -->


Prosthetist
===========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:53<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The orthoprosthetist is a health professional whose activity consists of designing and delivering prostheses for people with disabilities, who are missing a limb or who have disabilities (muscular, osteo-articular, neurological disorder).

As such, he is required to make fingerprints and casts on his patient in order to carry out the necessary equipment.

The professional can perform the following devices:

- custom prostheses or orthotics, intended to maintain, correct or replace all or part of a patient's limb or trunk;
- trunk orthotics to correct spine deformities;
- human body positioning orthotics (sitting, standing or lying down).

*For further information*: Article L. 4364-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To be an orthoprosthetist, the professional must:

- be professionally qualified. For this, it must be the holder either:- diploma, title or professional certification in the national directory of professional certifications ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)),
  - recognition of his professional competence from the Minister for Health, the Minister for Veterans and Victims of War or a health insurance agency,
  - a Certificate of Senior Technician (BTS) prosthetist - orthotist;
- to register his diploma (see infra "5°). (a) Proceed with the registration of his diploma").

*For further information*: Articles L. 4364-1 and L. 4362-2 , D. 4364-8 of the Public Health Code; Article 3 of the February 1, 2011 decree on the professions of prosthetist and orthotist for the device of persons with disabilities.

#### Training

The orthopaedic orthotist podiatrist certification can be issued either by:

- The Graduate School of Orthopaedic Orthopaedics Podiatrist (ECOTEV);
- the Vaucluse Chamber of Commerce and Industry (CCI) of Tarn;
- Association of Professional Courses in Pharmacy, Health, Health, Social and Environment (ACPPAV).

Candidates from a training course as a student, after a course of continuing education, in a professionalisation contract, by individual application, or through the procedure can register for this certification. Validation of Experience (VAE). For more information, you can see[VAE's official website](http://www.vae.gouv.fr/).

The Certificate of Professional Fitness (CAP) of orthoprosthetist (Level V) and the Certificate of Senior Technician (BTS) prosthetist - orthotist (level III (bac 2) are diplomas accessible under the same conditions as certification.

The professional title "Technical Prosthetist and Orthotist" (Level V) is available to candidates on apprenticeship contracts, after a continuous training course, in a professionalization contract or through the VAE procedure.

#### Costs associated with qualification

The cost of training leading to the profession of orthoprosthetist is paid, its cost varies according to the course envisaged. It is advisable to get close to the institutions concerned for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, legally established and practising the activity of orthoprosthetist, may engage in the same activity on a temporary and casual basis. France.

Where neither access to the activity nor its exercise is regulated in that Member State, the national must justify having carried out this activity for at least one year in the last ten years in one or more member states;

Prior to the provision of services, the professional is required to make a declaration to the regional prefect (see infra "5°. b. Pre-declaration for the national for a temporary and casual exercise (LPS)").

In addition, the professional must justify having the language skills necessary to carry out his activity.

*For further information*: Articles L. 4364-6 and L. 4364-7 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any EU or EEA national, legally established and practising the activity of orthoprosthetist, may carry out the same activity on a permanent basis in France.

For this, the professional must be the holder either:

- a training certificate issued by a Member State that regulates the exercise and access to that profession;
- where the Member State does not regulate access or practice to the profession, a training document attesting that it has been prepared to carry out the activity of orthoprosthetist and a certificate certifying that it has carried out this activity during the at least one year in the last ten years;
- a training certificate issued by a third state but recognised by a Member State other than France, allowing to carry out this activity, as well as a proof of exercise of this activity for at least three years.

In case of substantial differences between the training received by the professional and that required to carry out the activity of the orthoprosthetist in France, the prefect may decide to submit it to a compensation measure (see infra "5.0. c. Good to know: compensation measures").

In addition, the professional must justify having the language skills necessary to carry out his activity.

*For further information*: Article L. 4364-5 of the Public Health Code.

3°.Professional rules
---------------------------------

**Respect for professional confidentiality**

The orthoprosthetist, as a health professional, is bound by professional confidentiality.

*For further information*: Article D. 4364-12 of the Public Health Code.

**Rules of good practice**

In addition, the professional is bound by the rules of good practice that are the responsibility of his profession.

As such, it must include:

- to work in premises equipped and adapted to the needs of the exercise of his activity, and allowing him to receive his patients in good conditions of isolation (phonic and visual);
- maintain and update a file for each of its patients including all the information about them and a description of the necessary devices and possible interventions.

*For further information*: Article D. 4364-13 of the Public Health Code; Articles 13 to 26 of the order of February 1, 2011 mentioned above.

4°. Insurance
---------------------------------

The orthoprosthetist, as a health professional, is required to take out professional liability insurance for the risks incurred during the course of his activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Proceed to register his diploma

**Competent authority**

Applicants must apply to the Regional Health Agency (ARS) to register.

**Supporting documents**

To do so, it must pass on the following information, deemed validated and certified by the organization that issued the diploma or the training title:

- The civil status of the holder of the diploma and all the data to identify the applicant;
- The name and address of the institution that delivered the training;
- the title of the training.

**Outcome of the procedure**

After checking the exhibits, the LRA registers the diploma.

**Please note**

Registration is only possible for one department, however if the professional wishes to practice in several departments, he will be placed on the list of the department in which the main place of his activity is located.

**Cost**

Free.

*For further information*: Article D. 4364-18 of the Public Health Code; Articles D. 4333-1 to D. 4333-6-1 of the Public Health Code.

### b. Pre-declaration for the national for a temporary and casual exercise (LPS)

**Competent authority**

The national must apply to the prefect of the region where he wishes to carry out his services.

**Supporting documents**

His application must include:

- the declaration form attached to the schedule of the[decreed from 19 February 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000021863886&dateTexte=20171113) Completed and signed;
- all the supporting documents mentioned in this form.

**Time and procedure**

The regional prefect informs the applicant within one month of receiving his file:

- That he can begin his service delivery;
- He cannot begin his service delivery;
- that he must be subject to a prior check of his professional qualifications. If necessary, where there is a substantial difference between the training received by the national and that required to carry out this activity in France, the prefect may decide to subject him to a compensation measure (see infra "5°. c. Good to know: compensation measures").

In the absence of a response from the prefect within one month, the national may begin his service delivery.

**Please note**

The national will be placed on a specific list by the prefect, who will provide him with a receipt of his declaration mentioning his registration number.

This declaration must be renewed annually under the same conditions.

*For further information*: Articles R. 4364-11-3, R. 4331-12 to R. 4331-15 and D. 4364-11-9 of the Public Health Code; order of 19 February 2010 relating to the prior declaration of the provision of services for the practice of orthoprosthetist, podo-orthetist, ocularist, epithetist, orthopedist-orthotist.

### c. Application for authorization of exercise for the national for a permanent exercise (LE)

**Competent authority**

The national must submit his application in duplicate, by letter recommended with notice of receipt, to the National Commission of Prosthetists and Orthotists.

**Supporting documents**

His application must include the[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DB4E58672EA257FEDF6B78008E6B179E.tplgfr24s_1?idArticle=LEGIARTI000021777548&cidTexte=LEGITEXT000021777545&dateTexte=20180412) request for authorisation completed and signed.

In addition, it must provide the following supporting documents, if any, with their certified translation into French:

- A photocopy of his valid ID
- a copy of his training document allowing the exercise of the activity of orthopedist - orthotist in that Member State and, if necessary, of his additional diplomas;
- any documentation justifying the applicant's continuing education or work experience;
- A certificate from the Member State certifying that the applicant is not subject to any sanction;
- A copy of the certificates issued by the authorities mentioning the applicant's level of training as well as the details of the training (hourly volume, instructions followed);
- where the state does not regulate access to the profession or its exercise, proof that it has been engaged in this activity for at least two years in the last ten years;
- when the national has acquired his training title in a third state but recognised by a Member State, the recognition of his training title.

**Time and procedure**

The regional prefect acknowledges receipt of the request within one month. If the application is not answered within four months of receiving the application, it is deemed denied.

*For further information*: Articles R. 4364-11 to R. 4364-11-2 of the Public Health Code; decree of 20 January 2010 setting out the composition of the file to be provided to the competent authorisation commission for the examination of applications submitted for the exercise in France of the professions of orthoprosthetist, podo-orthologist, ocularist, epithist, orthopedist-orthosthetist.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the professional, and that required to carry out the activity of orthoprosthetist in France, the competent authority may decide to submit it to the choice, either to an adaptation internship or to an aptitude test.

The aptitude test must be completed within six months of notification to the applicant. Its purpose is to verify the knowledge and skills of the national in order to practise in France. The adaptation course, carried out with a qualified professional, aims to enable the national to acquire the necessary knowledge to carry out this activity.

*For further information*: Articles R. 4311-35 and R. 4311-36 of the Public Health Code; decree of 24 March 2010 setting out the modalities for organising the aptitude test and the adaptation course for the practice in France of the professions of orthoprosthetist, podo-orthologist, ocularist, epithetist, orthopedist-orthetist by european states or party to the European Economic Area agreement.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

