﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP265" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Infectious and tropical diseases" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="infectious-and-tropical-diseases" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/infectious-and-tropical-diseases.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="infectious-and-tropical-diseases" -->
<!-- var(translation)="None" -->


# Infectious and tropical diseases

Latest update: <!-- begin-var(last-update) -->2020-12<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of activity

The physician specializing in infectious and tropical diseases has expertise in emerging, complex, resistant, rare or tropical infections:

* the diagnosis and management of the most frequent pathologies, in particular in the context of emergency: all non-complex community infections (acute community lung disease, urinary tract infection, dermo-hypoderma, traumatic infected wound or by bite );
* infectious emergencies in their initial phase (meningitis, malaria, severe sepsis, etc.);
* febrile neutropenia, cardiovascular and thromboembolic diseases, diabetes, joint diseases, community and nosocomial infections including the severity criteria of sepsis, hematological pathologies, respiratory pathologies, allergic diseases, particularities of the elderly;
* the modalities of prescription, monitoring and significant drug interactions for corticosteroid therapy, immunosuppressive treatment, labile blood products, anti-infectives, anticoagulants and antiplatelet agents;
* the main causes and justification of the relevant additional examinations in front of an unexplained prolonged fever, polyadenopathy, splenomegaly, haematological anomaly, exanthema, metabolic disorder;
* the physiopathology of allergic and non-allergic hypersensitivity.

## 2 °. Professional qualifications

### a. National requirements

#### National legislation

Pursuant to Article L. 4111-1 of the Public Health Code, to legally practice the profession of doctor in France, those concerned must cumulatively meet the following three conditions:

* hold a French state diploma in medicine or a diploma, certificate or other title mentioned in Article L. 4131-1 of the Public Health Code (see below "Good to know: recognition automatic diploma ”);
* be of French nationality, Andorran citizenship or a national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) of Morocco or Tunisia, subject to the 'application of rules resulting from the Public Health Code or from international commitments. However, this condition does not apply to a doctor who holds a French state diploma in medicine;
* with some exceptions, be registered on the roll of one of the departmental councils of the Order of Physicians (see below 5 °. a. "Applying for registration on the roll of the Order of Physicians").

*To go further*: Articles L. 4111-1, L. 4112-6, L. 4112-7 and L. 4131-1 of the Public Health Code.

**To note**

If all of these conditions are not met, the exercise of the profession of doctor is illegal and punishable by two years' imprisonment and a fine of 30,000 euros.

*To go further*: Articles L. 4161-1 and L. 4161-5 of the Public Health Code.

**Good to know: automatic recognition of diplomas**

Pursuant to Article L. 4131-1 of the Public Health Code, EU or EEA nationals may practice the profession of doctor if they hold one of the following titles:

* medical training certificates issued by an EU or EEA state in accordance with community obligations and appearing on the list set out in the appendix to the decree of July 13, 2009 setting the lists and conditions for recognition of qualifications training for doctors and medical specialists issued by EU Member States or parties to the EEA Agreement referred to in 2 ° of Article L. 4131-1 of the Public Health Code;
* medical training certificates issued by an EU or EEA state in accordance with community obligations, not appearing on the aforementioned list, if they are accompanied by a certificate from this state certifying that they sanction training in accordance with these obligations and that they are assimilated by him to the training titles appearing on this list;
* medical training certificates issued by an EU or EEA state sanctioning medical training started in that state prior to the dates appearing in the aforementioned decree and not in accordance with community obligations, if they are accompanied a certificate from one of these States certifying that the holder of the training certificates has devoted himself, in that State, in an effective and lawful manner, to the exercise of the profession of doctor in the specialty concerned for at least three consecutive years during the fiftieth years preceding the issuance of the certificate;
* medical training certificates issued by the former Czechoslovakia, the former Soviet Union or the former Yugoslavia or which attest to training started before the date of independence of the Czech Republic, Slovakia, Estonia, of Latvia, Lithuania or Slovenia, if they are accompanied by a certificate from the competent authorities of one of these States certifying that they have the same legal validity as the training certificates issued by this state. This attestation is accompanied by a certificate issued by these same authorities indicating that its holder has practiced in that State, in an effective and lawful manner, the profession of doctor in the specialty concerned for at least three consecutive years during the five years preceding the issuance of the certificate;
* medical training certificates issued by an EU or EEA State not appearing on the aforementioned list if they are accompanied by a certificate issued by the competent authorities of that State certifying that the holder of the qualification training was established on its territory on the date fixed in the aforementioned decree and that it acquired the right to exercise the activities of general practitioner within the framework of its national system of social security;
* medical training certificates issued by an EU or EEA state sanctioning medical training started in that state prior to the dates appearing in the aforementioned decree and not in accordance with community obligations but allowing legal practice the profession of doctor in the State which issued them, if the doctor can prove that he has performed in France during the previous five years three consecutive full-time years of hospital functions in the specialty corresponding to the training titles as associate associate , associate associate practitioner, associate assistant or university functions in the capacity of associate clinical head of universities or associate assistant of universities, provided that they have been in charge of hospital functions at the same time;
* the specialist doctor's training certificates issued by Italy appearing on the aforementioned list attesting to specialist doctor training started in this State after December 31, 1983 and before January 1, 1991, if they are accompanied by a certificate issued by the authorities of that State indicating that its holder has practiced in that State, in an effective and lawful manner, the profession of doctor in the specialty concerned for at least seven consecutive years during the ten years preceding the issue of the certificate.

*To go further*: Article L. 4131-1 of the Public Health Code; order of July 13, 2009 setting the lists and the conditions for the recognition of the training certificates of doctor and specialist doctor issued by the Member States of the European Union or parties to the agreement on the European Economic Area referred to in 2 ° of Article L. 4131-1 of the Public Health Code.

#### Training

Medical studies are made up of three cycles lasting between nine and eleven years, depending on the course chosen.

The training, which takes place at the university, includes many internships and is punctuated by two competitions:

* the first occurs at the end of the first year. This year of study, called the "common first year of health studies" (PACES), is common to students of medicine, pharmacy, dentistry, physiotherapy and midwives. At the end of this first competition, the students are ranked according to their results. Those in the relevant rank with regard to the numerus clausus are allowed to continue their studies and to choose, if necessary, to continue training leading to the practice of medicine. From the start of the 2020 school year, the PACES will be withdrawn for selection on file within the framework of Parcoursup;
* the second takes place at the end of the second cycle (that is to say at the end of the sixth year of study): this competition is called national classifying tests (ECN) or formerly "internship". At the end of this competition, students choose, according to their classification, their specialty and / or their city of assignment. The duration of subsequent studies varies depending on the specialty chosen. This system will be replaced by a two-part examination, between the fifth and the sixth year, organized around a written test and an oral test, which will replace the ECN in 2023.

To obtain his state diploma (DE) in medicine, the student must validate all his internships, his diploma of specialized studies (DES) and successfully defend his thesis.

*To go further*: Article L. 632-1 of the Education Code.

**Good to know**

Medical students must carry out compulsory vaccinations. For more details, it is advisable to refer to Article R. 3112-1 of the Public Health Code.

** General education diploma in medical sciences **

The first cycle leads to the general training diploma in medical sciences. It comprises six semesters and corresponds to the license level.

The objectives of the training are:

* the acquisition of basic scientific knowledge, essential for the subsequent mastery of the knowledge and know-how necessary for the exercise of medical professions. This scientific base is broad and encompasses biology, certain aspects of the exact sciences and several disciplines of the human and social sciences;
* the fundamental approach of healthy and sick humans, including all aspects of semiology.

It includes theoretical, methodological, applied and practical lessons as well as the completion of internships including an introductory course in care lasting four weeks in a hospital establishment.

*To go further*: decree of 22 March 2011 relating to the study regime for the general training diploma in medical sciences.

** Advanced training diploma in medical sciences **

The second cycle of medical studies leads to the diploma of advanced training in medical sciences. It includes six semesters of training and corresponds to master's level.

Its objective is the acquisition of generic skills allowing students to subsequently exercise, in a hospital or ambulatory environment, postgraduate functions and to acquire the professional skills of the training in which they will engage in the during their specialization.

The skills to be acquired are those of communicator, clinician, cooperator, member of a multiprofessional healthcare team, public health actor, scientist and ethical and deontological manager. The student must also learn to be reflective.

The lessons mainly focus on what is frequent or serious or constitutes a public health problem as well as what is clinically exemplary.

The objectives of the training are:

* the acquisition of knowledge relating to physiopathological processes, pathology, therapeutic bases and prevention supplementing and deepening those acquired during the previous cycle;
* training in the scientific approach;
* learning clinical reasoning;
* the acquisition of generic skills preparing for the third cycle of medical studies.

In addition to theoretical and practical lessons, the training includes the completion of thirty-six months of training and twenty-five guards.

*To go further*: decree of April 8, 2013 relating to the study regime for the first and second cycle of medical studies.

** DES in surgery **

As of the [decree](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037) of November 27, 2017 which reformed the 3rd cycle of medical studies, the diploma of specialized studies (DES ) of general surgery has given way to the following DES, specialized in surgery:

* maxillofacial surgery;
* oral surgery;
* orthopedic and trauma surgery;
* pediatric surgery;
* plastic, reconstructive and aesthetic surgery;
* thoracic and cardiovascular surgery;
* vascular surgery ;
* visceral and digestive surgery;
* obstetric gynecology ;
* neurosurgery;
* ophthalmology;
* otorhinolaryngology - head and neck surgery;
* urology.

* To go further*: appendix II of the decree of 27 November 2017 amending the decree of 12 April 2017 relating to the organization of the third cycle of medical studies and the decree of 21 April 2017 relating to knowledge, skills and training models for specialized study diplomas and establishing the list of these diplomas and transversal specialist options and training for the third cycle of medical studies.

#### Costs associated with qualification

There is a charge for the training leading to the DE of doctor of medicine. Its cost varies according to the universities which provide the courses. For more details, it is advisable to contact the university in question.

### b. EU or EEA national: for temporary and occasional exercise (freedom to provide services)

A doctor who is a national of an EU or EEA State who is established and legally exercises his activity in one of these States may exercise in France, on a temporary and occasional basis, acts of his profession on the condition have previously sent a prior declaration to the National Council of the Order of Physicians (see below "5 °. b. Make a prior declaration for EU or EEA nationals exercising a temporary and occasional activity" ).

**To know**

Registration on the register of the Order of Physicians is not required for doctors in a situation of Freedom to provide services. They are therefore not required to pay the ordinal contributions. The doctor is simply registered on a specific list maintained by the National Council of the College of Physicians.

The prior declaration must be accompanied by a declaration concerning the linguistic knowledge necessary for the performance of the service. In this case, the control of language proficiency must be proportionate to the activity to be performed and carried out once the professional qualification has been recognized.

When the training certificates do not benefit from automatic recognition (see above "2 °. A. National legislation"), the professional qualifications of the service provider can be verified before the first provision of services. In the event of substantial differences between the qualifications of the person concerned and the training required in France likely to be harmful to public health, the provider is subject to a test.

The doctor in a situation of LPS is required to respect the professional rules applicable in France, in particular all the ethical rules (see below "3 °. Conditions of good repute, deontological rules, ethics"). It is subject to the disciplinary jurisdiction of the Order of Physicians.

**To note**

The service is provided under the French professional title of doctor. However, when the training certificates do not benefit from recognition and in the event that the qualifications have not been verified, the service is provided under the professional title of the State of establishment, so as to avoid any confusion. with the French professional title.

*To go further*: Article L. 4112-7 of the Public Health Code.

### c. EU or EEA national: for permanent exercise (Freedom of establishment)

#### The system of automatic recognition of diplomas obtained in an EU state

Article L. 4131-1 of the Public Health Code creates a system for automatic recognition in France of certain diplomas or titles, where applicable, accompanied by certificates, obtained in an EU or EEA State ( see above "2 °. a. National legislation").

It is up to the departmental council of the competent Order of Physicians to verify the regularity of diplomas, titles, certificates and attestations, to grant them automatic recognition and then to rule on the application for registration on the roll of the Order.

*To go further*: Article L. 4131-1 of the Public Health Code; order of July 13, 2009 setting the lists and the conditions for the recognition of the training certificates of doctor and specialist doctor issued by the Member States of the European Union or parties to the agreement on the European Economic Area referred to in 2 ° of Article L. 4131-1 of the Public Health Code.

#### The derogatory regime: prior authorization

If the EU or EEA national does not meet the conditions to benefit from the system of automatic recognition of his qualifications or diplomas, he comes under an authorization system, known as the general system (see below. "5 °. C. Where applicable, request an individual authorization to exercise").

People who do not benefit from automatic recognition but who hold a training certificate allowing them to legally practice the profession of doctor may be individually authorized to practice in the specialty concerned by the Minister responsible for health, after obtaining an opinion. a commission composed in particular of professionals.

If the examination of the professional qualifications attested by the training certificates and professional experience reveals substantial differences with the qualifications required for access to the profession in the specialty concerned and its exercise in France, the person concerned must submit to a compensatory measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

* offer the applicant the choice between an adaptation internship or an aptitude test;
* impose an adaptation period or an aptitude test;
* impose an adaptation course and a test.

*To go further*: article L. 4131-1-1 of the Public Health Code.

## 3 °. Conditions of good repute, deontological rules, ethics

### a. Compliance with the Code of ethics of physicians

The provisions of the Medical Code of Ethics apply to all physicians practicing in France, whether they are registered on the roll of the Order or whether they are exempt from this obligation (see above "5 °. A. Request his registration on the roll of the Order of Physicians ”).

**To know**

All the provisions of the Code of Ethics are codified in Articles R. 4127-1 to R. 4127-112 of the Public Health Code.

As such, the physician must in particular respect the principles of morality, probity and dedication essential to the practice of medicine. He is also subject to medical confidentiality and must practice in complete independence.

*To go further*: Articles R. 4127-1 to R. 4127-112 of the Public Health Code.

### b. Cumulation of activities

A doctor can only exercise another activity if such combination is compatible with the principles of professional independence and dignity which apply to him. The combination of activities should not allow him to benefit from his prescriptions or his medical advice.

Thus, the doctor cannot combine medical practice with another activity related to the field of health. In particular, he is prohibited from working as an optician, ambulance driver or manager of an ambulance company, manufacturer or seller of medical devices, owner or manager of a hotel for spa guests, a gym, a spa, a massage room.

Likewise, it is forbidden for a doctor who fulfills an elective mandate or an administrative function to use it to increase his clientele.

*To go further*: Articles R. 4127-26 and R. 4127-27 of the Public Health Code.

### c. Conditions of good repute

To be able to practice, the doctor must certify that no proceeding that may give rise to a conviction or a sanction likely to have consequences for his entry on the roll is pending against him.

*To go further*: article R. 4112-1 of the Public Health Code.

### d. Obligation of continuous professional development

Physicians must participate in a multi-year program of continuing professional development. This program aims in particular to assess professional practices, improve skills, improve the quality and safety of care, and maintain and update knowledge and skills.

All of the actions carried out by physicians as part of their obligation of continuing professional development are described in a specific document attesting to compliance with this obligation.

*To go further*: Articles L. 4021-1 et seq. And R. 4021-4 et seq. Of the Public Health Code.

### e. Physical aptitude

Doctors must not present any infirmity or pathology incompatible with the practice of the profession (see below "5 °. A. Applying for registration on the roll of the Order of Physicians").

*To go further*: article R. 4112-2 of the Public Health Code.

## 4 °. Insurance

### a. Obligation to take out professional liability insurance

As a health professional, a practicing physician must take out professional liability insurance.

However, if he works as an employee, this insurance is only optional. Indeed, in this case, it is up to the employer to take out such insurance for its employees for acts performed in the course of their professional activity.

*To go further*: Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the Autonomous Retirement Fund for Doctors of France (CARMF)

Any doctor registered on the Order's roll and practicing in the liberal form (even part-time and even if he otherwise exercises a salaried activity) has the obligation to join the CARMF.

#### Time limit

The person concerned must register with the CARMF within one month of starting his liberal activity.

#### Terms

The person concerned must return the declaration form, completed, dated and countersigned by the departmental council of the Order of Physicians. This form can be downloaded from the CARMF website.

**To know**

In the event of practice within a self-employed company (SEL), membership of the CARMF is also compulsory for all professional partners practicing there.

### c. Obligation to report to Health Insurance

Once registered on the Order's roll, the doctor practicing in liberal form must declare his activity to the Primary Health Insurance Fund (CPAM).

#### Terms

Registration with the CPAM can be done online on the official Health Insurance website.

#### Vouchers

The declarant must provide a complete file including:

* a copy of a valid identity document;
* a professional bank identity statement (RIB);
* where applicable, the supporting document (s) allowing access to the sector.

For more information, it is advisable to refer to the section devoted to the private installation of doctors on the Health Insurance website.

## 5 °. Qualification recognition procedures and formalities

### a. Apply for registration with the Order of Physicians

Registration on the roll of the Order is compulsory to legally exercise the activity of doctor in France.

Registration on the roll of the Order does not apply:

* to EU or EEA nationals who are established and legally exercising the activity of doctor in a Member State or party, when they perform in France, temporarily and occasionally, acts of their profession ( see above "2 °. b. EU and EEA nationals: for a temporary and occasional exercise");
* to doctors belonging to active executives of the military health service;
* to physicians who, having the status of state official or official of a local authority, are not called upon, in the exercise of their functions, to practice medicine.

*To go further*: Articles L. 4112-5 to L. 4112-7 of the Public Health Code.

**To note**

Registration on the roll of the Order allows the automatic and free issuance of the health professional card (CPS). CPS is an electronic business identity card. It is protected by a confidential code and notably contains the doctor's identification data (identity, profession, specialty). For more information, it is recommended to refer to the government site of the [French digital health agency](http://esante.gouv.fr/).

#### Competent authority

The application for registration is addressed to the president of the council of the Order of Physicians of the department in which the person concerned wishes to establish his professional residence.

The request can be submitted directly to the departmental council of the Order concerned or sent to it by registered mail with request for acknowledgment of receipt.

*To go further*: article R. 4112-1 of the Public Health Code.

**To know**

In the event of transfer of his professional residence outside the department, the practitioner is required to request his removal from the roll of the Order of the department where he practiced and his registration on the roll of the Order of his new professional residence.

*To go further*: article R. 4112-3 of the Public Health Code.

#### Procedure

Upon receipt of the request, the departmental council appoints a rapporteur who examines the request and makes a written report.

The board checks the candidate's qualifications and requests communication of bulletin No. 2 of the person's criminal record. In particular, it checks that the candidate:

* fulfills the necessary conditions of morality and independence (see above "3 °. c. Conditions of good repute");
* fulfills the necessary conditions of competence;
* does not present an infirmity or a pathological state incompatible with the exercise of the profession (see above "3 °. e. Physical aptitude").

In the event of serious doubt about the professional competence of the applicant or the existence of an infirmity or a pathological condition incompatible with the practice of the profession, the departmental council refers the matter to the regional or interregional council, which conducts an expertise. If it is noted, in the light of the expert's report, a professional insufficiency making the practice of the profession dangerous, the departmental council refuses the registration and specifies the practitioner's training obligations.

No decision to refuse registration may be taken without the person concerned having been invited at least fifteen days in advance by registered letter with request for acknowledgment of receipt to appear before the board to present their explanations.

The decision of the Bar Council is notified, within the following week, to the person concerned, to the National Council of the College of Physicians and to the Director General of the Regional Health Agency (ARS). Notification is made by registered letter with acknowledgment of receipt.

The notification mentions the means of appeal against the decision. The refusal decision must be motivated.

*To go further*: Articles R. 4112-2 and R. 4112-4 of the Public Health Code.

#### Time limit

The president acknowledges receipt of the complete file within one month of its registration, and informs the applicant of any missing documents

The departmental council of the Order must rule on the registration request within a maximum period of three months from receipt of the complete application file. In cases falling under the general scheme, an additional period of one month may apply in cases where the competent authority requires additional information to process its request, for a total of four months. If there is no response within this period, the registration request is deemed to be rejected.

This period is extended to six months for nationals of third countries when it is necessary to carry out an investigation outside mainland France. The person concerned is then notified.

*To go further*: Articles L. 4112-3 and R. 4112-1 of the Public Health Code.

#### Vouchers

The interested party must send a complete registration request file including:

* two copies of the standardized questionnaire with an identity photo completed, dated and signed, available in the departmental councils of the Order or directly downloadable from the [official website of the National Council of the Order of Physicians](https://www.conseil-national.medecin.fr/);
* a photocopy of a valid identity document or, where applicable, a certificate of nationality issued by a competent authority;
* where applicable, a photocopy of a valid EU citizen family member's residence card, valid long-term EU resident card or resident card mentioning valid refugee status;
* where applicable, a photocopy of the valid European blue card;
* a copy, accompanied if necessary by a translation, made by a certified translator, of the training certificates to which are attached:
  * when the applicant is an EU or EEA national, the certificate (s) provided (see above "2 °. a. National requirements"),
  * when the applicant has an individual exercise authorization (see above "2 °. c. EU and EEA nationals: with a view to permanent exercise"), a copy of this authorization,
  * when the applicant presents a diploma issued in a foreign state whose validity is recognized on French territory, a copy of the titles to which this recognition may be subject;
* for nationals of a foreign state, a criminal record extract or an equivalent document dated less than three months, issued by a competent authority of the state of origin. This document can be replaced, for nationals of EU or EEA states who require proof of character or good repute for access to the activity of doctor, by a certificate, dated less than three years ago. months, from the competent authority of the State of origin certifying that these conditions of morality or good repute are met;
* a declaration on the honor of the applicant certifying that no proceeding that could give rise to a conviction or sanction likely to have consequences on the entry on the roll is pending against him;
* a certificate of cancellation of registration or registration issued by the authority with which the applicant was previously registered or registered or, failing that, a declaration on the applicant's honor certifying that he has never been registered or registered or, failing that, a certificate of registration or registration in an EU or EEA State;
* all the elements likely to establish that the applicant has the linguistic knowledge necessary for the exercise of the profession;
* a resume ;
* contracts and amendments having as their object the exercise of the profession as well as those relating to the use of the equipment and the premises in which the applicant practices;
* if the activity is carried out in the form of SEL or professional civil society (SCP), the articles of association of this company and their possible amendments;
* if the applicant is a civil servant or public official, the appointment decree;
* if the applicant is a university professor - hospital practitioner (PU-PH), university lecturer - hospital practitioner (MCU-PH) or hospital practitioner (PH), the order of appointment as a hospital practitioner and, the where applicable, the decree or order of appointment as university professor or university lecturer.

For more information, it is advisable to refer to the official website of the National Council of the Order of Physicians.

*To go further*: Articles L. 4113-9 and R. 4112-1 of the Public Health Code.

#### Remedies

The applicant or the National Council of the Order of Physicians can challenge the decision to register or refuse to register within 30 days of notification of the decision or the implicit decision of rejection. The appeal is brought before the regionally competent regional council.

The regional council must take a decision within two months of receiving the request. In the absence of a decision within this period, the appeal is deemed to be rejected.

The decision of the regional council is also subject to appeal, within 30 days, to the National Council of the Order of Physicians. The decision thus rendered may itself be appealed against to the Council of State.

*To go further*: Articles L. 4112-4 and R. 4112-5 of the Public Health Code.

#### Cost

Registration on the roll of the Order is free but it gives rise to the obligation to pay the compulsory ordinal contribution, the amount of which is set annually and which must be paid during the first quarter of the current calendar year. Payment can be made online on the official website of the National Council of the Order of Physicians. As an indication, in 2017, the amount of this contribution was 333 euros.

*To go further*: Article L. 4122-2 of the Public Health Code.

### b. Make a prior declaration of activity for EU or EEA nationals exercising a temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally exercises the activities of a doctor in one of these States may exercise in France on a temporary or occasional basis if he makes a prior declaration (see above “ 2 ° b. EU and EEA nationals: for a temporary and occasional exercise ”).

The prior declaration must be renewed every year.

**To note**

Any change in the applicant's situation must be notified under the same conditions.

*To go further*: Articles L. 4112-7 and R. 4112-9-2 of the Public Health Code.

#### Competent authority

The declaration must be sent, before the first provision of services, to the National Council of the Order of Physicians if the oral surgeon has studied dentistry.

*To go further*: article R. 4112-9 of the Public Health Code.

#### Methods of declaration and receipt

The declaration can be sent by mail or made directly online on the official website of the Order of Physicians or Dentists.

When the National Council of the Order of Physicians receives the declaration and all the necessary supporting documents, it sends the provider a receipt specifying his registration number and the discipline exercised.

**To note**

The service provider shall inform the competent national health insurance body in advance of its provision of services by sending a copy of this receipt or by any other means.

*To go further*: Articles R. 4112-9-2 and R. 4112-11 of the Public Health Code.

#### Time limit

Within one month of receiving the declaration, the National Council of the Order informs the applicant:

* whether or not he can start the provision of services;
* when the verification of professional qualifications reveals a substantial difference with the training required in France, he must prove that he has acquired the missing knowledge and skills by submitting an aptitude test. If he satisfies this check, he is informed within one month that he can start providing services;
* when the examination of the file reveals a difficulty requiring additional information, the reasons for the delay in examining the file. He then has one month to obtain the additional information requested. In this case, before the end of the second month from receipt of this information, the National Council informs the service provider, after re-examination of his file:
  * whether or not he can start the provision of services,
  * when the verification of the service provider's professional qualifications reveals a substantial difference with the training required in France, he must demonstrate that he has acquired the missing knowledge and skills, in particular by passing an aptitude test.

In the latter case, if he satisfies this control, he is informed within one month that he can start the provision of services. Otherwise, he is informed that he cannot start the provision of services.

In the absence of a response from the National Council of the Order within these deadlines, the provision of services can begin.
In total, the application is processed within three months, to which an additional month may be added if the diploma falls under the general system of recognition of qualifications.

*To go further*: article R. 4112-9-1 of the Public Health Code.

#### Vouchers

The prior declaration must be accompanied by a declaration concerning the linguistic knowledge necessary for the performance of the service and the following supporting documents:

* the prior declaration form for the provision of services, the model of which is appended to the decree of 20 January 2010 relating to the prior declaration of the provision of services for the exercise of the professions of doctor, dentist and midwife, completed, dated and signed. The information requested relates to:
  * the identity of the applicant,
  * on the profession concerned,
  * on professional insurance,
  * for a renewal, on the periods of service provision and on the professional activities carried out;
* a copy of a valid identity document or a document certifying the nationality of the applicant;
* a copy of the training certificate (s), accompanied, where applicable, by a translation by a certified translator;
* a certificate from the competent authority of the State of establishment of the EU or the EEA certifying that the person concerned is legally established in that State and that he does not incur any prohibition to practice, accompanied, where applicable, a translation into French prepared by a certified translator.

**To note**

The control of language proficiency must be proportionate to the activity to be performed and carried out once the professional qualification has been recognized.

*To go further*: Articles L. 4112-7 of the Public Health Code; Order of 20 January 2010 relating to the prior declaration of the provision of services for the exercise of the professions of doctor, dentist and midwife.

#### Cost

Free.

### vs. If necessary, request an individual authorization to exercise

#### For EU or EEA nationals

The following can apply for an individual authorization from EU or EEA nationals holding a training certificate:

* issued by one of these States and allowing the exercise of the profession in that State, but not benefiting from automatic recognition (see above "2 °. c. EU and EEA nationals: with a view to permanent exercise ”);
* issued by a third state but recognized by a member state of the EU or the EEA, provided that they can prove that they have practiced the profession of doctor in the specialty for a period equivalent to three years full time in that state member.

An exercise authorization committee (CAE) examines the applicant's training and professional experience.

It can offer a compensation measure:

* when the training is at least one year less than that of the French DE, when it covers substantially different subjects or when one or more components of the professional activity the exercise of which is subject to the aforementioned diploma n ' do not exist in the corresponding profession in the home Member State or have not been educated in that State;
* when the training and experience of the applicant are not such as to cover these differences.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

* offer the applicant the choice between an adaptation internship or an aptitude test;
* impose an adaptation period or an aptitude test;
* impose an adaptation course and an aptitude test.

The purpose of the ** aptitude test ** is to verify, by written or oral tests or by practical exercises, the applicant's ability to practice the profession of doctor in the specialty concerned. It relates to subjects which are not covered by the applicant's training title (s) or by his professional experience.

The purpose of the ** adaptation internship ** is to enable those concerned to acquire the skills necessary for the exercise of the profession of doctor. It is performed under the responsibility of a doctor and may be accompanied by optional additional theoretical training. The duration of the internship does not exceed three years. It can be done part time.

*To go further*: Articles L. 4111-2 II, L. 4131-1-1, R. 4111-17 to R. 4111-20 and R. 4131-29 of the Public Health Code.

#### For third country nationals

Can apply for an individual authorization to exercise, provided that they demonstrate a sufficient level of command of the French language, persons holding a training certificate:

* issued by an EU or EEA state with proven experience by any means;
* issued by a third country allowing the practice of the profession of doctor in the country of graduation, if they pass anonymous tests to verify fundamental and practical knowledge. For more information on these tests, it is advisable to refer to the official website of the National Management Center (CNG), followed by three years of service in a service or organization approved for the training of interns.

**To note**

Doctors holding a specialized studies diploma obtained as part of the internship abroad are deemed to have passed the knowledge check tests.

*To go further*: Articles L. 4111-2 (I and I bis), D. 4111-1, D. 4111-6 and R. 4111-16-2 of the Public Health Code.

#### Competent authority

The request is sent in two copies, by registered letter with acknowledgment of receipt to the unit responsible for the exercise authorization commissions (CAE) of the CNG.

The authorization to exercise is issued by the Minister of Health after consulting the CAE.

*To go further*: Articles R. 4111-14 and R. 4131-29 of the Public Health Code; decree of February 25, 2010 setting the composition of the file to be provided to the CAEs competent for the examination of applications submitted for the exercise in France of the professions of doctor, dental surgeon, midwife and pharmacist.

#### Time limit

The CNG acknowledges receipt of the request within one month of receipt.

Silence maintained for a certain period of time from receipt of the complete file constitutes a decision to reject the request. This deadline is:

* four months for applications submitted by EU or EEA nationals holding a diploma issued in one of these states;
* six months for applications submitted by third country nationals holding a diploma issued by an EU or EEA state;
* one year for other requests. This period may be extended by two months, by decision of the ministerial authority notified at the latest one month before the expiry thereof, in the event of serious difficulty relating to the assessment of the candidate's professional experience.

*To go further*: Articles R. 4111-2, R. 4111-14 and R. 4131-29 of the Public Health Code.

#### Vouchers

The application file must contain:

* an application form for authorization to exercise the profession, the model of which appears in appendix 1 of the decree of 25 February 2010, completed, dated and signed and showing, if applicable, the specialty in which the candidate submits his application;
* a photocopy of a valid identity document;
* a copy of the training certificate allowing the exercise of the profession in the State where it was obtained as well as, where applicable, a copy of the specialist training certificate;
* where applicable, a copy of additional diplomas;
* all useful documents justifying continuing training, experience and skills acquired during professional practice in an EU or EEA State, or in a third country (certificates of employment, balance sheet activity, operative assessment, etc.);
* in the context of functions exercised in a State other than France, a declaration from the competent authority of that State, dated less than one year, attesting to the absence of sanctions against the applicant.

Depending on the applicant's situation, other supporting documents are required. For more information, it is advisable to refer to the official website of the CNG.

**To know**

The supporting documents must be written in French or translated by a certified translator.

*To go further*: decree of February 25, 2010 fixing the composition of the file to be provided to the competent practice authorization committees for the examination of requests submitted for the practice in France of the professions of doctor, surgeon- dentist, midwife and pharmacist; Government instruction of November 17, 2014 n ° DGOS / RH1 / RH2 / RH4 / 2014/318.

### d. Remedies

#### French support centre

The ENIC-NARIC is the French centre for information on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided by the national administration in each EU and EEA country. It aims to find solutions to problems between an EU citizen and the government of another of these countries. SOLVIT is heavily involved in the recognition of professional qualifications.

##### Conditions

Applicants may use SOLVIT’s services if they are able to prove:

* that the public authorities of an EU Member State have breached their EU rights as a citizen or business of another EU Member State
* that they have not yet taken the case to court (administrative appeals do not count)

##### Procedure

The citizen must complete an [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once the form has been submitted, SOLVIT contacts him/her within one week to ask for further information, if necessary, and to check whether the problem falls within its remit.

##### Supporting documents

To refer a problem to SOLVIT, the citizen must provide:

* his/her full contact details
* a detailed description of the problem
* all relevant evidence(e.g. correspondence and decisions received from the administrative authority in question)

##### Timeframe

SOLVIT aims to find solutions within 10 weeks – starting on the day the case is taken on by the SOLVIT centre in the country where the problem occurred.

##### Cost

Free-of-charge.

##### End of the procedure

At the end of these 10 weeks, SOLVIT puts forward a solution:

* If the solution resolves the problem concerning the application of EU law, it is accepted and the case is closed
* If the problem is not settled, the case is closed as unresolved and referred to the European Commission

##### Further information

SOLVIT in France: Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris, [official website of the Secretariat General for European Affairs](https://sgae.gouv.fr/sites/SGAE/accueil.html).