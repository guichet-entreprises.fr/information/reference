﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP193" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Oculariste" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="oculariste" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/oculariste.html" -->
<!-- var(last-update)="2020-04-15 17:21:48" -->
<!-- var(url-name)="oculariste" -->
<!-- var(translation)="None" -->

# Oculariste

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:48<!-- end-var -->

## 1°. Définition de l'activité

L'oculariste est un professionnel de santé spécialisé dans la conception, la fabrication et l'adaptation de prothèses oculaires sur les patients ayant subi une malformation ou une mutilation du globe oculaire.

Il s’occupera de prendre les empreintes, de sculpter et de mouler dans de la résine ou du verre, une prothèse oculaire pour combler esthétiquement l'absence d'un œil.

*Pour aller plus loin* : article D. 4364-3 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession d'oculariste est réservée aux titulaires du diplôme d'État d'oculariste.

Toutefois, peuvent également exercer la profession, les personnes justifiant soit :

- d'être titulaires du diplôme universitaire de prothèse oculaire et qui possèdent une attestation validant une expérience professionnelle de trois années passées chez un oculariste ou un service de santé spécialisé dans la confection de prothèse ;
- d'une compétence professionnelle reconnue par le ministère chargé des anciens combattants et victimes de guerre, ou les organismes d'assurance maladie ;
- d'une compétence professionnelle reconnue par une commission nationale regroupant des professionnels de l'appareillage de personnes handicapées.

*Pour aller plus loin* : articles L. 4364-1 et D. 4364-9 du Code de la santé publique  ; articles 5 et 8 de l'arrêté du 1er février 2011 relatif aux professions de prothésiste et d'orthésiste pour l'appareillage de personnes handicapées.

#### Formation

La profession d'oculariste est ouverte aux titulaires d'un diplôme à partir du niveau V au moins qui peut être un certificat d'aptitude professionnelle (CAP) en prothèse orthèse, podo-orthèse ou un brevet de technicien supérieur (BTS) en prothèse-orthèse. Un diplôme universitaire en prothèse oculaire appliquée est également proposé dans plusieurs facultés de médecine.

Pour plus d'informations, il est conseillé de se rapprocher des établissements délivrant ces titres.

#### Coûts associés à la qualification

La formation est payante quelle que soit la formation choisie. Pour plus d'informations, il est conseillé de se rapprocher des établissements concernés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE), exerçant légalement l’activité d'oculariste dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet de région dans laquelle il souhaite faire la prestation (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : articles L. 4364-6 et R. 4364-11-3 du Code de la santé publique .

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité d'oculariste dans cet État, peut exercer la même activité en France de manière permanente s'il :

- est titulaire d'un titre de formation délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une autorisation individuelle d'exercice auprès du préfet de région dans laquelle il souhaite exercer sa profession (cf. infra « 5°. b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

*Pour aller plus loin* : article L. 4364-5 du Code de la santé publique .

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le ressortissant qui souhaite exercer la profession d'oculariste en France est tenu de respecter des règles de bonne pratique, et notamment :

- celles relatives au secret professionnel ;
- d'exercer dans des locaux accessibles à des personnes handicapées, préservant l'intimité du patient et conformes aux règles d'hygiène et de sécurité ;
- d'exécuter la prescription médicale sans y effectuer des modifications sauf si elles sont médicalement ou techniquement nécessaires ;
- de faire enregistrer son diplôme au répertoire ADELI (cf. infra « 5°. c. Enregistrement auprès du répertoire ADELI »).

*Pour aller plus loin* : articles D. 4364-12 à D. 4364-18 du Code de la santé publique  ; articles 13 à 26 de l'arrêté du 1er février 2011.

## 4°. Assurance

En qualité de professionnel de santé, l'oculariste exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet de région est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de la profession dans l’État d'obtention ;
- une attestation, datant de moins de trois mois, de l'autorité compétente de l’État de l'UE ou de l'EEE, certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- toutes pièces justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État de l'UE ou de l'EEE autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l’État ayant reconnu ce titre,
  - toutes pièces justifiant que le ressortissant a exercé la profession dans cet État pendant trois ans ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée ;
- une attestation de responsabilité civile professionnelle.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

À réception du dossier, le préfet de région disposera d'un délai d'un mois pour se prononcer sur la demande et informera le ressortissant :

- qu'il peut débuter la prestation. Dès lors, le préfet enregistrera le demandeur au répertoire ADELI ;
- qu'il sera soumis à une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France ;
- qu'il ne pourra pas débuter la prestation ;
- de toute difficulté pouvant retarder sa décision. Dans ce dernier cas, le préfet pourra rendre sa décision dans les deux mois suivant la résolution de cette difficulté, et au plus tard dans les trois mois de sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaudra acceptation de la demande de déclaration.

**À noter**

La déclaration est renouvelable tous les ans ou à chaque changement de situation du demandeur.

*Pour aller plus loin* : arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du de la santé publique.

### b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

#### Autorité compétente

L'autorisation d'exercice est délivrée par le préfet de région, après avis de la commission des prothésistes et orthésistes pour l'appareillage des personnes handicapées.

#### Pièces justificatives

La demande d'autorisation s'effectue par le dépôt d'un dossier comprenant l'ensemble des documents suivants :

- une copie d'une pièce d'identité en cours de validité et un document mentionnant les coordonnées du ressortissant ;
- une lettre de demande d'autorisation d'exercice ;
- la description détaillée de l'activité professionnelle du ressortissant ;
- les références des médecins avec lesquels il a l'habitude de travailler et leurs appréciations sur les prestations fournies par le candidat ;
- tout document permettant d'attester de son expérience professionnelle fourni par l'employeur ;
- dans la mesure du possible, une lettre de son employeur décrivant son activité avec ses appréciations.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet accuse réception du dossier dans le délai d'un mois et se prononcera après avoir eu l'avis de la commission des prothésistes et orthésistes pour l'appareillage des personnes handicapées. Cette dernière est chargée d'examiner les connaissances et les compétences du ressortissant acquises lors de sa formation ou au cours de son expérience professionnelle. Elle pourra soumettre le ressortissant à une mesure de compensation.

Le silence gardé du préfet de région dans un délai de quatre mois vaut décision de rejet de la demande d'autorisation.

*Pour aller plus loin* : articles R. 4364-11 à R. 4364-11-2 du Code de la santé publique  ; articles 10 à 12 de l'arrêté du 1er février 2011.

#### Bon à savoir : mesures de compensation

Si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession d'oculariste et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente pourra soit :

- proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation et/ou une épreuve d’aptitude ;

*Pour aller plus loin* : article L. 4364-5 du Code de la santé publique.

### c. Enregistrement auprès du répertoire ADELI

Le ressortissant souhaitant exercer la profession d'oculariste en France est tenu de faire enregistrer son autorisation d'exercer sur le répertoire ADELI (« Automatisation Des Listes »).

#### Autorité compétente

L’enregistrement au répertoire Adeli se fait auprès de l’agence régionale de santé (ARS) du lieu d’exercice.

#### Délai

La demande d’enregistrement est présentée dans le mois suivant la prise de fonction du ressortissant, quel que soit le mode d’exercice (libéral, salarié, mixte).

#### Pièces justificatives

À l'appui de sa demande d'enregistrement, l'oculariste doit fournir un dossier comportant :

- le diplôme original ou titre attestant de la formation d'oculariste délivrée par l'État de l'UE ou de l'EEE (traduit en français par un traducteur agréé, le cas échéant) ;
- une pièce d’identité ;
- le formulaire Cerfa 10906-06 complété, daté et signé.

#### Issue de la procédure

Le numéro Adeli du ressortissant sera directement mentionné sur le récépissé du dossier, délivré par l'ARS.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 4364-2 et D. 4364-18 du Code de la santé publique .

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).