﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP213" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Pharmacien" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="pharmacien" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/pharmacien.html" -->
<!-- var(last-update)="2020-04-15 17:21:58" -->
<!-- var(url-name)="pharmacien" -->
<!-- var(translation)="None" -->

# Pharmacien

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:58<!-- end-var -->

## 1°. Définition de l'activité

Le pharmacien est un professionnel de santé dont la mission principale est de délivrer des médicaments au public. Dans le cadre de son activité, il va s'assurer de la conformité des doses prescrites par le médecin à une personne malade, ou encore lui expliquer le traitement. Il peut également être amené à réaliser, sur prescription, des préparations spécifiques.

En outre, il est amené à effectuer des tâches plus administratives. Ainsi, il doit gérer la comptabilité de son officine, commander et réceptionner les produits, ainsi que gérer les stocks.

Lorsqu'il travaille en laboratoire, il va analyser les prélèvements, fabriquer de nouveaux médicaments et surveiller ceux existants.

*Pour aller plus loin* : article L. 4211-1 du Code de la santé publique 

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

En application de l'article L. 4221-1 du Code de la santé publique, pour exercer légalement la profession de pharmacien en France, les intéressés doivent remplir cumulativement les trois conditions suivantes :

- être titulaire du diplôme français d’État de pharmacien ou d’un diplôme, certificat ou autre titre mentionnés aux articles L. 4221-2 à L. 4221-5 du Code de la santé publique (cf. infra « Bon à savoir : la reconnaissance automatique de diplôme ») ;
- être de nationalité française, citoyen andorran, ressortissant d'un État membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, ou ressortissant d'un pays dans lequel les Français peuvent exercer la pharmacie lorsqu'ils sont titulaires du diplôme qui en ouvre l'exercice aux nationaux de ce pays ;
- sauf exception, être inscrit au tableau du Conseil de l’Ordre des pharmaciens (cf. infra « 5°. b. Demander son inscription au tableau de l’Ordre des pharmaciens »).

#### Bon à savoir : lareconnaissance automatique de diplôme

En application des articles L. 4221-2 à L. 4221-5 du Code de la santé publique, les ressortissants de l'UE ou de l'EEE peuvent exercer la profession de pharmacien s'ils sont titulaires d'un des titres suivants :

- un des titres de pharmacien délivrés par un État de l'UE ou de l'EEE cités en annexe de l'arrêté du 13 février 2007 ;
- un titre de pharmacien délivré par un État de l'UE ou de l'EEE ne figurant pas sur la liste de l'annexe ci-dessus, accompagné d'une attestation de cet État ;
- un titre délivré par un État de l'UE ou de l'EEE sanctionnant une formation de pharmacien commencée avant l'une des dates citées dans l'arrêté du 13 février 2007, et accompagné d'une attestation certifiant que le ressortissant s'est consacré légalement aux activités de pharmacien pendant au moins trois années au cours des cinq années précédant la délivrance de l'attestation ;
- un titre délivré par un État de l'UE ou de l'EEE, sanctionnant une formation de pharmacien commencée avant l'une des dates citées dans l'arrêté du 13 février 2007, et permettant d'exercer légalement la profession de pharmacien dans cet État. Le ressortissant devra justifier avoir effectué en France trois années dans la fonction hospitalière en qualité d'attaché associé, de praticien attaché associé, d'assistant associé ou dans la fonction universitaire en qualité de chef de clinique associé des universités ou d'assistant associé des universités.

*Pour aller plus loin* : article L. 4221-2 à L. 4221-5 du Code de la santé publique ; arrêté du 13 février 2007 fixant la liste des diplômes, certificats et autres titres de pharmacien délivrés par les États membres de l'Union européenne, les États parties à l'accord sur l'Espace économique européen et la Confédération suisse visée à l'article L. 4221-4 (1°) du Code de la santé publique.

#### Formation

Les études de pharmacie sont composées de trois cycles d'une durée totale comprise entre six et neuf ans selon la filière choisie.

##### Diplôme de formation générale en sciences pharmaceutiques

Le premier cycle est sanctionné par le diplôme de formation générale en sciences pharmaceutiques. Il comprend six semestres et correspond au niveau licence. Les deux premiers semestres correspondent à la PACES.

La formation a pour objectif d'acquérir :

- les connaissances de base dans le domaine des sciences exactes et des sciences biologiques ;
- une connaissance spécifique des disciplines nécessaires à l'étude du médicament et des autres produits de santé ;
- les compétences nécessaires à la bonne utilisation de ces connaissances ;
- les éléments utiles à l'orientation de l'étudiant vers les différents métiers de la pharmacie touchant notamment les domaines de l'officine et des pharmacies à usage intérieur, de la biologie médicale, de l'industrie et de la recherche.

Elle comprend des enseignements théoriques, méthodologiques, appliqués et pratiques ainsi que l’accomplissement d'un stage obligatoire en officine et de deux stages optionnels.

*Pour aller plus loin* : arrêté du 22 mars 2011 relatif au régime des études en vue du diplôme de formation générale en sciences pharmaceutiques.

##### Diplôme de formation approfondie en sciences pharmaceutiques

Le deuxième cycle est sanctionné par le diplôme de formation approfondie en sciences pharmaceutiques. Peuvent s'inscrire à la formation les titulaires du diplôme de formation générale en sciences pharmaceutiques. Il comprend quatre semestres de formation et correspond au niveau master.

Il a pour objectif :

- l'acquisition de connaissances scientifiques, médicales et pharmaceutiques, complétant et approfondissant celles acquises au cours du premier cycle ;
- une formation à la démarche scientifique ;
- l'acquisition de connaissances pratiques ;
- l'apprentissage du travail en équipe et l'acquisition des techniques de communication ;
- l'introduction au développement professionnel continu.

Outre les enseignements théoriques et pratiques, la formation comprend l'accomplissement :

- d'un stage en officine d'une à deux semaines ;
- d'une stage hospitalier de douze mois à mi-temps ou six mois à temps plein.

Le deuxième cycle est validé par la réussite au contrôle de connaissances des enseignements dispensés au cours de la formation, ainsi que la délivrance d'un certificat de synthèse pharmaceutique.

*Pour aller plus loin* : articles 4 à 17 de l'arrêté du 8 avril 2013 relatif au régime des études en vue du diplôme d’État de docteur en pharmacie.

##### Diplôme d’État de docteur en pharmacie

Le troisième cycle est sanctionné par la délivrance du diplôme d’État de docteur en pharmacie. Il comporte :

- un cycle court de deux semestres de formation ;
- d'un cycle long de huit semestres de formation pour les étudiants reçus au concours de l'internat ;
- la soutenance d'une thèse.

Le troisième cycle court doit permettre à l'étudiant :

- d'approfondir les connaissances et les compétences afférentes à l'orientation professionnelle choisie et éventuellement de s'engager dans une spécialisation pharmaceutique particulière, spécifique à cette orientation professionnelle ;
- de préparer sa thèse en vue de l'obtention du diplôme d’État de docteur en pharmacie.

Il est accompagné d'un stage pratique professionnelle d'une durée de six mois à temps plein, qui peut être accompli dans une officine, une pharmacie mutualiste, une pharmacie d'une société de secours minière, un établissement pharmaceutique ou dans un établissement industriel ou commercial dont les activités sont susceptibles de concourir à la formation du pharmacien.

Dans un délai de deux ans après la validation des enseignements dispensés au cours du troisième cycle court, l'étudiant doit soutenir une thèse devant un jury.

*Pour aller plus loin* : article L. 633-2 du Code de l'éducation ; article 18 et suivants de l'arrêté du 8 avril 2013 relatif au régime des études en vue du diplôme d’État de docteur en pharmacie.

#### Coûts associés à la qualification

La formation menant à l’obtention du DE de docteur en pharmacie est payante. Son coût varie selon les universités qui dispensent les enseignements. Pour plus d'informations, il est conseillé de se rapprocher de l’université considérée.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le professionnel ressortissant d’un État de l'UE ou de l'EEE qui est établi et exerce légalement son activité dans l’un de ces États peut exercer en France, de manière temporaire et occasionnelle, la même activité et ce, sans être inscrit au tableau de l'Ordre des pharmaciens.

Pour cela, le professionnel doit effectuer une déclaration préalable, ainsi qu'une déclaration justifiant qu'il possède les connaissances linguistiques nécessaires pour exercer en France (cf. infra « 5°. a. Effectuer une déclaration préalable d'activité pour les ressortissants de l'UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

**À savoir**

L’inscription au tableau de l’Ordre des pharmaciens n’est pas requise pour le professionnel en situation de libre prestation de services (LPS). Il n'est donc pas tenu de s’acquitter des cotisations ordinales. Le pharmacien est simplement enregistré sur une liste spécifique tenue par le Conseil national de l’Ordre des pharmaciens.

La déclaration préalable doit être accompagnée d’une déclaration concernant les connaissances linguistiques nécessaires à la réalisation de la prestation. Dans cette hypothèse, le contrôle de la maîtrise de la langue doit être proportionné à l’activité à exercer et réalisé une fois la qualification professionnelle reconnue.

Lorsque les titres de formation ne bénéficient pas d’une reconnaissance automatique (cf. supra « 2°. a. Législation nationale »), les qualifications professionnelles du prestataire sont vérifiées avant la première prestation de services. En cas de différences substantielles entre les qualifications de l’intéressé et la formation exigée en France de nature à nuire à la santé publique, le prestataire est soumis à une épreuve d’aptitude.

Le pharmacien en situation de LPS est tenu de respecter les règles professionnelles applicables en France, notamment l’ensemble des règles déontologiques (cf. infra « 3°. Conditions d'honorabilité, règles déontologiques, éthique »). Il est soumis à la juridiction disciplinaire de l’Ordre des pharmaciens.

**À noter**

La prestation est réalisée sous le titre professionnel français de pharmacien. Toutefois, lorsque les titres de formation ne bénéficient pas d’une reconnaissance et dans le cas où les qualifications n’ont pas été vérifiées, la prestation est réalisée sous le titre professionnel de l’État d’établissement, de manière à éviter toute confusion avec le titre professionnel français.

*Pour aller plus loin* : article L. 4222-9 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

#### Le régime de reconnaissance automatique du diplôme

L'article L. 4221-4 du Code de la santé publique crée un régime de reconnaissance automatique en France de certains diplômes ou titres, le cas échéant, accompagnés de certificats, obtenus dans un État de l’UE ou de l’EEE (cf. supra « 2°. a. Législation nationale »).

Il appartient au Conseil national de l’Ordre des pharmaciens de vérifier la régularité des diplômes, titres, certificats et attestations, d’en accorder la reconnaissance automatique puis de statuer sur la demande d’inscription au tableau de l’Ordre.

*Pour aller plus loin* : article L. 4221-1 du Code de la santé publique.

#### Le régime de l'autorisation individuelle d'exercer

Si le ressortissant de l’UE ou de l’EEE ne remplit pas les conditions pour bénéficier du régime de reconnaissance automatique de ses titres ou diplômes, il relève d’un régime d’autorisation d’exercice (cf. infra « 5°. c. Le cas échéant, demander une autorisation individuelle d’exercice »).

Les personnes qui ne bénéficient pas de la reconnaissance automatique mais qui sont titulaires d’un titre de formation permettant d’exercer légalement la profession de pharmacien, peuvent être individuellement autorisées à exercer dans la spécialité concernée par le ministre chargé de la santé, après avis d’une commission composée notamment de professionnels.

Si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession dans la spécialité concernée et son exercice en France, l’intéressé doit se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente peut soit :

- proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation et une épreuve.

*Pour aller plus loin* : article L. 4221-1-1 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Respect du Code de déontologie des pharmaciens

Les dispositions du Code de déontologie s’imposent à tous les pharmaciens exerçant en France, qu'ils soient inscrits au tableau de l’Ordre ou qu’ils soient dispensés de cette obligation (cf. supra « 5°. b. Demander son inscription au tableau de l’Ordre des pharmaciens »).

**À savoir**

L’ensemble des dispositions du Code de déontologie est codifié aux articles R. 4235-1 à R. 4235-77 du Code de la santé publique.

À ce titre, les pharmaciens doivent notamment respecter les principes de dignité, de non-discrimination, de secret professionnel ou encore d'indépendance.

*Pour aller plus loin* : articles L. 4235-1, et R. 4235-1 à R. 4235-77 du Code de la santé publique.

### b. Cumul d'activités

Le pharmacien ne peut exercer une autre activité que si un tel cumul est compatible avec les principes d’indépendance et de dignité professionnelles qui s’imposent à lui.

Il est également interdit à un pharmacien qui remplit un mandat électif ou une fonction administrative d’en user pour accroître sa clientèle.

*Pour aller plus loin* : articles R. 4235-4 et R. 4235-23 du Code de la santé publique.

### c. Obligation de développement professionnel continu

Les pharmaciens doivent participer annuellement à un programme de développement professionnel continu. Ce programme vise à maintenir et actualiser leurs connaissances et leurs compétences ainsi qu'à améliorer leurs pratiques professionnelles.

À ce titre, le professionnel de santé (salarié ou libéral) doit justifier de son engagement dans une démarche de développement professionnel. Le programme se présente sous la forme de formations (présentielles, mixtes ou non-présentielles) d’analyse, d’évaluation, et d’amélioration des pratiques et de gestion des risques. L’ensemble des formations suivies est consigné dans un document personnel contenant les attestations de formation.

*Pour aller plus loin* : article R. 4235-11 du Code de la santé publique ; décret n° 2016-942 du 8 juillet 2016 relatif à l'organisation du développement professionnel continu des professionnels de santé.

## 4°. Assurance

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, le pharmacien exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

### b. Obligation d'affiliation à la caisse d'assurance vieillesse des pharmaciens (CAVP)

Tout pharmacien inscrit au tableau de l'Ordre des pharmaciens et exerçant sous la forme libérale (même à temps partiel et même s’il exerce par ailleurs une activité salariée) a l’obligation d’adhérer à la CAVP.

L’intéressé doit se déclarer à la CAVP dans le mois suivant le début de son activité libérale.

*Pour aller plus loin* : article R. 643-1 du Code de la sécurité sociale ; site de la [CAVP](https://www.cavp.fr/).

### c. Obligation de déclaration auprès de l’Assurance maladie

Une fois inscrit au tableau de l’Ordre, le pharmacien exerçant sous forme libérale doit déclarer son activité auprès de la Caisse primaire d’assurance maladie (CPAM).

#### Modalités

L’inscription auprès de la CPAM peut être réalisée en ligne sur le site officiel de l’Assurance maladie.

#### Pièces justificatives

Le déclarant doit communiquer un dossier complet comprenant :

- la copie d’une pièce d’identité en cours de validité ;
- un relevé d’identité bancaire (RIB) professionnel.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d'activité pour les ressortissants de l'UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

Tout ressortissant de l’UE ou de l’EEE qui est établi et exerce légalement les activités de pharmacien dans l’un de ces États peut exercer en France de manière temporaire ou occasionnelle s’il en fait la déclaration préalable (cf. supra « 2°. b. Ressortissants UE et EEE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services) »).

La déclaration préalable doit être renouvelée tous les ans.

**À noter**

Tout changement de situation du demandeur doit être notifié dans les mêmes conditions.

#### Autorité compétente

La déclaration doit être adressée, avant la première prestation de services, au Conseil national de l’Ordre des pharmaciens.

#### Modalités de la déclaration et récépissé

La déclaration peut être envoyée par courrier ou directement effectuée en ligne sur le site officiel de l’Ordre des pharmaciens.

Lorsque le Conseil national de l’Ordre reçoit la déclaration et l’ensemble des pièces justificatives nécessaires, il adresse au prestataire un récépissé précisant son numéro d’enregistrement ainsi que la discipline exercée.

**À noter**

Le prestataire de services informe préalablement l’organisme national d’assurance maladie compétent de sa prestation de services par l’envoi d’une copie de ce récépissé ou par tout autre moyen.

#### Délai

Dans un délai d’un mois à compter de la réception de la déclaration, le Conseil national de l’Ordre informe le demandeur :

- qu’il peut ou non débuter la prestation de services ;
- lorsque la vérification des qualifications professionnelles met en évidence une différence substantielle avec la formation exigée en France, qu’il doit prouver avoir acquis les connaissances et les compétences manquantes en se soumettant à une épreuve d’aptitude. S’il satisfait à ce contrôle, il est informé dans un délai d’un mois qu’il peut débuter la prestation de services ;
- lorsque l’examen du dossier met en évidence une difficulté nécessitant un complément d’informations, des raisons du retard pris dans l’examen de son dossier. Il dispose alors d’un délai d’un mois pour obtenir les compléments d’information demandés. Dans ce cas, avant la fin du deuxième mois à compter de la réception de ces informations, le Conseil national informe le prestataire, après réexamen de son dossier :
  - qu’il peut ou non débuter la prestation de services,
  - lorsque la vérification des qualifications professionnelles du prestataire met en évidence une différence substantielle avec la formation exigée en France, qu’il doit démontrer qu’il a acquis les connaissances et compétences manquantes, notamment en se soumettant à une épreuve d’aptitude.

Dans cette dernière hypothèse, s’il satisfait à ce contrôle, il est informé dans le délai d’un mois qu’il peut débuter la prestation de services. Dans le cas contraire, il est informé qu’il ne peut pas débuter la prestation de services. En l’absence de réponse du Conseil national de l’Ordre dans ces délais, la prestation de services peut débuter.

#### Pièces justificatives

La déclaration préalable doit être accompagnée d’une déclaration concernant les connaissances linguistiques nécessaires à la réalisation de la prestation et des pièces justificatives suivantes :

- le [formulaire de déclaration préalable de prestation de services](http://www.ordre.pharmacien.fr/content/download/376629/1813657/version/1/file/Formulaire+-+Arr%C3%AAt%C3%A9+du+8+d%C3%A9cembre+2017+relatif+%C3%A0+la+d%C3%A9claration+pr%C3%A9alable+de+prestation+de+services+.pdf) ;
- la copie d’une pièce d’identité en cours de validité ou d’un document attestant la nationalité du demandeur ;
- la copie du ou des titres de formation, accompagnée, le cas échéant, d’une traduction réalisée par un traducteur agréé ;
- une attestation de l’autorité compétente de l’État d’établissement de l’UE ou de l’EEE certifiant que l’intéressé est légalement établi dans cet État et qu’il n’encourt aucune interdiction d’exercer, accompagnée, le cas échéant, d’une traduction en français établie par un traducteur agréé.

**À noter**

Le contrôle de la maîtrise de la langue doit être proportionné à l’activité à exercer et réalisé une fois la qualification professionnelle reconnue.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 4222-9 à L. 4222-10, et R.4112-9 à R. 4112-12 du Code de la santé publique.

### b. Formalités pour les ressortissants de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Le cas échéant, demander une autorisation individuelle d’exercice

Si le ressortissant ne relève pas du régime de reconnaissance automatique de son diplôme, il doit solliciter une autorisation d'exercer.

##### Autorité compétente

La demande est adressée en deux exemplaires, par lettre recommandée avec demande d’avis de réception à la cellule chargée des commissions d’autorisation d’exercice (CAE) du Centre national de gestion (CNG).

Il doit adjoindre à sa demande :

- le [formulaire de demande d’autorisation d’exercice de la profession](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4349F01DDAE283C55604F0C28FE06D3D.tplgfr23s_3?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180118) ;
- une photocopie d’une pièce d’identité en cours de validité ;
- une copie du titre de formation permettant l’exercice de la profession dans l’État d’obtention ainsi que, le cas échéant, une copie du titre de formation de spécialiste ;
- le cas échéant, une copie des diplômes complémentaires ;
- toutes pièces utiles justifiant des formations continues, de l’expérience et des compétences acquises au cours de l’exercice professionnel dans un État de l’UE ou de l’EEE, ou dans un État tiers (attestations de fonctions, bilan d’activité, bilan opératoire, etc.) ;
- dans le cadre de fonctions exercées dans un État autre que la France, une déclaration de l’autorité compétente de cet État, datant de moins d’un an, attestant l’absence de sanctions à l’égard du demandeur.

Selon la situation du demandeur, d’autres pièces justificatives sont exigées. Pour plus d’informations, il est conseillé de se reporter au site officiel du [CNG](http://www.cng.sante.fr/).

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé.

##### Délai

Le CNG accuse réception de la demande dans le délai d’un mois à compter de sa réception.

Le silence gardé pendant un certain délai à compter de la réception du dossier complet vaut décision de rejet de la demande. Ce délai est porté à :

- quatre mois pour les demandes présentées par les ressortissants de l’UE ou de l’EEE titulaires d’un diplôme délivré dans l’un de ces États ;
- six mois pour les demandes présentées par les ressortissants d’États tiers titulaires d’un diplôme délivré par un État de l’UE ou de l’EEE ;
- un an pour les autres demandes.

Ce délai peut être prolongé de deux mois, par décision de l’autorité ministérielle notifiée au plus tard un mois avant l’expiration de celui-ci, en cas de difficulté sérieuse portant sur l’appréciation de l’expérience professionnelle du candidat.

##### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation et l'expérience professionnelle du ressortissant et celles requises pour exercer en France, la CNG peut décider soit :

- de proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- d'imposer un stage d’adaptation ou une épreuve d’aptitude ;
- d'imposer un stage d’adaptation et une épreuve d’aptitude.

**L’épreuve d’aptitude** a pour objet de vérifier, par des épreuves écrites ou orales ou par des exercices pratiques, l’aptitude du demandeur à exercer la profession de pharmacien dans la spécialité concernée. Elle porte sur les matières qui ne sont pas couvertes par le ou les titres de formation du demandeur ou de son expérience professionnelle.

**Le stage d’adaptation** a pour objet de permettre aux intéressés d’acquérir les compétences nécessaires à l’exercice de la profession de pharmacien. Il est accompli sous la responsabilité d’un pharmacien et peut être accompagné d’une formation théorique complémentaire facultative. La durée du stage n’excède pas trois ans. Il peut être effectué à temps partiel.

*Pour aller plus loin* : arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de médecin, chirurgien-dentiste, sage-femme et pharmacien.

#### Demander son inscription au tableau de l'Ordre

L'inscription au tableau de l'Ordre est obligatoire pour exercer légalement l'activité de pharmacien en France.

L'inscription ne s'applique pas :

- aux pharmaciens inspecteurs de santé publique, inspecteurs des agences régionales de santé, inspecteurs de l'Agence nationale de sécurité du médicament et des produits de santé ;
- aux pharmaciens fonctionnaires ou assimilés du ministère chargé de la santé ;
- aux pharmaciens fonctionnaires ou assimilés du ministère chargé de l'enseignement supérieur, n'exerçant pas par ailleurs d'activité pharmaceutique ;
- aux pharmaciens appartenant au cadre actif du service de santé des armées de terre, de mer et de l'air.

**À noter**

L’inscription au tableau de l’Ordre permet la délivrance automatique et gratuite de la carte de professionnel de santé (CPS). La CPS est une carte d’identité professionnelle électronique. Elle est protégée par un code confidentiel et contient notamment les données d’identification de pharmacien (identité, profession, spécialité). Pour plus d’informations, il est recommandé de se reporter au site gouvernemental de l’Agence française de la santé numérique.

##### Autorité compétente

L'article R. 4222-1 du Code de la santé publique prévoit que la demande d’inscription est adressée, selon son cas, au président du conseil régional de la région dans laquelle le pharmacien veut exercer, ou au président du conseil central de la section.

##### Pièces justificatives

L’intéressé doit adresser, par tout moyen, un dossier complet de demande d’inscription comprenant :

- deux exemplaires du questionnaire normalisé avec une photo d’identité rempli, daté et signé, disponible dans les conseils départementaux de l’Ordre ou directement téléchargeable sur le site officiel du Conseil national de l’Ordre des pharmaciens ;
- une photocopie d’une pièce d’identité en cours de validité ou, le cas échéant, une attestation de nationalité délivrée par une autorité compétente ;
- Le cas échéant, une photocopie de la carte de séjour de membre de la famille d’un citoyen de l’UE en cours de validité, de la carte de résident de longue durée-CE en cours de validité ou de la carte de résident portant mention du statut de réfugié en cours de validité ;
- le cas échéant, une photocopie de la carte professionnelle européenne en cours de validité ;
- une copie, accompagnée le cas échéant d’une traduction faite par un traducteur agréé, des titres de formation à laquelle sont joints :
  - lorsque le demandeur est un ressortissant de l’UE ou de l’EEE, la ou les attestations prévues (cf. supra « 2°. a. Exigences nationales »),
  - lorsque le demandeur bénéficie d’une autorisation d’exercice individuelle (cf. supra « 2°. c. Ressortissants UE et EEE : en vue d’un exercice permanent »), la copie de cette autorisation,
  - lorsque le demandeur présente un diplôme délivré dans un État étranger dont la validité est reconnue sur le territoire français, la copie des titres à la possession desquels cette reconnaissance peut être subordonnée ;
- pour les ressortissants d’un État étranger, un extrait de casier judiciaire ou un document équivalent datant de moins de trois mois, délivré par une autorité compétente de l’État d’origine. Cette pièce peut être remplacée, pour les ressortissants des États de l’UE ou de l’EEE qui exigent une preuve de moralité ou d’honorabilité pour l’accès à l’activité de médecin, par une attestation, datant de moins de trois mois, de l’autorité compétente de l’État d’origine certifiant que ces conditions de moralité ou d’honorabilité sont remplies ;
- une déclaration sur l’honneur du demandeur certifiant qu’aucune instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur l’inscription au tableau n’est en cours à son encontre ;
- un certificat de radiation, d’inscription ou d’enregistrement délivré par l’autorité auprès de laquelle le demandeur était antérieurement inscrit ou enregistré ou, à défaut, une déclaration sur l’honneur du demandeur certifiant qu’il n’a jamais été inscrit ou enregistré ou, à défaut, un certificat d’inscription ou d’enregistrement dans un État de l’UE ou de l’EEE ;
- tous les éléments de nature à établir que le demandeur possède les connaissances linguistiques nécessaires à l’exercice de la profession ;
- un curriculum vitae.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

##### Délai

Le conseil régional ou central statue sur la demande d’inscription dans un délai maximum de trois mois à compter de la réception du dossier complet de demande. À défaut de réponse dans ce délai, la demande d’inscription est réputée rejetée.

Ce délai est porté à six mois pour les ressortissants des États tiers lorsqu’il y a lieu de procéder à une enquête hors de la France métropolitaine. L’intéressé en est alors avisé.

Si l’inscription est accordée, la décision adoptée par le conseil est notifiée à l’intéressé par lettre recommandée, accompagnée de son certificat d’inscription.

La décision du conseil régional est également susceptible d’appel, dans les 30 jours, auprès du Conseil national de l’Ordre des pharmaciens. La décision ainsi rendue peut elle-même faire l’objet d’un recours devant le Conseil d’État.

##### Coût

L’inscription au tableau de l’Ordre est gratuite mais elle engendre l’obligation de s’acquitter de la cotisation ordinale obligatoire dont le montant est fixé annuellement et qui doit être versée au cours du premier trimestre de l’année civile en cours.

*Pour aller plus loin* : articles L. 4222-1 à L. 4222-8, et R. 4222-1 à R. 4222-4-3 du Code de la santé publique.

### c. Carte professionnelle européenne (CPE)

La carte professionnelle européenne (ou European professional card) est une procédure électronique permettant de faire reconnaître des qualifications professionnelles dans un autre État de l’UE.

La procédure CPE peut être utilisée lorsque le ressortissant souhaite exercer son activité dans un autre État de l'UE à titre temporaire et occasionnel, ou bien à titre permanent.

#### Procédure

Le ressortissant qui souhaite s'établir en France ou y effectuer une prestation de services, s'adresse à l'autorité compétente de son État et lui transmet un dossier par voie électronique. L'autorité compétente sera alors chargée d'envoyer la demande à la direction régionale de la jeunesse, des sports et de la cohésion sociale d’Île-de-France, qui la fera suivre au Conseil national de l'Ordre des pharmaciens.

#### Pièces justificatives

La demande de CPE doit être accompagnée de l'ensemble des pièces justificatives suivantes :

- une photocopie lisible d'une pièce d'identité en cours de validité ;
- une déclaration du Conseil national de l'Ordre des pharmaciens, attestant de l'inscription au tableau de l'Ordre et de l'absence de suspension ou d'interdiction d'exercice ;
- en cas de reconnaissance automatique, une copie des titres de formations et, le cas échéant, un certificat de conformité, un certificat de changement de dénomination ou une attestation de droits acquis ;
- en cas de reconnaissance de qualifications professionnelles, une copie des titres de formation, toutes pièces utiles pour fournir des informations sur la formation suivie, ainsi que tout document justifiant les qualifications requises ;
- en cas de reconnaissance d'un titre délivré par un État tiers, une attestation certifiant de trois ans d'expérience en France ;
- tout document justifiant d'une expérience professionnelle d'au moins un an au cours des dix dernières années, lorsque l’État ne réglemente ni la formation, ni l'exercice de la profession ;
- une attestation de responsabilité civile professionnelle.

Le demandeur pourra, en outre, joindre toute attestation de sa connaissance de la langue française.

#### Issue de la procédure

Après réception des pièces justificatives, le Conseil de l'Ordre pourra décider :

- de délivrer la CPE ;
- de refuser de délivrer la CPE par décision motivée et susceptible de recours ;
- de soumettre le professionnel à une mesure de compensation en cas de différence substantielle avec la formation requise pour exercer la profession en France

*Pour aller plus loin* : articles R. 4222-9 à R. 4222-11 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la mise en œuvre de la carte professionnelle européenne mentionnée à l'article L. 4002-2 du Code de la santé publique.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).