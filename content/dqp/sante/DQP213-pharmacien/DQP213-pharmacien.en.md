﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP213" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Pharmacist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="pharmacist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/pharmacist.html" -->
<!-- var(last-update)="2020-04-15 17:21:58" -->
<!-- var(url-name)="pharmacist" -->
<!-- var(translation)="Auto" -->


Pharmacist
==========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:58<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The pharmacist is a health professional whose primary mission is to deliver medicines to the public. As part of his activity, he will make sure that the doses prescribed by the doctor to a sick person are compliant, or explain the treatment. It may also be required to make specific preparations on prescription.

In addition, he is required to perform more administrative tasks. Thus, he must manage the accounting of his dispensary, order and receive the products, as well as manage the inventory.

When he works in the laboratory, he will analyze the samples, make new drugs and monitor existing ones.

*For further information*: Article L. 4211-1 of the Public Health Code

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Under Article L. 4221-1 of the Public Health Code, in order to legally practise as a pharmacist in France, those concerned must meet the following three conditions cumulatively:

- hold the French state diploma of pharmacist or a diploma, certificate or other title mentioned in articles L. 4221-2 to L. 4221-5 of the Code of Public Health (see infra "Good to know: automatic recognition of diploma");
- be a French national, Andorran citizen, national of a Member State of the European Union or party to the agreement on the European Economic Area, or a national of a country in which the French can practice pharmacy when they are holders of the diploma that opens the exercise to the nationals of this country;
- with exceptions, be listed on the Board of the College of Pharmacists (see infra "5°. b. Ask for inclusion on the College of Pharmacists list").

**Good to know: automatic graduation**

Under Articles L. 4221-2 to L. 4221-5 of the Public Health Code, EU or EEA nationals may practise as pharmacists if they hold one of the following titles:

- one of the pharmacist's certificates issued by an EU or EEA state cited in[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=D81969C59246FC9D4C6D48DB84015B93.tplgfr23s_3?idArticle=LEGIARTI000027964612&cidTexte=LEGITEXT000006055521&dateTexte=20180118) 13 February 2007;
- a pharmacist's certificate issued by an EU or EEA state not on the list of the above annex, accompanied by a certificate from that state;
- a title issued by an EU or EEA state sanctioning pharmacist training started before one of the dates mentioned in the decree of 13 February 2007, and accompanied by a certificate certifying that the national has legally devoted himself to Pharmacist activities for at least three years in the five years prior to the issuance of the certificate;
- a title issued by an EU or EEA state, sanctioning pharmacist training begun before one of the dates mentioned in the 13 February 2007 decree, and allowing the legal practice of pharmacist in that state. The national must justify having spent three years in the hospital function in France as associate attaché, associate practitioner, associate assistant or in the university function as associate clinic head of the universities or associate assistants of universities.

*For further information*: Article L. 4221-2 to L. 4221-5 of the Public Health Code; decree of 13 February 2007 setting out the list of diplomas, certificates and other pharmacist titles issued by the Member States of the European Union, the States Parties to the agreement on the European Economic Area and the Swiss Confederation covered by Article L. 4221-4 (1) of the Public Health Code.

#### Training

Pharmacy studies consist of three cycles with a total duration of between six and nine years depending on the chosen range.

**General education degree in pharmaceutical sciences**

The first cycle is sanctioned by the general training diploma in pharmaceutical sciences. It consists of six semesters and corresponds to the license level. The first two semesters correspond to THE PACES.

The aim of the training is to acquire:

- Basic knowledge in the field of exact sciences and biological sciences;
- Specific knowledge of the disciplines needed to study the drug and other health products;
- The skills needed to make the right use of this knowledge
- useful elements for the student's orientation to the various professions of pharmacy, including the areas of officiine and domestic pharmacies, medical biology, industry and research.

It includes theoretical, methodological, applied and practical teachings as well as the completion of a compulsory internship in officiine and two optional internships.

*For further information*: :[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023850773&dateTexte=20180118) March 22, 2011 on the education regime for the General Education Diploma in Pharmaceutical Sciences.

**In-depth training degree in pharmaceutical sciences**

The second cycle is sanctioned by the diploma of in-depth training in pharmaceutical sciences. Holders of a general training degree in pharmaceutical sciences can enrol in the training. It consists of four semesters of training and corresponds to the master level.

Its objective is to:

- the acquisition of scientific, medical and pharmaceutical knowledge, complementing and deepening those acquired during the first cycle;
- training in the scientific process
- Practical knowledge
- Learning to work as a team and acquiring communication techniques
- introduction to continuous professional development.

In addition to theoretical and practical teachings, the training includes completion:

- an internship in the office of one to two weeks;
- 12 months part-time or six months full-time hospital internship.

The second cycle is validated by the success of the knowledge of the teachings taught during the training, as well as the issuance of a certificate of pharmaceutical synthesis.

*For further information*: Articles 4 to 17 of the April 8, 2013 order on the education regime for the State Doctor of Pharmacy Diploma.

**State Doctor's Diploma in Pharmacy**

The third cycle is sanctioned by the issuance of the state diploma of doctor in pharmacy. It includes:

- A short cycle of two semesters of training;
- an eight-semester cycle of training for students who have been awarded the boarding school competition;
- the defence of a thesis.

The short postgraduate degree should allow the student to:

- to deepen the knowledge and skills related to the chosen career path and possibly to engage in a particular pharmaceutical specialisation, specific to this career orientation;
- to prepare his thesis for the completion of the state diploma of doctor of pharmacy.

It is accompanied by a six-month full-time professional practical internship, which can be completed in a dispensary, a mutual pharmacy, a pharmacy of a mining relief company, a pharmaceutical establishment or in a an industrial or commercial establishment whose activities are likely to contribute to the training of the pharmacist.

Within two years of validating the teachings taught during the short postgraduate degree, the student must support a thesis before a jury.

*For further information*: Article L. 633-2 of the Education Code; Article 18 and following of the order of April 8, 2013 relating to the education regime for the State Diploma of Doctor of Pharmacy.

#### Costs associated with qualification

The training leading to obtaining the DOCTOR's DEGREE in pharmacy is paid for. Its cost varies depending on the universities that provide the teachings. For more information, it is advisable to get closer to the university in question.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The professional who is a member of an EU or EEA state who is established and legally practises in one of these states may carry out the same activity in France on a temporary and occasional basis without being included in the Order of Pharmacists.

To do this, the professional must make a prior declaration, as well as a declaration justifying that he has the necessary language skills to practice in France (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

**What to know**

Registration on the College of Pharmacists' roster is not required for the free-service professional (LPS). It is therefore not required to pay ordinal dues. The pharmacist is simply registered on a specific list maintained by the National Council of the College of Pharmacists.

The pre-declaration must be accompanied by a statement regarding the language skills necessary to carry out the service. In this case, the control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

When training titles do not receive automatic recognition (see supra "2.0). a. National legislation"), the provider's professional qualifications are checked before the first service is provided. In the event of substantial differences between the qualifications of the person concerned and the training required in France that could harm public health, the claimant is subjected to an aptitude test.

Pharmacists in LPS situations are obliged to respect the professional rules applicable in France, including all ethical rules (see infra "3°. Conditions of honorability, ethical rules, ethics"). It is subject to the disciplinary jurisdiction of the College of Pharmacists.

**Please note**

The service is carried out under the French professional title of pharmacist. However, where training qualifications are not recognised and qualifications have not been verified, the performance is carried out under the professional title of the State of Establishment, in order to avoid confusion With the French professional title.

*For further information*: Article L. 4222-9 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

**The automatic diploma recognition scheme**

Article L. 4221-4 of the Public Health Code creates a regime for automatic recognition in France of certain diplomas or titles, if any, accompanied by certificates, obtained in an EU or EEA state (see above "2." a. National Legislation").

It is up to the National Council of the College of Pharmacists to verify the regularity of diplomas, titles, certificates and certificates, to grant automatic recognition and then to rule on the application for inclusion on the Order's list.

*For further information*: Article L. 4221-1 of the Public Health Code.

**The regime of individual authorisation to practise**

If the EU or EEA national does not qualify for the automatic recognition of his or her credentials, he or she falls under an authorisation scheme (see below "5o). c. If necessary, apply for an individual exercise permit").

Individuals who do not receive automatic recognition but who hold a training designation to legally practise as a pharmacist may be individually allowed to practice in the specialty concerned. Minister for Health, after advice from a commission made up of professionals.

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession in the specialty concerned and its exercise in France, the person concerned must submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- impose an adjustment course or aptitude test
- impose an adjustment course and an ordeal.

*For further information*: Article L. 4221-1-1 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Compliance with the Pharmacists' Code of Ethics

The provisions of the Code of Ethics are imposed on all pharmacists practising in France, whether they are registered on the Order's board or are exempt from this obligation (see supra "5." b. Ask for inclusion on the College of Pharmacists list").

**What to know**

All provisions of the Code of Ethics are codified in sections R. 4235-1 to R. 4235-77 of the Public Health Code.

As such, pharmacists must respect the principles of dignity, non-discrimination, professional secrecy or independence.

*For further information*: Articles L. 4235-1, and R. 4235-1 to R. 4235-77 of the Public Health Code.

### b. Cumulative activities

The pharmacist can only engage in any other activity if such a combination is compatible with the principles of professional independence and dignity imposed on him.

A pharmacist who serves an elective or administrative function is also prohibited from using it to increase his or her clientele.

*For further information*: Articles R. 4235-4 and R. 4235-23 of the Public Health Code.

### c. Obligation for continuous professional development

Pharmacists must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of training (present, mixed or non-presential) in analysis, evaluation, and practice improvement and risk management. All training is recorded in a personal document containing training certificates.

*For further information*: Article R. 4235-11 of the Public Health Code; Decree No. 2016-942 of 8 July 2016 relating to the organisation of the continuous professional development of health professionals.

4°. Insurance
---------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a pharmacist practising in a liberal capacity must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

### b. Pharmacists' Old Age Insurance Fund (CAVP)

Any pharmacist registered on the Board of the College of Pharmacists and practising in the liberal form (even part-time and even if he is also employed) has an obligation to join the CAVP.

The individual must report to the OPC within one month of the start of his Liberal activity.

*For further information*: Article R. 643-1 of the Social Security Code; the site of the[Cavp](https://www.cavp.fr/).

### c. Health Insurance Reporting Obligation

Once on the Order's roster, the pharmacist practising in liberal form must declare his activity with the Primary Health Insurance Fund (CPAM).

**Terms**

Registration with the CPAM can be made online on the official website of Medicare.

**Supporting documents**

The registrant must provide a complete file including:

- Copying a valid piece of identification
- a professional bank identity statement (RIB).

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally practises pharmacists in one of these states may practice in France on a temporary or occasional basis if he makes the prior declaration (see supra "2°. b. EU and EEA nationals: for a temporary and casual exercise (Freedom to provide services)").

The advance declaration must be renewed every year.

**Please note**

Any change in the applicant's situation must be notified under the same conditions.

**Competent authority**

The declaration must be addressed, before the first service delivery, to the National Council of the College of Pharmacists.

**Terms of reporting and receipt**

The declaration can be sent by mail or directly made online on the official website of the College of Pharmacists.

When the National Council of the Order receives the declaration and all the necessary supporting documents, it sends the claimant a receipt specifying its registration number as well as the discipline exercised.

**Please note**

The service provider informs the relevant national health insurance agency of its provision of services by sending a copy of the receipt or by any other means.

**Timeframe**

Within one month of receiving the declaration, the National Council of the Order informs the applicant:

- Whether or not he can start delivering services;
- when the verification of professional qualifications shows a substantial difference with the training required in France, he must prove to have acquired the missing knowledge and skills by submitting to an ordeal aptitude. If he meets this check, he is informed within one month that he can begin the provision of services;
- when the file review highlights a difficulty requiring further information, the reasons for the delay in reviewing the file. He then has one month to obtain the requested additional information. In this case, before the end of the second month from the receipt of this information, the National Council informs the claimant, after reviewing his file:- whether or not it can begin service delivery,
  - when the verification of the claimant's professional qualifications shows a substantial difference with the training required in France, he must demonstrate that he has acquired the missing knowledge and skills, including subjecting to an aptitude test.

In the latter case, if he meets this control, he is informed within one month that he can begin the provision of services. Otherwise, he is informed that he cannot begin the delivery of services. In the absence of a response from the National Council of the Order within these timeframes, service delivery may begin.

**Supporting documents**

The pre-declaration must be accompanied by a statement regarding the language skills required to carry out the service and the following supporting documents:

- The[Advance Service Delivery Reporting Form](http://www.ordre.pharmacien.fr/content/download/376629/1813657/version/1/file/Formulaire+-+Arr%C3%AAt%C3%A9+du+8+d%C3%A9cembre+2017+relatif+%C3%A0+la+d%C3%A9claration+pr%C3%A9alable+de+prestation+de+services+.pdf) ;
- Copying a valid piece of identification or a document attesting to the applicant's nationality;
- Copying the training document or titles, accompanied, if necessary, by a translation by a certified translator;
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and that he is not prohibited from practising, accompanied, if necessary, by a french translation established by a certified translator.

**Please note**

The control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

**Cost**

Free.

*For further information*: Articles L. 4222-9 at L. 4222-10, and R.4112-9 at R. 4112-12 of the Public Health Code.

### b. Formalities for EU or EEA nationals for a permanent exercise (LE)

#### If necessary, seek individual authorisation to exercise

If the national is not under the automatic recognition scheme, he must apply for a licence to practise.

**Competent authority**

The request is addressed in two copies, by letter recommended with request for notice of receipt to the unit responsible for the commissions of exercise authorization (CAE) of the National Management Centre (NMC).

He must add to his request:

- The[application form for authorization to practice the profession](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4349F01DDAE283C55604F0C28FE06D3D.tplgfr23s_3?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180118) ;
- A photocopy of a valid ID
- A copy of the training title allowing the practice of the profession in the state of obtaining as well as, if necessary, a copy of the specialist training title;
- If necessary, a copy of the additional diplomas;
- any useful evidence justifying continuous training, experience and skills acquired during the professional exercise in an EU or EEA state, or in a third state (certificates of functions, activity report, operational assessment, etc. ) ;
- in the context of functions performed in a state other than France, a declaration by the competent authority of that State, less than one year old, attesting to the absence of sanctions against the applicant.

Depending on the applicant's situation, additional supporting documentation is required. For more information, please refer to the official website of the[Cng](http://www.cng.sante.fr/).

**What to know**

Supporting documents must be written in French or translated by a certified translator.

**Timeframe**

The NMC acknowledges receipt of the request within one month of receipt.

The silence kept for a certain period of time from the receipt of the full file is worth the decision to dismiss the application. This delay is increased to:

- four months for applications from EU or EEA nationals with a degree from one of these states;
- six months for applications from third-party nationals with a diploma from an EU or EEA state;
- one year for other applications.

This period may be extended by two months, by decision of the ministerial authority notified no later than one month before the expiry of the latter, in the event of a serious difficulty in assessing the candidate's professional experience.

**Good to know: compensation measures**

Where there are substantial differences between the training and work experience of the national and those required to practise in France, the NMC may decide either:

- Suggest that the applicant choose between an adjustment course or an aptitude test;
- impose an adjustment course or aptitude test;
- to impose an adjustment course and an aptitude test.

**The aptitude test** is intended to verify, through written or oral tests or practical exercises, the applicant's ability to practise as a pharmacist in the specialty concerned. It deals with subjects that are not covered by the applicant's training or experience.

**The adaptation course** is intended to enable interested parties to acquire the skills necessary to practice the profession of pharmacist. It is carried out under the responsibility of a pharmacist and can be accompanied by optional additional theoretical training. The duration of the internship does not exceed three years. It can be done part-time.

*For further information*: decree of 25 February 2010 setting out the composition of the file to be provided to the competent authorisation commissions for the examination of applications submitted for the exercise in France of the professions of doctor, dental surgeon, midwife and Pharmacist.

#### Ask for inclusion on the Order's board

Registration on the Order's board is mandatory to legally practice the activity of pharmacist in France.

The registration does not apply:

- pharmacists public health inspectors, inspectors of regional health agencies, inspectors of the National Agency for the Safety of Medicines and Health Products;
- pharmacists who are civil servants or the like-for-like ministry of health;
- pharmacists who are civil servants or assimilated by the Ministry of Higher Education, who are not otherwise engaged in pharmaceutical activities;
- pharmacists belonging to the active framework of the army, sea and air health service.

**Please note**

Registration on the Order's board allows for the automatic and free issuance of the Health Professional Card (CPS). The CPS is an electronic business identity card. It is protected by a confidential code and contains, among other things, pharmacist identification data (identity, occupation, specialty). For more information, it is recommended to refer to the government website of the French Digital Health Agency.

**Competent authority**

Section R. 4222-1 of the Public Health Code provides that the application for registration is addressed, as the case may be, to the president of the regional council of the region in which the pharmacist wishes to practice, or to the president of the central council of the section.

**Supporting documents**

The applicant must submit, by any means, a complete application for registration including:

- two copies of the standardized questionnaire with a completed, dated and signed photo ID, available in the College's departmental councils or directly downloadable from the official website of the National Council of the College of Pharmacists;
- A photocopy of a valid ID or, if necessary, a certificate of nationality issued by a competent authority;
- If so, a photocopy of a valid EU citizen's family residence card, the valid long-term resident-EC card or the resident's card with valid refugee status;
- If so, a photocopy of the valid European business card;
- a copy, accompanied if necessary by a translation by a certified translator, of the training courses to which are attached:- where the applicant is an EU or EEA national, the certificate or certificates provided (see above "2. a. National requirements"),
  - applicant is granted an individual exercise permit (see supra "2. v. EU and EEA nationals: for a permanent exercise"), copying this authorisation,
  - When the applicant presents a diploma issued in a foreign state whose validity is recognized on French territory, the copy of the titles to which that recognition may be subordinated;
- for nationals of a foreign state, a criminal record extract or an equivalent document less than three months old, issued by a competent authority of the State of origin. This part can be replaced, for EU or EEA nationals who require proof of morality or honourability for access to the medical activity, by a certificate, less than three months old, from the competent authority of the State. certifying that these moral or honourability conditions are met;
- a statement on the applicant's honour certifying that no proceedings that could give rise to a conviction or sanction that could affect the listing on the board are against him;
- a certificate of delisting, registration or registration issued by the authority with which the applicant was previously registered or registered or, failing that, a declaration of honour from the applicant certifying that he or she was never registered or registered or, failing that, a certificate of registration or registration in an EU or EEA state;
- all the evidence that the applicant has the language skills necessary to practice the profession;
- a resume.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Timeframe**

The regional or central council decides on the application for registration within three months of receipt of the full application file. If a response is not answered within this time frame, the application for registration is deemed rejected.

This period is increased to six months for nationals of third countries when an investigation is to be carried out outside metropolitan France. The person concerned is then notified.

If registration is granted, the decision taken by the board is notified to the person concerned by recommended letter, accompanied by his registration certificate.

The decision of the regional council is also subject to appeal, within 30 days, to the National Council of the College of Pharmacists. The decision itself can be appealed to the Council of State.

**Cost**

Registration on the College's board is free, but it creates an obligation to pay the mandatory ordinal dues, the amount of which is set annually and which must be paid in the first quarter of the current calendar year.

*For further information*: Articles L. 4222-1 at L. 4222-8, and R. 4222-1 at R. 4222-4-3 of the Public Health Code.

### c. European Professional Card (CPE)

The European professional card is an electronic procedure for 500-member professional qualifications in another EU state.

The CPE procedure can be used when the national wishes to operate in another EU state on a temporary and casual basis, or on a permanent basis.

**Procedure**

A national who wishes to settle in France or provide services, contacts the competent authority of his state and sends him a file electronically. The competent authority will then send the request to the regional directorate of youth, sports and social cohesion of Ile-de-France, which will send it to the National Council of the Order of Pharmacists.

**Supporting documents**

The CPE application must be accompanied by all the following supporting documents:

- A readable photocopy of a valid piece of identification
- a statement from the National Council of the College of Pharmacists, attesting to the order's list and the absence of suspension or ban on practising;
- In the case of automatic recognition, a copy of the training titles and, if applicable, a certificate of compliance, a certificate of change of name or a certificate of acquired rights;
- in the event of recognition of professional qualifications, a copy of the training titles, any useful documents to provide information on the training taken, as well as any documents justifying the required qualifications;
- in the case of recognition of a title issued by a third state, a certificate certifying three years of experience in France;
- any document justifying work experience of at least one year in the last ten years, when the state does not regulate the training or the practice of the profession;
- a certificate of professional civil liability.

In addition, the applicant will be able to attach any proof of his knowledge of the French language.

**Outcome of the procedure**

After receiving the supporting documents, the Council of the Order may decide:

- To deliver the CPE;
- refuse to issue the CPE by reasoned and appeal-prone decision;
- submit the professional to a compensation measure in case of substantial difference with the training required to practice the profession in France

*For further information*: Articles R. 4222-9 to R. 4222-11 of the Public Health Code; 8 December 2017 on the implementation of the European professional card mentioned in Article L. 4002-2 of the Public Health Code.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

