﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP050" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Cardiovascular Medicine" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="cardiovascular-medicine" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/cardiovascular-medicine.html" -->
<!-- var(last-update)="2020-04-15 17:21:17" -->
<!-- var(url-name)="cardiovascular-medicine" -->
<!-- var(translation)="Auto" -->


Cardiovascular Medicine
=======================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The cardiologist is the physician who specializes in the prevention, diagnosis and treatment of cardiovascular diseases, i.e. heart and vessel diseases.

Cardiology is the medical discipline that studies the heart, arteries and veins, their functioning, their diseases and the means to treat them.

It covers the angiology of artery and vein diseases and the phlebology that focuses on vein diseases.

Blood is circulated through the heart, arteries and veins. These provide a flow and pressure (otherwise known as voltage) suitable for all situations.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Under Article L. 4111-1 of the Public Health Code, in order to legally practise as a doctor in France, those concerned must meet cumulatively the following three conditions:

- hold the French state diploma of doctor of medicine or a diploma, certificate or other title mentioned in Article L. 4131-1 of the Public Health Code (see below "Good to know: automatic recognition of diploma");
- be a French national, Andorran citizen or national of a Member State of the European Union (EU)* or party to the European Economic Area (EEA) agreement* Morocco, subject to the application of rules derived from the public health code or international commitments. However, this condition does not apply to a doctor with the French state diploma of doctor of medicine;
- with exceptions, be listed on the board of one of the departmental councils of the College of Physicians (see infra 5o. a. "Ask for inclusion on the order of doctors' list").

However, persons who do not meet the qualifications of diploma or nationality may be allowed to practise as a doctor by individual order of the Minister of Health (see below: "5. c. If necessary, apply for an individual exercise permit").

*For further information*: Articles L. 4111-1, L. 4112-6, L. 4112-7 and L. 4131-1 of the Public Health Code.

**Please note**

Failing to meet all of these conditions, the practice of the profession of doctor is illegal and punishable by two years' imprisonment and a fine of 30,000 euros.

*For further information*: Articles L. 4161-1 and L. 4161-5 of the Public Health Code.

**Good to know: automatic diploma recognition**

Under Article L. 4131-1 of the Public Health Code, EU or EEA nationals may practise as a doctor if they hold one of the following titles:

- doctor training documents issued by an EU or EEA state in accordance with EU obligations and listed in the annex[the decree of 13 July 2009 setting out the lists and conditions for the recognition of doctor and specialist medical training certificates issued by EU Member States or parties to the EEA agreement covered by Article L. 4131-1 of the Health Code Public](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020930148&fastPos=1&fastReqId=1774860260&categorieLien=cid&oldAction=rechTexte) ;
- medical training certificates issued by an EU or EEA state in accordance with EU obligations, not on the above list, if they are accompanied by a certificate from that state certifying that they are sanctioning training compliant with these obligations and are assimilated by him to the training titles on this list;
- medical training certificates issued by an EU or EEA state sanctioning medical training started in that state prior to the dates in the above-mentioned decree and not in accordance with EU obligations, if accompanied a certificate from one of these states certifying that the holder of the training titles has devoted himself, in that State, effectively and lawfully, to the practice of the profession of doctor in the specialty concerned for at least three consecutive years in the Five years prior to the issuance of the certificate;
- doctor training certificates issued by the former Czechoslovakia, the former Soviet Union or the former Yugoslavia or which sanction training begun before the independence date of the Czech Republic, Slovakia, Estonia, Latvia, Lithuania or Slovenia, if accompanied by a certificate from the competent authorities of one of these states certifying that they have the same legal validity as the training documents issued by that state. This certificate is accompanied by a certificate issued by the same authorities indicating that the holder has exercised in that State, effectively and lawfully, the profession of doctor in the specialty concerned for at least three consecutive years in the Five years prior to the issuance of the certificate;
- medical training certificates issued by an EU or EEA state not on the above list if they are accompanied by a certificate issued by the competent authorities of that state certifying that the holder of the training certificate was established on its territory on the date set out in the aforementioned decree and that it has acquired the right to carry out the activities of a general practitioner under its national social security scheme;
- medical training certificates issued by an EU or EEA state sanctioning medical training started in that state prior to the dates in the above-mentioned decree and not in line with EU obligations but allowing to legally practise the profession of doctor in the state that issued them, if the doctor justifies having performed in France in the previous five years three consecutive full-time years of hospital functions in the corresponding specialty training as associate attaché, associate practitioner, associate assistant or academic functions as associate clinic head of universities or associate assistant of universities, provided that he has been appointed hospital functions at the same time;
- Italy's specialist medical training certificates on the aforementioned list sanctioning specialist medical training begun in that state after 31 December 1983 and before 1 January 1991, if accompanied by a certificate issued by the state authorities indicating that its holder has exercised in that state, effectively and lawfully, the profession of physician in the specialty concerned for at least seven consecutive years in the ten years preceding issuing the certificate.

*For further information*: Article L. 4131-1 of the Public Health Code; 13 July 2009 decree setting out the lists and conditions for the recognition of the training documents of doctors and specialist doctors issued by the Member States of the European Union or parties to the agreement on the European Economic Area referred to in the 2nd Section L. 4131-1 of the Public Health Code.

#### Training

Medical studies consist of three cycles with a total duration of between nine and eleven years, depending on the course chosen.

The training, which takes place at the university, includes many internships and is punctuated by two competitions:

- The first occurs at the end of the first year. This year of study, called the "first year of common health studies" (PACES) is common to students in medicine, pharmacy, odontology, physiotherapy and midwives. At the end of this first competition, students are ranked according to their results. Those in a useful rank under the numerus clausus are allowed to continue their studies and to choose, if necessary, to continue training leading to the practice of medicine;
- the second year occurs at the end of the 2nd cycle (i.e. at the end of the 6th year of study): this competition is called national classification tests (ECN) or formerly "boarding school". At the end of this competition, students choose, based on their ranking, their specialty and/or their city of assignment. The duration of the studies that follow varies depending on the specialty chosen.

To obtain a state degree (DE) as a doctor of medicine, the student must validate all his internships, his diploma of specialized studies (DES) and support his thesis successfully.

*For further information*: Article L. 632-1 of the Education Code.

**Good to know**

Medical students must carry out compulsory vaccinations. For more information, please refer to Section R. 3112-1 of the Public Health Code.

##### General education diploma in medical sciences

The first cycle is sanctioned by the general training diploma in medical sciences. It consists of six semesters and corresponds to the license level. The first two semesters correspond to THE CAPS.

The aim of the training is to:

- the acquisition of the basic scientific knowledge, essential for the subsequent mastery of the knowledge and know-how necessary for the practice of medical professions. This scientific base is broad and encompasses biology, certain aspects of the exact sciences and several disciplines of the humanities and social sciences;
- the fundamental approach of the healthy man and the sick man, including all aspects of semiology.

It includes theoretical, methodological, applied and practical teachings as well as the completion of internships including a four-week introductory care course in a hospital.

*For further information*: order of March 22, 2011 relating to the education scheme for the general training diploma in medical sciences.

##### In-depth training degree in medical sciences

The second cycle of medical studies is sanctioned by the diploma of in-depth training in medical sciences. It consists of six semesters of training and corresponds to the master's level.

Its objective is to acquire the generic skills that enable students to subsequently perform the functions of the postgraduate or outpatient settings and to acquire the professional skills of the training in which they will engage during their specialization.

The skills to be acquired are those of communicator, clinician, co-operator, member of a multi-professional health care team, public health actor, scientist and ethical and ethical leader. The student must also learn to be reflexive.

The lessons are focused on what is common or serious or a public health problem and what is clinically exemplary.

The objectives of the training are:

- the acquisition of knowledge about pathophysiological processes, pathology, therapeutic bases and prevention complementing and deepening those acquired during the previous cycle;
- training in the scientific process
- Learning clinical reasoning
- the acquisition of generic skills preparing for the third cycle of medical studies.

In addition to theoretical and practical teachings, the training includes the completion of thirty-six months of internships and twenty-five guards.

*For further information*: decree of 8 April 2013 relating to the curriculum for the 1st and 2nd cycle of medical studies.

##### Medicine DES

Access to the third cycle is through the ECNs. To practice as a cardiologist, the professional must obtain the four-year DES of cardiology and vascular diseases.

**Please note**

Students can only attend NCT twice.

The training includes:

- teachings lasting about 250 hours, general and specific to the specialty;
- hands-on training:- four semesters in approved services for the DES of cardiology and vascular diseases, of which at least three must be performed in hospital-university or contracted services. These semesters must be carried out in at least two different departments or departments,
  - one semester in an approved department for the complementary vascular medicine or vascular surgery DES or in a licensed functional exploration laboratory for the DES of cardiology and vascular diseases,
  - three semesters in services approved for services other than the DES of cardiology and vascular diseases, preferably in approved services for the DES of endocrinology, diabetes and metabolic diseases, internal medicine, nephrology, neurology, paediatrics (cardiological orientation), pneumology or radiodiagnosis and medical imaging or for the supplemental medical resuscitation DES.

*For further information*: Article 1 and Appendix B of the September 22, 2004 decree setting out the list of medical DES.

#### Costs associated with qualification

The training leading to obtaining the DOCTOR of medicine IS paid for. Its cost varies depending on the universities that provide the teachings. For more details, it is advisable to get closer to the university in question.

### b. EU and EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A doctor who is a member of an EU or EEA state who is established and legally practises in one of these states may perform acts of his profession in France on a temporary and occasional basis, provided that he has previously prior declaration to the National Council of the College of Physicians (see infra "5 b. Make a prior declaration for EU or EEA nationals engaged in temporary and occasional activity").

**What to know**

Registration on the College of Physicians' roster is not required for physicians in a free-supply of services (LPS) situation. As a result, they are not required to pay ordinal dues. The doctor is simply registered on a specific list maintained by the National Council of the College of Physicians.

The pre-declaration must be accompanied by a statement regarding the language skills necessary to carry out the service. In this case, the control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

Where training qualifications do not receive automatic recognition (see supra "2 a. National Legislation"), the provider's professional qualifications are checked before the first service is provided. In the event of substantial differences between the qualifications of the person concerned and the training required in France that could harm public health, the claimant is subjected to a test.

The doctor in LPS situation is obliged to respect the professional rules applicable in France, including all ethical rules (see infra "3." Conditions of honorability and ethical rules"). It is subject to the disciplinary jurisdiction of the College of Physicians.

**Please note**

The service is performed under the French professional title of doctor. However, where training qualifications are not recognised and qualifications have not been verified, the performance is carried out under the professional title of the State of Establishment, in order to avoid confusion With the French professional title.

*For further information*: Article L. 4112-7 of the Public Health Code.

### c. EU and EEA nationals: for a permanent exercise (Freedom of establishment)

#### The automatic recognition of diplomas obtained in an EU state

Article L. 4131-1 of the Public Health Code creates a regime for automatic recognition in France of certain diplomas or titles, if any, accompanied by certificates, obtained in an EU or EEA state (see above "2." a. National Legislation").

It is up to the departmental council of the college of doctors responsible to verify the regularity of diplomas, titles, certificates and certificates, to grant automatic recognition and then to decide on the application for registration on the Order's list. .

*For further information*: Article L. 4131-1 of the Public Health Code; 13 July 2009 decree setting out the lists and conditions for the recognition of the training documents of doctors and specialist doctors issued by the Member States of the European Union or parties to the agreement on the European Economic Area referred to in the 2nd Section L. 4131-1 of the Public Health Code.

#### The derogatory regime: prior authorisation

If the EU or EEA national does not qualify for the automatic recognition of his or her credentials, he or she falls under an authorisation scheme (see below "5o). c. If necessary, apply for individual exercise authorization).

Individuals who do not receive automatic recognition but who hold a training designation to legally practise as a doctor may be individually allowed to practice in the specialty concerned by Minister of Health, after advice from a commission made up of professionals.

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession in the specialty concerned and its exercise in France, the person concerned must submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- impose an adjustment course or aptitude test
- impose an adjustment course and an ordeal**.**

*For further information*: Article L. 4131-1-1 of the Public Health Code.

3°. Conditions of honorability and ethical rules
-----------------------------------------------------------

### a. Compliance with the physicians' code of ethics

The provisions of the code of medical ethics are imposed on all doctors practising in France, whether they are registered on the Order's board or are exempt from this obligation (see above: "5. a. Request inclusion on the College of Physicians list").

**What to know**

All provisions of the Code of Ethics are codified in sections R. 4127-1 to R. 4127-112 of the Public Health Code.

As such, the physician must respect the principles of morality, probity and dedication essential to the practice of medicine. He is also subject to medical secrecy and must exercise independently.

*For further information*: Articles R. 4127-1 to R. 4127-112 of the Public Health Code.

### b. Cumulative activities

The physician may only engage in any other activity if such a combination is compatible with the principles of professional independence and dignity imposed on him. The accumulation of activities should not allow him to take advantage of his prescriptions or his medical advice.

Thus, the doctor cannot combine the medical exercise with another activity close to the health field. In particular, he is prohibited from practising as an optician, paramedic or manager of an ambulance company, manufacturer or seller of medical devices, owner or manager of a hotel for curators, a gym, a spa, a massage practice.

Similarly, a physician who fulfils an elective or administrative mandate is prohibited from using it to increase his or her clientele.

*For further information*: Articles R. 4127-26 and R. 4127-27 of the Public Health Code.

### c. Conditions of honorability

In order to practice, the physician must certify that no proceedings that could give rise to a conviction or a sanction that could affect his inclusion on the board are against him.

*For further information*: Article R. 4112-1 of the Public Health Code.

### d. Obligation for continuous professional development

Physicians must participate in a multi-year continuous professional development program. The program focuses on assessing professional practices, improving skills, improving the quality and safety of care, maintaining and updating knowledge and skills.

All the actions carried out by doctors under their obligation to develop professionally are traced in a specific document attesting to compliance with this obligation.

*For further information*: Articles L. 4021-1 and the following and R. 4021-4 and the following of the Public Health Code.

### e. Physical aptitude

Physicians must not present with infirmity or pathology incompatible with the practice of the profession (see infra "5.0). a. Request inclusion on the College of Physicians list").

*For further information*: Article R. 4112-2 of the Public Health Code.

4°. Social legislation and insurance
--------------------------------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a physician practising in a liberal capacity must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the independent pension fund of doctors of France (CARMF)

Any doctor registered on the Order's board and practising in the liberal form (even part-time and even if he is also employed) has an obligation to join the CARMF.

#### Time

The person concerned must register with the CARMF within one month of the start of his Liberal activity.

#### Terms

The person concerned must return the declaration form, completed, dated and countersigned by the departmental council of the College of Physicians. This form is downloadable on the[CARMF website](http://www.carmf.fr/doc/formulaires/cotisants/declaration-en-vue.pdf).

**What to know**

In the event of a practice in a liberal practice company (SEL), membership in the CARMF is also mandatory for all professional partners practising there.

### c. Health Insurance Reporting Obligation

Once on the Order's roster, the doctor practising in liberal form must declare his activity with the Primary Health Insurance Fund (CPAM).

#### Terms

Registration with the CPAM can be carried out online on the[Health Insurance's official website](https://installation-medecin.ameli.fr/installation_medecin/).

#### Supporting documents

The registrant must provide a complete file including:

- Copying a valid piece of identification
- a professional bank identity statement (RIB)
- if necessary, the title (s) to allow access to Sector 2.

For more information, it is advisable to refer to the[section on the liberal installation of doctors on the Medicare website](http://www.ameli.fr/professionnels-de-sante/medecins/gerer-votre-activite/votre-installation-en-liberal/vous-vous-installez-en-liberal.php).

5°. Qualifications process and formalities
----------------------------------------------------

### a. Request registration on the College of Physicians' Table

Registration on the Order's board is mandatory to legally carry out the activity of a doctor in France.

The inscription on the Order's board does not apply:

- EU or EEA nationals who are established and who are legally practising as a doctor in a Member State or party, when they perform acts of their profession on a temporary and occasional basis in France (see supra "2°. b. EU and EEA nationals: for temporary and occasional exercise);
- doctors belonging to the active executives of the Armed Forces Health Service;
- physicians who, having the status of a public servant or a holding agent of a local authority, are not called upon, in the course of their duties, to practice medicine.

*For further information*: Articles L. 4112-5 to L. 4112-7 of the Public Health Code.

**Please note**

Registration on the Order's board allows for the automatic and free issuance of the Health Professional Card (CPS). The CPS is an electronic business identity card. It is protected by a confidential code and contains, among other things, the doctor's identification data (identity, occupation, specialty). For more information, it is recommended to refer to the[government website of the French Digital Health Agency](http://esante.gouv.fr/services/espace-cps/cartes-professionnelles-de-sante).

#### Competent authority

The application for registration is addressed to the Chairman of the Board of the College of Physicians of the Department in which the person concerned wishes to establish his professional residence.

The application can be submitted directly to the departmental council of the Order concerned or sent to it by registered mail with request for notice of receipt.

*For further information*: Article R. 4112-1 of the Public Health Code.

**What to know**

In the event of a transfer of his professional residence outside the department, the practitioner is required to request his removal from the order of the department where he was practising and his registration on the order of his new professional residence.

*For further information*: Article R. 4112-3 of the Public Health Code.

#### Procedure

Upon receipt of the application, the county council appoints a rapporteur who conducts the application and makes a written report.

The board verifies the candidate's titles and requests disclosure of bulletin 2 of the applicant's criminal record. In particular, it verifies that the candidate:

- fulfils the necessary conditions of morality and independence (see supra "3.3. c. Conditions of honorability");
- meets the necessary competency requirements;
- does not present a disability or pathological condition incompatible with the practice of the profession (see supra "3." e. Physical aptitude").

In case of serious doubt about the applicant's professional competence or the existence of a disability or pathological condition incompatible with the practice of the profession, the county council refers the matter to the regional or inter-regional council expertise. If, in the opinion of the expert report, there is a professional inadequacy that makes the practice of the profession dangerous, the departmental council refuses registration and specifies the training obligations of the practitioner.

No decision to refuse registration can be made without the person being invited at least a fortnight in advance by a recommended letter requesting notice of receipt to appear before the Board to explain.

The decision of the College Council is notified, within a week, to the person concerned to the National Council of the College of Physicians and to the Director General of the Regional Health Agency (ARS). The notification is by recommended letter with request for notice of receipt.

The notification mentions that the remedies against the decision. The decision to refuse must be justified.

*For further information*: Articles R. 4112-2 and R. 4112-4 of the Public Health Code.

#### Time

The Chair acknowledges receipt of the full file within one month of its registration.

The College's departmental council must decide on the application for registration within three months of receipt of the full application file. If a response is not answered within this time frame, the application for registration is deemed rejected.

This period is increased to six months for nationals of third countries when an investigation is to be carried out outside metropolitan France. The person concerned is then notified.

It may also be extended for a period of no more than two months by the departmental council when an expert opinion has been ordered.

*For further information*: Articles L. 4112-3 and R. 4112-1 of the Public Health Code.

#### Supporting documents

The applicant must submit a complete application file including:

- two copies of the standardized questionnaire with a completed, dated and signed photo ID, available in the College's departmental councils or directly downloadable on the[official website of the National Council of the College of Physicians](https://www.conseil-national.medecin.fr/sites/default/files/questionnaireinscriptionordremedecins.pdf) ;
- A photocopy of a valid ID or, if necessary, a certificate of nationality issued by a competent authority;
- If applicable, a photocopy of a valid EU citizen's family residence card, the valid long-term resident-EC card or the resident card with valid refugee status;
- If so, a photocopy of the valid European credit card;
- a copy, accompanied if necessary by a translation, made by a certified translator, of the training titles to which are attached:- where the applicant is an EU or EEA national, the certificate or certificates provided (see above "2. a. National requirements"),
  - applicant is granted an individual exercise permit (see supra "2. v. EU and EEA nationals: for a permanent exercise"), copying this authorisation,
  - when the applicant presents a diploma issued in a foreign state whose validity is recognized on French territory, the copy of the titles to which such recognition may be subordinated,
- for nationals of a foreign state, a criminal record extract or an equivalent document less than three months old, issued by a competent authority of the State of origin. This part can be replaced, for EU or EEA nationals who require proof of morality or honourability for access to the medical activity, by a certificate, less than three months old, from the competent authority of the State. certifying that these moral or honourability conditions are met;
- a statement on the applicant's honour certifying that no proceedings that could give rise to a conviction or sanction that could affect the listing on the board are against him;
- a certificate of registration or registration issued by the authority with which the applicant was previously registered or registered or, failing that, a declaration of honour from the applicant certifying that he or she was never registered or registered or, failing that, a certificate of registration or registration in an EU or EEA state;
- all the evidence that the applicant has the language skills necessary to practice the profession;
- A resume
- contracts and endorsements for the practice of the profession as well as those relating to the use of the equipment and the premises in which the applicant practises;
- If the activity is carried out in the form of SEL or a professional civil society (SCP), the statutes of that company and their possible endorsements;
- If the applicant is a public servant or public official, the order of appointment;
- if the applicant is a professor of universities - hospital practitioner (PU-PH), university lecturer - hospital practitioner (MCU-PH) or hospital practitioner (PH), the order of appointment as a hospital practitioner and, if necessary, the decree or order of appointment as a university professor or lecturer at universities.

For more information, it is advisable to refer to the[official website of the National Council of the College of Physicians](https://www.conseil-national.medecin.fr/l-inscription-au-tableau-1233).

*For further information*: Articles L. 4113-9 and R. 4112-1 of the Public Health Code.

#### Remedies

The applicant or the National Council of the College of Physicians may challenge the decision to register or refuse registration within 30 days of notification of the decision or the implied decision to reject it. The appeal is brought before the territorially competent regional council.

The regional council must decide within two months of receiving the application. In the absence of a decision within this period, the appeal is deemed dismissed.

The decision of the regional council is also subject to appeal, within 30 days, to the National Council of the College of Physicians. The decision itself can be appealed to the Council of State.

*For further information*: Articles L. 4112-4 and R. 4112-5 of the Public Health Code.

#### Cost

Registration on the College's board is free, but it creates an obligation to pay the mandatory ordinal dues, the amount of which is set annually and which must be paid in the first quarter of the current calendar year. Payment can be made online on the[official website of the National Council of the College of Physicians](https://paiements.ordre.medecin.fr/). As an indication, in 2017, the amount of this contribution amounts to 333 euros.

*For further information*: Article L. 4122-2 of the Public Health Code.

### b. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally practises as a doctor in one of these states may exercise in France on a temporary or occasional basis if he makes the prior declaration (see supra 2 b. "EU and EEA nationals: in temporary and occasional exercise").

The advance declaration must be renewed every year.

**Please note**

Any change in the applicant's situation must be notified under the same conditions.

*For further information*: Articles L. 4112-7 and R. 4112-9-2 of the Public Health Code.

#### Competent authority

The declaration must be addressed, before the first service delivery, to the National Council of the College of Physicians.

*For further information*: Article R. 4112-9 of the Public Health Code.

#### Terms of reporting and receipt

The return can be sent by mail or directly made online on the[official website of the College of Physicians](https://sve.ordre.medecin.fr/loc_fr/default/?__CSRFTOKEN__=3924ffb9-b716-467f-bd6c-27f3fffb03fe).

When the National Council of the College of Physicians receives the declaration and all the necessary supporting documents, it sends the claimant a receipt specifying its registration number and discipline.

**Please note**

The service provider informs the relevant national health insurance agency of its provision of services by sending a copy of the receipt or by any other means.

*For further information*: Articles R. 4112-9-2 and R. 4112-11 of the Public Health Code.

#### Time

Within one month of receiving the declaration, the National Council of the Order informs the applicant:

- Whether or not he can start delivering services;
- when the verification of professional qualifications shows a substantial difference with the training required in France, he must prove to have acquired the missing knowledge and skills by submitting to an ordeal aptitude. If he meets this check, he is informed within one month that he can begin the provision of services;
- when the file review highlights a difficulty requiring further information, the reasons for the delay in reviewing the file. He then has one month to obtain the requested additional information. In this case, before the end of the 2nd month from the receipt of this information, the National Council informs the claimant, after reviewing his file:- whether or not it can begin service delivery,
  - when the verification of the claimant's professional qualifications shows a substantial difference with the training required in France, he must demonstrate that he has acquired the missing knowledge and skills, including subjecting to an aptitude test.

In the latter case, if he meets this control, he is informed within one month that he can begin the provision of services. Otherwise, he is informed that he cannot begin the delivery of services.

In the absence of a response from the National Council of the Order within these timeframes, service delivery may begin.

*For further information*: Article R. 4112-9-1 of the Public Health Code.

#### Supporting documents

The pre-declaration must be accompanied by a statement regarding the language skills required to carry out the service and the following supporting documents:

- The advance service delivery form, the model of which is listed in an appendix to the[January 20, 2010 order on the prior declaration of the provision of services for the practice of physician, dental surgeon and midwifery](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021776754&dateTexte=20170209), completed, dated and signed. The requested information relates to:- The identity of the applicant
  - The profession concerned,
  - professional insurance,
  - and, for renewal, on periods of service delivery and on professional activities,
- Copying a valid piece of identification or a document attesting to the applicant's nationality;
- Copying the training document or titles, accompanied, if necessary, by a translation by a certified translator;
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and that he is not prohibited from practising, accompanied, if necessary, by a french translation established by a certified translator.

**Please note**

The control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

*For further information*: Articles L. 4112-7 of the Public Health Code; January 20, 2010 order on the prior declaration of the provision of services for the practice of physician, dental surgeon and midwife.

#### Cost

Free.

### c. If necessary, seek individual authorisation to exercise

#### For EU or EEA nationals

EU or EEA nationals with a training licence may apply for individual authorisation:

- issued by one of these states not receiving automatic recognition (see supra 2.c. EU and EEA nationals: for permanent exercise);
- issued by a third state but recognised by an EU or EEA Member State, provided they justify practising as a doctor in the specialty for a period equivalent to three years full-time in that Member State.

An Authorization Board (CAE) reviews the applicant's training and work experience.

It may propose a compensation measure:

- where the training is at least one year less than that of the French ED, when it covers substantially different subjects, or when one or more components of the professional activity whose exercise is subject to the aforementioned diploma do not exist in the corresponding profession in the Member State of origin or have not been taught in that state;
- training and experience of the applicant are not likely to cover these differences.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- impose an adjustment course or aptitude test
- impose an adjustment course and an aptitude test.

**The aptitude test** is intended to verify, through written or oral tests or practical exercises, the applicant's fitness to practise as a physician in the relevant specialty. It deals with subjects that are not covered by the applicant's training or experience.

**The adaptation course** is intended to enable interested parties to acquire the skills necessary to practice the profession of physician. It is carried out under the responsibility of a doctor and can be accompanied by optional additional theoretical training. The duration of the internship does not exceed three years. It can be done part-time.

*For further information*: Articles L. 4111-2 II, L. 4131-1-1, R. 4111-17 to R. 4111-20 and R. 4131-29 of the Public Health Code.

#### For nationals of a third state

Individual authorization to practice may apply, provided they warrant a sufficient level of proficiency in the French language, people with a training degree:

- issued by an EU or EEA state whose experience is attested by any means;
- issued by a third state allowing the practice of the profession of doctor in the country of graduation:- whether they meet anonymous tests to verify basic and practical knowledge. For more information on these events, it is advisable to refer to the[National Management Centre (NMC) official website](http://www.cng.sante.fr/Epreuves-de-verification-des.html),
  - and whether they warrant three years of duties in an accredited department or organization for the training of interns.

**Please note**

Physicians with a specialized degree obtained as part of the foreign internship are deemed to have met the knowledge-verification tests.

*For further information*: Articles L. 4111-2 (I and I bis), D. 4111-1, D. 4111-6 and R. 4111-16-2 of the Public Health Code.

#### Competent authority

The request is addressed in two copies, by letter recommended with request for notice of receipt to the unit responsible for the commissions of exercise authorization (CAE) of the NMC.

The authorization to exercise is issued by the Minister of Health after notification from the EAC.

*For further information*: Articles R. 4111-14 and R. 4131-29 of the Public Health Code; decree of 25 February 2010 setting out the composition of the file to be provided to the competent CAEs for the examination of applications submitted for the exercise in France of the professions of doctor, dental surgeon, midwife and pharmacist.

#### Time

The NMC acknowledges receipt of the request within one month of receipt.

The silence kept for a certain period of time from the receipt of the full file is worth the decision to dismiss the application. This deadline is:

- four months for applications from EU or EEA nationals with a degree from one of these states;
- six months for applications from third-party nationals with a diploma from an EU or EEA state;
- one year for other applications. This period may be extended by two months, by decision of the ministerial authority notified no later than one month before the expiry of the latter, in the event of a serious difficulty in assessing the candidate's professional experience.

*For further information*: Articles R. 4111-2, R. 4111-14 and R. 4131-29 of the Public Health Code.

#### Supporting documents

The application file must contain:

- an application form for authorisation to practice the profession, modelled on Schedule 1 of the Order of February 25, 2010, completed, dated and signed, showing, if any, the specialty in which the applicant files;
- A photocopy of a valid ID
- A copy of the training title allowing the practice of the profession in the state of obtaining as well as, if necessary, a copy of the specialist training title;
- If necessary, a copy of the additional diplomas;
- any useful evidence justifying continuous training, experience and skills acquired during the professional exercise in an EU or EEA state, or in a third state (certificates of functions, activity report, operational assessment, etc. ) ;
- in the context of functions performed in a state other than France, a declaration by the competent authority of that State, less than one year old, attesting to the absence of sanctions against the applicant.

Depending on the applicant's situation, additional supporting documentation is required. For more information, it is advisable to refer to the[NMC's official website](http://www.cng.sante.fr/Liste-des-pieces-a-fournir.html).

**What to know**

Supporting documents must be written in French or translated by a certified translator.

*For further information*: decree of 25 February 2010 setting out the composition of the file to be provided to the competent authorisation commissions for the examination of applications submitted for the practice in France of the professions of doctor, dental surgeon, midwife and pharmacist; 17 November 2014 no. DGOS/RH1/RH2/RH4/2014/318.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris,[official website of the General Secretariat for European Affairs](https://sgae.gouv.fr/sites/SGAE/accueil.html).

