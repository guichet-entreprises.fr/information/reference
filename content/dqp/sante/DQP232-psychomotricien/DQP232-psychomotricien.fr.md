﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP232" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Psychomotricien" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="psychomotricien" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/psychomotricien.html" -->
<!-- var(last-update)="2020-04-15 17:22:04" -->
<!-- var(url-name)="psychomotricien" -->
<!-- var(translation)="None" -->

# Psychomotricien

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:04<!-- end-var -->

## 1°. Définition de l’activité

Le psychomotricien est un professionnel de santé paramédical dont l'activité consiste à effectuer la rééducation motrice de ses patients. À ce titre, il peut être amené à réaliser sur ses patients :

- un bilan psychomoteur ;
- une rééducation de l'ensemble des troubles psychomoteurs ;
- des traitements pour pallier :
  - aux déficiences intellectuelles,
  - aux troubles caractériels, de la personnalité ou émotionnels de ses patients.

Ce professionnel ne peut exercer que sur prescription médicale.

*Pour aller plus loin* : articles L. 4332-1 et L. 4334-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de psychomotricien le professionnel doit :

- être qualifié professionnellement ;
- avoir procédé à l'enregistrement de son diplôme.

#### Formation

Pour être reconnu comme étant qualifié professionnellement, il doit soit :

- être titulaire du diplôme d’État de psychomotricien (bac+3) ;
- avoir exercé l'activité de psychomotricien pendant au moins trois ans au cours des dix années précédant la date du 8 mai 1988 et avoir satisfait à un contrôle des connaissances dans les trois ans suivants cette date.

##### Diplôme d’État (DE) de psychomotricien

Le DE de psychomotricien est accessible sur concours aux candidats ayant suivi une formation en vue d'acquérir les connaissances nécessaires à l'exercice de la profession, en matière médicale, en sciences humaines et spécifiques et à la maîtrise des techniques psychomotrices.

Cette formation d'une durée de trois ans est dispensée au sein d'un établissement d'enseignement supérieur et est constituée :

- d'une première année composée d'une formation de 712 heures réparties en six modules d'enseignements théoriques et d'un module pratique de psychomotricité ;
- d'une deuxième année composée d'une formation de 645 heures réparties en cinq modules théoriques, un module théorico-clinique et un module pratique ;
- d'une troisième année composée d'une formation de 485 heures réparties en un module théorique, un module théorico-clinique et un module pratique de psychomotricité.

Le candidat doit également effectuer des stages d'un volume total de 600 heures et un mémoire de fin d'études.

**À noter**

Le programme des enseignements figure à l'annexe I de l'arrêté du 7 avril 1998 relatif aux études préparatoires au diplôme d’État de psychomotricien.

Le DE de psychomotricien est délivré par le préfet de région aux candidats ayant réussi avec succès l'examen prévu en fin de formation. Cet examen comprend une épreuve de mise en situation professionnelle d'une durée variant entre 45 minutes et une heure et la soutenance de son mémoire pendant 45 minutes. Une fois le diplôme obtenu, le professionnel est tenu de se faire enregistrer auprès de l'agence régionale de santé (ARS) (cf. infra « 5°. a. Obligation de procéder à l'enregistrement du diplôme »).

**Bon à savoir**

Les candidats titulaires de l'un des diplômes figurant à l'article 25 et ayant obtenu une moyenne générale de dix sans aucune note inférieure à huit à un examen écrit portant sur un module théorique peuvent être dispensés d'effectuer la première année de formation. Le cas échéant, les candidats devront fournir lors de leur inscription la photocopie de leur diplôme admis en dispense de la première année de formation.

*Pour aller plus loin* : articles D. 4332-2 à R. 4332-8 du Code de la santé publique ; arrêté du 7 avril 1998 précité.

#### Coûts associés à la qualification

La formation en vue d'acquérir le DE de psychomotricien est payante et son coût varie selon le cursus envisagé. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant l'activité de psychomotricien, peut exercer à titre temporaire et occasionnel, la même activité en France et ce, sans avoir à procéder à son enregistrement.

Lorsque ni l'accès, ni l'exercice de la profession ne sont réglementés dans l’État membre, le professionnel doit avoir exercé l'activité de psychomotricien pendant au moins un an au cours des dix dernières années dans un ou plusieurs État(s) membre(s).

Dès lors qu'il remplit ces conditions, le ressortissant devra effectuer une déclaration avant sa première prestation de services (cf. infra « 5°. b. Déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

**À noter**

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel et celle requise pour exercer l'activité de psychomotricien en France, le préfet peut décider de le soumettre à une épreuve d'aptitude.

De plus, le ressortissant doit justifier avoir les connaissances linguistiques nécessaires à l'exercice de la profession de psychomotricien en France.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de psychomotricien, peut exercer à titre permanent la même activité en France.

Pour cela l'intéressé doit être titulaire soit :

- d'un titre de formation délivré par un État membre lui permettant d'exercer l'activité de psychomotricien dès lors que cet État réglemente l'activité ;
- lorsque l’État ne réglemente ni l'accès, ni l'exercice de la profession, d'un titre de formation attestant qu'il a suivi une préparation à l'exercice de la profession. Il doit également justifier d'une expérience professionnelle d'un an au cours des dix dernières années ;
- d'un titre de formation délivré par un État tiers et reconnu dans un État membre de l'UE lui permettant d'exercer cette profession et d'une expérience professionnelle de trois ans dans cet État membre.

Dès lors qu'il remplit ces conditions, le ressortissant doit effectuer une demande d'autorisation d'exercice auprès du préfet de région (cf. infra « 5°. c. Demande d'autorisation d'exercice pour le ressortissant UE en vue d'un exercice permanent (LE) »).

**À noter**

Lorsque l'examen de ses qualifications professionnelles fait apparaître des différences substantielles entre la formation reçue par le ressortissant et la formation requise pour exercer l'activité en France, le préfet peut décider de le soumettre à une mesure de compensation (cf. infra « 5°. c. Bon à savoir : mesures de compensation »).

En outre, tout comme dans le cadre de la libre prestation de services, le ressortissant doit justifier des connaissances linguistiques nécessaires à l'exercice de sa profession en France.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le psychomotricien, en qualité de professionnel de santé, est tenu au respect de la vie privée du patient et au secret professionnel. En outre, il doit informer ses patients de leur état de santé et notamment sur :

- les différents traitements proposés ;
- les risques encourus ;
- les alternatives possibles en cas de refus.

*Pour aller plus loin* : articles L. 1110-4 et L. 1111-2 du Code de la santé publique.

## 4°. Assurances et sanctions

### Assurance

Le psychomotricien en tant que professionnel de santé est tenu de souscrire une assurance de responsabilité civile professionnelle pour les risques encourus au cours l'exercice de son activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

### Sanctions pénales

Le fait d'exercer l'activité de psychomotricien sans être qualifié professionnellement est considéré comme une usurpation de titres et est puni d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : article L. 4334-2 du Code de la santé publique ; article 433-17 du Code pénal.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Obligation de procéder à l'enregistrement du diplôme

#### Autorité compétente

Le demandeur doit adresser une demande à l'ARS en vue de procéder à son enregistrement.

#### Pièces justificatives

Le demandeur doit transmettre les informations suivantes, réputées validées et certifiées par l'organisme ayant délivré le diplôme ou le titre de formation :

- l'état civil du titulaire du diplôme et toutes les données permettant, d'identifier le demandeur ;
- les nom et adresse de l'établissement ayant délivré la formation ;
- l'intitulé de la formation.

#### Issue de la procédure

Après vérifications des pièces, l'ARS procède à l'enregistrement du diplôme.

**À noter**

L'inscription n'est possible que pour un seul département. Toutefois si le professionnel souhaite exercer dans plusieurs départements, le demandeur sera inscrit sur la liste du département dans lequel se trouve le lieu principal de son activité.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 4333-1 et D. 4333-1 du Code de la santé publique.

### b. Déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser avant sa première prestation de services une demande au préfet de la région au sein de laquelle il souhaite l'exercer.

#### Pièces justificatives

La demande doit comporter les documents suivants, le cas échéant assortis de leur traduction en français :

- le formulaire de déclaration de prestation de services fixé à l'annexe de l'arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique ;
- une copie de sa pièce d'identité en cours de validité ;
- une copie de son titre de formation lui permettant d'exercer l'activité de psychomotricien dans son État d'établissement ;
- une attestation datant de moins de trois mois certifiant qu'il est légalement établi dans un État membre et n'encourt aucune interdiction d'exercer ;
- lorsque ni l'accès à l'activité, ni son exercice ne sont réglementés dans l’État membre, tout document justifiant que le professionnel a exercé l'activité de psychomotricien pendant un an au cours des dix dernières années ;
- lorsque le titre de formation a été établi par un État tiers et reconnu par un État membre :
  - la reconnaissance du titre de formation établie par l’État membre,
  - toute pièce justifiant que le professionnel a exercé pendant au moins trois ans l'activité de psychomotricien,
  - en cas de renouvellement, une copie de la déclaration précédente et de la première déclaration.

#### Délais et procédure

Le préfet informe le demandeur dans un délai d'un mois soit :

- qu'il peut débuter la prestation de services sans vérification de ses qualifications professionnelles ;
- lorsqu'il existe une différence substantielle entre la formation reçue par le demandeur et celle exigée en France pour exercer l'activité de psychomotricien, qu'il se soumette à une épreuve d'aptitude en vue de démontrer qu'il possède l'ensemble des connaissances nécessaires pour exercer ;
- qu'il ne peut débuter la prestation de services.

En l'absence de réponse par le préfet de région au-delà d'un délai d'un mois, le professionnel peut débuter sa prestation de services. Le professionnel est inscrit sur une liste particulière et reçoit un récépissé et un numéro d'inscription.

**À noter**

La déclaration doit être renouvelée tous les ans dans les mêmes conditions.

*Pour aller plus loin* : articles R. 4332-12 et R. 4331-12 à R. 4331-15 du Code de la santé publique ; arrêté du 8 décembre 2017 susvisé.

### c. Demande d'autorisation d'exercice pour le ressortissant UE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant doit adresser sa demande en deux exemplaires par lettre recommandée avec avis de réception au préfet de la région au sein de laquelle il souhaite exercer.

#### Pièces justificatives

Sa demande doit comporter :

- le [formulaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C441904E50DA962D4EC05BCFE7BC0E2B.tplgfr31s_1?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180319) de demande d'autorisation d'exercice complété et signé ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une copie de son titre de formation lui permettant d'exercer l'activité de psychomotricien et le cas échéant la photocopie de diplômes complémentaires ;
- l'ensemble des pièces permettant de justifier de ses formations continues et expériences professionnelles acquises dans un État membre ;
- une déclaration de l’État membre attestant que le ressortissant ne fait l'objet d'aucune sanction ;
- une copie de l'ensemble de ses attestations mentionnant le niveau de la formation reçue et le détail des heures et volume des enseignements suivis ;
- lorsque ni l'accès à la formation, ni son exercice ne sont réglementés dans l’État membre, tout document permettant de justifier qu'il a exercé l'activité de psychomotricien pendant un an au cours des dix dernières années ;
- lorsque le titre de formation a été délivré par un État tiers mais reconnu dans un État membre, la reconnaissance du titre de formation par l’État membre.

#### Procédure

Le préfet de région accuse réception de la demande dans un délai d'un mois à compter de sa réception. En l'absence de réponse dans un délai de quatre mois, la demande est considérée comme rejetée.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel et celle exigée pour exercer l'activité de psychomotricien en France, le préfet peut exiger qu'il se soumette à une épreuve d'aptitude ou un stage d'adaptation. L'épreuve d'aptitude prend la forme d'un examen noté sur vingt. Le stage d'adaptation doit lui s'effectuer dans un établissement de santé public ou privé sous la responsabilité d'un professionnel qualifié exerçant l'activité de psychomotricien depuis au moins trois ans.

*Pour aller plus loin* : article L. 4332-4 du Code de la santé publique ; arrêté du 30 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste, opticien-lunetier par des ressortissants des États membres de l'Union européenne ou parties à l'accord sur l'Espace économique européen.

*Pour aller plus loin* : articles R. 4332-9 à R. 4332-11 du Code de la santé publique ; arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de psychomotricien, orthophoniste, orthoptiste, audioprothésiste et opticien-lunetier.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris, ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).