﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP232" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Psychomotor therapist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="psychomotor-therapist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/psychomotor-therapist.html" -->
<!-- var(last-update)="2020-04-15 17:22:05" -->
<!-- var(url-name)="psychomotor-therapist" -->
<!-- var(translation)="Auto" -->


Psychomotor therapist
===============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:05<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The psychomotrician is a paramedical health professional whose activity consists of performing the motor rehabilitation of his patients. As such, he may have to realize on his patients:

- a psychomotor check-up
- rehabilitation of all psychomotor disorders;
- treatments to compensate:- intellectual disabilities,
  - to the characteristic, personality or emotional disorders of its patients.

This professional can only exercise on medical prescription.

*For further information*: Articles L. 4332-1 and L. 4334-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of psychomotrician the professional must:

- Be professionally qualified
- have registered his diploma.

#### Training

To be recognized as a professionally qualified, he must either:

- hold the state degree of psychomotrician (bac-3);
- have been a psychomotrician for at least three years in the ten years prior to May 8, 1988 and have completed a knowledge check within three years of that date.

**State Diploma (DE) as a psychomotrician**

Psychomotrician ED is available by competition to candidates who have been trained to acquire the necessary knowledge to practice the profession, in the medical field, in the humanities and in the specific sciences and in the mastery of techniques. psychomotors.

This three-year course is provided at a higher education institution and consists of:

- a first year consisting of a 712-hour training course divided into six theoretical teaching modules and a practical psychomotricity module;
- a second year consisting of 645 hours of training divided into five theoretical modules, a theoretic-clinical module and a practical module;
- a third year consisting of a 485-hour training course divided into a theoretical module, a theoretic-clinical module and a practical module of psychomotricity.

The applicant must also complete internships with a total volume of 600 hours and a final dissertation.

**Please note**

The curriculum is included in Schedule I of the 7 April 1998 decree on preparatory studies for the state diploma of psychomotrician.

The psychomotrician ED is issued by the regional prefect to candidates who have successfully passed the exam scheduled at the end of the training. This examination includes a employment probation lasting between 45 minutes and an hour and the defence of his brief for 45 minutes. Once the diploma has been obtained, the professional is required to register with the regional health agency (ARS) (see infra "5°). a. Obligation to register the diploma").

**Good to know**

Applicants holding one of the diplomas listed in the[Article 25](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C3A25728E791E25F8A93F957B0465010.tplgfr33s_1?idArticle=LEGIARTI000027763599&cidTexte=LEGITEXT000005625897&dateTexte=20180316) and having achieved an overall average of ten without a score below eight on a written examination of a theoretical module may be excused from completing the first year of training. If necessary, applicants will be required to provide a photocopy of their diploma admitted as a waiver from the first year of training.

*For further information*: Articles D. 4332-2 to R. 4332-8 of the Public Health Code; 7 April 1998 aforementioned.

#### Costs associated with qualification

Training to acquire psychomotrician ED is paid for and its cost varies depending on the course envisaged. It is advisable to get close to the institutions concerned for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, which is legally established and engaged in psychomotrician activity, may engage in the same temporary and occasional activity in France without having to register it.

Where neither access nor the practice of the profession is regulated in the Member State, the professional must have been a psychomotrician for at least one year in the last ten years in one or more member states.

Once the national meets these conditions, he or she will have to make a declaration before the first service is provided (see below "5°). b. Pre-declaration for the EU national for a temporary and casual exercise (LPS)").

**Please note**

Where there are substantial differences between the training received by the professional and that required to carry out the activity of psychomotrician in France, the prefect may decide to submit it to an aptitude test.

In addition, the national must justify having the language skills necessary to practice the profession of psychomotrician in France.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of a Member State of the European Union (EU) or of a State party to the agreement on the European Economic Area (EEA) legally established and practising the activity of psychomotrician, may carry out the same activity on a permanent basis in France.

In order to do so, the person concerned must be the holder of:

- a training certificate issued by a Member State allowing it to carry out the activity of psychomotrician as long as that State regulates the activity;
- where the state does not regulate access to or exercise of the profession of a training document that it has been prepared for the practice of the profession. It must also justify a one-year professional experience in the last ten years;
- a training certificate issued by a third state and recognised in an EU Member State allowing it to practise that profession and three years of professional experience in that Member State.

Once the national meets these conditions, he must apply to the regional prefect for permission to exercise (see below "5°). c. Application for authorisation to exercise for the EU national for a permanent exercise (LE)).

**Please note**

When the examination of his professional qualifications reveals substantial differences between the training received by the national and the training required to carry out the activity in France, the prefect may decide to subject him to a measure of compensation (see infra "5.00. c. Good to know: compensation measures").

In addition, just as in the context of the free provision of services, the national must justify the language skills necessary for the exercise of his profession in France.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The psychomotrician, as a health professional, is bound to respect the patient's privacy and professional secrecy. In addition, he must inform his patients of their state of health and in particular on:

- The different treatments on offer
- The risks involved
- possible alternatives in the event of a refusal.

*For further information*: Articles L. 1110-4 and L. 1111-2 of the Public Health Code.

4°. Insurance and sanctions
-----------------------------------------------

**Insurance**

The psychomotrician as a health professional is required to take out professional liability insurance for the risks incurred during the course of his activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

**Criminal sanctions**

The act of performing as a psychomotrician without being qualified professionally is considered a usurpation of securities and is punishable by one year's imprisonment and a fine of 15,000 euros.

*For further information*: Article L. 4334-2 of the Public Health Code; Article 433-17 of the Penal Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obligation to register the diploma

**Competent authority**

The applicant must apply to the LRA for registration.

**Supporting documents**

The applicant must provide the following information, deemed validated and certified by the organization that issued the diploma or training title:

- The civil status of the holder of the diploma and all the data to identify the applicant;
- The name and address of the institution that delivered the training;
- the title of the training.

**Outcome of the procedure**

After checking the exhibits, the LRA registers the diploma.

**Please note**

Registration is only possible for one department. However, if the professional wishes to practice in several departments, the applicant will be placed on the list of the department in which the main place of his activity is located.

**Cost**

Free.

*For further information*: Articles L. 4333-1 and D. 4333-1 of the Public Health Code.

### b. Pre-declaration for EU national for temporary and casual exercise (LPS)

**Competent authority**

The national must apply to the prefect of the region in which he wishes to practice before his first service delivery.

**Supporting documents**

The application must include the following documents, if any, with their translation into French:

- the service delivery reporting form attached to the schedule of the December 8, 2017 order on the prior declaration of service delivery for genetic counsellors, medical physicists and pharmacy and pharmacy preparers hospital pharmacy, as well as for occupations in Book III of Part IV of the Public Health Code;
- A copy of his valid ID
- A copy of his training degree allowing him to carry out the activity of psychomotrician in his state of establishment;
- a certificate less than three months old certifying that it is legally established in a Member State and does not incur any prohibition on practising;
- where neither access to the activity nor its exercise is regulated in the Member State, any documentation justifying that the professional has been a psychomotrician for one year in the last ten years;
- where the training designation has been established by a third state and recognised by a Member State:- Recognition of the training title established by the Member State,
  - any evidence that the professional has been working as a psychomotrician for at least three years,
  - in the event of a renewal, a copy of the previous statement and the first statement.

**Delays and procedures**

The prefect informs the applicant within one month:

- that he can start providing services without checking his professional qualifications;
- where there is a substantial difference between the training received by the applicant and that required in France to carry out the activity of psychomotrician, that he submits to an aptitude test in order to demonstrate that he owns all the Knowledge needed to practice;
- that it cannot begin service delivery.

In the absence of a response by the regional prefect beyond a one-month period, the professional can begin his service delivery. The professional is on a particular list and receives a receipt and registration number.

**Please note**

The declaration must be renewed every year under the same conditions.

*For further information*: Articles R. 4332-12 and R. 4331-12 to R. 4331-15 of the Public Health Code; December 8, 2017 aforementioned.

### c. Application for authorisation to exercise for the EU national for a permanent exercise (LE)

**Competent authority**

The national must send his application in two copies by letter recommended with notice of receipt to the prefect of the region in which he wishes to practice.

**Supporting documents**

His application must include:

- The[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C441904E50DA962D4EC05BCFE7BC0E2B.tplgfr31s_1?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180319) Application for a completed and signed exercise authorization;
- A photocopy of a valid ID
- A copy of his training title allowing him to carry out the activity of psychomotrician and, if necessary, a photocopy of additional diplomas;
- all the documents to justify his continuous training and professional experience acquired in a Member State;
- a statement from the Member State stating that the national is not subject to any sanction;
- A copy of all of his certificates mentioning the level of training received and the details of the hours and volume of the teachings followed;
- where neither access to training nor its exercise is regulated in the Member State, any documentation to justify that it has been a psychomotrician for one year in the last ten years;
- where the training certificate has been issued by a third state but recognised in a Member State, the Recognition of the Training Title by the Member State.

**Procedure**

The regional prefect acknowledges receipt of the request within one month of receiving it. If no response is made within four months, the application is considered rejected.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the professional and that required to carry out the activity of psychomotrician in France, the prefect may require that he submit to an aptitude test or an adjustment course. The aptitude test takes the form of one in twenty graded exams. The accommodation course must be carried out in a public or private health facility under the responsibility of a qualified professional who has been working as a psychomotrician for at least three years.

*For further information*: Article L. 4332-4 of the Public Health Code; decree of 30 March 2010 setting out the organisation of the aptitude test and the adaptation course for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist, optician-lunetier by European Union Member States or parties to the European Economic Area Agreement.

*For further information*: Articles R. 4332-9 to R. 4332-11 of the Public Health Code; decree of 25 February 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the practice in France of the professions of psychomotrician, speech therapist, orthoptist, audioprosthetist and optician-lunetier.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris, ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

