﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP043" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Nursery nurse" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="nursery-nurse" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/nursery-nurse.html" -->
<!-- var(last-update)="2020-04-15 17:21:15" -->
<!-- var(url-name)="nursery-nurse" -->
<!-- var(translation)="Auto" -->


Nursery nurse
===================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The childcare assistant is a professional who provides care, under the responsibility of the nurse or childminder, and carries out activities of awakening and education to preserve and restore the continuity of life, well-being and the child's autonomy. It also participates in the reception and social integration of children with disabilities, chronic diseases or at risk of exclusion.

*To go further* : Appendix I of the january 16, 2006 decree on training leading to the State Diploma of Childcare Auxiliary.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Persons holding the State Diploma of Childcare Auxiliary, Certificate of Fitness for Childcare Auxiliary or Professional Diploma of Auxiliary of Childcare, Child Care Auxiliary May practise as a childcare assistant. Child welfare.

*To go further* Article L. 4392-1 of the Public Health Code.

#### Training

The State Diploma of Childcare Auxiliary is prepared, more often than not, in ten months of continuing education (or eighteen months in case of discontinuous training).

The training, which takes place in a specialized school or training institute, is open to all persons over the age of seventeen who pass the entrance competition in one of the institutes or training schools. On the other hand, no degree requirements are required.

The final admission to a training institute for childcare assistants is conditional on the production of a medical certificate, established by a medical officer, certifying that the candidate does not present a physical or psychological contraindication to and that its vaccinations comply with current regulations.

The state diploma can also be obtained by validation of the experience (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

*To go further* 16 January 2006.

#### Costs associated with qualification

Training as a childcare assistant pays off. Its cost varies, depending on the training organization, between 2,000 and 5,000 euros (amount as an indication). For more information, it is advisable to get closer to the training centre in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

The national of a Member State of the European Union (EU)*or party to the European Economic Area (EEA) agreement*, legally established in one of these states may carry out the same activity in France on a temporary and occasional basis provided that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If neither access nor training is regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

The provider's professional qualifications are checked before the first service is provided. In the event of a substantial difference between the claimant's qualifications and the training required in France, which is likely to harm public health, the competent authority asks the claimant to prove that he has acquired the knowledge and skills compensation measures (see infra "Good to know: compensation measures").

In all cases, the European national wishing to practise in France on a temporary or occasional basis must possess the necessary language skills to carry out the activity and master the weight and measurement systems used in France. France.

*To go further* Articles L. 4392-4 and L. 4392-5 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state wishing to practise as a childcare assistant in France on a permanent basis must obtain a licence to practise.

EU or EEA nationals who have successfully completed a high school education may be allowed to practise in France if they hold:

- a training certificate issued by an EU or EEA state in which access or practice of the profession is regulated and which allows legal practice to be practised;
- a training certificate issued by an EU or EEA state in which neither access nor the practice of the profession is regulated, accompanied by a certificate justifying the exercise of this activity in that state for at least two years to full-time over the past ten years;
- a training certificate, issued by a third state, recognised in an EU or EEA state, other than France, allowing the profession to be legally practised there.

Nationals wishing to work in France as childcare assistants must have the language skills necessary to carry out their activity and those relating to the weight and measurement systems used in France.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected, depending on his choice, to an test (see below: "Good to know: compensation measures").

*To go further* Articles L. 4392-2 and L. 4392-5 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### Obligation of continuing vocational training

Childcare assistants must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of presentational, mixed or non-presential training. All training is recorded in a personal document containing training certificates.

*To go further* : decree of 30 December 2011 on the continued professional development of allied health professionals.

4°. Insurance
---------------------------------

In the event of a liberal exercise, the childcare assistant is required to take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out on occasion.

5°. Qualification recognition process and formalities
---------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally practises as a childcare assistant in an EU or EEA state may practice the same profession in France on a temporary or occasional basis if he makes the prior declaration.

If neither access nor the exercise of this activity is regulated in the EU or EEA State of practice, the person concerned must provide evidence of a professional exercise of at least two years full-time in the last ten years.

The person's professional qualifications are checked before the first service is provided. In the event of substantial differences between the person's qualifications and the training required in France, the claimant may be required to prove that he has acquired the missing knowledge and skills, including through compensation (see below: "Good to know: compensation measures").

#### Competent authority

The prior declaration of activity must be addressed, before the first performance, to the regional directorate of youth, sports and social cohesion (DRJSCS) of the Loire region.

#### Renewal of pre-declaration

The advance declaration must be renewed once a year if the claimant wishes to make a new benefit in France.

#### Procedure

The Minister responsible for health decides after the opinion of the Committee of Childcare Assistants of the Loire region. Within one month of receiving the statement, the Minister informs the individual that he or she may or may not begin the provision of services or that he must provide proof that he has acquired the missing knowledge and skills, including through compensation measures.

The Minister may also inform within one month of receiving the declaration of the need for further information. In this case, the Minister has two months from receiving additional information to inform the person concerned whether or not he can begin his service delivery.

In the absence of a response from the Minister responsible for health within the specified time frame, service delivery may begin.

#### Delivery of receipt

The Minister responsible for health lists the health care provider on a particular list and sends him a receipt with his registration number. The claimant must then inform the relevant national insurance agency of its benefit by sending a copy of that receipt.

#### Supporting documents

The supporting documents to be provided are:

- the completed declaration form, the model of which is presented as an appendix to the March 24, 2010 order setting out the composition of the file to be provided to the relevant licensing commissions for the review of applications submitted for the fiscal year France of the professions of nursing assistant, childcare assistant and ambulance worker;
- Photocopying of an ID, supplemented if necessary by a certificate of nationality;
- Photocopying of the training title (translated into French by a certified translator);
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and does not incur any prohibition of practising (translated into French by a certified translator).

#### Cost

Free

*To go further* Articles L. 4392-4, R. 4392-5, R. 4331-12 to R. 4331-15 of the Public Health Code, order of 24 March 2010 above and order of 11 August 2010 appointing regional commissions to give notice of declarations of free provision of services for paramedics, nursing assistants, childcare assistants and paramedics.

### b. Formalities for EU nationals for a permanent exercise (LE)

#### Asking for permission to practice

A national of an EU or EEA state must obtain an authorisation to be able to work permanently as a childcare assistant on French territory.

##### Competent authority

The request must be addressed to the regional prefect of the person's place of settlement. The prefect issues the authorisation to exercise, after advice from the Commission of Childcare Assistants.

##### Procedure

The applicant must submit his application in duplicate by letter recommended with notice of receipt accompanied by a complete file to the Regional Directorate of Youth, Sports and Social Cohesion (DRJSCS) of the place where he wishes to practice.

##### Time

The regional prefect must acknowledge receipt within one month of receipt of the application accompanied by the full file. In the event of an incomplete file, the prefect must indicate the missing documents and the time frame within which they must be communicated to him.

If the prefect remains silent for four months after the application is received, the application for leave is considered rejected.

##### Supporting documents

The supporting documents to be provided are:

- The application form for authorisation to practice the profession, the model of which is provided as an appendix to the order of March 24, 2010;
- A photocopy of a valid ID
- A copy of the training title allowing the profession to be practised in the country of obtainment;
- A copy of the additional diplomas if necessary;
- all useful documents justifying continuous training, experience and skills acquired during work experience in an EU state, EEA or third state;
- a statement by the competent authority of the EU State or the Establishment EEA, less than a year old, attesting to the absence of sanctions;
- A copy of the certificates of the authorities issuing the training certificate specifying the level of training, the details of the lessons year by year and the content and duration of the internships carried out;
- for those who have worked in an EU or EEA state that does not regulate access to or exercise of the profession, all documents justifying an exercise in that state for the equivalent of two full-time years in the last ten years;
- for those persons holding a training certificate issued by a third state and recognised in an EU or EEA state, other than France, the recognition of the training certificate established by the state authorities having recognised that title.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

##### Remedies

If the application for leave is rejected, the applicant may initiate:

- a graceful appeal to the regional prefect who made the decision within two months of the decision (implicit or express) of rejection;
- a hierarchical appeal to the Minister responsible for health within two months of the decision (implicit or express) of rejection;
- a legal appeal before the territorially competent administrative court within two months of the decision (implicit or express) of rejection.

In order to obtain permission to practise, the person concerned may be required to undergo an aptitude test or an adjustment course if it appears that the professional qualifications and experience he uses are substantially different from those required for the practice of the profession in France (see Infra "Good to know").

**Good to know: compensation measures**

In order to obtain permission to practise, the person concerned may be required to undergo an aptitude test or an adjustment course, if it appears that the professional qualifications and experience he uses are substantially different. those required for the practice of the profession in France.

If compensation measures are deemed necessary, the regional prefect responsible for issuing the exercise authorization indicates to the person concerned that he has two months to choose between the aptitude test and the adjustment course.

##### The aptitude test

The DRJSCS, which organises the aptitude tests, must summon the person by recommended letter with notice of receipt at least one month before the start of the tests. This summons mentions the day, time and place of the trial.

The aptitude test may take the form of written or oral questions noted on 20 on each of the subjects that were not initially taught or not acquired during the professional experience.

Admission is pronounced on the condition that the individual has achieved a minimum average of 10 out of 20, with no score less than 8 out of 20. The results of the test are notified to the person concerned by the regional prefect.

If successful, the regional prefect authorizes the person concerned to practise the profession.

##### The adaptation course

It is carried out in a public or private health facility approved by the regional health agency (ARS). The trainee is placed under the pedagogical responsibility of a qualified professional who has been practising the profession for at least three years and who establishes an evaluation report.

The internship, which eventually includes additional theoretical training, is validated by the head of the reception structure on the proposal of the qualified professional evaluating the trainee.

The results of the internship are notified to the person concerned by the regional prefect.

The application for leave to practice is then made after further notice from the Nursing Committee.

*To go further* Articles L. 4392-2, R. 4392-1 and following refers to articles R. 4311-35 and R. 4311-36 of the Public Health Code and ordered from March 24, 2010.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris,[official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

