﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP214" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Pharmacien spécialisé en biologie médicale" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="pharmacien-specialise-en-biologie" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/pharmacien-specialise-en-biologie-medicale.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="pharmacien-specialise-en-biologie-medicale" -->
<!-- var(translation)="None" -->

# Pharmacien spécialisé en biologie médicale

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le pharmacien spécialisé en biologie médicale est un professionnel de la santé dont l'activité consiste à réaliser un examen médical en vue de la prévention, du diagnostic et de la prise en charge des patients présentant des troubles pathologiques.

Le pharmacien spécialiste (également appelé biologiste médical) est amené à réaliser cet examen au sein d'un laboratoire de biologie médicale.

*Pour aller plus loin* : article L. 6211-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer en tant que pharmacien spécialisé en biologie médicale, le professionnel doit être :

- de nationalité française, andorrane, ou ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) ;
- titulaire d'un diplôme français d’État de docteur en pharmacie ou de pharmacien ainsi que :
  - d'un diplôme de spécialité en biologie médicale,
  - d'une qualification en biologie médicale délivrée par le conseil national de l'ordre des pharmaciens (cf. infra « 2°. a. Diplôme d'études spécialisées en biologie médicale (DESBM) »,
  - d'une autorisation individuelle d'exercice délivrée par le ministre chargé de la santé, dans les conditions prévues à l'article L. 4111-2 du Code de la santé publique ;
- être inscrit à l'ordre des pharmaciens (cf. infra « 5°. a. Inscription au tableau de l'ordre des pharmaciens ».

En outre, certains professionnels peuvent faire reconnaître leur domaine de spécialisation (cf. ci-après « Reconnaissance d'un domaine de spécialisation »).

*Pour aller plus loin* : article L. 6213-1 du Code de la santé publique.

#### Reconnaissance d'un domaine de spécialisation

Peut également exercer en tant que biologiste médical le professionnel qui, à compter du 30 mai 2013 (date d'entrée en vigueur de l'ordonnance n° 2010-49 du 13 janvier 2010 relative à la biologie médicale) :

- a pratiqué la biologie médicale dans un établissement de santé pendant au moins deux ans au cours des dix dernières années, ou qui a commencé à exercer entre le 13 janvier 2008 et le 13 janvier 2010 et a exercé cette activité pendant deux ans avant le 13 janvier 2012. Toutefois le professionnel qui a exercé la biologie médicale dans un domaine de spécialisation ne peut exercer qu'au sein de ce domaine ;
- a exercé la profession de vétérinaire, et qui ayant commencé une spécialisation en biologie médicale avant le 13 janvier 2010, a obtenu sa spécialisation au plus tard le 13 janvier 2016 ;
- a exercé les fonctions de directeur ou directeur adjoint d'un centre national pour la lutte contre les maladies transmissibles et autorisé à exercer la biologie médicale par le ministre chargé de la santé ;
- a été autorisé par le ministre chargé de la santé, dans un domaine de spécialisation de son laboratoire ;
- a exercé pendant au moins trois ans les fonctions de biologiste médical dans une structure ou un laboratoire de biologie médicale.

Le cas échéant, le professionnel devra effectuer une demande de reconnaissance de son domaine de spécialisation (cf. infra « 5°. a. Demande de reconnaissance d'un domaine de spécialisation »).

En outre, le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles devra également effectuer, une demande d'autorisation d'exercice (cf. infra « 5°. c. Demande d'autorisation d'exercice pour le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles »).

*Pour aller plus loin* : articles L. 6213-2 et L. 6213-2-1 du Code de la santé publique.

#### Formation

Pour connaître les modalités de formation en vue d'obtenir le titre de pharmacien il est conseillé de se reporter à la fiche « [Pharmacien](https://www.guichet-qualifications.fr/fr/dqp/sante/pharmacien.html) ».

##### Diplôme d'études spécialisées en biologie médicale (DESBM)

Pour exercer en tant que biologiste médical, le pharmacien doit être titulaire du DES « Biologie médicale ».

Cette formation est accessible aux étudiants en pharmacie ayant réussi le concours de l'internat et permet au futur spécialiste d'acquérir les connaissances et la pratique nécessaires à l'exercice de ses fonctions.

Elle se compose de huit semestres dont au moins trois dans un lieu de stage avec encadrement universitaire et au moins un dans un lieu de stage sans encadrement universitaire.

**À noter**

Dans le cadre de son projet professionnel, le futur biologiste médical peut candidater à une formation spécialisée transversale (FST) parmi les suivantes :

- bio-informatique médicale ;
- génétique et médecine moléculaire bioclinique ;
- hématologie bioclinique ;
- hygiène - prévention de l'infection, résistances ;
- médecine et biologie de la reproduction - andrologie ;
- nutrition appliquée ;
- pharmacologie médicale/thérapeutique ;
- thérapie cellulaire/transfusion.

La formation du DES se décompose en trois phases : la phase socle, la phase d'approfondissement et la phase de consolidation.

###### La phase socle

D'une durée de quatre semestres, elle permet au candidat d'acquérir les connaissances de base de la spécialité choisie et d'effectuer :

- trois stages dans un lieu agréé à titre principal en biologie médicale et doit couvrir les domaines de la biochimie-biologie moléculaire, l'hématologie et la bactériologie-virologie ;
- un stage dans un lieu agréé à titre principal en biologie médicale et dans un domaine différent du précédent stage.

###### La phase d'approfondissement

D'une durée de deux semestres, elle permet au candidat d'acquérir les connaissances et compétences nécessaires à l'exercice de la spécialité choisie et d'effectuer deux stages dans un lieu agréé à titre principal en biologie médicale et bénéficiant d'un agrément pour ce domaine.

###### La phase de consolidation

D'une durée de deux semestres, elle permet au candidat de consolider ses connaissances et d'effectuer :

- un stage d'un semestre dans un lieu agréé à titre principal en biologie médicale ;
- un stage d'un semestre dans un lieu bénéficiant d'un agrément fonctionnel en biologie médicale ou agréé à titre principal dans une autre spécialité et à titre secondaire en biologie médicale.

*Pour aller plus loin* : arrêté du 27 novembre 2017 modifiant l’arrêté du 12 avril 2017 relatif à l’organisation du troisième cycle des études de médecine et l’arrêté du 21 avril 2017 relatif aux connaissances, aux compétences et aux maquettes de formation des diplômes d’études spécialisées et fixant la liste de ces diplômes et des options et formations spécialisées transversales du troisième cycle des études de médecine.

#### Coûts associés à la qualification

Les formations menant à l'obtention du diplôme de pharmacien et du diplôme de spécialisation en biologie médicale, sont payantes et leur coût varie selon l'établissement choisi. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État de l'UE ou de l'EEE légalement établi et exerçant l'activité de pharmacien spécialisé en biologie médicale peut exercer à titre temporaire et occasionnel la même activité.

Pour cela, il devra effectuer une déclaration préalable et justifier être titulaire des connaissances linguistiques nécessaires pour exercer en France.

Pour connaître les modalités de cette déclaration préalable, il est conseillé de se reporter au paragraphe « 5°. Démarche et formalités de reconnaissance de qualification » de la fiche « [Pharmacien](https://www.guichet-qualifications.fr/fr/dqp/sante/pharmacien.html) ».

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE, légalement établi et exerçant l'activité de pharmacien, peut exercer à titre permanent la même activité.

À ce titre, le ressortissant peut bénéficier soit :

- du régime de reconnaissance automatique de son diplôme (cf. ci-après «  Bon à savoir : la reconnaissance automatique de diplôme ») ;
- du régime de l'autorisation individuelle d'exercer. Le cas échéant, le profession devra effectuer une demande d'autorisation (cf. infra « 5°. d. Le cas échéant, demander une autorisation individuelle d'exercice »).

#### Bon à savoir : La reconnaissance automatique de diplôme

Peut exercer la profession de pharmacien, le ressortissant d'un État membre de l'UE ou de l'EEE, dès lors qu'il est titulaire de l'un des titres ou diplômes suivants :

- un titre de formation de pharmacien délivré par un État membre et figurant à l'annexe de l'arrêté du 13 février 2007 fixant la liste des diplômes, certificats et autres titres de pharmacien délivrés par les États membres de l'Union européenne, les États parties à l'accord sur l'Espace économique européen et la Confédération suisse visée à l'article L. 4221-4 (1°) du Code de la santé publique ;
- un titre de formation de pharmacien délivré par un État membre de l'UE ou de l'EEE ne figurant pas sur cette liste, accompagné d'une attestation de formation ;
- un titre de formation de pharmacien délivré par un État membre de l'UE ou de l'EEE sanctionnant une formation commencée avant le 13 février 2007 et accompagné d'une attestation de cet État, certifiant que le ressortissant à exercé cette activité pendant au moins trois ans de manière consécutive au cours des cinq années précédant a délivrance de l'attestation ;
- un titre délivré par un État de l'UE ou de l'EEE, sanctionnant une formation de pharmacien commencée avant l'une des dates citées au sein de l'arrêté du 13 février 2007, et permettant d'exercer légalement la profession de pharmacien dans cet État. Le ressortissant devra justifier avoir effectué en France trois années dans la fonction hospitalière en qualité d'attaché associé, de praticien attaché associé, d'assistant associé ou dans la fonction universitaire en qualité de chef de clinique associé des universités ou d'assistant associé des universités.

*Pour aller plus loin* : articles L. 4221-2, L. 4221-4 et L. 4221-5 du Code de la santé publique ; arrêté du 13 février 2007 fixant la liste des diplômes, certificats et autres titres de pharmacien délivrés par les États membres de l'Union européenne, les États parties à l'accord sur l'Espace économique européen et la Confédération suisse visée à l'article L. 4221-4 (1°) du Code de la santé publique.

*Pour aller plus loin* : article L. 6213-1 du Code de la santé publique.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### a. Respect du Code de déontologie des pharmaciens

Le pharmacien spécialisé en biologie médical est soumis au respect des règles professionnelles applicables aux pharmaciens.

L'ensemble des dispositions du Code de déontologie des pharmaciens sont codifiées aux articles R. 4235-1 à R. 4235-77 du Code de la santé publique.

À ce titre, le professionnel doit notamment respecter les principes de dignité, de non-discrimination, de secret professionnel ou encore d'indépendance.

*Pour aller plus loin* : article L. 6213-7 du Code de la santé publique.

### b. Cumul d'activités

Le pharmacien ne peut exercer une autre activité que si un tel cumul est compatible avec les principes d’indépendance et de dignité professionnelle qui s’imposent à lui.

Il est également interdit à un pharmacien qui remplit un mandat électif ou une fonction administrative d’en user pour accroître sa clientèle.

*Pour aller plus loin* : articles R. 4235-4 et R. 4235-23 du Code de la santé publique.

### c. Obligation de développement professionnel continu

Les pharmaciens spécialisés en biologie médicale doivent participer annuellement à un programme de développement professionnel continu. Ce programme vise à maintenir et actualiser leurs connaissances et leurs compétences ainsi qu'à améliorer leurs pratiques professionnelles.

À ce titre, le professionnel de santé (salarié ou libéral) doit justifier de son engagement dans une démarche de développement professionnel. Le programme se présente sous la forme de formations (présentielles, mixtes ou non-présentielles) d’analyse, d’évaluation, et d’amélioration des pratiques et de gestion des risques. L’ensemble des formations suivies est consigné dans un document personnel contenant les attestations de formation.

*Pour aller plus loin* : article R. 4235-11 du Code de la santé publique ; décret n° 2016-942 du 8 juillet 2016 relatif à l'organisation du développement professionnel continu des professionnels de santé.

## 4°. Assurances

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, le pharmacien exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

### b. Obligation d'affiliation à la Caisse d'assurance vieillesse des pharmaciens (CAVP)

Tout pharmacien inscrit au tableau de l'Ordre des pharmaciens et exerçant sous la forme libérale (même à temps partiel et même s’il exerce par ailleurs une activité salariée) a l’obligation d’adhérer à la CAVP.

L’intéressé doit se déclarer à la CAVP dans le mois suivant le début de son activité libérale.

*Pour aller plus loin* : article R. 643-1 du Code de la sécurité sociale ; site de la [CAVP](https://www.cavp.fr/).

### c. Obligation de déclaration auprès de l’Assurance maladie

Une fois inscrit au tableau de l’Ordre, le pharmacien exerçant sous forme libérale doit déclarer son activité auprès de la Caisse primaire d’assurance maladie (CPAM).

#### Modalités

L’inscription auprès de la CPAM peut être réalisée en ligne sur le site officiel de l’Assurance maladie.

#### Pièces justificatives

Le déclarant doit communiquer un dossier complet comprenant :

- la copie d’une pièce d’identité en cours de validité ;
- un relevé d’identité bancaire (RIB) professionnel.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande d'inscription à l'Ordre national des pharmaciens

L'inscription au tableau de l'Ordre est obligatoire pour exercer légalement l'activité de pharmacien en France.

L'inscription ne s'applique pas :

- aux pharmaciens inspecteurs de santé publique, inspecteurs des agences régionales de santé, inspecteurs de l'Agence nationale de sécurité du médicament et des produits de santé ;
- aux pharmaciens fonctionnaires ou assimilés du ministère chargé de la santé ;
- aux pharmaciens fonctionnaires ou assimilés du ministère chargé de l'enseignement supérieur, n'exerçant pas, par ailleurs, d'activité pharmaceutique ;
- aux pharmaciens appartenant au cadre actif du service de santé des armées de terre, de mer et de l'air.

**À noter **

L’inscription au tableau de l’Ordre permet la délivrance automatique et gratuite de la carte de professionnel de santé (CPS). La CPS est une carte d’identité professionnelle électronique. Elle est protégée par un code confidentiel et contient notamment les données d’identification de pharmacien (identité, profession, spécialité). Pour plus d’informations, il est recommandé de se reporter au site gouvernemental de l’Agence française de la santé numérique.

#### Autorité compétente

L'article R. 4222-1 prévoit que la demande d’inscription est adressée, selon son cas, au président du conseil régional de la région dans laquelle le pharmacien veut exercer, ou au président du conseil central de la section.

#### Pièces justificatives

L’intéressé doit adresser, par tout moyen, un dossier complet de demande d’inscription comprenant :

- deux exemplaires du questionnaire normalisé avec une photo d’identité rempli, daté et signé, disponible dans les conseils départementaux de l’Ordre ou directement téléchargeable sur le site officiel du Conseil national de l’Ordre des pharmaciens ;
- une photocopie d’une pièce d’identité en cours de validité ou, le cas échéant, une attestation de nationalité délivrée par une autorité compétente ;
- le cas échéant, une photocopie de la carte de séjour de membre de la famille d’un citoyen de l’UE en cours de validité, de la carte de résident de longue durée-CE en cours de validité ou de la carte de résident portant mention du statut de réfugié en cours de validité ;
- le cas échéant, une photocopie de la carte professionnelle européenne en cours de validité ;
- une copie, accompagnée le cas échéant d’une traduction faite par un traducteur agréé, des titres de formation à laquelle sont joints :
  - lorsque le demandeur est un ressortissant de l’UE ou de l’EEE, la ou les attestations prévues (cf. supra « 2°. a. Exigences nationales »),
  - lorsque le demandeur bénéficie d’une autorisation d’exercice individuelle (cf. supra « 2°. c. Ressortissants UE et EEE : en vue d’un exercice permanent »), la copie de cette autorisation,
  - lorsque le demandeur présente un diplôme délivré dans un État étranger dont la validité est reconnue sur le territoire français, la copie des titres à la possession desquels cette reconnaissance peut être subordonnée ;
- pour les ressortissants d’un État étranger, un extrait de casier judiciaire ou un document équivalent datant de moins de trois mois, délivré par une autorité compétente de l’État d’origine. Cette pièce peut être remplacée, pour les ressortissants des États de l’UE ou de l’EEE qui exigent une preuve de moralité ou d’honorabilité pour l’accès à l’activité de médecin, par une attestation, datant de moins de trois mois, de l’autorité compétente de l’État d’origine certifiant que ces conditions de moralité ou d’honorabilité sont remplies ;
- une déclaration sur l’honneur du demandeur certifiant qu’aucune instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur l’inscription au tableau n’est en cours à son encontre ;
- un certificat de radiation d’inscription ou d’enregistrement délivré par l’autorité auprès de laquelle le demandeur était antérieurement inscrit ou enregistré ou, à défaut, une déclaration sur l’honneur du demandeur certifiant qu’il n’a jamais été inscrit ou enregistré ou, à défaut, un certificat d’inscription ou d’enregistrement dans un État de l’UE ou de l’EEE ;
- tous les éléments de nature à établir que le demandeur possède les connaissances linguistiques nécessaires à l’exercice de la profession ;
- un curriculum vitae.

**À savoir** 

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

Le conseil régional ou central statue sur la demande d’inscription dans un délai maximum de trois mois à compter de la réception du dossier complet de demande. À défaut de réponse dans ce délai, la demande d’inscription est réputée rejetée.

Ce délai est porté à six mois pour les ressortissants des États tiers lorsqu’il y a lieu de procéder à une enquête hors de la France métropolitaine. L’intéressé en est alors avisé.

Si l’inscription est accordée, la décision adoptée par le conseil est notifiée à l’intéressé par lettre recommandée, accompagnée de son certificat d’inscription.

La décision du conseil régional est également susceptible d’appel, dans les 30 jours, auprès du Conseil national de l’Ordre des pharmaciens. La décision ainsi rendue peut elle-même faire l’objet d’un recours devant le Conseil d’État.

#### Coût

L’inscription au tableau de l’Ordre est gratuite mais elle engendre l’obligation de s’acquitter de la cotisation ordinale obligatoire dont le montant est fixé annuellement et qui doit être versée au cours du premier trimestre de l’année civile en cours.

*Pour aller plus loin* : articles L. 4222-1 à L. 4222-8, et R. 4222-1 à R. 4222-4-3 du Code de la santé publique.

### b. Demande de reconnaissance de spécialisation

#### Autorité compétente

Le professionnel doit adresser sa demande au Centre national de gestion par lettre recommandée avec avis de réception à l'adresse suivante :

Centre national de gestion, département concours, autorisations d'exercice, mobilité-développement professionnel, 21 B, rue Leblanc 75737 Paris Cedex 15

#### Pièces justificatives

Sa demande doit comporter les documents suivants :

- une lettre de demande motivée et mentionnant le domaine de spécialisation envisagé ;
- une photocopie d'une pièce d'identité en cours de validité ;
- un curriculum vitae détaillé ainsi que les éventuelles publications du demandeur ;
- une copie de l'ensemble de ses diplômes ;
- une attestation justifiant que le ressortissant à exercé la biologie médicale pendant au moins deux ans au cours des dix dernières années et avant le 13 janvier 2012 ;
- pour le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles, son doctorat d'exercice ou d'université, ou son diplôme d'ingénieur en lien avec la biologie médicale ;
- pour le pharmacien exerçant au sein d'un centre hospitalier ou universitaire, un document justifiant qu'il exerce bien au sein d'un tel établissement depuis au moins trois ans et mentionnant son statut.

#### Délai et procédure

Le Centre national de gestion accuse réception de la demande dans un délai d'un mois à compter de sa réception et la transmet à la commission nationale de biologie médicale. En l'absence de réponse dans un délai de quatre mois à compter de la réception du dossier complet, la reconnaissance est accordée au demandeur.

*Pour aller plus loin* : article R. 6213-2 du Code de la santé publique ; arrêté du 14 février 2017 fixant la composition du dossier à fournir à la Commission nationale de biologie médicale prévue à l'article L. 6213-12 du Code de la santé publique et définissant les domaines de spécialisation mentionnés à l'article R. 6213-1 du même Code.

### c. Demande d'autorisation d'exercice pour le directeur ou le directeur adjoint d'un centre national de lutte contre les maladies transmissibles

#### Autorité compétente

Le professionnel doit adresser sa demande au Centre national de gestion par lettre recommandée avec avis de réception.

#### Pièces justificatives

Sa demande doit comporter :

- une lettre motivée de demande ;
- une photocopie d'une pièce d'identité en cours de validité ;
- un curriculum vitae décrivant son cursus professionnel et le cas échéant, l'ensemble de ses publications ;
- une copie de sa demande d'autorisation d'exercice des fonctions de directeur ou directeur adjoint de laboratoire ;
- une photocopie de l'ensemble de ses diplômes, titres, ou certifications dans le domaine de la biologie médicale.

#### Délai et procédure

Le Centre national de gestion accuse réception de la demande dans un délai d'un mois à compter de sa réception. L'absence de réponse du ministre chargé de la santé au delà d'un délai de quatre mois vaut rejet de la demande d'autorisation.

*Pour aller plus loin* : R. 6213-5 à R. 6213-7 du Code de la santé publique, arrêté du 14 février 2017 susvisé ; V de l'article 9 de l'ordonnance n° 2010-49 du 13 janvier 2010 relative à la biologie médicale.

### d. Le cas échéant, demander une autorisation individuelle d’exercice

Si le ressortissant ne relève pas du régime de reconnaissance automatique de son diplôme, il doit solliciter une autorisation d'exercer.

#### Autorité compétente

La demande est adressée en deux exemplaires, par lettre recommandée avec demande d’avis de réception à la cellule chargée des commissions d’autorisation d’exercice (CAE) du Centre national de gestion (CNG).

Il doit adjoindre à sa demande :

- le [formulaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4349F01DDAE283C55604F0C28FE06D3D.tplgfr23s_3?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180118) de demande d’autorisation d’exercice de la profession ;
- une photocopie d’une pièce d’identité en cours de validité ;
- une copie du titre de formation permettant l’exercice de la profession dans l’État d’obtention ainsi que, le cas échéant, une copie du titre de formation de spécialiste ;
- le cas échéant, une copie des diplômes complémentaires ;
- toutes pièces utiles justifiant des formations continues, de l’expérience et des compétences acquises au cours de l’exercice professionnel dans un État de l’UE ou de l’EEE, ou dans un État tiers (attestations de fonctions, bilan d’activité, bilan opératoire, etc.) ;
- dans le cadre de fonctions exercées dans un État autre que la France, une déclaration de l’autorité compétente de cet État, datant de moins d’un an, attestant de l’absence de sanctions à l’égard du demandeur.

Selon la situation du demandeur, d’autres pièces justificatives sont exigées. Pour plus d’informations, il est conseillé de se reporter au site officiel du [CNG](http://www.cng.sante.fr/).

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé.

#### Délai

Le CNG accuse réception de la demande dans le délai d’un mois à compter de sa réception.

Le silence gardé pendant un certain délai à compter de la réception du dossier complet vaut décision de rejet de la demande. Ce délai est porté à :

- quatre mois pour les demandes présentées par les ressortissants de l’UE ou de l’EEE titulaires d’un diplôme délivré dans l’un de ces États ;
- six mois pour les demandes présentées par les ressortissants d’États tiers titulaires d’un diplôme délivré par un État de l’UE ou de l’EEE ;
- un an pour les autres demandes.

Ce délai peut être prolongé de deux mois, par décision de l’autorité ministérielle notifiée au plus tard un mois avant l’expiration de celui-ci, en cas de difficulté sérieuse portant sur l’appréciation de l’expérience professionnelle du candidat.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation et l'expérience professionnelle du ressortissant et celles requises pour exercer en France, la CNG peut décider soit :

- de proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- d'imposer un stage d’adaptation et/ou une épreuve d’aptitude.

**L’épreuve d’aptitude** a pour objet de vérifier, par des épreuves écrites ou orales ou par des exercices pratiques, l’aptitude du demandeur à exercer la profession de pharmacien dans la spécialité concernée. Elle porte sur les matières qui ne sont pas couvertes par le ou les titres de formation du demandeur ou de son expérience professionnelle.

**Le stage d’adaptation** a pour objet de permettre aux intéressés d’acquérir les compétences nécessaires à l’exercice de la profession de pharmacien. Il est accompli sous la responsabilité d’un pharmacien et peut être accompagné d’une formation théorique complémentaire facultative. La durée du stage n’excède pas trois ans. Il peut être effectué à temps partiel.

*Pour aller plus loin* : arrêté du 25 février 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de médecin, chirurgien-dentiste, sage-femme et pharmacien.

### e. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).