﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP214" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Pharmacist specialised in clinical pathology" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="pharmacist-specialised-in-clinical-pathology" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/pharmacist-specialised-in-clinical-pathology.html" -->
<!-- var(last-update)="2020-04-15 17:21:59" -->
<!-- var(url-name)="pharmacist-specialised-in-clinical-pathology" -->
<!-- var(translation)="Auto" -->


Pharmacist specialised in clinical pathology
==========================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:59<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The pharmacist specializing in medical biology is a health professional whose activity consists of carrying out a medical examination for the prevention, diagnosis and management of patients with pathological disorders.

The specialist pharmacist (also known as a medical biologist) is required to carry out this examination in a medical biology laboratory.

*To go further* Article L. 6211-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice as a pharmacist specializing in medical biology, the professional must be:

- a French, Andorran national, or national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement;
- holder of a French state degree as a doctor of pharmacy or pharmacist as well as:- a specialty degree in medical biology,
  - a qualification in medical biology issued by the National Council of the Order of Pharmacists (see infra "2°. a. Diploma of Specialized Studies in Medical Biology (DESBM) ",
  - an individual exercise authorization issued by the Minister responsible for health, under the conditions of Article L. 4111-2 of the Public Health Code;
- registered with the order of pharmacists (see infra "5°). a. Registration on the pharmacists' list."

In addition, some professionals may have their area of specialization recognized (see "Recognition of a Specialization Area").

*To go further* Article L. 6213-1 of the Public Health Code.

**Recognition of a specialty area**

As a medical biologist, the professional who, as of May 30, 2013 (the effective date of[The ordinance](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021683301&dateTexte=20180409) 2010-49 of January 13, 2010 on medical biology):

- practiced medical biology in a health facility for at least two years over the past 10 years, or who began practising between January 13, 2008 and January 13, 2010 and practiced this activity for two years prior to January 13, 2012. However, the professional who has practiced medical biology in a field of specialization can only practice within this field;
- practiced as a veterinarian, who began a specialization in medical biology before January 13, 2010, was awarded his specialization by January 13, 2016;
- served as Director or Deputy Director of a National Centre for communicable Diseases and authorized to practice medical biology by the Minister for Health;
- was authorized by the Minister responsible for health, in an area of specialisation of his laboratory;
- has served for at least three years as a medical biologist in a medical biology structure or laboratory.

If necessary, the professional will have to apply for recognition of his area of specialization (see infra "5°. a. Request for recognition of a specialty area").

In addition, the director or deputy director of a national centre for the control of communicable diseases will also have to apply for an exercise authorization (see infra "5°. c. Application for authorization for practice for the director or deputy director of a national communicable disease centre").

*To go further* Articles L. 6213-2 and L. 6213-2-1 of the Public Health Code.

#### Training

To find out how to train for the title of pharmacist, it is advisable to refer to the listing " [Pharmacist](https://www.guichet-qualifications.fr/fr/professions-reglementees/pharmacien/) ».

**Diploma in Medical Biology (DESBM)**

To practice as a medical biologist, the pharmacist must hold the "Medical Biology" DES.

This training is available to pharmacy students who have passed the internship competition and allows the future specialist to acquire the knowledge and practice necessary to carry out his duties.

It consists of eight semesters, of which at least three are in an internship with university supervision and at least one in an internship place without university supervision.

###### Note that

As part of his professional project, the future medical biologist may apply for a specialized cross-sectional training (TSF) among the following:

- medical bioinformatics;
- genetics and bioclinical molecular medicine;
- bioclinical hematology;
- hygiene - infection prevention, resistance;
- medicine and reproductive biology - andrology;
- Applied nutrition
- Medical/therapeutic pharmacology;
- cell therapy/transfusion.

The formation of the DES is divided into three phases: the base phase, the deepening phase and the consolidation phase.

**The base phase**

Lasting four semesters, it allows the candidate to acquire the basic knowledge of the chosen specialty and to perform:

- three internships in a senior accredited location in medical biology and must cover the fields of molecular biochemistry, hematology and bacteriology-virology;
- an internship in a senior licensed location in medical biology and in a field different from the previous internship.

**The deepening phase**

Lasting two semesters, it allows the candidate to acquire the knowledge and skills necessary to practice the chosen specialty and to complete two internships in a place approved as a principal in medical biology and benefiting from accreditation. for this area.

**The consolidation phase**

Lasting two semesters, it allows the candidate to consolidate his knowledge and perform:

- a one-semester internship in a senior licensed location in medical biology;
- a one-semester internship in a place with functional accreditation in medical biology or a principally accredited in another specialty and secondary in medical biology.

*To go further* : Decree of 27 November 2017 amending the decree of 12 April 2017 relating to the organisation of the third cycle of medical studies and the decree of 21 April 2017 relating to the knowledge, skills and training models of the diplomas of study and establishing the list of these diplomas and the cross-sectional specialized options and training of the postgraduate medical studies.

#### Costs associated with qualification

Training leading to the diploma of pharmacist and diploma of specialization in medical biology, is paid and the cost varies depending on the institution chosen. For more information, it is advisable to check with the institutions concerned.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a legally established EU state or EEA who practises as a pharmacist specialising in medical biology may perform the same activity on a temporary and casual basis.

In order to do so, he will have to make a prior declaration and justify having the necessary language skills to practice in France.

To find out the terms of this advance declaration, it is advisable to refer to paragraph "5." "Qualification Recognition Procedures" of the Card " [Pharmacist](https://www.guichet-qualifications.fr/fr/professions-reglementees/pharmacien/) ».

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU state, legally established and practising as a pharmacist, may carry out the same activity on a permanent basis.

As such, the national can benefit from either:

- automatic graduation recognition scheme (see "Good to know: automatic diploma recognition");
- individual authorization to practise. If necessary, the profession will have to apply for authorisation (see infra "5°). d. If necessary, apply for an individual authorization to exercise").

###### Good to know

Automatic diploma recognition

May practise as a pharmacist, the national of an EU or EEA Member State, if he holds one of the following qualifications or diplomas:

- a pharmacist training certificate issued by a Member State and listed on the[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=529A28DB27A7E812110245674BD5769B.tplgfr34s_1?idArticle=LEGIARTI000027964612&cidTexte=LEGITEXT000006055521&dateTexte=20180416) of the decree of 13 February 2007 setting out the list of diplomas, certificates and other pharmacist titles issued by the Member States of the European Union, the States Parties to the Agreement on the European Economic Area and the Swiss Confederation referred to in Article L . 4221-4 (1) of the Public Health Code;
- a pharmacist training certificate issued by an EU or EEA Member State not on this list, accompanied by a training certificate;
- a pharmacist training certificate issued by an EU or EEA Member State sanctioning training that began before 13 February 2007 and accompanied by a certificate from that state, certifying that the national has been engaged in this activity for at least three consecutive years in the five years prior to the issuance of the certificate;
- a title issued by an EU or EEA state, sanctioning pharmacist training begun before one of the dates mentioned in the 13 February 2007 decree, and allowing legal practice as a pharmacist in that state. The national must justify having spent three years in the hospital function in France as associate attaché, associate practitioner, associate assistant or in the university function as associate clinic head of the universities or associate assistants of universities.

*To go further* Articles L. 4221-2, L. 4221-4 and L. 4221-5 of the Public Health Code; decree of 13 February 2007 setting out the list of diplomas, certificates and other pharmacist titles issued by the Member States of the European Union, the States Parties to the agreement on the European Economic Area and the Swiss Confederation covered by Article L. 4221-4 (1) of the Public Health Code.

*To go further* Article L. 6213-1 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Compliance with the Pharmacists' Code of Ethics

The pharmacist specializing in medical biology is subject to the professional rules applicable to pharmacists.

All provisions of the Pharmacists' Code of Ethics are codified in sections R. 4235-1 to R. 4235-77 of the Public Health Code.

As such, the professional must respect the principles of dignity, non-discrimination, professional secrecy or independence.

*To go further* Article L. 6213-7 of the Public Health Code.

### b. Cumulative activities

The pharmacist can only engage in any other activity if such a combination is compatible with the principles of independence and professional dignity imposed on him.

A pharmacist who serves an elective or administrative function is also prohibited from using it to increase his or her clientele.

*To go further* Articles R. 4235-4 and R. 4235-23 of the Public Health Code.

### c. Obligation for continuous professional development

Pharmacists specializing in medical biology must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of training (present, mixed or non-presential) in analysis, evaluation, and practice improvement and risk management. All training is recorded in a personal document containing training certificates.

*To go further* Article R. 4235-11 of the Public Health Code; Decree No. 2016-942 of 8 July 2016 relating to the organisation of the continuous professional development of health professionals.

4°. Insurance
---------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a pharmacist practising in a liberal capacity must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

### b. Pharmacists' Old Age Insurance Fund (CAVP)

Any pharmacist registered on the Board of the College of Pharmacists and practising in the liberal form (even part-time and even if he is also employed) has an obligation to join the CAVP.

The individual must report to the OPC within one month of the start of his Liberal activity.

*To go further* Article R. 643-1 of the Social Security Code; the site of the[Cavp](https://www.cavp.fr/).

### c. Health Insurance Reporting Obligation

Once on the Order's roster, the pharmacist practising in liberal form must declare his activity with the Primary Health Insurance Fund (CPAM).

**Terms**

Registration with the CPAM can be made online on the official website of Medicare.

**Supporting documents**

The registrant must provide a complete file including:

- Copying a valid piece of identification
- a professional bank identity statement (RIB).

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Application to register with the National Order of Pharmacists

Registration on the Order's board is mandatory to legally practice the activity of pharmacist in France.

The registration does not apply:

- pharmacists public health inspectors, inspectors of regional health agencies, inspectors of the National Agency for the Safety of Medicines and Health Products;
- pharmacists who are civil servants or the like-for-like ministry of health;
- pharmacists who are civil servants or the like-for-like ministry in charge of higher education, who are not, moreover, engaged in pharmaceutical activity;
- pharmacists belonging to the active framework of the army, sea and air health service.

**Note that **

Registration on the Order's board allows for the automatic and free issuance of the Health Professional Card (CPS). The CPS is an electronic business identity card. It is protected by a confidential code and contains, among other things, pharmacist identification data (identity, occupation, specialty). For more information, it is recommended to refer to the government website of the French Digital Health Agency.

**Competent authority**

Article R. 4222-1 provides that the application for registration is addressed, as the case may be, to the president of the regional council of the region in which the pharmacist wishes to practice, or to the president of the central council of the section.

**Supporting documents**

The applicant must submit, by any means, a complete application for registration including:

- two copies of the standardized questionnaire with a completed, dated and signed photo ID, available in the College's departmental councils or directly downloadable from the official website of the National Council of the College of Pharmacists;
- A photocopy of a valid ID or, if necessary, a certificate of nationality issued by a competent authority;
- If applicable, a photocopy of a valid EU citizen's family residence card, the valid long-term resident-EC card or the resident card with valid refugee status;
- If so, a photocopy of the valid European business card;
- a copy, accompanied if necessary by a translation by a certified translator, of the training courses to which are attached:- where the applicant is an EU or EEA national, the certificate or certificates provided (see above "2. a. National requirements"),
  - applicant is granted an individual exercise permit (see supra "2. v. EU and EEA nationals: for a permanent exercise"), copying this authorisation,
  - When the applicant presents a diploma issued in a foreign state whose validity is recognized on French territory, the copy of the titles to which that recognition may be subordinated;
- for nationals of a foreign state, a criminal record extract or an equivalent document less than three months old, issued by a competent authority of the State of origin. This part can be replaced, for EU or EEA nationals who require proof of morality or honourability for access to the medical activity, by a certificate, less than three months old, from the competent authority of the State. certifying that these moral or honourability conditions are met;
- a statement on the applicant's honour certifying that no proceedings that could give rise to a conviction or sanction that could affect the listing on the board are against him;
- a certificate of registration or registration issued by the authority with which the applicant was previously registered or registered or, failing that, a declaration of honour from the applicant certifying that he or she was never registered or registered or, failing that, a certificate of registration or registration in an EU or EEA state;
- all the evidence that the applicant has the language skills necessary to practice the profession;
- a resume.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Timeframe**

The regional or central council decides on the application for registration within three months of receipt of the full application file. If a response is not answered within this time frame, the application for registration is deemed rejected.

This period is increased to six months for nationals of third countries when an investigation is to be carried out outside metropolitan France. The person concerned is then notified.

If registration is granted, the decision taken by the board is notified to the person concerned by recommended letter, accompanied by his registration certificate.

The decision of the regional council is also subject to appeal, within 30 days, to the National Council of the College of Pharmacists. The decision itself can be appealed to the Council of State.

**Cost**

Registration on the College's board is free, but it creates an obligation to pay the mandatory ordinal dues, the amount of which is set annually and which must be paid in the first quarter of the current calendar year.

*To go further* Articles L. 4222-1 at L. 4222-8, and R. 4222-1 at R. 4222-4-3 of the Public Health Code.

### b. Application for specialization recognition

**Competent authority**

The professional must submit his application to the National Management Centre by recommended letter with notice of receipt at:

National Management Centre, Competition Department, Exercise Authorizations, Mobility and Professional Development, 21 B, rue Leblanc 75737 Paris Cedex 15

**Supporting documents**

His application must include the following documents:

- A reasoned letter of request outseating the area of specialization envisaged;
- A photocopy of a valid ID
- A detailed resume and any applicant's publications
- A copy of all his diplomas
- a certificate justifying that the national has practiced medical biology for at least two years in the last ten years and before 13 January 2012;
- for the deputy director of a national centre for communicable diseases, his phD in practice or university, or his engineering degree in connection with medical biology;
- for the pharmacist practising in a hospital or university, a document justifying that he has been practising well in such an institution for at least three years and mentioning his status.

**Time and procedure**

The National Management Centre acknowledges receipt of the application within one month of receipt and sends it to the National Medical Biology Commission. If the applicant is not answered within four months of receipt of the full file, the applicant is granted recognition.

*To go further* Article R. 6213-2 of the Public Health Code; order of February 14, 2017 setting out the composition of the file to be provided to the National Medical Biology Commission under Article L. 6213-12 of the Public Health Code and defining the areas of specialization mentioned in Article R. 6213-1 of the same Code.

### c. Application for authorization to practice for the director or deputy director of a national communicable disease centre

**Competent authority**

The professional must apply to the National Management Centre by recommended letter with notice of receipt.

**Supporting documents**

His application must include:

- A reasoned letter of request
- A photocopy of a valid ID
- A resume describing his professional background and, if necessary, all of his publications;
- A copy of his application for permission to perform the duties of director or assistant director of laboratory;
- a photocopy of all his diplomas, titles, or certifications in the field of medical biology.

**Time and procedure**

The National Management Centre acknowledges receipt of the application within one month of receipt. Failure to respond to the Minister of Health beyond four months is the reason for the rejection of the application for leave.

*To go further* : R. 6213-5 to R. 6213-7 of the Public Health Code, ordered February 14, 2017 above; V of Section 9 of Ordinance No. 2010-49 of January 13, 2010 relating to medical biology.

### d. If necessary, seek individual authorisation to exercise

If the national is not under the automatic recognition scheme, he must apply for a licence to practise.

**Competent authority**

The request is addressed in two copies, by letter recommended with request for notice of receipt to the unit responsible for the commissions of exercise authorization (CAE) of the National Management Centre (NMC).

He must add to his request:

- The[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4349F01DDAE283C55604F0C28FE06D3D.tplgfr23s_3?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180118) Request for permission to practice the profession;
- A photocopy of a valid ID
- A copy of the training title allowing the practice of the profession in the state of obtaining as well as, if necessary, a copy of the specialist training title;
- If necessary, a copy of the additional diplomas;
- any useful evidence justifying continuous training, experience and skills acquired during the professional exercise in an EU or EEA state, or in a third state (certificates of functions, activity report, operational assessment, etc. ) ;
- in the context of functions performed in a state other than France, a declaration by the competent authority of that State, less than one year old, attesting to the absence of sanctions against the applicant.

Depending on the applicant's situation, additional supporting documentation is required. For more information, please refer to the official website of the[Cng](http://www.cng.sante.fr/).

**What to know**

Supporting documents must be written in French or translated by a certified translator.

**Timeframe**

The NMC acknowledges receipt of the request within one month of receipt.

The silence kept for a certain period of time from the receipt of the full file is worth the decision to dismiss the application. This delay is increased to:

- four months for applications from EU or EEA nationals with a degree from one of these states;
- six months for applications from third-party nationals with a diploma from an EU or EEA state;
- one year for other applications.

This period may be extended by two months, by decision of the ministerial authority notified no later than one month before the expiry of the latter, in the event of a serious difficulty in assessing the candidate's professional experience.

**Good to know: compensation measures**

Where there are substantial differences between the training and work experience of the national and those required to practise in France, the NMC may decide either:

- Suggest that the applicant choose between an adjustment course or an aptitude test;
- to impose an adjustment course and/or an aptitude test.

**The aptitude test** is intended to verify, through written or oral tests or practical exercises, the applicant's ability to practise as a pharmacist in the specialty concerned. It deals with subjects that are not covered by the applicant's training or experience.

**The adaptation course** is intended to enable interested parties to acquire the skills necessary to practice the profession of pharmacist. It is carried out under the responsibility of a pharmacist and can be accompanied by optional additional theoretical training. The duration of the internship does not exceed three years. It can be done part-time.

*To go further* : decree of 25 February 2010 setting out the composition of the file to be provided to the competent authorisation commissions for the examination of applications submitted for the exercise in France of the professions of doctor, dental surgeon, midwife and Pharmacist.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

