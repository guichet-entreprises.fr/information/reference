﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP153" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Nurse" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="nurse" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/nurse.html" -->
<!-- var(last-update)="2020-04-15 17:21:37" -->
<!-- var(url-name)="nurse" -->
<!-- var(translation)="Pro" -->


# Nurse

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:37<!-- end-var -->


<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->



## 1°. Definition of the profession

A nurse is a professional who determines, organises, provides and assesses nursing care on prescription or on medical advice. He or she helps to collect clinical and epidemiological data and is involved in prevention, screening, training and health education. A nurse is able to administer some vaccinations without a prescription and can renew certain prescriptions.

For more information, please refer to Article R. 4311-1 of the French Public Health Code.

## 2°. Professional qualifications

### a. National requirements

#### National legislation

The profession of nurse is restricted to holders of a French state diploma (*diplôme d’État*) in nursing or a qualification listed in the Order of 13 November 1964.

For more information, please refer to Article L. 4311-2 *et seq.* of the French Public Health Code.

#### Training

To obtain the French state diploma (*diplôme d’État*) in nursing, a student must complete three years of training in a nursing school (*institut de formation en soins infirmiers*, IFSI).

Admission to a nursing school (IFSI) is by competitive examination and is open only to candidates aged at least seventeen who have passed their baccalaureate.

In addition, there are certain health requirements attaching to IFSI entrance:

* The candidate must not be suffering from a physical or psychological condition incompatible with the nursing profession
* The candidate must comply with statutory vaccination requirements. He or she must also have undergone a tuberculin test and tested positive or, failing this, have had two failed attempts at BCG vaccination.

For more information, please refer to Article L. 4311-7 of the French Public Health Code.

#### Costs of qualifying as a nurse

The cost of IFSI training varies according to the nature of the school and the extent to which fees may be covered by the regional councils in which the IFSIs are established.

For more details, candidates are advised to contact the IFSI in question.

### b. EU citizens: temporary and occasional practice (freedom to provide services)

Citizens of a Member State of the European Union (EU) or the European Economic Area (EEA) who are lawfully established and practising as nurses responsible for general care in one of these Member States may practise the same profession in France on a temporary and occasional basis provided that they have previously sent a declaration of practice to the prefect of the *département* in which the service will be performed.

In cases where the evidence of formal qualifications is not recognised in France, the qualifications of a citizen of an EU or EEA Member State must be checked prior to the first provision of services. Where there is a substantial difference between the professional qualifications of the service provider and the training required in France, the provider must provide proof that he or she has acquired the knowledge or competence lacking, in particular by means of compensation measures (see below: “Useful tip: Compensation measures”).

In every instance, an EU citizen wishing to practise in France on a temporary or occasional basis must have the knowledge of languages necessary for practising the profession and understand the system of weights and measures used in France.

For more information, please refer to Article L. 4311-22 of the French Public Health Code.

### c. EU citizens: permanent practice (freedom of establishment)

A citizen of an EU or EEA Member State wishing to practise permanently in France will come under one of two separate systems. However, in both cases the citizen must have the knowledge of languages necessary for practising the profession in France and understand the system of weights and measures used in that country.

#### Automatic recognition of a diploma

Holders of a formal qualification as a nurse responsible for general care issued by an EU or EEA Member State listed in the Order of 10 June 2004 are entitled to automatic recognition of their diploma. In addition, citizens who do not hold one of the qualifications listed in Appendix I of the decree of 10 June 2004 are entitled to automatic recognition provided that they have evidence of formal training, compliance with which is certified by an attestation from the State where it was obtained. For the full list of such qualifications, please refer to the Order of 10 June 2004.

In every instance, the persons concerned do not need to apply for authorisation to practise. On the other hand, they must register with the *département* board of the French Nursing Council (*Ordre national des infirmiers*) responsible for the area where they are intending to practise and must apply for registration in the ADELI register (see below: “Applying for registration of the diploma or authorisation to practise (ADELI No.)”).

#### Individual authorisation to practise

To practise permanently in France, citizens not entitled to automatic recognition must obtain an individual authorisation to practise issued by the regional prefect. To apply for it, they must send a complete application file to the Regional Directorate for Youth, Sport and Social Cohesion (*Direction régionale de la jeunesse, des sports et de la cohésion sociale*, DRJSCS) of the area where they intend to practise.

If there is a substantial difference between the professional qualifications obtained and those required in France, the applicant may be required to accept compensation measures (see below: “Useful tip: Compensation measures”), set out in the Order of 24 March 2010.

For more information, please refer to Articles L. 4311-4 *et seq.* and R. 4311-34 *et seq.* of the French Public Health Code, the Order of 10 June 2004 establishing the list of diplomas, certificates and other qualifications for nurses responsible for general care issued by EU Member States or parties to the EEA Agreement referred to in Article L. 4311-3 of the French Public Health Code, the Order of 20 January 2010 on prior declaration of provision of services for practising the professions of nurse, etc. and the Order of 24 March 2010 laying down the organisational arrangements for the aptitude test and adaptation period for practice of the profession of nurse in France by citizens of EU Member States or States party to the EEA Agreement.

## 3°. Good character, code of conduct, ethics

All the general duties binding on French nurses apply to other nurses practising in France.

Nurses are thus bound by the principles of dignity, non-discrimination and independence. They are subject to the profession’s conditions of practice, the rules of professional conduct applying in France and the relevant disciplinary jurisdiction.

For more information, please refer to Articles R. 4312-1 *et seq.* of the French Public Health Code.

### a. Multiple activities

Nurses may engage in another activity if this is compatible with the dignity and quality required by professional practice and complies with the rules in force.

For more information, please refer to Article R. 4312-20 of the French Public Health Code.

### b. Conditions of good repute

To be able to practise, a nurse must not be:

* prohibited temporarily or permanently from practising his or her profession in France or abroad
* suspended owing to the serious risk to which practice of his or her profession would expose patients.

For more information, please refer to Articles L. 4311-16 and L. 4311-26 of the French Public Health Code.

### c. Duty of ongoing professional development

Nurses must take part in a continuing professional development programme annually. This programme is designed to maintain their knowledge and skills, bring the latter up to date and improve their professional practices.

Consequently, healthcare professionals (whether employed or self-employed) must provide proof that they are undertaking professional development. The programme takes the form of training courses (classroom, non-classroom and mixed) to study, assess and improve practices and risk management. All training courses completed are recorded in a personal document certifying this training.

For more information, please refer to Articles L. 4021-1 and R. 4382-1 of the French Public Health Code.

### d. Physical aptitude

The nurse must not present an infirmity or pathology that would make practice of the profession unsafe.

For more information, please refer to Article L. 4311-18 of the French Public Health Code.

## 4°. Social legislation and insurance

### a. Obligation to take out professional liability insurance

As a healthcare professional, a nurse practising in a private capacity must take out professional liability insurance. Conversely, for physiotherapists who are employees such insurance is optional. In this case, it is up to the employer to take out insurance for its employees to cover actions carried out in the course of their professional duties.

For more information, please refer to Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the Medical Auxiliaries’ Pension Fund (*Caisse autonome de retraite et de prévoyance des auxiliaires médicaux*, CARPIMKO)

Nurses practising in a private capacity, even if part-time and employed elsewhere, are required to join CARPIMKO (a pension fund for nurses, physiotherapists, podiatrists and speech therapists).

**Supporting documents**

The applicant must send CARPIMKO the following without delay:

* The membership questionnaire [downloadable from the CARPIMKO website](http://www.carpimko.com/document/pdf/affiliation_declaration.pdf) or a letter indicating the starting date of private practice
* A photocopy of his/her *diplôme d’État*
* A photocopy of the registration number (ADELI No.) of the diploma issued by the regional health agency (ARS) or a photocopy of the reverse side of the diploma.

## 5°. Process and procedures for recognition of professional qualifications

### a. Making a prior declaration of practice for EU citizens intending to practise on a temporary and occasional basis (freedom to provide services)

**Competent authority**

The prior declaration of practice must be sent to the national board of the French Nursing Council (*Conseil national de l’ordre des infirmiers*, CNOI) before the first provision of services.

**Renewal of prior declaration**

The prior declaration must be renewed once a year if the service provider intends to provide services in France during that year.

**Supporting documents**

* The completed form for prior declaration of practice, a template for which is provided in the appendix to the Order of 20 January 2010
* A certificate confirming that professional liability insurance has been taken out for actions performed on French territory
* A photocopy of an ID document providing proof of the applicant’s nationality
* A photocopy of formal qualifications
* A certificate from the competent authority of the EU or EEA Member State in which the applicant is established, attesting to the fact that he/she is lawfully established in that country and not subject to any ban, even temporary, on practising.

**Useful tip**

Supporting documents other than the ID document must be translated into French by a sworn translator approved by the French courts or authorised to work with the judicial or administrative authorities of an EU or EEA Member State.

**Timeframe**

Within one month of receiving the prior declaration the CNOI will inform the service provider of the outcome of the examination of his/her qualifications

Within this same timeframe the CNOI may request further information. In this case, the initial one-month timeframe is extended by one month.

At the end of this procedure the CNOI will inform the service provider that, as the case may be, he/she:

* can begin to provide services

* cannot begin to provide services

* must provide proof that he/she has acquired the knowledge or competence lacking, if his/her training reveals significant differences from the training required in France (in particular by means of compensation measures).

Should the CNOI fail to reply within these timeframes, the services may be provided.

**Issue of receipt**

The CNOI will register the service provider in a specific list and then send him/her a registration receipt. The service provider must then notify the competent national health insurance body of his/her provision of services and send it the registration number.

**Useful tip**

A service provider on a temporary and occasional basis is not required to register with the ADELI register or pay the mandatory membership subscription for the French Nursing Council.

**Cost**

Free of charge.

For more information, please refer to Article L. 4311-22 *et seq.* and Article R. 4311-38 *et seq.* of the French Public Health Code and the above-mentioned Order of 20 January 2010.

### b. Formalities for EU citizens intending to practise on a permanent basis (freedom of establishment)

#### Applying for authorisation to practise, where necessary

Citizens not entitled to automatic recognition of their diplomas must apply for authorisation to practise.

**Competent authority**

The application for authorisation to practise must be sent to the prefect of the region where the applicant wishes to become established. The prefect will issue the authorisation to practise, where necessary after consulting the nurses’ advisory committee.

**Procedure**

The application for authorisation to practise must be sent to the Regional Directorate for Youth, Sport and Social Cohesion (*Direction régionale de la jeunesse, des sports et de la cohésion sociale*, DRJSCS) of the area where the applicant intends to practise. The regional prefect will acknowledge receipt of the application within one month.

**Supporting documents**

The application for authorisation to practise must be in duplicate and contain:

* A completed application form for authorisation to practise the profession. This is available online from the website of the DRJSCS concerned
* A photocopy of a valid ID document
* A copy of the formal qualifications allowing the profession to be practised in the country where they were obtained
* A copy of additional qualifications, if applicable
* Any relevant documents providing proof of in-service training, experience and skills acquired during professional practice in an EU or EEA Member State or a third country
* A declaration, no more than a year old, from the competent authority of the EU or EEA Member State certifying the absence of any sanctions against the applicant
* A copy of certificates from the authorities having issued the formal qualifications, specifying the level of training and detailing, year by year, the courses taken and their length in hours, as well as the content and duration of validated traineeships
* For applicants having practised in an EU or EEA Member State that does not regulate either access to or training for the profession: any documents proving that he/she has practised in that country for at least two years full-time during the previous ten years
* For applicants holding formal qualifications issued by a third country and recognised in an EU or EEA Member State other than France: recognition of these formal qualifications and, where applicable, of the professional qualification of specialist, provided by the authorities of the EU or EEA Member State having recognised these qualifications.

**Useful tip**

These supporting documents must be translated into French by a sworn translator where necessary.

**Please note**

To obtain the authorisation to practise, the applicant may have to accept compensation measures (aptitude test or adaptation period) if his or her formal qualifications and professional experience prove to be substantially different from what is required to practise the profession in France.

If compensation measures are deemed necessary, the regional prefect responsible for issuing the authorisation to practise must notify the applicant that he/she has a period of two months in which to choose between the aptitude test and the adaptation period (see below: “Useful tip: Compensation measures”).

**End of the procedure**

Failure by the regional prefect to reply within four months of receipt of the complete application signifies a decision to reject the application.

**Remedies**

An applicant may challenge a (formal or implied) decision to reject his/her application for authorisation to practise. To this end, he/she may, within two months of notification of the rejection decision, lodge one of the following:

* An application to the regional prefect to reconsider the decision
* An appeal to the Minister for Health
* An appeal to the administrative court with territorial jurisdiction.

**Cost**

Free of charge.

**Useful tip: Compensation measures**

**Aptitude test**

The DRJSCS organising the aptitude tests must send the applicant notification by registered letter with acknowledgement of receipt at least one month before the start of the tests. This notification must include the date, time and place of the test. The aptitude test may take the form of written or oral questions marked out of twenty and covering each of the subject areas not originally taught or not learnt through professional experience.

To pass the test, an applicant must have an average mark of at least 10 out of 20 with no mark below 8. The results of the test will be notified to the applicant by the regional prefect.

If successful, the applicant will be authorised to practise his/her profession by the regional prefect.

**Adaptation period**

This will be spent in a public or private healthcare establishment approved by the regional health agency (ARS).  The applicant will be placed under the educational supervision of a qualified professional having practised the profession for at least three years, who will write the assessment report.

The adaptation period, which may include supplementary theoretical training, is validated by the head of the host institution on the proposal of the qualified professional assessing the applicant.

The results of the adaptation period will be notified to the applicant by the regional prefect.

Where necessary, a decision on authorisation to practise is then taken after again consulting the committee referred to in Article L. 4311-4 of the French Public Health Code.

For more information, please refer to Article L. 4311-4 and Article R. 4311-34 *et seq.* of the French Public Health Code and to the above-mentioned Order of 20 January 2010.

#### Applying for registration with the French Nursing Council (*Ordre national des infirmiers*)

Registration with the French Nursing Council is mandatory  in order to be able to practise lawfully as a nurse on French territory.

**Competent authority**

The registration application should be sent to the *département* or *interdépartement* board of the French Nursing Council (CDOI or CIOI) of the area where the applicant wishes to practise, preferably by registered letter with acknowledgement of receipt.

**Procedure**

The Chair of the CDOI or CIOI will acknowledge receipt of the complete application within one month.  The CDOI or CIOI has three months in which to consider and rule on the application. The decision will be notified to the applicant by registered letter with acknowledgement of receipt no more than one week after it has been taken by the CDOI or CIOI.

**Supporting documents**

* The application form for registration with the French Nursing Council, which can be obtained from the relevant CDOI or CIOI or downloaded from the [French Nursing Council website](http://www.ordre-infirmiers.fr/lordre-et-les-conseils-ordinaux/inscription-a-lordre.html)
* A photocopy of a valid ID document, together with, where applicable, a nationality certificate issued by the competent authority
* A copy of the nursing diploma (translated by a sworn translator where necessary). This copy must provided together with:
  * either a certificate from the State having issued the diploma stating that the training complies with EU requirements,
  * or a certificate stating that over the ten years preceding the application the applicant has practised as a nurse for two years full-time, including full responsibility for planning, organisation and administration of nursing care delivered to patients,
* Proof of knowledge of the French language and the system of weights and measures used on French territory
* Proof of good character: either a criminal record certificate, no more than three months old, issued by a competent authority of the home country, or a certificate of good character or repute, no more than three months old, issued by the board of the nursing council or competent authority of an EU Member State
* A self-declaration by the applicant stating that no proceedings are pending that might lead to sentencing or a sanction that could affect registration with the French Nursing Council
* A certificate of deregistration or disenrolment issued by the authority with which the applicant was previously registered or enrolled. Failing this, the applicant must produce a self-declaration stating that he/she has never been registered or enrolled, or, failing this, a certificate of deregistration or disenrolment in an EU or EEA Member State
* A curriculum vitae.

Other supporting documents may be required depending on the CDOI or CIOI. For more details, applicants are advised to contact the board in question.

Applicants wishing to practise in the form of a company must attach the following, in addition to the documents mentioned above:

* A copy of the articles of association and, if they exist, a copy of the business’s rules and regulations and a copy or duplicate of the memorandum of association
* A certificate of registration with the French Nursing Council for each of the partners, or, if they are not yet registered, proof of the registration application
* If the business is carried on in the form of a professional practice (*société d’exercice libéral*, SEL):
  * A certificate from the registry of the commercial court or court of first instance proving that the company’s application for registration in the commercial register has been filed with that registry,
  * A certificate from the partners specifying the nature and evaluation of contributions, the registered capital, the number, minimum amount and distribution of partnership or outside shares, and a solemn declaration that the contributions are paid in full or in part.

**Remedies**

An appeal must be lodged with the regional or interregional board with jurisdiction for the area of the CDOI or CIOI that made the decision and within 30 days of notification of this decision.

**Cost**

Registration with the French Nursing Council is free of charge but entails a mandatory membership subscription whose amount is determined on a yearly basis by the Council’s national board.

For more information, please refer to Articles L. 4311-15, L. 4311-16, L. 4312-7 and R. 4112-1 *et seq.*, implemented by Articles R. 4311 52, R. 4113-4 and R. 4113-28 of the French Public Health Code.

#### Applying to register the company with the French Nursing Council if practising as an SEL or professional partnership (*Société Civile Professionnelle*, SCP)

Applicants wishing to practise in the form of an SCP or SEL must register their company in the French Nursing Council’s register for the location of its registered office.

**Competent authority**

The registration application should be sent to the CDOI or CIOI of the area where the applicants wish to practise, preferably by registered letter with acknowledgement of receipt.

**Procedure**

The Chair of the CDOI or CIOI will acknowledge receipt of the complete application within one month.  The CDOI or CIOI has three months in which to consider and rule on the application. The decision will be notified to the applicant by registered letter with acknowledgement of receipt no more than one week after it has been taken by the CDOI or CIOI.

**Supporting documents**

* A copy of the articles of association and, if they exist, a copy of the business’s rules and regulations and a copy or duplicate of the memorandum of association
* A certificate of registration with the French Nursing Council for each of the partners, or, if they are not yet registered, proof of the registration application
* If the business is carried on in the form of a SEL:
  * A certificate from the registry of the commercial court or court of first instance proving that the company’s application for registration in the commercial register has been filed with that registry,
  * A certificate from the partners specifying the nature and evaluation of contributions, the registered capital, the number, minimum amount and distribution of partnership or outside shares, and a solemn declaration that the contributions are paid in full or in part.

**Remedies**

An appeal must be lodged with the regional or interregional board with jurisdiction for the area of the CDOI or CIOI that made the decision and within 30 days of notification of this decision.

**Cost**

Registration with the French Nursing Council is free of charge but entails a mandatory membership subscription whose amount is determined on a yearly basis by the Council’s national board.

#### Applying for registration of the diploma or authorisation to practise (ADELI No.)

Nurses are required to register their evidence of formal qualifications or the authorisation needed to practise the profession.

**Competent authority**

The diploma or authorisation to practise must be registered in the ADELI (*Automatisation DEs LIstes*) automated register with the ARS of the place of where the profession is practised.

**Timeframe**

The registration application must be submitted in the month following the start of practice, whatever form the practice takes (employed, self-employed, both). The receipt issued by the ARS will include the ADELI number. The ARS will then send the applicant an application form for a health professionals card (*carte de professionnel de santé*).

**Supporting documents**

* The original diploma (translated into French by a sworn translator where necessary)
* An ID document
* Proof of registration with the French Nursing Council in the *département* where the profession is being practised
* CERFA Form 10906\*06 completed, dated and signed.

This list may vary from region to region. For more details, applicants are advised to contact the ARS in question.

**Cost**

Free of charge.

For more information, please refer to Article L. 4311-15 of the French Public Health Code.

#### Applying for membership of the health insurance scheme

Registration with the health insurance scheme will allow the latter to cover the care provided. It will also result in provision of a health professionals card (*carte de professionnel de santé*, CPS) and social security treatment forms in the nurse’s name.

**Competent authority** 

The application is made through the health professionals department (*service des relations avec les professionnels de santé*) of the health insurance office (*Caisse Primaire d’Assurance Maladie*, CPAM) for the area where the applicant is practising.

**Supporting documents**:

* A copy of the diploma
* The ADELI entry
* The application form for a health professionals card (CPS)
* Bank account details
* Health insurance card (*carte vitale*) and social security certificate (*attestation de carte vitale*).

Other supporting documents may be required depending on the CPAM. For more details, applicants are advised to contact the CPAM in question.

### c. European Professional Card (EPC)

The European Professional Card is an electronic procedure to have professional qualifications recognised in another EU Member State.

The EPC procedure can be used if the citizen wishes to practise as a nurse in another EU country:

* either temporarily and occasionally
* or permanently.

The EPC is valid:

* indefinitely for long-term establishment
* normally eighteen months for temporary provision of services (or twelve months for professions affecting public health and safety).

To apply for an EPC the applicant must:

* create a user account with the European Commission Authentication Service (ECAS)
* complete his/her EPC profile (personal information, contact details, etc.)
* create an EPC application by uploading scanned copies of supporting documents.

**Cost**

For each EPC application both home country and host country authorities may charge fees, the amount of which will depend on the circumstances.

**Timeframe**

*For an EPC application for temporary and occasional practice*

Within one week the home country authority will acknowledge receipt of the EPC application, state whether any documents are missing and provide information about any fees. The host country authority then checks the application.

* If host country checks are not needed, the home country authority reviews the application and takes a final decision within three weeks.
* If host country checks are needed, the home country authority has one month to review the application and forward it to the host country. The host country then takes a final decision within three months.

*For an EPC application for permanent practice*

Within one week the home country authority will acknowledge receipt of the EPC application, state whether any documents are missing and provide information about any fees. The home country then has one month to review the application and forward it to the host country. The latter will take a final decision within three months.

If the host country authorities find that the applicant’s education, training or professional experience does not meet the standards required in the host country, they may ask the applicant to take an aptitude test or complete an adaptation period.

Outcomes of EPC application:

* If the EPC application is granted, it is then possible to generate an EPC certificate online.
* If the host country authorities fail to take a decision within the time allowed, the applicant’s qualifications are implicitly recognised and an EPC is issued. The applicant can then generate an EPC certificate from his or her online account.
* If the EPC application is rejected, reasons must be given and information provided on how to challenge this decision.

### d. Remedies

#### French support centre

The ENIC-NARIC is the French centre for information on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided nationally by each Member State of the European Union or State party to the EEA Agreement. It aims to find solutions to problems between an EU citizen and the government of another of these countries. SOLVIT is heavily involved in the recognition of professional qualifications.

**Conditions**

An applicant may use SOLVIT’s services only if he/she is able to prove:

* that the public authorities of an EU Member State have breached their EU rights as a citizen or business of another EU Member State
* that they have not yet taken the case to court (administrative appeals do not count).

**Procedure**

The citizen must complete an [online complaints form]( http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=en&origin=solvit-web).

Once the form has been submitted, SOLVIT contacts him/her within one week to ask for further information, if necessary, and to check whether the problem falls within its remit.

**Supporting documents**

To refer a problem to SOLVIT, the citizen must provide:

* his/her full contact details
* a detailed description of the problem
* all relevant evidence(e.g. correspondence and decisions received from the administrative authority in question)

**Timeframe**

SOLVIT aims to find solutions within 10 weeks starting on the day the case is taken on by the SOLVIT centre in the country where the problem has occurred.

**Cost**

Free of charge.

**Outcome of procedure**

By the end of ten weeks, SOLVIT will put forward a solution:

* If the solution resolves the problem concerning the application of EU law, it is accepted and the case is closed
* If the problem is not settled, the case is closed as unresolved and referred to the European Commission

**Further information**

SOLVIT in France: Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).