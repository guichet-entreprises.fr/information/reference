﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP168" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Manipulateur d'électroradiologie médicale" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="manipulateur-d-electroradiologie" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/manipulateur-d-electroradiologie-medicale.html" -->
<!-- var(last-update)="2020-04-15 17:21:38" -->
<!-- var(url-name)="manipulateur-d-electroradiologie-medicale" -->
<!-- var(translation)="None" -->

# Manipulateur d'électroradiologie médicale

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:38<!-- end-var -->

## 1°. Définition de l'activité

Le manipulateur d'électroradiologie médicale (« manipulateur ERM ») est un professionnel dont les missions principales sont :

- de réaliser des actes d'électroradiologie médicale permettant d'établir des diagnostics ;
- de préparer le matériel de ponction, de cathétérisme, d'injection, d'explosion et du matériel médico-chirurgical ;
- de réaliser des traitements utilisant des rayonnements ionisants ou non.

Le manipulateur ERM participe également à :

- la transmission écrite des informations relatives au déroulement des examens et traitements ;
- la surveillance clinique du patient au cours de la mission ;
- l'exécution des soins nécessités par l'acte réalisé.

**À savoir**

Le manipulateur ERM n'est pas compétent pour interpréter les images et sortir un diagnostic au patient.

*Pour aller plus loin* : articles L. 4351-1, R. 4351-1 à R. 4351-3 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'accès à la profession de manipulateur ERM est réservé aux titulaires du :

- diplôme d’État français de manipulateur d'électroradiologie médicale (DEMEM) délivré par le ministère chargé des affaires sociales et de la santé ;
- diplôme de technicien supérieur en imagerie médicale et radiologie thérapeutique (DTS IMRT) délivré par le ministère chargé de l'Enseignement supérieur et de la Recherche.

*Pour aller plus loin* : article L. 4351-3 du Code de la santé publique.

#### Formation

L'accès à ces deux formations se fait directement après le baccalauréat : par voie de concours pour le DE de manipulateur d'électroradiologie médicale et par l'étude du dossier scolaire avec entretien devant un jury pour le DTS IMRT.

Les deux formations durent trois ans et confèrent le grade de licence à l'étudiant.

Pour le DEMEM, l'enseignement est composé de deux parties :

- l'une théorique de 2 100 heures ;
- l'autre pratique de 2 100 heures.

Pour plus d'informations, il est conseillé de se rapprocher des instituts de formation ([IFMEM](https://www.sup-admission.com/sante/paramedical/manip/radio-4/#tabs2)) qui proposent la formation menant au DEMEM, et des [écoles](https://www.sup-admission.com/sante/paramedical/manip/radio-4/#tabs3) menant à l'obtention du DTS IMRT.

*Pour aller plus loin* : arrêté du 14 juin 2012 relatif au diplôme d’État de manipulateur d'électroradiologie médicale.

#### Coûts associés à la qualification

La formation menant à l'obtention de l'un de ces titres est payante. Pour plus d'informations, il est conseillé de se renseigner auprès des établissements la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’UE ou de l’EEE, exerçant légalement l’activité de manipulateur ERM dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet de région dans laquelle il souhaite faire la prestation (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : article L. 4351-8 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité de manipulateur ERM dans cet État, peut exercer la même activité en France de manière permanente s’il :

- est titulaire d'un titre de formation délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession ;
- est titulaire d’un diplôme, titre ou certificat acquis dans un État tiers mais reconnu et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité de manipulateur ERM dans l’État qui a admis l’équivalence.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une autorisation individuelle d'exercice auprès du préfet de région dans laquelle il souhaite exercer sa profession (cf. infra « 5°. b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, le préfet pourra le soumettre à des mesures de compensation.

*Pour aller plus loin*: article L. 4351-4 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Règles de bonne pratique

La société européenne de radiothérapie et d'oncologie (ESTRO) a rédigé un Code d'éthique et de conduite visant à guider le manipulateur ERM dans la pratique de sa profession.

Ce code précise notamment que le manipulateur ERM doit :

- assurer la sécurité et la qualité des traitements de radiothérapie ;
- respecter et protéger l’intérêt supérieur du patient ;
- respecter le secret médical ;
- s'assurer du consentement éclairé du patient.

### b. Sanctions pénales

L'exercice illégal de la profession de manipulateur ERM et l'usage sans le titre de formation sont des délits punissables de peines d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : articles L. 4353-1 et L. 4353-2 du Code de la santé publique.

## 4°. Assurance

En qualité de professionnel de santé, le manipulateur ERM exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet de région est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de la profession dans l’État d'obtention ;
- une attestation, datant de moins de trois mois, de l'autorité compétente de l’État de l'UE ou de l'EEE, certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ; 
- toutes pièces justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État de l'UE ou de l'EEE autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l’État ayant reconnu ce titre,
  - toutes pièces justifiant que le ressortissant a exercé la profession dans cet État pendant trois ans ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée ;
- une attestation de responsabilité civile professionnelle.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

À réception du dossier, le préfet de région disposera d'un délai d'un mois pour se prononcer sur la demande et informera le ressortissant :

- qu'il peut débuter la prestation. Dès lors, le préfet enregistrera le demandeur au répertoire Adeli ;
- qu'il sera soumis à une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France ;
- qu'il ne pourra pas débuter la prestation ;
- de toute difficulté pouvant retarder sa décision. Dans ce dernier cas, le préfet pourra rendre sa décision dans les deux mois suivant la résolution de cette difficulté, et au plus tard dans les trois mois de sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaudra acceptation de la demande de déclaration.

**À noter**

La déclaration est renouvelable tous les ans ou à chaque changement de situation du demandeur.

*Pour aller plus loin* : articles R. 4351-25 et R. 4331-12 à R. 4331-15 du Code de la santé publique.

### b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

#### Autorité compétente

L'autorisation d'exercice est délivrée par le préfet de région, après avis de la commission des manipulateurs d'électroradiologie médicale.

#### Pièces justificatives

La demande d'autorisation s'effectue par le dépôt d'un dossier comprenant l'ensemble des documents suivants :

- le [formulaire de demande d'autorisation d'exercice](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1F8B7371FFFC32398488D9490CC59F7F.tplgfr24s_3?idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180213) ;
- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation ;
- le cas échéant, une copie des diplômes complémentaires ;
- toute pièce justifiant des formations continues, des expériences et des compétences acquises dans l’État de l'UE ou de l'EEE ;
- une déclaration de l'autorité compétente de l'État de l'UE ou de l'EEE justifiant l'absence de sanction à l'encontre du ressortissant ;
- une copie des attestations des autorités spécifiant le niveau de formation, le détail et le volume horaire des enseignements suivis ainsi que le contenu et la durée des stages validés ;
- tout document justifiant que le ressortissant a exercé l'activité de manipulateur ERM pendant un an au cours des dix dernières années, dans un État de l'UE ou de l'EEE, lorsque ni l'accès ni l'exercice ne sont réglementés dans cet État.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet accuse réception du dossier dans le délai d'un mois et se prononcera après avoir eu l'avis de la commission des manipulateurs ERM. Cette dernière est chargée d'examiner les connaissances et les compétences du ressortissant acquises lors de sa formation ou au cours de son expérience professionnelle. Elle pourra soumettre le ressortissant à une mesure de compensation.

Le silence gardé du préfet de région dans un délai de quatre mois vaut décision de rejet de la demande d'autorisation.

#### Bon à savoir : mesures de compensation

Si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession de manipulateur ERM et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente pourra soit :

- proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve d’aptitude ;
- imposer un stage d’adaptation et/ou une épreuve d’aptitude.

*Pour aller plus loin* : articles R. 4351-22 à R. 4351-24 du Code de la santé publique ; arrêté du 20 janvier 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de conseiller en génétique, infirmier, masseur-kinésithérapeute, pédicure-podologue, ergothérapeute, manipulateur en électroradiologie médicale et manipulateur ERM.

### c. Enregistrement auprès du répertoire Adeli

Le ressortissant souhaitant exercer la profession de manipulateur ERM en France est tenu de faire enregistrer son autorisation d'exercer sur le répertoire Adeli (« Automatisation Des Listes »).

#### Autorité compétente

L’enregistrement au répertoire Adeli se fait auprès de l’agence régionale de santé (ARS) du lieu d’exercice.

#### Délai

La demande d’enregistrement est présentée dans le mois suivant la prise de fonction du ressortissant, quel que soit le mode d’exercice (libéral, salarié, mixte).

#### Pièces justificatives

À l'appui de sa demande d'enregistrement, le manipulateur ERM doit fournir un dossier comportant :

- le diplôme original ou titre attestant de la formation de manipulateur ERM délivré par l'État de l'UE ou de l'EEE (traduit en français par un traducteur agréé, le cas échéant) ;
- une pièce d’identité ;
- le formulaire Cerfa n° 10906-06 complété, daté et signé.

#### Issue de la procédure

Le numéro Adeli du ressortissant sera directement mentionné sur le récépissé du dossier, délivré par l'ARS.

#### Coût

Gratuit.

*Pour aller plus loin* : article L. 4351-10 du Code de la santé publique.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).