﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP168" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Radiographer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="radiographer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/radiographer.html" -->
<!-- var(last-update)="2020-04-15 17:21:38" -->
<!-- var(url-name)="radiographer" -->
<!-- var(translation)="Auto" -->


Radiographer
====================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:38<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Medical Electroradiology Manipulator ("ERM Manipulator") is a professional whose main missions are:

- perform medical electroradiology to diagnose;
- prepare puncture, catheterization, injection, explosion and medical-surgical equipment;
- to perform treatments using ionizing or non-ionizing radiation.

The ERM manipulator also participates in:

- Written transmission of information about the conduct of examinations and treatments;
- Clinical monitoring of the patient during the mission;
- the execution of the care required by the act carried out.

**What to know**

The ERM manipulator is not competent to interpret the images and release a diagnosis to the patient.

*For further information*: Articles L. 4351-1, R. 4351-1 to R. 4351-3 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Access to the ERM manipulator profession is restricted to holders of:

- French state diploma of manipulator of medical electroradiology (DEMEM) issued by the Ministry for Social Affairs and Health;
- Graduate Technician diploma in Medical Imaging and Therapeutic Radiology (DTS IMRT) issued by the Ministry of Higher Education and Research.

*For further information*: Article L. 4351-3 of the Public Health Code.

#### Training

Access to these two courses is done directly after the baccalaureate: by way of competition for the DE of manipulator of medical electroradiology and by the study of the school file with interview before a jury for the DTS IMRT.

Both courses last three years and confer the degree of bachelor's degree on the student.

For DEMEM, teaching is made up of two parts:

- one theoretical of 2,100 hours;
- the other 2,100-hour practice.

For more information, it is advisable to get closer to the training institutes ([IFMEM](https://www.sup-admission.com/sante/paramedical/manip/radio-4/#tabs2)) that offer the training leading up to DEMEM, and[Schools](https://www.sup-admission.com/sante/paramedical/manip/radio-4/#tabs3) leading to the IMRT SDR.

*For further information*: order of 14 June 2012 relating to the state diploma of manipulator of medical electroradiology.

#### Costs associated with qualification

Training leading to one of these titles is paid for. For more information, it is advisable to check with the dispensing institutions.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The national of an EU or EEA state, legally practising as an ERM manipulator in one of these states, may use his or her professional title in France, either temporarily or occasionally.

He will have to apply, before his first performance, by declaration addressed to the prefect of the region in which he wishes to make the delivery (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least one year, in the ten years before the performance.

*For further information*: Article L. 4351-8 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, who is established and legally practises as an ERM manipulator in that state, may carry out the same activity in France on a permanent basis if he:

- holds a training certificate issued by a competent authority in another Member State, which regulates access to or exercise of the profession;
- has worked full-time or part-time for one year in the last ten years in another Member State that does not regulate training or the practice of the profession;
- holds a diploma, title or certificate acquired in a third state but recognised and admitted in equivalence by an EU or EEA state on the additional condition that the person concerned has been practising for three years the activity of manipulator ERM in the State which has admitted equivalence.

Once the national fulfils one of these conditions, he or she will be able to apply for an individual authorisation to practise from the regional prefect in which he wishes to practice his profession (see infra "5o). b. Request an exercise permit for the EU or EEA national for permanent activity (LE) ").

Where there are substantial differences between the professional qualification of the national and the training required in France, the prefect may subject him to compensation measures.

*To go further*Article L. 4351-4 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Rules of good practice

The European Society of Radiotherapy and Oncology (ESTRO) has drafted a Code of Ethics and Conduct to guide the ERM manipulator in the practice of his profession.

In particular, the Code specifies that the ERM manipulator must:

- Ensure the safety and quality of radiation therapy
- Respect and protect the best interests of the patient
- Respect medical confidentiality
- ensure the patient's informed consent.

### b. Criminal sanctions

The illegal practice of the profession of manipulator ERM and the use without the title of training are offences punishable by one year's imprisonment and a fine of 15,000 euros.

*For further information*: Articles L. 4353-1 and L. 4353-2 of the Public Health Code.

4°. Insurance
---------------------------------

As a health professional, the liberal ERM manipulator must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The regional prefect is responsible for deciding on the request for a prior declaration of activity.

**Supporting documents**

The application is made by filing a file that includes the following documents:

- A copy of a valid ID
- A copy of the training title allowing the profession to be practised in the state of obtainment;
- a certificate, less than three months old, from the competent authority of the EU State or the EEA, certifying that the person concerned is legally established in that state and that, when the certificate is issued, there is no prohibition, even temporary, Exercise
- any evidence justifying that the national has practised the profession in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise;
- where the training certificate has been issued by a third state and recognised in an EU or EEA state other than France:- recognition of the training title established by the state authorities that have recognised this title,
  - any evidence justifying that the national has practiced the profession in that state for three years;
- If so, a copy of the previous statement as well as the first statement made;
- a certificate of professional civil liability.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Timeframe**

Upon receipt of the file, the regional prefect will have one month to decide on the application and will inform the national:

- that he can start the performance. From then on, the prefect will register the applicant in the Adeli directory;
- that he will be subject to a compensation measure if there are substantial differences between the training or professional experience of the national and those required in France;
- he will not be able to start the performance;
- any difficulty that could delay its decision. In the latter case, the prefect will be able to make his decision within two months of the resolution of this difficulty, and no later than three months of notification to the national.

The prefect's silence within these deadlines will be worth accepting the request for declaration.

**Please note**

The return is renewable every year or at each change in the applicant's situation.

*For further information*: Articles R. 4351-25 and R. 4331-12 to R. 4331-15 of the Public Health Code.

### b. Requesting an exercise permit for the EU or EEA national for permanent activity (LE)

**Competent authority**

The authorisation to exercise is issued by the regional prefect, after advice from the commission of the manipulators of medical electroradiology.

**Supporting documents**

The application for authorization is made by filing a file containing all of the following documents:

- The[application form for authorization to practice](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1F8B7371FFFC32398488D9490CC59F7F.tplgfr24s_3?idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180213) ;
- A copy of a valid ID
- A copy of the training title
- If necessary, a copy of the additional diplomas;
- any evidence justifying ongoing training, experience and skills acquired in the EU or EEA State;
- a statement from the competent authority of the EU State or the EEA justifying the absence of sanctions against the national;
- A copy of the authorities' certificates specifying the level of training, the detail and the hourly volume of the courses followed, as well as the content and duration of the validated internships;
- any document justifying that the national has been a manipulator for one year in the last ten years, in an EU or EEA state, where neither access nor exercise is regulated in that state.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The prefect acknowledges receipt of the file within one month and will decide after having the opinion of the commission of manipulators ERM. The latter is responsible for examining the knowledge and skills of the national acquired during his training or during his professional experience. It may subject the national to a compensation measure.

The silence kept by the prefect of the region within four months is a decision to reject the application for authorisation.

**Good to know: compensation msures**

While the examination of the professional qualifications attested by the training credentials and work experience reveals substantial differences with the qualifications required for access to the profession of manipulator ERM and its in France, the person concerned will have to submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority may either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- require an adjustment course and/or aptitude test.

*For further information*: Articles R. 4351-22 to R. 4351-24 of the Public Health Code; decree of 20 January 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the exercise in France of the professions of genetic counsellor, nurse, masseur-kinesitherapist, pedicure-podologist, occupational therapist, manipulator in medical electroradiology and manipulator ERM.

### c. Registration with the Adeli directory

A national wishing to practise as an ERM manipulator in France is required to register his authorisation to practice on the Adeli directory ("Automation of The Lists").

**Competent authority**

Registration in the Adeli directory is done with the regional health agency (ARS) of the place of practice.

**Timeframe**

The application for registration is submitted within one month of taking office of the national, regardless of the mode of practice (liberal, salaried, mixed).

**Supporting documents**

In support of his application for registration, the ERM manipulator must provide a file containing:

- the original diploma or title attesting to the training of ERM manipulator issued by the EU State or the EEA (translated into French by a certified translator, if applicable);
- ID
- Deer Form 10906-06 completed, dated and signed.

**Outcome of the procedure**

The national's Adeli number will be directly mentioned on the receipt of the file, issued by the ARS.

**Cost**

Free.

*For further information*: Article L. L4351-10 of the Public Health Code.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

