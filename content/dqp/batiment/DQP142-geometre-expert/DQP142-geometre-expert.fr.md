﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP142" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Bâtiment" -->
<!-- var(title)="Géomètre-expert" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="batiment" -->
<!-- var(title-short)="geometre-expert" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/batiment/geometre-expert.html" -->
<!-- var(last-update)="2020-04-15 17:20:46" -->
<!-- var(url-name)="geometre-expert" -->
<!-- var(translation)="None" -->

# Géomètre-expert

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:46<!-- end-var -->

## 1°. Définition de l’activité

Le géomètre-expert est un professionnel dont la mission est :

- de réaliser les études et les travaux topographiques fixant les limites des biens fonciers dans le cadre de missions d’aménagement du territoire ;
- de procéder à toutes opérations techniques ou études sur l’évaluation, la gestion ou l’aménagement des biens fonciers.

*Pour aller plus loin* : article 1er de la loi n° 46-942 du 7 mai 1946 instituant l’Ordre des géomètres-experts.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer la profession de géomètre-expert, l’intéressé doit être inscrit au tableau de l’Ordre des géomètres-experts.

À ce titre, il doit :

- avoir 25 ans révolus à l'inscription ;
- être titulaire :
  - soit du diplôme de géomètre-expert foncier délivré par le ministre chargé de l’éducation nationale,
  - soit du diplôme d’ingénieur géomètre délivré par une école d’ingénieurs ;
- ne pas être ou avoir été frappé de faillite personnelle, redressement ou liquidation judiciaire ;
- ne pas être ou avoir été condamné pénalement ou révoqué pour des agissements contraires à la probité, à l’honneur ou aux règles applicables à la profession de géomètre-expert ;
- ne pas être sous le coup d’une interdiction temporaire ou définitive d’exercer la profession de géomètre-expert.

Le ressortissant d’un État membre de l’Union européenne (UE) ou d’un État partie à l’Espace économique européen (EEE) devra remplir les conditions suivantes :

- posséder les connaissances linguistiques nécessaires à l’exercice de la profession en France ;
- ne pas avoir fait l’objet d’une sanction de même nature que celles visées ci-avant ;
- avoir fait l’objet d’une reconnaissance de qualification (cf. infra « 5°. a. Demande de déclaration préalable en vue d’un exercice temporaire ou occasionnel (LPS) »).

*Pour aller plus loin* : article 16 du décret n° 96-478 du 31 mai 1996 portant règlement de la profession de géomètre-expert et Code des devoirs professionnels.

#### Formation

Trois voies permettent d’acquérir le titre de géomètre-expert :

- le diplôme d’ingénieur géomètre ;
- le diplôme de géomètre-expert foncier délivré par le gouvernement ;
- la procédure de validation des acquis par l’expérience (VAE). Pour plus d’informations, il est conseillé de se reporter au [site officiel de la VAE](http://www.vae.gouv.fr/).

En outre, les futurs géomètres-experts doivent suivre un stage obligatoire de deux ans et rédiger un mémoire.

##### Diplôme d’ingénieur géomètre (niveau bac +5)

La principale voie d’accès à la profession de géomètre-expert est le cursus d’ingénieur géomètre de niveau master (bac +5) au sein d’une école d’enseignement supérieur, suivi de deux années de stage professionnel.

Trois écoles en France dispensent la formation d’ingénieur géomètre :

- l’ESGT (École supérieure des géomètres et topographes) au Mans ;
- l’ESTP (École spéciale des travaux publics) à Paris ;
- l’INSA (Institut national des sciences appliquées) à Strasbourg.

Peuvent accéder à ce cursus les titulaires d’un brevet de technicien supérieur (BTS) topographe après deux années de classe préparatoire, et les titulaires d’un bac professionnel « technicien géomètre-topographe ».

La formation d’ingénieur géomètre dure trois ans et comprend plusieurs stages professionnels. Un premier stage à l’étranger (quatre à douze semaines), un deuxième stage lors de la troisième année de formation (de quatre à six mois) au sein d’une entreprise et au cours duquel l’élève ingénieur effectuera un travail de fin d’études (TFE) conclu par la soutenance d’un mémoire.

À l’issue de cette formation, l’ingénieur géomètre devra également effectuer un stage d’exercice professionnel d’une durée de deux ans dans un cabinet de géomètre-expert.

##### Diplôme de géomètre-expert foncier délivré par le gouvernement (DPLG)

Ce diplôme est accessible aux candidats ayant accompli un stage professionnel, validé les unités de formation exigées et satisfait aux exigences du mémoire.

Parallèlement à son stage, le candidat doit suivre les unités de formation suivantes :

- exercice de la profession de géomètre-expert et délégation de service public (seize jours de formation) ;
- droit (seize jours de formation) ;
- sciences de la mesure et géomatique (seize jours de formation) ;
- aménagement du territoire (huit jours de formation) ;
- aménagement de la propriété (huit jours de formation).

En outre, au cours des trois années qui suivent la validation des formations et la validation de son stage professionnel, le candidat doit soutenir, devant un jury, un mémoire portant sur l’exercice du métier de géomètre-expert et visant à évaluer ses aptitudes professionnelles et la qualité de ses travaux.

À l’issue de ces épreuves, le diplôme de géomètre-expert foncier DPLG est délivré au candidat qui :

- a réussi la soutenance de son mémoire ;
- possède le certificat de validation des unités de formation prescrites, ainsi que le certificat de fin de stage délivré par le conseil régional de l’Ordre des géomètres-experts ;
- justifie du ou des diplômes suivants :
  - un master dans les spécialités suivantes : sciences de l’ingénieur, métiers de l’urbanisme, de l’architecture et des paysages, géomatique, topographie,
  - un diplôme d’ingénieur généraliste,
  - un diplôme de fin d’études de l’institut de topométrie du Conservatoire national des arts et métiers (CNAM),
  - un diplôme de licence et de cinq ans de pratique professionnelle,
  - un BTS de géomètre-topographe et de six ans de pratique professionnelle,
  - un diplôme de niveau bac +2 et de huit ans de pratique professionnelle ;
- exerce l’activité de géomètres-expert depuis au moins 15 ans ;
- le cas échéant, est en mesure de produire les documents attestant la pratique professionnelle établis par ses employeurs successifs.

Le géomètre-expert diplômé peut alors faire suivre son nom de la mention « Géomètre-expert foncier DPLG ».

*Pour aller plus loin* : arrêté du 8 décembre 2015 relatif au diplôme de géomètre-expert foncier délivré par le gouvernement.

##### Stage professionnel de deux ans

Pour être inscrit à l’Ordre des géomètres-experts, un stage d’exécution de travaux professionnels doit obligatoirement être suivi par le futur géomètre-expert, sous la surveillance et la responsabilité d’un membre de l’Ordre ou d’une administration agréée par le ministre chargé de l’enseignement supérieur et de l’architecture.

La demande d’inscription à un stage est faite par courrier recommandé au président du conseil régional de la région dont dépend le cabinet principal du maître de stage. Une fois la demande reçue, le président du conseil régional indiquera les modalités et les pièces nécessaires à la procédure d’inscription.

Au cours de son stage, le stagiaire fera l’objet d’une évaluation et, à ce titre, devra remettre au conseil régional de l’Ordre un rapport de stage chaque année.

À l’issue du stage, le conseil régional de l’Ordre émettra un avis et délivrera au candidat un certificat de fin de stage.

Durant son stage, le stagiaire doit suivre des modules de formations d’une durée totale de seize jours sur les thèmes suivants :

- l’éthique professionnelle et la déontologie ;
- le bornage ;
- la propriété publique ;
- les servitudes ;
- les divisions foncières ;
- la copropriété ;
- l’expertise/médiation ;
- l’aménagement durable du territoire ;
- le référentiel foncier unifié ;
- la gestion/comptabilité appliquée à la profession.

**Bon à savoir**

Pour la filière DPLG, ces formations se cumulent avec les formations précitées.

Certains candidats peuvent, s’ils en font la demande, bénéficier d’une réduction de la durée du stage pouvant aller jusqu'à un an dès lors qu’ils justifient de quinze ans d’expérience professionnelle dont cinq ans au moins dans des fonctions d’encadrement.

*Pour aller plus loin* : article 6 du décret n° 2010-1406 du 12 novembre 2010 relatif au diplôme de géomètre-expert foncier délivré par le gouvernement.

#### Coûts associés à la qualification 

La formation menant à l’obtention du diplôme d’ingénieur géomètre est payante et son coût varie selon le cursus professionnel envisagé. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d’un État membre de l’UE ou de l’EEE qui est établi et exerce légalement l’activité de géomètre-expert peut exercer en France de manière temporaire et occasionnelle la même activité, et ce sans être inscrit à l’Ordre des géomètres-experts.

Il devra adresser, préalablement à l’exécution de la prestation de services, une demande auprès du conseil régional dans le ressort duquel la prestation doit être réalisée (cf. infra « 5°. a. Demande de déclaration préalable en vue d’un exercice temporaire ou occasionnel (LPS) »).

Le candidat doit également avoir les connaissances linguistiques nécessaires à l’exercice de la profession en France.

*Pour aller plus loin* : article 2-1 de la loi du 7 mai 1946 ; article 39 du décret n° 96-478 du 31 mai 1996 portant règlement de la profession de géomètre-expert et Code des devoirs professionnels.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Pour exercer à titre permanent sur le territoire français, le ressortissant d’un État membre de l’UE ou de l’EEE doit être en possession :

- soit d’une attestation de compétence ou d’un titre de formation délivré par l’autorité compétente d’un État qui réglemente l’exercice de la profession ;
- soit d’une attestation indiquant qu’il a exercé l’activité pendant un an au cours des dix dernières années précédant la demande dans un État qui ne réglemente pas la profession.

Dès lors qu’il remplit l’une de ces conditions, il pourra demander une reconnaissance de qualification auprès du ministre chargé de l’urbanisme (cf. infra « 5°. b. Demande de reconnaissance de qualification professionnelle pour le ressortissant en vue d’un exercice permanent (LE) »).

**À noter**

Le géomètre-expert qui a effectué une demande de reconnaissance peut également solliciter son inscription au tableau de l’Ordre des géomètres-experts (cf. infra « 4°. Inscription à l’Ordre des géomètres-experts et assurance »).

*Pour aller plus loin* : article 7-1 du décret du 31 mai 1996.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### a. Règles déontologiques et éthiques

Le géomètre-expert est soumis, durant toute la durée de sa prestation, au respect des règles disciplinaires. Le conseil régional de l’Ordre dans la circonscription duquel il sera inscrit assure le respect des règles suivantes :

- garantie d’indépendance lors de chacune de ses missions. À noter que la qualité de membre de l’Ordre est incompatible avec une charge d’officier public ou ministériel ;
- une obligation de conseil et de transparence envers ses clients ;
- une obligation de dater et signer l’ensemble des plans et documents établis ainsi que d’apposer son cachet ;
- le devoir de mémoire ;
- le secret professionnel ;
- un recours à la publicité possible uniquement à titre informatif.

**À savoir**

Toute publicité de la part du géomètre-expert sera soumise au conseil régional de l’Ordre avant sa diffusion.

*Pour aller plus loin* : article 8 de la loi du 7 mai 1946.

### b. Sanctions disciplinaires 

En cas de sanction, le professionnel de l'immobilier peut encourir une peine disciplinaire, à savoir :

- un avertissement ;
- un blâme ;
- une suspension pour une durée maximale d’un an ;
- une radiation du stage ou du tableau de l’Ordre qui implique l’interdiction d’exercer la profession.

*Pour aller plus loin* : articles 23, 23-1 et 24 de la loi du 7 mai 1946.

### c. Sanctions pénales

Toute personne qui exercerait la profession de géomètre-expert sans en remplir les conditions pourra être poursuivie pour exercice illégal de la profession et punie des peines encourues pour le délit d’usurpation (cf. article 433-17 du Code pénal).

La violation du secret professionnel est également punie d’un an d’emprisonnement et de 15 000 euros d’amende 

*Pour aller plus loin* : articles 226-13 et 226-14 du Code pénal.

## 4°. Inscription à l’Ordre des géomètres-experts et assurance

### a. Demande d’inscription à l’Ordre des géomètres-experts

L’inscription à l’Ordre des géomètres-experts est obligatoire pour exercer légalement l’activité sur le territoire français.

#### Autorité compétente

Le géomètre-expert doit adresser au président du conseil régional de la circonscription dans laquelle il désire s’établir une demande comprenant :

- un formulaire de renseignements remis par le conseil régional en trois exemplaires ;
- une copie d'un diplôme délivrant le titre de géomètre-expert ou d’une décision ministérielle portant reconnaissance de qualification ou, à défaut, l’accusé de réception du dossier complet de la demande de reconnaissance par le ministre chargé de l’urbanisme ;
- trois photographies d'identité ;
- une attestation de versement de l'indemnité pour frais de dossier ;
- une fiche d'état civil et une pièce d'identité en cours de validité ;
- un extrait du casier judiciaire ou un document équivalent ;
- pour le ressortissant, un diplôme d'études en langue française ou un diplôme approfondi de langue française, ou, le cas échéant, une attestation de possession des compétences linguistiques à l'exercice de la profession en France établie par le géomètre-expert chez qui le demandeur a effectué un stage.

À compter de la réception complète de la demande, le président du conseil régional en accuse réception à l’intéressé dans un délai d’un mois.

#### Coûts

Le montant de l’indemnité pour frais de dossier est fixé chaque année par le Conseil supérieur après approbation du commissaire du gouvernement (à titre indicatif, elle était de 200 € en 2017).

#### Voie de recours

Le demandeur peut former dans les deux mois de la notification de la décision, un recours auprès du Conseil supérieur de l’Ordre qui aura alors quatre mois pour statuer.

Une fois inscrit le géomètre-expert personne physique prête serment devant le conseil régional et reçoit un numéro d’inscription à l’Ordre délivré par le Conseil supérieur ainsi qu’une carte professionnelle.

*Pour aller plus loin* : arrêté du 12 novembre 2009 relatif à l’inscription au tableau de l’Ordre des géomètres-experts.

### b. Assurance

En qualité de professionnel, le géomètre-expert doit, à l’ouverture de tout chantier, souscrire une assurance de responsabilité professionnelle. S’il exerce en tant que salarié, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de cette activité.

*Pour aller plus loin* : articles L. 241-1 et suivants du Code des assurances.
 
## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande de déclaration préalable en vue d’un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le Conseil de l’Ordre des géomètres-experts est compétent pour se prononcer sur la demande de déclaration préalable.

#### Pièces justificatives

La demande est un dossier comportant les pièces justificatives suivantes :

- une attestation d'assurance de responsabilité civile professionnelle ;
- une attestation certifiant que le géomètre-expert est légalement établi dans un État membre de l’UE ou de l’EEE pour y exercer sa profession ;
- la preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE.

*Pour aller plus loin* : article 2-1 de la loi du 7 mai 1946 ; articles 39 et suivants du décret du 31 mai 1996.

### b. Demande de reconnaissance de qualification professionnelle pour le ressortissant en vue d’un exercice permanent (LE)

#### Autorité compétente

Le ministre chargé de l’urbanisme est compétent pour se prononcer sur la demande de reconnaissance de qualification du ressortissant.

#### Pièces justificatives

La demande doit être adressée en double exemplaire et comporter :

- une fiche d’état civil et une pièce d’identité datant de moins de trois mois ;
- une copie des diplômes, certificats ou titres attestant la formation reçue, ainsi qu’un descriptif du contenu des études poursuivies, des stages effectués et le nombre d’heures correspondant ;
- une attestation de compétence ou un titre de formation, ou tout autre document justifiant que le géomètre-expert a bien exercé pendant un an au cours des dix dernières années dans un État membre de l’UE ou de l’EEE.

#### Délai

Le ministre chargé de l'urbanisme disposera de trois mois à compter de la réception du dossier complet pour se prononcer sur la demande. Il pourra décider soit de reconnaître les qualifications professionnelles du ressortissant, et dans ce cas, lui permettre d'effectuer l'activité de géomètre-expert en France, soit de la soumettre à une mesure de compensation.

**À noter**

L’absence de réponse dans un délai de trois mois équivaut au rejet de la demande.

#### Bon à savoir : mesures de compensation

Le ministre chargé de l’urbanisme peut décider que le ressortissant accomplira un stage d’adaptation pendant trois ans maximum ou qu'il se soumettra à une épreuve d’aptitude. Cette dernière consiste en la présentation et la discussion d’un dossier portant sur l’expérience professionnelle et les connaissances du candidat pour l’exercice de la profession. Le dossier est remis à l’intéressé avant sa présentation devant un jury, qui fera ensuite connaître les résultats de l’épreuve au ministre chargé de l’urbanisme.

Une fois la qualification obtenue, le demandeur pourra solliciter son inscription au tableau de l’Ordre des géomètres-experts (cf. supra « 4°. a. Demande d’inscription à l’Ordre des géomètres-experts »).

*Pour aller plus loin* : article 2-1 de la loi du 7 mai 1946 ; articles 7 et suivants du décret du 31 mai 1996.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).