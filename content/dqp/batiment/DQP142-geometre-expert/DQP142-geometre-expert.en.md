﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP142" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construction" -->
<!-- var(title)="Land surveyor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construction" -->
<!-- var(title-short)="land-surveyor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/construction/land-surveyor.html" -->
<!-- var(last-update)="2020-04-15 17:20:47" -->
<!-- var(url-name)="land-surveyor" -->
<!-- var(translation)="Auto" -->


Land surveyor
===============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:47<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The surveyor-expert is a professional whose mission is:

- carry out topographical studies and work setting the boundaries of land in land use of land-use planning missions;
- conduct any technical operations or studies on the assessment, management or management of land assets.

*For further information*: Article 1 of Law 46-942 of May 7, 1946 establishing the Order of Expert Surveyors.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practise as an expert surveyor, the individual must be registered on the order of surveyor-experts.

As such, it must:

- 25 years old by enrollment
- Be a holder:- or the land surveyor-expert diploma issued by the Minister for National Education,
  - or a surveyor engineering degree from an engineering school;
- not to be or have been affected by personal bankruptcy, redress or liquidation;
- not to be or have been criminally convicted or dismissed for acts contrary to probity, honour or rules applicable to the profession of surveyor-expert;
- not be subject to a temporary or permanent ban from practising as an expert surveyor.

The national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) must meet the following conditions:

- Have the language skills necessary to practice the profession in France;
- Not having been subject to a sanction of the same nature as those referred to above;
- have been the subject of a qualification recognition (see infra "5°). a. Request for pre-reporting for a temporary or casual exercise (LPS)).

*For further information*: Article 16 of Decree 96-478 of 31 May 1996 on the regulation of the profession of surveyor-expert and code of professional duties.

#### Training

There are three ways to acquire the title of surveyor-expert:

- a surveyor's engineering degree;
- The government's land surveyor-expert diploma;
- The Experience Validation Process (VAE). For more information, it is advisable to refer to the[VAE's official website](http://www.vae.gouv.fr/).

In addition, future surveyors must complete a mandatory two-year internship and write a dissertation.

**Diploma of surveyor engineer (bac level 5)**

The main route to the profession of surveyor-expert is the master's degree in surveyor engineering (bac 5) in a higher education school, followed by two years of professional internship.

Three schools in France provide training as surveyor engineers:

- ESGT (Higher School of Surveyors and Topographers) in Le Mans;
- ESTP (Special School of Public Works) in Paris;
- INSA (National Institute of Applied Sciences) in Strasbourg.

The holders of a superior technician (BTS) certificate after two years of preparatory class can access this course, and holders of a professional degree "surveyor-topographer technician".

The training as a surveyor engineer lasts three years and includes several professional internships. A first internship abroad (four to twelve weeks), a second internship during the third year of training (four to six months) in a company and during which the student engineer will carry out a work of completion (TFE) concluded by the defense a memoir.

At the end of this training, the surveyor engineer will also be required to complete a two-year professional internship in a surveyor-expert firm.

**Government-issued surveyor-expert land diploma (DPLG)**

This diploma is available to candidates who have completed a professional internship, validated the required training units and meets the requirements of the dissertation.

In addition to the internship, the candidate must complete the following training units:

- exercise of the profession of surveyor-expert and public service delegation (sixteen days of training);
- right (sixteen days of training);
- measurement and geomatics sciences (sixteen days of training);
- land use planning (eight days of training);
- development of the property (eight days of training).

In addition, during the three years following the validation of training and the validation of his professional internship, the candidate must support, before a jury, a dissertation on the practice of the profession of surveyor-expert and aimed at assessing his skills and the quality of his work.

At the end of these tests, the DPLG land surveyor-expert diploma is awarded to the candidate who:

- successfully supported his brief;
- has the certificate of validation of the prescribed training units, as well as the certificate of completion of the internship issued by the regional council of the Order of Surveyors-experts;
- justifies the following degrees:- a master's degree in the following specialties: engineering sciences, urban planning, architecture and landscape trades, geomatics, topography,
  - a general engineering degree,
  - an end-of-study degree from the Topometric Institute of the National Conservatory of Arts and Crafts (CNAM),
  - a bachelor's degree and five years of professional practice,
  - a BTS surveyor-topographer and six years of professional practice,
  - a baccalaureate degree and eight years of professional practice;
- has been working as an expert surveyor for at least 15 years;
- if so, is able to produce documents attesting to professional practice established by successive employers.

The certified surveyor-expert can then refer to his name as "DPLG Geometer-Expert.

*For further information*: decree of 8 December 2015 relating to the diploma of surveyor-expert land issued by the government.

**Two-year internship**

To be registered with the Order of Expert Surveyors, an internship of professional work must be followed by the future surveyor-expert, under the supervision and responsibility of a member of the Order or an administration approved by Minister for Higher Education and Architecture.

The application for an internship is made by registered letter to the president of the regional council of the region on which the main office of the trainee depends. Once the application is received, the regional council president will indicate the terms and documents necessary for the registration process.

During the internship, the trainee will be evaluated and, as such, will be required to submit an internship report to the College's regional council each year.

At the end of the internship, the College's regional council will issue a notice and issue the candidate with a certificate of completion of the internship.

During the internship, the trainee must complete training modules of a total of sixteen days on the following topics:

- Professional ethics and ethics;
- boundary;
- Public property
- easements;
- Land divisions;
- Condominium;
- Expertise/mediation
- Sustainable land use
- Unified land repository
- management/accounting applied to the profession.

**Good to know**

For the DPLG sector, these courses are combined with the aforementioned courses.

Some candidates may, if they apply, benefit from a reduction in the duration of the internship of up to one year if they justify fifteen years of professional experience, including at least five years in management positions.

*For further information*: Article 6 of Decree No. 2010-1406 of November 12, 2010 on the government's land surveyor-expert diploma.

#### Costs associated with qualification

Training leading to the diploma of surveyor engineer is paid and its cost varies according to the professional course envisaged. For more information, it is advisable to check with the institutions concerned.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of an EU or EEA Member State who is established and legally practises the activity of surveyor-expert may carry out the same activity in France on a temporary and occasional basis, without being registered with the Order of Expert Surveyors.

It will have to apply to the regional council in the case of which the delivery must be carried out in advance of the delivery of services (see infra "5o). a. Request for pre-reporting for a temporary or casual exercise (LPS)).

The candidate must also have the necessary language skills to practice the profession in France.

*For further information*: Article 2-1 of the Law of 7 May 1946; Article 39 of Decree 96-478 of 31 May 1996 on the regulation of the profession of surveyor-expert and code of professional duties.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

To exercise permanently on French territory, the national of an EU or EEA member state must be in possession of:

- either a certificate of competency or a training certificate issued by the competent authority of a state that regulates the practice of the profession;
- or a certificate indicating that he has been in the business for one year in the last ten years prior to the application in a state that does not regulate the profession.

Once he fulfils one of these conditions, he will be able to apply for qualification recognition from the Minister responsible for urban planning (see infra "5°. b. Application for recognition of professional qualification for the national for a permanent exercise (LE)).

**Please note**

An expert surveyor who has applied for recognition may also apply for inclusion in the Order of Expert Surveyors (see below "4." Registration to the Order of Surveyors-Experts and Insurance").

*For further information*: Article 7-1 of the May 31, 1996 decree.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethical and ethical rules

The surveyor-expert is subject, throughout his performance, to compliance with disciplinary rules. The Order's regional council in the riding from which it will be registered ensures compliance with the following rules:

- guarantee of independence in each of its missions. It should be noted that membership of the Order is incompatible with a public or ministerial officer office;
- an obligation to advise and be transparent to its clients;
- an obligation to date and sign all the plans and documents drawn up as well as to stamp it;
- The duty of remembrance
- Professional secrecy
- possible use of advertising only for informational purposes.

**What to know**

Any advertising by the surveyor-expert will be submitted to the Order's regional council before it is broadcast.

*For further information*: Article 8 of the Act of May 7, 1946.

### b. Disciplinary sanctions

In the event of a sanction, the real estate professional may incur a disciplinary penalty, namely:

- A warning
- Blame
- A suspension for up to one year
- a delisting from the college's internship or table, which implies a ban on practising the profession.

*For further information*: Sections 23, 23-1 and 24 of the May 7, 1946 Act.

### c. Criminal sanctions

Anyone who practises as an expert surveyor without fulfilling its conditions may be prosecuted for illegal practice of the profession and punishable by penalties for the offence of usurpation (see Article 433-17 of the Penal Code).

Violation of professional secrecy is also punishable by one year's imprisonment and a fine of 15,000 euros

*For further information*: Articles 226-13 and 226-14 of the Penal Code.

4°. Registration for the College of Expert Surveyors and Insurance
--------------------------------------------------------------------------------------

### a. Application to register with the College of Surveyors-Experts

Registration for the Order of Expert Surveyors is mandatory to carry out the activity legally on French territory.

**Competent authority**

The surveyor-expert must send a request to the president of the regional council of the riding in which he wishes to make an application, which includes:

- An information form provided by the regional council in three copies;
- A copy of a diploma issuing the title of surveyor-expert or a ministerial decision recognizing qualification or, failing that, the acknowledgement of receipt of the full file of the application for recognition by the Minister responsible for planning;
- three identity photographs;
- A certificate of payment of the claim fee;
- A valid civil registration card and identification;
- an extract from the criminal record or an equivalent document;
- for the national, a French-language degree or a deep French-language diploma, or, if necessary, a certificate of possession of language skills in the practice of the profession in France established by the surveyor-expert applicant has completed an internship.

Once the application is fully received, the president of the regional council acknowledges the request within one month.

**Costs**

The amount of the compensation for file costs is set each year by the Higher Council after approval by the Government Commissioner (as an indication, it was 200 euros in 2017).

**Remedy**

The applicant may file an appeal with the Higher Council of the Order within two months of notification of the decision, which will then have four months to decide.

Once the surveyor-expert is registered before the regional council and receives a registration number to the Order issued by the Higher Council as well as a professional card.

*For further information*: order of November 12, 2009 relating to the inclusion on the board of the Order of Surveyors-experts.

### b. Insurance

As a professional, the surveyor-expert must, at the opening of any construction site, take out professional liability insurance. If he exercises as an employee, it is up to the employer to take out such insurance for his employees for the acts carried out during this activity.

*For further information*: Articles L. 241-1 and the following from the Insurance Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for pre-reporting for a temporary and casual exercise (LPS)

**Competent authority**

The Council of the Order of Surveyors is responsible for deciding on the request for prior declaration.

**Supporting documents**

The application is a file with the following supporting documents:

- a certificate of professional liability insurance;
- a certificate certifying that the expert surveyor is legally established in an EU or EEA member state to practise his profession;
- proof by any means that the national has been engaged in this activity, full-time or part-time, in the last ten years when neither professional activity nor training is regulated in the EU or EEA State.

*For further information*: Article 2-1 of the Law of 7 May 1946; Articles 39 and following of the may 31, 1996 decree.

### b. Application for recognition of professional qualification for the national for a permanent exercise (LE)

**Competent authority**

The Minister for Planning is responsible for deciding on the application for recognition of qualification of the national.

**Supporting documents**

The request must be submitted in duplicate and include:

- A civil registration card and identification less than three months old;
- A copy of diplomas, certificates or titles attesting to the training received, as well as a description of the content of the studies carried out, the internships carried out and the corresponding number of hours;
- a certificate of competency or a training document, or any other document justifying that the surveyor-expert has exercised for a year in the last ten years in an EU or EEA member state.

**Timeframe**

The Minister responsible for urban planning will have three months from receipt of the full file to decide on the application. It may decide either to recognize the professional qualifications of the national, and in this case, to allow him to carry out the activity of surveyor-expert in France, or to submit it to a compensation measure.

**Please note**

Failure to respond within three months is tantamount to the rejection of the application.

**Good to know: compensation measures**

The Minister for Planning may decide that the national will complete an adjustment course for up to three years or that he will submit to an aptitude test. The latter consists of the presentation and discussion of a file on the candidate's professional experience and knowledge for the practice of the profession. The file is given to the person concerned before it is presented to a jury, who will then make the results of the test known to the Minister responsible for urban planning.

Once qualified, the applicant will be able to apply for registration in the Order of Expert Surveyors (see supra "4°. a. Application for registration with the Order of Surveyors-Experts").

*For further information*: Article 2-1 of the Law of 7 May 1946; Articles 7 and following of the decree of 31 May 1996.

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

