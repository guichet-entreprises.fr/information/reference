﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP094" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construction" -->
<!-- var(title)="Home inspector" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construction" -->
<!-- var(title-short)="home-inspector" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/construction/home-inspector.html" -->
<!-- var(last-update)="2020-04-15 17:20:46" -->
<!-- var(url-name)="home-inspector" -->
<!-- var(translation)="Auto" -->


Home inspector
========================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:46<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The real estate diagnostician is a professional whose activity consists, on the occasion of a real estate transaction, of establishing a technical balance sheet of the building.

The Technical Diagnostics (DDT) file may include:

- The energy performance of the building for the release of the building's energy label;
- The safety of its gas facilities;
- Electricity facilities
- the presence of asbestos, lead or any other cause of unsanitary conditions.

**Please note**

The documents established in connection with his activity include the mention of the professional's certification.

*For further information*: Articles L. 271-4 and R. 271-3 of the Building and Housing Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of real estate diagnosticator the professional must:

- be professionally qualified (see infra "2.2). Initial training");
- be in possession of a certificate of competency (see infra "2." Certification").

*For further information*: Article R. 271-1 of the Building and Housing Code.

#### Training

**Initial training**

To be recognized as a professionally qualified, the professional must hold one of the following degrees:

- professional license mention "BTP trades: energy and environmental performance of buildings" or "safety of goods and people";
- Building and public works research projector diploma issued by the National Conservatory of Arts and Crafts (Cnam);
- Real Estate Diagnostics Technician Diploma from the Technical Institute of Gas and Air (ITGA);
- real estate diagnostic diploma issued by a higher education institution.

These diplomas are available to candidates from a training course under student or student status, apprenticeship contract, after continuing education, professionalisation contract, individual application or through the process of validation of experience (VAE). For more information, it is advisable to refer to the[VAE's official website](http://www.vae.gouv.fr/).

In the absence of any of the above degrees, the professional must have three years of professional experience as a technician or master's officer in the building field, or any other equivalent function.

*For further information*: National directory of professional certifications ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ;[Appendix 2](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2402E91AE29265EE7B4A9C101AD212D6.tplgfr22s_3idArticle=LEGIARTI000025027138&cidTexte=LEGITEXT000025027132&dateTexte=20180227) of the order of 16 October 2006 setting out the criteria for certifying the skills of individuals making the diagnosis of energy performance or the certificate of consideration of thermal regulations, and the criteria accreditation of certification bodies.

**Certification**

The professional holding one of the aforementioned diplomas must, in order to make a real estate diagnosis, justify guarantees of competence.

In order to do so, the individual or legal person must obtain a certification of competence issued by a certifying body accredited by the French Accreditation Committee ([Cofrac](https://www.cofrac.fr/)) in the construction sector (see infra "5°. a. Certification procedure").

*For further information*: Articles L. 271-6 and R. 271-1 of the Building and Housing Code.

#### Costs associated with qualification

The cost of qualification varies depending on the course envisaged. For more information, it is advisable to get closer to the establishment in question.

### b. EU nationals: for temporary and casual (Freedom to provide services) or permanent (Freedom of establishment(LE))

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may practice on a temporary and casual basis or the same activity in France.

For this reason, the national is subject to the same requirements as the French national (see above "2." a. National requirements") and must thus justify a diploma of a level equivalent to that required for a French national.

In addition, it must obtain certification from a European body that is a signatory to the multilateral European agreement taken as part of the European coordination of accreditation bodies.

*For further information*: Appendix 2 of the order of 16 October 2006 above.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The professional must carry out his activity impartially and independently and, as such, must have no connection to interfere with these obligations.

*For further information*: Article L. 271-6 of the Building and Housing Code.

**Please note**

The professional may be required to comply with the ethical rules specific to each certifying body. For more information, it is advisable to visit the website of these organizations.

**Criminal sanctions**

The real estate diagnosticor faces a fine of up to 1,500 euros (or 3,000 euros in the event of a repeat offence) if he:

- makes a diagnosis without being professionally qualified, without having taken out insurance (see infra "4°. Insurance" or unaware of the obligations of impartiality and independence;
- is certified by an uncredited body to issue certification.

*For further information*: Article R. 271-4 of the Building and Housing Code.

4°. Insurance
---------------------------------

The real estate diagnosticor is required to take out insurance to cover the risks incurred during his activity.

The amount of this guarantee cannot be less than 300,000 euros per claim and 500,000 euros per year of insurance.

**Please note**

If the professional exercises as an employee, it is up to the employer to take out such insurance for his employees, for the acts carried out during their professional activity.

*For further information*: Articles L. 271-6 and R. 271-2 of the Building and Housing Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Certification procedure

The candidate for certification must submit an application file to the certifying body which will judge its admissibility. Once eligible, the candidate must be the subject of:

- theoretical examination to verify that the person has the knowledge of:- generalities about the building,
  - The thermal of the building,
  - The envelope of the building,
  - systems (heating technologies, the introduction of renewable energy, etc.),
  - Regulatory texts
- a practical examination of a candidate's situational analysis to verify that the candidate is capable of assessing a building's energy consumption and developing an aggressive performance diagnosis taking into account the specifics of the case Treaty.

The certification awarded to the professional is valid for five years. At the end of this period it must proceed with its renewal.

**Please note**

A professional may only hold one certification and must provide a declaration of honour attesting that he has no other certification.

*For further information*: Article R. 134-4 of the Building and Housing Code; October 16, 2006.

### b. Monitoring procedure

The certified professional is monitored by the certifying body and must provide:

- The status of claims and complaints against him during his activity;
- all the reports prepared as well as information about the mission carried out (date, local, type of mission, and information relating to the energy class of the premises).

**Please note**

This information may be requested during the certification renewal process.

*For further information*: decree of 16 October 2006 above; Section R. 271-1 of the Housing and Construction Code.

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form.

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

