﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP094" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Bâtiment" -->
<!-- var(title)="Diagnostiqueur immobilier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="batiment" -->
<!-- var(title-short)="diagnostiqueur-immobilier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/batiment/diagnostiqueur-immobilier.html" -->
<!-- var(last-update)="2020-04-15 17:20:46" -->
<!-- var(url-name)="diagnostiqueur-immobilier" -->
<!-- var(translation)="None" -->

# Diagnostiqueur immobilier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:46<!-- end-var -->

## 1°. Définition de l’activité

Le diagnostiqueur immobilier est un professionnel dont l'activité consiste, à l'occasion d'une transaction immobilière, à établir un bilan technique du bâtiment.

Le dossier de diagnostic technique (DDT) peut porter sur :

- la performance énergétique du bâtiment en vue de la délivrance de l'étiquette énergétique de l'immeuble ;
- la sécurité de ses installations de gaz ;
- les installations d'électricité ;
- la présence d'amiante, de plomb ou toute autre cause d'insalubrité des lieux.

**À noter**

Les documents établis dans le cadre de son activité comportent la mention de la certification du professionnel.

*Pour aller plus loin* : articles L. 271-4 et R. 271-3 du Code de la construction et de l'habitation.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de diagnostiqueur immobilier le professionnel doit :

- être qualifié professionnellement (cf. infra « 2°. Formation initiale ») ;
- être en possession d'un certificat de compétence (cf. infra « 2°. Certification »).

*Pour aller plus loin* : article R. 271-1 du Code de la construction et de l'habitation.

#### Formation

##### Formation initiale

Pour être reconnu comme étant qualifié professionnellement, le professionnel doit être titulaire de l'un des diplômes suivants :

- licence professionnelle mention « Métiers du BTP : performance énergétique et environnementale des bâtiments » ou « sécurité des biens et des personnes » ;
- diplôme de projecteur d'études bâtiment et travaux publics délivré par le Conservatoire national des arts et métiers (Cnam) ;
- diplôme de technicien en diagnostics immobiliers délivré par l'Institut technique des gaz et de l'air (ITGA) ;
- diplôme de diagnostiqueur immobilier délivré par un établissement d'enseignement supérieur.

Ces diplômes sont accessibles aux candidats issus d'un parcours de formation sous statut d'élève ou d'étudiant, en contrat d'apprentissage, après une formation continue, en contrat de professionnalisation, par candidature individuelle ou via la procédure de validation des acquis de l’expérience (VAE). Pour plus d’informations, il est conseillé de se reporter au [site officiel de la VAE](http://www.vae.gouv.fr/).

À défaut de l'un des diplômes susvisés, le professionnel doit être titulaire d'une expérience professionnelle de trois ans en tant que technicien ou agent de maîtrise dans le domaine du bâtiment, ou toute autre fonction équivalente.

*Pour aller plus loin* : Répertoire national des certifications professionnelles ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ; annexe 2 de l'arrêté du 16 octobre 2006 définissant les critères de certification des compétences des personnes physiques réalisant le diagnostic de performance énergétique ou l'attestation de prise en compte de la réglementation thermique, et les critères d'accréditation des organismes de certification.

##### Certification

Le professionnel titulaire de l'un des diplômes précités doit, pour effectuer un diagnostic immobilier, justifier de garanties de compétence.

Pour cela, l'intéressé personne physique ou morale doit obtenir une certification de compétence délivrée par un organisme certificateur accrédité par le Comité français d'accréditation ([Cofrac](https://www.cofrac.fr/)) dans le domaine de la construction (cf. infra « 5°. a. Procédure de certification »).

*Pour aller plus loin* : articles L. 271-6 et R. 271-1 du Code de la construction et de l'habitation.

#### Coûts associés à la qualification

Le coût de la qualification varie selon le cursus envisagé. Pour plus d'informations, il est conseillé de se rapprocher de l'établissement considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services) ou permanent (Libre Établissement(LE))

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de diagnostiqueur immobilier peut exercer à titre temporaire et occasionnel ou permanent la même activité en France.

Pour cela, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. supra « 2°. a. Exigences nationales ») et doit ainsi justifier d'un diplôme d'un niveau équivalent à celui requis pour un ressortissant français.

En outre, il doit obtenir une certification délivrée par un organisme européen signataire de l'accord européen multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation.

*Pour aller plus loin* : annexe 2 de l'arrêté du 16 octobre 2006 précité.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le professionnel doit exercer son activité en toute impartialité et indépendance et, à ce titre, ne doit avoir aucun lien de nature à porter atteinte à ces obligations.

*Pour aller plus loin* : article L. 271-6 du Code de la construction et de l'habitation.

**À noter**

Le professionnel peut être tenu au respect de règles déontologiques propres à chaque organisme certificateur. Pour plus d'informations, il est conseillé de consulter le site internet de ces organismes.

#### Sanctions pénales

Le diagnostiqueur immobilier encourt une amende d'un montant maximum de 1 500 euros (ou 3 000 euros en cas de récidive) dès lors qu'il :

- établit un diagnostic sans être qualifié professionnellement, sans avoir souscrit une assurance (cf. infra « 4°. Assurances ») ou en méconnaissance des obligations d'impartialité et d'indépendance ;
- est certifié par un organisme non accrédité pour délivrer la certification.

*Pour aller plus loin* : article R. 271-4 du Code de la construction et de l'habitation.

## 4°. Assurances

Le diagnostiqueur immobilier est tenu de souscrire une assurance permettant de couvrir les risques encourus lors de son activité.

Le montant de cette garantie ne peut être inférieur à 300 000 euros par sinistre et 500 000 euros par année d'assurance.

**À noter**

Si le professionnel exerce en tant que salarié, c'est à l'employeur de souscrire une telle assurance pour ses salariés, pour les actes effectués à l'occasion de leur activité professionnelle.

*Pour aller plus loin* : articles L. 271-6 et R. 271-2 du Code de la construction et de l'habitation.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Procédure de certification

Le candidat à la certification doit adresser un dossier de candidature à l'organisme certificateur qui jugera de sa recevabilité. Une fois recevable le candidat doit faire l'objet :

- d'un examen théorique en vue de vérifier que la personne possède les connaissances sur :
  - les généralités sur le bâtiment,
  - la thermique du bâtiment,
  - l'enveloppe du bâtiment,
  - les systèmes (technologies de chauffage, mise en place des énergies renouvelables, etc.),
  - les textes réglementaires ;
- d'un examen pratique portant sur une mise en situation du candidat et ayant pour but de vérifier qu'il est capable d'évaluer la consommation énergétique d'un bâtiment et d'élaborer un diagnostic de performance énergique en tenant compte des particularités du cas traité.

La certification attribuée au professionnel est valable pendant cinq ans. À l'issue de cette période il doit procéder à son renouvellement.

**À noter**

Un professionnel ne peut être titulaire que d'une seule certification et doit à ce titre fournir une déclaration sur l'honneur attestant qu'il n'en possède aucune autre.

*Pour aller plus loin* : article R. 134-4 du Code de la construction et de l'habitation ; arrêté du 16 octobre 2006 précité.

### b. Procédure de surveillance

Le professionnel certifié fait l'objet d'une surveillance par l'organisme certificateur et doit lui fournir :

- l'état des réclamations et plaintes à son encontre au cours de son activité ;
- l'ensemble des rapports établis ainsi que les informations concernant la mission effectuée (date, locaux, type de mission, et les informations relatives à la classe énergétique des locaux).

**À noter**

Ces informations peuvent lui être demandées lors de la procédure de renouvellement de sa certification.

*Pour aller plus loin* : arrêté du 16 octobre 2006 précité ; article R. 271-1 du Code de l'habitation et de la construction.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne.

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).