﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP085" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construction" -->
<!-- var(title)="Building inspector" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construction" -->
<!-- var(title-short)="building-inspector" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/construction/building-inspector.html" -->
<!-- var(last-update)="2020-04-15 17:20:45" -->
<!-- var(url-name)="building-inspector" -->
<!-- var(translation)="Auto" -->


Building inspector
====================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:45<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The technical controller's mission is to prevent any technical hazards that might occur during the construction of a work.

It will ensure the soundness of the structures, the safety of the people who will occupy it, compliance with regulations on people with reduced mobility and energy performance.

**Please note**

Technical control is mandatory for works referred to in Section R. 111-38 of the Building and Housing Code.

*For further information*: Article L. 111-23 of the Building and Housing Code;[Decree 99-443 of May 28, 1999](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000561661&dateTexte=20180416) general technical clauses applicable to public contracts for technical control.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of technical controller of construction, the professional must be:

- professionally qualified;
- ministerial approval.

*For further information*: Articles L. 111-25 and R. 111-32-2 of the Building and Housing Code.

#### Training

To be recognized as a professionally qualified person, the person must justify:

- for operational staff and engineers:- either have a post-secondary degree in building or civil engineering that warrants at least four years of study, and have at least three years of practical experience in the design, implementation, technical control or expertise of Buildings
  - or have six years of practical experience in the field;
- for mission execution staff:- either hold a high school certificate in the intended field of activity, and a practice of at least three years in the design, construction, technical control or expertise of constructions,
  - or have six years of practical experience in this field.

Once the professional fulfils these conditions, in order to practise as a technical controller of construction, he must apply for ministerial approval (see infra "5°. a. Application for approval").

*For further information*: Article R. 111-32-2 of the Building and Housing Code.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA), acting as a technical controller of construction, may carry out the same activity on a temporary and casual basis France.

In order to do so, the person must make a prior declaration to the Minister responsible for construction prior to his first performance (see infra "5°. b. Pre-declaration for EU or EEA nationals for temporary and casual exercise").

In addition, where neither access to the activity nor its exercise is regulated in that state, the national must justify having engaged in this activity for at least one year in the ten years prior to his first benefit.

*For further information*: Article L. 111-25 of the Building and Housing Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA Member State, legally established and acting as a technical controller of construction, may carry out the same activity on a permanent basis in France.

In order to do so, the person must have a certificate of competency or a training certificate issued by an EU or EEA Member State regulating the activity of technical controller of construction and attesting to a level of qualification at the less equal to the level immediately lower than that required for a French national (see above "2 degrees). a. Training").

Once the national fulfils these conditions, he must apply for approval from the Minister responsible for the construction, as well as the French national (see infra "5°. a. Application for approval").

In the event of substantial differences between his professional qualifications and those required in France, the national will be auditioned by the[Accreditation Commission](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=67FE5CB5D0712DFD0DD7565EC7F5A972.tplgfr36s_1?idArticle=LEGIARTI000020740035&cidTexte=LEGITEXT000006074096&dateTexte=20091007) and will have to demonstrate his skills and knowledge in the construction field.

*For further information*: Article R. 111-32-2 of the Building and Housing Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Responsibility**

The construction technical controller is subject to a presumption of full liability for all damages that will compromise the strength of the structure or render it unsuitable for its intended purpose.

*For further information*: Article L. 111-13 of the Building and Housing Code.

**Incompatibilities**

Technical controller activity is incompatible with the design, execution or work expertise activities. In addition, the technical controller must act impartially and not interfere with the independence of those carrying out the above activities.

*For further information*: Article R. 111-31 of the Building and Housing Code.

4°. Insurance
---------------------------------

As part of his mission and because of his full responsibility, the technical controller will have to take out 10-year liability insurance.

*For further information*: Article L. 111-24 of the Building and Housing Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for approval

**Competent authority**

The Minister responsible for construction is responsible for issuing the licence, which has a renewable validity of five years.

**Supporting documents**

The application for approval is made by filing a file that includes:

- The applicant's marital status and the address of his or her home;
- the justification of the theoretical competence and practical experience of the management staff, the internal organisation of the technical management, the rules for assistance to operational services effectively responsible for monitoring and the criteria Hiring or assigning officers
- The commitment that the comptroller will act impartially and independently;
- The comptroller's commitment to the attention of the administration will be made to any changes to the information he has given in order to obtain accreditation;
- If so, all the approvals it has obtained previously, in the construction sector;
- the extent of the approval required according to the building category or categories of the structure, and the nature or extent of the vagaries that may result.

**Procedure**

The file will be submitted to the Accreditation Commission, which will ensure compliance with the exhibits. She will hear from the applicant before deliberating on what to do with her application. When it gives a favourable opinion, it will notify the Minister who will issue the approval within six months of receiving the full file. The silence kept within this time will be worth refusing the application.

*For further information*: Articles R. 111-29 and R. 111-32 of the Building and Housing Code; Appendix III of the[decreed november 26, 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021344640&dateTexte=20180416) setting out the practical ways in which the technical controller activity can be accessed;[link to the secretariat of the Technical Controllers Accreditation Commission](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### b. Pre-declaration for EU or EEA national for temporary and casual exercise

**Competent authority**

The Minister responsible for construction is responsible for deciding on the prior declaration of activity.

**Supporting documents**

This request can be sent electronically or by mail, and takes the form of a folder with the following supporting documents:

- The marital status of the national and the address of his or her home;
- a certificate justifying that it is legally established in an EU or EEA state and that it does not incur a ban on practising;
- any documents justifying his professional qualifications;
- any document justifying that he has been engaged in this activity for at least two years in the last ten years as long as the EU or EEA State in which it is established does not regulate this activity;
- The commitment that the national will act impartially and independently;
- The nature of the performance he plans to perform and its start and end date;
- a certificate of liability insurance tailored to the benefit he plans to make.

**Procedure**

The Minister responsible for construction will have one month to verify the file and authorize the delivery, after favourable advice from the Accreditation Commission. In the event of substantial differences between the national's professional qualifications and those required in France, the Minister may ask the national to appear before the commission and demonstrate his knowledge and skills construction.

The silence of the competent authority after receipt of the file, within one month, will be worth accepting the application.

*For further information*: Articles R. 111-29-1 and R. 111-32-1 of the Building and Housing Code;[link to the secretariat of the Technical Controllers Accreditation Commission](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

#### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

Six degrees. Reference texts
----------------------------

- Articles L. 111-23 to L. 111-26 of the Building and Housing Code;
- Articles R. 111-29 to R. 111-42 of the Building and Housing Code;
- Order of 26 November 2009 setting out the practical terms of access to the exercise of the technical controller activity;
- Decree 99-443 of 28 May 1999 relating to the general technical clauses applicable to public technical control contracts;
- Standard NF P 03-100.

