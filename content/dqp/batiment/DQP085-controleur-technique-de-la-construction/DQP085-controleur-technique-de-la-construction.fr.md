﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP085" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Bâtiment" -->
<!-- var(title)="Contrôleur technique de la construction" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="batiment" -->
<!-- var(title-short)="controleur-technique-de-la-construction" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/batiment/controleur-technique-de-la-construction.html" -->
<!-- var(last-update)="2020-04-15 17:20:45" -->
<!-- var(url-name)="controleur-technique-de-la-construction" -->
<!-- var(translation)="None" -->

# Contrôleur technique de la construction

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:45<!-- end-var -->

## 1°. Définition de l’activité

Le contrôleur technique a pour mission de prévenir tous les aléas techniques qui pourraient intervenir lors de la réalisation d'un ouvrage.

Il veillera à la solidité des ouvrages, à la sécurité des personnes qui l'occuperont, au respect de la réglementation relative aux personnes à mobilité réduite et aux performances énergétiques.

**À noter**

Le contrôle technique est obligatoire pour les ouvrages mentionnés à l'article R. 111-38 du Code de la construction et de l'habitation.

*Pour aller plus loin* : article L. 111-23 du Code de la construction et de l'habitation ; décret n° 99-443 du 28 mai 1999 relatif au cahier des clauses techniques générales applicables aux marchés publics de contrôle technique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de contrôleur technique de la construction, le professionnel doit être :

- qualifié professionnellement ;
- titulaire d'un agrément ministériel.

*Pour aller plus loin* : articles L. 111-25 et R. 111-32-2 du Code de la construction et de l'habitation.

#### Formation

Pour être reconnu comme étant qualifié professionnellement, l'intéressé doit justifier :

- pour le personnel d'encadrement opérationnel et les ingénieurs :
  - soit être titulaire d'un diplôme de niveau post-secondaire en bâtiment ou génie civil justifiant au moins quatre années d'études, et avoir une expérience pratique d'au moins trois ans dans la conception, la réalisation, le contrôle technique ou l’expertise de constructions,
  - soit avoir une expérience pratique de six ans dans le domaine ;
- pour le personnel d'exécution des missions :
  - soit être titulaire d'un certificat d'études secondaire dans le domaine d'activité envisagé, et une pratique d'au moins trois ans dans la conception, la réalisation, le contrôle technique ou l’expertise de constructions,
  - soit avoir une expérience pratique de six ans dans ce domaine.

Dès lors qu'il remplit ces conditions, le professionnel doit, pour exercer la profession de contrôleur technique de la construction, solliciter un agrément ministériel (cf. infra « 5°. a. Demande en vue d'obtenir un agrément »).

*Pour aller plus loin* : article R. 111-32-2 du Code de la construction et de l'habitation.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), exerçant l'activité de contrôleur technique de la construction, peut exercer à titre temporaire et occasionnel, la même activité en France.

Pour cela, l'intéressé doit effectuer avant sa première prestation, une déclaration préalable auprès du ministre chargé de la construction (cf. infra « 5°. b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel »).

En outre, lorsque ni l'accès à l'activité ni son exercice ne sont réglementés dans cet État, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an au cours des dix années précédant sa première prestation.

*Pour aller plus loin* : article L. 111-25 du Code de la construction et de l'habitation.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'UE ou de l'EEE, légalement établi et exerçant l'activité de contrôleur technique de la construction, peut exercer à titre permanent la même activité en France.

Pour cela, l'intéressé doit être titulaire d'une attestation de compétences ou d'un titre de formation délivré par un État membre de l'UE ou de l'EEE réglementant l'activité de contrôleur technique de la construction et attestant d'un niveau de qualification au moins égal au niveau immédiatement inférieur à celui exigé pour un ressortissant français (cf. supra « 2°. a. Formation »).

Dès lors qu'il remplit ces conditions, le ressortissant doit solliciter un agrément auprès du ministre chargé de la construction, au même titre que le ressortissant français (cf. infra « 5°. a. Demande en vue d'obtenir un agrément »).

En cas de différences substantielles entre ses qualifications professionnelles et celles requises en France, le ressortissant sera auditionné par la [commission d'agrément](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=67FE5CB5D0712DFD0DD7565EC7F5A972.tplgfr36s_1?idArticle=LEGIARTI000020740035&cidTexte=LEGITEXT000006074096&dateTexte=20091007) et devra démontrer ses compétences et ses connaissances dans le domaine de la construction.

*Pour aller plus loin* : article R. 111-32-2 du Code de la construction et de l'habitation.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Responsabilité

Le contrôleur technique de la construction est soumis à une présomption de responsabilité de plein droit pour l'ensemble des dommages qui compromettront la solidité de l'ouvrage ou le rendront impropres à sa destination.

*Pour aller plus loin* : article L. 111-13 du Code de la construction et de l'habitation.

### Incompatibilités

L'activité de contrôleur technique est incompatible avec les activités de conception, d'exécution ou d'expertise d'ouvrage. En outre, le contrôleur technique devra agir avec impartialité et ne pas porter atteinte à l'indépendance des personnes exerçant les activités citées ci-avant.

*Pour aller plus loin* : article R. 111-31 du Code de la construction et de l'habitation.

## 4°. Assurances

Dans le cadre de sa mission et en raison de sa responsabilité de plein droit, le contrôleur technique devra souscrire une assurance de responsabilité décennale.

*Pour aller plus loin* : article L. 111-24 du Code de la construction et de l'habitation.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir un agrément

#### Autorité compétente

Le ministre chargé de la construction est compétent pour délivrer l'agrément dont la durée de validité est de cinq ans renouvelable.

#### Pièces justificatives

La demande d'agrément se fait par le dépôt d'un dossier comprenant les pièces suivantes :

- l'état civil du demandeur ainsi que l'adresse de son domicile ;
- la justification de la compétence théorique et de l'expérience pratique du personnel de direction, l'organisation interne de la direction technique, les règles d'assistance aux services opérationnels chargés effectivement du contrôle et les critères d'embauche ou d'affectation des agents ;
- l'engagement que le contrôleur agira avec impartialité et indépendance ;
- l'engagement que le contrôleur portera à l'attention de l'administration toute modification des renseignements qu'il a donné pour l'obtention de l'agrément ;
- le cas échéant, tous les agréments qu'il a obtenu précédemment, dans le domaine de la construction ;
- l'étendue de l'agrément demandé selon la ou les catégories de construction d'ouvrages, et la nature ou l'importance des aléas qui pourront en découler.

#### Procédure

Le dossier sera soumis à la commission d'agrément qui s'assurera de la conformité des pièces. Elle auditionnera le demandeur avant de délibérer sur les suites à donner à sa demande. Lorsqu'elle rend un avis favorable, elle en informera le ministre qui délivrera l'agrément dans un délai de six mois à compter de la réception du dossier complet. Le silence gardé dans ce délai vaudra refus de la demande.

*Pour aller plus loin* : articles R. 111-29 et R. 111-32 du Code de la construction et de l'habitation ; annexe III de l'arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique ; [lien vers le secrétariat de la commission d’agrément des contrôleurs techniques](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel

#### Autorité compétente

Le ministre chargé de la construction est compétent pour se prononcer sur la déclaration préalable d'activité.

#### Pièces justificatives

Cette demande peut être envoyée sous format électronique ou par courrier, et prend la forme d'un dossier comportant les pièces justificatives suivantes :

- l'état civil du ressortissant ainsi que l'adresse de son domicile ;
- une attestation justifiant qu'il est légalement établi dans un État de l'UE ou de l'EEE et qu'il n'encourt pas d'interdiction d'exercer ;
- tout document justifiant de ses qualifications professionnelles ;
- tout document justifiant qu'il a exercé cette activité pendant au moins deux ans au cours des dix dernières années dès lors que l’État de l'UE ou de l'EEE dans lequel il est établi ne réglemente pas cette activité ;
- l'engagement que le ressortissant agira avec impartialité et indépendance ;
- la nature de la prestation qu'il envisage de faire et sa date de début et de fin ;
- une attestation d'assurance de responsabilité adaptée à la prestation qu'il envisage de faire.

#### Procédure

Le ministre chargé de la construction disposera d'un délai d'un mois pour vérifier le dossier et autoriser la prestation, après avis favorable de la commission d'agrément. En cas de différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France, le ministre pourra demander au ressortissant de se présenter devant la commission et démontrer ses connaissances et ses compétences dans le domaine de la construction.

Le silence de l'autorité compétente après réception du dossier, dans un délai d'un mois, vaudra acceptation de la demande.

*Pour aller plus loin* : articles R. 111-29-1 et R. 111-32-1 du Code de la construction et de l'habitation ; [lien vers le secrétariat de la commission d’agrément des contrôleurs techniques](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

## 6°. Textes de référence

- Articles L. 111-23 à L. 111-26 du Code de la construction et de l’habitation ;
- Articles R. 111-29 à R. 111-42 du Code de la construction et de l’habitation ;
- Arrêté du 26 novembre 2009 fixant les modalités pratiques d’accès à l’exercice de l’activité de contrôleur technique ;
- Décret n° 99-443 du 28 mai 1999 relatif au cahier des clauses techniques générales applicables aux marchés public de contrôle technique ;
- Norme NF P 03-100.