﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP040" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Bâtiment" -->
<!-- var(title)="Architecte (droits acquis)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="batiment" -->
<!-- var(title-short)="architecte-droits-acquis" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/batiment/architecte-droits-acquis.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="architecte-droits-acquis" -->
<!-- var(translation)="None" -->

# Architecte (droits acquis)

## 1°. Définition de l’activité

L'architecte est un professionnel chargé des différentes phases de la conception et de la réalisation des travaux d'un ouvrage.

En premier lieu, il est tenu de réaliser une enquête de faisabilité du terrain avant de dessiner les premiers plans du futur bâtiment. Il se chargera d'obtenir le permis de construire et négociera les prix avec les différents entrepreneurs qui travailleront sur le chantier. Tout au long du projet, il devra tenir compte de la réglementation en matière d'urbanisme, de contraintes juridiques et techniques, ainsi que des exigences du client sur le budget et les délais notamment.

Une fois que les plans ont été conçus, l'architecte coordonne les équipes chargées de la réalisation du chantier jusqu'à la livraison de l'ouvrage.

## 2°. Qualifications professionnelles

#### Droits acquis

Parmi les activités décrites, seul l’établissement du projet architectural faisant l’objet de la demande de permis de construire pour certaines constructions est réservée à des architectes à raison de conditions ayant trait à leurs qualifications professionnelles :

- toute construction soumise à demande de permis de construire entreprise par une personne morale ;
- toute construction, à usage autre qu’agricole, soumise à demande de permis de construire excédant une superficie de 150 m² pour les personnes physiques ;
- toute construction soumise à demande de permis de construire excédant une superficie de 800 m² pour les exploitations agricoles quelle que soit leur nature juridique ou pour les constructions nécessaires au stockage et à l'entretien de matériel agricole par les coopératives d'utilisation de matériel agricole.

Le projet architectural définit, par des plans et documents écrits, l'implantation des bâtiments, leur composition, leur organisation et l'expression de leur volume ainsi que le choix des matériaux et des couleurs. Il précise, par des documents graphiques ou photographiques, l'insertion dans l'environnement et l'impact visuel des bâtiments ainsi que le traitement de leurs accès et de leurs abords.

L'exercice de cette activité en France est réservée aux personnes inscrites au tableau régional des architectes.

L’inscription au tableau de l’Ordre des architectes peut être demandée par un ressortissant d’un État membre de l'Union européenne (UE) ou d’un État partie à l'accord sur l'Espace économique européen (EEE), titulaire d'un diplôme ou d'un titre de formation délivré par un État membre de l’UE ou de l’EEE et reconnu par l’État français.

Lorsque le diplôme n’est pas reconnu par l’État français, ou que les demandeurs sont titulaires d’un diplôme ou titre de formation d’un État tiers, ils doivent obtenir une reconnaissance de leurs qualifications professionnelles avant de pouvoir demander une inscription au tableau de l’Ordre des architectes (se reporter à la fiche « [Architecte](https://www.guichet-qualifications.fr/fr/dqp/batiment/architecte.html) »).

## 3°. Diplômes ou titres de formation délivrés par un autre État de l'UE ou de l'EEE

Sont reconnus en France et bénéficient des mêmes effets que les diplômes nationaux d'architecte les diplômes ou titres de formation délivrés par un autre État membre dont la liste est fixée au point 5.7 de l'annexe V de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles. Certaines attestations permettent également une reconnaissance automatique des qualifications professionnelles du demandeur pour une inscription au tableau de l’Ordre des architectes. Ces attestations sont listées à l’article 49 de la directive de 2005 (se reporter à la fiche « [Architecte](https://www.guichet-qualifications.fr/fr/dqp/batiment/architecte.html) »).

*Pour aller plus loin* : articles 21 et 49 de la [directive 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32005L0036&from=FR) du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles.

Pour connaître les modalités d'exercice de la profession d'architecte en France, il est conseillé de se reporter à la fiche « [Architecte](https://www.guichet-qualifications.fr/fr/dqp/batiment/architecte.html) ».