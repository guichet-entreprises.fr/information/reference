﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP040" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construction" -->
<!-- var(title)="Architect (acquired rights)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construction" -->
<!-- var(title-short)="architect-acquired-rights" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/construction/architect-acquired-rights.html" -->
<!-- var(last-update)="2020-04-15 17:20:44" -->
<!-- var(url-name)="architect-acquired-rights" -->
<!-- var(translation)="Auto" -->


Architect (acquired rights)
===========================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:44<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The architect is a professional in charge of the various phases of the design and construction of a work.

First, it is required to carry out a feasibility survey of the land before drawing the first plans of the future building. He will obtain the building permit and negotiate prices with the various contractors who will work on the site. Throughout the project, it will have to take into account planning regulations, legal and technical constraints, as well as the client's budget and deadlines requirements.

Once the plans have been designed, the architect coordinates the teams responsible for the construction of the construction site until the delivery of the work.

2°. Professional qualifications
----------------------------------------

#### Acquired rights

The practice of the profession of architect in France is reserved for those registered in the regional table of architects.

However, professionals with a diploma or training degree issued by another Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement may practise as an architect in France French nationals.

#### Diplomas or training documents issued by another EU or EEA state

The diplomas or training documents issued by another Member State, listed in Appendix VI of the European Parliament and Council 7 September 2005 on the recognition of professional qualifications, are recognised in France and have the same effects as national architectural diplomas.

*To go further* Article 49 of the[Directive 2005/36/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32005L0036&from=FR) Parliament and the Council of 7 September 2005 on the recognition of professional qualifications.

To find out how to practice the architectural profession in France, it is advisable to refer to the "Architect" sheet.

