﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP039" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construction" -->
<!-- var(title)="Architect" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construction" -->
<!-- var(title-short)="architect" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/construction/architect.html" -->
<!-- var(last-update)="2020-04-15 17:20:44" -->
<!-- var(url-name)="architect" -->
<!-- var(translation)="Auto" -->


Architect
=========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:44<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The architect is responsible for the various phases of the design and construction of the work of a work.

First, it is required to carry out a feasibility survey of the land before drawing the first plans of the future building. He will obtain the building permit and negotiate prices with the various contractors who will work on the site. Throughout the project, it will have to take into account planning regulations, legal and technical constraints, as well as the client's budget and deadlines requirements.

Once the plans have been designed, the architect coordinates the teams responsible for the construction of the construction site until the delivery of the work.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of the profession of architect is reserved for those registered in the regional table of architects. Registration is open to holders of the State Architect Diploma who have completed an additional year of training leading to the certification to practice the master's degree in its own name (HMONP).

*To go further* Article 9 of Law 77-2 of January 3, 1977 on architecture.

#### Training

Architectural training is delivered at 20 national higher schools of architecture throughout France, at the National Higher Institute of Applied Sciences (INSA) in Strasbourg, and at the Special School of Architecture (ESA) in Paris.

It is accessible after graduation and lasts five years. Access is made after reviewing the candidate's file, complete with an interview with possibly tests.

The studies consist of two cycles:

- a three-year undergraduate degree, which leads to the Diploma of Architecture Studies (DEEA);
- a second cycle, in two years (master level), which leads to the State Diploma of Architect (DEA).

#### Costs associated with qualification

Training costs range from 300 to 500 euros per year for public schools. The ESA's costs are about 9,000 euros per year. For more information, it is advisable to get closer to the different schools that provide the training.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) or European Economic Area (EEA) state legally practising as an architect in one of these states may use his or her professional title in France, either temporarily or occasionally.

He will have to apply for it, prior to his performance, by written declaration to the regional council of the Order of Architects (see infra "5°. a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)").

In the event of substantial differences between his professional qualifications and the training required in France, the council may submit him to an aptitude test which consists of a thirty-minute oral test. This will cover all or part of the materials covered by the[Article 9](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=FBD3A253B7755AFB8CE594DDFEF3610E.tplgfr34s_3?idArticle=LEGIARTI000021673054&cidTexte=LEGITEXT000021673028&dateTexte=20180214) of the decree of 17 December 2009 on the modalities for the recognition of professional qualifications for the practice of the profession of architect. Depending on the subject, an architectural project may be asked of the national to assess his skills.

*To go further* Article 10-1 of Law 77-2 of January 3, 1977 on architecture; Articles 10 to 14 of Decree No. 2009-1490 of December 2, 2009 on the recognition of professional qualifications for the practice of the profession of architect.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state may settle in France to practice permanently if he or she holds:

- the state diploma of architect or another French diploma of architect recognized by that state, and the HMONP;
- a diploma, certificate or other foreign title recognized by that state allowing the practice of the profession of architect and recognized by the state.

In both cases, he will be able to apply directly for his registration in the regional list of architects with the regional council of the Order of Architects (see infra "5°. c. Request registration on the regional architect's list for the EU or EEA national for a permanent exercise (LE)).

Can also settle in France permanently, the EU or EEA national who:

- holds a training certificate issued by a third state but recognised by an EU or EEA state and has allowed the profession of architect to be practised for at least three years;
- was recognized as qualified by the Minister for Culture, after a review of all his knowledge, qualifications and professional experience;
- was recognized as qualified by the Minister for Culture after presenting professional references that attest that he distinguished himself by the quality of his architectural achievements, after advice from a national commission.

Once the national fulfils one of these conditions, he or she will be able to apply for recognition of his professional qualifications before applying for registration on the board (see below "5o). b. If necessary, request prior recognition of his professional qualifications for the EU or EEA national for inclusion in the regional architect's list").

If there are substantial differences between his training and the professional qualifications required in France, the national may be subjected to an aptitude test organised within six months of his decision.

*To go further* Article 10 of Law 77-2 of January 3, 1977 on architecture; Articles 1 to 9 of Decree No. 2009-1490 of December 2, 2009 on the recognition of professional qualifications for the practice of the architectural profession.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The provisions of the Code of Ethics are imposed on all architects practising in France.

As such, the architect must respect the principles of respect for professional confidentiality, competition and conflicts of interest.

For more information, it is advisable to refer to the[order of architects website](https://www.architectes.org/code-de-deontologie-des-architectes).

*To go further* Sections 18 and 19 of Law 77-2 of January 3, 1977 on architecture.

4°. Insurance
---------------------------------

The architect practising on a liberal basis must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*To go further* Article 16 of Law 77-2 of January 3, 1977 on architecture.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)

**Competent authority**

The regional council of the Order of Architects of the place of execution of the service is competent to decide on the application for prior declaration.

**Supporting documents**

The request for prior declaration of activity is made by filing a file that includes:

- a statement mentioning the national's intention to provide services in France;
- A certificate of professional civil insurance of the national less than three months old;
- A copy of the training titles
- A copy of the certificate certifying that the national is established in an EU or EEA state and does not fall under a ban on practising;
- a copy of a valid ID.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The competent authority has one month from the receipt of the declaration to decide on the request for declaration. When the applicant is required to pass an aptitude test, the regional council will have an additional one month to decide.

*To go further* Articles 14 to 17 of the December 17, 2009 decree on the recognition of professional qualifications for the practice of the architectural profession.

### b. If necessary, request prior recognition of his professional qualifications for the EU or EEA national in view of his registration in the regional architect's list

**Competent authority**

The Minister for Culture is responsible for deciding on applications for recognition of the national's professional qualifications.

**Supporting documents**

The application for recognition must be addressed to the competent authority and submitted by file, in two copies, containing the following documents:

- A copy of a valid ID
- A copy of the training titles
- If applicable, a copy of the detailed description of the curriculum followed and its hourly volume, as well as a copy of the authorisation to carry the title of architect;
- If applicable, a copy of the training title issued by a third state but recognised by an EU or EEA state, as well as a copy:- either the certification issued by the EU State or the EEA attesting to the practice of the architectural profession for at least three years in that state,
  - either any document issued by the EU state or the EEA attesting to the practice of the architectural profession;
- a description of the training and work experience relevant to the exercise of the activity.

In the case of the national who would have distinguished himself by the quality of his achievements in the field of architecture, the file will include:

- A resume
- A letter outlining his motivations
- a collection presenting a diverse selection of studies and projects carried out showing their exterior appearance, interior design and insertion into the site, illustrated with photographs, plans and cuts as well as sketches and drawings;
- A list of references to the work and studies carried out;
- A copy of articles and publications devoted to the achievements presented or their author;
- Certificates and recommendations from employers and customers
- a certificate issued by the EU or EEA State certifying that the applicant's activities are architectural.

**Procedure**

The competent authority will acknowledge receipt of the file within one month. Upon receipt of the full file, the Minister responsible for culture will have four months to make his decision, after advice from the National Council of the Order of Architects.

In the event of substantial differences between the training and the qualifications required in France, he may decide to subject the national to the same aptitude test as that required in the case of free provision of services (see above "2. b. EU or EEA nationals: for a temporary and casual exercise (Freedom to provide services)").

*To go further* Articles 3 to 7 of Decree No. 2009-1490 of 2 December 2009 on the recognition of professional qualifications for the practice of the architectural profession; Articles 2 to 13 of the Decree of 17 December 2009 on the modalities of recognition of professional qualifications for the practice of the profession of architect.

### c. Request registration on the regional architect's list for the EU or EEA national in view of a permanent exercise (LE)

**Competent authority**

The Regional Council of the College of Architects is responsible for deciding on applications to register on the Order's board.

**Supporting documents**

The request is made by sending a file in two copies with the following supporting documents:

- A copy of a valid ID
- A copy of the training title
- a certificate of the national's professional civil insurance, less than three months old;
- an extract from the criminal record.

**Procedure**

The regional council will acknowledge receipt of the file within one month. The council's silence within two months will be worth the decision to reject the application.

*To go further* Article 1 of the Decree of 17 December 2009 on the terms of recognition of professional qualifications for the practice of the architectural profession.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

