﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP039" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Bâtiment" -->
<!-- var(title)="Architecte" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="batiment" -->
<!-- var(title-short)="architecte" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/batiment/architecte.html" -->
<!-- var(last-update)="2020-04-15 17:20:44" -->
<!-- var(url-name)="architecte" -->
<!-- var(translation)="None" -->

# Architecte

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:44<!-- end-var -->

## 1°. Définition de l'activité

L'architecte est chargé des différentes phases de la conception et de la réalisation des travaux d'un ouvrage.

En premier lieu, il est tenu de réaliser une enquête de faisabilité du terrain avant de dessiner les premiers plans du futur bâtiment. Il se chargera d'obtenir le permis de construire et négociera les prix avec les différents entrepreneurs qui travailleront sur le chantier. Tout au long du projet, il devra tenir compte de la réglementation en matière d'urbanisme, de contraintes juridiques et techniques, ainsi que des exigences du client sur le budget et les délais notamment.

Une fois que les plans ont été conçus, l'architecte coordonne les équipes chargées de la réalisation du chantier jusqu'à la livraison de l'ouvrage.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Parmi les activités décrites, seul l’établissement du projet architectural faisant l’objet de la demande de permis de construire pour certaines constructions est réservée à des architectes à raison de conditions ayant trait à leurs qualifications professionnelles : 

- toute construction soumise à demande de permis de construire entreprise par une personne morale ;
- toute construction, à usage autre qu’agricole, soumise à demande de permis de construire excédant une superficie de 150 m² pour les personnes physiques ;
- toute construction soumise à demande de permis de construire excédant une superficie de 800 m² pour les exploitations agricoles quelle que soit leur nature juridique ou pour les constructions nécessaires au stockage et à l'entretien de matériel agricole par les coopératives d'utilisation de matériel agricole.

Le projet architectural définit, par des plans et documents écrits, l'implantation des bâtiments, leur composition, leur organisation et l'expression de leur volume ainsi que le choix des matériaux et des couleurs. Il précise, par des documents graphiques ou photographiques, l'insertion dans l'environnement et l'impact visuel des bâtiments ainsi que le traitement de leurs accès et de leurs abords.

L'exercice de cette activité est réservé aux personnes inscrites au tableau régional des architectes. L'inscription est ouverte aux titulaires du diplôme d’État d'architecte ayant suivi une année supplémentaire de formation menant à l'obtention de l'habilitation à exercer la maîtrise d’œuvre en son nom propre (HMONP), du diplôme d'architecte délivré par le gouvernement (DPLG), du diplôme d’architecte ESA (délivré par l’École spéciale d’architecture de Paris à partir de 1988), du diplôme d’architecte DESA (délivré par l’École spéciale d’architecture de Paris avant 1988), du diplôme d’architecte ENSAIS (délivré par l’École nationale supérieure des arts et industries de Strasbourg-section architecture).

*Pour aller plus loin* : article 9 et le 1° de la loi n° 77-2 du 3 janvier 1977 sur l'architecture ; article 1 du décret n° 2009-1490 du 2 décembre 2009 relatif à la reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte ; article 1 de l’arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte.

#### Formation

La formation d'architecte est délivrée dans 20 écoles nationales supérieures d'architecture partout en France, à l'Institut national supérieur des sciences appliquées (INSA) de Strasbourg, et à l’École spéciale d'architecture (ESA) de Paris.

Elle est accessible après l'obtention du baccalauréat et dure cinq ans. Son accès se fait après examen du dossier du candidat, complété d'un entretien avec éventuellement des tests.

Les études sont composées de deux cycles :

- un premier cycle en trois ans (niveau licence), qui mène au diplôme d'études en architecture (DEEA) ;
- un second cycle, en deux ans (niveau master), qui mène au diplôme d’État d'architecte (DEA).

#### Coûts associés à la qualification

Les frais de formation varient de 300 € à 500 € par an pour les écoles publiques. Ceux de l'ESA sont d'environ 9 000 € par an. Pour plus d'informations, il est conseillé de se rapprocher des différentes écoles qui dispensent la formation.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE) exerçant légalement l’activité d'architecte dans l’un de ces États peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration écrite auprès du conseil régional de l'Ordre des architectes (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »). Cette déclaration est renouvelée une fois par an si le prestataire envisage d'exercer son activité professionnelle de façon occasionnelle au cours de l'année concernée ou en cas de changement matériel dans sa situation. Elle est accompagnée notamment des informations relatives aux couvertures d'assurance et autres moyens de protection personnelle ou collective.

Dans le cas où le prestataire ne bénéficie pas de la reconnaissance automatique des diplômes, le conseil régional de l'Ordre des architectes procède à la vérification des qualifications professionnelles déclarées. En cas de différences substantielles entre ses qualifications professionnelles et la formation exigée en France, le conseil pourra le soumettre à une épreuve d'aptitude qui consiste en une épreuve orale de trente minutes. Celle-ci portera sur tout ou partie des matières visées à l'article 9 de l'arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte. Selon les matières, un projet architectural pourra être demandé au ressortissant pour évaluer ses compétences.

*Pour aller plus loin* : article 10-1 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture ; articles 10 à 14 du décret n° 2009-1490 du 2 décembre 2009 relatif à la reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte ; articles 14 à 17 de l’arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente s'il est titulaire :

- du diplôme d’État d'architecte accompagné de l'HMONP, du DPLG, du diplôme d’architecte ESA, du diplôme d’architecte DESA et du diplôme d’architecte ENSAIS ;
- d'un diplôme, certificat ou autre titre étranger reconnu par cet État permettant l'exercice de la profession d'architecte et reconnu par l’État. Ces diplômes sont listés au point 5.7 de l’annexe V et à l’annexe VI de la directive de 2005/36/CE du parlement européen et du conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles modifiée ;
- d’une attestation de l'État membre d'origine indiquant qu’il a été autorisé à utiliser le titre professionnel d'architecte dans cet État membre et qu’il a exercé de manière effective et licite la profession concernée pendant au moins trois années consécutives au cours des cinq années précédant la délivrance de l'attestation. Cependant, ces attestations doivent certifier que leur titulaire a reçu l’autorisation de porter le titre professionnel d’architecte au plus tard aux dates suivantes : 
  - le 1er janvier 1995 pour l’Autriche, la Finlande et la Suède ;
  - le 1er mai 2004 pour la république Tchèque, l’Estonie, Chypre, la Lettonie, la Lituanie, la Hongrie, Malte, la Pologne, la Slovénie et la Slovaquie ;
  - le 1er juillet 2013 pour la Croatie ;
  - le 5 août 1987 pour les autres États membres.

Dans ces trois cas, il pourra directement demander son inscription au tableau régional des architectes auprès du conseil régional de l'Ordre des architectes (cf. infra « 5°. c. Demander son inscription au tableau régional d'architecte pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

Peut également s'établir en France de façon permanente, le ressortissant de l'UE ou de l'EEE qui :

- est titulaire d'un titre de formation délivré par un État tiers mais reconnu par un État de l'UE ou de l'EEE et ayant permis l'exercice de la profession d'architecte pendant au moins trois ans, à condition que cette expérience professionnelle soit certifiée par l'État dans lequel elle a été acquise. Cette procédure se fait au titre des dispositions de l’alinéa 1 du 2° de l’article 10 de la loi de 1977 sur l’architecture. Toutefois, si l’intéressé ne peut pas justifier son expérience professionnelle il devra faire sa demande au titre de l’alinéa 2 de du 2° de l’article 10 de cette même loi ;
- est titulaire d’un diplôme en architecture (peu importe que le diplôme soit obtenu dans un État membre de l’Union européenne ou de l’EEE ou dans un État tiers) et a été reconnu comme étant qualifié par le ministre chargé de la culture, après un examen de l'ensemble de ses connaissances, qualifications et expériences professionnelles au titre des dispositions du 3° de l’article 10 de la loi de 1977 sur l’architecture ;
- a été reconnu comme étant qualifié par le ministre chargé de la culture après avoir présenté des références professionnelles qui attestent qu'il s'est distingué par la qualité de ses réalisations architecturales, et ce, après avis du second collège d'une Commission nationale de reconnaissance des qualifications professionnelles au titre des dispositions du 4° de l’article 10 de la loi de 1977 sur l’architecture.

Concernant les demandes de reconnaissance des qualifications professionnelles au titre des 2° et 3° de l’article 10 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture, s'il existe des différences substantielles entre sa formation et les qualifications professionnelles requises en France, le ressortissant pourra être soumis à une épreuve d'aptitude organisée dans les six mois suivant la décision la prescrivant.

*Pour aller plus loin* : article 10 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture ; articles 1 à 9 du décret n° 2009-1490 du 2 décembre 2009 relatif à la reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte ; article 2 à 13 de l’arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Pour qu’un ressortissant d’un État membre de l’Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen puisse s’inscrire au tableau de l’Ordre des architectes, il doit jouir de ses droits civils et présenter les garanties de moralité nécessaires. De plus, les dispositions du Code de déontologie s'imposent à tous les architectes exerçant en France.

À ce titre, l'architecte doit notamment respecter les principes relatifs au respect du secret professionnel, à la concurrence et aux conflits d'intérêt.

L'architecte prestataire de services à titre occasionnel est soumis aux règles et procédures relatives aux conditions d'exercice de la profession, à l'usage du titre professionnel, aux règles professionnelles ou déontologiques et disciplinaires applicables à la profession, ainsi qu'aux obligations d'assurance correspondant aux prestations envisagées.

Pour plus d'informations, il est conseillé de se reporter au [site de l'Ordre des architectes](https://www.architectes.org/).

*Pour aller plus loin* : articles 10, 10-1, 18 et 19 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture.

## 4°. Assurance

L'architecte exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

Cette assurance n’est également pas obligatoire pour un architecte inscrit au tableau de l’Ordre des architecte dans la rubrique « exercice exclusif à l’étranger ou dans une collectivité d'outre-mer non soumise à la loi sur l'architecture » à condition de produire une attestation sur l’honneur par laquelle il certifie « ne pas accomplir ou faire accomplir par ses préposés d’actes professionnels à titre onéreux ou gratuit pouvant engager sa responsabilité au sens de l’article 16 de la loi du 3 janvier 1977 sur l’architecture ».

Les ressortissants européens qui souhaitent exercer la profession d’architecte de façon temporaire et occasionnelle sans une inscription au tableau de l’Ordre des architectes doivent transmettre une attestation datant de moins de trois mois prouvant qu’ils ont bien souscrit les assurances couvrant sa responsabilité civile professionnelle au regard de la législation française.

*Pour aller plus loin* : article 16 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le conseil régional de l'Ordre des architectes du lieu d'exécution de la première prestation envisagée est compétent pour se prononcer sur la demande déclaration préalable.

#### Pièces justificatives

La demande de déclaration préalable d'activité se fait par le dépôt d'un dossier comprenant les pièces suivantes :

- une déclaration mentionnant l'intention du ressortissant de faire une prestation de services en France ;
- une attestation de l'assurance civile professionnelle du ressortissant datant de moins de trois mois ;
- une copie des titres de formation ;
- une copie de l'attestation certifiant que le ressortissant est établi dans un État de l'UE ou de l'EEE et qu'il ne tombe pas sous le coup d'une interdiction d'exercer ;
- une copie d'une pièce d'identité en cours de validité.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

L'autorité compétente dispose d'un délai d'un mois à compter de la réception de la déclaration pour se décider sur la demande de prestation de services. En l’absence de décision à l’expiration de ce délai, la prestation de services peut être effectuée. 

Dans le cas où le prestataire ne bénéficie pas de la reconnaissance automatique des diplômes, le conseil régional de l'Ordre des architectes procède à la vérification des qualifications professionnelles déclarées. Le cas échéant, s’il y a une différence substantielle entre celles-ci et la formation requise en France, de nature à nuire à la sécurité publique et insusceptible d'être compensée par son expérience professionnelle ou par les connaissances, aptitudes et compétences acquises lors d'un apprentissage tout au long de la vie ayant fait l'objet, à cette fin, d'une validation en bonne et due forme par un organisme compétent, dans un État membre ou dans un pays tiers, le CROA propose à l’intéressé de se soumettre à une épreuve. Dans ce cas, un délai d’un mois à compter de la décision du conseil régional de l’Ordre de soumettre l’intéressé à une épreuve d’aptitude s’ajoute au précédent délai. En l'absence de décision à l'expiration des délais susmentionnés, la prestation de services peut être effectuée.

Cette déclaration est renouvelée une fois par an si le prestataire envisage d'exercer son activité professionnelle de façon occasionnelle au cours de l'année concernée ou en cas de changement matériel dans sa situation. Elle est accompagnée notamment des informations relatives aux couvertures d'assurance et autres moyens de protection personnelle ou collective.

*Pour aller plus loin* : articles 10 à 14 du décret n° 2009-1490 du 2 décembre 2009 relatif à la reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte ; articles 14 à 17 de l'arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte.

### b. Le cas échéant, demander une reconnaissance préalable de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vu de son inscription au tableau régional d'architecte

#### Autorité compétente

Le ministre chargé de la culture est compétent pour se prononcer sur les demandes de reconnaissances de qualifications professionnelles du ressortissant.

#### Pièces justificatives

La demande de reconnaissance doit être adressée à l'autorité compétente et transmise par dossier, en deux exemplaires.

Pour une demande de reconnaissance des qualifications professionnelles de la profession d’architecte au titre de l’alinéa 1 du 2° de l’article 10 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture, le dossier doit comporter les pièces suivantes :

- une copie de leur diplôme, certificat ou titre délivré par un État tiers qui a été reconnu dans un État membre de l'Union européenne autre que la France ou d'un autre État partie à l'accord sur l'Espace économique européen ;
- une copie de la certification délivrée par l'État membre de l'Union européenne autre que la France ou l'autre État partie à l'accord sur l'Espace économique européen, attestant de l'exercice de la profession d'architecte pendant une période minimale de trois ans et de l'expérience professionnelle acquise dans cet État ;
- un descriptif de la formation et de l'expérience professionnelle attestant des connaissances et qualifications pertinentes au regard de l'exercice de la profession d'architecte ;
- une copie d'une pièce d'identité en cours de validité.

Pour une demande de reconnaissance des qualifications professionnelles de la profession d’architecte au titre de l’alinéa 2 du 2° de l’article 10 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture, le dossier doit comporter les pièces suivantes :

- une copie de leur diplôme, certificat ou titre délivré par un État tiers qui a été reconnu dans un État membre de l'Union européenne autre que la France ou d'un autre État partie à l'accord sur l'Espace économique européen ;
- une copie de tout document délivré par l'État membre de l'Union européenne autre que la France ou de l'autre État partie à l'accord sur l'Espace économique européen, attestant de l'exercice de la profession d'architecte ;
- une copie d'une pièce d'identité en cours de validité ;
- un descriptif de la formation et de l'expérience professionnelle attestant des connaissances et qualifications pertinentes au regard de l'exercice de la profession d'architecte.

Pour une demande de reconnaissance des qualifications professionnelles de la profession d’architecte au titre du 3° de l’article 10 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture, le dossier doit comporter les pièces suivantes :

- une copie des diplômes, certificats ou autres titres ;
- une copie du descriptif détaillé du programme des études concernant l'organisation et le contenu horaire de la formation reçue ;
- un descriptif de la formation et de l'expérience professionnelle attestant des connaissances et qualifications pertinentes au regard de l'exercice de la profession d'architecte ;
- une copie d'une pièce d'identité en cours de validité ;
- un dossier d'œuvres présentant des projets architecturaux ou urbains en phase de réalisation, ou achevés, décrivant la participation personnelle du demandeur sur chaque projet et permettant de définir ses compétences d'architecte.

Pour une demande de reconnaissance des qualifications professionnelles au titre du 4° de l’article 10 de la loi de 1977 sur l’architecture, dans le cas du ressortissant qui se serait particulièrement distingué par la qualité de ses réalisations dans le domaine de l’architecture, le dossier comprendra :

- un curriculum vitae ;
- un courrier exposant ses motivations ;
- un recueil présentant un choix diversifié des études et projets réalisés faisant apparaître leur aspect extérieur, leur aménagement intérieur et leur insertion dans le site, illustré de photographies, de plans et coupes ainsi que de croquis et de dessins ;
- une liste de références des travaux et études réalisés ;
- une copie des articles et publications consacrés aux réalisations présentées ou à leur auteur ;
- les attestations et recommandations d'employeurs et de clients ;
- un certificat délivré par l’État de l'UE ou de l'EEE attestant que les activités du demandeur relèvent de l'architecture.

#### Procédure

L'autorité compétente accusera réception du dossier dans un délai d'un mois. 

Pour les demandes de reconnaissance des qualifications professionnelles au titre du 2° et 3° de l’article 10 de la loi de 1977 sur l’architecture, à réception du dossier complet, le ministre chargé de la culture aura quatre mois pour rendre sa décision, après avis du Conseil national de l'Ordre des architectes. En cas de différences substantielles entre la formation et les qualifications requises en France, il pourra décider de soumettre le ressortissant à une épreuve d'aptitude devant un jury composé de la directrice de l’architecture, d’un représentant du Conseil national de l’Ordre des architectes et d’un enseignant d’une École nationale supérieure d’architecture. La procédure et les matières proposées aux candidats sont les mêmes que celles prévues en cas de libre prestation de services (cf. supra « 2°. b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (libre prestation de services) »).

Pour la demande de reconnaissance des qualifications professionnelles au titre du 4° de l’article 10 de la loi de 1977 sur l’architecture, le ministre chargé de la culture aura deux mois, après avis du second collège de la Commission nationale de l’Ordre des architectes, pour rendre sa décision. 

*Pour aller plus loin* : articles 3 à 7 du décret n° 2009-1490 du 2 décembre 2009 relatif à la reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte ; articles 2 à 13 de l'arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte.

### c. Demander son inscription au tableau régional d'architecte pour le ressortissant de l'UE ou de l'EEE en vu d'un exercice permanent (LE)

#### Autorité compétente

Le conseil régional de l'Ordre des architectes est compétent pour se prononcer sur les demandes d'inscription au tableau de l'Ordre.

#### Pièces justificatives

La demande se fait par l'envoi d'un dossier en deux exemplaires comportant les pièces justificatives suivantes :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation reconnu par l’Etat français ou une attestation prévue par l’article 49 de la directive de 2005/36/CE modifiée ;
- le cas échéant, une décision de reconnaissance des qualifications professionnelles délivrée par le ministre chargé de culture ;
- une déclaration par laquelle le demandeur s'engage à fournir une attestation d'assurance ;
- un extrait du casier judiciaire.

#### Procédure

Le conseil régional accusera réception du dossier dans un délai d'un mois. Le silence gardé du conseil dans un délai de deux mois vaudra décision de rejet de la demande d'inscription.

*Pour aller plus loin* : article 1 de l'arrêté du 17 décembre 2009 relatif aux modalités de reconnaissance des qualifications professionnelles pour l'exercice de la profession d'architecte.

### d. Voies de recours

#### Recours contentieux contre le rejet d’une demande de reconnaissance des qualifications professionnelles 

Le silence gardé par l’Administration pendant quatre mois sur une demande de reconnaissance des qualifications professionnelles pour l’exercice de la profession d’architecte au titre des 2° et 3°de l’article 10 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture vaut décision de rejet. Ainsi, en cas de silence de l’Administration ou de décision défavorable, le demandeur peut former un recours contentieux dans un délai de deux mois auprès du tribunal administratif de Paris. 

Pour les demandes de reconnaissance des qualifications professionnelles relatives au 4° de l’article 10 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture, le silence gardé par l’Administration pendant deux mois à compter de l’avis rendu par le second collège de la Commission nationale de reconnaissance des qualifications professionnelles vaut décision de rejet. Ainsi, en cas de silence de l’Administration ou de décision défavorable, le demandeur peut former un recours contentieux dans un délai de deux mois auprès du tribunal administratif de Paris. 

#### Recours contre le refus d’une inscription au tableau de l’Ordre des architectes 

Le recours devant le ministre chargé de la culture est un préalable obligatoire au recours contentieux.

Le silence gardé par le conseil régional de l’Ordre des architectes pendant 2 mois sur une demande d’inscription au tableau de l’Ordre des architectes vaut décision de rejet. Ainsi, en cas de silence du conseil régional de l’Ordre des architectes ou de décision défavorable, le demandeur peut former un recours dans un délai de deux mois auprès du ministre chargé de la culture.

Le ministre chargé de la culture doit se prononcer dans un délai de deux mois, silence valant rejet. Ainsi, en cas de silence de celui-ci ou de décision défavorable, le demandeur peut former un recours contentieux dans un délai de deux mois auprès du tribunal administratif de Paris. 

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes. Il ne délivre aucune équivalence de diplôme pour la profession d’architecte.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).