﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP216" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Maritime sector" -->
<!-- var(title)="Maritime pilot" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="maritime-sector" -->
<!-- var(title-short)="maritime-pilot" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/maritime-sector/maritime-pilot.html" -->
<!-- var(last-update)="2020-04-15 17:22:18" -->
<!-- var(url-name)="maritime-pilot" -->
<!-- var(translation)="Auto" -->


Maritime pilot
============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:18<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The marine pilot is a professional whose mission is to assist the captain of a ship on his exit and entry manoeuvres in ports, estuaries, streams and canals. He advises them on the approach, berthing, departing or towing operations of the boat.

Attached to a pilot station, he masters all the specifics and provides the necessary directions and advice to the commander.

*To go further* Articles L. 5341-1 and L. 5341-2 of the Transportation Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practise as a marine pilot, the candidate must hold a licence issued by the regional prefect, intervening after success in a competition organized by the flight station in which the person wished to practice.

The flight stations each determine their own conditions for admission to the competition. However, there are common provisions, including:

- Be between the age of 24 and 35 to compete at a pilot station;
- to have sailed for at least 72 months in the merchant navy or in state vessels, including 48 months in the navigation service and watching over long-haul, coasting, deep-sea or offshore armed vessels;
- Have the physical skills
- to hold a training title.

*To go further* Articles R. 5341-24 and R. 5341-28 of the Transportation Code.

#### Training

To enter the competition, the candidate must first have the training documents required by the flight station, which can be:

- The first-class captain's certificate of maritime navigation;
- The merchant navy's graduate degree;
- Captain 3000's patent.

Once the candidate has one of these diplomas, he can enter the competition by forwarding a file to the Departmental Directorate of Territories and the Sea concerned, including the following supporting documents:

- A reasoned and handwritten statement
- A navigation survey
- a no.3 extract from the criminal record of less than three months;
- The TOEIC certificate of less than two years with the number of points required to obtain the training title;
- If necessary, the list of laps performed under the understudy with the drivers of the station concerned prior to the competition;
- any certificate indicating the functions taken on board a state or merchant navy vessel;
- a certificate of physical fitness.

The competition jury reviews the documents in the file and decides to submit the candidate for written and oral tests, accompanied by an individual interview.

Admission is pronounced if the person has obtained an average score of 12 out of 20, with no score less than 5 out of 20

*To go further* Article R. 5341-24;[decree of 26 September 1990](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000026771294) related to the organisation and programme of the pilot competitions.

#### Physical fitness

The practice of the profession is subject to a medical examination to justify the candidate's physical fitness. The latter is determined during the medical visits that take place annually, throughout the career of the maritime pilot.

If during a visit the pilot was found to be physically unfit, he could be referred to a local visiting commission as well as a counter-visit committee when requesting it.

In the event of a proven physical incapacity, the pilot will be forced to no longer perform the function, by decision of the prefect.

*To go further* Article R. 5341-26 of the Transport Code;[decreed from 8 April 1991](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006077516) fitness for pilot and pilot captain duties.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

There are no regulations for a national of a Member State of the European Union (EU) or part of the European Economic Area (EEA) wishing to practise as a maritime pilot in France, either on a temporary or casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals (see below "5. "Qualification Recognition Process and Formalities") will find to apply.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state, who legally carries out the activity of maritime pilot in that state, may settle in France to carry out the same activity on a permanent basis.

He must first apply for a visa to obtain a professional designation recognition visa from the Interregional Directorate of the Sea, with a view to applying for the maritime pilot competition at a pilot station (see infra "5o). a. Obtain a visa for the EU or EEA national for a permanent exercise (LE) ").

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least two years, within ten years. years before the performance.

*To go further* Article 10 of the year[decree of June 24, 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/24/DEVT1502017D/jo/texte) relating to the issuance of maritime vocational training documents and the conditions under which armed vessels are operated on board trade, recreational, fishing and marine crops.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The person concerned must respect moral conditions and not have been convicted of a correctional or criminal sentence.

To do so, he will have to present an extract from his criminal record of less than three months or a certificate of less than three months, from the EU State or the EEA, certifying these conditions.

*To go further* Article L. 5521-4 of the Transportation Code.

4°. Insurance
---------------------------------

The owner of the vessel piloted by the person concerned is civilly liable for the damage caused by the vessel during the piloting manoeuvres. In this case, the pilot will provide a surety which will be abandoned when his liability is put on the line for proven misconduct.

On the other hand, when the pilot is the ship's owner, he or she will have to take out professional liability insurance himself.

*To go further* Articles L. 5341-11 to L. 5341-14 of the Transportation Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obtain a visa for the EU or EEA national for a permanent exercise (LE)

**Competent authority**

The Interregional Director of the Sea, sitting in the administrative region of the ship's armament port, is responsible for issuing the certificate of recognition authorizing the permanent exercise of maritime pilot in France.

**Supporting documents**

In order to practice the maritime pilot profession on a permanent basis in France, the person concerned must produce a complete file containing:

- A valid piece of identification
- A copy of diplomas, titles or certificates justifying the national's professional qualification;
- a certificate justifying its activity for at least two years in the last ten years, when neither the training nor the practice of the profession is regulated in the Member State, as well as the details of the lessons followed and the content and duration Validated internships
- A certificate of physical fitness to navigate
- a copy of an extract from the national's criminal record.

**What to know **

If necessary, the pieces must be translated into French by a certified translator.

**Outcome of the procedure**

The competent authority has two months to decide on the application for a visa, as soon as the full file is received.

The silence kept at the end of this period will be worth decision to reject the application for visa.

If the application is accepted, the competent authority may issue a three-month temporary certificate in advance allowing it to apply for a vacancy at a flight station. The final visa will then be given to him for a period of five years, renewable upon request.

*To go further* Articles 10 and following of the decree of 24 June 2015;[decree of 25 September 2007](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000794396) relating to the recognition of maritime vocational training documents issued by other EU Member States or third countries for service on board french-flagged arms of trade and pleasure vessels.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

