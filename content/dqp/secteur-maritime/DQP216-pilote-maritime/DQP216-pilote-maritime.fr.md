﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP216" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur maritime" -->
<!-- var(title)="Pilote maritime" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-maritime" -->
<!-- var(title-short)="pilote-maritime" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-maritime/pilote-maritime.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="pilote-maritime" -->
<!-- var(translation)="None" -->

# Pilote maritime

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

Le pilote maritime est un professionnel dont la mission est d'assister le commandant de bord d'un navire sur ses manœuvres de sortie et d'entrée dans les ports, les estuaires, les cours d'eau et les canaux. Il les conseille ainsi sur les opérations d'approche, d'accostage, d'appareillage ou de remorquage du bateau.

Rattaché à une station de pilotage, il en maîtrise toutes les spécificités et fournit les indications et conseils nécessaires au commandant.

*Pour aller plus loin* : articles L. 5341-1 et L. 5341-2 du Code des transports.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer la profession de pilote maritime, le candidat doit être titulaire d'une licence délivrée par le préfet de région, intervenant après réussite à un concours organisé par la station de pilotage dans laquelle l’intéressé souhaite exercer.

Les stations de pilotage déterminent chacune leurs propres conditions d’admission au concours. Cependant, il existe des dispositions communes et notamment celles :

- d'avoir entre 24 ans et 35 ans pour se présenter au concours d'une station de pilotage ;
- d'avoir navigué au moins 72 mois dans la marine marchande ou dans des bâtiments de l’État, dont 48 mois dans le service navigation et veille sur des navires armés au long cours, au cabotage, à la grande pêche ou à la pêche au large ;
- de posséder les aptitudes physiques requises ;
- d'être titulaire d'un titre de formation.

*Pour aller plus loin* : articles R. 5341-24 et R. 5341-28 du Code des transports.

#### Formation

Pour se présenter au concours, le candidat doit, au préalable, disposer des titres de formation requis par la station de pilotage, qui peuvent être :

- le brevet de capitaine de première classe de la navigation maritime ;
- le diplôme d'étude supérieur de la marine marchande ;
- le brevet de capitaine 3000.

Dès lors que le candidat possède l'un de ces diplômes, il pourra se présenter au concours en transmettant un dossier à la direction départementale des territoires et de la mer concernée, comportant les pièces justificatives suivantes :

- une déclaration motivée et manuscrite ;
- un relevé de navigation ;
- un extrait n° 3 du casier judiciaire de moins de trois mois ;
- le certificat TOEIC de moins de deux ans avec le nombre de points requis pour l'obtention du titre de formation ;
- le cas échéant, la liste des tours effectués en doublure avec les pilotes de la station concernée préalablement au concours ;
- tout certificat indiquant les fonctions prises à bord d'un bâtiment de l’État ou de la marine marchande ;
- un certificat d'aptitude physique.

Le jury du concours examine les pièces du dossier et décide de soumettre le candidat à des épreuves écrites et orales, accompagnées d'un entretien individuel.

L'admission est prononcée dès lors que l'intéressé a obtenu une note moyenne de 12 sur 20, et ce, sans note inférieure à 5 sur 20

*Pour aller plus loin* : article R. 5341-24 ; arrêté du 26 septembre 1990 relatif à l'organisation et au programme des concours de pilotage.

#### Aptitude physique

L'exercice de la profession est soumis à un examen médical permettant de justifier l'aptitude physique du candidat. Cette dernière est déterminée lors des visites médicales qui interviennent annuellement, tout au long de la carrière du pilote maritime.

Si lors d'une visite, le pilote s'avérait être inapte physiquement, il pourrait être renvoyé devant une commission locale de visite ainsi que devant une commission de contre-visite lorsqu'il en fait la demande.

En cas d'inaptitude physique avérée, le pilote se verra contraint de ne plus exercer la fonction, par décision du préfet.

*Pour aller plus loin* : article R. 5341-26 du Code des transports ; arrêté du 8 avril 1991 relatif aux conditions d'aptitude physique aux fonctions de pilote et de capitaine pilote.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l’Union européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer la profession de pilote maritime en France, à titre temporaire ou occasionnel.

Dès lors, seules les mesures prises pour le libre établissement des ressortissants de l'UE ou de l'EEE (cf. infra « 5. Démarche et formalités de reconnaissance de qualification ») trouveront à s'appliquer.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE, qui exerce légalement l'activité de pilote maritime dans cet État, peut s'établir en France pour y exercer cette même activité de manière permanente.

Il devra, au préalable, demander la délivrance d'un visa de reconnaissance des titres professionnels auprès de la direction interrégionale de la mer, en vue d'une candidature au concours de pilote maritime dans une station de pilotage (cf. infra « 5°. a. Obtenir un visa de reconnaissance de titres pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins deux ans, au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : article 10 du décret du 24 juin 2015 relatif à la délivrance des titres de formation professionnelle maritime et aux conditions d'exercice de fonctions à bord des navires armés au commerce, à la plaisance, à la pêche et aux cultures marines.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

L'intéressé doit respecter des conditions de moralité et ne pas avoir fait l'objet de condamnation pour une peine correctionnelle ou criminelle.

Pour cela, il devra présenter un extrait de son casier judiciaire de moins de trois mois ou d'une attestation de moins de trois mois, de l'État de l'UE ou de l'EEE, certifiant ces conditions.

*Pour aller plus loin* : article L. 5521-4 du Code des transports.

## 4°. Assurance

L'armateur du navire piloté par l'intéressé est responsable civilement des dommages causés par ce dernier au cours des manœuvres de pilotage. Dans ce cas, le pilote fournira une caution qui sera abandonnée lorsque sa responsabilité sera mise en jeu pour faute avérée.

En revanche, lorsque le pilote est l'armateur du navire, il devra souscrire lui-même une assurance de responsabilité civile professionnelle.

*Pour aller plus loin* : articles L. 5341-11 à L. 5341-14 du Code des transports.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Obtenir un visa de reconnaissance de titres pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le directeur interrégional de la mer siégeant dans la région administrative du port d'armement du navire, est compétent pour délivrer l'attestation de reconnaissance autorisant l'exercice permanent de pilote maritime en France.

#### Pièces justificatives

Pour exercer à titre permanent la profession de pilote maritime en France, l'intéressé doit produire un dossier complet comportant :

- une pièce d'identité en cours de validité ;
- une copie des diplômes, titres ou certificats justifiant la qualification professionnelle du ressortissant ;
- une attestation justifiant son activité pendant au moins deux ans au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre, ainsi que le détail des enseignements suivis et le contenu et la durée des stages validés ;
- un certificat d'aptitude physique à la navigation ;
- une copie d'un extrait du casier judiciaire du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Issue de la procédure

L'autorité compétente dispose d'un délai de deux mois pour se prononcer sur la demande de délivrance de visa, dès réception du dossier complet.

Le silence gardé à l'expiration de ce délai vaudra décision de rejet de la demande de délivrance de visa.

En cas d'acceptation de la demande, l'autorité compétente pourra délivrer, au préalable, une attestation temporaire de trois mois lui permettant de se porter candidat à une vacance au sein d'une station de pilotage. Le visa définitif lui sera remis par la suite pour une durée de cinq ans, renouvelable sur demande.

*Pour aller plus loin* : articles 10 et suivants du décret du 24 juin 2015 ; arrêté du 25 septembre 2007 relatif à la reconnaissance des titres de formation professionnelle maritime délivrés par d'autres États membres de l'Union européenne ou des pays tiers pour le service à bord des navires armés au commerce et à la plaisance battant pavillon français.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).