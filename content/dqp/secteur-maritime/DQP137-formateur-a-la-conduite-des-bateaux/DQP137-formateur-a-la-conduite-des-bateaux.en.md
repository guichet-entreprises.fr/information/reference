﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP137" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Maritime sector" -->
<!-- var(title)="Boating instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="maritime-sector" -->
<!-- var(title-short)="boating-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/maritime-sector/boating-instructor.html" -->
<!-- var(last-update)="2020-04-15 17:22:16" -->
<!-- var(url-name)="boating-instructor" -->
<!-- var(translation)="Auto" -->


Boating instructor
===========================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The pleasure boat trainer is a professional in charge of teaching motor pleasure craft with a power of more than 4.5 kilowatts, in sea and/or inland waters.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to train the pleasure boat, the professional must meet the following requirements:

- not be the subject of any criminal or correctional sentence, the list of which is set at the[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000021871175&cidTexte=LEGITEXT000006056685&dateTexte=20180509) Decree No. 2007-1167 of 2 August 2007 on driver's licences and motor boat training;
- have held a licence to operate motorboats at sea and inland waters for at least three years, or a title recognized as equivalent, listed within the[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024497884) July 21, 2011 on obtaining a motor pleasure boat driving certificate by equivalence with a professional title or qualification;
- Meet the age requirements of the motor pleasure craft licence;
- meet the fitness requirements set out in Schedule VI of the 28 September 2007 order on the motor pleasure boat licence, the accreditation of training establishments and the issuance of teaching licences;
- be professionally qualified (see infra "2.2). a. Training").

*For further information*: Article L. 5272-3 of the Transport Code and Article 32 of Decree No. 2007-1167 of August 2, 2007 as a case.

#### Training

To be recognized as a professionally qualified, the professional must be a holder:

- a certificate of first aid training;
- Is:- a minimum V-level title or diploma that requires training in educational or sports education or animation. As long as this training is not related to boating, the professional must also take additional training in boating,
  - professional nautical qualification. Since this qualification does not relate to teaching or animation of an educational or sporting nature, it must also take additional training in pedagogy;
- a restricted marine radio operator certificate for the maritime mobile service, a restricted operator certificate or a general operator certificate.

Once the professional meets these requirements, he or she must undergo assessment training in order to obtain a teaching licence (see below "Assessment Training").

*For further information*: Article L. 5272-3 of the Transportation Code.

**Motor pleasure boat licence**

In order to train recreational boating, the professional must hold the licence to drive pleasure craft that includes:

- for maritime waters:- the "coastal" option for navigation up to six miles from a shelter,
  - The "offshore" extension for navigation beyond six miles from a shelter;
- inland waters:- The "inland waters" option for vessels less than 20 metres long,
  - the extension of "great inland water pleasure" for navigation on a boat of a length of twenty meters or more.

This permit is issued by the prefect of the department in which the department that has applied for a permit at its seat, candidates who are at least sixteen years of age and who have passed a theoretical and practical examination in an approved institution.

This review consists of:

- one or two written theoretical tests, the programme of which varies according to the option chosen and is fixed to the articles[1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000028531760&cidTexte=LEGITEXT000006057103&dateTexte=20180509) And[2](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000028531762&cidTexte=LEGITEXT000006057103&dateTexte=20180509) of the order of 28 September 2007 above;
- practical training common to both options. The pedagogical objectives of this training are set at the[Appendix I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000023750270&cidTexte=LEGITEXT000006057103&dateTexte=20180509) September 28, 2007.

*For further information*: Articles 2 to 21 and Appendix III of the decree of 2 August 2007 above.

**Evaluation training**

In order to obtain a teaching licence, the professional who meets the above conditions must complete an evaluation training course at an accredited school for pleasure boat training.

The program of this training is set in Schedule III of the order of 28 September 2007 mentioned above and is intended to verify the candidate's ability to:

- Know and understand the fundamentals of the "training/evaluation" process
- Use a training repository to be retransmitted as a program;
- Combine each learning sequence with an evaluation and decision-making device;
- ensure that training input data is updated.

#### Costs associated with qualification

The cost of qualification varies depending on the institution delivering the training. It is advisable to get close to the institutions concerned for more information.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA), legally established and practising the activity of trainer in the operation of pleasure boats, may practice in France, as a temporary and casual, the same activity.

Where the EU or EEA Member State does not regulate access to the activity or its exercise, the person concerned must justify having carried out this activity for at least one year in the last ten years.

Once the national fulfils these conditions, he must, before his first service delivery, make a prior declaration for the verification of his professional qualifications. If these qualifications relate only part of the training activity, he will be able to benefit from partial access to the activity (see below "Pre-Declaration for the EU National for Freedom to provide services").

*For further information*: paragraph 7 and following section L. 5272-3 of the Transportation Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any EU or EEA national, legally established and training the pleasure boat, may carry out the same activity in France on a permanent basis.

Where the state regulates access and practice of this profession, the professional must justify holding a certificate of competency or a title to carry out this activity;

If the state does not regulate access to the profession or its exercise, the national must justify having exercised this activity for one year in the last ten years, the profession of trainer to the conduct of pleasure boats and holding a certificate of competency or a training document attesting to his preparation for the practice of this profession.

**Please note**

May also exercise, the professor holding a title allowing the practice of this profession, issued in a third country but recognized by a Member State, if he justifies having exercised this activity for at least three years in the State having accepted the validity of the title.

Once the professional meets these conditions, he must apply for a teaching licence under the same conditions as the French national (see infra "5o). a. Request for permission to teach").

**Good to know: compensation measures**

In the event of substantial differences between the training received by the national and that required to practice in France, the prefect may decide to submit him either to an aptitude test or to an adjustment course.

The aptitude test consists of theoretical and practical training at one of the basic options of the pleasure licence. At the end of these trainings, the professional conducts an interview before a jury in order to evaluate his pedagogical and expression qualities.

The adaptation course, the duration of which is set by the prefect, must be carried out within an accredited institution. During this internship, the professional must attend at least three full training sessions at one of the basic licensing options and provide two as a trainer. At the end of this internship, he is issued with a certificate of follow-up to the internship and an internship report written by the school's trainer.

*For further information*: Article 32 of the decree of 2 August 2007 mentioned above; Article 8 of the order of 28 September 2007 above.

3°.Professional rules
---------------------------------

The pleasure boat trainer must ensure that the vessel has on board, armament and safety equipment adapted to its characteristics.

*For further information*: Article D. 4211-4 of the Transport Code; order of 11 April 2012 relating to the armament and safety equipment of pleasure boats sailing or parked in inland waters.

4°. Sanctions
---------------------------------

The professional who would train the motor boat without authorisation to teach valid, faces a one-year prison sentence and a fine of 15,000 euros.

*For further information*: Article L. 5273-3 of the Transportation Code.

5°. Qualification recognition process and formalities
---------------------------------------------------------------

### a. Request for permission to teach

**Competent authority**

The professional must apply to the prefect of the department in which the department that has applied for accreditation from the training institution employing him at his headquarters.

**Supporting documents**

His application must include:

- A photocopy of his ID
- An identity photograph
- A photocopy of his various driving titles for pleasure ships and pleasure boats;
- A photocopy of the first aid training certificate or equivalent title;
- A photocopy of his title or qualification
- If applicable and as appropriate, a photocopy of his restricted certificate as a maritime radio operator, his restricted operator certificate or his general operator certificate;
- A certificate of follow-up of the training course for evaluation;
- a medical certificate of less than six months to justify his physical fitness, the model of which is fixed in Schedule VII of the order of 28 September 2007.

**Please note**

The photocopying of the title, diploma or nautical qualification is replaced by the photocopying of the work certificates of the various employers, as long as the professional has been in this activity for at least three years during the five Years.

**Outcome of the procedure**

The professional-issued teaching authorization is valid for five years.

*For further information*: Article 13 of the decree of 2 August 2007 above.

### b. Pre-declaration for EU nationals for free provision of services (LPS)

**Competent authority**

The national must apply by any means to the prefect responsible for issuing the authorisation to teach (see above "5.0). a. Request for permission to teach").

**Supporting documents**

Its application must contain the following documents, if any, with their translation into French:

- A document of the applicant's identity and proof of nationality;
- a certificate certifying that it is legally established in an EU or EEA Member State to train motor boating and that it is not prohibited from practising;
- proof of his professional qualifications
- where the state does not regulate access to the activity or its exercise, a proof certifying that the professional has been engaged in this activity for at least one year in the last ten years;
- proof that the national is not the subject of any conviction for the offences mentioned in the[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000021871175&cidTexte=LEGITEXT000006056685&dateTexte=20180509) 2 August 2007 aforementioned.

**Time and procedure**

The prefect checks the applicant's professional qualifications and, within a maximum of one month, informs him of his decision. In the absence of a response beyond this time, the professional can begin his service delivery.

In the event of substantial differences between the training of the professional and that required in France to carry out this activity, the prefect may decide, within one month, to submit him to an aptitude test. This aptitude test takes the form of a theoretical and practical training session conducted before a jury on one of the basic options of the pleasure permit. The candidate must then interview the jury to assess their knowledge of the administrative environment of the recreational licence.

**Please note**

Each year the professional is required to apply for renewal of his authorization by providing the following documents:

- Permission to teach expires
- An identity photograph
- a medical certificate justifying his physical fitness.

*For further information*: Article 32 bis of the decree of 2 August 2007 and 13.3 of Article 13 of the decree of 28 September 2007 mentioned above.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

