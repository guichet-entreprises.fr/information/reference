﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP137" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur maritime" -->
<!-- var(title)="Formateur à la conduite des bateaux de plaisance" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-maritime" -->
<!-- var(title-short)="formateur-a-la-conduite-des-bateaux" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-maritime/formateur-a-la-conduite-des-bateaux-de-plaisance.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="formateur-a-la-conduite-des-bateaux-de-plaisance" -->
<!-- var(translation)="None" -->

# Formateur à la conduite des bateaux de plaisance

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l’activité

Le formateur à la conduite des bateaux de plaisance est un professionnel en charge de l'enseignement de la conduite des bateaux de plaisance à moteur d'une puissance supérieure à 4,5 kilowatts, en eaux maritimes et/ou en eaux intérieures.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de formateur à la conduite des bateaux de plaisance, le professionnel doit remplir les conditions suivantes :

- ne faire l'objet d'aucune condamnation à une peine criminelle ou correctionnelle dont la liste est fixée à l'annexe du décret n° 2007-1167 du 2 août 2007 relatif au permis de conduire et à la formation à la conduire des bateaux de plaisance à moteur ;
- être titulaire depuis au moins trois ans, d'un permis de conduire des bateaux de plaisance à moteur en mer et en eaux intérieures ou d'un titre reconnu comme équivalent, dont la liste est fixée au sein de l'arrêté du 21 juillet 2011 relatif à l'obtention d'un titre de conduite des bateaux de plaisance à moteur par équivalence avec un titre ou une qualification professionnelle ;
- remplir les conditions d'âge du permis de conduire les bateaux de plaisance à moteur ;
- remplir les conditions d'aptitude physique fixées à l'annexe VI de l'arrêté du 28 septembre 2007 relatif au permis de conduire des bateaux de plaisance à moteur, à l'agrément des établissements de formation et à la délivrance des autorisations d'enseigner ;
- être qualifié professionnellement (cf. infra « 2°. a. Formation »).

*Pour aller plus loin* : article L. 5272-3 du Code des transports et article 32 du décret n° 2007-1167 du 2 août 2007 susvisé.

#### Formation

Pour être reconnu comme étant qualifié professionnellement, le professionnel doit être titulaire :

- d'une attestation de formation aux premiers secours ;
- soit :
  - d'un titre ou d'un diplôme d'un niveau V minimum et sanctionnant une formation portant sur l'enseignement ou l'animation à caractère éducatif ou sportif. Dès lors que cette formation n'est pas liée au nautisme, le professionnel doit en outre, suivre une formation complémentaire au nautisme,
  - d'une qualification professionnelle nautique. Dès lors que cette qualification ne porte pas sur l'enseignement ou l'animation à caractère éducatif ou sportif, il doit en outre, suivre une formation complémentaire à la pédagogie ;
- d'un certificat restreint de radiotéléphoniste maritime du service mobile maritime, d'un certificat restreint d'opérateur ou d'un certificat général d'opérateur.

Dès lors qu'il remplit ces conditions, le professionnel doit suivre une formation à l'évaluation en vue d'obtenir une autorisation d'enseigner (cf. infra « Formation à l'évaluation »).

*Pour aller plus loin* : article L. 5272-3 du Code des transports.

##### Permis de conduire des bateaux de plaisance à moteur

Pour exercer l'activité de formateur à la conduite des bateaux de plaisance, le professionnel doit être titulaire du permis de conduire des bateaux de plaisance comportant :

- pour les eaux maritimes :
  - l'option « côtière » pour une navigation jusqu'à six milles d'un abri, 
  - l'extension « hauturière » pour une navigation au delà de six milles d'un abri ;
- pour les eaux intérieures :
  - l'option « eaux intérieures » pour les bateaux d'une longueur inférieure à vingt mètres, 
  - l'extension « grande plaisance eaux intérieures » pour une navigation sur un bateau d'une longueur égale ou supérieure à vingt mètres.

Ce permis est délivré par le préfet du département dans le lequel le service qui a instruit la demande de permis à son siège, aux candidats âgés de seize ans minimum et ayant passé avec succès un examen théorique et pratique au sein d'un établissement agréé à cet effet.

Cet examen se compose :

- d'une ou deux épreuves théoriques écrites dont le programme varie selon l'option choisie et est fixé aux articles 1 et 2 de l'arrêté du 28 septembre 2007 précité ;
- d'une formation pratique commune aux deux options. Les objectifs pédagogiques de cette formation sont fixés à l'annexe I de l'arrêté du 28 septembre 2007.

*Pour aller plus loin* : articles 2 à 21 et annexe III du décret du 2 août 2007 précité.

##### Formation à l'évaluation

En vue d'obtenir une autorisation d'enseigner, le professionnel remplissant les conditions susvisées, doit effectuer un stage de formation à l'évaluation au sein d'un établissement agréé pour la formation à la conduite des bateaux de plaisance.

Le programme de cette formation est fixé à l'annexe III de l'arrêté du 28 septembre 2007 précité et a pour but de vérifier les capacités du candidat à :

- connaître et comprendre les fondamentaux du processus « formation/évaluation » ;
- exploiter un référentiel de formation en vue de le retransmettre sous forme de programme ;
- associer à chaque séquence d'apprentissage un dispositif d'évaluation et de prise de décision ;
- assurer la mise à jour des données d'entrée de la formation.

#### Coûts associés à la qualification

Le coût de la qualification varie selon l'établissement délivrant la formation. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant l'activité de formateur à la conduite des bateaux de plaisance, peut exercer en France, à titre temporaire et occasionnel, la même activité.

Lorsque l’État membre de l'UE ou de l'EEE ne réglemente ni l'accès à l'activité, ni son exercice, l'intéressé doit justifier avoir exercé cette activité pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, le ressortissant doit, avant sa première prestation de services, effectuer une déclaration préalable en vue de la vérification de ses qualifications professionnelles. Si ces qualifications ne concernent qu'une partie de l'activité de formateur, il pourra bénéficier d'un accès partiel à l'activité (cf. infra « Déclaration préalable pour le ressortissant de l'UE en vue d'une Libre Prestation de Services (LPS) »).

*Pour aller plus loin* : alinéa 7 et suivants de l'article L. 5272-3 du Code des transports.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant de l'UE ou de l'EEE, légalement établi et exerçant l'activité de formateur à la conduite des bateaux de plaisance, peut exercer en France, à titre permanent, la même activité.

Lorsque l’État réglemente l'accès et l'exercice de cette profession, le professionnel doit justifier être titulaire d'une attestation de compétences ou d'un titre permettant d'exercer cette activité.

Si l’État ne réglemente ni l'accès à la profession, ni son exercice, le ressortissant doit justifier avoir exercé cette activité pendant un an au cours des dix dernières années, la profession de formateur à la conduite des bateaux de plaisance et être titulaire d'une attestation de compétence ou un titre de formation attestant de sa préparation à l'exercice de cette profession.

**À noter**

Peut également exercer, le professeur titulaire d'un titre permettant l'exercice de cette profession, délivré dans un pays tiers mais reconnu par un État membre, dès lors qu'il justifie avoir exercé cette activité pendant au moins trois ans au sein de l’État ayant admis la validité du titre.

Dès lors qu'il remplit ces conditions, le professionnel doit effectuer une demande en vue d'obtenir une autorisation d'enseigner dans les mêmes conditions que le ressortissant français (cf. infra « 5°. a. Demande en vue d'obtenir une autorisation d'enseigner »).

#### Bon à savoir : mesures de compensation

En cas de différences substantielles entre la formation reçue par le ressortissant et celle exigée pour exercer en France, le préfet peut décider de le soumettre soit à une épreuve d'aptitude, soit à un stage d'adaptation.

L'épreuve d'aptitude consiste en une formation théorique et une formation pratique à l'une des options de base du permis plaisance. À l'issue de ces formations, le professionnel effectue un entretien devant un jury en vue d'évaluer ses qualités pédagogiques et d'expression.

Le stage d'adaptation dont la durée est fixée par le préfet, doit être effectué au sein d'un établissement agréé. Au cours de ce stage, le professionnel doit assister à au moins trois sessions de formation complètes à l'une des options de base du permis et en assurer deux en tant que formateur. À l'issue de ce stage, une attestation de suivi de stage lui est délivrée ainsi qu'un rapport de stage rédigé par le formateur de l'établissement.

*Pour aller plus loin* : article 32 du décret du 2 août 2007 susvisé ; article 8 de l'arrêté du 28 septembre 2007 précité.

## 3°. Règles professionnelles

Le formateur à la conduite des bateaux de plaisance doit s'assurer que le bateau dispose à bord, du matériel d'armement et de sécurité adapté à ses caractéristiques.

*Pour aller plus loin* : article D. 4211-4 du Code des transports ; arrêté du 11 avril 2012 relatif au matériel d'armement et de sécurité des bateaux de plaisance naviguant ou stationnant sur les eaux intérieures.

## 4°. Sanctions

Le professionnel qui exercerait l'activité de formateur à la conduite des bateaux de plaisance à moteur sans autorisation d'enseigner en cours de validité, encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : article L. 5273-3 du Code des transports.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir une autorisation d'enseigner

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet du département dans lequel le service ayant instruit la demande d'agrément de l'établissement de formation l'employant, à son siège.

#### Pièces justificatives

Sa demande doit comporter :

- une photocopie de sa pièce d'identité ;
- une photographie d'identité ;
- une photocopie de ses différents titres de conduite des navires et bateaux de plaisance ;
- une photocopie de l'attestation de formation aux premiers secours ou d'un titre équivalent ;
- une photocopie de son titre ou de sa qualification ;
- le cas échéant et selon le cas, une photocopie de son certificat restreint de radiotéléphoniste maritime, de son certificat restreint d'opérateur ou de son certificat général d'opérateur ;
- une attestation de suivi du stage de formation à l'évaluation ;
- un certificat médical datant de moins six mois permettant de justifier de son aptitude physique dont le modèle est fixé à l'annexe VII de l'arrêté du 28 septembre 2007.

**À noter**

La photocopie du titre, du diplôme ou de la qualification professionnelle nautique est remplacée par la photocopie des certificats de travail des différents employeurs, dès lors que le professionnel a exercé cette activité pendant au moins trois ans durant les cinq dernières années.

#### Issue de la procédure

L'autorisation d'enseigner délivrée au professionnel est valable pendant cinq ans.

*Pour aller plus loin* : article 13 du décret du 2 août 2007 précité.

### b. Déclaration préalable pour le ressortissant de l'UE en vue d'une libre prestation de services (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande par tout moyen, au préfet compétent pour délivrer l'autorisation d'enseigner (cf. supra « 5°. a. Demande en vue d'obtenir une autorisation d'enseigner »).

#### Pièces justificatives

Sa demande doit contenir les documents suivants, le cas échéant, assortis de leur traduction en français :

- une pièce d'identité du demandeur et un justificatif de sa nationalité ;
- une attestation certifiant qu'il est légalement établi dans un État membre de l'UE ou de l'EEE pour exercer l'activité de formateur à la conduite des bateaux de plaisance à moteur et qu'il n'encourt aucune interdiction d'exercer ;
- un justificatif de ses qualifications professionnelles ;
- lorsque l’État ne réglemente ni l'accès à l'activité, ni son exercice, un justificatif certifiant que le professionnel a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- la preuve que le ressortissant ne fait l'objet d'aucune condamnation pour les infractions mentionnées à l'annexe du décret du 2 août 2007 précité.

#### Délai et procédure

Le préfet procède à la vérification des qualifications professionnelles du demandeur et, dans un délai maximal d'un mois, l'informe de sa décision. En l'absence de réponse au delà de ce délai, le professionnel peut débuter sa prestation de services.

En cas de différences substantielles entre la formation du professionnel et celle exigée en France pour exercer cette activité, le préfet peut décider, dans un délai d'un mois, de le soumettre à une épreuve d'aptitude. Cette épreuve d'aptitude prend la forme d'une séance de formation théorique et pratique effectuée devant un jury et portant sur une des options de base du permis de plaisance. Le candidat doit ensuite effectuer un entretien devant le jury, en vue d'évaluer ses connaissances sur l'environnement administratif du permis de plaisance.

**À noter**

Chaque année le professionnel est tenu d'adresser une demande de renouvellement de son autorisation en fournissant les documents suivants :

- l'autorisation d'enseigner arrivant à échéance ;
- une photographie d'identité ;
- un certificat médical justifiant de son aptitude physique.

*Pour aller plus loin* : article 32 bis du décret du 2 août 2007 et 13.3 de l'article 13 de l'arrêté du 28 septembre 2007 précité.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).