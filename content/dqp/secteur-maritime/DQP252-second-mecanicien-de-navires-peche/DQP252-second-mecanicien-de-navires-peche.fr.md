﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP252" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur maritime" -->
<!-- var(title)="Second mécanicien de navires de pêche" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-maritime" -->
<!-- var(title-short)="second-mecanicien-de-navires-peche" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-maritime/second-mecanicien-de-navires-de-peche.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="second-mecanicien-de-navires-de-peche" -->
<!-- var(translation)="None" -->

# Second mécanicien de navires de pêche

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

Le second mécanicien de navires de pêche est l'adjoint direct du chef mécanicien. Il a pour missions principales d'assurer la propulsion du navire et de diriger les officiers et le personnel d'exécution du service machine.

Dans le cadre de ses fonctions, le second mécanicien assurera également la planification et la réalisation des opérations de maintenance, l'identification des défauts de fonctionnement des machines, ainsi que le suivi des travaux exécutés en cours de navigation.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession de second mécanicien de navires de pêche est réservée au titulaire du brevet de second mécanicien, remplissant l'ensemble des conditions suivantes :

- avoir 20 ans au moins au jour du dépôt de la demande de brevet ;
- être titulaire d'un certificat d'aptitude médicale ;
- posséder les qualifications professionnelles requises ;
- avoir accompli un service en mer d'au moins douze mois en qualité d'officier chargé du quart à la machine.

**À noter**

Le brevet de second mécanicien est valable pour une durée de cinq ans et doit être renouvelé dans les règles de l'arrêté du 24 juillet 2013 relatif à la revalidation des titres de formation professionnelle maritime.

*Pour aller plus loin* : article 10 de l'arrêté du 19 avril 2016 relatif à la délivrance du brevet de second mécanicien et du brevet de chef mécanicien.

#### Formation

Pour satisfaire à la condition de qualifications professionnelles, le candidat au brevet de second mécanicien doit être titulaire :

- de l'un des brevets suivants en cours de validité :
  - brevet d'officier chef de quart machine,
  - brevet d'officier chef de quart de navire de mer,
  - brevet mentionné au tableau 3 de l'arrêté du 19 avril 2016 ;
- de l'un des diplômes suivants en cours de validité :
  - diplôme de chef mécanicien,
  - diplôme d'études supérieures de la marine marchande,
  - diplôme ou attestation mentionnés au tableau 4 de l'arrêté ;
- du certificat attestant la validation de l'enseignement médical de niveau II (EM II).

*Pour aller plus loin* : article 10 de l'arrêté du 19 avril 2016 relatif à la délivrance du brevet de second mécanicien et du brevet de chef mécanicien.

#### Aptitude physique

L'accès à la profession de second mécanicien sur les navires de pêche est soumis à des conditions minimales d'aptitude physique qui sont évaluées lors de visites médicales requises :

- avant l'accès à la profession de second mécanicien ;
- avant le premier embarquement ;
- avant toute entrée en formation maritime ;
- avant l'expiration du certificat d'aptitude médicale dont la durée varie selon l'âge de l'officier.

À l'issue de la visite médicale, le médecin peut décider que le second mécanicien est apte et se verra remettre un certificat d'aptitude.

En revanche, lorsque la décision du médecin indique une aptitude partielle, une inaptitude temporaire ou une inaptitude totale, il pourra la contester auprès de la Commission Médicale Régionale d’Aptitude à la Navigation (CMRA).

*Pour aller plus loin* : article L. 5521-1 du Code des transports et décret du 3 décembre 2015 relatif à la santé et à l'aptitude médicale à la navigation.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE), exerçant légalement l’activité de second mécanicien sur les navires de pêche dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au directeur interrégional de la mer compétent de la région administrative dans laquelle il est identifié (cf. infra « 4°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

**À savoir**

L'exercice de second mécanicien en France, à titre temporaire ou occasionnel, requiert que le ressortissant possède toutes les connaissances linguistiques nécessaires.

Lorsqu'il existe des différences substantielles entre la formation du ressortissant et celles exigées en France, ou que l'intéressé n'a pas acquis la totalité des compétences requises pour exercer dans un navire de pêche battant pavillon français, des mesures de compensation peuvent être prises (cf. infra « 4°. a. Bon à savoir : mesure de compensation »).

*Pour aller plus loin* : articles 20 du décret du 24 juin 2015 et 8 de l'arrêté du 8 février 2010.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- le titre présenté a son équivalent exact en France ;
- il a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une des trois conditions précédentes, il devra demander une attestation de reconnaissance auprès du directeur interrégional de la mer compétent (cf. infra « 4°. b. Obtenir une attestation de reconnaissance pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE) »).

Si, lors de l'examen du dossier, le directeur interrégional de la mer constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées pour exercer sur un navire battant pavillon français, des mesures de compensation pourront être prises (cf. infra « 4°. b. Bon à savoir : mesure de compensation »)

*Pour aller plus loin* : articles 4 à 5 de l'arrêté du 8 février 2010.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

L'intéressé qui souhaite exercer la profession de second mécanicien, doit respecter des conditions de moralité et ne pas avoir fait l'objet de condamnation pour une peine correctionnelle ou criminelle.

Pour cela, il devra présenter un extrait de son casier judiciaire de moins de trois mois ou d'une attestation de moins de trois mois de l'État de l'UE ou de l'EEE, certifiant ces conditions.

*Pour aller plus loin* : articles 8 et 9 du décret du 2 juin 2015.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le directeur interrégional de la mer dans la région administrative dans laquelle le second mécanicien souhaite effectuer la prestation ou dans laquelle se situe le port d’armement du navire de pêche, est compétent pour se prononcer sur la déclaration. Il accusera réception de la demande dans un délai d'un mois à compter de la réception du dossier.

#### Renouvellement de la déclaration préalable

La déclaration doit être renouvelée une fois par an et en cas de changement de situation du ressortissant.

#### Pièces justificatives

Pour exercer la profession de second mécanicien sur un navire de pêche, le ressortissant adresse à l'autorité compétente un dossier comprenant les pièces justificatives suivantes :

- une déclaration écrite et signée par le ressortissant ;
- une pièce d'identité en cours de validité du ressortissant ;
- une attestation établie par l'autorité compétente de l’État de l'UE ou de l'EEE certifiant que le ressortissant est légalement établi dans cet État et n'encourt aucune interdiction d'exercer ;
- une attestation justifiant des qualifications professionnelles du ressortissant ;
- une attestation justifiant son activité pendant au moins deux ans au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre ;
- une attestation justifiant que les conditions de moralité sont respectées ;
- un certificat d'aptitude physique à la navigation ;
- une attestation de maîtrise des connaissances linguistiques.

#### Délai

La prestation peut débuter dès lors qu'il n'y a pas d'opposition de la part de la direction interrégionale de la mer :

- à l'expiration d'un délai d'un mois à compter de la demande de déclaration ;
- en cas de demande de complément d'information ou de la vérification des qualifications professionnelles, à l'expiration d'un délai de deux mois à compter de la réception de la demande complète.

**À noter**

En cas de demande d'accès partiel à la profession, le ressortissant devra réaliser les mêmes démarches que pour l'exercice de l'activité à titre temporaire ou occasionnel sur le territoire français.

#### Bon à savoir : mesure de compensation

Pour obtenir l’autorisation d’exercer, l’intéressé peut être amené à se soumettre à une épreuve d’aptitude s’il apparaît que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France.

L'épreuve d'aptitude doit permettre d'établir que les connaissances et qualifications concernées sont maîtrisées.

##### Coût

Gratuit.

*Pour aller plus loin* : articles 7-2 à 10 de l'arrêté du 8 février 2010.

### b. Obtenir une attestation de reconnaissance pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE)

#### Autorité compétente

Le directeur interrégional de la mer siégeant dans la région administrative du port d'armement du navire de pêche, est compétent pour délivrer l'attestation de reconnaissance autorisant l'exercice permanent de second mécanicien en France.

#### Procédure

La demande d'attestation de reconnaissance est adressée par tout moyen à l'autorité compétente de la région administrative dans laquelle il souhaite s'établir. En cas de document manquant, l'autorité compétente dispose d'un mois à compter de la réception du dossier, pour en informer le ressortissant.

#### Pièces justificatives

Pour exercer à titre permanent la profession de second mécanicien en France, l'intéressé doit produire un dossier complet comportant :

- le formulaire [Cerfa n° 14750](https://www.service-public.fr/professionnels-entreprises/vosdroits/R12438) dûment complété et signé ;
- une pièce d'identité en cours de validité ;
- une attestation d'expérience professionnelle délivrée par l'autorité compétente de l’État membre, lorsque la profession n'est pas réglementée dans cet État ;
- lorsqu'il est demandé par l'autorité compétente, le cas échéant, le programme de formation conduisant à la délivrance du titre ;
- une attestation justifiant que les conditions de moralité sont respectées ;
- un certificat d'aptitude physique à la navigation ;
- une attestation de maîtrise des connaissances linguistiques.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Issue de la procédure

L'autorité compétente dispose d'un délai d'un mois pour se prononcer sur la demande d'attestation, dès réception du dossier complet.

Toute décision, qu'elle soit d'acceptation, de refus ou prononçant des mesures de compensation, doit être motivée.

Le silence gardé à l'expiration d'un délai de deux mois vaudra décision de rejet de la demande d'attestation de reconnaissance.

En cas d'acceptation de la décision, l'autorité compétente délivre l'attestation de reconnaissance dont la durée de validation est de cinq ans.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de compensation de son choix, soit un stage d'adaptation, soit une épreuve d'aptitude, réalisée dans les six mois suivant la décision de l'autorité compétente.

##### Coût

Gratuit.

*Pour aller plus loin* : articles 2 à 5-2 de l'arrêté du 8 février 2010.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).