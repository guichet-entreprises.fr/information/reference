﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP252" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Maritime sector" -->
<!-- var(title)="Second engineer on fishing vessels" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="maritime-sector" -->
<!-- var(title-short)="second-engineer-on-fishing-vessels" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/maritime-sector/second-engineer-on-fishing-vessels.html" -->
<!-- var(last-update)="2020-04-15 17:22:19" -->
<!-- var(url-name)="second-engineer-on-fishing-vessels" -->
<!-- var(translation)="Auto" -->


Second engineer on fishing vessels
==============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:19<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The second fishing vessel engineer is the chief engineer's direct assistant. Its main tasks are to ensure the propulsion of the ship and to direct the officers and performance personnel of the machine service.

As part of his duties, the second engineer will also plan and carry out maintenance operations, identify engine malfunctions, and track work performed during navigation.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of second engineer of fishing vessels is reserved for the holder of the second engineer's certificate, fulfilling all of the following conditions:

- Be at least 20 years old on the day the patent application is filed
- Hold a certificate of medical fitness
- Have the necessary professional qualifications
- have completed at least 12 months of service at sea as an officer in charge of the machine watch.

**Please note**

The second mechanic's certificate is valid for a period of five years and must be renewed under the rules of the order of 24 July 2013 relating to the revalidation of maritime vocational training certificates.

*For further information*: Article 10 of the[decree of 19 April 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032569477&dateTexte=20171208) second mechanic's patent and chief engineer's certificate.

#### Training

To meet the requirement of professional qualifications, the candidate for the second mechanic's certificate must be the holder of:

- one of the following valid patents:- certificate of officer head of watch machine,
  - certificate as officer in charge of sea ship watch,
  - patent listed in Table 3 of the Order of 19 April 2016;
- one of the following valid degrees:- diploma as chief engineer,
  - graduate degree from the Merchant Navy,
  - diploma or certificate mentioned in Table 4 of the decree;
- certificate certifying the validation of Level II medical education (EM II).

*For further information*: Article 10 of the[decree of 19 April 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032569477&dateTexte=20171208) second mechanic's patent and chief engineer's certificate.

#### Physical fitness

Access to the profession of second engineer on fishing vessels is subject to minimum fitness requirements that are assessed during required medical visits:

- prior to access to the profession of second mechanic;
- Before the first boarding;
- before entering maritime training;
- before the expiry of the certificate of medical fitness, the duration of which varies according to the age of the officer.

At the end of the medical visit, the doctor may decide that the second mechanic is fit and will be given a certificate of fitness.

On the other hand, when the doctor's decision indicates a partial suitability, temporary incapacity or total incapacity, he may challenge it with the Regional Medical Commission for Navigation Aptitude (CMRA).

*For further information*: Article L. 5521-1 of the Transport Code and[decree of December 3, 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031560450) health and medical fitness to navigate.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

A national of a European Union (EU) or European Economic Area (EEA) state, legally acting as a second mechanic on fishing vessels in one of these states, may use his or her professional title in France as a temporary or casual.

He will have to request it, before his first performance, by declaration addressed to the interregional director of the competent sea of the administrative region in which he is identified (see infra "4°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least one year, in the ten years before the performance.

**What to know**

The exercise of second mechanic in France, on a temporary or casual basis, requires that the national possess all the necessary language skills.

Where there are substantial differences between the training of the national and those required in France, or where the person concerned has not acquired all the skills required to practise in a French-flagged fishing vessel, compensation can be taken (see infra "4°. a. Good to know: compensation measure").

*For further information*: Articles 20 of the year[decree of June 24, 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/24/DEVT1502017D/jo/texte) and 8 of the[decreed from 8 February 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021844162&dateTexte=20171207).

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a title issued by a competent authority in another Member State, which regulates access to the profession or its exercise;
- The title presented has its exact equivalent in France;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once he fulfils one of the three previous conditions, he will have to apply for a certificate of recognition from the competent Interregional Director of the Sea (see infra "4°. b. Obtain a certificate of recognition for EU or EEA nationals for a permanent exercise (LE)).

If, in reviewing the file, the Interregional Director of the Sea finds that there are substantial differences between the professional training and experience of the national and those required to operate on a flagged vessel compensation measures can be taken (see infra "4°. b. Good to know: compensation measure"

*For further information*: Articles 4 to 5 of the February 8, 2010 order.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

A person who wishes to practise as a second mechanic must respect moral conditions and have not been convicted of a correctional or criminal sentence.

To do so, he will have to present an extract from his criminal record of less than three months or a certificate of less than three months from the EU State or the EEA, certifying these conditions.

*For further information*: Articles 8 and 9 of the decree of 2 June 2015.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The Interregional Director of the Sea in the administrative region in which the second engineer wishes to carry out the delivery or in which the armament port of the fishing vessel is located, is competent to decide on the declaration. He will acknowledge receipt of the application within one month of receipt of the file.

**Renewal of pre-declaration**

The declaration must be renewed once a year and in the event of a change in the national's situation.

**Supporting documents**

In order to practise as a second engineer on a fishing vessel, the national sends a file to the competent authority containing the following supporting documents:

- A written statement signed by the national;
- A valid piece of identification for the national;
- a certificate from the eu's state or EEA competent authority certifying that the national is legally established in that state and does not incur any prohibition on practising;
- A certificate justifying the national's professional qualifications;
- a certificate justifying its activity for at least two years in the last ten years, when neither training nor the practice of the profession is regulated in the Member State;
- A certificate justifying that the moral conditions are being met;
- A certificate of physical fitness to navigate
- a certificate of mastery of language skills.

**Timeframe**

The service can begin as long as there is no opposition from the Interregional Directorate of the Sea:

- The expiry of a one-month period from the request for declaration;
- in the event of a request for further information or verification of professional qualifications, at the end of a two-month period from receipt of the full application.

**Please note**

In the event of a request for partial access to the profession, the national will have to take the same steps as for the exercise of the activity on a temporary or casual basis on French territory.

**Good to know: compensation measure**

In order to obtain permission to practise, the person concerned may be required to submit to an aptitude test if it appears that the qualifications and work experience he uses are substantially different from those required for practising the profession in France.

The aptitude test must establish that the relevant knowledge and qualifications are mastered.

**Cost**

Free.

*For further information*: Articles 7-2 to 10 of the February 8, 2010 order.

### b. Obtain a certificate of recognition for EU or EEA nationals for a permanent exercise (LE)

**Competent authority**

The Interregional Director of the Sea, sitting in the administrative region of the fishing vessel's armament port, is competent to issue the certificate of recognition authorizing the permanent exercise of second engineer in France.

**Procedure**

The request for a certificate of recognition is addressed by any means to the competent authority of the administrative region in which it wishes to settle. In the event of a missing document, the competent authority has one month from receipt of the file, to inform the national.

**Supporting documents**

In order to practice as a second mechanic on a permanent basis in France, the person concerned must produce a complete file containing:

- The form[Cerfa No. 14750](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14750.do) Duly completed and signed;
- A valid piece of identification
- a certificate of professional experience issued by the competent authority of the Member State, when the profession is not regulated in that State;
- when requested by the competent authority, if necessary, the training programme leading to the issuance of the title;
- A certificate justifying that the moral conditions are being met;
- A certificate of physical fitness to navigate
- a certificate of mastery of language skills.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Outcome of the procedure**

The competent authority has one month to decide on the application for certification, as soon as the full file is received.

Any decision, whether it is acceptance, refusal or res compensation, must be justified.

The silence kept at the end of a two-month period will be worth the decision to reject the application for recognition.

If the decision is accepted, the competent authority issues the certificate of recognition, which has a validation period of five years.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice, either an adjustment course or an aptitude test, carried out within six months of the decision to competent authority.

**Cost**

Free.

*For further information*: Articles 2 to 5-2 of the February 8, 2010 order.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

