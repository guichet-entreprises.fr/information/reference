﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP195" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Maritime sector" -->
<!-- var(title)="Deck officer on fishing vessels" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="maritime-sector" -->
<!-- var(title-short)="deck-officer-on-fishing-vessels" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/maritime-sector/deck-officer-on-fishing-vessels.html" -->
<!-- var(last-update)="2020-04-15 17:22:17" -->
<!-- var(url-name)="deck-officer-on-fishing-vessels" -->
<!-- var(translation)="Auto" -->


Deck officer on fishing vessels
====================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Chief Watch Officer on the Fishing Vessel ("Officer") is a sailor whose missions are:

- assist the captain or second captain;
- to manoeuvre the vessel while it is in navigation;
- coordinating and holding the shift, day and night;
- to help maintain the safety and security of the vessel and crew;
- to mentor his team at the bridge by planning and organizing his work.

It also contributes to:

- Anti-disaster
- control of the crew's handling and updating of nautical documents;
- to ensure that the navigation devices are operating by the relevant agencies and to take steps to maintain them.

2°. Professional qualifications
----------------------------------------

### a. National requirements

### National legislation

The occupation of Chief Watch Officer at the Fishing Vessel Bridge is reserved for the bridge watch officer's certificate holder.

The issuance of this patent requires the individual to:

- 18 years old on the day the patent application was filed;
- either hold the diploma of officer in charge of the bridge specified below or one of the diplomas in Table 2 of Schedule I of the order of 22 December 2015;
- be certified as validating level II or III medical education;
- Holding the general operator certificate;
- completed a service at sea on the deck for at least twelve months with on-board training. For more information, it is advisable to refer to the[Order of 13 August 2015 relating to training registers on board ships](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031070040&categorieLien=id).

**Please note**

The required certificates and certificates must be valid at the time of the patent application.

*For further information*: Articles 2, 14 and following of the December 22, 2015 order.

### Officer's diploma at the bridge

The Master Watch Officer diploma at the bridge is a certificate justifying the acquisition of modules or success in international initial training of bridge officers.

#### Professional training of officers

This course includes training leading to the acquisition of modules and training leading to the issuance or revalidation of the following certificates or certificates:

- Basic Safety Training Certificate (CFBS),
- Certificate of Advanced FireFighting Qualification (CQALI),
- Certificate of Fitness to Operate Lifeboats and Liferafts (CAEERS),
- certificate certifying the validation of Level II medical education (EM II),
- General Operator Certificate (CGO),
- certificate of fitness for the duties of the ship's security officer,
- basic training certificate for cargo-related operations of liquefied gas tankers,
- basic training certificate for cargo operations of tankers and tankers for chemicals,
- Certification of training in the Electronic Card and Information Visualization System (ECDIS);
- Certification of training in management and teamwork as well as resource management at the bridge and machine;
- training certificate for personnel serving on passenger vessels required in Articles 3 to 6 of the order of 6 May 2014 as a result.

**Admission requirements**

The person who wishes to integrate the professional training of the officers must be the holder:

- A certificate of medical fitness for navigation;
- Captain 500's patent or a patent recognized in the Schedule I list of the December 22, 2015 order;
- certificate justifying the acquisition of the[OCQP probation module](http://www.ucem-nantes.fr/referentiels/cer-pont).

Also available for this training are those who hold:

- A certificate as a marine senior marine technician in the "fishing and management of the marine environment" specialty;
- From the certificate of officer head watch to the machine;
- the certificate of a deckhand at the bridge or the sea certificate qualified bridge.

*For further information*: Articles 6 and following, and Appendixes I, II and III of the December 22, 2015 order.

**The result of the training leading to the acquisition of modules**

Each module of Article 5 of the December 23, 2015 order is acquired when the candidate has completed the[Training](http://www.ucem-nantes.fr/images/stories/documents/new_ref/reforme_filiere_B/OCQP/Annexe-IV.pdf) and averaged 10 out of 20 at all events.

Upon obtaining each module, a certificate is issued to the training candidate, valid for five years.

#### International initial training

The initial international training takes place over three years at the National Maritime Graduate School (ENSM). Its registration is done on the selection of files whose conditions are set by the Director of the NSMS.

For more information on international initial training, please refer to[the ENSM website](https://www.supmaritime.fr/officier-chef-de-quart-passerelle-international-capitaine-3000.html).

*For further information*: order of 5 July 2017 relating to the international initial training course for the diplomas of chief watch officer and captain 3000.

#### Graduation

To obtain the diploma of chief watch officer at the bridge, an individual who has completed the professional training of officers or the initial international training must be the holder of:

- A valid certificate of medical fitness for navigation;
- The certificate justifying the successful follow-up of the initial international training delivered by the Director of the ENSM;
- certificates and certificates validated during the initial international training, attached, if necessary, with the certificate justifying the training of service at sea necessary for their issuance;
- certified english language proficiency certificate at least B1 from the Common European Reference Framework for Languages (CECR).

### Physical fitness

Access to the profession of bridge watch officer on fishing vessels is subject to minimum physical fitness requirements.

These fitness conditions are assessed during required medical visits:

- Prior to access to the profession of officer;
- Before the first boarding;
- before entering maritime training;
- before the expiry of the certificate of medical fitness, the duration of which varies according to the age of the officer.

At the end of the medical visit, the doctor may decide that the officer is fit. From then on, he will be awarded a certificate of aptitude.

When the doctor's decision indicates partial fitness, temporary incapacity or total incapacity, the officer may challenge it with the Regional Medical Commission for Navigation Aptitude (CMRA).

*For further information*: decree of 3 December 2015 on health and medical fitness to navigate.

### Control of language and legal knowledge

In order to practise as a bridge watch officer, the person must have knowledge of French and legal matters.

To justify them, the officer must provide:

- a French secondary or higher education diploma or a certificate of less than one year attesting to a B2 master's degree. For more information, please refer to the Ministry of National Education's website with the[Common European Framework for Languages](http://eduscol.education.fr/cid45678/cadre-europeen-commun-de-reference-cecrl.html) ;
- any diploma of French higher education sanctioning specific training or teaching relating to the powers and prerogatives of public power conferred on the captain of a French-flagged ship.

If the officer does not have any of these documents, he will have to submit to a written test in French and an interview before a jury whose composition is mentioned in Article 5 of the decree of June 2, 2015. The written test and interview will test whether the officer has the legal knowledge necessary for the position, and assess his or her ability to communicate in a professional context and write in the French language.

*For further information*: Articles 3 and following from the[decree of 2 June 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/2/DEVT1422283D/jo/texte),[Section L. 5521-3 of the Transport Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023086525&idArticle=LEGIARTI000023073934&dateTexte=&categorieLien=cid).

### Costs associated with training

Training leading to the diploma of officer in charge of the bridge watch is paid. For more information, it is advisable to refer to the[Maritime Competitions and Reviews Unit](http://www.ucem-nantes.fr/) and the ENSM.

### b. EU or EEA nationals: for temporary or casual exercise (Free Service)

A national of a European Union (EU) or European Economic Area (EEA) state, who is legally acting as a watch officer on fishing vessels in one of these states, may make use of his or her professional title in France, on a temporary or casual basis. He must request it, prior to his first performance, by declaration addressed to the interregional director of the competent sea of the administrative region in which he is identified (cf. infra "5.a. Make a prior declaration EU/EEA nationals engaged in temporary and occasional activity (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least one year, in the ten years before the performance.

**What to know**

In order to be a temporary or casual officer in charge of the bridge watch, the national must have the necessary language skills.

Where there are substantial differences between the training of the national and those required in France, or where the person concerned has not acquired all the skills required to practise in a French-flagged fishing vessel, compensation can be taken (see "5.a. Good to know: compensation measure").

*For further information*: Articles 19 and 20 of the decree of 24 June 2015.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a title issued by a competent authority in another Member State, which regulates access to the profession or its exercise;
- The title presented has its exact equivalent in France;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once he fulfils one of the three previous conditions, he will have to apply for a certificate of recognition from the competent Interregional Director of the Sea. For more information, it is advisable to refer to paragraph 5.b. Obtain a certificate of recognition for EU or EEA nationals for a permanent exercise (LE)."

If, in reviewing the file, the Interregional Director of the Sea finds that there are substantial differences between the professional training and experience of the national and those required to operate on a flagged vessel compensation measures may be taken ("5.b. Good to know: compensation measure")

*For further information*: Articles 4 to 5 of the February 8, 2010 order.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

In order to serve as the officer in charge of the bridge watch, the person must:

- Respect moral conditions
- justify that no reference to the performance of one's duties is recorded on the second bulletin of his criminal record, namely a criminal sentence or a correctional sentence.

These moral conditions are met if the EU or EEA national produces:

- an extract of less than three months from his criminal record from the EU State or EEA according to the provisions of the international conventions in force,
- or a certificate of less than three months from the Member State certifying compliance with these conditions. A certification model is set by a joint decree of the Minister for the Sea and the Minister of Justice.

*For further information*: Article L. 5521-4 of the Transport Code and Article 8 and following of the Decree 24 June 2015.

4°. Insurance
---------------------------------

The officer, who practises his profession as an employee, is covered by the professional liability insurance taken out by his employer, for the acts carried out on occasion.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The Interregional Director of the Sea in the administrative region in which the officer wishes to perform the performance or in which the armament port of the fishing vessel is located, is competent to decide on the declaration. He will acknowledge receipt of the application within one month of receipt of the file.

**Renewal of pre-declaration**

The declaration must be renewed once a year and in the event of a change in the national's situation.

**Supporting documents**

The application to practise in France, on a temporary and casual basis, is a file involving:

- A written statement signed by the national;
- A valid piece of identification for the national;
- a certificate from the eu's state or EEA competent authority certifying that the national is legally established in that state and does not incur any prohibition on practising;
- A certificate justifying the national's professional qualifications;
- a certificate justifying its activity for at least two years in the last ten years, when neither training nor the practice of the profession is regulated in the Member State;
- A certificate justifying that the moral conditions are being met;
- A certificate of physical fitness to navigate
- a certificate of mastery of language skills.

**Timeframe**

The service can begin as long as there is no opposition from the Interregional Directorate of the Sea:

- The expiry of a one-month period from the request for declaration;
- in the event of a request for further information or verification of professional qualifications, at the end of a two-month period from receipt of the full application.

**Please note**

In the event of a request for partial access to the profession, the national will have to take the same steps as for the exercise of the activity on a temporary or casual basis on French territory.

**Cost**

Free.

*For further information*: Articles 7-2 to 9 of the February 8, 2010 order.

**Good to know: compensation measure**

In order to obtain permission to practise, the person concerned may be required to submit to an aptitude test if it appears that the qualifications and work experience he uses are substantially different from those required for practising the profession in France.

The aptitude test must establish that the relevant knowledge and qualifications are mastered.

*For further information*: Article 10 of the February 8, 2010 order.

### b. Obtain a certificate of recognition for EU or EEA nationals for a permanent exercise (LE)

**Competent authority**

The Interregional Director of the Sea, sitting in the administrative region of the fishing vessel's armament port, is competent to issue the certificate of recognition authorizing the permanent exercise of the officer in France.

**Procedure**

The request for a certificate of recognition is addressed by any means to the competent authority of the administrative region in which it wishes to settle. In the event of a missing document, the competent authority has one month from receipt of the file, to inform the national.

**Supporting documents**

In order to carry out the profession of officer in France on a permanent basis, the person concerned must produce a complete file containing:

- The form[Cerfa No.14750](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14750.do) Duly completed and signed;
- A valid piece of identification
- a certificate of professional experience issued by the competent authority of the Member State, when the profession is not regulated in that State;
- when requested by the competent authority, if necessary, the training programme leading to the issuance of the title;
- A certificate justifying that the moral conditions are being met;
- A certificate of physical fitness to navigate
- a certificate of mastery of language skills.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Outcome of the procedure**

The competent authority has one month to decide on the application for certification, as soon as the full file is received.

Any decision, whether it is acceptance, refusal or res compensation, must be justified.

The silence kept at the end of a two-month period will be worth the decision to reject the application for recognition.

If the decision is accepted, the competent authority issues the certificate of recognition, which has a validation period of five years.

**Cost**

Free.

*For further information*: Articles 2 to 4-1, and Article 7 of the February 8, 2010 order.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice, either an adjustment course or an aptitude test, carried out within six months of the decision to competent authority.

*For further information*: Articles 5 to 5-2 of the February 8, 2010 order.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

