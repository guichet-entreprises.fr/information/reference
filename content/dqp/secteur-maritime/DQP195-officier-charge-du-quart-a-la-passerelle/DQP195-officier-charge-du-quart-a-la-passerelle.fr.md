﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP195" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur maritime" -->
<!-- var(title)="Officier chargé du quart à la passerelle sur les navires de pêche" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-maritime" -->
<!-- var(title-short)="officier-charge-du-quart-a-la-passerelle" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-maritime/officier-charge-du-quart-a-la-passerelle-sur-les-navires-de-peche.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="officier-charge-du-quart-a-la-passerelle-sur-les-navires-de-peche" -->
<!-- var(translation)="None" -->

# Officier chargé du quart à la passerelle sur les navires de pêche

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L’officier chef de quart à la passerelle sur les navires de pêche (« officier ») est un marin dont les missions sont :

- d'assister le capitaine ou le second capitaine ;
- d'assurer la manœuvre du navire lorsque celui-ci est en navigation ;
- de coordonner et d'assurer la tenue du quart, de jour comme de nuit ;
- de participer au maintien de la sécurité et de la sûreté du navire et de l'équipage ;
- d'encadrer son équipe à la passerelle en planifiant et en organisant son travail.

Il contribue également :

- à la lutte contre les sinistres ;
- au contrôle de la tenue et de la mise à jour des documents nautiques par l'équipage ;
- à s'assurer du fonctionnement des appareils de navigation par les organismes compétents et en effectuer les démarches pour leur maintenance.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession d'officier chef de quart à la passerelle des navires de pêche est réservée au titulaire du brevet d'officier chef de quart à la passerelle.

La délivrance de ce brevet nécessite au préalable que l'intéressé :

- ait 18 ans au jour du dépôt de la demande de brevet ;
- soit titulaire du diplôme d'officier chef de quart à la passerelle précisé ci-dessous ou de l'un des diplômes du tableau 2 de l'annexe I de l'arrêté du 22 décembre 2015 ;
- soit titulaire du certificat attestant la validation de l'enseignement médical de niveau II ou III ;
- soit titulaire du certificat général d'opérateur ;
- ait accompli un service en mer au pont d'au moins douze mois accompagné d'une formation suivie à bord. Pour en savoir plus, il est conseillé de se reporter à l'arrêté du 13 août 2015 relatif aux registres de formation à bord des navires.

**À noter**

Les attestations et certificats requis doivent être en cours de validité au moment de la demande de brevet.

*Pour aller plus loin* : articles 2, 14 et suivants de l'arrêté du 22 décembre 2015.

#### Diplôme d'officier chef de quart à la passerelle

Le diplôme d'officier chef de quart à la passerelle est une attestation justifiant l'acquisition de modules ou la réussite à une formation initiale internationale des officiers à la passerelle.

#### Formation professionnelle des officiers

Ce cursus contient une formation conduisant à l'acquisition de modules et une formation conduisant à la délivrance ou la revalidation des certificats ou attestations suivants :

- certificat de formation de base à la sécurité (CFBS),
- certificat de qualification avancée à la lutte contre l'incendie (CQALI),
- certificat d'aptitude à l'exploitation des embarcations et radeaux de sauvetage (CAEERS),
- certificat attestant la validation de l'enseignement médical de niveau II (EM II),
- certificat général d'opérateur (CGO),
- certificat d'aptitude aux fonctions d'agent de sûreté du navire,
- certificat de formation de base aux opérations liées à la cargaison des navires-citernes pour gaz liquéfiés,
- certificat de formation de base aux opérations liées à la cargaison des pétroliers et des navires-citernes pour produits chimiques,
- attestation de formation au système de visualisation des cartes électroniques et d'information (ECDIS) ;
- attestation de formation à la direction et au travail en équipe ainsi qu'à la gestion des ressources à la passerelle et à la machine ;
- attestation de formation pour le personnel servant à bord des navires à passagers requises aux articles 3 à 6 de l'arrêté du 6 mai 2014 susvisé.

##### Conditions d'admission

La personne qui souhaite intégrer la formation professionnelle des officiers doit être titulaire :

- d'un certificat d'aptitude médicale à la navigation ;
- du brevet de capitaine 500 ou d'un brevet reconnu dans la liste de l'annexe I de l'arrêté du 22 décembre 2015 ;
- de l'attestation justifiant l'acquisition du [module probatoire OCQP](http://www.ucem-nantes.fr/referentiels/cer-pont).

Peuvent également suivre cette formation, les personnes titulaires :

- d'un brevet de technicien supérieur maritime de la spécialité « pêche et gestion de l'environnement marin » ;
- du brevet d'officier chef de quart à la machine ;
- du certificat de matelot quart à la passerelle ou du certificat marin qualifié pont.

*Pour aller plus loin* : articles 6 et suivants, et annexes I, II et III de l'arrêté du 22 décembre 2015.

##### Issue de la formation menant à l'acquisition de modules

Chaque module de l'article 5 de l'arrêté du 23 décembre 2015 est acquis lorsque le candidat a suivi la [formation](http://www.ucem-nantes.fr/images/stories/documents/new_ref/reforme_filiere_B/OCQP/Annexe-IV.pdf) et obtenu une moyenne supérieure ou égale à 10 sur 20 à l’ensemble des épreuves.

À l'obtention de chaque module, une attestation est délivrée au candidat à la formation, valable pour cinq ans.

#### Formation initiale internationale

La formation initiale internationale se déroule sur trois années à l’École nationale supérieure maritime (ENSM). Son inscription se fait sur sélection de dossier dont les conditions sont fixées par le directeur de l'ENSM.

Pour plus d'informations concernant la formation initiale internationale, il est conseillé de se reporter sur [le site de l'ENSM](https://www.supmaritime.fr/officier-chef-de-quart-passerelle-international-capitaine-3000.html).

*Pour aller plus loin* : arrêté du 5 juillet 2017 relatif au cursus de formation initiale internationale pour l'obtention des diplômes d'officier chef de quart passerelle et de capitaine 3000.

##### Obtention du diplôme

Pour obtenir le diplôme d'officier chef de quart à la passerelle, l'intéressé qui a suivi la formation professionnelle d'officiers ou la formation initiale internationale doit être titulaire :

- d'un certificat d'aptitude médicale à la navigation en cours de validité ;
- de l'attestation justifiant du suivi avec succès de la formation initiale internationale délivrée par le directeur de l'ENSM ;
- des certificats et des attestations validés pendant la formation initiale internationale, joints, le cas échéant, de l'attestation justifiant la formation de service en mer nécessaire à leur délivrance ;
- d'une attestation de maîtrise linguistique certifiée de l'anglais d'au moins niveau B1 du Cadre européen commun de référence pour les langues (CECR).

#### Aptitude physique

L'accès à la profession d'officier chargé du quart à la passerelle sur les navires de pêche est soumis à des conditions minimales d'aptitude physique.

Ces conditions d'aptitude physique sont évaluées lors de visites médicales requises :

- avant l'accès à la profession d'officier ;
- avant le premier embarquement ;
- avant toute entrée en formation maritime ;
- avant l'expiration du certificat d'aptitude médicale dont la durée varie selon l'âge de l'officier.

À l'issue de la visite médicale, le médecin peut décider que l'officier est apte. Dès lors, il se verra remettre un certificat d'aptitude.

Lorsque la décision du médecin indique une aptitude partielle, une inaptitude temporaire ou une inaptitude totale, l'officier peut la contester auprès de la Commission Médicale Régionale d’Aptitude à la Navigation (CMRA).

*Pour aller plus loin* : décret du 3 décembre 2015 relatif à la santé et à l'aptitude médicale à la navigation.

#### Contrôle des connaissances linguistiques et juridiques

Pour exercer la profession d'officier chargé du quart à la passerelle, l'intéressé doit posséder des connaissances en langue française et en matière juridique.

Pour les justifier, l'officier doit fournir :

- un diplôme de l'enseignement secondaire ou supérieur français ou un certificat de moins d'un an attestant d'un niveau de maîtrise B2. Pour en savoir plus, il est conseillé de se reporter au site du ministère de l'éducation nationale présentant le [cadre européen commun de référence pour les langues](http://eduscol.education.fr/cid45678/cadre-europeen-commun-de-reference-cecrl.html) ;
- tout diplôme de l'enseignement supérieur français sanctionnant une formation ou un enseignement spécifique relatif aux pouvoirs et prérogatives de puissance publique conférées au capitaine d'un navire battant pavillon français.

Si l'officier ne possède aucun de ces justificatifs, il devra se soumettre à une épreuve écrite en français et à un entretien devant un jury dont la composition est mentionnée à l'article 5 du décret du 2 juin 2015. L'épreuve écrite et l'entretien permettront de vérifier si l'officier possède les connaissances en matière juridique nécessaire à la fonction, et d'apprécier son aptitude à communiquer dans un contexte professionnel et à rédiger en langue française.

*Pour aller plus loin* : articles 3 et suivants du décret du 2 juin 2015, article L. 5521-3 du Code des transports.

#### Coûts associés à la formation

Les formations menant à l'obtention du diplôme d'officier chargé du quart à la passerelle sont payantes. Pour plus d'informations, il est conseillé de se reporter aux sites de l'[Unité des concours et examens maritimes](http://www.ucem-nantes.fr/) et de l'ENSM.

### b. Ressortissants UE ou EEE : en vue d’un exercice temporaire ou occasionnel (Libre Prestation de Service)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE), exerçant légalement l’activité d'officier chargé du quart à la passerelle sur navires de pêche dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit en faire la demande, préalablement à sa première prestation, par déclaration adressée au directeur interrégional de la mer compétent de la région administrative dans laquelle il est identifié (cf. infra « 5.a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

**À savoir**

Pour exercer à titre temporaire ou occasionnel la profession d'officier chargé du quart à la passerelle, le ressortissant doit posséder les connaissances linguistiques nécessaires.

Lorsqu'il existe des différences substantielles entre la formation du ressortissant et celles exigées en France, ou que l'intéressé n'a pas acquis la totalité des compétences requises pour exercer dans un navire de pêche battant pavillon français, des mesures de compensation peuvent être prises (cf. « 5.a. Bon à savoir : mesure de compensation »).

*Pour aller plus loin* : articles 19 et 20 du décret du 24 juin 2015.

### c. Ressortissants UE ou EEE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- le titre présenté a son équivalent exact en France ;
- il a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une des trois conditions précédentes, il devra demander une attestation de reconnaissance auprès du directeur interrégional de la mer compétent. Pour plus d'information, il est conseillé de se reporter infra au paragraphe « 5.b. Obtenir une attestation de reconnaissance pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE) ».

Si, lors de l'examen du dossier, le directeur interrégional de la mer constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées pour exercer sur un navire battant pavillon français, des mesures de compensation pourront être prises (« 5.b. Bon à savoir : mesure de compensation »)

*Pour aller plus loin* : articles 4 à 5 de l'arrêté du 8 février 2010.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Pour exercer la fonction d'officier chargé du quart à la passerelle, l'intéressé doit :

- respecter des conditions de moralité ;
- justifier qu'aucune mention incompatible avec l'exercice de ses fonctions n'est inscrite sur le bulletin n°2 de son casier judiciaire, à savoir une peine criminelle ou une peine correctionnelle.

Ces conditions de moralité sont respectées si le ressortissant de l'UE ou de l'EEE produit :

- un extrait de moins de trois mois de son casier judiciaire de l’État de l'UE ou de l'EEE selon les dispositions des conventions internationales en vigueur, 
- ou d'une attestation de moins de trois mois de l’État membre certifiant le respect de ces conditions.Un modèle d'attestation est fixé par un arrêté conjoint du ministre chargé de la mer et du ministre de la justice.

*Pour aller plus loin* : article L. 5521-4 du Code des transports et article 8 et suivants du décret 24 juin 2015.

## 4°. Assurance

L'officier, qui exerce sa profession en tant que salarié, est couvert par l'assurance de responsabilité professionnelle souscrite par son employeur, pour les actes effectués à l’occasion cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le directeur interrégional de la mer dans la région administrative dans laquelle l'officier souhaite effectuer la prestation ou dans laquelle se situe le port d’armement du navire de pêche, est compétent pour se prononcer sur la déclaration. Il accusera réception de la demande dans un délai d'un mois à compter de la réception du dossier.

#### Renouvellement de la déclaration préalable

La déclaration doit être renouvelée une fois par an et en cas de changement de situation du ressortissant.

#### Pièces justificatives

La demande d'exercer en France, à titre temporaire et occasionnel, est un dossier comportant :

- une déclaration écrite et signée par le ressortissant ;
- une pièce d'identité en cours de validité du ressortissant ;
- une attestation établie par l'autorité compétente de l’État de l'UE ou de l'EEE certifiant que le ressortissant est légalement établi dans cet État et n'encourt aucune interdiction d'exercer ;
- une attestation justifiant des qualifications professionnelles du ressortissant ;
- une attestation justifiant son activité pendant au moins deux ans au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre ;
- une attestation justifiant que les conditions de moralité sont respectées ;
- un certificat d'aptitude physique à la navigation ;
- une attestation de maîtrise des connaissances linguistiques.

#### Délai

La prestation peut débuter dès lors qu'il n'y a pas d'opposition de la part de la direction interrégionale de la mer :

- à l'expiration d'un délai d'un mois à compter de la demande de déclaration ;
- en cas de demande de complément d'information ou de la vérification des qualifications professionnelles, à l'expiration d'un délai de deux mois à compter de la réception de la demande complète.

**À noter**

En cas de demande d'accès partiel à la profession, le ressortissant devra réaliser les mêmes démarches que pour l'exercice de l'activité à titre temporaire ou occasionnel sur le territoire français.

#### Coût

Gratuit.

*Pour aller plus loin* : articles 7-2 à 9 de l'arrêté du 8 février 2010.

#### Bon à savoir : mesure de compensation

Pour obtenir l’autorisation d’exercer, l’intéressé peut être amené à se soumettre à une épreuve d’aptitude s’il apparaît que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France.

L'épreuve d'aptitude doit permettre d'établir que les connaissances et qualifications concernées sont maîtrisées.

*Pour aller plus loin* : article 10 de l'arrêté du 8 février 2010.

### b. Obtenir une attestation de reconnaissance pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE)

#### Autorité compétente

Le directeur interrégional de la mer siégeant dans la région administrative du port d'armement du navire de pêche, est compétent pour délivrer l'attestation de reconnaissance autorisant l'exercice permanent de l'officier en France.

#### Procédure

La demande d'attestation de reconnaissance est adressée par tout moyen à l'autorité compétente de la région administrative dans laquelle il souhaite s'établir. En cas de document manquant, l'autorité compétente dispose d'un mois à compter de la réception du dossier, pour en informer le ressortissant.

#### Pièces justificatives

Pour exercer à titre permanent la profession d'officier en France, l'intéressé doit produire un dossier complet comportant :

- le formulaire [Cerfa n°14750](https://www.service-public.fr/professionnels-entreprises/vosdroits/R12438) dûment complété et signé ;
- une pièce d'identité en cours de validité ;
- une attestation d'expérience professionnelle délivrée par l'autorité compétente de l’État membre, lorsque la profession n'est pas réglementée dans cet État ;
- lorsqu'il est demandé par l'autorité compétente, le cas échéant, le programme de formation conduisant à la délivrance du titre ;
- une attestation justifiant que les conditions de moralité sont respectées ;
- un certificat d'aptitude physique à la navigation ;
- une attestation de maîtrise des connaissances linguistiques.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Issue de la procédure

L'autorité compétente dispose d'un délai d'un mois pour se prononcer sur la demande d'attestation, dès réception du dossier complet.

Toute décision, qu'elle soit d'acceptation, de refus ou prononçant des mesures de compensation, doit être motivée.

Le silence gardé à l'expiration d'un délai de deux mois vaudra décision de rejet de la demande d'attestation de reconnaissance.

En cas d'acceptation de la décision, l'autorité compétente délivre l'attestation de reconnaissance dont la durée de validation est de cinq ans.

#### Coût

Gratuit.

*Pour aller plus loin* : articles 2 à 4-1, et article 7 de l'arrêté du 8 février 2010.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de compensation de son choix, soit un stage d'adaptation, soit une épreuve d'aptitude, réalisée dans les six mois suivant la décision de l'autorité compétente.

*Pour aller plus loin* : articles 5 à 5-2 de l'arrêté du 8 février 2010.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).