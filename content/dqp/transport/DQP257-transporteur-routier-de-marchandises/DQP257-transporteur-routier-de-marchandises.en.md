﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP257" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Road haulier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="road-haulier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/transport/road-haulier.html" -->
<!-- var(last-update)="August 2021" -->
<!-- var(url-name)="road-haulier" -->
<!-- var(translation)="Auto" -->


Road haulier
====================

Latest update: <!-- begin-var(last-update) -->August 2021<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The trucking company is a professional whose activity is to provide a service for the delivery of goods by means of a motor vehicle.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of a road freight carrier, the professional must:

- Be registered:- trade and corporate register (RCS) or trades directory. It is advisable to refer to the "RCS Registration" sheet for more information,
  - The National Electronic Register of Road Transport Companies;
- meet the requirements:- Settlement
  - professionally honoured,
  - financial and professional capacity.

In addition, any company wishing to operate as a road freight carrier must appoint a transport manager to manage vehicle maintenance, verification of contracts and transport documents, accounting , checking safety procedures and assigning loads or services to drivers.

#### Conditions of access to the profession

**Professional capacity**

The professional wishing to carry out the activity of a road freight carrier must either:

- hold a diploma, a university degree, a certificate of study or a professional designation issued in France by a higher education institution or an authorised body and, as long as he has received the necessary knowledge to do this activity during his training;
- justify having continuously managed a public freight transport, moving or leasing company of industrial vehicles with drivers destined for the transport of goods in one or more European Union states during the 10 years prior to December 4, 2009.

Failing to fulfil any of these conditions, it must submit to a written examination on all the subjects set out in Schedule I of regulation (EC) 1071/2009 of the European Parliament and the Council of 21 October 2009 establishing common rules on the conditions to be met in order to practise as a carrier by road.

**Please note**

The professional who wishes to carry out a transport activity with vehicles not exceeding a maximum authorized weight of 3.5 tons meets this requirement of professional capacity as long as he holds a certificate of capacity in light transport issued by the regional prefect.

*To go further*: Articles R. 3211-36 to R. 3211-42 of the Transportation Code.

**Establishment**

The establishment requirement is met as long as:

- The French-based company holds the company's main documents (accounting documents, personnel management, drivers' working and rest time data, etc.) on its premises;
- The carrier has at least one registered vehicle.
- the company does carry out the activities of these vehicles on an ongoing basis thanks to appropriate administrative equipment and techniques.

**Please note**

If the company is not headquartered in France, these documents must be in its main establishment, if necessary, the professional must inform the regional prefect.

*To go further*: Articles R. 3211-19 to R. 3211-23 of the Transportation Code.

**Financial capacity**

Each year, the professional must, through certified documents (by an accountant, an auditor or a certified management centre), certify his financial capacity.

To do so, it must justify having capital and reserves:

- at least 1,800 euros for the first vehicle and 900 euros for the following, for vehicles not exceeding a maximum allowable weight of 3.5 tons;
- EUR 9,000 for the first vehicle and EUR 5,000 for each of the following vehicles, for vehicles exceeding this limit.

Failing that, the professional may present guarantees granted by one or more financial institutions holding bail, as long as this guarantee does not exceed half of the financial capacity due.

*To go further*: Articles R. 3211-32 to R. 3211-35 of the Transportation Code.

#### Registration in the National Register of Road Transport Companies

**Competent authority**

The professional must apply to the prefect of the region where his company's headquarters are located. Companies not headquartered in France must apply to the prefect of the region where their main establishment is located.

**Supporting documents**

His request must include, as the case may be, the cerfa form No. [16093](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57874) or [16094](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14156) requesting authorization to practise the profession of road transport operator and registration in the register, as well as the supporting documents mentioned.

**Outcome of the procedure**

The registration in this register gives rise to the issuance by the prefect:

- a Community licence when the company uses one or more vehicles with a maximum authorized weight of more than 3.5 tonnes;
- internal transport licence when this limit is not exceeded.

These licences with a renewable 10-year validity are issued with as many certified copies as registered vehicles.

*To go further*: Article R. 3211-9 of the Transportation Code; Order of 28 December 2011 relating to the authorization to exercise the profession of public road transport operator and the terms and conditions of the request for authorization by companies.

3°. Conditions of honorability
-----------------------------------------

No one may carry out the activity of a road freight carrier as long as it has been the subject of either:

- several convictions mentioned in bulletin 2 of his criminal record and leading to a ban on practising a commercial or industrial profession;
- several convictions listed on bulletin 2 of his criminal record, including:- breaches of hazardous material transportation obligations (see Articles L. 1252-5 to L. 1252-7 of the Transportation Code),
  - breaches of contract pricing obligations, falsification of electronic documents or data, or the issuing of false information, or failure to install control devices (see Articles L. 3242-2 to L. 3242-5, L. 3315-4 to L. 3315-6, L. 3315-6, L. 3315-7, L. 3315-9 and L. 3315-10 of the Transportation Code),
  - offences referred to in sections 221-6-1, 222-19-1 and following of the Penal Code (such as wilful or involuntary interference with a person's physical integrity, moral harassment, sexual assault, etc.),
  - offences referred to in Articles L. 654-4 to L. 654-15 of the Code of Commerce (penalties for bankruptcy),
  - Traffic Violations (see Sections L. 221-2 and the following of the Highway Traffic Act),
  - breaches of waste prevention and management obligations (see Articles L. 541-46-5- of the Environment Code).

*To go further*: Article R. 3211-27 of the Transportation Code.

