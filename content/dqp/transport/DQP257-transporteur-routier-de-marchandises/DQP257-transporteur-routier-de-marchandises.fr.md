﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP257" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Transporteur routier de marchandises" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="transporteur-routier-de-marchandises" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/transport/transporteur-routier-de-marchandises.html" -->
<!-- var(last-update)="Août 2021" -->
<!-- var(url-name)="transporteur-routier-de-marchandises" -->
<!-- var(translation)="None" -->

# Transporteur routier de marchandises

Dernière mise à jour : <!-- begin-var(last-update) -->Août 2021<!-- end-var -->

## 1°. Définition de l’activité

Le transporteur routier de marchandises est un professionnel dont l'activité consiste à fournir une prestation de services ayant pour objet la livraison de marchandises au moyen d'un véhicule motorisé.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de transporteur routier de marchandises, le professionnel doit :

- être inscrit :
  - au registre du commerce et des sociétés (RCS) ou au répertoire des métiers,
  - au registre électronique national des entreprises de transport par route ;
- respecter les exigences en matière :
  - d'établissement,
  - d'honorabilité professionnelle,
  - de capacité financière et professionnelle.

En outre, toute entreprise qui souhaite exercer l'activité de transporteur routier de marchandises doit désigner un gestionnaire de transport en vue de gérer l'entretien des véhicules, la vérification des contrats et des documents de transports, la comptabilité, la vérification des procédures en matière de sécurité et l'affectation des chargements ou des services aux conducteurs.

#### Conditions d'accès à la profession

##### Capacité professionnelle

Le professionnel souhaitant exercer l'activité de transporteur routier de marchandises doit soit :

- être titulaire d'un diplôme, d'un titre universitaire, d'un certificat d'études ou d'un titre professionnel délivré en France par un établissement d'enseignement supérieur ou un organisme habilité et dès lors qu'il a reçu les connaissances nécessaires pour exercer cette activité lors de sa formation ;
- justifier avoir géré de manière continue une entreprise de transport public de marchandises, de déménagement ou de location de véhicules industriels avec conducteur destinés au transport de marchandises dans un ou plusieurs État(s) de l'Union européenne durant les dix dernières années précédant le 4 décembre 2009.

À défaut de remplir l'une de ces conditions, il doit se soumettre à un examen écrit portant sur l'ensemble des matières fixées à l'annexe I du règlement (CE) n° 1071/2009 du Parlement européen et du Conseil du 21 octobre 2009 établissant des règles communes sur les conditions à respecter pour exercer la profession de transporteur par route.

**À noter**

Le professionnel qui souhaite exercer une activité de transport avec des véhicules n'excédant pas un poids maximum autorisé de 3,5 tonnes satisfait à cette exigence de capacité professionnelle dès lors qu'il est titulaire d'une attestation de capacité en transport léger délivrée par le préfet de région.

*Pour aller plus loin* : articles R. 3211-36 à R. 3211-42 du Code des transports.

##### Établissement

L'exigence d'établissement est satisfaite dès lors que :

- l'entreprise établie en France détient dans ses locaux les principaux documents de l'entreprise (documents comptables, de gestion du personnel, les données relatives au temps de travail et de repos des conducteurs, etc.) ;
- le transporteur dispose d'au moins un véhicule immatriculé ;
- l'entreprise exerce effectivement et en permanence les activités relatives à ces véhicules grâce à des équipements administratifs adaptés et des techniques appropriées.

**À noter**

Si l'entreprise n'a pas son siège en France, ces documents doivent se trouver au sein de son établissement principal, le cas échéant, le professionnel doit en informer le préfet de région.

*Pour aller plus loin* : articles R. 3211-19 à R. 3211-23 du Code des transports.

##### Capacité financière

Chaque année, le professionnel doit, au moyen de documents certifiés (par un expert-comptable, un commissaire aux comptes ou un centre de gestion agréé), attester de sa capacité financière.

Pour cela, il doit justifier avoir des capitaux et des réserves :

- d'un montant au moins égal à 1 800 euros pour le premier véhicule et 900 euros pour les suivants, pour les véhicules n'excédant pas un poids maximum autorisé de 3,5 tonnes ;
- d'un montant de 9 000 euros pour le premier véhicule et de 5 000 euros pour chacun des véhicules suivants, pour les véhicules excédant cette limite.

À défaut, le professionnel peut présenter des garanties accordées par un ou plusieurs organismes financiers se portant caution, dès lors que cette garantie n'excède pas la moitié de la capacité financière exigible.

*Pour aller plus loin* : articles R. 3211-32 à R. 3211-35 du Code des transports.

#### Inscription au registre national des entreprises de transport par route

##### Autorité compétente

Le professionnel doit adresser sa demande au préfet de région où se trouve le siège de sa société. Les entreprises n'ayant pas de siège en France doivent adresser leur demande au préfet de la région où se situe leur principal établissement.

##### Pièces justificatives

Sa demande doit comporter, selon le cas, le formulaire Cerfa n° [16093](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57874) ou [16094](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14156) de demande d'autorisation d'exercer la profession de transporteur routier et d'inscription au registre, ainsi que les pièces justificatives mentionnées.

##### Issue de la procédure

L'inscription à ce registre donne lieu à la délivrance par le préfet :

- d'une licence communautaire lorsque l'entreprise utilise un ou plusieurs véhicules dont le poids maximal autorisé excède 3,5 tonnes ;
- d'une licence de transport intérieur lorsque cette limite n'est pas excédée.

Ces licences d'une validité de dix ans renouvelable sont délivrées avec autant de copies certifiées conformes que de véhicules immatriculés.

*Pour aller plus loin* : article R. 3211-9 du Code des transports ; arrêté du 28 décembre 2011 relatif à l'autorisation d'exercer la profession de transporteur public routier et aux modalités de la demande d'autorisation par les entreprises.

## 3°. Conditions d’honorabilité

Nul ne peut exercer l'activité de transporteur routier de marchandises dès lors qu'il a fait l'objet soit :

- de plusieurs condamnations mentionnées au bulletin n° 2 de son casier judiciaire et conduisant à une interdiction d'exercer une profession commerciale ou industrielle ;
- de plusieurs condamnations mentionnées au bulletin n° 2 de son casier judiciaire pour notamment :
  - manquements aux obligations relatives au transport de matière dangereuse (cf. articles L. 1252-5 à L. 1252-7 du Code des transports),
  - manquements aux obligations en matière de fixation du prix du contrat, falsification des documents ou des données électroniques, ou avoir délivré de faux renseignements, ou ne pas avoir procédé à l'installation des dispositifs de contrôle (cf. articles L. 3242-2 à L. 3242-5, L. 3315-4 à L. 3315-6, L. 3315-6, L. 3315-7, L. 3315-9 et L. 3315-10 du Code des transports),
  - les infractions mentionnées aux articles 221-6-1, 222-19-1 et suivants du Code pénal (telles que l'atteinte volontaire ou involontaire à l'intégrité physique d'une personne, harcèlement moral, agression sexuelle, etc.),
  - les infractions mentionnées aux articles L. 654-4 à L. 654-15 du Code de commerce (peines complémentaires à la banqueroute),
  - les infractions au Code de la route (cf. articles L. 221-2 et suivants du Code de la route),
  - manquements aux obligations en matière de prévention et de gestion des déchets (cf. articles L. 541-46-5° du Code de l'environnement).

*Pour aller plus loin* : article R. 3211-27 du Code des transports.