﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Taxi driver" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="taxi-driver" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/transport/taxi-driver.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="taxi-driver" -->
<!-- var(translation)="Auto" -->


Taxi driver
===========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The taxi driver is a professional whose activity consists of transporting on-demand, people and their luggage by means of a vehicle of up to eight seats and equipped with special equipment (plate, electronic payment means etc.).

As such, the person concerned, who holds a parking permit, can wait for customers on the public road at places reserved for him or her.

*For further information*: Articles L. 3120-5 and L. 3121-1 of the Transportation Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of taxi driver the professional must be:

- professionally qualified. For this it must either:- have passed a professional exam (see infra "2.2). Training"),
  - justify a professional experience as a taxi driver of at least one year in the last ten years;
- Holder of a parking permit (ADS) and a business card;
- have received level 1 prevention training and civic relief ([PSC 1](http://www.croix-rouge.fr/Je-me-forme/Particuliers/Prevention-et-secours-civiques-de-niveau-1-IRR)) at least two months before the exercise of its activity;
- declared physically fit.

*For further information*: Article R. 3121-17 of the Transport Code; Decree 91-834 on first aid training.

**Parking authorization**

The professional wishing to carry out the activity of taxi driver must have a parking permit allowing him to park or drive on the sites reserved for him in order to wait for the customers. It must also justify its actual and continuous exploitation.

The person concerned must apply for permission (see infra" 5°. a. Request for parking permits").

**Please note**

Specific provisions are provided for holders of a parking permit issued prior to the enactment of the law of 1 October 2014 relating to taxis and chauffeur-driven transport cars. (see Section R. 3121-8 of the Transportation Code).

In addition, the parking permit may be withdrawn by the competent authority as long as the professional:

- is no longer in possession of his business card;
- Makes the request;
- is declared permanently unfit (in case of withdrawal of his driver's licence);
- Dies.

*For further information*: Articles L. 3121-1-2 and following and L. 3121-11 of the Transportation Code.

**Business card**

In order to carry out his activity the taxi driver must:

- Having held a driver's licence for at least three years used in the terms and conditions of Sections R. 221-1 and following of the Highway Traffic Act);
- be professionally fit (see infra"Training");
- to meet the conditions of honorability (see infra"3. Conditions of honorability").

Once the professional meets these conditions, he can apply for the issuance of the professional card (see infra" 5. b. Request for the business card").

**Please note**

The professional is required, once in possession of his business card, to affix it to the windshield of his taxi.

*For further information*: Articles L. 3120-2-2 and R. 3120-6. Transport Code.

**Physical fitness**

In order to carry out his activity, the taxi driver must have carried out a medical check of fitness to drive with a doctor approved by the prefect.

In order to do this, the professional must provide the doctor with the[Form](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14880.do) Cerfa 14880 and the[Form](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14948.do) 14948 relating to French and European driver's licenses as well as the parts mentioned.

After this medical visit, a medical certificate will be given to the prefect who, if necessary, will issue the professional with a certificate of physical fitness.

*For further information*: Article R. 226-1 and the following of the Highway Traffic Act; order of 31 July 2012 relating to the organisation of medical control of fitness to drive.

#### Training

The professional must take an exam that includes written eligibility tests and a practical entrance test.

No one may register for this review if they have been subjected to:

- a permanent withdrawal of his business card within ten years of his application for registration;
- exclusion for fraud from such a review in the past five years.

**Competent authority**

The exam sessions are organised by the Chamber of Trades and Crafts (CMA) of the region in which the professional wishes to practice.

**Supporting documents**

The candidate who has passed the eligibility tests must apply a signed file, including:

- A request to register for the desired session
- A photocopy of a valid ID
- For EU nationals a work permit;
- proof of residence of less than three months
- A photocopy of the driver's licence
- a certificate of physical fitness (see supra" 2. Physical aptitude");
- A photo ID
- settlement of registration fees (cf.[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034379062&fastPos=10&fastReqId=673563316&categorieLien=cid&oldAction=rechTexte) April 6, 2017 setting the amounts of registration fees for the taxi driver and driver of chauffeured transport car exams).

*For further information*: order of 6 April 2017 relating to the programmes and evaluation of the examinations for access to the professions of taxi driver and driver of chauffeured transport car; Article 23 of the Craft Code.

#### Other training

**Continuous training**

The taxi driver is required to take a continuing education course every five years at an approved training centre.

The 14-hour course aims to update the professional's knowledge and includes:

- The following mandatory modules:- the right of special public transport of persons,
  - regulations specific to taxi activity,
  - Road safety
- a module to choose from:- English
  - management and business development (including the use of new information and communication technologies),
  - civic prevention and relief.

At the end of their training, the candidate receives a certificate of follow-up training.

*For further information*: Article R. 3120-8-2 of the Transportation Code; order of 11 August 2017 relating to the continuous training of taxi drivers and drivers of chauffeur-driven transport cars and the mobility of taxi drivers.

**Mobility training**

A professional who wishes to work as a taxi driver in a department other than the one in which he obtained his professional qualification must undergo a 14-hour mobility training, including the following modules:

- Knowledge of the territory
- local regulations.

At the end of this training, the training centre gives a certificate of follow-up and an authorization to exercise to the candidate and to the prefect of the department in which he obtained his examination.

*For further information*: order of August 11, 2017 aforementioned.

#### Costs associated with qualification

The examination leading to the profession of taxi driver is paid and its cost varies depending on the nature of the tests envisaged.

*For further information*: decree of 6 April 2017 setting the amounts of registration fees for the examinations of taxi drivers and drivers of chauffeured transport cars.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) for the exercise of taxi driver activity.

As such, the professional who wishes to carry out this activity on a temporary and casual basis in France is subject to the same requirements as the French national (see supra" 2. a. National requirements").

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any legally established EU national who practises the activity of taxi driver can, permanently, carry out the same activity in France.

For this, the professional must:

- be professionally qualified, i.e.:- Be a certificate of competency or a certificate of proof issued by the competent authority of the state of which he is a national,
  - justify having been a taxi driver for at least one year in the last ten years;
- procedures as the French national (see infra" 5°. a. Steps and formalities for recognition of qualifications");
- justify having the language skills necessary to practice your profession in France.

The professional aptitude will thus be recognized by the prefect of the professional's domicile department or, if necessary, the police prefect.

**Good to know: compensation measure**

Where there are substantial differences between the professional qualification of the national and the training required in France to carry out the activity of taxi driver, the prefect may require that the person concerned submit to an accommodation or an aptitude test.

*For further information*: Article R. 3120-8-1 of the Transportation Code.

3°. Conditions of honorability
-----------------------------------------

The professional who wishes to carry out the activity of taxi driver must not have been subject to:

- a withdrawal of half of the maximum number of points on the driver's licence;
- a final conviction for driving without a driver's licence or refusing to return a licence after it has been invalidated or cancelled;
- a final sentence of six months' imprisonment for one of the following offences:- Flight
  - Scam
  - breach of trust,
  - Wilful interference with the integrity of the person,
  - sexual assault,
  - arms trafficking,
  - extortion,
  - drug trafficking.

*For further information*: Article R. 3120-8 of the Transportation Code.

4°. Insurance and sanctions
-----------------------------------------------

### Insurance

The taxi driver, as a professional, must take out professional liability insurance.

In addition, he must take out insurance for the vehicle, stating:

- The name and address of the insurance company
- The identity of the insured person
- The number of the insurance policy;
- The insurance period
- The characteristics of the vehicle.

*For further information*: Articles L. 3120-4 and R. 3120-4 of the Transportation Code and Section R. 211-15 of the Insurance Code.

### Sanctions

**Administrative sanctions**

The professional incurs administrative penalties if he:

- Does not exploit the parking permit effectively or continuously;
- seriously or repeatedly violates its authorisation or regulations applicable to taxi drivers.

If necessary, the authority that has issued its parking permit may proceed with its final or temporary withdrawal and issue a warning.

*For further information*: Articles L. 3124-1 to L. 3124-5 of the Transportation Code.

**Criminal sanctions**

The vehicle used by the professional for the exercise of his activity must be equipped with the following equipment:

- a horokilometric meter (taximeter);
- an outdoor light device marked "taxi"
- A plate attached to the vehicle and visible from the outside indicating the number of the parking permit and the geographic jurisdiction of that permit;
- A printer to provide billing to the customer
- an electronic payment terminal.

If the vehicle does not have all the above, the professional incurs a fine of 450 euros.

The professional also incurs a fine, the amount of which varies depending on the nature of the offence committed as long as he:

- does not comply with the rates for taxi fares (cf.[Decree](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031285847&categorieLien=cid) No. 2015-1252 of October 7, 2015 relating to taxi fares;
- informs the customer of their availability and location without having the parking permit;
- canvasses its customers;
- does not comply with all the provisions applicable to taxi vehicles or drivers of such vehicles;
- is not in a position to provide insurance to law enforcement in the event of a check-up;
- did not put his business card on his vehicle or present it during a police check.

*For further information*: Articles R. 3124-2 and R. 3124-3 and Articles R. 3124-11 to R. 3124-13 of the Transportation Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for parking permit

**Competent authority**

In order to obtain a parking permit, the professional must register on a waiting list. The request for authorisation must be addressed to the mayor of the place where the professional wishes to practice or to the police prefect for professionals wishing to practice in Paris.

To be on a waiting list, the professional:

- Must hold a business card.
- Must not be in possession of a parking permit;
- must not be placed on another waiting list.

**Supporting documents**

For more information on the terms of the application, it is advisable to get closer to the town hall concerned.

**Please note**

The authority responsible for issuing the authorisations may also impose common distinctive signs on all taxis (color).

**Outcome of the procedure**

The professional may only carry out his activity in the jurisdiction mentioned by his authorisation. The validity of this authorization is five years renewable.

*For further information*: Article L. 3121-1- to L. 3121-8 and R. 3121-4, R. 3121-12 to R. 3121-13 of the Transportation Code.

### b. Request for business card

**Competent authority**

The professional must address his request to the prefect of department or the prefect of police in his area of competence.

**Supporting documents**

It is advisable to approach the prefecture in which the professional wishes to practice to know the list of parts to be provided at the time of the application.

**Timeframe**

The prefecture issues the business card within three months of filing the application.

*For further information*: Articles L. 3120-2-2 and R. 3121-16 of the Transportation Code

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

