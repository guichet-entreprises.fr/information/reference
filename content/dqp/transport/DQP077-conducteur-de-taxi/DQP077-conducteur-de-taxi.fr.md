﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Conducteur de taxi" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="conducteur-de-taxi" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/transport/conducteur-de-taxi.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="conducteur-de-taxi" -->
<!-- var(translation)="None" -->

# Conducteur de taxi

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

Le conducteur de taxi est un professionnel dont l'activité consiste à transporter à la demande, des personnes et leurs bagages au moyen d'un véhicule de huit places assises maximum et muni d'équipements spéciaux (plaque, moyens de paiement électronique etc.).

L'intéressé, titulaire d'une autorisation de stationnement peut, à ce titre, attendre la clientèle sur la voie publique à des endroits qui lui sont réservés.

*Pour aller plus loin* : articles L. 3120-5 et L. 3121-1 du Code des transports.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de conducteur de taxi le professionnel doit être :

- qualifié professionnellement. Pour cela il doit soit :
  - avoir réussi un examen professionnel (cf. infra « 2°. Formation »),
  - justifier d'une expérience professionnelle en tant que conducteur de taxi d'au moins un an au cours des dix dernières années ;
- titulaire d'une autorisation de stationnement (ADS) et d'une carte professionnelle ;
- avoir suivi une formation de prévention et des secours civiques de niveau 1 ([PSC 1](http://www.croix-rouge.fr/Je-me-forme/Particuliers/Prevention-et-secours-civiques-de-niveau-1-IRR)) au moins deux mois avant l'exercice de son activité ;
- déclaré apte physiquement.

*Pour aller plus loin* : article R. 3121-17 du Code des transports ; décret n° 91-834 relatif à la formation aux premiers secours.

##### Autorisation de stationnement

Le professionnel souhaitant exercer l'activité de conducteur de taxi doit être titulaire d'une autorisation de stationnement lui permettant de stationner ou de circuler sur les emplacements qui lui sont réservés en vue d'attendre la clientèle. Il doit en outre, justifier de son exploitation effective et continue.

L'intéressé doit effectuer une demande d'autorisation (cf. infra « 5°. a. Demande d'autorisation de stationnement »).

**À noter**

Des dispositions spécifiques sont prévues pour les titulaires d'une autorisation de stationnement délivrée avant la promulgation de la loi du 1er octobre 2014 relative aux taxis et aux voitures de transport avec chauffeur. (cf. article R. 3121-8 du Code des transports).

En outre, l'autorisation de stationnement peut être retirée par l'autorité compétente dès lors que le professionnel :

- n'est plus en possession de sa carte professionnelle ;
- en fait la demande ;
- est déclaré inapte définitivement (en cas de retrait de son permis de conduire) ;
- décède.

*Pour aller plus loin* : articles L. 3121-1-2 et suivants et L. 3121-11 du Code de transports.

##### Carte professionnelle

Pour exercer son activité le conducteur de taxi doit :

- être titulaire d'un permis de conduire du véhicule depuis au moins trois ans utilisé selon les modalités prévues aux articles R. 221-1 et suivants du Code de la route) ;
- être apte professionnellement (cf. infra « Formation ») ;
- satisfaire aux conditions d'honorabilité (cf. infra « 3°. Conditions d'honorabilité »).

Dès lors que le professionnel rempli ces conditions, il peut effectuer une demande de délivrance de la carte professionnelle (cf. infra « 5°. b. Demande en vue d'obtenir la carte professionnelle »).

**À noter**

Le professionnel est tenu, une fois en possession de sa carte professionnelle, de l'apposer sur le pare-brise de son taxi.

*Pour aller plus loin* : articles L. 3120-2-2 et R. 3120-6. du Code des transports.

##### Aptitude physique

Le conducteur de taxi doit, pour exercer son activité, avoir effectué un contrôle médical d'aptitude à la conduite auprès d'un médecin agréé par le préfet.

Le professionnel doit, pour cela, fournir au médecin le formulaire [Cerfa 14880](https://www.service-public.fr/particuliers/vosdroits/R14006) et le formulaire [Cerfa 14948](https://www.service-public.fr/particuliers/vosdroits/R32666) relatifs aux permis de conduire français et européen ainsi que les pièces mentionnées.

Après cette visite médicale, un certificat médical sera remis au préfet qui, le cas échéant, délivrera au professionnel une attestation d'aptitude physique.

*Pour aller plus loin* : article R. 226-1 et suivants du Code de la route ; arrêté du 31 juillet 2012 relatif à l'organisation du contrôle médical de l'aptitude à la conduite.

#### Formation

Le professionnel doit se soumettre à un examen comprenant des épreuves écrites d'admissibilité et une épreuve pratique d'admission.

Nul ne peut s'inscrire à cet examen s'il a fait l'objet :

- d'un retrait définitif de sa carte professionnelle dans les dix ans précédents sa demande d'inscription ;
- d'une exclusion pour fraude à un tel examen au cours des cinq dernières années.

##### Autorité compétente

Les sessions d'examen sont organisées par la chambre des métiers et de l'artisanat (CMA) de la région au sein de laquelle le professionnel souhaite exercer.

##### Pièces justificatives

Le candidat ayant réussi les épreuves d'admissibilité doit adresser, pour se présenter à l'épreuve d'admission un dossier signé et comprenant :

- une demande d'inscription à la session souhaitée ;
- une photocopie d'une pièce d'identité en cours de validité ;
- pour les ressortissants de l'UE une autorisation de travail ;
- un justificatif de domicile de moins de trois mois ;
- une photocopie du permis de conduire ;
- une attestation d'aptitude physique (cf. supra « 2°. Aptitude physique ») ;
- une photo d'identité ;
- le règlement des frais d'inscription (cf. arrêté du 6 avril 2017 fixant les montants des droits d'inscription aux épreuves des examens de conducteur de taxi et de conducteur de voiture de transport avec chauffeur).

*Pour aller plus loin* : arrêté du 6 avril 2017 relatif aux programmes et à l'évaluation des épreuves des examens d'accès aux professions de conducteur de taxi et de conducteur de voiture de transport avec chauffeur ; article 23 du Code de l'artisanat.

#### Autres formations

##### Formation continue

Le conducteur de taxi est tenu de suivre un stage de formation continue tous les cinq ans dans un centre de formation agréé.

Cette formation d'une durée de quatorze heures, a pour but de mettre à jour les connaissances du professionnel et comporte :

- les modules obligatoires suivants :
  - droit du transport public particulier des personnes,
  - réglementation spécifique à l'activité de taxi,
  - sécurité routière ;
- un module au choix parmi les suivants :
  - anglais,
  - gestion et développement commercial (dont l'utilisation des nouvelles technologies de l'information et de la communication),
  - la prévention et secours civiques.

À l'issue de sa formation, le candidat reçoit une attestation de suivi de la formation continue.

*Pour aller plus loin* : article R. 3120-8-2 du Code des transports ; arrêté du 11 août 2017 relatif à la formation continue des conducteurs de taxi et des conducteurs de voiture de transport avec chauffeur et à la mobilité des conducteurs de taxi.

##### Formation à la mobilité

Le professionnel qui souhaite exercer son activité de conducteur de taxi dans un autre département que celui dans lequel il a obtenu sa qualification professionnelle doit suivre une formation de mobilité de quatorze heures, comprenant les modules suivants :

- la connaissance du territoire ;
- la réglementation locale.

À l'issue de cette formation, le centre de formation remet une attestation de suivi et une autorisation d'exercice au candidat ainsi qu'au préfet du département dans lequel il a obtenu son examen.

*Pour aller plus loin* : arrêté du 11 août 2017 susvisé.

#### Coûts associés à la qualification

L'examen menant à la profession de conducteur de taxi est payante et son coût varie selon la nature des épreuves envisagées.

*Pour aller plus loin* : arrêté du 6 avril 2017 fixant les montants des droits d'inscription aux épreuves des examens de conducteurs de taxi et de conducteur de voiture de transport avec chauffeur.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre prestation de services)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) pour l'exercice de l'activité de conducteur de taxi.

A ce titre, le professionnel qui souhaite exercer cette activité à titre temporaire et occasionnel en France est soumis aux mêmes exigences que le ressortissant français (cf. supra « 2°. a. Exigences nationales »).

### c. Ressortissants UE : en vue d’un exercice permanent (Libre établissement)

Tout ressortissant de l'UE légalement établi et exerçant l'activité de conducteur de taxi peut, exercer de manière permanente, la même activité en France.

Pour cela, le professionnel doit :

- être qualifié professionnellement, à savoir :
  - être titulaire d'une attestation de compétences ou d'un titre deformation délivré par l'autorité compétente de l’État dont il est ressortissant,
  - justifier avoir exercé l'activité de conducteur de taxi pendant au moins un an au cours des dix dernières années ;
- procéder aux mêmes formalités que le ressortissant français (cf. infra « 5°. a. Démarches et formalités de reconnaissance de qualifications ») ;
- justifier être titulaire des connaissances linguistiques nécessaires à l'exercice de sa profession en France.

L'aptitude professionnelle sera ainsi reconnue par le préfet du département de domiciliation du professionnel ou, le cas échéant, le préfet de police.

#### Bon à savoir : mesure de compensation

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France pour exercer l'activité de conducteur de taxi, le préfet peut exiger que l'intéressé se soumette à un stage d'adaptation ou une épreuve d'aptitude.

*Pour aller plus loin* : article R. 3120-8-1 du Code des transports.

## 3°. Conditions d’honorabilité

Le professionnel qui souhaite exercer l'activité de conducteur de taxi ne doit pas avoir fait l'objet :

- d'un retrait de la moitié du nombre maximal des points du permis de conduire ;
- d'une condamnation définitive pour conduite sans permis de conduire ou refus de restituer son permis après son invalidation ou annulation ;
- d'une condamnation définitive à une peine de six mois d'emprisonnement pour l'une des infractions suivantes :
  - vol,
  - escroquerie,
  - abus de confiance,
  - atteinte volontaire à l'intégrité de la personne,
  - agression sexuelle,
  - trafic d'armes,
  - extorsion de fonds,
  - trafic de stupéfiants.

*Pour aller plus loin* : article R. 3120-8 du Code des transports.

## 4°. Assurances et sanctions

### Assurance

Le conducteur de taxi, en qualité de professionnel doit souscrire une assurance de responsabilité civile professionnelle.

En outre, il doit souscrire à une assurance pour le véhicule, mentionnant :

- la dénomination et l'adresse de l'entreprise d'assurance ;
- l'identité de la personne assurée ;
- le numéro de la police d'assurance ;
- la période d'assurance ;
- les caractéristiques du véhicule.

*Pour aller plus loin* : articles L. 3120-4 et R. 3120-4 du Code des transports et article R. 211-15 du Code des assurances.

### Sanctions

#### Sanctions administratives

Le professionnel encourt des sanctions administratives dès lors qu'il :

- n'exploite pas l'autorisation de stationnement de manière effective ou continue ;
- viole de manière grave ou répétée son autorisation ou la réglementation applicable aux conducteurs de taxi.

Le cas échéant, l'autorité ayant délivré son autorisation de stationnement peut procéder à son retrait définitif ou temporaire et lui adresser un avertissement.

*Pour aller plus loin* : articles L. 3124-1 à L. 3124-5 du Code des transports.

#### Sanctions pénales

Le véhicule utilisé par le professionnel pour l'exercice de son activité doit être doté des équipements suivants :

- un compteur horokilométriques (taximètre) ;
- un dispositif lumineux extérieur portant la mention « taxi » ;
- une plaque fixée sur le véhicule et visible de l'extérieur indiquant le numéro de l'autorisation de stationnement et le ressort géographique de cette autorisation ;
- une imprimante permettant de fournir une facturation au client ;
- un terminal de paiement électronique.

Dès lors que le véhicule ne dispose pas de l'ensemble des éléments précités, le professionnel encourt une amende de 450 euros.

Le professionnel encourt également une amende dont le montant varie selon la nature de l'infraction commise dès lors qu'il :

- ne respecte pas les tarifs prévus en matière de courses de taxi (cf. décret n° 2015-1252 du 7 octobre 2015 relatif aux courses de taxi ;
- informe le client de sa disponibilité et de sa localisation sans être titulaire de l'autorisation de stationnement ;
- procède au démarchage de sa clientèle ;
- ne respecte pas l'ensemble des dispositions applicables aux véhicules de taxi ou aux conducteurs de tels véhicules ;
- n'est pas en mesure de présenter un justificatif d'assurance aux forces de l'ordre en cas de contrôle ;
- n'a pas apposé sa carte professionnelle sur son véhicule ou ne la présente pas lors d'un contrôle par les forces de l'ordre.

*Pour aller plus loin* : articles R. 3124-2 et R. 3124-3 et articles R. 3124-11 à R. 3124-13 du Code des transports.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande d'autorisation de stationnement

#### Autorité compétente

Le professionnel doit, en vue d'obtenir une autorisation de stationnement, s'inscrire sur une liste d'attente. La demande d'autorisation doit être adressée au maire du lieu où le professionnel souhaite exercer ou au préfet de police pour les professionnels souhaitant exercer à Paris.

Pour être inscrit sur une liste d'attente, le professionnel :

- doit être titulaire d'une carte professionnelle ;
- ne doit pas être en possession d'une autorisation de stationnement ;
- ne doit pas être inscrit sur une autre liste d'attente.

#### Pièces justificatives

Pour plus d'informations concernant les modalités de la demande, il est conseillé de se rapprocher de la mairie concernée.

**À noter**

L'autorité compétente pour délivrer les autorisations peut également imposer des signes distinctifs communs à l'ensemble des taxis (couleur).

#### Issue de la procédure

Le professionnel ne peut exercer son activité que dans le ressort mentionné par son autorisation. La durée de validité de cette autorisation est de cinq ans renouvelables.

*Pour aller plus loin* : article L. 3121-1- à L. 3121-8 et R. 3121-4, R. 3121-12 à R. 3121-13 du Code des transports.

### b. Demande en vue d'obtenir la carte professionnelle

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet de département ou au préfet de police dans sa zone de compétence.

#### Pièces justificatives

Il est conseillé de se rapprocher de la préfecture dans laquelle le professionnel souhaite exercer pour connaître la liste des pièces à fournir lors de la demande.

#### Délais

La préfecture délivre la carte professionnelle dans un délai de trois mois à compter du dépôt de la demande.

*Pour aller plus loin* : articles L. 3120-2-2 et R. 3121-16 du Code des transports

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).