﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP261" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Private hire vehicle driver" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="private-hire-vehicle-driver" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/transport/private-hire-vehicle-driver.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="private-hire-vehicle-driver" -->
<!-- var(translation)="Auto" -->



Private hire vehicle driver
=====================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The driver of a chauffeur-driven transport car (VTC) is a professional whose activity consists of transporting persons and their luggage with advance reservations using a vehicle with at least four and no more than nine seats, including that driver.

The driver may operate as a chauffeur-driven transport car operator (VTC), provided he is registered in the VTC register.

#### Activity regulation

Unlike taxis, the VTC driver is not eligible for parking authorization:

- He must justify a pre-booking to take care of a customer;
- it is forbidden to practice marauding, i.e. to stop, park or drive on the lane open to public traffic in search of customers;
- it cannot park on the lane open to public traffic at the front of stations and terminals more than an hour before the customer who has made a pre-booking is taken care of;
- Upon completion of the service, he is required to return to the location of the operator of that car or to a place, off the roadway, where parking is permitted, unless he justifies another advance reservation or a contract with the end customer.

*To go further:* Sections L. 3120-2 and L. 3122-9 of the Transportation Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

The VTC driver must meet certain installation and operating conditions, including:

- Have a driver's licence
- Meet a condition of professional fitness
- meet a condition of professional honour.

In addition, the driver must have taken out a liability insurance policy and must apply to the administrative authority for a professional VTC driver card.

*To go further:* Articles L. 3120-2, L.3120-4, R. 3120-1 to R.3120-9 of the Transportation Code.

#### Driver's licence conditions

The VTC driver must have a driver's licence to drive the vehicle used. The probationary period of the driver's licence must be expired. For nationals of a Member State of the European Union or a State party to the European Economic Area, the VTC driver must hold a licence that has been issued to him for more than three years.

*To go further:* Section R. 3120-6 and R. 3120-8-1 of the Transportation Code.

#### Condition of professional aptitude

The status of professional fitness can be verified by passing a vtc driver's entrance exam. This examination includes written eligibility tests and a practical entrance test, the program and tests of which are defined by joint order of the Minister for Transport and the Minister responsible for the Economy.

The written tests consist of a common core between candidates for the profession of taxi and VTC driver, and specific tests for the VTC driver profession. The practical test of admission consists of a situation.

The exam sessions are organised by the Chambers of Trades and Crafts (CMA). The candidate must pay a registration fee (approximately 202 euros in 2019 for all the tests). Registration must be done online[on the dedicated platform](https://examentaxivtc.fr). Applicants are strongly advised to pay their registration fees online by credit card.

The supporting documents to be provided at the time of registration consist of:

- A recent identity photograph
- A copy of the valid proof of identity (CNI, passport);
- proof of residence of less than 3 months
- a two-sided photocopy of driver's licence B;
- for foreigners from a non-EU state, the work permit mentioned in Article L.5221-2 of the Labour Code;
- a medical certificate, original and issued by a medical officer, as defined in The II of Article R. 221-11 of the Highway Traffic Act, less than two years old;
- payment of registration fees, by cheque or directly on the registration platform.

A candidate is not allowed to take the exam if he or she has been subjected to:

- a permanent withdrawal of his VTC driver's business card in the ten years prior to his application;
- exclusion for fraud on the entry exam in the VTC driver profession for the past five years.

*To go further:* Articles L. 3120-2-1 and R. 3120-7 of the Transportation Code; Articles 24-24-2 and 26 of the Craft Code; 6 April 2017 order on the programmes and evaluation of the examinations for access to the professions of taxi driver and driver of chauffeured transport car; Order of 6 April 2017 setting the amounts of registration fees for the examinations of taxi driver and driver of chauffeured transport car; general regulation of the exam approved by CMA France, accessible on the sites of the regional chambers of crafts and crafts.

The condition of professional fitness can also be determined by equivalence. In this case, the driver must justify a minimum of one year's work experience, full-time or part-time for an equivalent period of time, in the functions of professional driver of transport of persons, during the ten years preceding the application for a business card.

*To go further:* Section R. 3122-11 of the Transportation Code.

#### Physical fitness

In order to carry out his activity, the VTC driver must have carried out a medical check of fitness to drive with a doctor approved by the prefect. After this medical visit, if declared fit, the driver is given a certificate issued by the prefect.

*To go further:* Article R. 221-10 of the Highway Traffic Act; order of 31 July 2012 relating to the organisation of medical control of fitness to drive.

#### Continuous training

The VTC driver is required to complete a continuing education course every five years at an approved training centre. The purpose of this 14-hour training is to update the professional's knowledge. It has several mandatory modules, and a module of choice in a predefined list. At the end of their training, the candidate receives a certificate of follow-up training.

*To go further:* Article R. 3120-8-2 of the Transport Code; order of 11 August 2017 relating to the continuous training of taxi drivers and drivers of chauffeur-driven transport cars and the mobility of taxi drivers.

### b. EU nationals: for temporary and casual exercise (free provision of services)

There is no specific provision for the national of a Member State of the European Union or a State party to the Agreement on the European Economic Area for the exercise of VTC driver activity on a temporary and occasional basis. As such, the professional who wishes to carry out this activity on a temporary and casual basis in France is subject to the same requirements as the French national.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Nationals of a Member State of the European Union or a State party to the European Economic Area can exercise the profession of VTC driver on the national territory in a sustainable manner. To do this, they must first justify their professional suitability:

- either by the production of a certificate of competency or a training certificate issued by the competent authority of one of these states, when such certification or title is required to perform VTC's driving services (mutual recognition Qualifications);
- either through the production of parts that establish work experience of at least one year full-time, or part-time for an equivalent period of time, over the last ten years (equivalence);
- either by passing the entrance exam.

Specific provisions apply to cases where there is a difference between the training received by an applicant in his or her home state, and the skills that must be validated by the VTC examination under the Transport Code. In this case, if the skills acquired by the applicant during his professional experience or through the training he received in his Home State do not cover this difference or discrepancy, with respect to the subjects essential to the exercise of the VTC driver's activity, the department prefect, or the prefect of police, as the case may be, may require the applicant to pass an aptitude test or to take an adjustment course, in order to compensate for this difference.

Nationals of the European Union or a State party to the European Economic Area must have a level in French sufficient to practise the profession. A control of the level of knowledge of the language can be organised if there is a serious and concrete doubt about the sufficient level of language knowledge of the professional in relation to the activities he intends to carry out.

*To go further:* Section R. 3120-8-1 of the Transportation Code.

3°. Conditions of honorability
-----------------------------------------

The professional who wishes to carry out the activity of VTC driver must meet a condition of professional honorability. Also, it must not have been the subject of:

- a reduction of half the maximum number of points on a driver's licence;
- a final conviction for driving a vehicle without a licence or for refusing to return a driver's licence after the driver's licence has been invalidated or cancelled;
- a final sentence handed down by a french or foreign court to a criminal sentence or a correctional sentence of at least six months' imprisonment for theft, fraud, breach of trust, wilful infringement of the integrity of the sexual assault, arms trafficking, extortion or drug offences.

*To go further:* Section R. 3120-8 of the Transportation Code.

(4) Administrative and criminal sanctions
-----------------------------------------

For operators of chauffeur-made transport vehicles, failure to register with the VTC register is punishable by one year's imprisonment and a fine of 15,000 euros. The natural operator may also be suspended for up to 5 years, forfeiture of his vehicle or immobilized for up to one year.

The fact that a VTC driver takes care of a passenger without prior reservation or practice marauding exposes him to the same penalties.

In addition, the driver incurs the temporary or permanent withdrawal of his business card in the event of a violation of the regulations applicable to the particular public transport of persons.

*To go further:* Sections L. 3124-7, L. 3124-11 and L. 3124-12 of the Transportation Code.

5°. Application for VTC driver's business card
--------------------------------------------------------

Holding a professional VTC driver's card is mandatory to practice the profession.

#### Competent authority

The professional must submit his request to the prefect of the department of the place of residence.

#### Supporting documents

It is advisable to approach the prefecture in which the professional wishes to practice to know the list of parts to be provided at the time of the application.

#### Time

The prefecture issues the VTC driver's business card within three months of filing the application.

#### Bligations

A VTC driver using his vehicle professionally must place his VTC driver's business card on the windshield or, failing that, on the vehicle in such a way that the photograph is easily visible from the outside.

*To go further:* Sections L. 3120-2-2 and R. 3120-6 of the Transportation Code.

