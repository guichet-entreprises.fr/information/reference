﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP261" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Conducteur de voiture de transport avec chauffeur (VTC)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="conducteur-de-voiture-de-transport" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/transport/conducteur-de-voiture-de-transport-avec-chauffeur-vtc.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="conducteur-de-voiture-de-transport-avec-chauffeur-vtc" -->
<!-- var(translation)="None" -->

# Conducteur de voiture de transport avec chauffeur (VTC)

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

Le conducteur de voiture de transport avec chauffeur (VTC) est un professionnel dont l’activité consiste à transporter sur réservation préalable des personnes et leurs bagages au moyen d’un véhicule comportant au moins quatre et au plus neuf places, y compris celle du conducteur. 

Le conducteur peut exercer son activité en tant qu’exploitant de voiture de transport avec chauffeur (VTC), à condition d’être inscrit au registre des VTC.

#### Réglementation relative à l’activité

À la différence des taxis, le conducteur de VTC n’est pas éligible à l’autorisation de stationnement :

- il doit justifier d’une réservation préalable pour prendre en charge un client ;
- il lui est interdit de pratiquer la maraude, c’est-à-dire de s'arrêter, stationner ou circuler sur la voie ouverte à la circulation publique en quête de clients ;
- il ne peut stationner sur la voie ouverte à la circulation publique à l'abord des gares et des aérogares plus d’une heure avant la prise en charge du client qui a effectué une réservation préalable ;
- dès l'achèvement de la prestation, il est tenu de retourner au lieu d'établissement de l'exploitant de cette voiture ou dans un lieu, hors de la chaussée, où le stationnement est autorisé, sauf s'il justifie d'une autre réservation préalable ou d'un contrat avec le client final.

*Pour aller plus loin* : articles L. 3120-2 et L. 3122-9 du Code des transports.

## 2°. Qualifications professionnelles

### a. Exigences nationales

Le conducteur de VTC doit remplir certaines conditions d’installation et d’exploitation, et notamment :

- être titulaire du permis de conduire ;
- satisfaire à une condition d’aptitude professionnelle ;
- satisfaire à une condition d’honorabilité professionnelle.

En outre, le conducteur doit avoir souscrit un contrat d’assurance de responsabilité civile et doit solliciter auprès de l’autorité administrative une carte professionnelle de conducteur de VTC. 

*Pour aller plus loin* : articles L. 3120-2, L.3120-4, R. 3120-1 à R.3120-9 du Code des transports.

#### Conditions relatives au permis de conduire

Le conducteur de VTC doit être titulaire d'un permis de conduire autorisant la conduite du véhicule utilisé. Le délai probatoire du permis de conduire doit être expiré. Pour les ressortissants d'un État membre de l'Union européenne ou d'un État partie à l'Espace économique européen, le conducteur de VTC doit être titulaire d'un permis qui lui a été délivré depuis plus de trois ans.

*Pour aller plus loin* : article R. 3120-6 et R. 3120-8-1du Code des transports.

#### Condition d’aptitude professionnelle

La condition d’aptitude professionnelle peut être vérifiée par la réussite à un examen d’entrée dans la profession de conducteur de VTC. Cet examen comprend des épreuves écrites d'admissibilité et une épreuve pratique d'admission dont le programme et les épreuves sont définis par arrêté conjoint du ministre chargé des transports et du ministre chargé de l’économie.

Les épreuves écrites sont composées d’un tronc commun entre les candidats à la profession de conducteur de taxi et de VTC, et d’épreuves spécifiques pour la profession de conducteur de VTC. L’épreuve pratique d’admission consiste en une mise en situation.

Les sessions d’examen sont organisées par les chambres des métiers et de l'artisanat (CMA). Le candidat doit s’acquitter de frais d’inscription (environ 202 € en 2019 pour l’ensemble des épreuves). L’inscription doit être effectuée en ligne [sur la plateforme dédiée](https://examentaxivtc.fr). Il est fortement recommandé aux candidats de régler leur frais d’inscription en ligne, par carte bancaire.

Les pièces justificatives à fournir au moment de l’inscription se composent des documents suivants : 

- une photographie d’identité récente ;
- une copie du justificatif d’identité en cours de validité (CNI, passeport) ;
- un justificatif de domicile de moins de 3 mois ;
- une photocopie recto-verso du permis de conduire B ;
- pour les étrangers ressortissants d’un État non-membre de l’Union européenne, l’autorisation de travail mentionnée au II de l’article L.5221-2 du Code du travail ;
- un certificat médical, original et délivré par un médecin agréé, tel que défini au II de l’article R. 221-11 du Code de la route, datant de moins de deux ans ;
- le paiement des droits d’inscription, par chèque ou directement sur la plateforme d’inscription.

Un candidat n’est pas autorisé à s’inscrire à cet examen s’il a fait l’objet :

- d’un retrait définitif de sa carte professionnelle de conducteur de VTC dans les dix ans précédant sa demande d’inscription ;
- d’une exclusion pour fraude à l’examen d’entrée dans la profession de conducteur de VTC au cours des cinq dernières années.

*Pour aller plus loin* : articles L. 3120-2-1 et R. 3120-7 du Code des transports ; articles 24 à 24-2 et 26 du Code de l'artisanat ; arrêté du 6 avril 2017 relatif aux programmes et à l'évaluation des épreuves des examens d'accès aux professions de conducteur de taxi et de conducteur de voiture de transport avec chauffeur ; arrêté du 6 avril 2017 fixant les montants des droits d'inscription aux épreuves des examens de conducteur de taxi et de conducteur de voiture de transport avec chauffeur ; règlement général de l’examen approuvé par CMA France, accessible sur les sites des chambres régionales des métiers et de l’artisanat. 

La condition d’aptitude professionnelle peut également être constatée par équivalence. Dans ce cas, le conducteur doit justifier d’une expérience professionnelle d'une durée minimale d'un an, à temps plein ou à temps partiel pour une durée équivalente, dans des fonctions de conducteur professionnel de transport de personnes, au cours des dix années précédant la demande de carte professionnelle.

*Pour aller plus loin* : article R. 3122-11 du Code des transports.

#### Aptitude physique

Le conducteur de VTC doit, pour exercer son activité, avoir préalablement effectué un contrôle médical d’aptitude à la conduite auprès d’un médecin agréé par le préfet. Après cette visite médicale, s’il est déclaré apte, le conducteur se voit remettre une attestation délivrée par le préfet. 

*Pour aller plus loin* : article R. 221-10 du Code de la route ; arrêté du 31 juillet 2012 relatif à l’organisation du contrôle médical de l’aptitude à la conduite.

#### Formation continue

Le conducteur de VTC est tenu de suivre un stage de formation continue tous les cinq ans dans un centre de formation agréé. Cette formation, d’une durée de quatorze heures, a pour but de mettre à jour les connaissances du professionnel. Elle comporte plusieurs modules obligatoires, et un module au choix dans une liste prédéfinie. À l’issue de sa formation, le candidat reçoit une attestation de suivi de la formation continue.

*Pour aller plus loin* : article R. 3120-8-2 du Code des transports ; arrêté du 11 août 2017 relatif à la formation continue des conducteurs de taxi et des conducteurs de voiture de transport avec chauffeur et à la mobilité des conducteurs de taxi.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Aucune disposition spécifique n’est prévue pour le ressortissant d’un État membre de l’Union européenne ou d’un État partie à l’accord sur l’Espace économique européen pour l’exercice de l’activité de conducteur de VTC de manière temporaire et occasionnelle. A ce titre, le professionnel qui souhaite exercer cette activité à titre temporaire et occasionnel en France est soumis aux mêmes exigences que le ressortissant français.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Les ressortissants d'un État membre de l’Union européenne ou d'un État partie à l’Espace économique européen peuvent exercer de manière durable la profession de conducteur VTC sur le territoire national. Pour cela, ils doivent au préalable justifier de leur aptitude professionnelle :

- soit par la production d’une attestation de compétences ou d’un titre de formation délivré par l'autorité compétente d'un de ces États, lorsqu'une telle attestation ou un tel titre est exigé pour exécuter les prestations de conduite de VTC (reconnaissance mutuelle des qualifications) ;
- soit par la production de pièces de nature à établir une expérience professionnelle d'une durée minimale d'un an à temps plein, ou à temps partiel pour une durée équivalente, au cours des dix dernières années (équivalence) ;
- soit en réussissant l’examen d’entrée dans la profession.

Des dispositions particulières s’appliquent aux cas dans lesquels il existe une différence entre la formation reçue par un demandeur dans son État d’origine, et les compétences qui doivent être validées par l'examen VTC prévu par le Code des transports. Dans cette hypothèse, si les compétences acquises par le demandeur au cours de son expérience professionnelle ou au moyen de la formation qu'il a reçue dans son État d'origine ne permettent pas de couvrir cette différence ou cet écart, en ce qui concerne les matières essentielles à l’exercice de l’activité de conducteur de VTC, le préfet de département, ou le préfet de police selon le cas, peut obliger le demandeur à passer une épreuve d’aptitude ou à suivre un stage d’adaptation, afin de compenser cette différence. 

Les ressortissants de l’Union européenne ou d'un État partie à l’Espace économique européen doivent disposer d'un niveau en langue française suffisant pour exercer la profession. Un contrôle du niveau de connaissance de la langue peut être organisé s’il existe un doute sérieux et concret sur le niveau suffisant des connaissances linguistiques du professionnel au regard des activités qu'il entend exercer. 

*Pour aller plus loin* : article R. 3120-8-1 du Code des transports.

## 3°. Conditions d’honorabilité

Le professionnel qui souhaite exercer l’activité de conducteur de VTC doit satisfaire à une condition d’honorabilité professionnelle. Aussi, il ne doit pas avoir fait l’objet :

- d’une réduction de la moitié du nombre maximal de points du permis de conduire ;
- d’une condamnation définitive pour conduite d'un véhicule sans permis ou pour refus de restituer son permis de conduire après l'invalidation ou l'annulation de celui-ci ;
- d’une condamnation définitive prononcée par une juridiction, française ou étrangère, à une peine criminelle ou à une peine correctionnelle d'au moins six mois d'emprisonnement pour vol, escroquerie, abus de confiance, atteinte volontaire à l'intégrité de la personne, agression sexuelle, trafic d'armes, extorsion de fonds ou infraction à la législation sur les stupéfiants.

*Pour aller plus loin* : article R. 3120-8 du Code des transports.

## 4° Sanctions administratives et pénales

Pour les exploitants de véhicule de transport avec chauffeur, le défaut d’inscription au registre des VTC est puni d'un an d'emprisonnement et de 15 000 € d'amende. L’exploitant personne physique risque en outre une suspension de son permis de conduire pour une durée maximale de 5 ans, la confiscation de son véhicule ou son immobilisation pour une durée maximale d’un an.

Le fait, pour un conducteur de VTC, de prendre en charge un passager sans réservation préalable ou de pratiquer la maraude l’expose aux mêmes sanctions.

En outre, le conducteur encourt le retrait temporaire ou définitif de sa carte professionnelle en cas de violation de la réglementation applicable au transport public particulier de personnes. 

*Pour aller plus loin* : articles L. 3124-7, L. 3124-11 et L. 3124-12 du Code des transports.

## 5°. Demande en vue d’obtenir la carte professionnelle de conducteur de VTC

La détention d’une carte professionnelle de conducteur de VTC est obligatoire pour exercer la profession.

### Autorité compétente 

Le professionnel doit adresser sa demande au préfet du département du lieu de résidence.

### Pièces justificatives 

Il est conseillé de se rapprocher de la préfecture dans laquelle le professionnel souhaite exercer pour connaître la liste des pièces à fournir lors de la demande.

### Délais

La préfecture délivre la carte professionnelle de conducteur de VTC dans un délai de trois mois à compter du dépôt de la demande.

### Obligations

Le conducteur de VTC utilisant son véhicule à titre professionnel doit apposer sa carte professionnelle de conducteur de VTC sur le pare-brise ou, à défaut, sur le véhicule de telle façon que la photographie soit facilement visible de l'extérieur.

*Pour aller plus loin* : articles L. 3120-2-2 et R. 3120-6 du Code des transports.