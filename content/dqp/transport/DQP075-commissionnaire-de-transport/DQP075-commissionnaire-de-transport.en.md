﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP075" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Forwarding agent" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="forwarding-agent" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/transport/forwarding-agent.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="forwarding-agent" -->
<!-- var(translation)="Auto" -->


Forwarding agent
======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The transport commissionaire is a professional whose mission is to organize the movement of goods from one place to another, according to the modes and means of his choice.

He acts as an intermediary since he enters into a transport commission contract with his client, and one or more transport contracts with carriers that he charters on his behalf.

Section R. 1411-1 of the Transportation Code states that the various activities of the Transportation Commissioner are:

- Grouping operations: sending goods from multiple shippers or to multiple recipients, assembled and formed into a single batch for transport;
- Charter operations: shipments are entrusted without prior grouping to public carriers;
- City office operations: the commissionaire takes care of parcels or retail shipments and hands them separately to either public carriers or other transport commissionaires;
- Transport organisation operations: the commissionaire takes care of goods to and from the national territory, and provides transport by one or more public carriers through any route.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of transport commissionaire is reserved for anyone with a certificate of registration in the road transport register.

To be registered, the person must be in possession of a certificate of professional ability and must meet conditions of honour.

*For further information*: Article R. 1422-1 of the Transportation Code.

#### Training

The certificate of professional capacity, required for the application for registration, is issued by the prefect of the region to the person concerned who justifies either:

- to have a diploma in higher education, having received legal, economic, accounting, commercial or technical training, or a technical education diploma, having received training in transport activities;
- to have passed the tests of a[written review](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=72888CDED0E6534F1C718717EE40F003.tplgfr27s_3?cidTexte=JORFTEXT000031679969&dateTexte=20151224) conducted at an accredited training centre
- recognition of professional qualifications acquired in a European Union (EU) member state or party to the European Economic Area (EEA) agreement.

**Please note**

The professional capacity required to practise as a transport commissionaire is acquired as long as the person has undergone training in business management of at least 200 hours of training or when he has completed an 80-hour internship. ensuring a sufficient level of law applied to transport, transport economics and the transport commission.

*For further information*: Article R. 1422-4 of the Transport Code; Articles 7 and 8 of the Order of 21 December 2015 relating to the issuance of the certificate of professional capacity allowing the exercise of the profession of transport commissionaire.

#### Costs associated with qualification

Training leading to this occupation is paid for and the cost varies depending on the organization chosen. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

There are no regulations for a national of an EU or EEA who wishes to practise as a transport commissionaire in France, either on a temporary or casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals (see below "5. Steps and procedures for recognition of qualification") will find to apply.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

In order to carry out the activity of transport commissionaire in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- hold a certificate of competency or training certificate required for the exercise of the activity of transport commissionaire in an EU or EEA state when that state regulates access or the exercise of this activity on its territory;
- have worked full-time or part-time for one year in the past ten years in another state that does not regulate training or the practice of the profession
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been a transport commissionaire in the State for three years, admitted equivalence.

Once he fulfils one of these conditions, the national will be able to apply for recognition of his professional qualifications from the prefect of the region in which he wishes to practice his profession (see infra "5°. a. Request recognition of his professional qualifications for the EU or EEA national for permanent activity (LE)).

Where there are substantial differences between the professional qualification of the national and the training required in France, the territorially competent prefect may require that the person concerned submit to compensation measures (see infra "5" . a. Good to know: compensation measures").

*For further information*: Articles R. 1422-11 and R. 1422-15 of the Transportation Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The transport commissioner must meet conditions of honour and must not, among other things:

- have been convicted by a French court, registered in bulletin 2 of the criminal record, or by a foreign court and recorded in an equivalent document, and pronouncing a ban on practising a commercial profession or Industrial;
- driving without a licence
- Resort to concealed labour
- to transport so-called dangerous goods.

*For further information*: Article R. 1422-7 of the Transportation Code.

4°. Insurance
---------------------------------

The Liberal Transport Commissioner must take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request recognition of professional qualifications for EU or EEA nationals for permanent activity (LE)

**Competent authority**

The regional prefect is competent to issue the certificate of professional capacity justifying the recognition of the national's professional qualifications.

**Supporting documents**

The application for certification is made by sending a file to the prefect, including the following documents:

- The form [Cerfa 11414*05](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_11414.do) ;
- A valid piece of identification
- a proof of residence, for the person who has his or her usual residence in France;
- A copy of the certificate of competence or training certificate issued by an EU or EEA state that regulates the profession of transport commissionaire;
- If so, any document justifying the applicant's legal exercise as a freight operator for one year in the past ten years in an EU or EEA state that does not regulate access to the profession or the Training
- if applicable, any document justifying the applicant's actual exercise for at least three years, in a state that has granted equivalence a training certificate or certificate acquired in a third state and allowing the exercise of that Profession;
- training programs or the content of the experience gained.

**Please note**

If necessary, all supporting documents must be translated into French by a certified translator.

**Procedure**

When the file is sent, the prefect will acknowledge receipt within one month and inform the national of any missing documents. Unless compensation measures are taken against the national, delaying the procedure, the prefect will be able to issue the certificate of professional capacity within one month.

Once the national has obtained his certificate of professional capacity, he will be able to register in the road transport register.

*For further information*: Articles 9 to 15 of the December 21, 2015 order on the issuance of the certificate of professional capacity allowing the practice of the profession of transport commissionaire

**Good to know: compensation measures**

In case of differences between the training or experience of the national and the requirements required in France, the prefect may submit him to a compensation measure that may be an adjustment course or an aptitude test.

The adaptation course must last at least 80 hours and allow it to acquire a sufficient level of law applied to transport, transport economics and the transport commission.

The aptitude test is in the form of a multiple-choice questionnaire for which the national must obtain a score of at least 60 out of 100.

*For further information*: Article R. 1422-18 of the Transportation Code.

### b. Ask for registration in the registry of forwarding agents

Registration is done by sending the form [Cerfa No. 16092*01](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57872) to the relevant local transport authority.

*For further information*: Order of 4 October 2007 relating to the composition of the application file for registration in the register of freight forwarders.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

