﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP258" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Road passenger transport operator" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="road-passenger-transport-operator" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/transport/road-passenger-transport-operator.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="road-passenger-transport-operator" -->
<!-- var(translation)="Auto" -->


Road passenger transport operator
===================

Latest update: <!-- begin-var(last-update) -->August 2021<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The public passenger road carrier is a professional whose activity consists of transporting passengers for remuneration by means of road and motorized vehicles of at least four seats (including driver).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of a road haulier of travellers the professional must:

- Be registered as a road transport company;
- obtain a licence to practice the profession and to do so, meet the requirements for honourability, financial and professional capacity.

In addition, any company wishing to operate as a public road carrier of persons must appoint a transport manager to manage vehicle maintenance, verification of contracts and transport documents, auditing safety procedures and assigning loads or services to drivers.

*To go further*: Articles L. 3113-1 and R. 3113-43 of the Transportation Code; Order of 28 December 2011 relating to the authorisation to practise as a public road carrier and the terms of the application by companies for authorisation.

**Please note**

Some professionals are exempt from the requirement of financial and professional capacity:

- Individuals and associations offering on-demand and school transport services (if there is a lack of transport supply) when using a single vehicle with up to nine seats;
- Companies that carry out this activity on demand or on a regular basis as an accessory to another activity and have a single vehicle with up to nine seats;
- authorities and local authorities providing transportation services for non-commercial purposes and owning up to two vehicles.

*To go further*: Articles R. 3113-10 and R. 3113-11 of the Transportation Code.

#### Conditions of access to the profession

**Establishment**

To operate in France, the professional must:

- Have its head office or main establishment in France;
- have in the premises of its head office, or its main establishment in the absence of a head office in France:- key company documents (accounting documents, personnel management, drivers' working and rest time data, etc.),
  - The original Community or domestic transport licence issued by the regional prefect,
  - If necessary, all contracts with authorities organising public transport services for persons,
  - Any documents relating to the company's activity;
- Have at least one registered vehicle
- effectively and continuously direct the activities of these passenger vehicles.

*To go further*: Article R. 3113-18 to R. 3131-22 of the Transportation Code.

**Financial capacity**

Each year, professional must, by means of certified documents (by an accountant, an auditor or a certified management center), certify his financial capacity.

To do so, it must justify having capital and reserves:

- at least 1,500 euros for each vehicle not exceeding nine seats, including the driver;
- EUR 9,000 for the first vehicle and EUR 5,000 for each of the following vehicles, for vehicles exceeding this limit.

Failing that, the professional may present guarantees granted by one or more financial institutions holding bail, as long as this guarantee does not exceed half of the financial capacity due.

*To go further*: Articles R. 3113-31 to R. 3113-34 of the Transportation Code.

**Professional capacity**

The professional wishing to carry out the activity of a road freight carrier must submit to a written examination on all the subjects set out in Schedule I of regulation (EC) 1071/2009 of the European Parliament and the Council of 21 October 2009 establishing common rules on the conditions to be met to practice the profession of carrier by road.

However, can also obtain the certificate of professional ability, the person concerned who:

- holds a diploma, a university degree, a certificate of study or a professional designation issued in France by a higher education institution or an authorised body and, as long as he has received, during his training, the knowledge necessary to carry out this activity;
- justifies having continuously managed a public road transport company for people in one or more EU states in the last ten years prior to 4 December 2009.

In addition, a certificate of professional capacity in road transport of persons with vehicles not exceeding nine seats can also be issued to professionals:

- holders of a diploma or professional designation issued in France involving the knowledge of the subjects set out in Schedule I of the regulation of 21 October 2009 and subject to having passed the written examination;
- justifying that he had managed a road transport company on a continuous and principal basis for two years and, provided that he had not ceased to operate for more than ten years.

**Please note**

As long as the professional has not been in business for five years, the prefect can train him to update his knowledge.

*To go further*: Articles R. 3113-35 to R. 3113-42 of the Transportation Code.

#### Registration in the register of road transport companies

**Competent authority**

The professional must apply to the prefect of the region where his company's headquarters are located. Companies not headquartered in France must apply to the prefect of the region where their main establishment is located.

**Supporting documents**

His request must include, as the case may be, the cerfa form No. [16093](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57874) or [16094](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14156) requesting authorization to practise the profession of road transport operator and registration in the register, as well as the supporting documents mentioned.

**Outcome of the procedure**

The prefect has three months to decide on the application. This period may be extended by one month in case of missing documents on file. The registration in this register gives rise to the issuance by the prefect:

- a Community licence when the company uses one or more buses or coaches, subject to not being registered in the public transport register;
- an internal transportation licence when the company uses one or more vehicles other than buses or coaches, or when it is registered in the public transit register.

These licences with a renewable 10-year validity are issued with as many certified copies as registered vehicles.

*To go further*: Article R. 3113-2 to R. 3113-9 of the Transportation Code; Order of 28 December 2011 relating to the authorization to exercise the profession of public road transport operator and the terms and conditions of the request for authorization by companies.

3°. Conditions of honorability
-----------------------------------------

The professional engaged in a travelling hauling activity must not have been subject to:

- several convictions mentioned in bulletin 2 of his criminal record and leading to a ban on practising a commercial or industrial profession;
- of several convictions mentioned in bulletin 2 of his criminal record as soon as he:- failed to meet the requirements for the transportation of hazardous materials, in terms of pricing the contract, falsification of electronic documents or data, false information, or failure to install control devices or in the area of waste prevention and management,
  - has committed an offence under the Penal Code for personal injury or sexual assault, bankruptcy trade code, or Highway Traffic Act.

*To go further*: Articles R. 3113-23 to R. 3113-30, L. 1252-5 to L. 1252-7, L. 3242-2 to L. 3242-5 and L. 3315-4 to L. 3315-6 of the Transportation Code; Articles L. 654-4 to L. 654-15 of the Code of Commerce; Articles 221-6-1, 222-19-1 and the following of the Penal Code; Article L. 541-46-5 of the Environment Code; Section L. 221-2 and the following of the Highway Traffic Act.

