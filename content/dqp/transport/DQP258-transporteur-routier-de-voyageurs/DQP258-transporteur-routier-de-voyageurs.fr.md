﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP258" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Transporteur routier de voyageurs" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="transporteur-routier-de-voyageurs" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/transport/transporteur-routier-de-voyageurs.html" -->
<!-- var(last-update)="Août 2021" -->
<!-- var(url-name)="transporteur-routier-de-voyageurs" -->
<!-- var(translation)="None" -->

# Transporteur routier de voyageurs

Dernière mise à jour : <!-- begin-var(last-update) -->Août 2021<!-- end-var -->

## 1°. Définition de l’activité

Le transporteur routier public de voyageurs est un professionnel dont l'activité consiste à transporter contre rémunération des voyageurs au moyen de véhicules routiers et motorisés de quatre places minimum (conducteur compris).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de transporteur routier de voyageurs le professionnel doit :

- être inscrit au registre des entreprises de transport par route ;
- obtenir une autorisation d'exercer la profession et pour cela, satisfaire aux exigences en matière d'honorabilité, de capacité financière et professionnelle.

En outre, toute entreprise qui souhaite exercer l'activité de transporteur public routier de personnes doit désigner un gestionnaire de transport en vue de gérer l'entretien des véhicules, la vérification des contrats et des documents de transports, la comptabilité, la vérification des procédures en matière de sécurité et l'affectation des chargements ou des services aux conducteurs.

*Pour aller plus loin* : articles L. 3113-1 et R. 3113-43 du Code des transports ; arrêté du 28 décembre 2011 relatif à l'autorisation d'exercer la profession de transporteur public routier et aux modalités de la demande d'autorisation par les entreprises.

**À noter**

Certains professionnels sont dispensés de l'exigence de capacité financière et professionnelle :

- les particuliers et les associations proposant des services à la demande et de transport scolaire (en cas de carence de l'offre de transport), lorsqu'ils utilisent un seul véhicule de neuf places maximum ;
- les entreprises qui exercent cette activité à la demande ou de manière régulière à titre accessoire d'une autre activité et qui possèdent un seul véhicule de neuf places maximum ;
- les régies et les collectivités territoriales réalisant des prestations de transport à des fins non commerciales et possédant deux véhicules maximum.

*Pour aller plus loin* : articles R. 3113-10 et R. 3113-11 du Code des transports.

#### Conditions d'accès à la profession

##### Établissement

Pour exercer son activité en France, le professionnel doit :

- avoir son siège social ou son établissement principal en France ;
- avoir dans les locaux de son siège social, ou de son principal établissement en l'absence de siège social en France :
  - les principaux documents de l'entreprise (documents comptables, de gestion du personnel, les données relatives au temps de travail et de repos des conducteurs, etc.),
  - l'original de la licence communautaire ou de transport intérieur délivrée par le préfet de région,
  - le cas échéant, l'ensemble des contrats conclus avec des autorités organisatrices de services de transport public de personnes,
  - tout document se rapportant à l'activité de l'entreprise ;
- disposer d'au moins un véhicule immatriculé ;
- diriger effectivement et en permanence les activités relatives à ces véhicules de transport de voyageurs.

*Pour aller plus loin* : article R. 3113-18 à R. 3131-22 du Code des transports.

##### Capacité financière

Chaque annéee, professionnel doit, au moyen de documents certifiés (par un expert-comptable, un commissaire aux comptes ou un centre de gestion agréé), attester de sa capacité financière.

Pour cela, il doit justifier avoir des capitaux et des réserves :

- d'un montant au moins égal à 1 500 euros pour chaque véhicule n'excédant pas neufs places, conducteur compris ;
- d'un montant de 9 000 euros pour le premier véhicule et de 5 000 euros pour chacun des véhicules suivants, pour les véhicules excédant cette limite.

À défaut, le professionnel peut présenter des garanties accordées par un ou plusieurs organismes financiers se portant caution, dès lors que cette garantie n'excède pas la moitié de la capacité financière exigible.

*Pour aller plus loin* : articles R. 3113-31 à R. 3113-34 du Code des transports.

##### Capacité professionnelle

Le professionnel souhaitant exercer l'activité de transporteur routier de marchandises doit se soumettre à un examen écrit portant sur l'ensemble des matières fixées à l'annexe I du règlement (CE) n° 1071/2009 du Parlement européen et du Conseil du 21 octobre 2009 établissant des règles communes sur les conditions à respecter pour exercer la profession de transporteur par route.

Toutefois, peut également obtenir l'attestation de capacité professionnelle, l'intéressé qui :

- est titulaire d'un diplôme, d'un titre universitaire, d'un certificat d'études ou d'un titre professionnel délivré en France par un établissement d'enseignement supérieur ou un organisme habilité et dès lors qu'il a reçu, lors de sa formation, les connaissances nécessaires pour exercer cette activité ;
- justifie avoir géré de manière continue une entreprise de transport public routier de personnes dans un ou plusieurs État(s) de l'Union européenne durant les dix dernières années précédant le 4 décembre 2009.

En outre, une attestation de capacité professionnelle en transport routier de personnes avec des véhicules n'excédant pas neufs places peut également être délivrée aux professionnels :

- titulaires d'un diplôme ou d'un titre professionnel délivré en France impliquant la connaissances des matières fixées à l'annexe I du règlement du 21 octobre 2009 et sous réserve d'avoir réussi l'examen écrit ;
- justifiant avoir géré de manière continue et principale une entreprise de transport routier de personnes pendant deux ans et à condition de ne pas avoir cessé d'exercer depuis plus de dix ans.

**À noter**

Dès lors que le professionnel n'a pas exercé son activité depuis cinq ans, le préfet peut lui imposer une formation en vue d'actualiser ses connaissances.

*Pour aller plus loin* : articles R. 3113-35 à R. 3113-42 du Code des transports.

#### Inscription au registre des entreprises de transport par route

##### Autorité compétente

Le professionnel doit adresser sa demande au préfet de région où se trouve le siège de sa société. Les entreprises n'ayant pas de siège en France doivent adresser leur demande au préfet de la région où se situe leur principal établissement.

##### Pièces justificatives

Sa demande doit comporter, selon le cas, le formulaire Cerfa n° [16093](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57874) ou [16094](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14156) de demande d'autorisation d'exercer la profession de transporteur routier et d'inscription au registre, ainsi que les pièces justificatives mentionnées.

##### Issue de la procédure

Le préfet dispose d'un délai de trois mois pour se prononcer sur la demande. Ce délai peut être prolongé d'un mois en cas de pièces manquantes au dossier. L'inscription à ce registre donne lieu à la délivrance par le préfet :

- d'une licence communautaire lorsque l'entreprise utilise un ou plusieurs bus ou autocars, sous réserve de ne pas être inscrite au registre des transports publics collectifs ;
- d'une licence de transport intérieur lorsque l'entreprise utilise un ou plusieurs véhicules autres que des autobus ou autocars, ou lorsqu'elle est inscrite au registre des transports publics collectifs.

Ces licences d'une validité de dix ans renouvelable sont délivrées avec autant de copies certifiées conformes que de véhicules immatriculés.

*Pour aller plus loin* : article R. 3113-2 à R. 3113-9 du Code des transports ; arrêté du 28 décembre 2011 relatif à l'autorisation d'exercer la profession de transporteur public routier et aux modalités de la demande d'autorisation par les entreprises.

## 3°. Conditions d’honorabilité

Le professionnel exerçant une activité de transporteur routier de voyageurs ne doit pas avoir fait l'objet :

- de plusieurs condamnations mentionnées au bulletin n° 2 de son casier judiciaire et conduisant à une interdiction d'exercer une profession commerciale ou industrielle ;
- de plusieurs condamnations mentionnées au bulletin n° 2 de son casier judiciaire dès lors qu'il :
  - n'a pas satisfait aux obligations relatives au transport de matières dangereuses, en matière de fixation du prix du contrat, de falsification des documents ou des données électroniques, de faux renseignements, ou qu'il n'a pas procédé à l'installation des dispositifs de contrôle ou en matière de prévention et de gestion des déchets,
  - a commis un infraction au Code pénal en matière d'atteinte à l'intégrité physique d'une personne ou d'agression sexuelle, au Code de commerce relative à la banqueroute, ou au Code de la route.

*Pour aller plus loin* : articles R. 3113-23 à R. 3113-30, L. 1252-5 à L. 1252-7, L. 3242-2 à L. 3242-5 et L. 3315-4 à L. 3315-6 du Code des transports ; articles L. 654-4 à L. 654-15 du Code de commerce ; articles 221-6-1, 222-19-1 et suivants du Code pénal ; article L. 541-46-5° du Code de l'environnement ; article L. 221-2 et suivants du Code de la route.