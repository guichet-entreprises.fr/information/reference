﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="LGV driver" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="lgv-driver" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/transport/lgv-driver.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="lgv-driver" -->
<!-- var(translation)="Auto" -->


LGV driver
==========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The professional road driver of HGV vehicles is a professional, whose activity consists of driving vehicles transporting goods of more than 3.5 tons or transporting persons with more than nine seats (including driver's seat) in a professional setting.

*To go further* Article L. 3314-2 of the Transportation Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of professional road driver of heavy vehicles, the professional must hold a driver qualification card. For this it must be:

- Have completed initial vocational training
- hold one of the Level IV and V road driver titles or diplomas, registered in the National Register of Professional Certifications ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)).

*To go further* Article R. 3314-28 of the Transportation Code.

#### Training

In order to obtain a qualification card, the professional must justify either having completed compulsory initial training (FMO) or holding one of the following diplomas or professional titles:

- for the transport of goods:- a bachelor's degree in "road freight driver" (CTRM),
  - a Certificate of Professional Fitness (CAP) road freight driver or road freight driver (CLM),
  - a certificate of professional studies (BEP) driving and services in road transport,
  - a professional (TP) designation as a driver of road freight transport on all vehicles (CTRMV) or on carrier (CTRMP) issued by the Minister responsible for employment and vocational training;
- for passenger transport:- a CAP reception and driving officer, passenger transport,
  - a TP of intercity road traffic driver (CTRIV) issued by the Minister for Employment and Vocational Training,
  - a Commercial and Driver's TP for Urban Road Passenger Transport (ACCTRUV) issued by the Minister for Employment and Vocational Training.

*To go further* :[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=BF895445AB868383BB76802EBB11EA7A.tplgfr22s_2?idArticle=LEGIARTI000027654685&cidTexte=LEGITEXT000027654687&dateTexte=20180502) of the order of 26 February 2008 setting out the list of Level IV and V diplomas admitted in equivalence under the initial qualification of drivers of certain vehicles assigned to road freight or passenger transport.

**Mandatory Minimum Initial Training (FIMO)**

This training, which lasts at least 280 hours, allows the professional, after the completion of a final examination, to obtain the professional title of road driving and, as such, to drive:

- from the age of 18, the vehicles for which the C1, C1E, C or EC licences are required, when this training has focused on the transport of goods;
- vehicles requiring a driving licence in the D1, D1E, D or DE categories, when this training has been carried out on passenger transport.

The initial professional qualification can also be obtained by the candidate who has completed a minimum initial training so-called accelerated. This training, which lasts at least 140 hours over four consecutive weeks, allows the professional to drive:

- vehicles for which the C1E, C1E (from 18-year-old) licence is required, C, or CE (from the age of 21) in the case of training in the transport of goods;
- vehicles for which the D1 or D1E licence (from age 21), D, or DE (from age 23) is required when the training has focused on passenger transport. However, this age is increased to 21 years for driving vehicles whose D or DE licence is required when the professional performs regular and non-50-kilometre passenger services.

At the end of this initial training, the professional receives a certificate of training in accordance with the model set out in Schedule 2 of the[decreed from 31 December 2010](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFCONT000023452189) setting the conditions for issuing the driver qualification card and amending the 4 July 2008 decree defining the model of certificates relating to the initial and continuing vocational training of road transport drivers from goods and passengers.

*To go further* Articles R. 3314-1 and the following of the Transportation Code.

**Specific "bridge" training**

The professional with an initial qualification as a freight driver, may obtain the qualification of passenger driver, as long as he holds a valid class D1, D1E, D or DE licence and after having followed a 35-hour additional training course.

Such equivalence is also provided for the professional who has obtained an initial qualification as a passenger driver and who wishes to obtain the qualification of freight driver. If applicable, the individual must have a valid C1, C1E, C or EC licence and have completed a 35-hour training course.

In addition, drivers with a class driver's licence are deemed to have obtained the initial qualification:

- D or DE valid and issued before September 10, 2008 for the exercise of passenger transport activity;
- C or EC valid and issued before 10 September 2009, for the exercise of the freight transport activity.

*To go further* Articles R. 3314-7 to R. 3314-9 of the Transportation Code.

**Compulsory Continuing Education (FCO)**

The professional is required to complete a 35-hour continuing education course every five years from the time of obtaining his or her initial qualification. This training must be delivered by an accredited body under the conditions of Article R. 338-8 of the Education Code.

The programme and modalities of these initial and ongoing training courses are set at the[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000018008166) January 3, 2008 on the programme and how to implement the initial and ongoing vocational training of road freight and passenger drivers.

*To go further* Articles R. 3314-10 to R. 3314-14 of the Transportation Code.

**Driver's qualification card**

The driver qualification card is issued, after checking the validity of the driver's licence, to the titular professional:

- one of the diplomas or training titles set out in the annex of the decree of 26 February 2008 above;
- certificate of training.

A model of this map is attached to the[Appendix 1](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFCONT000023452189) December 31, 2010. This card is renewed every five years after each continuing education session.

*To go further* Article R. 3314-28 of the Transportation Code.

#### Costs associated with qualification

Qualifying training pays off, and the cost varies depending on the relevant training centres. It is advisable to get closer to the training centres considered for more information.

### b. EU nationals: for temporary and casual (Freedom to provide services) or permanent (Free Settlement (LE))

There is no provision for the national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) for a temporary and occasional exercise (LPS) or permanent (LE) in France.

As such, the national is subject to the same provisions as the French national (see supra "2.2). a. National requirements").

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The professional road driver of HGV vehicles is required to comply with the specific provisions regarding working hours and rest time.

As such, the professional must:

- take breaks every six hours minimum:- at least thirty minutes when his total working time is between six and nine hours,
  - at least forty-five minutes when his total working time is more than nine hours;
- Be careful not to exceed sixty hours of work per week;
- Ensure that his working hours do not exceed ten hours, as long as part of his work is carried out between midnight and five hours over a period of twenty-four hours;
- when operating independently, keep the documents necessary to count its working hours.

In addition, the professional can obtain from his employer all the information relating to the counting of his working time.

*To go further* Articles L. 3312-1 at L. 3312-9 and D. 3312-21 to D. 3312-22 of the Transportation Code.

4°. Sanctions
---------------------------------

The professional faces a one-year prison sentence and a fine of 30,000 euros if he falsifies documents, provides false information, deteriorates, or modifys devices for the control or does not proceed with their installation.

If necessary, the vehicle on which the offence was committed will be removed from circulation until it is regularized.

In addition, the professional faces a sentence of six months' imprisonment and 3,750 euros if he carries out this activity without a driver's card or with a card that does not belong to him.

*To go further* Article L. 3315-4 of the Transportation Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

