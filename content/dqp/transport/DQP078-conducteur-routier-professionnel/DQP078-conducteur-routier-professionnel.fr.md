﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transport" -->
<!-- var(title)="Conducteur routier professionnel de véhicules poids lourds" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transport" -->
<!-- var(title-short)="conducteur-routier-professionnel" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/transport/conducteur-routier-professionnel-de-vehicules-poids-lourds.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="conducteur-routier-professionnel-de-vehicules-poids-lourds" -->
<!-- var(translation)="None" -->

# Conducteur routier professionnel de véhicules poids lourds

Dernière mise à jour : <!-- begin-var(last-update) -->2021-01<!-- end-var -->

## 1°. Définition de l’activité

Le conducteur routier professionnel de véhicules poids lourds est un professionnel, dont l'activité consiste à conduire des véhicules de transport de marchandises de plus de 3,5 tonnes ou de transport de personnes comportant plus de neuf places assises (y compris le siège du conducteur) dans un cadre professionnel.

*Pour aller plus loin* : article L. 3314-2 du Code des transports.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de conducteur routier professionnel de véhicules poids lourds, le professionnel doit être titulaire d’un permis de conduire de la catégorie de véhicule correspondant en cours de validité (permis de conduire des catégories C1, C1E, C ou CE pour le transport de marchandises, ou permis de conduire des catégories D1, D1E, D ou DE pour le transport de voyageurs). Il doit également être titulaire d'une carte de qualification de conducteur, afin de prouver le respect de ses obligations de formation professionnelle. Il doit pour cela :

- soit avoir suivi avec succès une formation initiale minimale obligatoire (FIMO)  ;
- soit détenir l'un des titres ou diplômes de conducteur routier suivants : 
  - pour le transport de marchandises :
    - un baccalauréat professionnel « conducteur transport routier de marchandises » (CTRM),
    - un certificat d'aptitude professionnelle (CAP) conducteur routier de marchandises ou conducteur routier livreur de marchandises (CLM),
    - un titre professionnel (TP) de conducteur du transport routier de marchandises sur tous véhicules (CTRMV) ou sur porteur (CTRMP) délivré par le ministre chargé de la formation professionnelle ;
  - pour le transport de voyageurs :
    - un CAP agent d'accueil et de conduite routière, transport de voyageurs,
    - un titre professionnel (TP) de conducteur du transport en commun sur route (CTCR) délivré par le ministre chargé de la formation professionnelle ;
- soit avoir suivi, pour les conducteurs déjà détenteurs d’une qualification initiale, une formation continue obligatoire (FCO).

*Pour aller plus loin* : articles R. 3314-2, R. 3314-3, R. 3314-10 et R. 3314-28 du Code des transports ; arrêté du 26 février 2008 fixant la liste des titres et diplômes de niveaux IV et V admis en équivalence au titre de la qualification initiale des conducteurs de certains véhicules affectés aux transports routiers de marchandises ou de voyageurs.

#### Qualification initiale 

La qualification initiale de conducteur est obtenue à l’issue d’une formation professionnelle initiale qui peut être longue ou accélérée.

**Formation initiale « longue »** : cette formation, d'une durée d'au moins 280 heures, permet au professionnel, après la réussite d'un examen final, d'obtenir l'un des titres ou diplômes de conducteur routier précédemment mentionnés. 

**Formation initiale « accélérée » ou formation initiale minimale obligatoire (FIMO)** : cette formation, d'une durée d'au moins 140 heures, est dispensée sur quatre semaines obligatoirement consécutives, sauf lorsqu'elle est réalisée dans le cadre d'une formation en alternance. Elle est réalisée en face-à-face pédagogique, et  se conclut par un examen final.

*Pour aller plus loin :* articles R. 3314-1 à R. 3314-6 du Code des transports ; arrêté du 3 janvier 2008 relatif au programme et aux modalités de mise en œuvre de la formation professionnelle initiale et continue des conducteurs du transport routier de marchandises et de voyageurs, notamment les annexes I pour la FIMO marchandises et II pour la FIMO voyageurs..

#### Formation complémentaire dite « passerelle »

Le professionnel titulaire d'une qualification initiale de conducteur de transport de marchandises peut obtenir la qualification de conducteur de voyageurs, dès lors qu'il est titulaire d'un permis de catégorie D1, D1E, D ou DE en cours de validité, après avoir suivi avec succès une formation complémentaire d’une durée de 35 heures, préalablement à toute activité de conduite dans le secteur du transport de voyageurs. Cette formation porte sur les parties du programme de formation spécifiques au secteur du transport de voyageurs.

Une telle « passerelle » est également prévue pour le professionnel ayant obtenu une qualification initiale de conducteur de transport de voyageurs, et souhaitant obtenir la qualification initiale de conducteur du transport de marchandises. L'intéressé doit être titulaire d'un permis de catégorie C1, C1E, C ou CE en cours de validité et avoir suivi avec succès une formation d’une durée de 35 heures, préalablement à toute activité de conduite dans le secteur du transport de marchandises. Cette formation porte sur les parties du programme de formation spécifiques au secteur du transport de marchandises.

*Pour aller plus loin :* articles R. 3314-7 et R. 3314-8 du Code des transports ; arrêté du 3 janvier 2008 relatif au programme et aux modalités de mise en œuvre de la formation professionnelle initiale et continue des conducteurs du transport routier de marchandises et de voyageurs, notamment les annexes I ter pour la formation passerelle marchandises et II ter pour la formation passerelle voyageurs.

#### Formation continue obligatoire (FCO)

Le professionnel ayant obtenu une qualification initiale de conducteur est tenu d'effectuer une formation continue obligatoire d'une durée de 35 heures tous les cinq ans, le premier stage ayant lieu cinq ans après l'obtention de la qualification initiale.

La FCO se déroule pendant le temps habituel de travail. Pour les conducteurs salariés, elle constitue une formation contribuant au respect de l’obligation, à la charge de l’employeur, d’assurer l’adaptation des salariés à leur poste de travail.

Les conducteurs qui ont interrompu leur activité de conduite, à titre professionnel, pendant plus de cinq ans consécutifs, doivent, préalablement à la reprise de leur activité, suivre une FCO.

*Pour aller plus loin :* articles R. 3314-10 à R. 3314-14 du Code des transports ; arrêté du 3 janvier 2008 relatif au programme et aux modalités de mise en œuvre de la formation professionnelle initiale et continue des conducteurs du transport routier de marchandises et de voyageurs, notamment les annexes I bis pour la FCO marchandises et II bis pour la FCO voyageurs.

#### Carte de qualification du conducteur

La carte de qualification de conducteur est délivrée par l’Imprimerie nationale (organisme chargé de la délivrance des cartes), après vérification de la validité de son permis de conduire, au professionnel ayant obtenu un des diplômes ou titres de formation mentionnés par l'arrêté du 26 février 2008 susmentionné, ou ayant suivi avec succès une FIMO, une formation complémentaire « passerelle » ou une FCO.

Le modèle de cette carte est conforme au modèle de l’Union européenne. Cette carte est renouvelée tous les cinq, ans après chaque session de formation continue. 

*Pour aller plus loin :* article R. 3314-28 du Code des transports ; annexe II de la directive 2003/59/CE du 15 juillet 2003 relative à la qualification initiale et à la formation continue des conducteurs de certains véhicules routiers affectés aux transports de marchandises ou de voyageurs (modèle de l’Union européenne pour la carte de qualification de conducteur)

#### Organismes de formation

La formation initiale dite « longue » est validée dans les organismes mentionnés à l'article R. 338-8 du Code de l'éducation, ou dans les établissements rattachés au ministère de l’Education nationale.

Les FIMO, formation complémentaire dites « passerelle » et FCO sont dispensées par des établissements agréés par le préfet de région compétent, dans les conditions prévues à l'article R. 3314-19 du Code des transports et l’arrêté du 3 janvier 2008 relatif à l'agrément des centres de formation professionnelle habilités à dispenser la formation professionnelle initiale et continue des conducteurs du transport routier de marchandises et de voyageurs, ou par un moniteur d’entreprise. Elles peuvent être assurées par des moniteurs d'entreprise uniquement sous la responsabilité d'un établissement agréé. 

#### Coûts associés à la qualification

La formation initiale est payante. Son coût varie selon le type de formation suivi et les centres de formation compétents. Il est conseillé de se rapprocher des centres de formation considérés pour de plus amples informations.

La FCO coûte environ 700 €, à la charge de l'employeur pour les conducteurs salariés. 

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services) ou permanent (Libre Établissement)

Aucune disposition spécifique n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'un exercice temporaire et occasionnel (LPS) ou permanent (LE) en France.

À ce titre, le ressortissant est soumis aux mêmes dispositions que le ressortissant français (cf. supra « 2°. a. Exigences nationales »). Ces exigences sont issues de la transposition de la directive 2003/59/CE relative à la qualification initiale et à la formation continue des conducteurs de certains véhicules routiers affectés aux transports de marchandises ou de voyageurs, qui a instauré un cadre harmonisé pour tous les États membres de l’Union européenne. A ce titre, les cartes de qualification de conducteurs, attestant du respect des obligations de formation professionnelle, font l’objet d’une reconnaissance mutuelle entre les États membres. 

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le conducteur routier professionnel de véhicules poids lourds est tenu au respect des dispositions applicables en matière de durée de travail et de temps de repos.

À ce titre le professionnel doit :

- effectuer des pauses toutes les six heures minimum :
  - d'au moins trente minutes lorsque son temps total de travail est compris entre six et neuf heures,
  - d'au moins quarante-cinq minutes lorsque son temps total de travail est supérieur à neuf heures ;
- veiller à ne pas excéder soixante heures de travail hebdomadaires ;
- veiller à ce que sa durée de travail n'excède pas dix heures, dès lors qu'une partie de son travail est effectuée entre minuit et cinq heures sur une période de vingt-quatre heures ;
- lorsqu'il exerce son activité à titre indépendant, conserver les documents nécessaires au décompte de sa durée de travail.

En outre, le professionnel peut obtenir de son employeur l'ensemble des informations relatives au décompte de son temps de travail.

*Pour aller plus loin :* articles L. 3312-1 à L. 3312-9 et D. 3312-21 à D. 3312-22 du Code des transports.

## 4°. Sanctions

Le professionnel encourt une peine d'un an d'emprisonnement et de 30 000 € d'amende dès lors qu'il falsifie des documents, fournit de faux renseignements, détériore, ou modifie des dispositifs destinés au contrôle ou ne procède pas à leur installation. Le cas échéant, le véhicule sur lequel l'infraction a été commise sera retiré de la circulation jusqu'à sa régularisation.

Tout conducteur doit être en mesure de justifier de la régularité de sa situation au regard des obligations de qualification initiale ou de formation continue par la présentation aux agents de contrôle d’une carte de qualification ou à défaut d’un permis de conduire mentionnant le code « 95 » de l’Union. Pour les conducteurs salariés, l'employeur doit être en mesure de justifier de la régularité de la situation de ses conducteurs salariés au regard de ces mêmes obligations par la production, pour chaque salarié concerné, d'une copie de l'un de ces documents justificatifs. Le défaut de présentation de ces documents est passible d’une contravention de 3e (450 € d’amende au plus) ou de 4e classe (750 € d’amende au plus). 

*Pour aller plus loin :* articles L. 3315-4, R. 3315-1, R. 3315-2, R. 3315-7 et R. 3315-8 du Code des transports.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).