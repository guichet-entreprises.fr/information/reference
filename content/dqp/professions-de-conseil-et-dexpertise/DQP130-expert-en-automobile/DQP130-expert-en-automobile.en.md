﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP130" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Road traffic accident expert" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="road-traffic-accident-expert" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/road-traffic-accident-expert.html" -->
<!-- var(last-update)="2020-04-15 17:21:07" -->
<!-- var(url-name)="road-traffic-accident-expert" -->
<!-- var(translation)="Auto" -->


Road traffic accident expert
=================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:07<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The automotive expert is a professional whose job is to prepare expert reports. It thus intervenes primarily in the context of the "damaged vehicles" procedures governed by the provisions of Articles L. 327-1 to L. 327-6 and R.327-1 to R.327-6 of the Highway Traffic Act. These procedures consist of the "severely damaged vehicles" (VGE) and "Economically Irreparable Vehicles" (EVR) procedures.

It is also responsible for implementing the procedure for re-entering circulation of these vehicles as part of the VGE procedure.

As a result, the expert has a major role in road safety. It has an obligation to inform the owner of the vehicle if it is likely to endanger the life of the driver or others (Article R. 326-2 of the Highway Traffic Act). It intervenes mainly when a vehicle is involved in an accident and very often by being mandated by the insurer of the owner of the vehicle. It is usually the insurer of the affected vehicle that designates the car expert in the context of a damage guarantee (all accidents, collisions, theft, etc.) as in that of a liability guarantee. The expert then on behalf of the insurance company of the material acts of expertise (identification of the vehicle, record of damage and imputation of these, definition of a repair methodology, encryption of the cost of the rehabilitation and the value of the vehicle, if necessary) without the power of representation. Relationships between insurers and experts are thus often central to the practical practice of the profession.

This professional may be required to practice in an expert firm, with an insurance company, with forensic experts, with law firms, the state administration or individuals.

In accordance with the provisions of Section D. 114-12 of the Public Relations Code and the Administration, any user may obtain an information certificate on the standards applicable to the automotive profession. To do so, you should send your application to the following address: devenir-expert-automobile@interieur.gouv.fr.

*For further information*: Article L. 326-4 of the Highway Traffic Act.

2°. Professional qualifications
----------------------------------------

### a. National requirements

To practice as an automotive expert and obtain qualification for the control of damaged vehicles, the professional must:

- Have a degree in automotive experts
- be included on a national list of automotive experts set by the Minister for Transport (see below "5°). a. Application for inclusion on the list of automotive experts");
- have been trained to update the legal and technical knowledge required to conduct procedures for damaged vehicles. Without this training and obtaining this EV qualification, the expert will not be able to intervene in the VE procedures.

*For further information*: Article R. 326-11 of the Highway Traffic Act.

**Please note**

Professionals with an automotive expert degree of less than one year old are deemed to have completed the specific training.

*For further information*: order of 26 July 2011 relating to the obtaining and maintenance of the qualification for the control of damaged vehicles for automotive experts.

#### National legislation

#### Training

**Car expert diploma**

To be recognized as a professionally qualified, the professional must have a Level III automotive expert degree (B.A. 2). This diploma confers the quality of automotive expert but is not enough on its own to allow the exercise of this activity (see infra "5°. a. Application for inclusion on the list of automotive experts").

This diploma is available to both qualified candidates:

- a Level IV (bac) degree or a certified degree of this level;
- experience of at least three years in the practice of auto repair;
- at least two years of experience from an expert activity as an intern with someone with the status of an automotive expert.

**Please note**

The duration of this work experience is reduced to one year for those with the following diplomas:

- a Senior Technician's (BTS) certificate in the automotive field or agricultural machinery;
- a university degree in technology
- an engineering degree.

The list of these diplomas is set out in Appendix I of the Order of 31 July 2012 defining the automotive expert diploma available at the[Official bulletin](http://cache.media.education.gouv.fr//file/34/83/4/BO_MEN_20-09-12_226834.pdf) Department of Education as of September 20, 2012.

This diploma is awarded to candidates who have successfully completed an examination consisting of written tests and practical and oral work on teaching units. The program of these units is set at the[Appendix IIa](http://www.enseignementsup-recherche.gouv.fr/pid20536/bulletin-officiel.html?cid_bo=61279&cbo=1) 31 July 2012, available in the Above Official Bulletin.

*For further information*: Decree 95-493 of 25 April 1995 establishing and regulating the automotive expert diploma; order of 31 July 2012 setting the definition of an automotive expert's diploma.

**Updating knowledge**

Professionally qualified professionals must train to update their legal and technical knowledge necessary to conduct procedures for damaged vehicles.

This one-day training is provided by training organizations accredited by the Minister responsible for transport. It includes an administrative part and a technical part whose programme is set at the[Appendix 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0893E48E42291FCC14A543F7FC009173.tplgfr25s_1?idArticle=LEGIARTI000024423600&cidTexte=LEGITEXT000024423570&dateTexte=20180522) 26 July 2011.

At the end of this training, a training follow-up certificate, the model of which is set at the[Appendix 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0893E48E42291FCC14A543F7FC009173.tplgfr25s_1?idArticle=LEGIARTI000024423600&cidTexte=LEGITEXT000024423570&dateTexte=20180522) is given to the professional.

Once the professional meets these qualification requirements, he must apply for registration on the national list of automotive experts (see infra "5°. a. Application for inclusion on the list of automotive experts").

*For further information*: Article R. 326-11 of the Highway Traffic Act; order of 26 July 2011 relating to the obtaining and maintenance of the qualification for the control of damaged vehicles for automotive experts.

#### Costs associated with qualification

Training leading to an automotive expert diploma and knowledge refresh training are usually paid for. It is advisable to get closer to the approved establishments for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may engage in the same temporary and occasional activity in France.

Where the Member State does not regulate access to the activity or its exercise, the national must justify having carried out this activity for one year in the last ten years.

Once the person meets these conditions, the person must make a declaration to the Minister responsible for transport before the first service delivery.

*For further information*: Article L. 326-4 of the Highway Traffic Act; Sections R. 326-5, R. 326-6, R. 326-8, R. 326-8-1 and R. 326-9 of the Highway Traffic Act.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of a legally established EU state or EEA who is an automotive expert may carry out the same activity in France on a permanent basis.

In order to do so, the person concerned must register himself on the national list of automotive experts, under the same conditions as the French national (see infra "5°. a. Application for inclusion on the list of automotive experts").

*For further information*: Articles L. 326-1 and L. 326-4 of the Highway Traffic Act; Sections R. 326-5, R. 326-6, R. 326-8, R. 326-8-1 and R. 326-9 of the Highway Traffic Act.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Conditions of honorability**

In order to be an automotive expert, the professional must not have been the subject of any conviction for:

- Theft;
- Scam;
- breach of trust
- Receiving;
- Sexual assaults;
- subtraction committed by a custodian of public authority;
- false testimony;
- corruption or influence peddling;
- a misdemeanor punishable by theft, fraud or breach of trust.

*For further information*: Article L. 326-2 of the Highway Traffic Act.

**Incompatibilities**

The activity of automotive expert is incompatible with:

- Holding a public or ministerial officer office;
- activities related to the production, sale, rental, repair and representation of motor vehicles and accessory parts;
- Insurance.

*For further information*: Article L. 326-6 of the Highway Traffic Act.

**Professional rules**

The automotive expert is required to:

- indicate the price of the benefit to the person seeking their services;
- to give its conclusions within its mission but must inform the owner of the vehicle of any failure to comply with the vehicle that could endanger the life of others;
- to prepare an expert report with the following, and send a copy to the vehicle owner:- The name of the expert who carried out the expertise,
  - All the transactions carried out,
  - The name and quality of the people present at the expert meeting,
  - Documents provided by the owner,
  - The expert's conclusions
- inform the owner and custodian of the vehicle as soon as they become aware of a dispute about the technical conclusions of the vehicle or the cost of damage or repairs.

Given the high level of road safety issues related to the practice of the profession and the missions of automotive experts, the professional incurs a disciplinary sanction in case of misconduct or breach of these rules and his professional obligations.

*For further information*: Articles R. 326-1 to R. 326-4, Sections R. 326-14 and D. 326-15 of the Highway Traffic Act.

4°. Insurance and sanctions
-----------------------------------------------

**Insurance**

The automotive expert must take out liability insurance.

*For further information*: Article L. 326-7 of the Highway Traffic Act.

**Sanctions**

A professional who works as an automotive expert without being on the list or professionally qualified faces a one-year prison sentence and a fine of 15,000 euros.

In addition, in the event of a conviction for breaches of honour or probity, the professional incurs a temporary or permanent ban from practising his activity.

*For further information*: Articles L. 326-8 and L. 326-9 of the Highway Traffic Act; Decree of 5 February 2002 amending the order of 13 August 1974 on the minimum conditions of the professional liability insurance contract to be undersigned by automotive experts; order of 13 August 1974 relating to the minimum conditions of the professional liability insurance contract to be undersigned by automotive experts.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Application for inclusion on the list of automotive experts

Registration on the national list of automotive experts is mandatory in order to be able to carry out this professional activity. It's worth amenities.

**Competent authority**

The professional must apply to the Minister responsible for transport.

**Supporting documents**

Its application must include the following information, if any, with their translation into French:

- his marital status;
- a copy is:- a professional automotive expert's certificate (obtained between 1977 and 1994) or the recognition of expert status, provided for by Decree No. 74-472 of 17 May 1974 relating to automotive experts, or the automotive expert's diploma created in 1995 (or his transcripts),
  - a title issued by an EU Member State equivalent to the above titles,
  - any documents justifying his professional experience in automotive expertise;
- a declaration of honour attesting that he holds no public or ministerial officer office and does not practice any profession incompatible with the conduct of that activity;
- A proof of insurance less than three months old
- an excerpt from bulletin 3 of his criminal record or an equivalent document for foreign nationals, as well as a statement on honour justifying that he has not been the subject of any criminal convictions (see supra "3." Conditions of honorability") less than three months old;
- proof that it meets the training requirements required for this activity.

**Time and procedure**

The Minister responsible for transport acknowledges receipt of the request within one month and decides on the application within three months of receiving the full file.

This list of experts is available on the[Road Safety website](http://www.securite-routiere.gouv.fr/).

*For further information*: Articles R. 326-5 and R. 326-13 of the Highway Traffic Act; Decree No. 95-493 of 25 April 1995 portan creation and general regulation of the automotive expert diploma; Decree 74-472 of 17 May 1974 relating to automotive experts.

**Good to know: compensation measure**

In the event of substantial differences between the training received by the professional abroad and that required in France to carry out the activity of automotive expert, likely to harm the safety of persons, the Minister in charge of transport may decide to submit to an aptitude test within a maximum of six months or an adjustment course lasting up to two years and carried out under the responsibility of an automotive expert on the national list.

*For further information*: decree of 15 June 2017 relating to the recognition of the professional qualifications of automotive experts; Articles R. 326, R. 326-8, R. 326-8-16-8 and R. 326-8-1 of the Highway Traffic Act).

### b. Pre-declaration for EU national for temporary and casual exercise (LPS)

**Competent authority**

The professional must submit his request by any means to the Minister responsible for transport.

**Supporting documents**

Its application must include the following documents, if any, with their translation into French:

- A document justifying his identity
- a certificate certifying that it is legally established in an EU Member State to carry out the activity of automotive expert and is not prohibited from practising;
- proof of his professional qualifications
- where the EU Member State does not regulate access to the profession or its exercise, evidence that the person has engaged in the practice of monitoring repairs and re-entering vehicles for at least one year in the next ten years In recent years
- a document justifying that he has taken out liability insurance.

**Time and procedure**

The Minister responsible for transport verifies the national's knowledge within one month of receiving the request. It registers the national on the list of automotive experts for a period of one year. After recognising his professional qualification, the Minister in charge of transport may ask the professional to justify his level of practice in French (Article R. 326-7 of the Highway Code).

In the absence of a response from the Minister within one month of receiving his application, the applicant is deemed to be on that list.

**Please note**

In order to renew his registration, the national must apply to the Minister responsible for transport for renewal and must undergo the training to update his knowledge (see above "2. a. Knowledge update").

*For further information*: Article R. 326-6 of the Highway Traffic Act; Article 2 of the order of 26 July 2011.

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

