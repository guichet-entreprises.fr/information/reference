﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP130" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Expert en automobile" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="expert-en-automobile" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/expert-en-automobile.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="expert-en-automobile" -->
<!-- var(translation)="None" -->

# Expert en automobile

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L'expert en automobile est un professionnel dont l'activité consiste à dresser des rapports d'expertise. Il intervient ainsi essentiellement dans le cadre des procédures « véhicules endommagés » régies par les dispositions des articles L. 327-1 à L. 327-6 et R.327-1 à R.327-6 du Code de la route. Ces procédures sont constituées des procédures « véhicules gravement endommagés » (VGE) et « Véhicules économiquement irréparables » (VEI).

Il est également chargé de mettre en œuvre la procédure de remise en circulation de ces véhicules dans le cadre de la procédure VGE.

L'expert a en conséquence un rôle majeur en matière de sécurité routière. Il a ainsi l'obligation d'informer le propriétaire du véhicule si celui-ci est susceptible de mettre en danger la vie du conducteur ou celle d'autres personnes (article R. 326-2 du Code de la route). Il intervient principalement dès lors qu'un véhicule est accidenté et très souvent en étant mandaté par l'assureur du propriétaire du véhicule. C'est en effet généralement l'assureur du véhicule sinistré qui désigne l'expert en automobile dans le cadre d'une garantie dommages (tous accidents, collision, vol, etc.) comme dans celui d'une garantie responsabilité civile. L'expert alors pour le compte de la société d'assurance des actes matériels d'expertise (identification du véhicule, relevé des dommages et imputation de ceux-ci, définition d'une méthodologie de réparation, chiffrage du coût de la remise en état et de la valeur du véhicule s'il y a lieu) sans pouvoir de représentation. Les relations entre assureurs et experts sont ainsi souvent centrales dans la pratique concrète de la profession.

Ce professionnel peut être amené à exercer au sein d'un cabinet d'experts, auprès d'une compagnie d'assurance, auprès d'experts judiciaires, auprès des cabinets juridiques, de l'administration d'État ou des particuliers.

Conformément aux dispositions de l'article D. 114-12 du Code des relations entre le public et l'Administration, tout usager peut obtenir un certificat d'information sur les normes applicables à la profession d'expert en automobile. Pour ce faire, il convient d'adresser votre demande à l'adresse suivante : devenir-expert-automobile@interieur.gouv.fr.

*Pour aller plus loin* : article L. 326-4 du Code de la route.

## 2°. Qualifications professionnelles

### a. Exigences nationales

Pour exercer en tant qu'expert automobile et obtenir sa qualification pour le contrôle des véhicules endommagés, le professionnel doit :

- être titulaire d'un diplôme d'expert en automobile ;
- être inscrit sur une liste nationale des experts en automobile fixée par le ministre chargé des transports (cf. infra « 5°. a. Demande d'inscription sur la liste des experts en automobile ») ;
- avoir suivi une formation en vue d'actualiser ses connaissances juridiques et techniques nécessaires à la conduite des procédures relatives aux véhicules endommagés. À défaut d'avoir suivi cette formation et obtenu cette qualification VE, l'expert ne pourra intervenir dans le cadre des procédures VE.

*Pour aller plus loin* : article R. 326-11 du Code de la route.

**À noter**

Les professionnels titulaires d'un diplôme d'expert en automobile datant de moins d'un an, sont réputés avoir suivi la formation spécifique.

*Pour aller plus loin* : arrêté du 26 juillet 2011 relatif à l'obtention et au maintien de la qualification pour le contrôle des véhicules endommagés pour les experts en automobile.

#### Législation nationale

#### Formation

##### Diplôme d'expert en automobile

Pour être reconnu comme étant qualifié professionnellement, le professionnel doit être titulaire d'un diplôme d'expert en automobile de niveau III (bac +2). Ce diplôme confère la qualité d'expert en automobile mais ne suffit pas à lui-seul à permettre l'exercice de cette activité (cf. infra « 5°. a. Demande d'inscription sur la liste des experts en automobile »).

Ce diplôme est accessible aux candidats titulaires à la fois :

- d'un diplôme de niveau IV (bac) ou d'un titre homologué d'un tel niveau ;
- d'une expérience d'au moins trois ans, relative à la pratique de la réparation automobile ;
- d'une expérience d'au moins deux ans d'une activité d'expertise en tant que stagiaire auprès d'une personne ayant la qualité d'expert en automobile.

**À noter**

La durée de cette expérience professionnelle est ramenée à un an pour les titulaires des diplômes suivants :

- un brevet de technicien supérieur (BTS) dans le domaine de l'automobile ou le machinisme agricole ;
- un diplôme universitaire de technologie ;
- un diplôme d'ingénieur.

La liste de ces diplômes est fixée à l'annexe I de l'arrêté du 31 juillet 2012 portant définition du diplôme d'expert en automobile disponible au [Bulletin officiel](http://cache.media.education.gouv.fr//file/34/83/4/BO_MEN_20-09-12_226834.pdf) du ministère de l'Éducation nationale en date du 20 septembre 2012.

Ce diplôme est délivré aux candidats ayant subi avec succès un examen composé d'épreuves écrites et de travaux pratiques et oraux portant sur des unités d'enseignements. Le programme de ces unités est fixé à l'[annexe IIa](http://www.enseignementsup-recherche.gouv.fr/pid20536/bulletin-officiel.html?cid_bo=61279&cbo=1) de l'arrêté du 31 juillet 2012, disponible au Bulletin officiel susvisé.

*Pour aller plus loin* : décret n° 95-493 du 25 avril 1995 portant création et règlement général du diplôme d'expert en automobile ; arrêté du 31 juillet 2012 portant définition du diplôme d'expert en automobile.

##### Actualisation des connaissances

Le professionnel qualifié professionnellement doit effectuer une formation en vue d'actualiser ses connaissances juridiques et techniques nécessaires à la conduite des procédures relatives aux véhicules endommagés.

Cette formation d'une journée est dispensée par des organismes de formation agréés par le ministre chargé des transports. Elle comprend une partie administrative et une partie technique dont le programme est fixé à l'annexe 1 de l'arrêté du 26 juillet 2011.

À l'issue de cette formation, une attestation de suivi de formation dont le modèle est fixé à l'annexe 1 est remise au professionnel.

Dès lors que le professionnel remplit ces conditions de qualifications, il doit effectuer une demande d'inscription sur la liste nationale des experts en automobile (cf. infra « 5°. a. Demande d'inscription sur la liste des experts automobiles »).

*Pour aller plus loin* : article R. 326-11 du Code de la route ; arrêté du 26 juillet 2011 relatif à l'obtention et au maintien de la qualification pour le contrôle des véhicules endommagés pour les experts en automobile.

#### Coûts associés à la qualification

La formation menant au diplôme d'expert en automobile et la formation d'actualisation des connaissances sont généralement payantes. Il est conseillé de se rapprocher des établissements agréés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité d'expert automobile peut exercer, à titre temporaire et occasionnel, la même activité en France.

Lorsque l’État membre ne réglemente ni l'accès à l'activité ni son exercice, le ressortissant doit justifier avoir exercé cette activité pendant un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, l'intéressé doit, avant sa première prestation de services, effectuer une déclaration auprès du ministre chargé des transports.

*Pour aller plus loin* : II de l'article L. 326-4 du Code de la route ; articles R. 326-5, R. 326-6, R. 326-8, R. 326-8-1 et R. 326-9 du Code de la route.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE légalement établi et exerçant l'activité d'expert automobile peut exercer la même activité en France à titre permanent.

Pour cela, l'intéressé doit procéder à son inscription sur la liste nationale des experts en automobile, dans les mêmes conditions que le ressortissant français (cf. infra « 5°. a. Demande d'inscription sur la liste des experts en automobile »).

*Pour aller plus loin* : articles L. 326-1 et L. 326-4 du Code de la route ; articles R. 326-5, R. 326-6, R. 326-8, R. 326-8-1 et R. 326-9 du Code de la route.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Conditions d'honorabilité

Pour exercer l'activité d'expert en automobile, le professionnel ne doit avoir fait l'objet d'aucune condamnation pour :

- vol ;
- escroquerie ;
- abus de confiance ;
- recel ;
- agressions sexuelles ;
- soustraction commise par un dépositaire de l'autorité publique ;
- faux-témoignage ;
- corruption ou trafic d'influence ;
- un délit puni des peines du vol, de l'escroquerie ou de l'abus de confiance.

*Pour aller plus loin* : article L. 326-2 du Code de la route.

### Incompatibilités

L'activité d'expert en automobile est incompatible avec :

- la détention d'une charge d'officier public ou ministériel ;
- l'exercice d'activités relatives à la production, la vente, la location, la réparation et la représentation de véhicules à moteur et des pièces accessoires ;
- les assurances.

*Pour aller plus loin* : article L. 326-6 du Code de la route.

### Règles professionnelles

L'expert en automobile est tenu :

- d'indiquer le prix de la prestation à la personne qui sollicite ses services ;
- de donner ses conclusions dans la limite de sa mission mais doit informer le propriétaire du véhicule de tout défaut de conformité du véhicule susceptible de mettre en danger la vie d'autrui ;
- de dresser un rapport d'expertise mentionnant les éléments suivants, et en adresser une copie au propriétaire du véhicule :
  - le nom de l'expert ayant procédé à l'expertise,
  - l'ensemble des opérations effectuées,
  - le nom et la qualité des personnes présentes lors de l'expertise,
  - les documents communiqués par le propriétaire,
  - les conclusions de l'expert ;
- d'informer le propriétaire et le dépositaire du véhicule dès qu'il a connaissance d'une contestation sur les conclusions techniques du véhicule ou portant sur le coût des dommages ou des réparations.

Comte tenu des forts enjeux de sécurité routière liés à l'exercice de la profession et des missions des experts en automobile, le professionnel encourt une sanction disciplinaire en cas de faute ou de manquement à ces règles et à ses obligations professionnelles.

*Pour aller plus loin* : articles R. 326-1 à R. 326-4, articles R. 326-14 et D. 326-15 du Code de la route.

## 4°. Assurances et sanctions

### Assurance

L'expert automobile doit souscrire une assurance de responsabilité civile.

*Pour aller plus loin* : article L. 326-7 du Code de la route.

### Sanctions

Le professionnel qui exerce l'activité d'expert en automobile sans être inscrit sur la liste prévue à cet effet ou sans être qualifié professionnellement encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende.

En outre, en cas de condamnation pour manquements à l'honneur ou à la probité, le professionnel encourt une interdiction temporaire ou définitive d'exercer son activité.

*Pour aller plus loin* : articles L. 326-8 et L. 326-9 du Code de la route ; arrêté du 5 février 2002 modifiant l'arrêté du 13 août 1974 relatif aux conditions minimales du contrat d'assurance de la responsabilité civile professionnelle que doivent souscrire les experts en automobiles ; arrêté du 13 août 1974 relatif aux conditions minimales du contrat d'assurance de la responsabilité civile professionnelle que doivent souscrire les experts en automobile.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande d'inscription sur la liste des experts en automobile

L'inscription sur la liste nationale des experts en automobile est obligatoire pour pouvoir exercer cette activité professionnelle. Elle vaut agrément.

#### Autorité compétente

Le professionnel doit adresser sa demande auprès du ministre chargé des transports.

#### Pièces justificatives

Sa demande doit comporter les informations suivantes, le cas échéant, assorties de leur traduction en français :

- son état civil ;
- une copie soit :
  - d'un brevet professionnel d'expert en automobile (obtenu entre 1977 et 1994) ou de la reconnaissance de la qualité d'expert, prévue par le décret n° 74-472 du 17 mai 1974 relatif aux experts en automobile, ou du diplôme d'expert en automobile créé en 1995 (ou de son relevé de notes),
  - d'un titre délivré par un État membre de l'UE équivalent aux titres ci-dessus,
  - de tout document justifiant de son expérience professionnelle en matière d'expertise automobile ;
- une déclaration sur l'honneur attestant qu'il ne détient aucune charge d'officier public ou ministériel et n'exerce aucune profession incompatible avec l'exercice de cette activité ;
- un justificatif d'assurance datant de moins de trois mois ;
- un extrait du bulletin n° 3 de son casier judiciaire ou d'un document équivalent pour les ressortissants étrangers, ainsi qu'une déclaration sur l'honneur justifiant qu'il n'a fait l'objet d'aucune condamnation pénales (cf. supra « 3°. Conditions d'honorabilité ») datant de moins de trois mois ;
- un justificatif attestant qu'il satisfait aux conditions de formations requises pour l'exercice de cette activité.

#### Délai et procédure

Le ministre chargé des transports accuse réception de la demande dans un délai d'un mois et statue sur la demande dans un délai de trois mois à compter de la réception du dossier complet.

Cette liste des experts est consultable sur le [site internet de la Sécurité routière](http://www.securite-routiere.gouv.fr/).

*Pour aller plus loin* : articles R. 326-5 et R. 326-13 du Code de la route ; décret n° 95-493 du 25 avril 1995 portan création et réglement général du diplôme d'expert en automobile ; décret n° 74-472 du 17 mai 1974 relatif aux experts en automobile.

#### Bon à savoir : mesure de compensation

En cas de différences substantielles entre la formation reçue par le professionnel à l'étranger et celle exigée en France pour exercer l'activité d'expert automobile, de nature à nuire à la sécurité des personnes, le ministre chargé des transports peut décider de le soumettre à une épreuve d'aptitude dans un délai maximal de six mois ou à un stage d'adaptation d'une durée de deux ans maximum et réalisé sous la responsabilité d'un expert en automobile inscrit sur la liste nationale.

*Pour aller plus loin* : arrêté du 15 juin 2017 relatif à la reconnaissance des qualifications professionnelles des experts en automobile ; articles R. 326, R. 326-8, R. 326-8-16-8 et R. 326-8-1 du Code de la route).

### b. Déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le professionnel doit adresser sa demande par tout moyen au ministre chargé des transports.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant assortis de leur traduction en français :

- un document justifiant de son identité ;
- une attestation certifiant qu'il est légalement établi dans un État membre de l'UE pour exercer l'activité d'expert automobile et n'encourt aucune interdiction d'exercer ;
- un justificatif de ses qualifications professionnelles ;
- lorsque l’État membre de l'UE ne réglemente ni l'accès à la profession ni son exercice, la preuve que l'intéressé a exercé une activité de pratique des opérations de suivi des réparations et de remise en circulation des véhicules pendant au moins un an au cours des dix dernières années ;
- un document justifiant qu'il a souscrit une assurance de responsabilité civile.

#### Délai et procédure

Le ministre chargé des transports procède à la vérification des connaissances du ressortissant dans un délai d'un mois à compter de la réception de la demande. Il procède à l'inscription du ressortissant sur la liste des experts automobiles pour une durée d'un an. Après reconnaissance de sa qualification professionnelle, le ministre chargé des transports peut demander au professionnel de justifier de son niveau de pratique en français (article R. 326-7 du Code de la route).

En l'absence de réponse de la part du ministre dans un délai d'un mois à compter de la réception de sa demande, le demandeur est réputé inscrit sur cette liste.

**À noter**

Pour procéder au renouvellement de son inscription, le ressortissant doit adresser au ministre chargé des transports une demande de renouvellement et doit suivre la formation d'actualisation de ses connaissances (cf. supra « 2°. a. Actualisation des connaissances »).

*Pour aller plus loin* : article R. 326-6 du Code de la route ; article 2 de l'arrêté du 26 juillet 2011 susvisé.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).