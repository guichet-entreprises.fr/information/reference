﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP164" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Mandataire d'intermédiaire en assurance" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="mandataire-d-intermediaire-en-assurance" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/mandataire-d-intermediaire-en-assurance.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="mandataire-d-intermediaire-en-assurance" -->
<!-- var(translation)="None" -->

# Mandataire d'intermédiaire en assurance

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l'activité

Le mandataire d'intermédiaire en assurance (MIA) est un professionnel agissant dans le cadre d'un mandat écrit conclu avec un intermédiaire, et dont l'activité est la présentation, la proposition ou l’aide à la conclusion d’une opération d’assurance. Sous réserve qu'il bénéficie d'un contrat d'encaissement, le MIA peut également encaisser les primes d'assurance.

En sa qualité de mandataire, le MIA ne peut pas :

- mandater une autre personne ;
- gérer des contrats ;
- s'occuper du règlement des sinistres.

*Pour aller plus loin* : article L.511-1 du Code des assurances.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne qui souhaite exercer la profession de MIA doit justifier d'une capacité professionnelle en assurance de niveau II, obligatoire pour présenter des produits d'assurance.

**À noter**

Lorsque le professionnel souhaite exercer l'activité d'intermédiaire d'assurances à titre accessoire, il devra justifier d'une capacité professionnelle en assurance de niveau III seulement.

Pour en savoir plus sur les niveaux de capacité professionnelle en assurance, il est conseillé de se reporter sur le [site de l'Orias](https://www.orias.fr/web/guest/assurance2).

#### Formation

Pour exercer la profession de MIA, l'intéressé doit justifier :

- soit d'un stage professionnel dont le programme minimal de formation est fixé par l'arrêté du 11 juillet 2008. Il ne pourra être inférieur à une durée de 150 heures, et devra être effectué :
  - soit auprès d'une entreprise d'assurance ou d'un intermédiaire,
  - soit auprès d'un centre de formation choisi par l'employeur ou le mandant ;

**À noter**

À l'issue du stage, le professionnel sera soumis à un contrôle des compétences acquises.

- soit d'une année d'expérience en tant que cadre dans une fonction relative à la production ou à la gestion de contrats d'assurance ou de capitalisation, au sein d'une entreprise d'assurance ou d'un intermédiaire en assurance ;
- soit de deux années d'expérience dans une fonction relative à la production ou à la gestion de contrats d'assurance ou de capitalisation au sein de ces mêmes entreprises ou de ces intermédiaires ;
- soit de la possession d'un diplôme ou d'un titre enregistré au Répertoire national des certifications professionnelles et correspondant simultanément :
  - au niveau de formation III de la nomenclature de formation utilisé par la commission nationale de la certification professionnelle,
  - à la spécialité de formation 313 relative aux activités de la finance, de la banque, des assurances et de l'immobilier ;
- soit d'un certificat de qualification professionnelle enregistré au Répertoire national des certifications professionnelle et correspondant à la spécialité de formation 313 relative aux activités de la finance, de la banque, des assurances et de l'immobilier.

Lorsque le professionnel exerce l'activité d'intermédiaire en assurance à titre accessoire, il devra justifier :

- soit d'avoir effectué une formation d'une durée raisonnable, adaptée aux produits et contrats qu'ils présentent ou proposent, sanctionnée par la délivrance d'une attestation de formation ;
- soit de présenter une ancienneté de six mois d'expérience dans une fonction relative à la production ou à la gestion de contrats d'assurance ou de capitalisation au sein d'une entreprise d'assurance ou d'un intermédiaire en assurance ;
- soit de la possession d'un diplôme ou d'un titre enregistré au Répertoire national des certifications professionnelles et correspondant simultanément :
  - au niveau de formation III de la nomenclature de formation utilisé par la commission nationale de la certification professionnelle,
  - à la spécialité de formation 313 relative aux activités de la finance, de la banque, des assurances et de l'immobilier ;
- soit d'un certificat de qualification professionnelle enregistré au Répertoire National des Certifications Professionnelles et correspondant à la spécialité de formation 313 relative aux activités de la finance, de la banque, des assurances et de l'immobilier.

*Pour aller plus loin* : articles R. 512-10 à R. 512-12, et A. 512-7 du Code des assurances.

#### Coût de la formation

La formation pour devenir MIA est payante. Pour plus d'information, il est conseillé de se rapprocher des établissements délivrant les diplômes ou certificats visés ci-dessus ou d'un centre de formation.

### b. Ressortissants de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d'un État de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE), établi et exerçant légalement les activités de MIA dans l'un de ces États, peut exercer la même activité en France de manière temporaire et occasionnelle.

Il doit simplement en informer au préalable le Registre de son État qui s'assurera de transmettre lui-même les informations au Registre français (cf. infra « 5°. a. Informer le Registre unique de l'État de l'UE ou de l'EEE »).

*Pour aller plus loin* : directive européenne d'intermédiation en assurance du 9 décembre 2002 dite « [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

### c. Ressortissants de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE, établi et exerçant légalement les activités de MIA dans l'un de ces États, peut exercer la même activité en France de manière permanente.

Il doit simplement en informer au préalable le Registre de son État qui s'assurera de transmettre lui-même les informations au Registre français (cf. infra « 5°. a. Informer le Registre unique de l'État de l'UE ou de l'EEE »).

*Pour aller plus loin* : directive européenne d'intermédiation en assurance du 9 décembre 2002 dite « [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le MIA est tenu de respecter des conditions d'honorabilité qui incombent à sa profession, conformément aux dispositions de l'article L. 322-2 du Code des assurances. Il ne doit notamment pas avoir fait l'objet d'une condamnation depuis moins de dix ans pour crime, à des peines pour blanchiment, corruption ou encore fraude fiscale, ou à la destitution des fonctions d'officier public ou ministérielles.

**À noter**

Un extrait du bulletin n° 2 du casier judiciaire du MIA est directement demandé par l'Orias pour l'exercice de la profession.

*Pour aller plus loin* : articles L. 322-2, L. 512-4 et R. 514-1 du Code des assurances.

## 4°. Obligation d'immatriculation et assurances

### a. Demander son immatriculation au registre de l'Orias

L'intéressé qui souhaite exercer en France la profession de MIA doit impérativement être immatriculé au Registre unique des intermédiaires en assurance, banque et finance (Orias).

#### Pièces justificatives

L'immatriculation est précédée par l'[ouverture d'un compte](https://www.orias.fr/espace-professionnel) sur le site de l'Orias et de l'envoi d'un dossier comportant l'ensemble des pièces justificatives suivantes :

- une copie de la pièce d'identité de l'intéressé ou un extrait KBis s'il est immatriculé au registre du commerce et des sociétés ;
- un document justifiant de sa capacité professionnelle telle que précisée au paragraphe « 2°. a. Formation » :
  - un livret de stage,
  - une attestation de formation,
  - une attestation de fonction,
  - un diplôme ou un certificat ;
- tout document attestant de l'existence d'un ou plusieurs mandats ;
- en cas d'encaissement de fonds, une attestation de garantie financière ;
- le règlement des frais d'inscription.

#### Renouvellement de l'inscription

La procédure d’immatriculation à l'Orias doit être renouvelée tous les ans et lors de toute modification de la situation professionnelle de l'intéressé. Dans ce dernier cas, le MIA devra tenir informé l'Orias un mois avant la modification ou dans le mois qui suit l’événement modificatif.

La demande de renouvellement doit impérativement intervenir avant le 31 janvier de chaque année et doit comporter :

- le règlement des frais d'inscription ;
- en cas d'encaissement de fonds, une attestation de garantie financière couvrant la période du 1er mars de l’année N au 28 février de l’année N+1.

**À savoir**

Les modèles d'attestations sont directement consultables sur le [site de l'Orias](https://www.orias.fr/web/guest/en-savoir-plus-ias).

#### Coût

Les frais d'immatriculation sont fixés à 30 € payables directement en ligne sur le site de l'Orias. En cas de non règlement de ces frais, l'Orias envoie un courrier informant l'intéressé qu'il dispose d'un délai de trente jours à compter de la date d'accusé réception du courrier pour régler la somme. À défaut du règlement de cette somme :

- la demande de première inscription à l'Orias ne sera pas prise en compte ;
- le MIA sera radié du registre, lorsqu'il s'agissait d'une demande de renouvellement.

*Pour aller plus loin* : article R. 512-5 du Code des assurances.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

En sa qualité de professionnel exerçant une activité libérale, l'intéressé a l’obligation de souscrire une assurance de responsabilité civile professionnelle.

Cependant, il pourra bénéficier du contrat d'assurance de responsabilité civile professionnelle de l'intermédiaire d'assurance par lequel il est mandaté, si ce dernier le déclare en qualité d'assuré additionnel.

### c. Obligation de souscrire une assurance garantie financière

Tout personne exerçant la profession de MIA, dès lors qu'elle encaisse des fonds, même à titre occasionnel, est tenue de souscrire à une assurance garantie financière, affectée au remboursement de ces fond à ses assurés.

Le montant minimum de la garantie financière doit être au moins égal à 115 000 euros, et ne peut être inférieur au double du montant moyen mensuel des fonds encaissés par le MIA, calculé sur la base des fonds encaissés au cours des douze derniers mois précédant le mois de la date de souscription ou de reconduction de l'engagement de caution.

*Pour aller plus loin* : articles L. 512-7, R. 512-15 et A. 512-5 du Code des assurance.

### d. Sanctions pénales

Tout intéressé qui ne respecterait pas l'une des obligations visées aux paragraphes 3° et 4° est passible d'une peine d'emprisonnement de deux ans ou d'une amende de 6 000 euros, ou des deux peines cumulées.

*Pour aller plus loin* : article L. 514-1 du Code des assurances.

## 5°. Démarches et formalités de reconnaissance de qualifications

### a. Informer le Registre unique de l'État de l'UE ou de l'EEE

#### Autorité compétente

Le ressortissant d'un État membre de l'UE ou de l'EEE qui exerçait l'activité de MIA dans cet État et qui souhaite exercer en libre prestation de service ou en libre établissement en France, doit au préalable en informer le Registre unique de son État.

#### Procédure

Le ressortissant devra communiquer au Registre unique de son État les informations suivantes :

- son nom, son adresse et, le cas échéant, son numéro d'immatriculation ;
- l’État membre dans lequel il souhaite exercer son activité en cas de LPS ou s'établir en cas de LE ;
- la catégorie d'intermédiaire d'assurance à laquelle il appartient, à savoir celle de MIA ;
- les branches d'assurance concernées s'il y a lieu.

#### Délai

Le Registre unique de l'État de l'UE ou de l'EEE auprès duquel le ressortissant a notifié son intention d'exercer son activité en France, dispose d'un délai d'un mois pour communiquer à l'Orias les informations relatives à l'intéressé.

Le ressortissant pourra ensuite commencer son activité de MIA en France dans un délai d'un mois après que le Registre unique de son État l'ait informé de la communication faite à l'Orias.

*Pour aller plus loin* : articles L. 515-1 et L. 515-2 du Code des assurances, article 6 de la directive européenne « DIA1 » du 9 décembre 2002 et articles 4 et 6 de la directive européenne sur l'intermédiation en assurance du 20 janvier 2016 dite [« DIA2 »](https://www.orias.fr/documents/10227/26917/2016-01-20_Directive%20europeenne%20sur%20la%20distribution%20assurances.pdf).

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).