﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP164" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Insurance intermediary representative" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="insurance-intermediary-representative" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/insurance-intermediary-representative.html" -->
<!-- var(last-update)="2020-04-15 17:21:08" -->
<!-- var(url-name)="insurance-intermediary-representative" -->
<!-- var(translation)="Auto" -->


Insurance intermediary representative
============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:08<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Insurance Intermediary Agent (MIA) is a professional acting under a written mandate with an intermediary, whose activity is the presentation, proposal or assistance in the conclusion of an insurance transaction. Provided it has a cashing contract, the MIA can also collect insurance premiums.

As an agent, the MIA cannot:

- Appointing another person
- Manage contracts
- take care of claims settlement.

*For further information*: Article L.511-1 of the Insurance Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Anyone who wishes to practice the profession of MIA must justify a professional capacity in Level II insurance, mandatory to present insurance products.

**Please note**

When the professional wishes to act as an insurance intermediary as an ancillary, he will have to justify a professional capacity in Level III insurance only.

To learn more about the levels of professional capacity in insurance, it is advisable to visit the[Orias](https://www.orias.fr/web/guest/assurance2).

#### Training

In order to practice the profession of MIA, the person concerned must justify:

- or a professional internship whose minimum training programme is set by the[decreed from 11 July 2008](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000019216662). It cannot be less than 150 hours, and must be carried out:- either with an insurance company or an intermediary,
  - either at a training centre chosen by the employer or principal;

**Please note**

At the end of the internship, the professional will be subject to a check of the acquired skills.

- or one year of experience as an executive in a function related to the production or management of insurance or capitalization contracts, within an insurance company or an insurance intermediary;
- two years of experience in a function related to the production or management of insurance or capitalization contracts within these same companies or intermediaries;
- either the possession of a diploma or a title registered in the National Directory of Professional Certifications and corresponding simultaneously:- training level III of the training nomenclature used by the National Commission for Professional Certification,
  - Training specialty 313 relating to the activities of finance, banking, insurance and real estate;
- or a certificate of professional qualification registered in the national directory of professional certifications and corresponding to the speciality of training 313 relating to the activities of finance, banking, insurance and real estate.

When the professional is an insurance intermediary as an incidental, he or she will have to justify:

- either to have carried out training of a reasonable duration, adapted to the products and contracts they present or propose, sanctioned by the issuance of a certificate of training;
- or to have six months of experience in a function related to the production or management of insurance or capitalization contracts within an insurance company or insurance intermediary;
- either the possession of a diploma or a title registered in the National Directory of Professional Certifications and corresponding simultaneously:- training level III of the training nomenclature used by the National Commission for Professional Certification,
  - Training specialty 313 relating to the activities of finance, banking, insurance and real estate;
- or a certificate of professional qualification registered in the National Register of Professional Certifications and corresponding to the speciality of training 313 relating to the activities of finance, banking, insurance and real estate .

*For further information*: Articles R. 512-10 to R. 512-12, and A. 512-7 of the Insurance Code.

#### Cost of training

Training to become a MIA pays off. For more information, it is advisable to approach the institutions issuing the above diplomas or certificates or to a training centre.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) state or party to the European Economic Area (EEA), established and legally operating MIA in one of these states, may carry out the same activity in France on a temporary and occasional basis.

He must simply inform the Register of his State beforehand, which will ensure that the information is transmitted to the French Register himself (see infra "5°. a. Inform the Single Register of the EU State or the EEA").

*For further information*: European Insurance Intermediation Directive of 9 December 2002 known as "[DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state, established and legally carrying out MIA's activities in one of these states, may carry out the same activity in France on a permanent basis.

He must simply inform the Register of his State beforehand, which will ensure that the information is transmitted to the French Register himself (see infra "5°. a. Inform the Single Register of the EU State or the EEA").

*For further information*: European Insurance Intermediation Directive of 9 December 2002 known as "[DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The MIA is required to comply with the conditions of honour that are the responsibility of its profession, in accordance with the provisions of the[Article L. 322-2 of the Insurance Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073984&idArticle=LEGIARTI000006797493&dateTexte=&categorieLien=cid). In particular, he must not have been convicted for less than ten years for crimes, money laundering, corruption or tax evasion, or the removal of public or ministerial official duties.

**Please note**

An excerpt from bulletin 2 of the MIA's criminal record is directly requested by the Orias for the practice of the profession.

*For further information*: Articles L. 322-2, L. 512-4 and R. 514-1 of the Insurance Code.

4°. Registration requirement and insurance
--------------------------------------------------------------

### a. Request registration on the Orias register

The person concerned who wishes to practise in France in the profession of MIA must be registered in the Single Register of Intermediaries in Insurance, Banking and Finance (Orias).

**Supporting documents**

The registration is preceded by the[opening an account](https://www.orias.fr/espace-professionnel) Orias website and sending a file containing all the following supporting documents:

- A copy of the person's ID or a KBis extract if it is registered in the Register of Trade and Companies;
- a document justifying his professional capacity as specified in paragraph "2." a. Training":- an internship booklet,
  - A certificate of training,
  - A certificate of office,
  - A diploma or certificate
- any document attesting to the existence of one or more warrants;
- In the event of cashing in funds, a certificate of financial guarantee;
- settlement of registration fees.

**Renewal of registration**

The registration procedure at the Orias must be renewed every year and with any change in the person's professional situation. In the latter case, the MIA will have to keep the Orias informed one month before the change or within one month of the amending event.

The renewal application must take place before January 31 of each year and must include:

- Settlement of registration fees
- in the event of cashing in, a financial guarantee certificate covering the period from 1 March of year N to 28 February of year No. 1.

**What to know**

The certificate templates are directly available on the[Orias](https://www.orias.fr/web/guest/en-savoir-plus-ias).

**Cost**

The registration fee is set at 30 euros payable directly online on the Orias website. In the event of non-payment of these charges, orias sends a letter informing the person concerned that it has a period of thirty days from the date of receipt of the mail to pay the sum. Failing to settle this amount:

- The application for a first registration to the Orias will not be taken into account;
- the MIA will be removed from the register when it was an application for renewal.

*For further information*: Article R. 512-5 of the Insurance Code.

### b. Obligation to take out professional liability insurance

As a professional in a liberal activity, the individual is obliged to take out professional liability insurance.

However, he will be able to benefit from the professional liability insurance policy of the insurance intermediary by which he is mandated, if the latter declares him as an additional insured.

### c. Obligation to take out financial guarantee insurance

Any person practising the profession of MIA, as long as he cashes funds, even on a casual basis, is required to take out financial guarantee insurance, assigned to the repayment of these funds to his policyholders.

The minimum amount of the financial guarantee must be at least EUR 115,000, and may not be less than double the monthly average amount of funds collected by the MIA, calculated on the basis of funds collected in the last twelve months prior to the month of date of subscription or renewal of the bond commitment.

*For further information*: Articles L. 512-7, R. 512-15 and A. 512-5 of the Insurance Code.

### d. Criminal sanctions

Any person interested in complying with one of the obligations under paragraphs 3 and 4 is liable to a two-year prison sentence or a fine of 6,000 euros, or two cumulative penalties.

*For further information*: Article L. 514-1 of the Insurance Code.

5°. Qualifications recognition procedures and formalities
-------------------------------------------------------------------

### a. Inform the Single Register of the EU State or the EEA

**Competent authority**

A national of an EU or EEA Member State who was engaged in MIA's activity in that state and who wishes to practice self-service or self-establishment in France must first inform the single register of his or her state.

**Procedure**

The national will have to provide the following information to the Single Register of his or her state:

- His name, address and, if applicable, registration number;
- The Member State in which it wishes to operate in the event of LPS or establish itself in the event of an LE;
- The category of insurance intermediary to which it belongs, namely that of MIA;
- insurance branches, if necessary.

**Timeframe**

The Single Register of the EU State or EEA, to which the national has notified his intention to operate in France, has one month to provide the Orias with the information relating to him.

The national will then be able to start his MIA activity in France within one month after the Single Register of his State has informed him of the communication made to the Orias.

*For further information*: Articles L. 515-1 and L. 515-2 of the Insurance Code, Article 6 of the European Directive "DIA1" of 9 December 2002 and Articles 4 and 6 of the European Directive on Insurance Intermediation of 20 January 2016["DIA2"](https://www.orias.fr/documents/10227/26917/2016-01-20_Directive%20europeenne%20sur%20la%20distribution%20assurances.pdf).

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

