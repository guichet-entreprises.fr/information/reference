﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP291" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Commissaire de justice" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="commissaire-de-justice" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/commissaire-de-justice.html" -->
<!-- var(last-update)="2023-04" -->
<!-- var(url-name)="commissaire-de-justice" -->
<!-- var(translation)="None" -->

# Commissaire de justice

Dernière mise à jour : <!-- begin-var(last-update) -->2023-04<!-- end-var -->

## Définition de la profession

La profession de commissaire de justice est née le 1er juillet 2022 de la fusion des professions d'huissier de justice et de commissaire-priseur judiciaire. 

Le commissaire de justice a la qualité d'officier public et ministériel dès sa nomination par le garde des Sceaux, ministre de la Justice. 

Il exerce des activités monopolistiques : ramener à exécution les décisions de justice ainsi que les actes ou titres en forme exécutoire, procéder aux inventaires, prisées et ventes aux enchères publiques de meubles corporels ou incorporels prescrits par la loi ou par décision de justice, signifier les actes et les exploits, faire les notifications prescrites par les lois et règlements lorsque le mode de notification n'a pas été précisé, accomplir les mesures conservatoires après l'ouverture d'une succession, assurer le service des audiences près les cours et tribunaux.

Il peut exercer également des activités concurrentielles : procéder au recouvrement amiable ou judiciaire de toutes créances, effectuer lorsqu'ils sont commis par justice ou à la requête de particuliers des constatations purement matérielles, ou encore être désigné à titre habituel en qualité de liquidateur dans certaines procédures de liquidation judiciaire.

Il peut, en outre, exercer des activités accessoires comme celles d'administrateur d'immeubles, d'agent d'assurances ou de médiateur judiciaire ou à titre conventionnel.

Enfin, le commissaire de justice peut exercer, sous certaines conditions, des activités de ventes volontaires.

## Qualifications professionnelles requises en France

### Les conditions

Pour exercer l'activité de commissaire de justice, le professionnel doit remplir les conditions suivantes :

- être français ou ressortissant d'un autre État membre de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE) ;
- n'avoir pas été l'auteur de faits ayant donné lieu à une condamnation pénale définitive pour des agissements contraires à l'honneur, à la probité ou aux bonnes mœurs ou de faits de même nature ayant donné lieu à une sanction disciplinaire ou administrative définitive de destitution, radiation, révocation, de retrait d'agrément ou d'autorisation dans la profession qu'il exerçait antérieurement ;
- n'avoir pas été frappé de faillite personnelle ou de l'interdiction prévue à l'article L. 653-8 du code de commerce ;
- être titulaire soit d'un master en droit (bac + 5), soit de l'un des titres ou diplômes qui seront reconnus comme équivalents pour l'exercice de la profession de commissaire de justice par arrêté du garde des Sceaux, ministre de la Justice ;
- avoir subi avec succès l'examen d'accès à la formation professionnelle de commissaire de justice, sous réserve de dispenses ;
- avoir subi la formation professionnelle initiale de commissaire de justice, sous réserve de dispenses ;
- avoir subi avec succès l'examen d'aptitude à la profession de commissaire de justice.

### La formation professionnelle initiale

L'accès à la profession nécessite, sauf cas de dispense, le suivi d'une formation initiale, d'une durée de deux ans, qui est assurée par la Chambre nationale des commissaires de justice, dans le cadre d'un institut national, placé sous son autorité. 

Les candidats admis à suivre la formation aux fonctions de commissaires de justice prennent le titre de commissaire de justice stagiaire.  
 
Elle comprend un enseignement théorique et un stage pratique :

- l'enseignement théorique est réparti en modules d'enseignement dont le programme et la durée sont fixés par arrêté du garde des Sceaux, ministre de la Justice ;
- le stage professionnel doit être accompli, sous le contrôle de la Chambre nationale des commissaires de justice, dans un office de commissaire de justice. Le stage peut, à la demande du stagiaire et pour une durée de six mois au maximum, être effectué soit dans un office de notaire, soit auprès d'un avocat, d'un expert-comptable ou d'un opérateur de ventes volontaires, soit dans une administration publique ou dans le service juridique ou fiscal d'une entreprise, ou encore dans un pays étranger auprès d'une personne exerçant une profession judiciaire ou juridique réglementée. Le commissaire de justice stagiaire participe alors à l'activité du maître de stage sous la direction et la responsabilité de celui-ci, sans pouvoir se substituer à lui pour les actes de sa profession.

A l'issue de la deuxième année de formation, un certificat d'accomplissement de la formation est délivré par la Chambre nationale des commissaires de justice au commissaire de justice stagiaire qui a suivi l'ensemble des modules obligatoires de la formation initiale et accompli le stage.

Pour exercer l'activité de commissaire de justice, le professionnel doit, enfin, avoir subi avec succès un examen d'aptitude à la profession de commissaire de justice. Cet examen est organisé par la Chambre nationale des commissaires de justice et a lieu au moins une fois par an.

Le programme, les conditions d'organisation et les modalités de l'examen sont fixés par arrêté du garde des Sceaux, ministre de la Justice, après avis du bureau de la Chambre nationale des commissaires de justice.
Nul ne peut se présenter plus de trois fois à l'examen d'aptitude à la profession de commissaire de justice.

L'examen d'aptitude à la profession de commissaire de justice est subi devant un jury national qui choisit les sujets des épreuves.

Le jury national est composé d'un magistrat de l'ordre judiciaire en activité ou honoraire (président), de deux professeurs de l'enseignement supérieur ou maîtres de conférences, l'un chargé d'un enseignement juridique, l'autre chargé d'un enseignement en histoire de l'art et de deux commissaires de justice, en activité ou ayant cessé d'exercer leurs fonctions depuis moins de cinq ans.

La Chambre nationale des commissaires de justice fixe le montant des droits de scolarité et d'examen, ainsi que les conditions dans lesquelles certaines personnes peuvent en être dispensées, pour les formations qu'il assure et pour les examens organisés pour la délivrance des certificats de spécialisation.

### Les dispenses

Sont dispensés de l'examen d'accès à la formation professionnelle de commissaire de justice, de tout ou partie de la formation initiale et de l'examen d'aptitude à la profession de commissaire de justice, par le bureau de la Chambre nationale des commissaires de justice :

- les anciens magistrats de l'ordre judiciaire régis par l'ordonnance du 22 décembre 1958 modifiée portant loi organique relative au statut de la magistrature, ainsi que les anciens membres du corps des magistrats des tribunaux administratifs et des cours administratives d'appel ;
- les anciens professeurs et anciens maîtres de conférences de droit ou de sciences économiques ;
- les anciens notaires ayant exercé leurs fonctions pendant au moins cinq ans ;
- les anciens avocats à la Cour de cassation et au Conseil d'Etat ayant exercé leurs fonctions pendant au moins deux ans ;
- les anciens avocats, les anciens avoués près les cours d'appel et les anciens conseils juridiques ayant exercé leurs fonctions pendant au moins cinq ans ;
- les anciens fonctionnaires de la catégorie A ou les personnes assimilées aux fonctionnaires de cette catégorie ayant exercé pendant trois ans au moins des activités juridiques ou fiscales au sein d'une personne morale de droit public ou d'une personne morale de droit privé chargée d'une mission de service public ;
- les personnes ayant accompli huit années au moins d'exercice professionnel dans le service juridique ou fiscal d'une entreprise, publique ou privée, employant au moins trois juristes ;
- les anciens greffiers, les anciens syndics et administrateurs judiciaires, les anciens mandataires judiciaires au redressement et à la liquidation des entreprises, ayant exercé leurs fonctions pendant au moins deux ans.

En outre, peuvent être dispensés soit de la condition de diplôme, soit de l'examen d'accès à la formation professionnelle de commissaire de justice et de tout ou partie de la formation :

- les clercs justifiant d'une pratique professionnelle d'au moins sept ans dans un ou plusieurs offices de commissaire de justice sont dispensés ;
- les personnes ayant exercé pendant cinq ans au moins les fonctions de principal clerc d'huissier de justice ou des activités professionnelles de responsable dans un office d'huissier de justice ou dans un organisme statutaire de la profession ;
- les personnes ayant exercé des fonctions de collaborateur d'huissier de justice pendant sept ans au moins et qui sont titulaires soit du certificat de capacité en droit, soit du diplôme universitaire de technologie des carrières juridiques et judiciaires, soit d'un diplôme national sanctionnant un premier cycle d'études juridiques, soit d'un diplôme délivré par l'Ecole nationale de procédure ou par la Chambre nationale des commissaires de justice.

### La nomination

Le professionnel remplissant les conditions générales d'aptitude aux fonctions telles que décrites ci-dessus doit être nommé commissaire de justice par arrêté du garde des Sceaux, ministre de la Justice.

La nomination s'effectue soit :

- **sur présentation** : dans ce cas, le candidat à la succession d'un commissaire de justice sollicite l'agrément du garde des Sceaux, ministre de la Justice, dans les conditions prévues aux articles 2 à 4 du décret n° 2022-949 du 29 juin 2022 relatif aux conditions d'exercice des commissaires de justice ;
- **dans un office à créer** : les commissaires de justice peuvent librement s'installer dans les zones où l'implantation d'offices apparaît utile pour renforcer la proximité ou l'offre de services, dans les conditions prévues à l'article 52 de la loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques. Ces zones sont déterminées par une carte, rendue publique et révisée tous les deux ans et établie conjointement par les ministres de la Justice et de l'Économie, sur proposition de l'Autorité de la concurrence. Les demandes de nomination, horodatées, sont enregistrées par téléprocédure sur le [site internet du ministère de la Justice](http://www.justice.gouv.fr/). Un tirage au sort permet de classer l'ordre des demandes et dès qu'une demande est tirée au sort, le demandeur indique, par téléprocédure sur le site internet du ministère de la Justice s'il maintient sa demande dans un délai de dix jours francs suivant la publication du procès-verbal du tirage au sort. Passé ce délai, il est réputé y avoir renoncé. Un arrêté du garde des Sceaux, ministre de la Justice, procède à la nomination du titulaire de l'office qu'il crée et dont il désigne le siège;
- **dans un office vacant** : un office non pourvu par l'exercice du droit de présentation à un office de commissaire de justice dépourvu de titulaire est déclaré vacant par arrêté du garde des Sceaux, ministre de la Justice. Cet arrêté ouvre une procédure de candidature. Le garde des Sceaux, ministre de la Justice, nomme à l'office vacant un candidat suivant l'ordre d'enregistrement des candidatures. En cas de pluralité de candidatures, enregistrées dans les vingt-quatre heures suivant l'ouverture de la procédure de candidature, l'ordre des candidatures est fixé par tirage au sort;

Le commissaire de justice peut également être recruté en qualité de salarié. Dans ce cas, le commissaire de justice salarié est également nommé par arrêté du ministre de la Justice.

**A noter :** Dans le mois de leur nomination, les commissaires de justice prêtent serment devant la cour d'appel du siège de leur office. Ils ne peuvent exercer leurs fonctions qu'à compter du jour de leur prestation de serment.

## Particularités de la réglementation de la profession

### Déontologie

Le professionnel exerçant l'activité de commissaire de justice est tenu au respect des règles déontologiques communes aux officiers publics et ministériels. 

A ce titre, il est tenu :

- à une obligation d'indépendance et d'impartialité, qui contraint le commissaire de justice à la neutralité dans l'exercice de sa mission. Il ne peut exercer son ministère s'il connaît ou entretient des liens avec l'une des parties ;
- d'agir en évitant tout conflit d'intérêts ;
- d'agir en toute probité ; 
- de respecter le secret professionnel ;
- de faire preuve de dignité : le commissaire de justice doit agir auprès des justiciables sans exercer de contrainte inutile, ni mettre en œuvre des mesures disproportionnées.

Les sanctions qu'il encourt en cas de manquement à ses devoirs vont jusqu'à la radiation et à l'interdiction définitive d'exercer sa profession.

**A noter :** Au cours des deux premières années d'exercice professionnel, la formation continue du commissaire de justice inclut dix heures au moins portant sur la gestion d'un office, la déontologie et le statut professionnel.

### Cumul d'activités

Après en avoir informé la chambre régionale dont il relève ainsi que le procureur général près la cour d'appel dans le ressort de laquelle est établi son office, le commissaire de justice peut également exercer les activités accessoires suivantes :

- administrateur d'immeubles ;
- agent d'assurances ;
- médiateur judiciaire ou à titre conventionnel.

### Conditions d'honorabilité

L'honorabilité du commissaire de justice conditionne sa nomination à un office.

L'honorabilité d'un associé peut également conditionner la constitution de certaines sociétés ou la cession d'action et de parts sociales.

### Sanctions

En cas de manquement aux règles déontologiques et professionnelles, le commissaire de justice peut faire l'objet d'une procédure disciplinaire. 

A ce titre, il encourt les peines disciplinaires suivantes :

- l'avertissement ;
- le blâme ;
- l'interdiction d'exercer à titre temporaire pendant une durée maximale de dix ans ;
- la destitution, qui emporte l'interdiction d'exercice à titre définitif ;
- le retrait de l'honorariat. 

La juridiction disciplinaire peut prononcer, à titre principal ou complémentaire, une peine d'amende. 

### Incompatibilités

Les commissaires de justice ne peuvent, sauf dispositions contraires, se livrer à aucun commerce en leur nom, pour le compte d'autrui ou sous le nom d'autrui.

Le commissaire de justice peut exercer sa profession soit à titre individuel, soit dans le cadre d'une entité dotée de la personnalité morale à l'exception des formes juridiques qui confèrent à leurs associés la qualité de commerçant.

### Assurance

Chaque commissaire de justice et chaque structure d'exercice de commissaire de justice est tenue à une obligation d'assurance, qui se fait au moyen d'une cotisation obligatoire versée à la Chambre nationale. 

Ainsi, la Chambre nationale des commissaires de justice garantit la responsabilité professionnelle pour les actes que les commissaires de justice accomplissent en cette qualité, y compris celle encourue en raison de leurs activités accessoires.

### Garantie financière

Cette garantie de la responsabilité professionnelle des commissaires de justice est assurée au moyen d'une cotisation spéciale, due par chaque commissaire de justice et dont le taux tient compte, dans des proportions déterminées par la chambre nationale, de la moyenne des produits bruts de chaque office et, le cas échéant, des activités accessoires. 

Au niveau de la structure d'exercice, chaque société titulaire d'un office de commissaire de justice est tenue de contracter une assurance professionnelle. La responsabilité de cette société titulaire d'un office de commissaire de justice est garantie dans les mêmes conditions que précédemment.

### Obligation de formation professionnelle continue

La formation professionnelle continue est obligatoire car elle assure la mise à jour et le perfectionnement des connaissances nécessaires à l'exercice de la profession.

D'une durée de vingt heures au cours d'une année civile ou de quarante heures au cours de deux années consécutives, l'obligation de formation est satisfaite par :

- la participation à des formations, à caractère juridique ou artistique, dispensées par des établissements de l'enseignement supérieur ;
- la participation à des formations à caractère technique, juridique ou artistique, habilitées par la Chambre nationale des commissaires de justice et dispensées par des commissaires de justice ou des établissements d'enseignement ;
- l'assistance à des colloques ou à des conférences ayant un lien avec l'activité professionnelle de commissaire de justice ;
- le fait de dispenser des enseignements ayant un lien avec l'activité professionnelle de commissaire de justice, dans un cadre universitaire ou professionnel ;
- la publication de travaux ayant un lien avec l'activité professionnelle de commissaire de justice.

Au cours des deux premières années d'exercice professionnel, cette formation inclut dix heures au moins portant sur la gestion d'un office, la déontologie et le statut professionnel. 

Enfin, chaque année, le professionnel est tenu de déclarer auprès de la Chambre nationale s'il a satisfait à son obligation de formation et selon quelles modalités. 

### Restrictions tarifaires

Les activités exercées en monopole par les commissaires de justice sont soumises à des tarifs réglementés alors que les activités exercées en concurrence avec d'autres professionnelles font l'objet de conventions d'honoraires entre le professionnel et son client (article L. 444-1 du code de commerce). 

La détermination des tarifs réglementés est faite en appréciation de la rentabilité globale de l'activité de la profession selon un objectif de taux de résultat moyen. 

S'agissant des tarifs des commissaires de justice, outre les textes généraux applicables à l'ensemble des professions règlementées du droit (articles R. 444-1 et suivants), les articles R. 444-42 à R. 444-57 du code de commerce apportent des précisions propres à cette profession.

Les tarifs des prestations exercées en monopole sont déterminés par voie d'arrêté et codifiés, s'agissant des commissaires de justice aux articles A. 444-1 à A. 444-52 du code de commerce. La rémunération mentionnée dans ces articles est présentée hors taxes et doit se voir appliquer une taxe sur la valeur ajoutée de 20%. 

Par ailleurs, les articles R. 444-3 et R. 444-12 du code de commerce organisent également le remboursement des frais et débours auxquels les commissaires de justice ont droit dans le cadre de leurs activités monopolistiques.

Enfin, tous les professionnels dont les tarifs sont règlementés transmettent tous les ans au Gouvernement et à l'Autorité de la concurrence les données économiques et financières concernant leur activité régulée, c'est-à-dire, les principaux coûts et produits des activités en monopole.

Sur la base de ces données recueillies, le tarif de chaque prestation, arrêté conjointement par les ministres de la Justice et de l'Économie, est révisé tous les 2 ans.

### Restrictions en matière de publicité

S'agissant des règles relatives à la communication, le règlement déontologique national des huissiers de justice applicable aux commissaires de justice, prévoit que « toute mention comparative ou dénigrante » et « toute référence à des fonctions ou activités sans lien avec l'exercice de la profession d'huissier de justice » sont interdites dans toute communication. Le commissaire de justice doit informer la Chambre nationale préalablement à « toute intervention lors d'une manifestation publique de promotion de la profession (et notamment la participation à des colloques ou séminaires, la presse écrite, les émissions de radio et de télévision) » (article 7 du règlement déontologique). L'huissier de justice ou sa structure d'exercice peut figurer dans tout annuaire, sous réserve que les mentions qui le concernent et le contenu de l'annuaire ne soient pas contraires aux principes fondamentaux de la profession (article 9 du règlement déontologique).

Le commissaire de justice peut créer un site internet ou une page web en vue de proposer ses services à la condition que le nom de domaine utilisé n'évoque pas de façon générique le titre de la profession ou bien un domaine général du droit ou de l'activité relevant de la profession. Les encarts ou bannières publicitaires, autres que ceux de la profession exercée, sont interdits (article 2 du décret n°2019-257 du 29 mars 2019 relatif aux officiers publics ou ministériels). 

Il doit informer la Chambre nationale de toute ouverture ou modification substantielle d'un site internet, dans un délai de deux jours et lui communiquer le nom du domaine qui permettent d'y accéder (article 10 du règlement déontologique national des huissiers de justice approuvé par l'arrêté du 18 décembre 2018 de la garde des Sceaux, ministre de la Justice, applicable aux commissaires de justice selon l'alinéa 2 de l'article 40 de l'ordonnance n° 2022-544 du 13 avril 2022 relative à la déontologie et à la discipline des officiers ministériels).

Le commissaire de justice a la possibilité de recourir à la sollicitation personnalisée et à la proposition de services en ligne. Ces dernières doivent respecter les règles déontologiques de la profession et exclure tout élément comparatif et dénigrant. La sollicitation personnalisée ne peut être effectuée que sous la forme d'un envoi postal ou d'un courrier électronique adressé à une personne physique ou morale déterminée, destinataire de l'offre de service. Tout démarchage physique ou téléphonique, et tout envoi de SMS sur un téléphone mobile sont exclus. Toute sollicitation personnalisée en rapport avec une affaire particulière est interdite (décret n° 2019-257 du 29 mars 2019 relatif aux officiers publics ou ministériels).

## Démarches de reconnaissance de qualification professionnelle

### Libre Établissement (exercice stable et continu)

Peuvent être nommées commissaires de justice sans remplir les conditions de diplômes et de formation professionnelle les personnes qui justifient :

- de diplômes, certificats, autres titres ou formations assimilées permettant l'exercice de la même profession dans un État membre de l'Union européenne ou dans un autre État partie à l'accord sur l'Espace économique européen délivrés :
  - soit par l'autorité compétente de cet État et sanctionnant une formation acquise de façon prépondérante dans l'Espace économique européen ;
  - soit par un pays tiers, à condition que soit fournie une attestation émanant de l'autorité compétente de l'État membre ou partie qui a reconnu les diplômes, certificats, autres titres ou formations assimilées certifiant que leur titulaire a une expérience professionnelle de trois ans au moins dans cet État ;
- ou de l'exercice à plein temps de la même profession pendant une année au moins ou, en cas d'exercice à temps partiel, pendant une durée totale équivalente au cours des dix années précédentes dans un État membre ou partie qui ne réglemente pas l'accès ou l'exercice de cette profession à condition que cet exercice soit attesté par l'autorité compétente de cet État. Toutefois, la condition d'une expérience professionnelle d'une année n'est pas exigée lorsque le ou les titres de formation détenus par le demandeur sanctionnent une formation réglementée directement orientée vers l'exercice de la profession.

#### Autorité compétente

La demande doit être adressée au [bureau de la Chambre nationale des commissaires de justice](https://commissaire-justice.fr/devenir-commissaire-de-justice/reconversion-ou-evolution-professionnelle/).

#### Procédure

Le dossier du demandeur doit être adressé au bureau de la Chambre nationale des commissaires de justice qui lui délivre un récépissé.

Le [formulaire](https://commissaire-justice.fr/wp-content/uploads/2021/01/Document_dispense.pdf) à adresser est mis en ligne sur leur site internet.

#### Pièces justificatives

Le professionnel intéressé doit se rapprocher du bureau de la Chambre nationale des commissaires de justice afin de fournir les pièces nécessaires selon sa situation.

Le [formulaire](https://commissaire-justice.fr/wp-content/uploads/2021/01/Document_dispense.pdf) précise que la demande doit être accompagnée d'un curriculum vitae et des pièces justifiant la demande de dispense (diplômes, certificats, etc.).

#### Délai de réponse

La décision du bureau de la Chambre nationale des commissaires de justice doit intervenir dans un délai de quatre mois à compter de la date de la délivrance du récépissé. 

Elle est motivée et précise le niveau de qualifications professionnelles requis en France et le niveau des qualifications professionnelles que possèdent les candidats conformément à la classification figurant à l'article 11 de la directive du 7 septembre 2005 susvisée. 

#### Mesures de compensation

Le professionnel intéressé est appelé à subir, devant un jury, un examen d'aptitude dont le programme et les modalités sont fixés par arrêté du garde des Sceaux, ministre de la Justice.
Un candidat peut être dispensé d'une épreuve lorsque les connaissances, aptitudes et compétences acquises au cours de sa formation, de son expérience professionnelle antérieure ou de l'apprentissage tout au long de la vie et ayant fait l'objet, à cette fin, d'une validation en bonne et due forme par un organisme compétent, dans un État membre ou dans un pays tiers, sont de nature à rendre inutile le passage de cette épreuve. Toutefois, il ne peut être dispensé d'une vérification de ses connaissances relatives à la réglementation professionnelle et à la gestion d'un office.
La liste des candidats admis à se présenter à l'examen est établie par le bureau de la Chambre nationale des commissaires de justice. 

Nul ne peut se présenter plus de trois fois à l'examen d'aptitude.

#### Voies de recours

Le professionnel intéressé adresse son dossier au bureau de la Chambre nationale des commissaires de justice. À la réception du dossier complet, le bureau de la Chambre nationale des commissaires de justice délivre un récépissé.

La décision est notifiée à l'intéressé dans un délai de quatre mois à compter la date de la délivrance du récépissé. Elle est nécessairement motivée.

Cette décision est susceptible de recours adressé au greffe du tribunal administratif.

### Libre Prestation de Services (exercice temporaire et occasionnel)

Aucune disposition spécifique n'est prévue pour le ressortissant d'un État membre de l'UE ou de l'EEE en vue d'exercer les activités mentionnées à l'article 1er de l'ordonnance n° 2016-728 du 2 juin 2016 relative au statut de commissaire de justice, à titre temporaire et occasionnel en France.

## Service d'assistance

### Centre d'information français

Le [Centre ENIC-NARIC](https://www.france-education-international.fr/expertises/enic-naric?langue=fr) est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

### SOLVIT

[SOLVIT](https://sgae.gouv.fr/sites/SGAE/accueil/leurope-au-service-des-citoyens/reseau-solvit--resolution-effica.html) est un service fourni par l'Administration nationale de chaque État membre de l'UE ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

#### Conditions

L'intéressé ne peut recourir à SOLVIT que s'il établit :

- que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
- qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

#### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web). Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

#### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

#### Délai

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

#### Coût

Gratuit.

#### Issue de la procédure

À l'issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
- s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

#### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site internet](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

## Liens utiles

### Textes de référence

- Loi n° 66-879 du 29 novembre 1966 relative aux sociétés civiles professionnelles ;
- Loi n° 90-1258 du 31 décembre 1990 relative à l'exercice sous forme de sociétés des professions libérales soumises à un statut législatif ou réglementaire ou dont le titre est protégé et aux sociétés en participation financières de professions libérales ;
- Loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques ;
- Ordonnance n° 2016-728 du 2 juin 2016 relative au statut de commissaire de justice ;
- Ordonnance n° 2022-544 du 13 avril 2022 relative à la déontologie et à la discipline des officiers ministériels ;
- Décret n° 2016-883 du 29 juin 2016 relatif à l'exercice des professions de commissaire de justice et de notaire sous forme de société autre qu'une société civile professionnelle ou qu'une société d'exercice libéral ;
- Décret n° 2018-129 du 23 février 2018 relatif à la formation spécifique prévue au III de l'article 25 de l'ordonnance n° 2016-728 du 2 juin 2016 relative au statut de commissaire de justice ;
- Décret n° 2019-257 du 29 mars 2019 relatif aux officiers publics ou ministériels ;
- Décret n° 2019-1185 du 15 novembre 2019 relatif à la formation professionnelle des commissaires de justice et aux conditions d'accès à cette profession ;
- Décret n° 2021-1625 du 10 décembre 2021 relatif aux compétences des commissaires de justice ;
- Décret n° 2022-900 du 17 juin 2022 relatif à la déontologie et à la discipline des officiers ministériels ;
- Décret n° 2022-729 du 28 avril 2022 relatif à l'organisation de la profession de commissaires de justice ;
- Décret n° 2022-949 du 29 juin 2022 relatif aux conditions d'exercice des commissaires de justice ;
- Décret n° 2022-950 du 29 juin 2022 relatif à certaines sociétés constituées pour l'exercice de la profession de commissaire de justice.

### Autres liens utiles

- Bureau de la [Chambre nationale des commissaires de justice](https://commissaire-justice.fr/)
- [Legifrance](https://www.legifrance.gouv.fr/)