﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP074" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Auctioneer conducting volontary auctions" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="auctioneer-conducting-volontary-auctions" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/auctioneer-conducting-volontary-auctions.html" -->
<!-- var(last-update)="2020-04-15 17:21:02" -->
<!-- var(url-name)="auctioneer-conducting-volontary-auctions" -->
<!-- var(translation)="Auto" -->


Auctioneer conducting volontary auctions
====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:02<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The auctioneer of voluntary sales is a professional responsible for assessing and sedring (detailing all the characteristics) an object or furniture, giving it a value and conducting its promotion and public auction.

**Please note**

The professional who makes voluntary sales of furniture at public auction takes the title of auctioneer of voluntary sales to the exclusion of any other.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To be a volunteer sales auctioneer, the professional must:

- Be French or a national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement;
- have not been the subject of a criminal conviction for facts contrary to honour or probity or, in his previous profession, a disciplinary or administrative sanction of dismissal, delisting, dismissal, withdrawal approval or authorisation for similar facts;
- Be professionally qualified and have successfully completed a two-year theoretical and practical training course;
- have made a prior declaration of activity with the Council for Voluntary Furniture Sales at public auctions (Voluntary Sales Council) (see infra "5°. a. Pre-declaration of activity").

*For further information*: Articles L. 321-4 and R. 321-18 of the Code of Commerce.

**Please note**

The voluntary sales of notaries and judicial officers in municipalities where there is no judicial auctioneer office can be carried out, subject to training. They then carry out their activity as an incidental. (see Article L. 321-2 of the Code of Commerce).

#### Training

To be recognized as a professionally qualified, the professional must have completed a two-year training course and hold either:

- a national law degree and a national bachelor's degree in one of the following areas:- art history,
  - Applied Arts,
  - Archaeology
  - visual arts;
- a degree recognized as equivalent to the above degrees, namely:- any national diploma sanctioning a level of training of at least three years after a bachelor's degree in the legal, economic, commercial or management disciplines;
  - any degree conferring the degree or master's degree, sanctioning studies in the legal, economic, commercial or management disciplines;
  - any diploma targeted by the Minister for Higher Education, sanctioning a level of training of at least three years of study after the bachelor's degree, in the legal, economic, commercial or management disciplines;
  - any diploma sanctioning a level of training corresponding to at least three years of study after the bachelor's degree in the legal, economic, commercial or management disciplines issued by the autonomous and co-managed free faculty of economics and Paris law until 2018 included;
  - any national diploma sanctioning a level of education of at least three years after the bachelor's degree in art history, applied arts, archaeology or visual arts;
  - any degree conferring a bachelor's degree or master's degree, sanctioning studies in art history, applied arts, archaeology or visual arts;
  - the louvre school's undergraduate degree;
  - the diploma of archivist paleographer issued by the National School of Charters;
  - the degree in bi-licensing law-history of art and archaeology from the University of Paris-I;
  - The bi-disciplinary degree in law and history of art from Lyon-II University;
  - the law-history degree from the University of Brest;
  - The diploma of "cultural goods and services consulting specialist" from the Institute of Graduate Studies of the Arts (IESA), issued until 2018 included.

**Please note**

The practice of certain professions also allows to be exempted from the national diploma in law (Articles R. 321-18 and R. 321-21 of the Code of Commerce)

*For further information*: Articles R. 321-18, A. 321-3 and A. 321-4 of the Code of Commerce.

**Request for access to internship and training**

The future professional holding one of the above diplomas must complete an accessible training course after an access exam.

The internship entrance exam takes place at least once a year and includes written and oral tests on artistic, legal, economic and accounting subjects as well as English and, as an option, another foreign living language.

**Competent authority**

The application must be addressed to the Voluntary Sales Council by letter recommended with notice of receipt or by any other equivalent means no later than one month before the date of the first test.

**Supporting documents**

His application must include:

- applying for the internship access exam;
- proof of identity and nationality
- A copy of his diplomas or training titles or the justification for their dispensation;
- the foreign language chosen for the internship entrance test.

**Time and outcome of the procedure**

The list of candidates eligible to take the examination of access to the internship is decided by the Voluntary Sales Council three weeks before the date of the first test.

The Voluntary Sales Council sends a certificate of success to the internship access exam to the candidate who has obtained the average for the admissions tests.

*For further information*: Articles A. 321-10 to A. 321-20 of the Code of Commerce.

**Training course**

With a duration of two years, of which a minimum of one year in France, the training course consists of:

- theoretical education in order to deepen the candidate's knowledge of the arts, technology, economics, accounting and legal matters;
- practical education and professional practice work.

At the end of the internship, a certificate of good completion of the internship is given to the intern by the Voluntary Sales Council and allows him to direct voluntary sales of furniture at public auctions.

**Please note**

Training internship exemptions are possible for some professionals justifying a professional practice of at least seven years in one or more offices of judicial auctioneer, with one or more sales operators. public auctioneers or sworn commodity brokers. These exemptions are only possible if these professionals have successfully passed an aptitude test before a jury including:

- a professor of art history;
- a heritage curator;
- a judicial auctioneer;
- a sworn commodity broker;
- two persons authorized to conduct voluntary furniture sales at public auctions.

*For further information*: Article R. 321-19 and Articles R. 321-26 to R. 321-31 of the Code of Commerce.

#### Costs associated with qualification

The training leading to the title of voluntary sales auctioneer is paid for and its cost varies according to the professional course envisaged. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of an EU or EEA Member State who is legally established and acts as a voluntary sales commissioner in that state may carry out the same activity in France on a temporary and occasional basis.

To do so, it must make a prior declaration to the Voluntary Sales Council (see infra "5°. b. Pre-declaration for EU or EEA nationals for temporary and casual exercise (LPS)).

Where neither activity nor training is regulated in the Member State in which the professional is legally established, he must have carried out this activity in one or more EU Member States for at least one year in the last ten years preceding the delivery.

*For further information*: Articles L. 321-24 and the following of the Code of Commerce.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

In order to carry out the activity of voluntary sales auctioneer in France on a permanent basis, the professional must:

- have the same professional qualifications as those required for a Frenchman (see above: "2. a. Professional qualifications");
- hold a certificate of competency or training certificate required for the exercise of the activity of auctioneer in an EU or EEA state when that state regulates access or the exercise of this activity on its territory;
- have a training certificate that attests to its preparation for the exercise of the activity of auctioneer when this certificate or title has been obtained in an EU or EEA state which does not regulate the access or exercise of this activity;
- have a certificate of competency or a training document that certifies the preparation for the exercise of the activity of voluntary sales auctioneer and also justify, in a Member State or a State party to the Agreement on the Economic Area which does not regulate access to or exercise of this occupation, a full-time exercise of the profession for at least one year in the previous ten years or for an equivalent period in the case of part-time exercise, provided that this exercise is attested by the competent authority of that state;
- apply for qualification recognition (see infra "5°). c. Application for qualification recognition for the national for a permanent exercise (LE)).

Where the training received by the professional relates to subjects substantially different from those required for the practice of the profession in France, the Voluntary Sales Council may require that he submit to a compensation measure (cf. infra "5°. (c) Good to know: compensation measures").

*For further information*: Articles R. 321-65 and the following of the Code of Commerce.

3°.Occupational rules and criminal sanctions
--------------------------------------------------------

**Professional rules**

The voluntary auctioneer must follow the following professional rules:

- Probity
- good manners;
- Honor.

Any breach or action contrary to these rules may be the subject of a conviction or a ban on practising.

*For further information*: Article L. 321-4 of the Code of Commerce.

**Criminal sanctions**

A professional is punished with two years' imprisonment and a fine of 375,000 euros for a professional to proceed or have a voluntary sale made:

- without having made the prior declaration of activity (see infra "5°. a. Pre-declaration of activity");
- without applying for qualification, where the professional is an EU national (see infra "5o v. Application for qualification recognition for THE EU or EEA national for a permanent exercise (LE);
- while it does not have the necessary qualifications to organize or carry out voluntary sales at public auctions or is prohibited from holding such a sale.

The professional also incurs additional penalties including:

- a ban on the activity for five years or more;
- The posting or dissemination of the sentence;
- forfeiture of money or objects improperly received by the professional.

*For further information*: Article L. 321-15 of the Code of Commerce.

4°. Advertising and insurance measures
----------------------------------------------------------

### Advertising measure

**Competent authority**

Before each voluntary sale of furniture at public auction, the professional must make publicity measures with the Voluntary Sales Council eight days before the sale.

**Supporting documents**

The professional must mention all the information relating to the organization of the sale, its technical and financial means as well as the following information:

- The date, location, and identity of the professional who will proceed with the sale, as well as the date of his declaration to the Voluntary Sales Council;
- The seller's quality as a trader or craftsman when the seller sells new products;
- The new character of the property;
- If so, the owner's status of the property put up for sale at the owner's time is an organizing voluntary sales operator or its employee, manager or partner, or an expert involved in the sale;
- The intervention of an expert in the organization of the sale;
- mention of the statute of limitations for civil liability actions incurred during a voluntary sale, i.e. a five-year period from the auction or prize.

**Please note**

The professional must keep an electronic sales register and a directory containing the minutes of the sales made every day.

*For further information*: Articles L. 321-7 and R. 321-32 to R. 321-35 of the Code of Commerce.

### Insurance

The professional who is a volunteer sales auctioneer must:

- Open an account in a credit institution to receive funds held on behalf of others during its activity;
- Take out professional liability insurance
- purchase insurance or a bond to guarantee the funds received during its activity. These guarantees can only be granted by a credit institution, an authorized financing company, an insurance company or a mutual surety company.

**Please note**

EU or EEA nationals must comply with these obligations for a permanent (LE) or temporary and casual exercise (LPS).

*For further information*: Articles L. 321-6, R. 321-10 to R. 321-17 and R. 321-56 of the Code of Commerce.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Pre-declaration of activity

**Competent authority**

The professional must send a statement by recommended letter with notice of receipt or by dematerialized to the Council of Voluntary Furniture Sales at public auctions.

**Supporting documents**

Its statement should include:

- Proof of identity
- a certificate certifying that he is not the subject of any criminal conviction, disciplinary sanction or ban on practising;
- A document justifying that the persons responsible for directing the sales are professionally qualified;
- A copy of the lease or title to the premises where the activity takes place, as well as the latest balance sheet or forecast balance sheet;
- A document justifying the opening of an account in a credit institution to receive funds held on behalf of others;
- A document justifying that the professional has taken out professional liability insurance;
- a document justifying that the professional has taken out insurance or a bond guaranteeing the representation of funds held on behalf of others.

*For further information*: Articles R. 321-1 to R. 321-4 of the Code of Commerce.

### b. Pre-declaration for EU or EEA national for temporary and casual exercise (LPS)

**Competent authority**

The professional must submit, by recommended letter with notice of receipt or any other written means, a request for prior declaration to the Voluntary Sales Council.

**Supporting documents**

The application must include the following in French or, if necessary, with a translation:

- Proof of the applicant's identity
- any document justifying the legality of the applicant's exercise, the voluntary sale of furniture at public auctions in the Member State, his professional status and, if necessary, the name of the professional body under his jurisdiction;
- if neither training nor activity is regulated in the Member State, proof by any means that it has exercised for at least one year in the last ten years the activity of a voluntary auctioneer;
- a certificate of less than three months certifying that he is not temporary or temporary to work;
- The date and location of the planned sale and the identity and qualification of the person responsible for the sale;
- evidence less than three months ago that the professional has taken out professional liability insurance or a bond guaranteeing the representation of funds held on behalf of others.

**Timeframe**

The prior declaration must be made at least one month before the first sale. The Voluntary Sales Council informs the applicant, if necessary, within a fortnight of receiving the request for any missing documents.

**Please note**

This declaration must be renewed once a year if the professional plans to practice on an occasional basis during the year or in the event of a change in his or her professional situation.

*For further information*: Articles L. 321-24 and subsequent articles R. 321-56 and the following of the Code of Commerce.

### c. Application for qualification recognition for EU or EEA nationals for permanent exercise (LE)

**Competent authority**

The professional who wishes to have his professional qualification recognised in order to carry out the activity of auctioneer in France on a permanent basis must apply for recognition of qualification by letter recommended with notice of or by any other means to the Voluntary Sales Council.

**Supporting documents**

The application must be accompanied by the following documents, if any translated into French:

- proof of the applicant's identity, nationality and residence;
- A copy of the applicant's certificate of competency or training designation giving him access to the activity of a voluntary sales auctioneer;
- If the national holds a diploma or training document issued by a third country and recognised by an EU member country or the EEA, a certificate issued by the competent authority of that Member State certifying the duration of the professional exercise of the applicant and exercise dates;
- a proof of the exercise of the activity of voluntary auctioneer in the last ten years since neither training nor activity is regulated in the Member State;
- proof of his professional qualification
- a document justifying that he is not the subject of any criminal conviction, disciplinary sanction or prohibition of the exercise of the activity of a volunteer sales commissioner.

**Time and remedies**

The Commission acknowledges receipt of the application within one month and informs the applicant if there is a missing piece. The Board's decision is sent by recommended letter to the applicant within three months of receipt of the full file. The Council's decision is subject to appeal by letter recommended with notice of receipt to the registry of the Court of Appeal of Paris.

*For further information*: Articles A. 321-66 and A. 321-27 of the Code of Commerce.

**Good to know: compensation measures**

The Voluntary Sales Council may require the professional to submit to a selection test before the professional jury in charge of the examination of access to the training course, or an adjustment course of up to three years.

*For further information*: Article R. 321-67 of the Code of Commerce.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

