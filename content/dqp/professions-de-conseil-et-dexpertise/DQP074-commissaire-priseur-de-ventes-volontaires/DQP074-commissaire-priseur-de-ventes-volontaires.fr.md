﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP074" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Commissaire-priseur de ventes volontaires" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="commissaire-priseur-de-ventes-volontaires" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/commissaire-priseur-de-ventes-volontaires.html" -->
<!-- var(last-update)="2020-04-15 17:21:02" -->
<!-- var(url-name)="commissaire-priseur-de-ventes-volontaires" -->
<!-- var(translation)="None" -->

# Commissaire-priseur de ventes volontaires

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:02<!-- end-var -->

## 1°. Définition de l'activité

Le commissaire-priseur de ventes volontaires est un professionnel chargé d'expertiser et de priser (détailler l'ensemble des caractéristiques) un objet ou bien meuble, de lui donner une valeur et de procéder à sa promotion et à sa vente aux enchères publiques.

**À noter**

Le professionnel qui procède aux ventes volontaires de meubles aux enchères publiques prend le titre de commissaire-priseur de ventes volontaires à l'exclusion de tout autre.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de commissaire-priseur de ventes volontaires, le professionnel doit :

- être français ou ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) ;
- n'avoir fait l'objet ni d'une condamnation pénale pour des faits contraires à l'honneur ou à la probité ni, dans la profession qu'il exerçait antérieurement, d'une sanction disciplinaire ou administrative de destitution, radiation, révocation, de retrait d'agrément ou d'autorisation pour des faits de même nature ;
- être qualifié professionnellement et avoir suivi avec succès un stage de formation théorique et pratique de deux ans ;
- avoir effectué une déclaration préalable d'activité auprès du Conseil des ventes volontaires de meubles aux enchères publiques (Conseil des ventes volontaires) (cf. infra « 5°. a. Déclaration préalable d'activité »).

*Pour aller plus loin* : articles L. 321-4 et R. 321-18 du Code de commerce.

**À noter**

Peuvent réaliser, sous réserve d'avoir suivi une formation, les ventes volontaires les notaires et huissiers de justice dans les communes où il n'y a pas d'office de commissaire-priseur judiciaire. Ils exercent alors leur activité à titre accessoire. (cf. article L. 321-2 du Code de commerce).

#### Formation

Pour être reconnu comme étant qualifié professionnellement, le professionnel doit avoir effectué un stage de formation d'une durée de deux ans et être titulaire soit :

- d'un diplôme national de licence en droit et d'un diplôme national de licence dans l'un des domaines suivants :
  - histoire de l'art,
  - arts appliqués,
  - archéologie,
  - arts plastiques ;
- d'un titre ou diplôme reconnu comme équivalent aux diplômes ci-dessus, à savoir :
  - tout diplôme national sanctionnant un niveau de formation correspondant au moins à trois années d'études après le baccalauréat dans les disciplines juridiques, économiques, commerciales ou de gestion ;
  - tout diplôme conférant le grade de licence ou le grade de master, sanctionnant des études dans les disciplines juridiques, économiques, commerciales ou de gestion ;
  - tout diplôme visé par le ministre chargé de l'enseignement supérieur, sanctionnant un niveau de formation correspondant au moins à trois années d'études après le baccalauréat, dans les disciplines juridiques, économiques, commerciales ou de gestion ;
  - tout diplôme sanctionnant un niveau de formation correspondant au moins à trois années d'études après le baccalauréat dans les disciplines juridiques, économiques, commerciales ou de gestion délivrés par la faculté libre autonome et cogérée d'économie et de droit de Paris jusqu'en 2018 inclus ;
  - tout diplôme national sanctionnant un niveau de formation correspondant au moins à trois années d'études après le baccalauréat en histoire de l'art, en arts appliqués, en archéologie ou en arts plastiques ;
  - tout diplôme conférant le grade de licence ou le grade de master, sanctionnant des études en histoire de l'art, en arts appliqués, en archéologie ou en arts plastiques ;
  - le diplôme de premier cycle de l'École du Louvre ;
  - le diplôme d'archiviste paléographe délivré par l'École nationale des chartes ;
  - le diplôme de bi-licence droit-histoire de l'art et archéologie de l'université Paris-I ;
  - le diplôme de licence bi-disciplinaire droit-histoire de l'art de l'université Lyon-II ;
  - le diplôme de licence droit-histoire de l'art de l'université de Brest ;
  - le diplôme de « spécialiste conseil en biens et services culturels » de l'Institut d'études supérieures des arts (IESA), délivré jusqu'en 2018 inclus. 

**À noter**

L'exercice de certaines professions permet également d'être dispensé du diplôme national en droit (articles R. 321-18 et R. 321-21 du Code de commerce)

*Pour aller plus loin* : articles R. 321-18, A. 321-3 et A. 321-4 du Code de commerce.

##### Demande d'accès au stage et formation

Le futur professionnel titulaire de l'un des diplômes susvisés doit effectuer un stage de formation accessible après un examen d'accès.

L'examen d'accès au stage a lieu au moins une fois par an et comporte des épreuves écrites et orales portant sur des matières artistiques, juridiques, économiques et comptables ainsi que sur l'anglais et, en option, sur une autre langue vivante étrangère.

##### Autorité compétente

La demande doit être adressée au Conseil des ventes volontaires par lettre recommandée avec avis de réception ou par tout autre moyen équivalent au plus tard un mois avant la date de la première épreuve.

##### Pièces justificatives

Sa demande doit contenir :

- sa candidature à l'examen d'accès au stage ;
- un justificatif d'identité et de nationalité ;
- une copie de ses diplômes ou titres de formation ou la justification de leur dispense ;
- la langue étrangère choisie pour l'épreuve d'admission au stage.

##### Délai et issue de la procédure

La liste des candidats admis à subir les épreuves de l'examen d'accès au stage est arrêtée par le Conseil des ventes volontaires trois semaines avant la date de la première épreuve.

Le Conseil des ventes volontaires adresse une attestation de réussite à l'examen d'accès au stage au candidat ayant obtenu la moyenne aux épreuves d'admission.

*Pour aller plus loin* : articles A. 321-10 à A. 321-20 du Code de commerce.

##### Stage de formation

D'une durée de deux ans, dont un an minimum en France, le stage de formation se compose :

- d'un enseignement théorique dans le but d'approfondir les connaissances du candidat en matière artistique, technique, économique, comptable et juridique ;
- d'un enseignement pratique et de travaux de pratique professionnelle.

À l'issue du stage, un certificat de bon accomplissement du stage est remis au stagiaire par le Conseil des ventes volontaires et lui permet de diriger des ventes volontaires de meubles aux enchères publiques.

**À noter**

Des dispenses de stage de formation sont possibles pour certains professionnels justifiant d'une pratique professionnelle d'au moins sept ans dans un ou plusieurs offices de commissaire-priseur judiciaire, auprès d'un ou plusieurs opérateurs de ventes volontaires aux enchères publiques ou courtiers de marchandises assermentés. Ces dispenses ne sont possibles que si ces professionnels ont subi avec succès un examen d'aptitude devant un jury composé notamment :

- d'un professeur d'histoire de l'art ;
- d'un conservateur de patrimoine ;
- d'un commissaire-priseur judiciaire ;
- d'un courtier de marchandises assermenté ;
- de deux personnes habilitées à diriger des ventes volontaires de meubles aux enchères publiques.

*Pour aller plus loin* : article R. 321-19 et articles R. 321-26 à R. 321-31 du Code de commerce.

##### Coûts associés à la qualification

La formation menant à l'obtention du titre de commissaire-priseur de ventes volontaires est payante et son coût varie selon le cursus professionnel envisagé. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'UE ou de l'EEE qui est légalement établi et exerce l'activité de commissaire-priseur de ventes volontaires dans cet État peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il doit pour cela effectuer une déclaration préalable auprès du Conseil des ventes volontaires (cf. infra « 5°. b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque ni l'activité ni la formation ne sont réglementées dans l’État membre dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité dans un ou plusieurs États membres de l'UE pendant au moins un an au cours des dix dernières précédant la prestation.

*Pour aller plus loin* : articles L. 321-24 et suivants du Code de commerce.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Pour exercer l'activité de commissaire-priseur de ventes volontaires en France à titre permanent, le professionnel doit :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétences ou d’un titre de formation requis pour l’exercice de l’activité de commissaire-priseur dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’un titre de formation qui atteste sa préparation à l’exercice de l’activité de commissaire-priseur lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- disposer d'une attestation de compétence ou d'un titre de formation qui certifie la préparation à l'exercice de l'activité de commissaire-priseur de ventes volontaires et justifier en outre, dans un État membre ou un État partie à l'accord sur l'Espace économique européen qui ne réglemente pas l'accès à cette profession ou son exercice, d'un exercice à plein temps de la profession pendant une année au moins au cours des dix années précédentes ou pendant une période équivalente en cas d'exercice à temps partiel, sous réserve que cet exercice soit attesté par l'autorité compétente de cet État ;
- effectuer une demande de reconnaissance de qualification (cf. infra « 5°. c. Demande de reconnaissance de qualification pour le ressortissant en vue d'un exercice permanent (LE) »).

Lorsque la formation reçue par le professionnel porte sur des matières substantiellement différentes de celles requises pour l'exercice de la profession en France, le Conseil des ventes volontaires peut exiger qu'il se soumette à une mesure de compensation (cf. infra « 5°. c Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles R. 321-65 et suivants du Code de commerce.

## 3°. Règles professionnelles et sanctions pénales

#### Règles professionnelles

Le commissaire-priseur de ventes volontaires doit respecter les règles professionnelles suivantes :

- la probité ;
- les bonnes mœurs ;
- l'honneur.

Tout manquement ou agissement contraire à ces règles pourra faire l'objet d'une condamnation ou d'une interdiction d'exercice.

*Pour aller plus loin* : article L. 321-4 du Code de commerce.

#### Sanctions pénales

Est puni de deux ans d'emprisonnement et de 375 000 euros d'amende le fait pour un professionnel de procéder ou faire procéder à une vente volontaire :

- sans avoir effectué la déclaration préalable d'activité (cf. infra « 5°. a. Déclaration préalable d'activité ») ;
- sans avoir effectué une demande de reconnaissance de qualification, lorsque le professionnel est un ressortissant de l'UE (cf. infra « 5° c. Demande de reconnaissance de qualification pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) ») ;
- alors qu'il ne dispose pas des qualifications requises pour organiser ou réaliser des ventes volontaires aux enchères publique ou fait l'objet d'une interdiction d'exercer une telle vente.

Le professionnel encourt également des peines complémentaires notamment :

- l'interdiction d'exercer l'activité pour une durée de cinq ans ou plus ;
- l'affichage ou la diffusion de la condamnation prononcée ;
- la confiscation des sommes ou objets irrégulièrement reçus par le professionnel.

*Pour aller plus loin* : article L. 321-15 du Code de commerce.

## 4°. Mesures de publicité et assurances

### Mesure de publicité

#### Autorité compétente

Avant chaque vente volontaire de meubles aux enchères publiques, le professionnel doit procéder à des mesures de publicité auprès du Conseil des ventes volontaires huit jours avant la vente.

#### Pièces justificatives

Le professionnel doit mentionner toutes les informations relatives à l'organisation de la vente, ses moyens techniques et financiers ainsi que les informations suivantes :

- la date, le lieu, ainsi que l'identité du professionnel qui procédera à la vente, ainsi que la date de sa déclaration auprès du Conseil des ventes volontaires ;
- la qualité de commerçant ou artisan du vendeur lorsque celui-ci met en vente des produits neufs ;
- le caractère neuf du bien ;
- le cas échéant, la qualité de propriétaire du bien mis en vente lors le propriétaire est un opérateur de ventes volontaires organisateur ou son salarié, dirigeant ou associé, ou un expert intervenant dans la vente ;
- l'intervention d'un expert dans l'organisation de la vente ;
- la mention du délai de prescription des actions en responsabilité civile engagées au cours d'une vente volontaire, à savoir un délai de cinq ans à compter de l'adjudication ou de la prisée.

**À noter**

Le professionnel doit tenir chaque jour, sous forme électronique, un registre des ventes ainsi qu'un répertoire contenant les procès-verbaux des ventes réalisées.

*Pour aller plus loin* : articles L. 321-7 et R. 321-32 à R. 321-35 du Code de commerce.

### Assurances

Le professionnel exerçant l'activité de commissaire-priseur de ventes volontaires doit :

- ouvrir un compte dans un établissement de crédit en vue de recevoir les fonds détenus pour le compte d'autrui au cours de son activité ;
- souscrire une assurance de responsabilité professionnelle ;
- souscrire une assurance ou un cautionnement garantissant les fonds reçus au cours de son activité. Ces garanties ne peuvent être consenties que par un établissement de crédit, une société de financement habilitée, une société d'assurance ou une société de caution mutuelle.

**À noter**

Les ressortissants de l'UE ou de l'EEE doivent respecter ces obligations en vue d'un exercice permanent (LE) ou temporaire et occasionnel (LPS).

*Pour aller plus loin* : articles L. 321-6, R. 321-10 à R. 321-17 et R. 321-56 du Code de commerce.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable d'activité

#### Autorité compétente

Le professionnel doit adresser une déclaration par lettre recommandée avec avis de réception ou par voie dématérialisée auprès du Conseil des ventes volontaires de meubles aux enchères publiques.

#### Pièces justificatives

Sa déclaration doit comporter les éléments suivants :

- un justificatif d'identité ;
- une attestation certifiant qu'il ne fait l'objet d'aucune condamnation pénale, sanction disciplinaire ou interdiction d'exercice ;
- un document justifiant que les personnes chargées de diriger les ventes sont qualifiées professionnellement ;
- une copie du bail ou du titre de propriété du local où s'exerce l'activité ainsi que le dernier bilan ou le bilan prévisionnel ;
- un document justifiant de l'ouverture dans un établissement de crédit d'un compte destiné à recevoir les fonds détenus pour le compte d'autrui ;
- un document justifiant que le professionnel a souscrit une assurance de responsabilité professionnelle ;
- un document justifiant que le professionnel a souscrit une assurance ou un cautionnement garantissant la représentation des fonds détenus pour le compte d'autrui.

*Pour aller plus loin* : articles R. 321-1 à R. 321-4 du Code de commerce.

### b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le professionnel doit adresser, par lettre recommandée avec avis de réception ou tout autre moyen écrit, une demande de déclaration préalable au Conseil des ventes volontaires.

#### Pièces justificatives

La demande doit comporter les éléments suivants en français ou, le cas échéant, assortis d'une traduction :

- un justificatif d'identité du demandeur ;
- tout document justifiant de la légalité de l'exercice du demandeur, de l'activité de ventes volontaires de meubles aux enchères publiques dans l’État membre, de sa qualité de professionnel et le cas échéant, du nom de l'organisme professionnel dont il relève ;
- si ni la formation ni l'activité ne sont réglementées dans l’État membre, la preuve par tout moyen qu'il a exercé pendant au moins un an au cours des dix dernières années l'activité de commissaire-priseur de ventes volontaires ;
- une attestation de moins de trois mois certifiant qu'il n'encourt aucune interdiction même temporaire d'exercer son activité ;
- l'indication de la date et du lieu de réalisation de la vente prévue ainsi que l'identité et la qualification de la personne qui en sera chargée ;
- une preuve datant de moins de trois mois que le professionnel a souscrit une assurance de responsabilité professionnelle ou un cautionnement garantissant la représentation des fonds détenus pour le compte d'autrui.

#### Délai

La déclaration préalable doit être adressée au moins un mois avant la première vente. Le Conseil des ventes volontaires informe le cas échéant, le demandeur dans un délai de quinze jours suivants la réception de la demande de tout document manquant.

**À noter**

Cette déclaration doit être renouvelée une fois par an si le professionnel envisage d'exercer à titre occasionnel au cours de l'année ou en cas de changement relatif à sa situation professionnelle.

*Pour aller plus loin* : articles L. 321-24 et suivants, et articles R. 321-56 et suivants du Code de commerce.

### c. Demande de reconnaissance de qualification pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le professionnel qui souhaite faire reconnaître sa qualification professionnelle en vue d'exercer l'activité de commissaire-priseur en France à titre permanent doit adresser une demande de reconnaissance de qualification par lettre recommandée avec avis de réception ou par tout autre moyen au Conseil des ventes volontaires.

#### Pièces justificatives

La demande doit être accompagnée des documents suivants, le cas échéant traduits en français :

- un justificatif d'identité, de nationalité et de domicile du demandeur ;
- une copie de l'attestation de compétences ou du titre de formation du demandeur lui donnant accès à l'activité de commissaire-priseur de ventes volontaires ;
- si le ressortissant est titulaire d'un diplôme ou titre de formation délivré par un pays tiers et reconnu par un pays membre de l'UE ou l'EEE, une attestation délivrée par l'autorité compétente de cet État membre certifiant la durée de l'exercice professionnel du demandeur ainsi que les dates d'exercice ;
- un justificatif d'exercice de l'activité de commissaire-priseur de ventes volontaires au cours des dix dernières années dès lors que ni la formation ni l'activité ne sont réglementées dans l’État membre ;
- une preuve de sa qualification professionnelle ;
- un document justifiant qu'il ne fait l'objet d'aucune condamnation pénale, sanction disciplinaire ou interdiction d'exercice de l'activité de commissaire-priseur de ventes volontaires.

#### Délai et voies de recours

Le Conseil accuse réception de la demande dans un délai d'un mois et informe le demandeur en cas de pièce manquante. La décision du conseil est transmise par lettre recommandée au demandeur dans un délai de trois mois à compter de la réception du dossier complet. La décision du Conseil est susceptible de recours adressé par lettre recommandée avec avis de réception au greffe de la cour d'appel de Paris.

*Pour aller plus loin* : articles A. 321-66 et A. 321-27 du Code de commerce.

#### Bon à savoir : mesures de compensation

Le Conseil des ventes volontaires peut exiger que le professionnel se soumette, au choix, à une épreuve d'aptitude devant le jury professionnel chargé de l'examen d'accès au stage de formation, ou un stage d'adaptation d'une durée maximale de trois ans.

*Pour aller plus loin* : article R. 321-67 du Code de commerce.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque Étatc membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).