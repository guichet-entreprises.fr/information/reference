﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP166" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Mandataire exclusif en opérations de banque et en services de paiement" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="mandataire-exclusif-en-operations" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/mandataire-exclusif-en-operations-de-banque-et-en-services-paiement.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="mandataire-exclusif-en-operations-de-banque-et-en-services-paiement" -->
<!-- var(translation)="None" -->

# Mandataire exclusif en opérations de banque et en services de paiement

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l'activité

Le mandataire exclusif en opérations de banque et en services de paiement (« MEOBSP ») est un intermédiaire dont l'activité consiste à présenter, proposer ou aider à la conclusion d'opérations de banque ou de services de paiement. Il a pour mission d'effectuer les travaux et de donner des conseils préparatoires aidant à la réalisation de ces opérations et de ces services.

Le MEOBSP exerce son activité en vertu d'un mandat délivré par un seul établissement financier tel qu'un établissement de crédit, une société de financement ou encore un établissement de paiement pour une catégorie déterminé d’opérations de banque ou de services de paiement.

Dans le cadre de ses fonctions, le MEOBSP peut également être amené à fournir des recommandations personnalisées sur des opérations relatives à des contrats de crédit immobilier tels que précisés par l'article L. 313-1 du Code monétaire et financier.

*Pour aller plus loin* : articles L. 519-1 à L. 519-2 du Code monétaire et financier.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne qui souhaite exercer la profession de MEOBSP doit justifier d'une capacité professionnelle en opérations de banque et service de paiement dont le niveau est déterminé par le Registre unique des intermédiaires en assurance, banque et finance (Orias).

Ainsi, le professionnel qui souhaite exercer l'activité de MEOBSP à titre principal, devra justifier d'un niveau II-IOBSP.

Lorsque l'activité de MEOBSP n'est exercée qu'à titre accessoire, un niveau III-IOBSP satisfera aux conditions.

Enfin s'il est amené à proposer des contrats de crédit immobilier, l'intéressé devra avoir suivi une formation de niveau III-CI.

Pour en savoir plus sur les niveaux de capacité professionnelle en assurance, il est conseillé de se reporter sur le [site de l'Orias](https://www.orias.fr/).

#### Formation

Le niveau II-IOBSP est atteint lorsque l'intéressé justifie :

- soit d'un diplôme sanctionnant des études supérieures d'un niveau de formation III selon la Nomenclature des niveaux de formation dans l'un des domaines suivants : Économie, Droit et sciences politiques, Finance, Banque, Assurance et Immobilier, Comptabilité et gestion ;
- soit d'un diplôme délivré par l'une des écoles de commerce et de gestion inscrite sur une [liste déterminée par le ministre chargé de l'enseignement supérieur](https://www.orias.fr/documents/13705/16521/BO%20Special%20du%2012%20mars%2020015_Master.pdf) ;
- soit d'une année d'expérience acquise en tant que cadre, dans des fonctions liées à la réalisation d'opérations de banque ou de services de paiement, au cours des trois années précédant l'immatriculation à l'Orias ;
- soit de deux années d'expérience acquises dans des fonctions liées à la réalisation d'opérations de banque ou de services de paiement, au cours des cinq années précédant l'immatriculation à l'Orias ;
- d'un stage professionnel de 80 heures effectué :
  - soit auprès d'un établissement de crédit, d'un établissement de paiement, d'une entreprise d'assurance, d'une société de financement ou d'un établissement de monnaie électronique fournissant des services de paiement,
  - soit auprès d'un organisme de formation choisi par l'intéressé, son employeur ou son mandant.

**À noter**

A l’issue de ce stage, un contrôle des compétences sous forme de questionnaire à choix multiple ou de questions courtes sera soumis à l’intéressé. L’examen sera réussi si la personne a juste à plus de 70% des questions.

Si, toutefois, l'intéressé ne souhaitait exercer l'activité de MEOBSP qu'à titre accessoire, il devrait justifier :

- soit d'un diplôme sanctionnant des études supérieures d'un niveau de formation III dans l'un de ces domaines : Économie, Droit et sciences politiques, Finance, Banque, Assurance et Immobilier, Comptabilité et gestion ;
- soit d'un diplôme délivré par l'une des écoles de commerce et de gestion inscrite sur la même liste du ministre chargé de l'enseignement supérieur visée ci-dessus ;
- soit de six mois d'expérience professionnelle acquis dans des fonctions liées à la réalisation d'opérations de banque ou de services de paiement, au cours des deux années précédant l'immatriculation à l'Orias ;
- d'un stage d'une durée suffisante effectué :
  - soit auprès d'un établissement de crédit, d'un établissement de paiement, d'une entreprise d'assurance, d'une société de financement ou d'un établissement de monnaie électronique fournissant des services de paiement,
  - soit auprès d'un organisme de formation choisi par l'intéressé, son employeur ou son mandant.

Dans le cas où l’intéressé propose des contrats de crédits immobilier, il devra être en mesure de justifier le suivi d'une formation de niveau III-CI :

- soit d'un diplôme sanctionnant des études supérieures d'un niveau de formation III dans l'une de ces domaines : Économie, Droit et sciences politiques, Finance, Banque, Assurance et Immobilier, Comptabilité et gestion ;
- soit d'un diplôme délivré par l'une des écoles de commerce et de gestion inscrite sur la même liste du ministre chargé de l'enseignement supérieur visée ci-dessus ;
- soit d'une année d'expérience acquise en tant que cadre, dans des fonctions liées à la réalisation d'activités d'élaboration, de proposition ou d'octroi des contrats de crédit immobilier, au cours des trois années précédant l'immatriculation à l'Orias ;
- soit de trois années d'expérience acquises dans des fonctions liées à la réalisation d'activités d'élaboration, de proposition ou d'octroi des contrats de crédit immobilier, au cours des dix années précédant l'immatriculation à l'Orias ;
- soit d'un stage professionnel d'une durée minimale de 40 heures dont le programme est prévu à l'annexe de l'arrêté du 9 juin 2016. À l'issue de ce stage, un contrôle des compétences sous forme de questionnaire à choix multiples ou de questions courtes sera soumis à l'intéressé. L'examen sera réussi si la personne a répondu juste à plus de 70 % des questions.

**À savoir**

La formation ou l'expérience professionnelle acquise dans un État de l'UE ou de l'EEE doit être complétée par un stage d'adaptation de trois mois, effectué auprès d'un intermédiaire en opération de banque et en services de paiement, d'un établissement de crédit ou d'une société de financement en France. Au cours du stage, une formation professionnelle devra être suivie d'une durée de 28 heures.

*Pour aller plus loin* : articles R. 519-9 et R. 519-10 du Code monétaire et financier.

#### Coût associés à la qualification

La formation pour obtenir l'un des diplômes menant à la profession de MEOBSP est payante. Pour plus d'informations, il est conseillé de se rapprocher des établissements le délivrant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Tout ressortissant d'un État membre de l'Union Européenne (UE) ou partie à l'accord de l'Espace Économique Européen (EEE), établi et exerçant légalement les activités de MEOBSP dans un de ces États, peut exercer la même activité à titre temporaire ou occasionnel en France.

Cependant, cette faculté n'est ouverte qu'aux ressortissants qui proposent des contrats de crédits immobiliers dans le cadre de leur activité de MEOBSP.

L'intéressé devra simplement en informer au préalable le Registre de son État qui s'assurera de transmettre lui-même les informations au Registre français (cf. infra « 5°. a. Informer le Registre unique de l'État de l'UE ou de l'EEE »). Ce dernier procédera ensuite à son enregistrement en France.

**À noter**

Outre les démarches visant à communiquer ses informations au Registre unique français par le Registre de son État d'origine, le ressortissant devra également suivre une formation de 14 heures menant à l'obtention d'un module spécialisé relatif au crédit immobilier.

*Pour aller plus loin* : article L. 519-9, R. 519-4 et R. 519-11-2 du Code monétaire et financier ; arrêté du 9 juin 2016.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, établi et exerçant légalement les activités de MEOBSP dans un de ces États, peut exercer la même activité à titre permanent en France.

Cependant, cette faculté n'est ouverte qu'aux ressortissants qui proposent des contrats de crédits immobiliers dans le cadre de leur activité de MEOBSP.

L'intéressé devra simplement en informer au préalable le Registre de son État qui s'assurera de transmettre lui-même les informations au Registre français (cf. infra « 5°. a. Informer le Registre unique de l'État de l'UE ou de l'EEE »). Ce dernier procédera ensuite à son enregistrement en France.

**À noter**

Outre les démarches visant à communiquer ses informations au Registre unique français par le Registre de son État d'origine, le ressortissant devra également suivre une formation de 14 heures menant à l'obtention d'un module spécialisé relatif au crédit immobilier.

*Pour aller plus loin* : articles L. 519-9, R. 519-4 et R. 519-11-2 du Code monétaire et financier ; arrêté du 9 juin 2016.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le MEOBSP doit se comporter avec loyauté et agir au mieux des intérêts de ses clients. Il est également tenu de respecter des conditions d'honorabilité incombant à la profession et notamment de ne pas avoir fait l'objet d'une condamnation depuis moins de dix ans pour crime, à des peines pour blanchiment, corruption ou encore fraude fiscale, ou à la destitution des fonctions d'officier public ou ministérielles.

En outre, le professionnel a une obligation d'information et de conseil envers ses clients ; il devra communiquer de manière claire et exacte toute information relative aux opérations de banque et services de paiement.

Une obligation de loyauté pèse également sur le professionnel dans le cadre du mandat qui le lie à un établissent de crédit, une société de financement ou encore un établissement de paiement.

*Pour aller plus loin* : articles L. 500-1, L. 519-3-3 et R. 519-19 du Code monétaire et financier.

## 4°. Obligation d'immatriculation et assurance

### a. Demander son immatriculation au registre de l'Orias

L'intéressé qui souhaite exercer en France la profession de MEOBSP proposant des contrats de crédits immobiliers doit impérativement être immatriculé au Registre unique des intermédiaires en assurance, banque et finance (Orias).

#### Pièces justificatives

L'immatriculation est précédée par l'[ouverture d'un compte](https://www.orias.fr/espace-professionnel) sur le site de l'Orias et de l'envoi d'un dossier comportant l'ensemble des pièces justificatives suivantes :

- une copie de la pièce d'identité de l'intéressé ou un extrait KBis de moins de trois mois si l'intéressé est immatriculé au registre du commerce et des sociétés ;
- un document justifiant de sa capacité professionnelle telle que précisée au paragraphe « 2°. a. Formation » :
  - soit une copie intégrale du livret de stage,
  - soit une attestation de fonction liées à la réalisation d'activités d'élaboration, de proposition ou d'octroi des contrats de crédit immobilier, lorsque l'intéressé est un ressortissant de l'UE ou de l'EEE,
  - soit une attestation de fonction liées à la réalisation d'opérations de banque ou de services de paiement,
  - soit une attestation du stage d'adaptation de trois mois, complété d'une formation de 28 heures lorsque l'expérience est acquise dans un État de l'UE ou de l'EEE,
  - soit une copie du diplôme, du certificat ou du titre délivrée par un État de l'UE ou de l'EEE et reconnus comme équivalents aux diplômes et titres entrant dans l'une des classifications de la Nomenclature des spécialités de formation 122, 128, 313 ou 314 ;
- tout document attestant de l'existence d'un mandat exclusif avec un établissement de crédit ;
- en cas d'encaissement de fonds, une attestation de garantie financière ;
- le règlement des frais d'inscription ;
- la déclaration des opérations de banque proposées parmi celles énumérées à l’article 1er de l’arrêté de 2016 au 11°.

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé.

#### Renouvellement de l'inscription

La procédure d’immatriculation à l'Orias doit être renouvelée tous les ans et lors de toute modification de la situation professionnelle de l'intéressé. Dans ce dernier cas, le MEOBSP devra tenir informé l'Orias un mois avant la modification ou dans le mois qui suit l’événement modificatif.

La demande de renouvellement doit impérativement intervenir avant le 31 janvier de chaque année et doit comporter :

- le règlement des frais d'inscription ;
- en cas d'encaissement de fonds, une attestation de garantie financière couvrant la période du 1er mars de l’année N au 28 février de l’année N+1.

**À savoir**

Les modèles d'attestations sont directement consultables sur le site de l'[Orias](https://www.orias.fr/banque1).

#### Coût

Les frais d'immatriculation sont fixés à 30 € payables directement en ligne sur le site de l'Orias. En cas de non règlement de ces frais, l'Orias envoie un courrier informant l'intéressé qu'il dispose d'un délai de trente jours à compter de la date d'accusé réception du courrier pour régler la somme.

À défaut du règlement de cette somme :

- la demande de première inscription à l'Orias ne sera pas prise en compte ;
- le MEOBSP sera radié du registre, lorsqu'il s'agissait d'une demande de renouvellement.

#### Exception

Est exempté d'immatriculation à l'Orias, le MEOBSP qui exerce cette activité à titre accessoire et ne dépasse les seuils d'activité suivants par année :

- pour les opérations de banque ou les services de paiement, le nombre total de 30 opérations ;
- 300 000 euros de crédits octroyés ou de services de paiement fournis ou réalisé par le MEOBSP.

*Pour aller plus loin* : articles L. 546-1, L. 546-2 et R. 519-2 du Code monétaire et financier.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

L'intéressé exerçant dans le cadre d'un mandat exclusif, bénéficie de l'assurance de responsabilité civile professionnelle de son mandant couvrant les risques pécuniaires liés à son activité habituelle de MEOBSP.

*Pour aller plus loin* : articles L. 519-3-4 et R. 519-16 du Code monétaire et financier.

### c. Obligation de souscrire une assurance garantie financière

Toute personne exerçant la profession de MEOBSP, dès lors qu'elle encaisse des fonds, même à titre occasionnel, est tenue de souscrire à une assurance garantie financière, affectée au remboursement de ces fonds à ses assurés.

Le montant minimum de la garantie financière doit être au moins égal à 115 000 euros, et ne peut être inférieur au double du montant moyen mensuel des fonds encaissés par le MEOBSP, calculé sur la base des fonds encaissés au cours des douze derniers mois précédant le mois de la date de souscription ou de reconduction de l'engagement de caution.

*Pour aller plus loin* : article R. 519-17 du Code monétaire et financier.

### d. Obligation de suivre une formation professionnelle continue

Les MEOBSP qui exercent des fonctions liées à des contrats de crédit immobilier doivent suivre une formation professionnelle continue visant à actualiser leurs connaissances et leurs compétences, en tenant compte des changements de législation ou de réglementation applicables en la matière.

Cette formation dont la durée minimale est de 7 heures, doit être suivie tous les ans.

### e. Sanctions pénales

Tout intéressé qui ne respecterait pas l'une des obligations visées aux paragraphes 3° et 4° est passible d'une peine d'emprisonnement de deux ans ou d'une amende de 6 000 euros, ou des deux peines cumulées.

Le fait pour l'intéressé de créer une confusion ou de faire croire qu'il est immatriculé au registre de l'Orias dans une catégorie autre est punie d'une peine d'emprisonnement de trois ans et d'une amende de 375 000 euros, ou de l'une des deux peines seulement.

*Pour aller plus loin* : articles L. 546-3 et L. 546-4 du Code monétaire et financier.

## 5°. Démarches et formalités de reconnaissance de qualification

**À savoir**

Les démarches et formalités de reconnaissance de qualification ne s'appliquent qu'au ressortissant de l'UE ou de l'EEE qui propose des contrats de crédits immobiliers dans le cadre de son activité de MEOBSP.

### a. Informer le Registre unique de l'État de l'UE ou de l'EEE

#### Autorité compétente

Le ressortissant d'un État membre de l'UE ou de l'EEE qui exerçait l'activité de MEOBSP dans cet État et qui souhaite exercer en libre prestation de service ou en libre établissement en France, doit au préalable en informer le Registre unique de son État.

#### Procédure

Le ressortissant devra communiquer au Registre unique de son État les informations suivantes :

- son nom, son adresse et, le cas échéant, son numéro d'immatriculation ;
- l’État membre dans lequel il souhaite exercer son activité en cas de libre prestation de service ou s'établir en cas de libre établissement ;
- la catégorie d'intermédiaire de banque à laquelle il appartient, à savoir celle de MEOBSP ;
- les branches concernées s'il y a lieu.

#### Délai

Le Registre unique de l'État de l'UE ou de l'EEE auprès duquel le ressortissant a notifié son intention d'exercer son activité en France, dispose d'un délai d'un mois pour communiquer à l'Orias les informations relatives à l'intéressé.

Le ressortissant pourra ensuite commencer son activité de MEOBSP en France dans un délai d'un mois après que le Registre unique de son État l'ai informé de la communication faite à l'Orias.

*Pour aller plus loin* : articles L. 519-9 du Code monétaire et financier et [32 de la directive 2014/17/UE du 4 février 2014](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014L0017).

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).