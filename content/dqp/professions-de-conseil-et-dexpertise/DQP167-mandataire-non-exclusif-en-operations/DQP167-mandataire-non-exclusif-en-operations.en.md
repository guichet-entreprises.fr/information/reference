﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP167" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Banking transactions and payment services non-exclusive representative" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="banking-transactions-and-payment-services-non-exclusive-representative" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/banking-transactions-and-payment-services-non-exclusive-representative.html" -->
<!-- var(last-update)="2020-04-15 17:21:10" -->
<!-- var(url-name)="banking-transactions-and-payment-services-non-exclusive-representative" -->
<!-- var(translation)="Auto" -->


Banking transactions and payment services non-exclusive representative
===================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:10<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The non-exclusive agent in banking and payment services ("MNEOBSP") is an intermediary whose activity consists of presenting, proposing or assisting in the conclusion of bank transactions or payment services. Its mission is to carry out the work and provide preparatory advice to assist in the implementation of these operations and services.

MNEOBSP operates under one or more mandates issued by one or more financial institutions such as credit institutions, finance companies, payment institutions or currency institutions. providing payment services.

As part of its duties, MNEOBSP may also be required to provide personalized recommendations on transactions relating to real estate credit contracts as specified by the[Article L. 313-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032222205&dateTexte=&categorieLien=cid) Monetary and Financial Code.

*For further information*: Articles L. 519-1 to L. 519-2 of the Monetary and Financial Code (CMF).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Anyone wishing to practice the profession of MNEOBSP must justify a professional capacity in banking operations and payment service whose level is determined by the Single Register of Intermediaries in Insurance, Banking and Finance (Orias ).

Thus, the professional who wishes to carry out the activity of MNEOBSP as principal, will have to justify a level II-IOBSP.

When MNEOBSP's activity is carried out only as an incidental, a level III-IOBSP will meet the conditions.

Finally, if he is required to offer real estate credit contracts, he must have completed a level III-CI training.

To learn more about the levels of professional capacity in banking operations and payment services, it is advisable to visit the[Orias](https://www.orias.fr/documents/10227/28915/2017-01-25%20Liste%20des%20pieces%20a%20joindre%20MOBSP.pdf).

#### Training

The I-IOBSP level is reached when the person is justified:

- or a degree sanctioning higher education of a level of education III according to the nomenclature of training levels in one of the following areas: Economics, Law and Political Science, Finance, Banking, Insurance and Real Estate, Accounting and Management
- or a diploma issued by one of the business and management schools listed on a[list determined by the Minister for Higher Education](https://www.orias.fr/documents/13705/16521/BO%20Special%20du%2012%20mars%2020015_Master.pdf) ;
- i.e. one year of executive experience in banking or payment services in the three years prior to registration at Orias;
- two years of experience in positions related to bank transactions or payment services, in the five years prior to registration at Orias;
- an 80-hour professional internship tailored to the products performed:- either with a credit institution, payment institution, insurance company, finance company or electronic money institution providing payment services,
  - or to a training organization chosen by the individual, his employer or his principal.

If, however, the person concerned wished to carry out the activity of MNEOBSP only as an incidental means, he would have to justify:

- or a degree sanctioning higher education of a level of education III in one of these fields: Economics, Law and Political Science, Finance, Banking, Insurance and Real Estate, Accounting and Management;
- i.e. a diploma issued by one of the business and management schools on the same list of the minister in charge of higher education referred to above;
- six months of professional experience in positions related to banking transactions or payment services, in the two years prior to registration at Orias;
- an internship of sufficient duration:- either with a credit institution, payment institution or insurance company,
  - or to a training organization chosen by the individual, his employer or his principal.

In the event that the individual proposes real estate loan contracts, he must be able to justify the follow-up of a level III-CI training:

- or a degree sanctioning higher education of a level of education III in one of these fields: Economics, Law and Political Science, Finance, Banking, Insurance and Real Estate, Accounting and Management;
- i.e. a diploma issued by one of the business and management schools on the same list of the minister in charge of higher education referred to above;
- or one year of experience as an executive in functions related to the development, proposal or awarding of real estate credit contracts in the three years prior to registration at Orias;
- three years of experience in functions related to the development, proposal or awarding of real estate credit contracts in the ten years prior to registration at Orias;
- or a minimum 40-hour professional internship, the program of which is scheduled at the[annex of the decree of 9 June 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032675834&dateTexte=20171102). At the end of this internship, a competency check in the form of multiple-choice questionnaires or short questions will be submitted to the individual. The exam will be successful if the person has answered just more than 70% of the questions.

**What to know**

Training or work experience in an EU or EEA state must be supplemented by a three-month adjustment course with a bank and payment services intermediary, a credit institution or a credit institution. financing company in France. During the course, vocational training will have to be followed for 28 hours.

*For further information*: Articles R. 519-9 and R. 519-10 of the Monetary and Financial Code.

#### Costs associated with qualification

Training to obtain one of the diplomas leading to the profession of MNEOBSP pays off. For more information, it is advisable to get close to the establishments issuing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

Any national of a Member State of the European Union (EU) or party to the agreement of the European Economic Area (EEA), established and legally carrying out the activities of MNEOBSP in one of these states, may carry out the same activity on a temporary or casual basis in France.

However, this faculty is only open to nationals who offer real estate credit contracts as part of their MNEOBSP activity.

The person concerned will simply have to inform the Register of his State in advance, which will ensure that the information is transmitted to the French Register himself (see infra "5o). a. Inform the Single Register of the EU State or the EEA"). The latter will then proceed with its registration in France, subject to having ensured that the principals for whom the person works are allowed to operate in France.

**Please note**

In addition to the steps to communicate his information to the French Single Register through the Register of his State of origin, the national will also have to undergo a 14-hour training leading to the obtaining of a specialized module relating to credit. Real estate.

*For further information*: Articles L. 519-9, R. 519-4 and R. 519-11-2 of the Monetary and Financial Code;[decree of 9 June 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032675834&categorieLien=id).

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, established and legally operating MNEOBSP in one of these states, may carry out the same activity on a permanent basis in France.

However, this faculty is only open to nationals who offer real estate credit contracts as part of their MNEOBSP activity.

The person concerned will simply have to inform the Register of his State in advance, which will ensure that the information is transmitted to the French Register himself (see infra "5o). a. Inform the Single Register of the EU State or the EEA"). The latter will then proceed with its registration in France, subject to having ensured that the principals for whom the person works are allowed to operate in France.

**Please note**

In addition to the steps to communicate his information to the French Single Register through the Register of his State of origin, the national will also have to undergo a 14-hour training leading to the obtaining of a specialized module relating to credit. Real estate.

*For further information*: Articles L. 519-9, R. 519-4 and R. 519-11-2 of the Monetary and Financial Code;[decree of 9 June 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032675834&categorieLien=id).

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

MNEOBSP must behave with loyalty and act in the best interests of its clients. It is also required to comply with conditions of honour liable to the profession and in particular not to have been convicted for less than ten years for crimes, money laundering, corruption or tax evasion, or removal from the functions of public or ministerial officer.

In addition, the professional has an obligation to inform and advise his clients; it will have to communicate clearly and accurately any information relating to banking transactions and payment services.

There is also an obligation of loyalty to the professional in the context of the mandates that bind him to credit institutions, finance companies or payment institutions.

*For further information*: Articles L. 500-1, L. 519-3-3 and R. 519-19 of the Monetary and Financial Code.

4°. Registration requirement and insurance
--------------------------------------------------------------

### a. Request registration on the Orias register

Those wishing to practise in France in the profession of MNEOBSP offering real estate credit contracts must be registered in the Single Register of Insurance, Banking and Finance Intermediaries (Orias).

**Supporting documents**

The registration is preceded by the[opening an account](https://www.orias.fr/espace-professionnel) Orias website and sending a file containing all the following supporting documents:

- A copy of the person's ID or a KBis extract of less than three months if the person is registered in the register of trade and companies;
- a document justifying his professional capacity as specified in paragraph "2." a. Training":- or a full copy of the internship booklet,
  - either a certificate of functions related to the development, proposal or awarding of real estate credit contracts, where the person concerned is an EU or EEA national,
  - either a certificate of functions related to the conduct of bank transactions or payment services,
  - i.e. a certificate of the three-month adjustment course, supplemented by a 28-hour training course when the experience is acquired in an EU or EEA state,
  - either a copy of the diploma, certificate or title issued by an EU or EEA state and recognised as equivalent to diplomas and titles in one of the classifications of the Specialty Training Nomenclature 122, 128, 313 or 314;
- any document attesting to the existence of one or more mandates with one or more credit institutions;
- In the event of cashing in funds, a certificate of financial guarantee;
- Settlement of registration fees
- proposed bank transactions.

**What to know**

Supporting documents must be written in French or translated by a certified translator, if necessary.

**Renewal of registration**

The registration procedure at the Orias must be renewed every year and with any change in the person's professional situation. In the latter case, the MNEOBSP will have to keep the Orias informed one month before the change or within one month of the amending event.

The renewal application must take place before January 31 of each year and must include:

- Settlement of registration fees
- in the event of cashing in, a financial guarantee certificate covering the period from 1 March of year N to 28 February of year No. 1.

**What to know**

The certificate templates are directly available on the[Orias](https://www.orias.fr/banque1).

**Cost**

The registration fee is set at 30 euros payable directly online on the Orias website. In the event of non-payment of these charges, orias sends a letter informing the person concerned that it has a period of thirty days from the date of receipt of the mail to pay the sum.

Failing to settle this amount:

- The application for a first registration to the Orias will not be taken into account;
- MNEOBSP will be removed from the register when it is an application for renewal.

**Exception**

The MNEOBSP, which engages in this activity as an incidental event and does not exceed the following activity thresholds per year, is exempt from registration at Orias:

- for bank transactions or payment services, the total number of 20 transactions;
- 200,000 euros of credits granted or payment services provided or carried out by the MNEOBSP.

*For further information*: Articles L. 546-1, L. 546-2 and R. 519-2 of the Monetary and Financial Code; March 1, 2012.

### b. Obligation to take out professional liability insurance

The person concerned, who is serving one or more terms, benefits from the professional liability insurance of his or her constituents covering the financial risks associated with his or her usual MNEOBSP activity.

*For further information*: Articles L. 519-3-4 and R. 519-16 of the Monetary and Financial Code.

### c. Obligation to take out financial guarantee insurance

Any person practising the profession of MNEOBSP, as long as he cashes in funds, even on a casual basis, is required to take out financial guarantee insurance, assigned to the repayment of these funds to his policyholders.

The minimum amount of the financial guarantee must be at least 115,000 euros, and cannot be less than double the monthly average amount of funds collected by the MNEOBSP, calculated on the basis of funds collected in the last twelve months preceding the month date of subscription or renewal of the bond commitment.

*For further information*: Article R. 519-17 of the Monetary and Financial Code.

### d. Obligation to undergo continuing vocational training

MNEOBSPs performing functions related to real estate credit contracts must undergo ongoing vocational training to update their knowledge and skills, taking into account changes in legislation or regulations.

This training, which has a minimum duration of 7 hours, must be completed every year.

### e. Criminal sanctions

Any person interested in complying with one of the obligations under paragraphs 3 and 4 is liable to a two-year prison sentence or a fine of 6,000 euros, or two cumulative penalties.

The fact that the person concerned creates confusion or makes it appear that he is registered on the Orias register in another category is punishable by a three-year prison sentence and a fine of 375,000 euros, or one of only two sentences.

*For further information*: Articles L. 546-3 and L. 546-4 of the Monetary and Financial Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

**What to know**

Qualification recognition procedures and formalities apply only to EU or EEA nationals who offer real estate credit contracts as part of their MNEOBSP business.

### a. Inform the Single Register of the EU State or the EEA

**Competent authority**

A national of an EU or EEA Member State who was active in that state and who wishes to practice self-service or self-establishment in France must first inform the single register of his or her state.

**Procedure**

The national will have to provide the following information to the Single Register of his or her state:

- His name, address and, if applicable, registration number;
- The Member State in which it wishes to carry out its activity in the event of a free service or to establish itself in the event of a Freedom of establishment;
- The category of bank intermediary to which he belongs, namely that of MNEOBSP;
- relevant branches, if necessary.

**Timeframe**

The Single Register of the EU State or EEA, to which the national has notified his intention to operate in France, has one month to provide the Orias with the information relating to him.

The national will then be able to start his MNEOBSP activity in France within one month after the Single Register of his State informed him of the communication made to the Orias.

*For further information*: Articles L. 519-9 of the Monetary and Financial Code and[32 of the 2014/17/EU Directive of 4 February 2014](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014L0017).

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

