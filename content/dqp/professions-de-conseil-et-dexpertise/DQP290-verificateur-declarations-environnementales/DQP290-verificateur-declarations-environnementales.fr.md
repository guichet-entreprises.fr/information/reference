﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP290" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Vérificateur de déclarations environnementales (tierce partie indépendante)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="verificateur-declarations-environnementales" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/verificateur-declarations-environnementales.html" -->
<!-- var(last-update)="2022-06" -->
<!-- var(url-name)="verificateur-declarations-environnementales" -->
<!-- var(translation)="None" -->

# Vérificateur de déclarations environnementales (tierce partie indépendante)

Dernière mise à jour : <!-- begin-var(last-update) -->2022-06<!-- end-var -->

## Définition de la profession

Une déclaration environnementale (fournie par un fabricant, plusieurs fabricants ou une organisation professionnelle) comporte les informations définies à l’article R. 171-18 du Code de la construction et de l’habitation et doit être vérifiée par une tierce partie indépendante (vérificateur de déclarations environnementales) pour être utilisées pour le calcul de la performance environnementale des bâtiments au sens de l’article L. 171-1 du code précité.

Une tierce partie indépendante vérifie le contenu d’une déclaration environnementale (pour les produits de construction et de décoration et les équipements électriques, électroniques et de génie climatique) et formalise la conformité de la déclaration environnementale par la délivrance d’une attestation de vérification.

Seules les tierces parties indépendantes (vérificateurs de déclarations environnementales) peuvent attester de la conformité d’une déclaration environnementale notamment pour leur utilisation dans le calcul de la performance environnementale des bâtiments au sens de l’article L. 171-1.     

## Qualifications professionnelles requises en France

Pour l'exercice de sa mission de vérification, la tierce partie indépendante doit disposer d'une attestation de reconnaissance d'aptitude individuelle délivrée à des personnes physiques par une personne morale chargée de l'application d'un programme de déclarations environnementales tel que défini à l'article R. 171-15 du Code de la construction et de l’habitation. Cette personne y atteste que la tierce partie indépendante dispose, pour l'exercice de sa mission, des garanties d'indépendance et d'impartialité requises ainsi que des connaissances et des compétences suivantes :

- au moins deux années d'expérience dans le domaine de l'analyse du cycle de vie des produits de construction ou de décoration, ou des équipements et des déclarations environnementales ;
- une connaissance générale sur les techniques de construction d'un bâtiment ainsi que sur l'évaluation des performances des composants le constituant ;
- une connaissance précise d'un ou plusieurs secteurs d'activité suivants :
  - les produits de construction et de décoration,
  - les équipements électriques, électroniques et de génie climatique ;
- une connaissance des aspects environnementaux liés aux produits de construction ou de décoration ou aux équipements ;
- une connaissance du cadre réglementaire portant sur les déclarations environnementales des produits de construction, de décoration et des équipements ;
- une connaissance des exigences, des lignes directrices, des principes et modes opératoires méthodologiques applicables dans le domaine des déclarations environnementales des produits de construction ou de décoration et des équipements destinés à un usage dans les ouvrages de bâtiment.

L'attestation de reconnaissance d'aptitude de la tierce partie indépendante est valable trois ans et est renouvelée par la personne morale chargée de l'application d'un programme de déclarations environnementales, à condition que cette personne morale se soit assurée que les exigences suivantes aient été respectées par la tierce partie indépendante :

- avoir mis à jour ses connaissances (précitées), au cours des 3 dernières années ;
- les contrôles des vérifications effectuées par la tierce partie indépendante n'ont pas fait apparaître d'erreurs ou de manquements notables répétés dans l'évaluation technique des vérifications réalisées ainsi que dans l'application des principes d'impartialité et d'indépendance.

## Particularités de la réglementation de la profession

### Déontologie

La conformité d’une déclaration environnementale est sanctionnée par une attestation de vérification. Cette attestation comprend donc les coordonnées de la tierce partie indépendante (vérificateur de déclarations environnementales) ainsi qu'une déclaration sur l'honneur établissant :

- son indépendance et l'absence de tout lien de nature à nuire à son impartialité vis-à-vis du déclarant, notamment n'être employé ni à temps plein ni à temps partiel par le déclarant ;
- sa déclaration des liens d'intérêts, au cours des trois dernières années, avec le déclarant notamment économiques dans un format établi par la personne morale chargée de l'application d'un programme de déclarations environnementales ;
- sa non-participation au processus d'élaboration de la déclaration environnementale, objet de la vérification.

### Sanctions

En cas d'erreurs ou de manquements notables et répétés de la part d'une tierce partie indépendante, identifiés notamment lors des contrôles complémentaires mentionnés à l'article R. 171-19, réalisés par une personne morale chargée de l'application d'un programme de déclarations environnementales, cette dernière peut, après avoir recueilli les observations de la tierce partie indépendante, suspendre ou retirer son attestation de reconnaissance d'aptitude.

### Obligation de formation professionnelle continue

Une tierce partie indépendante (vérificateur de déclarations environnementales) doit participer à au moins deux réunions annuelles, organisées par la personne morale chargée de l’application d’un programme de déclarations environnementales, sur la durée de validité de sa reconnaissance d’aptitude (trois ans).

## Démarches de reconnaissance de qualification professionnelle

### Libre Établissement (exercice stable et continu)

Une tierce partie indépendante ressortissante d'un État membre de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen qualifiée dans son État membre d'origine pour l'activité de vérification en tant que tierce partie indépendante peut s'établir en France. La tierce partie indépendante européenne s'enregistre auprès de la personne morale chargée de l'application d'un programme de déclarations environnementales.

#### Autorité compétente

Les autorités compétentes sont :

- le programme de déclarations environnementales pour les produits de construction et de décoration ([programme INIES](https://www.inies.fr/programmes-et-services/le-programme-de-verification-inies/), propriété de l’Alliance HQE GBC) :
  - inies@hqegbc.org
  - programmeinies@afnor.org
- le programme de déclarations environnementales pour les équipements électriques, électroniques et de génie climatique ([programme PEPecopassport](http://www.pep-ecopassport.org/fr/), propriété de l’Association PEP) :
  - contact@pep-ecopassport.org

#### Procédure

Le professionnel ressortissant d’un autre État membre doit s’adresser à une personne morale chargée de l’application d’un programme de déclarations environnementales pour déclarer son intention d’exercer en tant que tierce partie indépendante (vérificateur de déclarations environnementales). Après avoir reçu toutes les pièces justificatives, la personne morale chargée de l’application d’un programme de déclarations environnementales autorise l’exercice de la mission en tant que tierce partie indépendante (vérificateur de déclarations environnementales).

#### Pièces justificatives

Les pièces justificatives sont :

- preuve de nationalité ;
- preuve de qualification (par exemple : bulletin de salaire, curriculum vitae, diplôme, etc).

#### Coût

Les coûts fixés par les personnes morales chargées de l’application d’un programme de déclarations environnementales pour l’obtention d’une reconnaissance d’aptitude sont :

- pour le programme INIES : 1196€ (hors taxe) (ce coût couvre la durée de validité de la reconnaissance d’aptitude soit 3 ans) ;
- pour le programme PEPecopassport : 500€ (toutes taxes comprises).

Le coût fixé par la personne morale chargée de l’application du programme INIES pour un renouvellement (au bout de 3 ans) est de 1000€ (hors taxes).	

La personne morale chargée de l’application du programme PEPecopassport exige une cotisation annuelle pour chaque tierce partie indépendante de 1000€ (toutes taxes comprises).

### Libre Prestation de Services (exercice temporaire et occasionnel)

Une tierce partie indépendante ressortissante d'un État membre de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen souhaitant exercer l'activité de vérification en tant que tierce partie indépendante peut exercer en France, à titre temporaire et occasionnel, sous réserve d'être légalement établie dans un de ces États, pour y exercer la même activité. Lorsque cette activité ou la formation y conduisant ne sont pas réglementées dans l'État d'établissement, elle doit l'avoir exercée dans un ou plusieurs États membres de l'Union européenne ou États parties à l'accord sur l'Espace économique européen pendant au moins une année à temps plein ou pendant une durée équivalente à temps partiel au cours des dix années qui précèdent la prestation qu'elle entend réaliser en France. La tierce partie indépendante adresse à la personne morale chargée de l'application d'un programme de déclarations environnementales une déclaration préalable comprenant une attestation certifiant que le détenteur est légalement établi dans un État membre pour y exercer l'activité en question et une preuve de ses qualifications professionnelles.

#### Autorité compétente

Les autorités compétentes sont :

- le programme de déclarations environnementales pour les produits de construction et de décoration ([programme INIES](https://www.inies.fr/programmes-et-services/le-programme-de-verification-inies/), propriété de l’Alliance HQE GBC) :
  - inies@hqegbc.org
  - programmeinies@afnor.org 
- le programme de déclarations environnementales pour les équipements électriques, électroniques et de génie climatique ([programme PEPecopassport](http://www.pep-ecopassport.org/fr/), propriété de l’Association PEP) :
  - contact@pep-ecopassport.org

#### Procédure

La tierce partie indépendante adresse à la personne morale chargée de l'application d'un programme de déclarations environnementales une déclaration préalable comprenant une attestation certifiant que le détenteur est légalement établi dans un État membre pour y exercer l'activité en question et une preuve de ses qualifications professionnelles.

#### Pièces justificatives

Les pièces justificatives à délivrer à la personne morale chargée de l’application d’un programme de déclarations environnementales sont :

- preuve de nationalité ;
- attestation d’établissement légal ;
- preuve de qualification (par exemple : bulletin de salaire, curriculum vitae, diplôme, etc).

## Service d'assistance

### Centre d’information français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

#### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

#### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

#### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

#### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

#### Coût

Gratuit.

#### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

#### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site internet](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

## Liens utiles

### Textes de référence

- Loi n°2018-1021 du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique notamment son article L. 178 codifié à l’[article L. 171-2](https://circulaire.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041569814) du Code de la construction et de l’habitation ;
- [Décret n°2021-1674](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044506527) du 16 décembre 2021 relatif à la déclaration environnementale de produits de construction et de décoration ainsi que des équipements électriques, électroniques et de génie climatique ;
- [Arrêté du 14 décembre 2021](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044525628) relatif à la déclaration environnementale des produits destinés à un usage dans les ouvrages de bâtiment et à la déclaration environnementale des produits utilisée pour le calcul de la performance environnementale des bâtiments ;
- [Arrêté du 14 décembre 2021](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044525687) relatif à la vérification par tierce partie indépendante des déclarations environnementales des produits destinés à un usage dans les ouvrages de bâtiment et des déclarations environnementales des produits utilisées pour le calcul de la performance environnementale des bâtiments.

### Autres liens utiles

- [Site internet du programme de déclarations environnementales « INIES »](https://www.inies.fr/programmes-et-services/le-programme-de-verification-inies/) ;
- [Site internet du programme de déclarations environnementales « PEPecopassport »](http://www.pep-ecopassport.org/fr/).
