﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP090" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Courtier en assurance et en réassurance" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="courtier-en-assurance-et-en-reassurance" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/courtier-en-assurance-et-en-reassurance.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="courtier-en-assurance-et-en-reassurance" -->
<!-- var(translation)="None" -->

# Courtier en assurance et en réassurance

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

Le courtier en assurance et en réassurance (« courtier ») est un commerçant inscrit au registre du commerce et des sociétés dont la mission est de présenter, proposer ou aider à conclure des contrats d'assurance ou de réassurance. À ce titre, ses clients sont les assurés et non pas les compagnies d'assurance.

Son rôle est avant tout de conseiller ses clients en les aidant à souscrire des contrats d'assurance ou de réassurance et non de leur vendre ces contrats.

Dans le cadre de son activité, il peut être amené à concevoir des contrats sur mesure pour répondre à des demandes spécifiques de ses clients et à négocier les meilleures conditions tarifaires auprès des assureurs.

*Pour aller plus loin* : articles L. 511-1 et suivant du Code des assurances.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne qui souhaite exercer la profession de courtier doit justifier d'une capacité professionnelle en assurance de niveau I.

Pour en savoir plus sur les niveaux de capacité professionnelle en assurance, il est conseillé de se reporter au site de l'[Orias](https://www.orias.fr/web/guest/assurance2).

#### Formation

Pour exercer la profession de courtier, l'intéressé doit justifier :

- soit d'un stage professionnel dont le programme minimal de formation est fixé par l'arrêté du 11 juillet 2008. Il ne pourra être inférieur à une durée de 150 heures, et devra être effectué :
  - soit auprès d'une entreprise d'assurance, ou encore d'un intermédiaire s'il est immatriculé en qualité de courtier ou d'agent d'assurance,
  - soit auprès d'un organisme de crédit ou d'une société de financement ;

**À noter**

À l'issue du stage, le professionnel sera soumis à un contrôle des compétences acquises.

- soit de deux années d'expérience en tant que cadre dans une fonction relative à la production ou à la gestion de contrats d'assurance ou de capitalisation au sein d'une entreprise d'assurance ou auprès d'un intermédiaire en assurance ;
- soit de quatre années d'expérience dans une fonction relative à la production ou à la gestion de contrats d'assurance ou de capitalisation au sein de ces mêmes entreprises ou auprès de ces mêmes intermédiaires ;
- de la possession d'un diplôme ou d'un titre correspondant :
  - soit au niveau de formation master,
  - soit simultanément au niveau de formation licence et à la spécialité de formation 313 relative aux activités financières, bancaires, assurantielles et immobilières ;
- soit d'un certificat de qualification professionnelle enregistré au Répertoire national des certifications professionnelles et correspondant à la spécialité de formation 313 relative aux activités financières, bancaires, assurantielles et immobilières.

*Pour aller plus loin* : articles R. 512-9, R. 512-11 et A. 512-6 du Code des assurances.

#### Coûts associés à la qualification

La formation menant à la profession de courtier est payante. Pour plus d'informations, il est conseillé de se rapprocher d'un organisme de formation ou des établissements délivrant les diplômes ou certificats visés ci-dessus.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d'un État de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) établi et exerçant légalement les activités de courtier dans l'un de ces États peut exercer la même activité en France de manière temporaire et occasionnelle.

Il doit simplement en informer au préalable le registre de son État qui s'assurera de transmettre lui-même les informations au registre français (cf. infra « 5°. a. Informer le registre unique de l'État de l'UE ou de l'EEE »).

*Pour aller plus loin* : directive européenne d'intermédiation en assurance du 9 décembre 2002 dite « [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE établi et exerçant légalement les activités de courtier dans l'un de ces États peut exercer la même activité en France de manière permanente.

Il doit simplement en informer au préalable le registre de son État qui s'assurera de transmettre lui-même les informations au registre français (cf. infra « 5°. a. Informer le registre unique de l'État de l'UE ou de l'EEE »).

*Pour aller plus loin* : directive européenne d'intermédiation en assurance du 9 décembre 2002 dite « [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le courtier est tenu de respecter les conditions d'honorabilité qui incombent à sa profession, conformément aux dispositions de l'article L. 322-2 du Code des assurances. Il ne doit notamment pas avoir fait l'objet d'une condamnation depuis moins de dix ans pour crime, de peines pour blanchiment, corruption ou fraude fiscale, ou encore de pas avoir été destitué de fonctions d'officier public ou ministériel.

**À noter**

Un extrait du bulletin n° 2 du casier judiciaire du courtier est directement demandé par l'Orias pour l'exercice de la profession.

*Pour aller plus loin* : articles L. 322-2, L. 512-4 et R. 514-1 du Code des assurances.

## 4°. Obligation d'immatriculation et assurance

### a. Demander son immatriculation au registre de l'Orias

L'intéressé qui souhaite exercer en France la profession de courtier doit impérativement être immatriculé au registre unique des intermédiaires en assurance, banque et finance (Orias).

#### Pièces justificatives

Avant son immatriculation, l'intéressé doit procéder à l'[ouverture d'un compte](https://www.orias.fr/espace-professionnel) sur le site de l'Orias ainsi qu'à l'envoi d'un dossier comportant l'ensemble des pièces justificatives suivantes :

- un extrait KBIS datant de moins de trois mois et mentionnant l’activité de courtier ;
- un document justifiant sa capacité professionnelle telle que précisée au paragraphe « 2°. a. Formation » :
  - un livret de stage,
  - une attestation de formation,
  - une attestation de fonction,
  - un diplôme ou un certificat ;
- le règlement des frais d'inscription.

#### Renouvellement de l'inscription

La procédure d’immatriculation à l'Orias doit être renouvelée tous les ans et lors de toute modification de la situation professionnelle de l'intéressé. Dans ce dernier cas, le courtier devra tenir informé l'Orias un mois avant la modification ou dans le mois qui suit l’événement modificatif. Il devra en outre fournir une attestation de responsabilité civile professionnelle ainsi qu’une attestation de garantie financière.

La demande de renouvellement doit impérativement intervenir avant le 31 janvier de chaque année et doit comporter le règlement des frais d'inscription.

**À savoir**

Les modèles d'attestation sont directement consultables sur le site de l'[Orias](https://www.orias.fr/web/guest/en-savoir-plus-ias).

#### Coût

Les frais d'immatriculation sont fixés à 30 euros payables directement en ligne sur le site de l'Orias. En cas de non-règlement de ces frais, l'Orias envoie un courrier informant l'intéressé qu'il dispose d'un délai de trente jours à compter de la date d'accusé de réception du courrier pour régler la somme. À défaut du règlement de cette somme :

- la demande de première inscription à l'Orias ne sera pas prise en compte ;
- le courtier sera radié du registre s'il s'agit d'une demande de renouvellement.

*Pour aller plus loin* : article R. 512-5 du Code des assurances.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

Dans le cadre de son activité, le courtier a l’obligation de souscrire une assurance de responsabilité professionnelle le couvrant des dommages qu’il pourrait causer à autrui dans l’exercice de sa profession.

*Pour aller plus loin* : articles L. 512-6, R. 512-4 et A. 512-4 du Code des assurances.

### c. Obligation de souscrire une assurance garantie financière

Tout personne exerçant la profession de courtier, dès lors qu'elle encaisse des fonds, même à titre occasionnel, est tenue de souscrire une assurance garantie financière affectée au remboursement de ces fonds à ses assurés.

Le montant minimum de la garantie financière doit être au moins égal à 115 000 euros, et ne peut être inférieur au double du montant moyen mensuel des fonds encaissés par le courtier calculé sur la base des fonds encaissés au cours des douze mois précédant le mois de la date de souscription ou de reconduction de l'engagement de caution.

*Pour aller plus loin* : articles L. 512-7, R. 512-15 et A. 512-5 du Code des assurance.

### d. Sanctions pénales

Le non-respect des obligations visées aux paragraphes 3° et 4° par l'intéressé est puni d'une peine d'emprisonnement de deux ans et/ou d'une amende de 6 000 euros.

*Pour aller plus loin* : article L. 514-1 du Code des assurances.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Informer le registre unique de l'État de l'UE ou de l'EEE

#### Autorité compétente

Le ressortissant d'un État membre de l'UE ou de l'EEE qui exerçait l'activité de courtier dans cet État et qui souhaite exercer en libre prestation de services ou en libre établissement en France doit au préalable en informer le registre unique de son État.

#### Procédure

Le ressortissant devra communiquer au registre unique de son État les informations suivantes :

- son nom, son adresse et, le cas échéant, son numéro d'immatriculation ;
- l’État membre dans lequel il souhaite exercer son activité en cas de LPS ou s'établir en cas de LE ;
- la catégorie d'intermédiaire d'assurance à laquelle il appartient, à savoir celle de courtier ;
- les branches d'assurance concernées s'il y a lieu.

#### Délai

Le registre unique de l'État de l'UE ou de l'EEE auprès duquel le ressortissant a notifié son intention d'exercer son activité en France dispose d'un délai d'un mois pour communiquer à l'Orias les informations relatives à l'intéressé.

Le ressortissant pourra ensuite commencer son activité de courtier en France dans un délai d'un mois après que le registre unique de son État l'a informé de la communication faite à l'Orias.

*Pour aller plus loin* : articles L. 515-1 et L. 515-2 du Code des assurances ; article 6 de la directive européenne « DIA1 » du 9 décembre 2002 ; articles 4 et 6 de la directive européenne sur l'intermédiation en assurance du 20 janvier 2016 dite « [DIA2](https://www.orias.fr/documents/10227/26917/2016-01-20_Directive%20europeenne%20sur%20la%20distribution%20assurances.pdf) » ; ordonnance n° 2018-361 du 16 mai 2018 et décret n° 2018-431 du 1er juin 2018 qui transposent la directive 2016/97 du 20 janvier 2016, et qui entreront en vigueur le 1er octobre 2018.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).