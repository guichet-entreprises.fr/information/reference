﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP090" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Insurance and reinsurance broker" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="insurance-and-reinsurance-broker" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/insurance-and-reinsurance-broker.html" -->
<!-- var(last-update)="2020-04-15 17:21:06" -->
<!-- var(url-name)="insurance-and-reinsurance-broker" -->
<!-- var(translation)="Auto" -->


Insurance and reinsurance broker
================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:06<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The insurance and reinsurance broker ("broker") is a merchant registered in the register of trade and companies whose mission is to present, propose or assist in the conclusion of insurance or reinsurance contracts. As such, its customers are the insured and not the insurance companies.

Its role is primarily to advise its clients by helping them to take out insurance or reinsurance contracts and not to sell them these contracts.

As part of its business, it may be required to design tailored contracts to meet specific customer demands and negotiate the best rate terms with insurers.

*For further information*: Articles L. 511-1 and following the Insurance Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Anyone who wishes to practise as a broker must justify a professional ability in Level I insurance.

To learn more about the levels of professional capacity in insurance, it is advisable to refer to the[Orias](https://www.orias.fr/web/guest/assurance2).

#### Training

In order to practise as a broker, the person concerned must justify:

- or a professional internship whose minimum training programme is set by the[decreed from 11 July 2008](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000019216662). It cannot be less than 150 hours, and must be carried out:- either with an insurance company, or an intermediary if he is registered as a broker or insurance agent,
  - either with a credit agency or a finance company

**Please note**

At the end of the internship, the professional will be subject to a check of the acquired skills.

- two years of executive experience in a function related to the production or management of insurance or capitalization contracts within an insurance company or with an insurance intermediary;
- four years of experience in a function related to the production or management of insurance or capitalization contracts within or with those same intermediaries;
- holding a diploma or corresponding title:- either at the master's level,
  - or simultaneously at the level of bachelor's and training 313 in financial, banking, insurance and real estate activities;
- i.e. a certificate of professional qualification registered in the national directory of professional certifications and corresponding to the speciality of training 313 relating to financial, banking, insurance and real estate activities.

*For further information*: Articles R. 512-9, R. 512-11 and A. 512-6 of the Insurance Code.

#### Costs associated with qualification

Training leading to the profession of broker is paid off. For more information, it is advisable to approach a training organization or the institutions issuing the diplomas or certificates referred to above.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) state or party to the European Economic Area (EEA) established and legally practising brokering activities in one of these states may carry out the same activity in France on a temporary and occasional basis.

He must simply inform the register of his state beforehand, which will ensure that the information is transmitted to the French register himself (see infra "5°. a. Inform the single register of the EU state or the EEA").

*For further information*: European Insurance Intermediation Directive of 9 December 2002 known as " [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state established and legally practising brokering activities in one of these states may carry out the same activity in France on a permanent basis.

He must simply inform the register of his state beforehand, which will ensure that the information is transmitted to the French register himself (see infra "5°. a. Inform the single register of the EU state or the EEA").

*For further information*: European Insurance Intermediation Directive of 9 December 2002 known as " [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The broker is required to respect the conditions of honour that his profession is responsible for, in accordance with the provisions of the[Article L. 322-2 of the Insurance Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073984&idArticle=LEGIARTI000006797493&dateTexte=&categorieLien=cid). In particular, he must not have been convicted for less than ten years for crimes, money laundering, corruption or tax evasion, or of not being removed from public or ministerial official duties.

**Please note**

An excerpt from bulletin 2 of the broker's criminal record is directly requested by the Orias for the practice of the profession.

*For further information*: Articles L. 322-2, L. 512-4 and R. 514-1 of the Insurance Code.

4°. Registration requirement and insurance
--------------------------------------------------------------

### a. Request registration on the Orias register

The person concerned who wishes to practise in France as a broker must be registered in the single register of intermediaries in insurance, banking and finance (Orias).

**Supporting documents**

Before registration, the person concerned must proceed with the[opening an account](https://www.orias.fr/espace-professionnel) Orias website and sending a file containing all the following supporting documents:

- A KBIS extract less than three months old that mentions broker activity;
- a document justifying his professional capacity as specified in paragraph "2." a. Training":- an internship booklet,
  - A certificate of training,
  - A certificate of office,
  - A diploma or certificate
- settlement of registration fees.

**Renewal of registration**

The registration procedure at the Orias must be renewed every year and with any change in the person's professional situation. In the latter case, the broker will have to keep the Orias informed one month before the change or within one month of the amending event. He will also have to provide a certificate of professional civil liability as well as a certificate of financial guarantee.

The renewal application must take place before January 31 of each year and must include the payment of registration fees.

**What to know**

The certification templates are directly available on the[Orias](https://www.orias.fr/web/guest/en-savoir-plus-ias).

**Cost**

The registration fee is set at 30 euros payable directly online on the Orias website. In the event of non-payment of these charges, orias sends a letter informing the person concerned that it has a period of thirty days from the date of receipt of the mail to pay the sum. Failing to settle this amount:

- The application for a first registration to the Orias will not be taken into account;
- the broker will be removed from the register if it is a renewal application.

*For further information*: Article R. 512-5 of the Insurance Code.

### b. Obligation to take out professional liability insurance

As part of his business, the broker is required to take out professional liability insurance covering the damage he could cause to others in the course of his profession.

*For further information*: Articles L. 512-6, R. 512-4 and A. 512-4 of the Insurance Code.

### c. Obligation to take out financial guarantee insurance

Any person practising as a broker, as long as he cashes in funds, even on a casual basis, is required to take out financial guarantee insurance used to repay these funds to his policyholders.

The minimum amount of the financial guarantee must be at least 115,000 euros, and cannot be less than double the average monthly amount of funds collected by the broker calculated on the basis of funds collected in the twelve months preceding the month of the date subscription or renewal of the bond commitment.

*For further information*: Articles L. 512-7, R. 512-15 and A. 512-5 of the Insurance Code.

### d. Criminal sanctions

Failure to comply with the obligations covered by paragraphs 3 and 4 is punishable by a prison sentence of two years and/or a fine of 6,000 euros.

*For further information*: Article L. 514-1 of the Insurance Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Inform the single register of the EU state or the EEA

**Competent authority**

A national of an EU or EEA Member State who was a broker in that state and who wishes to practice free service or Freedom of establishment in France must first inform the single register of his or her state.

**Procedure**

The national will have to provide the following information to the single register of his or her state:

- His name, address and, if applicable, registration number;
- The Member State in which it wishes to operate in the event of LPS or establish itself in the event of an LE;
- The category of insurance intermediary to which he belongs, namely that of broker;
- insurance branches, if necessary.

**Timeframe**

The single register of the EU state or EEA, from which the national has notified his intention to operate in France, has one month to provide the Orias with the information relating to him.

The national will then be able to start his brokering activity in France within one month after the single register of his state has informed him of the communication made to the Orias.

*For further information*: Articles L. 515-1 and L. 515-2 of the Insurance Code; Article 6 of the EU directive "DIA1" of 9 December 2002; Articles 4 and 6 of the European Directive on Insurance Intermediation of 20 January 2016, known as " [DIA2](https://www.orias.fr/documents/10227/26917/2016-01-20_Directive%20europeenne%20sur%20la%20distribution%20assurances.pdf) » ; Ordinance No. 2018-361 of 16 May 2018 and Decree No. 2018-431 of 1 June 2018 which transpose the 2016/97 Directive of 20 January 2016, and which will come into force on 1 October 2018.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

