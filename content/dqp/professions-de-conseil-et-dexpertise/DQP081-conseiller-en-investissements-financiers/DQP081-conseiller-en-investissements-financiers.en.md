﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP081" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Financial investment advisor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="financial-investment-advisor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/financial-investment-advisor.html" -->
<!-- var(last-update)="2020-04-15 17:21:04" -->
<!-- var(url-name)="financial-investment-advisor" -->
<!-- var(translation)="Auto" -->


Financial investment advisor
============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:04<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Financial Investment Advisor (CIF) is a professional working as a professional in:

- advising on investments in financial instruments (equities, bonds, etc.);
- Advice on the provision of investment services;
- advice on carrying out transactions on various assets.

*For further information*: Article L. 541-1 of the Monetary and Financial Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of CIF, the person concerned must comply with all the following conditions:

- Be at least 18 years old
- usually reside in or be legally established in France;
- be registered in the single register of the[Orias](https://www.orias.fr/) (single register of insurance, banking and finance intermediaries);
- have not been the subject of a final conviction in the last ten years:- for crime,
  - a firm prison sentence or at least six months' suspended imprisonment,
  - removal from the duties of public or ministerial officer;
- not subject to a ban on practising, a temporary or permanent suspension, or a total or partial withdrawal of accreditation;
In addition, it must also be up to date with the annual contribution (450 euros) from the Financial Markets Authority (AMF).

*For further information*: Article L.500-1 of the Monetary and Financial Code.

#### Training

To perform the functions of CIF, the professional must either:

- Have a three-year degree in legal, economic or management graduate studies;
- Have completed 150 hours of vocational training in connection with CIF missions;
- have two years of professional experience in the five years prior to taking up operations related to investment, banking or the provision of investment services.

*For further information*: Article 325-1 of the General Regulation of the[Amf](http://www.amf-france.org/).

#### Costs associated with qualification

The training leading to CIF's professional qualification is paid for and the cost varies depending on the course envisaged. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

In order to strengthen investor protection and to harmonise the conditions for investment service providers across the European Union (EU), directives on financial instrument markets have been adopted on 21 April 2004 and 15 May 2014 by the European Parliament and Council.

In derogation from these so-called guidelines[MIF I](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32004L0039) And[MIF II](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32014L0065), the exercise of CIF's activity in France, on a temporary and casual basis, is not regulated for a national of an EU member state or the European Economic Area (EEA) in France on a temporary and casual basis.

Thus, the national who wishes to practice in France will be subject to the regulations applicable to French nationals (see above "2." a. Professional qualifications").

*For further information*: European Parliament and Council Directive 2014/65/EU (MIF II) of 15 May 2014; Regulation 600/2014 of 15 May 2014 of the European Parliament and Council.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The same exemption applies to EU nationals who wish to carry out CIF on a permanent basis.

It will then be subject to the regulations applicable to French nationals (see above "2). b. EU or EEA nationals: for a temporary and casual exercise (Freedom to provide services)").

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Rules of good conduct

The CIF is subject to compliance with the rules of good conduct and must:

- Act honestly, fairly and professionally;
- Respect fairness
- inform customers and satisfy their interests
- Empower themselves to prevent and manage any conflict of interest;
- carry out its missions independently.

*For further information*: Article L. 541-8-1 of the Monetary and Financial Code and Articles 325-3 and following of the AMF's General Regulation.

### b. Application to join a professional association

The CIF must join one of the following AMF-approved associations:

- Investment, finance and corporate transfer analysts and advisors (Acifte);
- National Association of Financial Advice - Cif ([Anacofi-Cif](http://www.anacofi.asso.fr/)) ;
- National Chamber of Wealth Management Advisors ([CNCGP](http://www.cncgp.fr/)) ;
- National Chamber of Financial Investment Advisors ([CNCIF](http://www.cncif.org/)) ;
- Independent Wealth Management Consulting Company ([CGPI Company](http://www.lacompagniedescgpi.fr/)).

**Please note**

These associations can register themselves in the single register of their members.

**Admission**

The association determines the written procedures for admission and sanction of the professional.

This association is responsible for ensuring:

- monitoring the professional activity of its members;
- their collective representation;
- to defend their rights and interests.

The association verifies that the CIF has an activity program stating:

- The types of activities carried out by the professional;
- The structure of its organization
- the identity of its shareholders and the amount of their shareholding.

The association determines the procedures for membership, withdrawal of membership, control and sanction of its members through written procedures.

*For further information*: Article L. 541-4 of the Monetary and Financial Code; Article 325-2 of the AMF General Regulation.

### Sanctions

The act of an CIF practising illegally is punishable by five years' imprisonment and a fine of 375,000 euros, under the same conditions as the scam (see Article 313-2 of the Penal Code).

In the event of a violation of the registration rules, the CIF faces a two-year prison sentence and/or a fine of 6,000 euros.

*For further information*: Article L. 546-4 of the Monetary and Financial Code.

4°. Insurance
---------------------------------

### a. Obligation to register in the orias single register

**Competent authority**

The CIF must be registered on the agency's single register of insurance intermediaries for the maintenance of the single register of insurance, banking and finance intermediaries ([Orias](https://www.orias.fr/)).

**Supporting documents**

For any application for registration, the professional must register on the[Orias](https://www.orias.fr/) and provide online:

- a registration certificate in the Register of Trade and Companies (Kbis) of less than three months or, failing that, an ID if it is not registered in the register of trade and companies;
- a certificate of membership in a professional association (cf.supra "3.3. b. Application to join a professional association";
- proof of professional ability, i.e.:- i.e. a three-year graduate degree,
  - a minimum of 150 hours of training acquired from a professional or organization engaged in financial investment advisory activities,
  - two years of professional experience in the past five years;
- a certificate of professional civil liability (see infra "4.0). b. Insurance");
- registration fees (30 euros).

**Please note**

If no payment is made within 30 days of receiving the application, it will be refused.

**Timeframe**

Orias registers the applicant within a maximum of two months of receipt of his full file.

*For further information*: Articles L. 546-1 and L. 541-2 of the Monetary and Financial Code; Section L. 512-1 of the Insurance Code.

### b. Insurance

As a professional, the CIF must take out professional liability insurance.

This guarantee takes effect on 1 March for a period of twelve months and is tacitly renewed on 1 January of each year.

When the professional starts his cIF business, his insurance contract runs for the period of the date of registration to March 1 of the following year.

The threshold for this guarantee is 150,000 euros per claim and 150,000 euros per year of insurance for legal entities with less than two employees.

*For further information*: Articles L. 541-3 and D. 541-9 of the Monetary and Financial Code.

5°. Remedies
----------------------

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

