﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP081" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Conseiller en investissements financiers" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="conseiller-en-investissements-financiers" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/conseiller-en-investissements-financiers.html" -->
<!-- var(last-update)="2020-04-15 17:21:04" -->
<!-- var(url-name)="conseiller-en-investissements-financiers" -->
<!-- var(translation)="None" -->

# Conseiller en investissements financiers

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:04<!-- end-var -->

## 1°. Définition de l’activité

Le conseiller en investissements financiers (CIF) est un professionnel exerçant les activités de :

- conseil en investissements portant sur des instruments financiers (actions, obligations, etc.) ;
- conseil sur la fourniture de services d’investissement ;
- conseil sur la réalisation d’opérations sur biens divers.

*Pour aller plus loin* : article L. 541-1 du Code monétaire et financier.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de CIF, l'intéressé doit respecter l'ensemble des conditions suivantes :

- avoir au moins 18 ans ;
- résider habituellement en France ou y être légalement établi ;
- être immatriculé au registre unique de l'[Orias](https://www.orias.fr/) (registre unique des intermédiaires en assurance, banque et finance) ;
- ne pas avoir fait l'objet, au cours des dix dernières années, d'une condamnation définitive :
  - pour crime,
  - à une peine d'emprisonnement ferme ou d'au moins six mois avec sursis,
  - d'une destitution des fonctions d'officier public ou ministériel ;
- ne pas avoir fait l'objet d'une interdiction d'exercer, d'une suspension à titre temporaire ou définitive, ou d'un retrait total ou partiel d'agrément.

Par ailleurs, il doit également être à jour de la contribution annuelle (450 euros) auprès de l'Autorité des marchés financiers (AMF).

*Pour aller plus loin* : article L.500-1 du Code monétaire et financier.

#### Formation

Pour exercer les fonctions de CIF, le professionnel doit soit :

- être titulaire d’un diplôme de trois années d’études supérieures juridiques, économiques ou de gestion ;
- avoir suivi une formation professionnelle d'une durée de 150 heures en lien avec les missions du CIF ;
- avoir acquis une expérience professionnelle de deux ans au cours des cinq années précédant son entrée en fonction dans des activités liées à l'investissement, aux opérations de banque ou encore à la fourniture de services d'investissement.

*Pour aller plus loin* : article 325-1 du règlement général de l’[AMF](http://www.amf-france.org/).

#### Coûts associés à la qualification

La formation menant à la qualification professionnelle de CIF est payante et son coût varie selon le cursus envisagé. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

En vue de renforcer la protection des investisseurs et d'harmoniser les conditions d'exercice des prestataires de services d'investissement à travers l'Union européenne (UE), des directives portant sur les marchés d'instruments financiers ont été adoptées le 21 avril 2004 et le 15 mai 2014 par le Parlement et le Conseil européens.

Par dérogation à ces directives dites [MIF I](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32004L0039) et [MIF II](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32014L0065), l'exercice pour un ressortissant d'un État membre de l'UE ou de l'Espace économique européen (EEE) de l'activité de CIF en France, à titre temporaire et occasionnel, n'est pas réglementé.

Ainsi, le ressortissant qui souhaite exercer en France sera soumis à la réglementation applicable aux ressortissants français (cf. supra « 2°. a. Qualifications professionnelles »).

*Pour aller plus loin* : directive 2014/65/UE (MIF II) du 15 mai 2014 du Parlement et du Conseil Européens ; règlement n° 600/2014 du 15 mai 2014 du Parlement et du Conseil Européens.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

La même dérogation s'applique au ressortissant de l'UE qui souhaiterait exercer l'activité de CIF à titre permanent.

Celui-ci sera alors soumis à la réglementation applicable aux ressortissants français (cf. supra « 2°. b. Ressortissant l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services) »).

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### a. Règles de bonne conduite

Le CIF est soumis au respect des règles de bonne conduite et doit à ce titre :

- agir de manière honnête, loyale et professionnelle ;
- respecter l'équité ;
- informer ses clients et satisfaire leurs intérêts ;
- se donner les moyens de prévenir et gérer tout conflit d'intérêts ;
- exercer ses missions de manière indépendante.

*Pour aller plus loin* : article L. 541-8-1 du Code monétaire et financier et articles 325-3 et suivants du règlement général de l’AMF.

### b. Demande d’adhésion à une association professionnelle

Le CIF doit adhérer à l’une des associations agréées par l’AMF suivantes :

- Analystes et conseillers en investissements, finance et transmission d’entreprise (Acifte) ;
- Association nationale des conseils financiers - Cif ([Anacofi-Cif](http://www.anacofi.asso.fr/)) ;
- Chambre nationale des conseillers en gestion de patrimoine ([CNCGP](http://www.cncgp.fr/)) ;
- Chambre nationale des conseillers en investissements financiers ([CNCIF](http://www.cncif.org/)) ;
- la Compagnie des conseils en gestion de patrimoine indépendants ([la Compagnie des CGPI](http://www.lacompagniedescgpi.fr/)).

**À noter**

Ces associations peuvent procéder elles-mêmes à l’inscription au registre unique de leurs adhérents.

#### Admission

L'association détermine les procédures écrites d'admission et de sanction du professionnel.

Cette association est chargée de veiller :

- au suivi de l’activité professionnelle de ses membres ;
- à leur représentation collective ;
- à la défense de leurs droits et intérêts.

L’association vérifie que le CIF dispose d’un programme d’activité mentionnant :

- les types d’activité exercés par le professionnel ;
- la structure de son organisation ;
- le cas échéant, l’identité de ses actionnaires et le montant de leur participation.

L’association détermine les procédures d’adhésion, de retrait d’adhésion, de contrôle et de sanction de ses membres par le biais de procédures écrites.

*Pour aller plus loin* : article L. 541-4 du Code monétaire et financier ; article 325-2 du règlement général de l’AMF.

### Sanctions

Le fait pour un CIF d’exercer illégalement la profession est passible de cinq ans d’emprisonnement et de 375 000 euros d’amende, dans les mêmes conditions que l’escroquerie (cf. article 313-2 du Code pénal).

En cas de violation des règles d’immatriculation, le CIF encourt une peine de deux ans d’emprisonnement et/ou de 6 000 euros d’amende.

*Pour aller plus loin* : article L. 546-4 du Code monétaire et financier.

## 4°. Assurances

### a. Obligation de s'immatriculer au registre unique de l'Orias

#### Autorité compétente

Le CIF doit être immatriculé sur le registre unique des intermédiaires d’assurance tenu par l’Organisme pour la tenue du registre unique des intermédiaires en assurance, banque et finance ([Orias](https://www.orias.fr/)).

#### Pièces justificatives

Pour toute demande d'immatriculation, le professionnel doit s'inscrire sur le site de l'[Orias](https://www.orias.fr/) et fournir en ligne :

- un extrait d'immatriculation au registre du commerce et des sociétés (Kbis) de moins de trois mois ou, à défaut, une pièce d'identité s'il n'est pas immatriculé au registre du commerce et des sociétés ;
- une attestation d'adhésion à une association professionnelle (cf.supra « 3°. b. Demande d'adhésion à une association professionnelle » ;
- un justificatif de capacité professionnelle, à savoir :
  - soit un diplôme sanctionnant trois années d'études supérieures,
  - soit une formation de 150 heures minimum acquise auprès d'un professionnel ou d'un organisme exerçant les activités de conseil en investissements financiers,
  - soit une expérience professionnelle de deux ans au cours des cinq dernières années ;
- une attestation de responsabilité civile professionnelle (cf.infra « 4°. b. Assurances ») ;
- le règlement des frais d'inscription (30 euros).

**À noter**

En l'absence de paiement dans un délai de trente jours suivant la réception de la demande, celle-ci sera refusée.

#### Délai

L'Orias procède à l'immatriculation du demandeur dans un délai maximum de deux mois à compter de la réception de son dossier complet.

*Pour aller plus loin* : articles L. 546-1 et L. 541-2 du Code monétaire et financier ; article L. 512-1 du Code des assurances.

### b. Assurance

En qualité de professionnel, le CIF doit souscrire une assurance de responsabilité civile professionnelle.

Cette garantie prend effet au 1er mars pour une durée de douze mois et est reconduite tacitement au 1er janvier de chaque année.

Lorsque le professionnel débute son activité de CIF, son contrat d'assurance court pour la période de la date de son immatriculation au 1er mars de l'année suivante.

Le seuil de cette garantie est de 150 000 euros par sinistre et 150 000 euros par année d’assurance pour les personnes morales de moins de deux salariés.

*Pour aller plus loin* : articles L. 541-3 et D. 541-9 du Code monétaire et financier.

## 5°. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).