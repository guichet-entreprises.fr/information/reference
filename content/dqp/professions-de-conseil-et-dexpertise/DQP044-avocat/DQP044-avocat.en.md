﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP044" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Lawyer/Advocate" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="lawyer-advocate" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/lawyer-advocate.html" -->
<!-- var(last-update)="2020-04-15 17:21:00" -->
<!-- var(url-name)="lawyer-advocate" -->
<!-- var(translation)="Auto" -->


Lawyer/Advocate
======

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:00<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The lawyer is a legal professional whose mission is to assist and/or represent the parties in the event of a dispute. He is competent to apply and plead before the courts, disciplinary bodies or public authorities.

The lawyer is also required to seek amicable solutions, receive judicial assignments, conduct legal consultations or write acts for others (acts under private seing, mail, formal notice, etc.).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of practising as a lawyer is reserved for those who are on the bar of lawyers, and who:

- have a master's degree in law (B.A. 4) or a training degree recognized as equivalent;
- hold the Certificate of Fitness to Practise (Capa) obtained at a regional training centre, with exceptions;
- have not been criminally convicted for acts contrary to honour, probity or good morals;
- have not been subject to disciplinary or administrative sanctions of dismissal, dismissal, dismissal, withdrawal of accreditation or authorization;
- have not been subject to personal bankruptcy.

*To go further* Article 11 of Law 71-1130 of 31 December 1971 on the reform of certain judicial and legal professions.

#### Training

Access to the Regional Vocational Training Centre delivering Capa is after passing an entrance exam with written eligibility and oral entrance exams organized by the universities.

The exam is open to holders of a minimum law degree.

The training at the regional centre consists of three periods:

- a basic common part that lasts six months during which the student lawyer learns ethics, the drafting of legal acts, pleading and oral debate;
- a second six-month period devoted to the implementation of the individual educational project;
- a final six-month period devoted to an internship with a lawyer.

At the end of these three periods, the student lawyer appears for the Capa exam.

*To go further* Article 51 and following of Decree 91-1197 of 27 November 1991 organising the profession of lawyer; Decree of 17 October 2016 setting out the programme and the terms of the access examination to the regional professional training centre for lawyers.

#### Costs associated with qualification

The fees for registration for the training leading to the issuance of the Capa depend on the schools that provide it. For more information, it is advisable to get closer to these establishments.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of an EU or EEA state, legally practising as a lawyer in one of these states, may use his or her professional title in France, either temporarily or occasionally.

He will have to represent his clients in accordance with the rules that apply to French lawyers.

In the case of representation before a Court of Appeal or in the case of proceedings before the high court subject to mandatory representation, the national will have to elect residence with an authorized lawyer.

*To go further* Articles 202 to 202-3 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

There are two separate regimes for practising as a lawyer in France, under two European Union directives:

- under Directive 98/5/EC of 16 February 1998, any national may practise, in any other Member State, the activities of a lawyer under his or her original professional title, subject to registration with the National Bar Council. After 3 years of practice under its original title, and under certain conditions, any national may benefit from assimilation under the French title (see Articles 83 and following of Law 71-1130 of 31 December 1971 on the reform of certain judicial and legal professions), and Articles 93-1 and 201 of Decree No. 91-1197 of 27 November 1991 organising the profession of lawyer);
- under the European Parliament and Council Directive 2005/36/EC of 7 September 2005 on the recognition of professional qualifications, any national of an EU or EEA state may settle in France to practise as a lawyer if he has completed a post-secondary education cycle of at least one year and justifies:- or a training title that allows the profession to be practised in that state,
  - or practising the profession for at least one year in the last ten years, in a state that does not regulate access or the practice of the profession.

Once he fulfils one of these conditions, he will be able to apply to the National Bar Council for recognition of his professional qualifications (see infra "5°. a. Request recognition of the EU or EEA national's professional qualifications for a permanent exercise (LE) ").

He can then apply for registration on the bar board of the place in which he wishes to practice.

**What to know**

The supporting documents required for inclusion on the board depend on the bar chosen by the national. However, the person concerned will at least have to send:

- A copy of bulletin 3 of the criminal record of less than three months;
- A copy of the master's degree or equivalent supporting documents depending on the person's situation;
- A copy of CAPA or equivalent supporting documents depending on the individual's situation;
- A valid piece of identification
- The minutes of the swearing-in.

If there are substantial differences between his training and the professional qualifications required in France, the national may be subjected to an aptitude test (see infra "5°. a. Good to know: compensation measure").

*To go further* Articles 99 and 203 of Decree 91-1197 of 27 November 1991 organising the profession of lawyer; Decree of October 10, 2017 setting out the program and the modalities of the aptitude examination for Articles 204-2 and 204-3 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethics

The National Bar Council has incorporated the code of ethics applicable to all lawyers practising in France into its National Internal Regulation (RIN).

As such, the lawyer must respect the codified ethical rules, including the rules relating to:

- Professional secrecy;
- respect for the principle of the contradictory;
- Confidentiality of exchanges;
- duty of care.

For more information, it is advisable to refer to the[National Bar Council website](https://www.cnb.avocat.fr/sites/default/files/rin_2017-03-31_consolidepublie-jo.pdf).

### b. Incompatibility

The legal profession is incompatible with:

- any commercial activity. However, the development of an ancillary business activity is permitted under certain conditions (see Article 111 of Decree 91-1197 of 27 November 1991 amended in 2016);
- the functions of partner in a partnership, a general partner in limited partnerships and shares, a manager in a limited liability company, chairman of the board of directors, a member of the board of directors or a member of the board of directors Managing director of a limited company, managing director of a civil society;
- any other profession except teaching, as an associate member of Parliament or as an assistant senator, as a substitute for a district judge, as a member assessor of children's courts or in the joint courts of rural leases, councillor, member of the social security courts, as well as those of arbitrator, mediator, conciliator or receiver.

*To go further* Articles 111 to 123 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

### c. Continuing vocational training

The lawyer must update his knowledge and develop his skills throughout his career during annual continuing training.

Continuing education must be at least 20 hours per year or 40 hours in two consecutive years.

The lawyer newly placed on the bar will also have to follow 10 hours of ethics in his first two years.

These trainings must be declared to the Council of the Bar Association by 31 January of the past calendar year. The lawyer will have to attach to this statement any proof that he participated in such training.

*To go further* Articles 85 and 85-1 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

4°. Financial insurance and guarantee
---------------------------------------------------------

### a. Obligation to take out professional liability insurance

A liberal lawyer must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

The lawyer must be covered for any acts he commits as part of his exercise to the tune of a minimum of 1,500,000 euros.

*To go further* Articles 205 and 206 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

### b. Obligation to justify a financial guarantee

The lawyer must justify a financial guarantee that will be allocated to the repayment of the funds received during his activity. This is a bonding commitment made to a bank or credit institution, an insurance company or a mutual surety company. Its amount must be at least equal to that of the funds it plans to hold.

*To go further* Articles 210 to 225 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request recognition of the EU or EEA national's professional qualifications for permanent exercise (LE)

**Procedure**

The National Bar Council is responsible for deciding on the application for recognition of the professional qualifications of the national wishing to settle in France to practise as a lawyer.

**Supporting documents**

In support of his application, the national will have to send a file to the competent authority, including:

- A valid piece of identification
- any documentation justifying that he has successfully completed a post-secondary education;
- copies of training titles or any documents justifying access to the legal profession, issued in an EU or EEA state;
- a certificate justifying that the national has practised the profession in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise;
- a certificate of registration of less than three months from the competent authority of the EU State or the EEA in which he has acquired his legal title.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The National Bar Council will have one month to inform the national of any missing documents. He will then have three months to decide on the application or submit the national to a compensation measure.

In the event of a silence, the application will be deemed rejected. However, the national may apply to the Paris Court of Appeal for reconsideration of his application.

*To go further* Article 99 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

**Good to know: compensation measure**

The aptitude test, which is organised in case of substantial differences between the national's training and professional experience with those required in France, consists of an oral test before a jury. The national will be required to pass a ten-minute presentation on a randomly drawn subject, as well as an interview with the jury, of about 20 minutes.

*To go further* : decree of October 10, 2017 setting out the program and the modalities of the aptitude examination provided for in Articles 204-2 and 204-3 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

