﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP044" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Avocat" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="avocat" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/avocat.html" -->
<!-- var(last-update)="2020-04-15 17:21:00" -->
<!-- var(url-name)="avocat" -->
<!-- var(translation)="None" -->

# Avocat

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:00<!-- end-var -->

## 1°. Définition de l'activité

L'avocat est un professionnel de la justice dont les missions sont d'assister et/ou de représenter les parties en cas de litige. Il est compétent pour postuler et plaider devant les tribunaux, les organismes disciplinaires ou les autorités publiques.

L'avocat est également amené à rechercher des solutions amiables, recevoir des missions des instances judiciaires, faire des consultations juridiques ou encore rédiger des actes pour autrui (actes sous seing privé, courrier, mise en demeure, etc.).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'exercice de la profession d'avocat est réservé aux personnes inscrites au tableau du barreau des avocats, et qui :

- sont titulaires d'une maîtrise en droit (bac + 4) ou d'un titre de formation reconnu comme équivalent ;
- sont titulaires du certificat d'aptitude à la profession d'avocat (Capa) obtenu dans un centre régional de formation, sauf exceptions ;
- n'ont pas fait l'objet de condamnation pénale pour des agissements contraires à l'honneur, à la probité ou aux bonnes mœurs ;
- n'ont pas fait l'objet de sanction disciplinaire ou administrative de destitution, radiation, révocation, retrait d'agrément ou d'autorisation ;
- n'ont pas fait l'objet de faillite personnelle.

*Pour aller plus loin* : article 11 de la loi n° 71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques.

#### Formation

L'accès au centre régional de formation professionnelle délivrant le Capa se fait après avoir réussi un examen d'entrée comportant des épreuves écrites d'admissibilité et des épreuves orales d'admission, organisées par les universités.

L'examen est ouvert aux titulaires d'une maîtrise en droit minimum.

La formation au centre régional se compose en trois période :

- une partie commune de base qui dure six mois pendant laquelle l'élève avocat apprend la déontologie, la rédaction des actes juridiques, la plaidoirie et le débat oral ;
- une deuxième période de six mois consacrée à la réalisation du projet pédagogique individuel ;
- une dernière période de six mois consacrée à un stage auprès d'un avocat.

À l'issue de ces trois périodes, l'élève avocat se présente à l'examen du Capa.

*Pour aller plus loin* : article 51 et suivants du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat ; arrêté du 17 octobre 2016 fixant le programme et les modalités de l'examen d'accès au centre régional de formation professionnelle d'avocats.

#### Coûts associés à la qualification

Les frais d’inscription à la formation menant à la délivrance du Capa dépendent des écoles qui la dispensent. Pour plus d’informations, il est conseillé de se rapprocher de ces établissements.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État de l'UE ou de l'EEE, exerçant légalement l’activité d'avocat dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra exercer la représentation de ses clients dans le respect des règles qui s'appliquent aux avocats français.

En cas de représentation devant une Cour d'appel ou en cas de procédure devant le tribunal de grande instance soumise à représentation obligatoire, le ressortissant devra élire domicile auprès d'un avocat habilité.

*Pour aller plus loin* : articles 202 à 202-3 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Il existe deux régimes distincts pour exercer la profession d'avocat en France, relevant de deux directives de l'Union européenne :

- dans le cadre de la directive 98/5/CE du 16 février 1998, tout ressortissant peut exercer, dans tout autre État membre, les activités d'avocat sous son titre professionnel d'origine, sous la condition de s'inscrire auprès du Conseil national des barreaux. Après 3 ans d'exercice sous son titre d'origine, et sous certaines conditions, tout ressortissant peut bénéficier d'une assimilation permettant d'exercer sous le titre français (cf. articles 83 et suivants de la loi n° 71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques, et articles 93-1 et 201 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat) ;
- dans le cadre de la directive directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles, tout ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer la profession d'avocat s'il a suivi un cycle d'études postsecondaire d'au moins un an et qu'il justifie :
  - soit d'un titre de formation permettant l'exercice de la profession dans cet État,
  - soit de l'exercice de la profession pendant au moins un an au cours des dix dernières années, dans un État qui ne réglemente ni l'accès, ni l'exercice de la profession.

Dès lors qu'il remplit l'une de ces conditions, il pourra demander la reconnaissance de ses qualifications professionnelles auprès du Conseil national des barreaux (cf. infra « 5°. a. Demander une reconnaissance de ses qualifications professionnelles au ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

Il pourra ensuite demander son inscription au tableau du barreau du lieu dans lequel il souhaite exercer.

**À savoir**

Les pièces justificatives demandées pour l'inscription au tableau dépendent du barreau choisi par le ressortissant. Cependant, l'intéressé devra au moins faire parvenir :

- une copie du bulletin n° 3 du casier judiciaire de moins de trois mois ;
- une copie du diplôme de la maîtrise de droit ou des pièces justificatives équivalentes en fonction de la situation de l'intéressé ;
- une copie du CAPA ou des pièces justificatives équivalentes en fonction de la situation de l'intéressé ;
- une pièce d'identité en cours de validité ;
- le procès-verbal de la prestation de serment.

S'il existe des différences substantielles entre sa formation et les qualifications professionnelles requises en France, le ressortissant pourra être soumis à un examen d'aptitude (cf. infra « 5°. a. Bon à savoir : mesure de compensation »).

*Pour aller plus loin* : articles 99 et 203 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat ; arrêté du 10 octobre 2017 fixant le programme et les modalités de l'examen d'aptitude prévu aux articles 204-2 et 204-3 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Déontologie

Le Conseil national des barreaux a intégré dans son règlement intérieur national (RIN), le Code de déontologie applicable à tous les avocats exerçant en France.

À ce titre, l'avocat doit respecter les règles déontologiques codifiées et notamment les règles relatives :

- au secret professionnel ;
- au respect du principe du contradictoire ;
- à la confidentialité des échanges ;
- au devoir de prudence.

Pour plus d'informations, il est conseillé de se reporter au [site du Conseil national des barreaux](https://www.cnb.avocat.fr/sites/default/files/rin_2017-03-31_consolidepublie-jo.pdf).

### b. Incompatibilité

La profession d'avocat est incompatible avec :

- toute activité à caractère commercial. Toutefois, le développement d'une activité commerciale à titre accessoire est autorisé sous certaines conditions (cf. article 111 du décret n° 91-1197 du 27 novembre 1991 modifié en 2016) ;
- les fonctions d'associé dans une société en nom collectif, d'associé commandité dans les sociétés en commandite simple et par actions, de gérant dans une société à responsabilité limitée, de président du conseil d'administration, membre du directoire ou directeur général d'une société anonyme, de gérant d'une société civile ;
- l'exercice de toute autres profession hormis les fonctions d'enseignement, de collaborateur de député ou d'assistant de sénateur, de suppléant de juge d'instance, de membre assesseur des tribunaux pour enfants ou des tribunaux paritaires de baux ruraux, de conseiller prud'homme, de membre des tribunaux des affaires de sécurité sociale, ainsi qu'avec celles d'arbitre, de médiateur, de conciliateur ou de séquestre.

*Pour aller plus loin* : articles 111 à 123 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

### c. Formation professionnelle continue

L'avocat doit mettre à jour ses connaissances et perfectionner ses compétences tout au long de sa carrière lors de formations continues annuelles.

La formation continue doit être d'au moins 20 heures par an ou 40 heures au cours de deux années consécutives.

L'avocat nouvellement inscrit au tableau du barreau devra également suivre 10 heures de déontologie au cours de ses deux premières années.

Ces formations doivent être déclarées auprès du Conseil de l'Ordre des avocats, au plus tard au 31 janvier de l'année civile écoulée. L'avocat devra joindre à cette déclaration tout justificatif attestant qu'il a participé à de telles formations.

*Pour aller plus loin* : articles 85 et 85-1 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

## 4°. Assurance et garantie financière

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

L'avocat exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

L'avocat devra être couvert de tout acte qu'il commettrait dans le cadre de son exercice à hauteur de 1 500 000 d'euros minimum.

*Pour aller plus loin* : articles 205 et 206 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

### b. Obligation de justifier d'une garantie financière

L'avocat doit justifier d'une garantie financière qui sera attribuée au remboursement des fonds reçus à l’occasion de son activité. Il s’agit d’un engagement de caution pris auprès d’une banque ou d’un établissement de crédit, une entreprise d’assurances ou une société de caution mutuelle. Son montant devra être au moins égal à celui des fonds qu'il envisage de détenir.

*Pour aller plus loin* : articles 210 à 225 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demander une reconnaissance de ses qualifications professionnelles au ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Procédure

Le Conseil national des barreaux est compétent pour se prononcer sur la demande de reconnaissance des qualifications professionnelles du ressortissant souhaitant s'établir en France pour y exercer la profession d'avocat.

#### Pièces justificatives

A l'appui de sa demande, le ressortissant devra envoyer un dossier à l'autorité compétente, comprenant :

- une pièce d'identité en cours de validité ;
- tout document justifiant qu'il a suivi un cycle d'études postsecondaires avec succès ;
- les copies des titres de formation ou tout document justifiant l'accès à la profession d'avocat, délivrés dans un État de l'UE ou de l'EEE ;
- une attestation justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice ;
- une attestation d'inscription de moins de trois mois de l'autorité compétente de l’État de l'UE ou de l'EEE dans lequel il a acquis son titre d'avocat.

**À savoir** 

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le Conseil national des barreaux disposera d'un délai d'un mois, à réception du dossier, pour informer le ressortissant de tout document manquant. Il aura ensuite trois mois pour se prononcer sur la demande ou soumettre le ressortissant à une mesure de compensation.

En cas de silence gardé, la demande sera réputée rejetée. Cependant, le ressortissant pourra saisir la cour d'appel de Paris pour demander le réexamen de sa demande.

*Pour aller plus loin* : articles 99 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

#### Bon à savoir : mesure de compensation

L'épreuve d'aptitude organisée en cas de différences substantielles entre la formation et l'expérience professionnelle du ressortissant avec celles exigées en France, consiste en une épreuve orale devant un jury. Le ressortissant devra réussir un exposé de dix minutes portant sur un sujet tiré au sort, ainsi qu'un entretien avec le jury, d'une vingtaine de minutes.

*Pour aller plus loin* : arrêté du 10 octobre 2017 fixant le programme et les modalités de l'examen d'aptitude prévu aux articles 204-2 et 204-3 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).