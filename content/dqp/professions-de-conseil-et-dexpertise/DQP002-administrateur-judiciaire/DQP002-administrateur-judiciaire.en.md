﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP002" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Insolvency practitioner" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="insolvency-practitioner" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/insolvency-practitioner.html" -->
<!-- var(last-update)="2020-04-15 17:20:58" -->
<!-- var(url-name)="insolvency-practitioner" -->
<!-- var(translation)="Auto" -->


Insolvency practitioner
======================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:58<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The insolvency practitioner is a professional who is entrusted by court order to administer the property of others or to exercise assistance or supervision functions in the management of these assets. As part of the prevention of business difficulties, the judicial administrator may be entrusted with mandate tasks*ad hoc* conciliation.

As part of the judicial treatment of the difficulties faced by companies, the judicial administrator intervenes in the procedures of safeguarding, judicial redress and even, in an exceptional way, in judicial liquidation in cases continuation of activity. In these three collective procedures, its designation is mandatory with failing companies with at least 20 employees and a turnover of 3 million euros.

The judicial administrator's mission is to draw up the economic, social and environmental assessment of the company and to develop a draft plan to safeguard or turnaround the company.

The administrator's mission is also:

- safeguarding, monitoring or assisting the debtor in managing the business;
- to assist the debtor in its management or to insure all or part of it in the debtor's place.

The judicial administrator may intervene as an interim administrator, on the decision of the court to manage a corporation and implement the necessary means to help him overcome his state of crisis. This mission, like others that the judicial administrator may also perform, is not specific to this profession.

*To go further* Article L. 811-1 of the Code of Commerce;[official website of the National Council of Judicial Directors and Agents](https://www.cnajmj.fr/fr/).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Judicial administrators are placed on a national list drawn up by the[National Registration and Discipline Commission for insolvency practitioners and Judicial Agents](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006242221&dateTexte=&categorieLien=cid).

To be registered, the person must meet the following conditions:

- Be French or a national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA);
- not having been criminally convicted for acts contrary to honour or probity;
- have not been subject to disciplinary or administrative sanctions for dismissal, dismissal, revocation, withdrawal of accreditation or authorization;
- not have been subjected to personal bankruptcy or any of the forfeiture or prohibition measures provided for by collective proceedings legislation.

*To go further* Articles L. 811-2 and L. 811-5 of the Code of Commerce.

#### Training

There are several ways to access the profession of judicial administrator:

- hold the diplomas under Articles R. 811-7 and R. 811-8 of the Code of Commerce and pass the Professional Internship Access Exam, complete a three- to six-year paid work placement, and then pass the Aptitude to Work Exam insolvency practitioner;
- hold a master's degree in administration and liquidation of troubled companies and justify:- at least five years of professional experience as an employee of a judicial administrator,
  - or at least eight years of professional accounting, legal or financial practice in the field of administration, financing, restructuring, including mergers and acquisitions, or the takeover of companies, particularly in difficulty,
  - or have completed a professional internship of at least thirty months in a judicial administrator's study on the list of the National Registration and Discipline Commission;

**Good to know: exam and internship exemptions**

Some exemptions may be granted to the person who wishes to become a judicial administrator, depending on his qualifications, professional experience and/or the duties he may have performed.

Persons who justify having acquired in a Member State of the European Union other than France or another State party to the agreement on the European Economic Area, a sufficient qualification for the profession of judicial administrator conditions of diploma, internship and professional examination are exempt, subject to having undergone a knowledge check-up in some cases (training in substantially different subjects or unregulated activity Member State of origin).

This qualification may result from:

- the justification for successfully completing a post-secondary education cycle of a minimum of three years or of an equivalent period of part-time education at a university or higher education institution or in another institution of a equivalent level of training, and, if applicable, the vocational training required in addition to this cycle of study;
- justification for a full-time exercise of the profession for at least two years in the previous ten years in a Member State or party that does not regulate access or the exercise of the profession of judicial administrator, provided that this be certified by the competent authority of that state. This condition of two-year professional experience is not required when the holding or the titles held sanction regulated training directly oriented towards the practice of the profession.

*To go further* Articles L. 811-5 last paragraph and R. 811-27 of the Code of Commerce.

In addition,:

- examination of access to the internship:- judicial agents who have practiced their profession for at least three years,
  - lawyers, notaries, judicial auctioneers, judicial officers, commercial court clerks, accountants and auditors, who have practiced their profession for at least five years,
  - persons holding one of the titles or diplomas mentioned in Article R. 811-7 of the Code of Commerce, justifying at least five years of professional accounting, legal or financial practice in the area of administration, financing, restructuring, including mergers and acquisitions, or the takeover of companies, particularly in difficulty,
  - Persons who have served as collaborators of a judicial administrator for a period of five years;
- professional internship:- judicial agents, lawyers, notaries, judicial auctioneers, judicial officers, clerks of commercial courts, former solicitors, accountants, auditors, having practiced their profession for ten years in the Least
  - employees of a judicial administrator who has worked for at least ten years,
  - those with the diplomas required for the pathway through the examination of access to the professional internship ([Article R. 811-7 of the Code of Commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) justifying a professional practice of at least fifteen years in the accounting, legal, financial or administrative, financing, restructuring or takeover fields;
- all the tests in the aptitude exam:- Judicial agents who have practiced their profession for at least five years and have completed the professional internship;
- civil and criminal law test of the fitness examination cases:- lawyers, notaries, judicial auctioneers, judicial officers, commercial court clerks,
  - those with the diplomas required for access to the profession through the examination of access to the professional internship ([Article R. 811-7 of the Code of Commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) that justify at least five years of professional legal practice in the field of administrator, financing restructuring or taking over companies;
- The aptitude test for the management of a judicial administrator's office:- persons who have served for at least five years as an employee of a judicial administrator,
  - accountants and auditors,
  - those with the diplomas required for access to the profession through the examination of access to the professional internship ([Article R. 811-7 of the Code of Commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) which justify at least five years of professional accounting or financial practice, in the field of administration, financing, restructuring, including mergers and acquisitions, or the takeover of companies, particularly in difficulty.

Finally, the duration of the professional internship is reduced to one year for:

- judicial agents, lawyers, notaries, judicial auctioneers, judicial officers, commercial court clerks, former solicitors, accountants, auditors who have exercised their profession for at least five years;
- people holding one of the titles or diplomas mentioned in the[Article R. 811-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid) code of commerce and justifying at least ten years of professional accounting, legal or financial practice, in the area of administration, financing, restructuring, including mergers and acquisitions, or the takeover of companies, particularly in trouble

*To go further* : Articles R. 811-7, R. 811-8, R. 811-24, R. 811-25, R. 811-26, R. 811-27, R. 811-28, R. 811-28-1 and R. 811-28-2 of the Code of Commerce.

#### Related costs

The entrance exams and the aptitude are free.

*To go further* Article R. 811-16 of the Code of Commerce.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A person from the EU or the EEA who wishes to temporarily and occasionally practise the profession of judicial administrator must meet the conditions specified above and carry out the registration procedures on the national list national registration and disciplinary commission.

*To go further* Article L. 811-5 of the Code of Commerce.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any EU or EEA national can enter the profession through traditional channels.

In addition, a national of an EU or EEA country with a minimum of three years' graduate degree may enter the profession to practice permanently if:

- he holds diplomas, certificates or other titles acquired in an EU or EEA Member State justifying a sufficient qualification for the profession of judicial administrator (study cycle of at least three years or equivalent) (Article R. 811-27 1) of the Code of Commerce);
- or if he has worked full-time for two years in the last ten years in a Member State which does not regulate access to the profession or its exercise, provided that this exercise is certified by the competent authority of that State (Article R. 811-27 2o code of commerce).

Once he fulfils one of these conditions, he may apply for registration on the list of judicial administrators with the National Commission for registration and discipline of judicial administrators and judicial agents (cf. infra "5°. a. Request registration on the list of judicial administrators for the national for a permanent exercise (LE)).

If the examination of professional qualifications shows substantial differences in relation to those required for access and practice of the profession in France, the person concerned may be subject to a knowledge check.

*To go further* Articles L. 811-5, R. 811-27 and R. 811-28 of the Code of Commerce.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Exercise incompatibility

Judicial administrators may operate individually or in any form of company. However, the legal form chosen should not confer on its partners the status of trader.

With the exception of a lawyer, the practice of judicial administrator is incompatible with:

- all commercial activities, whether carried out directly or by person interposed;
- the status of partner or manager of certain companies (Article L. 811-10 of the Code of Commerce)

*To go further* Articles L. 811-7 and L. 811-10 of the Code of Commerce.

4°. Continuing education and insurance
----------------------------------------------------------

### a. Obligation to undergo continuing vocational training

The judicial administrator is required to undergo continuous professional training. This training is defined by the National Council of insolvency practitioners for 20 hours in a calendar year or 40 hours in two consecutive years and is intended to maintain and update the administrator's knowledge. accounting, tax and legal.

*To go further* Articles L. 814-9, L. 814-10, R. 814-28-1 and the following Code of Commerce.

### b. Obligation to register with the insolvency practitioners Guarantee Fund

The judicial administrator is required to subscribe to a guarantee fund managed by the contributors themselves. This fund must guarantee the reimbursement of funds, effects or values received by each director as part of his missions.

On the other hand, when the director is employed, it is only optional and it is up to his employer to subscribe to such a guarantee.

*To go further* Article L. 814-3 of the Code of Commerce.

### c. Obligation to take out professional liability insurance

The judicial administrator must take out professional liability insurance covering the financial consequences resulting from his activity.

However, if he is an employee, it is up to the employer to take out such insurance.

*To go further* Article L. 814-4 of the Code of Commerce.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request registration on the list of judicial administrators for the national for a permanent exercise (LE)

**Competent authority**

The National Commission for the Registration and Discipline of insolvency practitioners and Judicial Agents has the authority to decide on the application for registration of the national (National Commission for Registration and Discipline of judicial administrators and judicial agents, 13 Place Vendôme, 75042 PARIS Cedex 01 (FRANCE)).

**Supporting documents**

The national must submit the application for registration to the Commission secretariat by a recommended letter with notice of receipt. This is in the form of a file with the following supporting documents:

- Applying for registration
- documents establishing the applicant's marital status and nationality;
- A copy of the titles and diplomas which he intends to avail himself of or, failing that, a certificate from the authorities authorized to issue them;
- If so, the certificate of success in the aptitude test;
- If so, any exemptions requested and the corresponding supporting documents;
- If so, the certificate of success in the knowledge check examination;
- The applicant also indicates his or her previous professional activities and the place where he plans to establish his or her professional residence;
- any useful document indicating that the conditions of Article L. 811-5 of the Code of Commerce (no conviction/sanction or prohibition) are met.

Documents must be forwarded to the following mailing address:

> Ministry of Justice<br>
> National Registration and Discipline Commission for insolvency practitioners and Judicial Officers<br>
> 13 Place Vendôme<br>
> 75042 PARIS Cedex 01 (FRANCE)

**Good to know**

Some specific documents are required when the practice of the profession is envisaged in social form (see in particular the[Articles R. 814-60 and the following of the Code of Commerce](https://www.legifrance.gouv.fr/affichCode.do;jsessionid=235FB1B419B02405DFBE8202B5B7EE09.tplgfr24s_2?idSectionTA=LEGISCTA000006191119&cidTexte=LEGITEXT000005634379&dateTexte=20190723).

**Procedure**

Once the National Commission has received the full file, it must seek the opinion of the National Council of insolvency practitioners and Judicial Agents (CNAJMJ), which has one month to decide. If the National Commission considers that there are substantial differences between the training or professional experience of the national and those required in France, it may submit it to a knowledge check.

In all cases, the Commission will not be able to make its decision on whether or not to put the national on the list or to ask for further information until it has received the notice of the CNAJMJ, within four months of receiving the file.

*To go further* Articles R. 811-27, R. 811-29 and the following of the Code of Commerce.

### b. Remedies

Appeals against the decision of the National Registration and Disciplinary Commission of Judicial Directors and Agents may be exercised before the Paris Court of Appeal within one month of the date of receipt of the letter of notification of the decision. The appeal is made either by declaration to the Registry of the Court of Appeal or by letter recommended with request for notice of receipt addressed to the Chief Clerk of that Court.

Representation by a lawyer is not mandatory.

*To go further* Articles R. 811-33, R. 811-35, R. 814-2 of the Code of Commerce

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of an EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

