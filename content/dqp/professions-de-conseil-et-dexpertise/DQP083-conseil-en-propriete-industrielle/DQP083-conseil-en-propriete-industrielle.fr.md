﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP083" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Conseil en propriété industrielle" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="conseil-en-propriete-industrielle" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/conseil-en-propriete-industrielle.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="conseil-en-propriete-industrielle" -->
<!-- var(translation)="None" -->

# Conseil en propriété industrielle

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le conseil en propriété industrielle est un professionnel qui a pour mission de conseiller, assister ou représenter ses clients en vue de l'obtention, du maintien, de l'exploitation ou de la défense des droits de propriété industrielle, droits annexes et droits portant sur toute question connexe.

Le conseil en propriété industrielle est également compétent pour fournir des consultations juridiques, portant en particulier sur la propriété industrielle, et rédiger des actes sous seing privé.

Il assure également une mission de représentation de ses clients dans les procédures devant l’[Institut national de la propriété industrielle](https://www.inpi.fr/fr) (Inpi).

*Pour aller plus loin* : articles L. 422-1 et L. 422-4 du Code de la propriété intellectuelle.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Le titre de conseil en propriété industrielle est réservé aux professionnels inscrits sur une liste des conseils en propriété industrielle, tenue par le directeur général de l'Inpi.

**À noter**

L’usurpation du titre de conseil en propriété industrielle ou d’un titre susceptible de prêter à confusion, est punie d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : article L. 422-1 du Code de la propriété intellectuelle et article 433-17 du Code pénal.

#### Conditions d’accès à la profession

Pour accéder à la profession de conseil en propriété industrielle en France, l'intéressé doit justifier :

- d’un diplôme de deuxième cycle juridique, scientifique ou technique, ou d’un titre reconnu comme équivalent ;
- d’un diplôme délivré par le [Centre d'études internationales de la propriété industrielle](http://www.ceipi.edu/) (Ceipi) de l'université de Strasbourg ou d'un titre reconnu comme équivalent ;
- une pratique professionnelle d'au moins 3 ans ;
- la réussite à un examen d’aptitude ou la validation d’expérience.

**À noter**

Le Ceipi propose deux formations différentes : l'une portant sur les brevets d'invention et conduisant au diplôme d'études internationales de la propriété industrielle en « Brevets d'Invention », et l'autre sur les marques, dessins et modèles conduisant au diplôme d'études internationales de la propriété industrielle en « Marques, Dessins et Modèles ». Pour plus d'information, se renseigner directement auprès du [Ceipi](http://www.ceipi.edu/).

#### Examen d’aptitude

L'examen d’aptitude en langue française organisé par l’Inpi comprend des épreuves écrites d’admissibilité et des épreuves orales. L’examen doit permettre d’établir s’il possède bien les connaissances théoriques et pratiques pour exercer les missions d’assistance, de conseil et de représentation en matière de propriété industrielle.

##### Procédure

L’inscription à l’examen d’aptitude est une demande présentée sous la forme d’une lettre datée et signée par le candidat, comportant son adresse personnelle et le secteur technique choisi pour l’épreuve orale.

##### Pièces justificatives

La lettre de demande d’inscription à l’examen doit être accompagnée :

- d’une copie de la pièce d’identité du candidat en cours de validité ;
- d’une copie du diplôme de deuxième cycle juridique, scientifique ou technique ou d’un titre reconnu comme équivalent ;
- d’une copie du diplôme délivré par le Centre d'études internationales de la propriété industrielle (Ceipi) de l'université de Strasbourg ou d'un titre reconnu comme équivalent ;
- d’un ou plusieurs certificats attestant d’une pratique professionnelle de trois ans minimum, décrivant leur durée et les fonctions exercées par le candidat.

L’expérience professionnelle doit avoir été réalisée sous la responsabilité d’une personne qualifiée en propriété industrielle.

##### Coût

Le candidat devra s’acquitter d’une participation aux frais fixée à 200 euros auprès de l’agent comptable de l’Inpi.

##### Issue de la procédure

Pour valider et obtenir l’examen d’aptitude, l’intéressé doit avoir obtenu une moyenne supérieure ou égale à 10 sur 20 à l’ensemble des épreuves.

#### Validation de l’expérience

Le cas échéant, le candidat sera dispensé d’examen d’aptitude s’il justifie d'une expérience professionnelle de huit années minimum sous la responsabilité d’une personne qualifiée en matière de propriété industrielle.

Il devra en faire la demande expresse auprès du directeur général de l’Inpi et sera tenu de passer un entretien oral devant un jury, après étude de sa candidature.

**À noter**

Le jury comprend un magistrat de l'ordre judiciaire, un professeur d'université enseignant le droit privé, un avocat, deux personnes compétentes en propriété industrielle et quatre personnes inscrites sur la liste des personnes qualifiées en propriété industrielle, dont deux conseils en propriété industrielle.

*Pour aller plus loin* : articles R. 421-1 et R. 421-6, portant sur les membres du jury à l’examen d’aptitude, R. 422-1-1, R. 422-1-2 et R. 421-2 du Code de la propriété intellectuelle.

#### Coûts associés à la qualification

La formation préparant à l’obtention des diplômes menant à la profession de conseil en propriété industrielle est payante. Son coût varie selon les universités qui dispensent les enseignements. Pour plus de précisions, il est conseillé de se rapprocher des universités considérées.

### b. Ressortissants UE : en vue d’un exercice temporaire ou occasionnel (LPS)

Le ressortissant d’un État de l’Union européenne (UE) ou de l’Espace économique européen (EEE), exerçant les activités de conseil en propriété industrielle dans l’un de ces États, peut faire usage de son titre professionnel pour représenter des personnes devant l’Inpi, dès lors que son titre est attesté par l’autorité compétente de l’État dans lequel il est légalement établi.

Lorsque l’exercice de la profession n’est pas soumis à la possession d’un titre réglementé dans l’État d’établissement, l’intéressé ne peut représenter des personnes devant l’Inpi que sous réserve de justifier :

- soit d’une pratique de la profession de conseil en propriété industrielle dans un ou plusieurs États membres de l’UE ou partie à l’EEE, d’une année au moins durant les dix dernières années, à temps plein ou à temps partiel. La justification de cette pratique peut se faire par tout moyen ;
- soit d’attester d’une formation réglementée dans son État d’établissement et donnant accès à la profession.

En tout état de cause, le ressortissant intervenant dans la représentation de personnes devant l’Inpi, sera tenu de respecter les règles professionnelles applicables à la profession de conseil en propriété industrielle en France, et notamment l’ensemble des règles déontologiques (cf. infra « 3°. Conditions d’honorabilité et règles déontologiques »).

En cas de manquement à ses obligations, les sanctions prévues par l’article L. 422-10 trouveront à s’appliquer (cf. infra « 3°. a. Conditions d’honorabilité »).

*Pour aller plus loin* : articles R. 422-7-1 et R. 422-7-2 du Code de la propriété intellectuelle.

### c. Ressortissants UE : en vue d’un exercice permanent (LE)

Le ressortissant d’un État de l’UE ou de l’EEE souhaitant exercer à titre permanent la profession de conseil en propriété industrielle en France peut demander son inscription sur la liste des personnes qualifiées en propriété industrielle (cf. infra « 5°. Démarche et formalités de reconnaissance de qualification »).

À ce titre, le titulaire d’un titre de conseil en propriété industrielle délivré dans l’un de ces États peut faire valoir ses qualifications professionnelles et être dispensé des conditions d’accès à la profession précitées (cf. supra « 2°. a. Conditions d’accès à la profession »).

Dans ce cas, la personne concernée devra avoir suivi avec succès un cycle d’études et justifier :

- soit d'un diplôme, certificat ou autre titre permettant l'exercice de la profession dans un État de l’UE ou de l’EEE délivrés :
  - par l’autorité compétente de cet État,
  - ou par une autorité d'un pays tiers, accompagné d’une attestation émanant de l'État de l’UE ou de l'EEE certifiant que son titulaire a acquis sur son territoire, une expérience professionnelle de trois ans au moins, à temps plein ou à temps partiel ;
- soit de l'exercice de la profession, à temps plein ou à temps partiel, pendant une année au cours des dix années précédentes, dans un État de l’UE ou de l’EEE qui ne réglemente pas l'accès ou l'exercice de cette profession. L’intéressé devra fournir toutes les attestations de compétence ou preuves de titres de formation préparant à l’exercice de la profession.

**À noter**

Si le demandeur certifie une formation réglementée, l’expérience professionnelle n’est pas requise.

En cas de différences substantielles entre les qualifications professionnelles acquises et celles requises en France, le ressortissant pourra être soumis à des mesures de compensation (cf. infra « Bon à savoir : mesure de compensation ») prévues par l’article R. 421-8 du Code de la propriété intellectuelle.

*Pour aller plus loin* : article R. 421-7 du Code de la propriété intellectuelle.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

L’ensemble des devoirs s’imposant aux conseils en propriété industrielle s’applique aux ressortissants souhaitant exercer la profession en France.

### a. Conditions d’honorabilité

Pour pouvoir exercer, le conseil en propriété industrielle ne doit pas :

- s’être rendu coupable d'une infraction aux règles liées à la condition d’exercice du conseil en propriété industrielle ;
- avoir realisé des actes contraires à la probité, à l'honneur ou à la délicatesse.

En application de l’article L. 422-10 du Code de la propriété intellectuelle, des mesures disciplinaires pourront être prononcées par la chambre de discipline de la [Compagnie nationale des conseils en propriété industrielle](https://www.cncpi.fr/) (CNCPI), à l’encontre du professionnel qui ne respecterait pas les dispositions énoncées, à savoir :

- un avertissement ;
- un blâme ;
- une radiation temporaire ou définitive. À noter : cette mesure disciplinaire est remplacée par une mesure d’interdiction temporaire ou définitive d’exercer pour le ressortissant de l’UE ou de l’EEE qui souhaiterait exercer en France.

*Pour aller plus loin* : articles L. 422-10 et R. 422-7-2 du Code de la propriété intellectuelle.

### b. Déontologie

Le conseil en propriété industrielle doit respecter notamment les principes d’indépendance, de confidentialité absolue des correspondances, de préservation des risques de conflit d’intérêt et de secret professionnel.

*Pour aller plus loin* : articles L. 422-11, R. 422-7-2, R. 422-52 et R. 422-54 du Code de la propriété intellectuelle.

### c. Incompatibilité d’exercice

La profession de conseil en propriété industrielle est incompatible avec :

- toute activité à caractère commerciale ;
- la qualité d’associé ou de gérant de société, sauf si cette société a pour objet le conseil en propriété industrielle ;
- la qualité de membre de conseil de surveillance ou d'administrateur d'une société commerciale, à moins que le conseil en propriété industrielle ait plus de sept années d'exercice professionnel et obtenu préalablement une dispense dans des conditions fixées par décret en Conseil d'État.

La profession de conseil en propriété industrielle est en revanche compatible avec les fonctions d'enseignement, ainsi qu'avec celles d'arbitre, de médiateur, de conciliateur ou d'expert judiciaire.

*Pour aller plus loin* : articles L. 422-12 et L. 422-13 du Code de la propriété intellectuelle.

### d. Obligation de formation professionnelle continue

Les conseils en propriété industrielle doivent participer à une formation professionnelle continue prévue à l'article L. 422-10-1 du Code de la propriété intellectuelle. Cette formation vise à assurer la mise à jour et le perfectionnement des connaissances nécessaires à l'exercice de la profession.

L'obligation de formation continue est satisfaite si le conseil en propriété industrielle justifie l’une des actions suivantes :

- participation à des formations, à caractère juridique, économique ou professionnel ;
- participation à des formations dispensées par des conseils en propriété industrielle ou par des personnes physiques ou morales établies sur le territoire d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) et habilitées à représenter les personnes devant le service central de la propriété industrielle de cet État ;
- présence à des colloques ou à des conférences à caractère juridique ou économique ayant un lien avec l'activité professionnelle de conseil en propriété industrielle ;
- dispense d'enseignements ayant un lien avec l'activité professionnelle de conseil en propriété industrielle dans un cadre universitaire ou professionnel ;
- publication de travaux à caractère juridique ou économique ayant un lien avec l'activité professionnelle de conseil en propriété industrielle.

Toute formation continue suivie par un ressortissant d’un État de l’UE ou de l’EEE dans l’un de ces États est réputée satisfaire à cette obligation.

#### Durée

La durée de la formation continue est de vingt heures au cours d'une année civile ou de quarante heures au cours de deux années consécutives. Au cours des deux premières années d'exercice professionnel, cette formation inclut dix heures au moins portant sur la gestion d'un cabinet, la déontologie et le statut professionnel.

#### Procédure

Les conseils en propriété industrielle déclarent, au plus tard le 31 janvier de chaque année, auprès de la CNCPI les actions accomplies aux fins de satisfaire leur obligation de formation continue au cours de la dernière année civile écoulée ou, le cas échéant, des deux dernières années écoulées.

Les justificatifs utiles à la vérification du respect de cette obligation sont joints à cette déclaration.

La CNCPI contrôle l'accomplissement effectif de l'obligation de formation professionnelle continue des conseils en propriété industrielle et vérifie la conformité des formations suivies et des actions menées, en particulier leur lien avec l'activité de conseil en propriété industrielle.

*Pour aller plus loin* : articles L. 422-10-1 et R. 422-55-1 du Code la propriété intellectuelle

## 4°. Assurance et garantie

Tout conseil en propriété industrielle doit justifier d'une assurance de responsabilité civile professionnelle garantissant les négligences et fautes qu’il pourrait commettre vis-à-vis de ses clients dans l'exercice de ses fonctions. Outre la souscription d'une assurance responsabilité civile professionnelle, le conseil en propriété industrielle doit disposer d'une garantie spécialement affectée au remboursement des fonds, effets ou valeurs reçus.

Les justificatifs d’assurance et de garantie doivent être transmis chaque année à l’Inpi.

*Pour aller plus loin* : article L. 422-8 du Code de la propriété intellectuelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### Le cas échéant, demander son inscription sur la liste de personnes qualifiées en matière de propriété industrielle

Les ressortissants de l'UE ou de l'EEE qui souhaitent exercer la profession de conseil en propriété industrielle sont tenus d’être inscrits sur une liste des personnes qualifiées en propriété industrielle pour l’exercice de leur activité.

L’inscription est soumise à une obligation de bonne moralité et de respect des conditions de diplôme et de pratique professionnelle. Elle précise la spécialisation du conseil en propriété industrielle selon les diplômes qu’il a obtenus et la pratique professionnelle qu’il a acquise.

#### Autorité compétente

Le directeur de l’Inpi, après avis de la CNCPI, dresse annuellement la liste des personnes qualifiées en propriété industrielle.

#### Pièces justificatives

À l'appui de sa demande d'inscription, le ressortissant doit fournir les justificatifs nécessaires de diplômes et de pratiques professionnelles précisés supra au paragraphe « 2°. b. Ressortissants UE : en vue d’un exercice permanent (LE) ».

**À savoir**

Le ressortissant qui souhaite être inscrit sur la liste ne doit pas :

- avoir fait l’objet d’une condamnation pénale ;
- être frappé d’une sanction disciplinaire ou administrative de destitution, radiation, révocation, de retrait d'agrément ou d'autorisation, pour des faits de même nature ;
- être frappé de faillite personnelle ou d’une sanction de même type.

Le cas échéant, le directeur général de l’Inpi pourra décider de le radier de la liste, par decision motivée, sous réserve que l’intéressé ait pu, au préalable, présenter ses observations.

**À noter**

Pour obtenir cette inscription sur la liste, le ressortissant peut être amené à effectuer une mesure de compensation (cf. infra « Bon à savoir : mesure de compensation) sous la forme d’un examen d’aptitude en langue française, s’il s’avère que les qualifications ou l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France.

#### Issue de la procédure

Le directeur de l’Inpi se prononcera sur la demande d’inscription après décision du jury dans un délai de quatre mois à compter de son dépôt.

Toutefois, en cas de non-conformité de la demande, il sera demandé à l’intéressé de la régulariser ou de contester les objections faites par l’Inpi, ce qui aura pour effet de suspendre ce délai.

Le silence gardé par le directeur général de l’Inpi à l’expiration du délai de quatre mois vaut acceptation de la demande.

*Pour aller plus loin* : articles L. 422-5 et R. 421-7 et suivants du Code la propriété intellectuelle.

#### Bon à savoir : mesure de compensation

Après examen des pièces justificatives, le directeur général de l’Inpi, compétent pour établir l’inscription sur la liste des personnes qualifiées en matière de PI, indique au ressortissant le niveau de qualification professionnelle requis pour exercer en France et les différences substantielles justifiant le recours à une mesure de compensation.

Cette mesure prend la forme d’un examen d’aptitude devant le jury mentionné supra au paragraphe « 2°. a. Exigences nationales ».

L’examen, dont les modalités et le programme sont fixés par arrêté conjoint du garde des Sceaux, ministre de la Justice, et du ministre chargé de la propriété industrielle, est organisé dans un délai de six mois à compter de la notification de la décision du directeur de l’Inpi à l’intéressé.

#### Coût

Gratuit.

*Pour aller plus loin* : article R.421-8 du Code de la propriété intellectuelle.

### Demander son inscription sur la liste des conseils en propriété industrielle

Une fois inscrit sur la liste des personnes qualifiées en propriété industrielle, le ressortissant peut demander son inscription sur la liste des conseils en propriété industrielle mentionnée à l’article R. 422-1 du Code de la propriété industrielle, à la condition :

- d’offrir ou de s’engager à offrir ses services dans un délai de trois mois soit à titre individuel ou en groupe, soit comme salarié d’un autre conseil en propriété industrielle ou auprès d’une société de conseils en propriété industrielle ;
- d'être de nationalité française ou ressortissant d’un État de l’UE ou de l’EEE ;
- d'avoir un domicile ou un établissement en France ;
- de remplir les conditions d’assurance et de garanties prévues infra au paragraphe « 4. Assurance et garantie ».

#### Autorité compétente

La demande d’inscription sur la liste des conseils en propriété industrielle est adressée au directeur général de l’Inpi.

#### Procédure

Dans un délai de quatre mois à compter de la réception de la demande d’inscription, le directeur général de l’Inpi rendra une décision motivée prise après avis de la CNCPI. À noter que l’avis est réputé écrit en cas de silence gardé de la CNCPI dans un délai d'un mois à compter de sa saisine.

En cas de non-conformité de la demande, le délai sera suspendu jusqu’à ce que le demandeur la régularise ou conteste les objections faites par l’Inpi.

À défaut de notification dans ce délai, la demande d’inscription est réputée acceptée.

#### Issue de la procédure

Une fois la demande acceptée, le directeur général de l’Inpi procède à l'inscription en indiquant :

- le nom du conseil en propriété industrielle ;
- la dénomination du cabinet ou la raison sociale de la société au sein duquel ou de laquelle il exerce.

#### Coût

Gratuit.

*Pour aller plus loin* : articles R. 422-1 et suivants du Code de la propriété intellectuelle.

### En cas d’exercice sous la forme d’une société civile professionnelle (SCP) ou d’une société d’exercice libéral (SEL), demander l’inscription de la société à l’Inpi

Si l’intéressé souhaite exercer sous forme d’une SCP ou d’une SEL, il doit inscrire cette société sur la liste dressée par le directeur général de l’Inpi, outre l’inscription obligatoire des conseils en propriété industrielle personnes physiques qui la composent.

*Pour aller plus loin* : article L.422-7 du Code la propriété intellectuelle.

### Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).