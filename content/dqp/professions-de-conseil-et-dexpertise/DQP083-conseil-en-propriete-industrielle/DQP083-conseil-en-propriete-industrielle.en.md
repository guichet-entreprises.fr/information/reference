﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP083" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Patent attorney" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="patent-attorney" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/patent-attorney.html" -->
<!-- var(last-update)="2020-04-15 17:21:04" -->
<!-- var(url-name)="patent-attorney" -->
<!-- var(translation)="Auto" -->


Patent attorney
==========================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:04<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

Industrial property consulting is a professional who is responsible for advising, assisting or representing clients in obtaining, maintaining, operating or defending industrial property rights, ancillary rights and rights. related issues.

The Industrial Property Council is also responsible for providing legal consultations, particularly on industrial property, and drafting deeds under private seing.

It also carries out a mission to represent its clients in the proceedings before the[National Institute of Industrial Property](https://www.inpi.fr/fr) (Inpi).

*For further information*: Articles L. 422-1 and L. 422-4 of the Intellectual Property Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The title of industrial property consultancy is reserved for professionals on a list of industrial property councils, held by the Director General of Inpi.

**Please note**

The usurpation of the title of industrial property council or a title likely to be confusing, is punishable by one year's imprisonment and a fine of 15,000 euros.

*For further information*: Article L. 422-1 of the Intellectual Property Code and Article 433-17 of the Penal Code.

#### Conditions of access to the profession

In order to enter the industrial property consulting profession in France, the person concerned must justify:

- a postgraduate degree in legal, scientific or technical, or a title recognized as equivalent;
- a diploma issued by the[Centre for International Industrial Property Studies](http://www.ceipi.edu/) (Ceipi) from the University of Strasbourg or a title recognized as equivalent;
- a professional practice of at least 3 years
- passing an aptitude exam or validating an experience.

**Please note**

The Ceipi offers two different courses: one on patents of invention and leading to the diploma of international studies of industrial property in "Invention Patents", and the other on the marks, designs leading to the international property studies degree in "Brands, Designs and Models." For more information, find out directly from the[Ceipi](http://www.ceipi.edu/).

#### Aptitude exam

The French language aptitude exam organized by Inpi includes written eligibility and oral tests. The review should determine whether he has the theoretical and practical knowledge to carry out the missions of assistance, advice and representation in industrial property.

**Procedure**

Registration for the aptitude test is an application in the form of a letter dated and signed by the applicant, including his personal address and the technical area chosen for the oral test.

**Supporting documents**

The letter of application for registration for the exam must be accompanied by:

- A copy of the candidate's valid ID
- A copy of the legal, scientific or technical graduate degree or a title recognized as equivalent;
- a copy of the diploma issued by the Centre for International Industrial Property Studies (Ceipi) at the University of Strasbourg or a title recognised as equivalent;
- one or more certificates attesting to a minimum of three years of professional practice, describing their duration and the duties performed by the candidate.

The professional experience must have been carried out under the responsibility of a qualified person in industrial property.

**Cost**

The candidate will have to pay a fee set at 200 euros with the accounting officer of the Inpi.

**Outcome of the procedure**

To validate and obtain the aptitude test, the individual must have obtained an average of 10 out of 20 on all the tests.

**Validating the experience**

If this is the case, the applicant will be exempt from an aptitude test if he or she has a minimum of eight years of professional experience under the responsibility of a qualified person in industrial property.

He will have to make an express request to the Director General of Inpi and will be required to give an oral interview to a jury, after considering his application.

**Please note**

The jury includes a magistrate of the judicial order, a university professor teaching private law, a lawyer, two competent persons of industrial property and four persons on the list of qualified persons in property two industrial property councils.

*For further information*: Articles R. 421-1 and R. 421-6, relating to the jury members on the aptitude test, R. 422-1-1, R. 422-1-2 and R. 421-2 of the Intellectual Property Code.

**Costs associated with qualification******

Training to prepare for graduation leading to the industrial property consulting profession is paid for. Its cost varies depending on the universities that provide the teachings. For more details, it is advisable to get closer to the universities considered.

### b. EU nationals: for temporary or casual exercise (LPS)

A national of a European Union (EU) or European Economic Area (EEA) state, engaged in industrial property advisory activities in one of these states, may use his or her professional title to represent persons Inpi, as long as its title is attested by the competent authority of the state in which it is legally established.

Where the practice of the profession is not subject to the possession of a regulated title in the State of the Establishment, the person concerned may only represent persons before the Inpi provided that he or she justifies:

- either a practice of the industrial property consultancy profession in one or more EU Member States or party to the EEA, for at least one year in the last ten years, full-time or part-time. The justification for this practice can be done by any means;
- or to certify regulated training in its state of establishment and giving access to the profession.

In any event, the national involved in the representation of persons before the Inpi, will be obliged to respect the professional rules applicable to the industrial property consulting profession in France, and in particular all ethical rules (See infra "3°.3. Conditions of honorability and ethical rules").

In the event of a breach of its obligations, the sanctions under Article L. 422-10 will apply (See infra "3°. a. Conditions of honorability").

*For further information*: Articles R. 422-7-1 and R. 422-7-2 of the Intellectual Property Code.

### c. EU nationals: for a permanent exercise (LP)

A national of an EU or EEA state wishing to practise on a permanent basis in the industrial property consultancy profession in France may apply for registration on the list of qualified persons in industrial property (see infra "5°. Qualification process and formalities").

As such, the holder of an industrial property advisory designation issued in one of these states may assert his professional qualifications and be exempted from the above-mentioned conditions of access to the profession (see supra "2." b. Conditions of access to the profession").

In this case, the person concerned must have successfully completed a cycle of studies and justify:

- either a diploma, certificate or other title allowing the practice of the profession in an EU or EEA state issued:- by the competent authority of that state,
  - or by an authority of a third country, accompanied by a certificate from the EU State or the EEA certifying that the holder has acquired at least three years of professional experience on his territory, full-time or part-time;
- either full-time or part-time, for a year in the previous ten years, in an EU or EEA state that does not regulate access or practice. The individual will be required to provide all certificates of competency or proof of training credentials preparing for the practice of the profession.

**Please note**

If the applicant certifies regulated training, work experience is not required.

In the event of substantial differences between the professional qualifications acquired and those required in France, the national may be subject to compensation measures (see below: "Good to know: compensation measure") provided by Article R. 421-8 of the Intellectual Property Code.

*For further information*: Article R. 421-7 of the Intellectual Property Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

All the duties imposed on industrial property councils apply to nationals wishing to practice the profession in France.

### a. Conditions of honorability

In order to practice, the industrial property council does not have to:

- Offending the rules related to the condition of the practice of industrial property counsel;
- have carried out acts contrary to probity, honour or delicacy.

Under Article L. 422-10 of the Intellectual Property Code, disciplinary action may be taken by the[National Industrial Property Consulting Company](https://www.cncpi.fr/) (CNCPI), against the professional who does not comply with the stated provisions, namely:

- A warning
- Blame
- temporary or permanent write-off. Note: this disciplinary measure is replaced by a temporary or permanent ban on practising for THE EU or EEA national who wishes to practise in France.

*For further information*: Articles L. 422-10 and R. 422-7-2 of the Intellectual Property Code.

### b. Ethics

The industrial property council must respect, among other things, the principles of independence, absolute confidentiality of correspondence, the preservation of the risks of conflict of interest and professional secrecy.

*For further information*: Articles L. 422-11, R. 422-7-2, R. 422-52 and R. 422-54 of the Intellectual Property Code.

### c. Exercise incompatibility

The industrial property consulting profession is incompatible with:

- any business activity
- The status of partner or company manager, unless the company is for industrial property advice;
- supervisory board member or director of a commercial corporation, unless the industrial property board has more than seven years of professional practice and obtained an exemption in advance under conditions set by decree in the Council of State.

On the other hand, the profession of industrial property consulting is compatible with the teaching functions, as well as those of arbitrator, mediator, conciliator or judicial expert.

*For further information*: Articles L. 422-12 and L. 422-13 of the Intellectual Property Code.

### d. Continuing vocational training obligation

Industrial property councils must participate in continuing professional training under Section L. 422-10-1 of the Intellectual Property Code. The purpose of this training is to ensure that the knowledge needed to practice the profession is updated and developed.

The continuing education requirement is met if the industrial property council justifies one of the following actions:

- participation in training, legal, economic or professional;
- participation in training provided by industrial property councils or by individuals or legal entities established on the territory of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area ( EEA) and empowered to represent persons before the central industrial property department of that state;
- attendance at legal or economic seminars or conferences related to professional industrial property consulting activity;
- dispensing teachings related to professional industrial property consulting activity in a university or professional setting;
- publication of legal or economic work related to professional industrial property consulting activity.

Any continuing education followed by a national of an EU or EEA state in one of these states is deemed to meet this obligation.

**Duration**

The duration of continuing education is twenty hours in a calendar year or forty hours in two consecutive years. During the first two years of professional practice, this training includes at least ten hours of firm management, ethics and professional status.

**Procedure**

Industrial property councils report, by 31 January of each year, to the CNCPI the actions taken to meet their continuing education obligations in the last calendar year or, if necessary, last two years.

The supporting documentation for verifying compliance with this obligation is attached to this statement.

The CNCPI monitors the effective fulfilment of the obligation of continuous vocational training of industrial property councils and verifies the compliance of the trainings followed and the actions carried out, in particular their link with the activity of industrial property advice.

*For further information*: Articles L. 422-10-1 and R. 422-55-1 of the Intellectual Property Code

4°. Insurance and warranty
----------------------------------------------

Any industrial property counsel must justify professional liability insurance guaranteeing the negligence and faults it may commit to its clients in the performance of its duties. In addition to the purchase of professional liability insurance, the industrial property council must have a guarantee specifically earmarked for the repayment of funds, effects or values received.

Insurance and warranty documents must be forwarded to the Inpi each year.

*For further information*: Article L. 422-8 of the Intellectual Property Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### If necessary, apply for inclusion on the list of qualified persons in industrial property

EU or EEA nationals who wish to practise as industrial property consultancies are required to be registered on a list of qualified persons in industrial property for the performance of their activity.

Registration is subject to an obligation of good character and respect for the conditions of diploma and professional practice. It specifies the specialization of the consulting in industrial property according to the diplomas he obtained and the professional practice he acquired.

**Competent authority**

The Director of Inpi, after advice from the CNCPI, draws up an annual list of qualified persons in industrial property.

**Supporting documents**

In support of his application for registration, the national must provide the necessary proof of diplomas and professional practices specified above paragraph "2." b. EU nationals: for a permanent exercise (LE)."

**What to know**

A national who wishes to be on the list must not:

- have been the subject of a criminal conviction;
- be subject to a disciplinary or administrative sanction of dismissal, deletion, dismissal, withdrawal of accreditation or authorization, for similar acts;
- be subject to personal bankruptcy or a similar sanction.

If so, the Director General of Inpi may decide to remove him from the list, on a reasoned decision, provided that the person concerned has been able to make his comments beforehand.

**Please note**

To obtain this registration on the list, the national may be required to perform a compensation measure (see below "Good to know: compensation measure) in the form of a French language aptitude test, if it turns out that the qualifications or the professional experience he uses is substantially different from those required for the practice of the profession in France.

**Outcome of the procedure**

The Director of Inpi will decide on the application for registration after the jury decides within four months of its filing.

However, in the event of non-compliance with the application, the applicant will be asked to regularize it or challenge the objections made by inpi, which will have the effect of suspending this period.

The silence kept by the Director General of Inpi at the end of the four-month period is worth accepting the application.

*For further information*: Articles L. 422-5 and R. 421-7 and the following intellectual property code.

**Good to know: compensation measure**

After reviewing the supporting documents, the Director General of Inpi, responsible for establishing registration on the IP qualified list, tells the national the level of professional qualification required to practice in France and the substantial differences justifying the use of a compensation measure.

This measure takes the form of an aptitude test before the jury mentioned above in paragraph "2." a. National requirements."

The review, the terms and program of which are set by joint decree of the Seal Guard, Minister of Justice, and the Minister for Industrial Property, is organised within six months of notification of the Decision of the Director of Inpi to the person concerned.

**Cost**

Free.

*For further information*: Article R.421-8 of the Intellectual Property Code.

### Ask for inclusion on the list of industrial property councils

Once on the list of qualified persons in industrial property, the national may apply for registration on the list of industrial property councils referred to in Article R. 422-1 of the Industrial Property Code, at the Condition:

- to offer or commit to offering its services within three months either as an individual or as a group, or as an employee of another industrial property consultancy or with an industrial property consulting firm;
- To be a French national or a national of an EU or EEA state;
- Having a home or establishment in France
- to meet the insurance and warranty conditions set out in paragraph 4. Insurance and warranty."

**Competent authority**

The application for inclusion on the list of industrial property councils is addressed to the Director General of Inpi.

**Procedure**

Within four months of receiving the application, the Director General of Inpi will make a reasoned decision made after advice from the CNCPI. Note that the notice is deemed written in case of silence kept by the CNCPI within one month of its referral.

In the event of non-compliance with the application, the deadline will be suspended until the applicant regulates or challenges the objections made by inpi.

If notification is notified within this time frame, the application for registration is deemed accepted.

**Outcome of the procedure**

Once the application has been accepted, the Director General of Inpi registers by stating:

- The name of the industrial property council;
- the name of the firm or the name of the company in which or from which it operates.

**Cost**

Free.

*For further information*: Articles R. 422-1 and the following of the Intellectual Property Code.

### In the case of an exercise in the form of a professional civil society (SCP) or a liberal exercise company (SEL), apply for the company's registration with the Inpi

If the person wishes to practice in the form of a CPS or an SEL, he must put that company on the list drawn up by the Director General of Inpi, in addition to the compulsory registration of the personal property councils that compose it.

*For further information*: Article L.422-7 of the Intellectual Property Code.

### Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

