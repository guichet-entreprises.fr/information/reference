﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP227" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Primary school teacher" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="primary-school-teacher" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/primary-school-teacher.html" -->
<!-- var(last-update)="2020-04-15 17:21:10" -->
<!-- var(url-name)="primary-school-teacher" -->
<!-- var(translation)="Auto" -->


Primary school teacher
==============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:10<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The school teacher is a professional whose activity is to provide part of the education of children aged three to eleven and to provide them with instruction in kindergartens and primary schools.

As such, the professional conducts an assessment of the work of his students and assists them with their personal work.

**Please note**

The school teacher is a professional official of the state and practises in a trades of school teachers.

*For further information*: Decree 90-680 of 1 August 1990 on the special status of school teachers.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice as a school teacher, the professional must be professionally qualified. For this it must:

- successfully completed the academy's School Teacher Recruitment Competition (CRPE);
- have completed an internship as a trainee professor in a department of the academy concerned.

#### Training

**School Teacher Recruitment Competition (CRPE)**

There are several ways in which the professional can enter this competition, depending on his professional course:

- external competition if the professional holds a master's degree (B.A. 5), preferably specialized in the teaching of education and training (MEEF) trades or meets the requirements to enrol in the second year master's degree. This competition may also include two additional regional language events, if necessary it will be named a special external competition;
- the internal competition reserved for the professional who justifies a three-year professional experience. The competition may also include two additional regional language events;
- the second internal competition accessible to the professional with three years of professional experience in the public service and holder of a bachelor's degree (B.A. 3);
- the third competition accessible to professionals with professional experience in the private sector and without a degree requirement.

The candidate must undergo written eligibility tests and an oral admission test set out by the decree of 19 April 2013 setting out the arrangements for the external competition, the special external competition, the second internal competition, the second competition and the third competition for the recruitment of school teachers.

**Training course**

The successful candidate is appointed a trainee school teacher and must complete a one-year internship.

During this internship, the professional is trained to acquire the skills necessary to practice his profession. It is issued by a higher education institution and consists of periods of professional situation in a school and periods of training within a higher education institution.

At the end of the internship, the trainee school teacher:

- is appointed by the Academic Director of the National Education Services;
- obtains the certificate of aptitude for school professorship;
- and is assigned to the department in which he was assigned as an intern.

**Please note**

If there is no vacancy in this department, he may be assigned to another department of the academy or another academy.

*For further information*: Articles 10 to 13 of the decree of 1 August 1990 above.

**Obligation of continuing education**

The professional is required to train regularly and to do so, to take training offered by the higher schools of professorship and education.

*For further information*: Article L. 921-2 of the Education Code.

#### Costs associated with qualification

The cost of training varies depending on the course envisaged. For more information, it is advisable to get close to the institutions concerned.

3°. Conditions of honorability, professional rules
-------------------------------------------------------------

**Age requirement**

To teach in a kindergarten, the professional must be at least eighteen years old. As soon as he wishes to run a school, this requirement is increased to twenty-one years.

*For further information*: Article L. 921-1 of the Education Code.

**Disabilities**

No one may practise as a school teacher if he has been convicted:

- for a crime or misdemeanour contrary to probity and morals;
- depriving him of his civil, civil and family rights or stripped of his parental authority;
- permanently depriving him of his right to teach.

In addition, cannot exercise the professional who has been removed from his duties as a teacher.

**Responsibility and duty of information**

The professional is responsible for all the school activities of his students. He must also keep parents informed of the evolution of his students' schooling.

