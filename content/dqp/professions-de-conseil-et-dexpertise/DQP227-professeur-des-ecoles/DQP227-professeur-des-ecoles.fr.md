﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP227" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Professeur des écoles" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="professeur-des-ecoles" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/professeur-des-ecoles.html" -->
<!-- var(last-update)="2020-04-15 17:21:10" -->
<!-- var(url-name)="professeur-des-ecoles" -->
<!-- var(translation)="None" -->

# Professeur des écoles

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:10<!-- end-var -->

## 1°. Définition de l’activité

Le professeur des écoles est un professionnel dont l'activité consiste à assurer une partie de l'éducation des enfants âgés de trois à onze ans et de leur fournir un enseignement au sein d'écoles maternelles et primaires.

À ce titre, le professionnel effectue une évaluation du travail de ses élèves et leur apporte son aide pour leur travail personnel.

**À noter**

Le professeur des écoles est un professionnel fonctionnaire de l’État et exerce au sein d'un corps de métiers des professeurs des écoles.

*Pour aller plus loin* : décret n° 90-680 du 1er août 1990 relatif au statut particulier des professeurs des écoles.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer en tant que professeur des écoles, le professionnel doit être qualifié professionnellement. Pour cela il doit :

- avoir subi avec succès le concours de recrutement de professeurs des écoles (CRPE) organisé par l'académie ;
- avoir effectué un stage en tant que professeur-stagiaire au sein d'un département de l'académie concernée.

#### Formation

##### Concours de recrutement de professeurs des écoles (CRPE)

Plusieurs voies permettent au professionnel d'accéder à ce concours, selon son cursus professionnel :

- le concours externe dès lors que le professionnel est titulaire d'un diplôme de niveau master (bac +5), de préférence spécialisé dans les métiers de l'enseignement de l'éducation et de la formation (MEEF) ou remplit les conditions pour s'inscrire en deuxième année de master. Ce concours peut également comporter deux épreuves supplémentaires en langue régionale, le cas échéant il sera nommé concours externe spécial ;
- le concours interne réservé au professionnel qui justifie d'une expérience professionnelle de trois ans. Ce concours peut également comporter deux épreuves supplémentaires en langue régionale ;
- le second concours interne accessible au professionnel bénéficiant d'une expérience professionnelle de trois ans dans la fonction publique et titulaire d'un diplôme de niveau licence (bac +3) ;
- le troisième concours accessible au professionnel bénéficiant d'une expérience professionnelle dans le secteur privé et sans condition de diplôme.

Le candidat doit subir des épreuves écrites d'admissibilité et une épreuve orale d'admission fixée par l'arrêté du 19 avril 2013 fixant les modalités d'organisation du concours externe, du concours externe spécial, du second concours interne, du second concours interne spécial et du troisième concours de recrutement de professeurs des écoles.

##### Stage de formation

Le candidat reçu au concours est nommé professeur des écoles stagiaire et doit accomplir un stage d'un an.

Au cours de ce stage, le professionnel bénéficie d'une formation en vue d'acquérir les compétences nécessaires à l'exercice de sa profession. Elle est délivrée par un établissement d'enseignement supérieur et se compose de périodes de mise en situation professionnelle dans une école et de périodes de formation au sein d'un établissement d'enseignement supérieur.

À l'issue du stage, le professeur des écoles stagiaire :

- est titularisé par le directeur académique des services de l'éducation nationale ;
- obtient le certificat d'aptitude au professorat des écoles ;
- et est affecté dans le département dans lequel il a été affecté en qualité de stagiaire.

**À noter**

S'il n'y a pas de poste vacant au sein de ce département, il pourra être affecté dans un autre département de l'académie ou d'une autre académie.

*Pour aller plus loin* : articles 10 à 13 du décret du 1er août 1990 précité.

##### Obligation de formation continue

Le professionnel est tenu de se former régulièrement et pour cela, de suivre une formation proposée par les écoles supérieures du professorat et de l'éducation.

*Pour aller plus loin* : article L. 921-2 du Code de l'éducation.

#### Coûts associés à la qualification

Le coût de la formation varie selon le cursus envisagé. Pour plus de renseignements, il est conseillé de se rapprocher des établissements concernés.

## 3°. Conditions d’honorabilité, règles professionnelles

### Condition d'âge

Pour enseigner dans une école maternelle, le professionnel doit être âgé d'au moins dix-huit ans. Dès lors qu'il souhaite diriger une école cette exigence est portée à vingt et un ans.

*Pour aller plus loin* : article L. 921-1 du Code de l'éducation.

### Incapacités

Nul ne peut exercer en qualité de professeur des écoles s'il a fait l'objet d'une condamnation :

- pour un crime ou un délit contraire à la probité et aux bonnes mœurs ;
- le privant de ses droits civils, civiques et de famille ou déchu de son autorité parentale ;
- le privant définitivement de son droit d'enseigner.

En outre, ne peut exercer le professionnel qui a été révoqué de ses fonctions d'enseignant.

### Responsabilité et devoir d'information

Le professionnel est responsable de l'ensemble des activités scolaires de ses élèves. Il doit en outre tenir informés les parents de l'évolution de la scolarité de ses élèves.