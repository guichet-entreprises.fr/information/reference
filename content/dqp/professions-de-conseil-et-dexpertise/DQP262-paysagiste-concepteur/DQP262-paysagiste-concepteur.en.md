﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| Copyright © Guichet Entreprises - All Rights Reserved
| 	All Rights Reserved.
| 	Unauthorized copying of this file, via any medium is strictly prohibited
| 	Dissemination of this information or reproduction of this material
| 	is strictly forbidden unless prior written permission is obtained
| 	from Guichet Entreprises.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->

<!-- var(key)="DQP262" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Landscape architect" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="landscape-architect" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/landscape-architect.html" -->
<!-- var(last-update)="2020-04-15 17:21:11" -->
<!-- var(url-name)="landscape-architect" -->
<!-- var(translation)="Auto" -->



Landscape architect
===================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:11<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The designer landscape designer, known internationally as*landscape architect*, is a life manager and territory project specialist. A high-level specialist at the heart of environmental and development issues, the tasks entrusted to him are mainly on two levels:

- **missions of study, consulting, accompaniment and programming as assistant to owner:** the designer landscaper conducts, as an expert or mediator, most often on behalf of local authorities, planning, programming, planning and environmental studies (territorial coherence schemes, local plans urban planning, guide plans, plans for the protection and enhancement of natural spaces, charters and landscape plans, landscape atlases). He also leads workshops (around a territory project, an urban project or a public space). It can also accompany the owner in the process of finding a prime contractor (help in drafting a specification, organising the competition);
- **operational diagnostic, design and project management missions as prime contractor, agent or co-contractor:** Based on an initial diagnosis, the designer landscaper designs and manages complete developments, on all types of spaces and at all scales, from the plot to the large territory: urban projects, eco-neighbourhoods, subdivisions, zones commercial and industrial spaces, urban, suburban and rural public spaces (places, lanes, parks, gardens, promenades, pathways, transport and energy infrastructure (insertion and surroundings), heritage and tourist sites, spaces rural, agricultural and forest, it also provides management and monitoring missions.

By making a significant contribution to improving the living environment with a view to sustainable development, they are actively participating in the implementation of the European Landscape Convention.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Law No. 2016-1087 of August 8, 2016 for the reconquest of biodiversity, nature and landscapes (Article 174) created the title of landscape designer. It makes the landscape profession a regulated profession within the scope of the 2005/36/EC directive on the recognition of professional qualifications. It is the use of the title of designer landscaper that is regulated and not access to the profession. This regulation of the title of landscape designer does not create a monopoly of activity.

The implementation of the regulations is framed by the following texts:

- [Decree No. 2017-673 of April 28, 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034517176&categorieLien=id)  relating to the use of the title of landscape designer;
- [order of August 28, 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000035589400&categorieLien=id) setting out the conditions for applying for and issuing the authorization to use the title of landscape designer of the persons mentioned in Decree No. 2017-673 of April 28, 2017 relating to the use of the title of designer landscaper.

#### Training

The designer landscaper has a multidisciplinary training focused on the design of living environments and territory projects. His training allows him to mobilize, through the approach of the landscape project, from the order to its implementation, knowledge in the fields of life sciences and technologies, economics, ecological, human and social sciences and arts and creation.

Designer landscape training is delivered in five national higher schools. Its access is made on a common national competition, after reviewing the candidate's file, completed by an interview with possibly tests.

Four schools deliver the[State diploma as a landscaper](https://www.francecompetences.fr/recherche_certificationprofessionnelle/), earning a master's degree, after a 3-year course accessible to bac 2. Created in 2014, it succeeds the diploma of landscaper graduated by the government (DPLG landscaper). These schools are:

- [Versailles-Marseille National Higher Landscape School](http://www.ecole-paysage.fr/) ;
- [INSA Centre-Val-de-Loire School of Nature and Landscape](http://www.insa-centrevaldeloire.fr/) ;
- [Bordeaux National Graduate School of Architecture and Landscape](http://www.bordeaux.archi.fr/) ;
- [Lille National Graduate School of Architecture and Landscape](http://www.lille.archi.fr/).

A school issues the diploma of landscape engineer: diploma worth a master's degree after a training in 3, 4 or 5 years, accessible in post-bac or more.

- [Higher Institute of Agricultural, Agri-Food, Horticultural and Landscape Sciences (AGROCAMPUS WEST)](https://www.agrocampus-ouest.fr/).

#### Related costs

Training fees range from 601 to 1,987 euros per year for non-students. For more information, it is advisable to get closer to the different schools providing the training.

### b. European nationals: for temporary and casual exercise (free provision of services (LPS))

Nationals of a Member State of the European Union or another State party to the Agreement on the European Economic Area may freely perform landscape design services in France on an occasional and temporary basis.

In this case, the service is carried out under the professional title of the Member State of the establishment, which is mentioned in the official language or one of the official languages of the member state of the establishment in order to avoid confusion with the title of landscape designer. In the event that the professional title does not exist in the establishment Member State, the national mentions his or her training title in the official language or one of the official languages of the establishment Member State.

No prior reporting is required for a temporary and occasional exercise (LPS).

### c. EU nationals: for a permanent exercise (Freedom of establishment)

The steps required to obtain permission to use the designer landscaper title are described in section 5.b. below.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

A " [good practice repository](https://norminfo.afnor.org/norme/BP%20X50-787/paysagiste-concepteur-definition-de-la-profession-et-de-ses-modalites-dexercice/84824) Afnor (BP X50 787) was set up by the French Landscape Federation in 2009.

4°. Social legislation and insurance
--------------------------------------------------------

To practice on a permanent basis in France, the designer landscaper is not subject to a legal obligation to take out insurance.

However, he may have to do so depending on the purpose of his mission or the nature of his contract with the owner (e.g. professional liability insurance operating and/or decadal).

5°. Procedures and formalities
----------------------------------------

Persons with a degree or professional experience acquired in another Member State of the European Union or a State party to the agreement on the European Economic Area, already established in France or wishing to settle there permanently, must file their application for authorisation with the Ministry of Ecological and Solidarity Transition, which is responsible for receiving and hearing applications for authorisation relating to the use of the designer landscaper designation. All the information, as well as a[downloadable instructions](https://www.ecologique-solidaire.gouv.fr/politique-des-paysages#e7), are available on the ministry's website.

### a. Assessment criteria

To obtain permission to use the Design Landscaper designation, applicants must have the following skills:

- **ability to design the landscape through a landscape project approach:**- Be able to spatially interpret a planning and land issue by questioning and prioritizing the elements of a diagnosis,
  - be able to design the maintenance, improvement, evolution, adaptation or transformation of landscapes,
  - Know how to define a strategy by choosing or proposing in an argumentative manner an appropriate approach and modus operandi; identify short-, medium- and long-term indicators to measure the expected effects,
  - Be able to invent an approach and create one's own tools, to be creative and to mobilize one's intuition to advance relevant and fair proposals,
  - be able to propose sustainable and sustainable developments, to imagine spaces and management methods over time and over time, considering in particular the cyclical and random impact of uses, seasons and climates,
  - demonstrate projective capabilities at all scales,
  - be able to qualify, define, represent spatial configurations and establish requirements about the relationship between built and outer spaces, to specify the material components of the project in accordance with the intentions compatible with the ecological conditions encountered,
  - Be able to take into account the aspirations and representations of the populations throughout the project, from diagnosis to technical transcription;
- **ability to mobilize and articulate general landscape knowledge:**- be able to mobilize general knowledge related to the landscape and its historical and current characteristics (agriculture, parks and gardens, visual arts, architecture, urban art, urban planning, planning) as well as knowledge scientific and technical related to landscapes (geomorphology, hydrography, agronomy, horticulture, ecology, natural and human geography, etc.) and certain principles of engineering of interest to the landscape (rain-fed sanitation, soil treatment, support, earthworks, plantations),
  - Be able to articulate the knowledge, know-how and artistic, scientific and technical practices acquired in training and/or throughout professional experience,
  - Have knowledge of stakeholders, how to intervene, institutional and regulatory frameworks and tools,
  - Know public policies on landscape, urban planning and planning;
- **ability to develop a diagnosis of territories and understand territorial issues:**- be able to develop a sensitive diagnosis of territories: identify, describe, analyse and characterize a landscape or territory through its various components (multidisciplinary and multiscalary approach to a site), its characteristics (geomorphological, hydrological, agricultural, human, heritage), its dynamics at work, and this at all scales,
  - know how to describe the permanent, invariant and mutable elements and to analyze the dynamic evolution of a landscape over time,
  - know how to conduct documentary research and investigations,
  - know how to carry out field investigations and surveys of different types,
  - know how to make good use of existing studies and diagnostics,
  - know how to analyze the main issues and problems related to the territories and be able to prioritize them,
  - know how to place landscape issues within the broader framework of societal, environmental, economic, political and heritage issues,
  - know how to identify all stakeholders and report on their views, intentions, balances of power, respective means of action, strategies and modes of intervention,
  - Be able to capture stakeholders' spatial and cultural perceptions, representations and projections of their environment and living environment;
- **ability to communicate, express and mediate landscape situations:**- be able to clearly express the diagnosis of a landscape situation by articulating sensitive characters, physical and human spatial organization, living elements, morphological and historical evolution and describing the factors and agents to The work,
  - be able to describe a situation and its issues from several complementary or contradictory points of view, distinguishing individual intentions and collective issues and expressing a situation according to a multi-criteria analysis,
  - be able to translate people's perceptions and representations into an appropriate form, enabling action and decision-making,
  - Be able to communicate, be pedagogical, transmit knowledge and information by adapting forms of communication to audiences and partners,
  - know how to accurately represent and express a situation through the appropriate mastery and use of different communication tools (written, graphic, plastic, etc.),
  - Be able to negotiate and argue
- **ability to anticipate the evolution of a landscape:**- be able to predict and integrate dynamic, evolutionary and variable elements (flows and uses, natural hazards) into the design
  - Be able to anticipate and simulate the evolution of a landscape under the cumulative effect of the interventions of different actors over time, in the short, medium and long term,
  - Be able to express a forward-looking vision by developing evolutionary scenarios and imagining different modes of action on the landscape,
  - Be able to understand and anticipate social, cultural and ecological changes;
- **ability to take on operational control and work in a multidisciplinary professional team:**- be able to understand the modalities and conditions of construction in the context of a project management operation (writing contract documents, precision of documents and drawings, choice of materials and materials (vegetables responsibilities and insurance),
  - know the links and complementarities between designer landscapers and other professionals (relationships between project management and project management, relationships with co-contractors in project management, knowledge of responsibilities respectively on construction sites),
  - Be able to work as a team,
  - Be able to discern the limits of one's abilities and call on one or more complementary expertises;
- **ability to handle multiple professional situations:**- be able to take on several professional situations specific to the landscape profession in the private or public or parapublic sector (contractor, prime contractor, assistant to contractor, consultant, mediator),
  - be able to lead many of the following missions (design, engineering, piloting, planning, consulting, study, teaching, research, awareness).

These capabilities are listed in Article 2 of the[order of August 28, 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000035589400&categorieLien=id) setting out the conditions for applying for and issuing the authorisation to use the title of landscape designer of the persons mentioned in Decree No. 2017-673 of April 28, 2017 relating to the use of the designer landscaper title and its annex.

### b. Supporting documents to be provided

The supporting documents to be provided are:

- A readable copy of a valid ID
- A readable copy of diplomas, certificates of diplomas, certificates, clearances or titles obtained;
- A resume
- A letter outlining the applicant's motivations
- A list of references to the work and studies carried out showing their duration, the order of magnitude of the budgets and the status and responsibility of the applicant;
- a certificate from the competent authority of the Member State, where it exists, certifying the duration of the professional exercise with the corresponding dates.

The aforementioned documents are written in French or accompanied by their translation into The French language by a sworn translator

If the competent authority is not in a position to rule solely on the basis of these exhibits, it may require additional elements. For example, it may ask for the illustrated, and well-argued description of the expected capabilities, of some emblematic projects and/or studies conducted by the applicant, where his personal role and method of working are clear.

If they wish, applicants can, from the first filing, send additional information to enable the competent authority to assess their level of competence as best as possible.

### c. How to file the application for authorization

Supporting documents must be e-mailed as a single pdf document to:[paysagiste-concepteur@developpement-durable.gouv.fr](mailto:paysagiste-concepteur@developpement-durable.gouv.fr).

If the applicant does not have an email address, they can be sent by recommended letter with acknowledgement to:

<p style="margin-left: 2em">Ministère de la Transition écologique et solidaire<br>
Direction de l’Habitat, de l’Urbanisme et des Paysages<br>
Sous-direction de la qualité du cadre de vie<br>
Bureau des paysages et de la publicité<br>
92055 Paris La Défense Cedex</p>### d. Response time

Response within four months of receiving an application that includes all supporting documents and meets expectations.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

#### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

#### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

#### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

#### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

#### Cost

Free.

#### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

#### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

Six degrees. Reference texts
----------------------------

[Decree No. 2017-673 of April 28, 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034517176&categorieLien=id) regarding the use of the title of landscape designer.

[Arrested August 28, 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000035589400&categorieLien=id) setting out the conditions for applying for and issuing the authorization to use the title of landscape designer of the persons mentioned in Decree No. 2017-673 of April 28, 2017 relating to the use of the title of designer landscaper.

