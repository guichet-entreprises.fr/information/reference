﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| Copyright © Guichet Entreprises - All Rights Reserved
| 	All Rights Reserved.
| 	Unauthorized copying of this file, via any medium is strictly prohibited
| 	Dissemination of this information or reproduction of this material
| 	is strictly forbidden unless prior written permission is obtained
| 	from Guichet Entreprises.
+-------------------------------------------------------------------------- -->

<!-- var(key)="DQP262" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Paysagiste concepteur" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="paysagiste-concepteur" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/paysagiste-concepteur.html" -->
<!-- var(last-update)="2020-04-15 17:21:11" -->
<!-- var(url-name)="paysagiste-concepteur" -->
<!-- var(translation)="None" -->

# Paysagiste concepteur

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:11<!-- end-var -->

## 1°. Définition de l'activité

Le paysagiste concepteur, connu à l’international sous le terme *landscape architect*, est un concepteur de cadres de vie et un spécialiste du projet de territoire. Spécialiste de haut niveau au cœur des enjeux environnementaux et des problématiques d'aménagement, les missions qui lui sont confiées se situent principalement à deux niveaux :

- **missions d'études, de conseil, d’accompagnement et de programmation en tant qu’assistant à maître d'ouvrage :** le paysagiste concepteur conduit, en tant qu’expert ou médiateur, le plus souvent pour le compte des collectivités territoriales, des études d'urbanisme, de programmation, d'aménagement et d’environnement (schémas de cohérence territoriale, plans locaux d'urbanisme, plans-guide, plans de protection et de mise en valeur des espaces naturels, chartes et plans de paysage, atlas de paysage). Il anime également des ateliers de concertation (autour d’un projet de territoire, d’un projet urbain ou d’un espace public). Il peut aussi accompagner le maître d’ouvrage dans le processus de recherche d’un maître d’œuvre (aide à la rédaction d’un cahier des charges, organisation des mises en concurrence) ;
- **missions opérationnelles de diagnostic, de conception et de maîtrise d’œuvre en tant que maître d'œuvre, mandataire ou cotraitant :** à partir d’un diagnostic initial, le paysagiste concepteur conçoit et assure la maîtrise d'œuvre complète d'aménagements, sur tous types d'espaces et à toutes les échelles, de la parcelle au grand territoire : projets urbains, écoquartiers, lotissements, zones commerciales et industrielles, espaces publics urbains, périurbains et ruraux (places, voieries, parcs, jardins, promenades, cheminements, infrastructures de transport et d’énergie (insertion et abords), sites patrimoniaux et touristiques, espaces naturels, ruraux, agricoles et forestiers, Il assure également les missions de gestion et de suivi.

En contribuant de façon notable à l’amélioration du cadre de vie dans une optique de développement durable, ils participent activement à la mise en oeuvre de la Convention européenne du paysage.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Legislation nationale

La loi n° 2016-1087 du 8 août 2016 pour la reconquête de la biodiversité, de la nature et des paysages (article 174) a créé le titre de paysagiste concepteur. Elle fait de la profession de paysagiste une profession réglementée relevant du champ de la directive 2005/36/CE relative à la reconnaissance des qualifications professionnelles. C’est l’utilisation du titre de paysagiste concepteur qui est réglementée et non pas l’accès à la profession. Cette réglementation du titre de paysagiste concepteur ne crée donc pas de monopole d’activité.

La mise en œuvre de la réglementation est encadré par les textes suivants : 

- décret n° 2017-673 du 28 avril 2017 relatif à l’utilisation du titre de paysagiste concepteur ;
- arrêté du 28 août 2017 fixant les conditions de demande et de délivrance de l’autorisation d’utiliser le titre de paysagiste concepteur des personnes mentionnées au décret n° 2017-673 du 28 avril 2017 relatif à l’utilisation du titre de paysagiste concepteur.

#### Formation

Le paysagiste concepteur dispose d'une formation pluridisciplinaire axée sur la conception de cadres de vie et de projets de territoire. Sa formation lui permet de mobiliser, à travers la démarche du projet de paysage, depuis la commande jusqu'à sa mise en œuvre,  des connaissances dans les domaines des sciences et technologies du vivant, des sciences économiques, écologiques, humaines et sociales et dans celui des arts et de la création. 

La formation de paysagiste concepteur est délivrée dans cinq écoles nationales supérieures. Son accès se fait sur concours national commun, après examen du dossier du candidat, complété d’un entretien avec éventuellement des tests.

Quatre écoles délivrent le [diplôme d’État de paysagiste](https://www.francecompetences.fr/recherche_certificationprofessionnelle/), valant grade de master, à l’issue d’une formation en 3 ans accessible à bac +2. Créé en 2014, il succède au diplôme de paysagiste diplômé par le gouvernement (paysagiste DPLG). Ces écoles sont :

- [École nationale supérieure de paysage de Versailles-Marseille](http://www.ecole-paysage.fr/) ;
- [École de la nature et du paysage de l’INSA Centre-Val-de-Loire](http://www.insa-centrevaldeloire.fr/) ;
- [École nationale supérieure d’architecture et de paysage de Bordeaux](http://www.bordeaux.archi.fr/) ;
- [École nationale supérieure d’architecture et de paysage de Lille](http://www.lille.archi.fr/).

Une école délivre le diplôme d’ingénieur en paysage : diplôme valant grade de master à l’issue d’une formation en 3, 4 ou 5 années, accessible en post-bac ou plus.

- [Institut supérieur des sciences agronomiques, agroalimentaires, horticoles et du paysage (AGROCAMPUS OUEST)](https://www.agrocampus-ouest.fr/).

#### Coûts associés

Les frais de formation varient de 601 € à 1 987 € par an pour les étudiants non boursiers. Pour plus d’informations, il est conseillé de se rapprocher des différentes écoles dispensant la formation.

### b. Ressortissants européens : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Les ressortissants d’un État membre de l’Union européenne ou d’un autre État partie à l’accord sur l’Espace économique européen peuvent effectuent librement en France des prestations de conception paysagère, à titre occasionnel et temporaire.

Dans ce cas, la prestation est effectuée sous le titre professionnel de l’État membre d’établissement, qui est mentionné dans la langue officielle ou l’une des langues officielles de l’État membre d’établissement de manière à éviter toute confusion avec le titre de paysagiste concepteur. Dans le cas où le titre professionnel n’existe pas dans l’État membre d’établissement, le ressortissant fait mention de son titre de formation dans la langue officielle ou l’une des langues officielles de l’État membre d’établissement.

Aucune déclaration préalable n’est exigée dans le cas d’un exercice temporaire et occasionnel (LPS).

### c. Ressortissants UE : en vue d'un exercice permanent (Libre Établissement)

Les démarches à effectuer pour obtenir l’autorisation d’utiliser le titre de paysagiste concepteur sont décrites dans la rubrique 5°. b. ci-dessous.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Un « [référentiel de bonnes pratiques](https://norminfo.afnor.org/norme/BP%20X50-787/paysagiste-concepteur-definition-de-la-profession-et-de-ses-modalites-dexercice/84824) » Afnor (BP X50 787) a été mis en place par la Fédération française du paysage en 2009.

## 4°. Législation sociale et assurance

Pour exercer à titre permanent en France, le paysagiste concepteur n’est pas soumis à une obligation légale de souscrire une assurance. 

Néanmoins, il peut être amené à le faire en fonction de l’objet de sa mission ou de la nature de son contrat avec le maître d’ouvrage (par exemple une assurance responsabilité civile professionnelle exploitation et/ou décennale).

## 5°. Démarches et formalités

Les personnes titulaires d’un diplôme ou d’une expérience professionnelle acquis dans un autre État membre de l’Union européenne ou un État partie à l’accord sur l’Espace économique européen, déjà établies en France ou souhaitant s’y installer durablement, doivent déposer leur demande d’autorisation auprès du ministère de la Transition écologique et solidaire, compétent pour recevoir et instruire les demandes d’autorisation relatives à l’utilisation du titre de paysagiste concepteur. L’ensemble des informations, ainsi qu’un [mode d’emploi téléchargeable](https://www.ecologique-solidaire.gouv.fr/politique-des-paysages#e7), sont accessibles sur le site du ministère.

### a. Critères d'appréciation

Pour obtenir l’autorisation d’utiliser le titre de paysagiste concepteur, les demandeurs doivent détenir les compétences suivantes :

- **capacité à concevoir le paysage par une démarche de projet de paysage :**
  - être capable d’interpréter spatialement une problématique d’aménagement et de territoire en questionnant et en hiérarchisant les éléments d’un diagnostic,
  - être capable de concevoir le maintien, l’amélioration, l’évolution, l’adaptation ou la transformation des paysages,
  - savoir définir une stratégie en choisissant ou en proposant de manière argumentée une démarche et un mode opératoire appropriés; identifier des indicateurs à court, moyen et long pour mesurer les effets envisagés,
  - être capable d’inventer une démarche et de créer ses propres outils, de faire preuve de créativité et de mobiliser son intuition pour avancer des propositions pertinentes et justes,
  - être capable de proposer des aménagements durables et soutenables, d’imaginer des espaces et des modes de gestion dans la durée et dans le temps en considérant notamment l’impact cyclique et aléatoire des usages, des saisons et des climats,
  - faire preuve de capacités projectuelles à toutes les échelles,
  - être capable de qualifier, définir, représenter les configurations spatiales et d’établir des prescriptions à propos des relations entre les volumes bâtis et les espaces extérieurs, de préciser les composantes matérielles du projet conformément aux intentions du projet et compatibles avec les conditions écologiques rencontrées,
  - être capable de prendre en considération les aspirations et les représentations des populations tout au long du projet, depuis le diagnostic jusqu’à la transcription technique ;
- **capacité à mobiliser des connaissances générales liées au paysage et à les articuler :**
  - être capable de mobiliser des connaissances générales liées au paysage et à leurs caractéristiques historiques et actuelles (agriculture, parcs et jardins, arts plastiques, architecture, art urbain, urbanisme, planification) ainsi que des connaissances scientifiques et techniques en lien avec les paysages (géomorphologie, hydrographie, agronomie, horticulture, écologie, géographie naturelle et humaine, etc.) et certains principes de l’ingénierie intéressant le paysage (assainissement pluvial, traitement des sols, soutènements, terrassements, plantations),
  - être capable d’articuler les connaissances, les savoir-faire et les pratiques artistiques, scientifiques et techniques acquis dans la formation et/ou au long de l’expérience professionnelle,
  - avoir une connaissance des acteurs, des modalités d’intervention, des cadres et outils institutionnels et réglementaires,
  - connaître les politiques publiques en matière de paysage, d’urbanisme et d’aménagement ;
- **capacité à élaborer un diagnostic des territoires et à comprendre les enjeux territoriaux :**
  - être capable d’élaborer un diagnostic sensible des territoires: identifier, décrire, analyser et caractériser un paysage ou un territoire au travers de ses différentes composantes (approche pluridisciplinaire et multiscalaire d’un site), de ses caractéristiques (géomorphologiques, hydrologiques, agricoles, humaines, patrimoniales), de ses dynamiques à l’oeuvre, et ce à toutes les échelles,
  - savoir décrire les éléments permanents, invariants et mutables et savoir analyser l’évolution dynamique d’un paysage dans le temps,
  - savoir mener des recherches documentaires et des enquêtes,
  - savoir réaliser des investigations de terrain et des relevés de différents types,
  - savoir faire bon usage des études et diagnostics existants,
  - savoir analyser les principaux enjeux et problématiques liés aux territoires et être capable de les hiérarchiser,
  - savoir replacer les enjeux liés aux paysages dans le cadre plus large des problématiques sociétales, environnementales, économiques, politiques et patrimoniales,
  - savoir identifier l’ensemble des parties prenantes et rendre compte de leurs points de vue, de leurs intentions, de leurs rapports de force, de leurs moyens d’action respectifs, de leurs stratégies et modes d’intervention,
  - être capable de saisir les perceptions, représentations et projections spatiales et culturelles des parties prenantes eu égard à leur environnement et à leur cadre de vie ;
- **capacité à communiquer, à exprimer et à mener des médiations de situations paysagères :**
  - être capable d’exprimer clairement le diagnostic d’une situation paysagère en articulant caractères sensibles, organisation spatiale physique et humaine, éléments vivants, évolution morphologique et historique et en décrivant les facteurs et agents à l’oeuvre,
  - être capable de décrire une situation et ses enjeux selon plusieurs points de vue complémentaires ou contradictoires, en distinguant intentions individuelles et enjeux collectifs et d’exprimer une situation selon une analyse multi-critères,
  - être capable de traduire les perceptions et les représentations des populations sous une forme appropriable, permettant la formulation d’actions et la prise de décisions,
  - être capable de communiquer, de faire preuve de pédagogie, de transmettre des savoirs et des informations en adaptant les formes de communication en fonction des publics et des partenaires,
  - savoir représenter et exprimer une situation avec justesse grâce à la maîtrise et à l’utilisation appropriée et adaptée de différents outils de communication (écrit, graphique, plastique, etc.),
  - être capable de négocier et d’argumenter ;
- **capacité à anticiper l’évolution d’un paysage :**
  - être capable de prévoir et d’intégrer dans la conception les éléments dynamiques, évolutifs et variables (flux et usages, risques naturels),
  - être capable d’anticiper et de simuler l’évolution d’un paysage sous l’effet cumulé des interventions des différents acteurs dans le temps, à court, moyen et long terme,
  - être capable d’exprimer une vision prospective en élaborant des scénarios d’évolution et en imaginant différents modes d’actions sur le paysage,
  - être capable de comprendre et anticiper les évolutions sociales, culturelles et écologiques ;
- **capacité à assumer une maîtrise d’œuvre opérationnelle et à travailler en équipe professionnelle pluridisciplinaire :**
  - être capable d’appréhender les modalités et conditions de réalisation d’un chantier dans le cadre d’une opération de maîtrise d’œuvre (rédaction des documents contractuels, précision des documents et dessins, choix des matériels et matériaux (végétaux notamment), responsabilités et assurances),
  - connaître les liens et les complémentarités entre les paysagistes concepteurs et les autres professionnels (relations entre maîtrise d’œuvre et maîtrise d’ouvrage, relations avec les co-contractants en maîtrise d’œuvre, connaissance des responsabilités respectives sur les chantiers),
  - être capable de travailler en équipe,
  - être capable de discerner les limites de ses aptitudes et de faire appel à une ou des expertises complémentaires ;
- **capacité à assumer plusieurs situations professionnelles :**
  - être en capacité d’assumer plusieurs situations professionnelles propres au métier de paysagiste dans le secteur privé ou public ou parapublic (maître d’ouvrage, maître d’œuvre, assistant à maître d’ouvrage, conseil, médiateur),
  - être capable de conduire plusieurs des missions suivantes (conception, ingénierie, pilotage, planification, conseil, étude, enseignement, recherche, sensibilisation).

Ces capacités sont listées à l’article 2 de l’arrêté du 28 août 2017 fixant les conditions de demande et de délivrance de l’autorisation d’utiliser le titre de paysagiste concepteur des personnes mentionnées au décret n° 2017-673 du 28 avril 2017 relatif à l’utilisation du titre de paysagiste concepteur et son annexe. 

### b. Pièces justificatives à fournir

Les pièces justificatives à fournir sont les suivantes :

- une copie lisible d’une pièce d’identité en cours de validité ;
- une copie lisible des diplômes, attestations de diplômes, certificats, habilitations ou titres obtenus ;
- un curriculum vitae ;
- un courrier exposant les motivations du demandeur ;
- une liste de références des travaux et études réalisés faisant apparaître leur durée, l’ordre de grandeur des budgets ainsi que le statut et la responsabilité du demandeur ;
- une attestation émanant, lorsqu’elle existe, de l’autorité compétente de l’Etat membre certifiant la durée de l’exercice professionnel avec les dates correspondantes.

Les documents précités sont rédigés en langue française ou accompagnés de leur traduction en langue française établie par un traducteur assermenté

Si l’autorité compétente n’est pas en mesure de statuer sur la seule base de ces pièces, elle peut exiger des éléments complémentaires. A titre d’exemple, elle peut demander la description illustrée, et argumentée au regard des capacités attendues, de quelques projets et/ou études emblématiques conduits par le demandeur, où son rôle personnel et sa méthode de travail apparaissent clairement.

S’ils le souhaitent, les demandeurs peuvent, dès le premier dépôt, adresser des éléments complémentaires en vue de permettre à l’autorité compétente d’apprécier au mieux leur niveau de compétences.

### c. Modalités de dépôt de la demande d’autorisation

Les pièces justificatives doivent être envoyé par courriel sous la forme d’un seul document pdf à l'adresse suivante : [paysagiste-concepteur@developpement-durable.gouv.fr](mailto:paysagiste-concepteur@developpement-durable.gouv.fr).

Le cas échéant, si le demandeur ne dispose pas d’une adresse de messagerie, elles peuvent être envoyées par lettre recommandée avec accusé de réception à :

<p style="margin-left: 2em">Ministère de la Transition écologique et solidaire<br>
Direction de l’Habitat, de l’Urbanisme et des Paysages<br>
Sous-direction de la qualité du cadre de vie<br>
Bureau des paysages et de la publicité<br>
92055 Paris La Défense Cedex</p>

### d. Délai de réponse

Réponse dans les quatre mois suivant la réception d’une demande comprenant l’ensemble des pièces justificatives et répondant aux attendus.

### e. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

## 6°. Textes de référence

Décret n° 2017-673 du 28 avril 2017 relatif à l'utilisation du titre de paysagiste concepteur.

Arrêté du 28 août 2017 fixant les conditions de demande et de délivrance de l’autorisation d’utiliser le titre de paysagiste concepteur des personnes mentionnées au décret n° 2017-673 du 28 avril 2017 relatif à l’utilisation du titre de paysagiste concepteur.