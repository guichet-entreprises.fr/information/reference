﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Conseiller à l'utilisation des produits phytopharmaceutiques" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="conseiller-a-l-utilisation-produits" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/conseiller-a-l-utilisation-des-produits-phytopharmaceutiques.html" -->
<!-- var(last-update)="2020-04-15 17:21:03" -->
<!-- var(url-name)="conseiller-a-l-utilisation-des-produits-phytopharmaceutiques" -->
<!-- var(translation)="None" -->

# Conseiller à l'utilisation des produits phytopharmaceutiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:03<!-- end-var -->

## 1°. Définition de l’activité

Le conseiller en produits phytopharmaceutiques est un professionnel dont l'activité consiste à fournir des conseils sur la lutte contre les ennemis des cultures et l’utilisation des produits phytopharmaceutiques en toute sécurité.

*Pour aller plus loin* : article R. 254-1 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de conseiller en produits phytopharmaceutiques, le professionnel doit :

- être titulaire d'un certificat individuel dit « Certiphyto » ;
- obtenir un agrément (cf. infra « 5°. b. Demande en vue d'obtenir l'agrément pour l'activité de conseil »).

*Pour aller plus loin* : articles L. 254-1 à L. 254-6 du Code rural et de la pêche maritime.

#### Formation

##### Certificat individuel pour l'activité « conseil à l'utilisation des produits phytopharmaceutiques »

Le certificat individuel est délivré au professionnel qui soit :

- a suivi une formation comprenant une épreuve écrite d'une heure de vérification des connaissances ;
- a passé avec succès un test d'une heure trente, composé de trente questions portant sur le programme de formation fixé à l'annexe II de l'arrêté du 29 août 2016 ;
- est titulaire de l'un des diplômes figurant à l'annexe I de l'arrêté du 29 août 2016, obtenu au cours des cinq années précédant la demande.

Dès lors qu'il remplit l'une de ces conditions, le professionnel doit adresser une demande en vue d'obtenir la délivrance de ce certificat (cf. infra « 5°. a. Demande en vue d'obtenir un certificat individuel d'exercice »).

*Pour aller plus loin* : arrêté du 29 août 2016 portant création et fixant les modalités d'obtention du certificat individuel pour l'activité « conseil à l'utilisation des produits phytopharmaceutiques ».

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) titulaire d'un certificat délivré par un État membre, légalement établi et exerçant une activité de conseil en produits phytopharmaceutiques, peut exercer à titre temporaire et occasionnel la même activité en France.

Pour cela, il doit, préalablement à sa première prestation de services, effectuer une déclaration auprès du directeur régional de l'alimentation, de l'agriculture et de la forêt (DRAAF) du lieu d’exercice de la première prestation de services (cf. infra « 5°. c. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS) »).

*Pour aller plus loin* : articles L. 204-1 et R. 254-9 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'UE ou de l'EEE légalement établi et exerçant une activité liée à l'utilisation de produits phytopharmaceutiques peut exercer à titre permanent la même activité en France.

Dès lors que le ressortissant est titulaire d'un certificat individuel d'exercice délivré par un État membre, il est réputé être titulaire du certificat exigé pour exercer en France.

*Pour aller plus loin* : II de l'article R. 254-9 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le professionnel est tenu d'informer ses clients sur l'ensemble des dispositions en matière d'utilisation de ces produits et notamment :

- les obligations en matière de délai entre l'application des produits et la récolte ;
- la vitesse maximale du vent au-delà de laquelle ils ne peuvent utiliser ces produits ;
- les dispositions relatives à l'épandage ou la vidange des effluents phytopharmaceutiques.

En outre, tout professionnel utilisant des produits phytopharmaceutiques est tenu de respecter le plan national fixant les objectifs et les mesures prises en vue de réduire les effets de l'utilisation de ces produits sur la santé humaine et l'environnement.

*Pour aller plus loin* : article L. 253-6 du Code rural et de la pêche maritime ; arrêté du 4 mai 2017 relatif à la mise sur le marché et à l'utilisation des produits phytopharmaceutiques et de leurs adjuvants visés à l'article L. 253-1 du Code rural et de la pêche maritime.

## 4°. Assurance et sanctions

### Assurance

Le professionnel est tenu, pour obtenir son agrément, de souscrire une assurance couvrant sa responsabilité civile professionnelle.

*Pour aller plus loin* : article L. 254-2 du Code rural et de la pêche maritime.

### Sanctions pénales

Le professionnel qui exerce l'activité de conseiller en produits phytopharmaceutiques, sans être titulaire de l'agrément prévu à l'article L. 254-1 du Code rural et de la pêche maritime encourt une peine de six mois d'emprisonnement et de 15 000 euros d'amende. Une peine identique s’applique si le détenteur de l’agrément exerce son activité sans satisfaire aux conditions exigées par l’article L. 254-2 ou par l’article L. 254-5.

*Pour aller plus loin* : article L. 254-12 du Code rural et de la pêche maritime.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir un certificat individuel d'exercice

#### Autorité compétente

Le professionnel doit adresser sa demande à la direction régionale de l'alimentation, de l'agriculture et de la forêt (DRAAF) du lieu de sa résidence, ou, le cas échéant, du lieu du siège social de l'organisme où ont été réalisées les formations.

#### Pièces justificatives

Le demandeur est tenu de créer un compte en ligne sur le site [service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) afin d'accéder au service de la demande de certificat.

Sa demande doit comprendre, selon le cas :

- un justificatif attestant le suivi de la formation et, le cas échéant, la réussite au test ;
- la copie de son diplôme ou titre.

#### Délai et procédure

Le certificat lui est délivré dans un délai de deux mois suivant le dépôt de sa demande. En l'absence de délivrance du certificat après ce délai, et sauf notification de refus, les pièces justificatives précitées valent certificat individuel pour une durée maximale de deux mois.

Ce certificat est valable pendant cinq ans et renouvelable dans les mêmes conditions.

*Pour aller plus loin* : articles R. 254-11 et R. 254-12 du Code rural et de la pêche maritime.

### b. Demande en vue d'obtenir l'agrément pour l'activité de conseil

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet de la région au sein de laquelle se trouve le siège social de l'entreprise. Si le professionnel a son siège social dans un autre État membre que la France, il devra adresser sa demande au préfet de la région dans laquelle il effectuera sa première prestation de services.

#### Pièces justificatives

Sa demande doit comporter le [formulaire de demande d'agrément](https://www.service-public.fr/professionnels-entreprises/vosdroits/R22206) complété et signé ainsi que les documents suivants :

- une attestation de police d'assurance de responsabilité civile professionnelle ;
- un certificat délivré par un organisme tiers justifiant qu'il exerce cette activité dans le respect des conditions garantissant la protection de la santé publique et de l'environnement ;
- la copie d'un contrat conclu avec un organisme tiers prévoyant le suivi nécessaire au maintien de la certification.

**À noter**

Les micro-distributeurs doivent quant à eux transmettre à la DRAAF les documents suivants :

- une attestation d'assurance de responsabilité civile professionnelle ;
- un justificatif de la détention du certiphyto par l'ensemble du personnel exerçant des fonctions d’encadrement, de vente ou de conseil ;
- un document justifiant qu'il est soumis au régime fiscal des micro-entreprises.

#### Délai et procédure

Le professionnel qui débute son activité doit solliciter un agrément provisoire qui lui sera délivré pour une durée de six mois non renouvelable. Par la suite, il pourra effectuer sa demande d'agrément définitif.

Le silence gardé par le préfet de région au-delà d'un délai de deux mois vaut décision de rejet.

En outre, le professionnel est tenu de fournir chaque année au préfet une copie de son attestation d'assurance.

*Pour aller plus loin* : articles R. 254-15-1 à R. 254-17 du Code rural et de la pêche maritime.

### c. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande par tout moyen à la DRAAF du lieu d'exercice de sa première prestation de services.

#### Pièces justificatives

Sa demande doit comporter le certificat individuel d'exercice délivré par l’État membre et, le cas échéant, assorti de sa traduction en français.

**À noter**

Cette déclaration doit être renouvelée chaque année et en cas de changement de situation professionnelle.

*Pour aller plus loin* : III de l'article R. 254-9 du Code rural et de la pêche maritime.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).