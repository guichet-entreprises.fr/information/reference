﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP045" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Avocat au Conseil d'État et à la Cour de cassation" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="avocat-au-conseil-detat" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/avocat-au-conseil-detat.html" -->
<!-- var(last-update)="Août 2021" -->
<!-- var(url-name)="avocat-au-conseil-detat" -->
<!-- var(translation)="None" -->

# Avocat au Conseil d'État et à la Cour de cassation

Dernière mise à jour : <!-- begin-var(last-update) -->Août 2021<!-- end-var -->

## 1°. Définition de l’activité

L'avocat au Conseil d'État et à la Cour de cassation est un professionnel de la justice chargé d'assister et/ou de représenter les parties en cas de contentieux auprès du Conseil d’État et de la Cour de cassation.

Ces avocats aux conseils sont les seuls habilités à plaider devant ces juridictions lorsque la représentation est obligatoire.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer la profession d'avocat au Conseil d'État et à la Cour de cassation, le professionnel doit remplir les conditions d'aptitude suivant :

- être de nationalité française ou ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) ;
- être titulaire d'une maîtrise en droit (bac +4) ou d'un titre ou formation reconnu comme équivalent pour l'accès à la profession d'avocat ;
- avoir été inscrit pendant au moins un an au tableau d'un barreau des avocats ;
- avoir suivi une formation spécifique (cf. infra « Formation à la profession d'avocat au Conseil d’État et à la Cour de cassation ») ;
- avoir subi avec succès l'examen d'aptitude à la profession d'avocat au Conseil d’État et à la Cour de cassation ;
- ne faire l'objet d'aucune condamnation pénale pour des agissements contraires à l'honneur, à la probité ou aux bonnes mœurs ;
- ne faire l'objet d'aucune sanction disciplinaire ou administrative de destitution, radiation, révocation, de retrait d'agrément ou d'autorisation ;
- ne pas faire l'objet de faillite personnelle ou d'une interdiction de diriger, de gérer, d'administrer ou de contrôler une entreprise commerciale, artisanale ou agricole.

Dès lors qu'il remplit ces conditions, le professionnel est nommé (cf. infra « 2°. a. Nomination ») par le garde des Sceaux soit :

- dans un office existant, sur présentation de l'intéressé ;
- dans un office créé ;
- dans un office vacant ;
- à côté de ces modes de nomination pour exercer en qualité d'avocat libéral, un avocat aux conseils peut être nommé en qualité de salarié dans un office.

**À noter**

Un professionnel titulaire d'un office d'avocat au Conseil d’État et à la Cour de cassation ne peut employer plus d'un avocat salarié au sein de ce même conseil.

*Pour aller plus loin* : article 1 du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d’État et à la Cour de cassation.

#### Bon à savoir : dispenses

Sont dispensés de l'obtention d'une maîtrise en droit, de l'inscription au tableau d'un barreau et de la formation spécifique :

- les membres et anciens membres du Conseil d’État, exceptés les maîtres des requêtes et les auditeurs ;
- les magistrats et les anciens magistrats de la Cour de cassation et de la Cour des comptes, à l'exception des conseillers, des avocats généraux référendaires et des auditeurs ;
- dès lors qu'ils justifient de quatre années d'exercice :
  - les professeurs d'université chargés d'un enseignement juridique,
  - les maîtres et anciens maîtres des requêtes au Conseil d’État, les conseillers, les avocats généraux référendaires et anciens conseillers généraux référendaires à la Cour de cassation et à la Cour des comptes ;
- dès lors qu'ils justifient d'au moins un an de pratique professionnelle auprès d'un avocat au Conseil d’État et à la Cour de cassation :
  - les magistrats et anciens magistrats de l'ordre judiciaire, autres que ceux cités ci-dessus et justifiant avoir exercé pendant au moins huit ans,
  - les membres et anciens membres du Conseil d’État et de la Cour des comptes autres que ceux cités ci-dessus, les membres et anciens membres du corps des tribunaux administratifs et des cours administratives d'appel et les membres et anciens membres des chambres régionales des comptes justifiant d'au moins huit ans d'expérience,
  - les maîtres de conférences de droit et les anciens maîtres-assistant, titulaires d'un doctorat en droit et justifiant d'une pratique d'au moins dix ans d'enseignement juridique,
  - les avocats et anciens avocats inscrits pendant au moins dix ans au tableau d'un barreau français ou d'un État membre de l'UE,
  - les conseils et anciens conseils juridiques justifiant avoir été inscrits pendant au moins dix ans sur une liste de conseils juridiques,
  - les notaires ayant au moins dix années d'expérience.

*Pour aller plus loin* : articles 2 à 4 du décret n° 91-1125 du 28 octobre 1991 susvisé.

#### Formation

##### Formation à la profession d'avocat au Conseil d’État et à la Cour de cassation

Cette formation, d'une durée de trois ans, est dispensée sous l'autorité du Conseil de l'Ordre des avocats au Conseil d’État et à la Cour de cassation qui en fixe les modalités. Le professionnel admis à la formation est inscrit sur un registre spécifique tenu par le Conseil de l'Ordre.

Cette formation est composée :

- d'un enseignement théorique sous la forme d'une participation aux travaux de la conférence du stage des avocats au Conseil d’État et à la Cour de cassation et de travaux de pratique professionnelle ;
- des travaux de pratique professionnelle sous le contrôle du Conseil de l'Ordre et sous la responsabilité d'un maître de stage, avocat au Conseil d’État et à la Cour de cassation.

En cas de condamnation pénale pour manquements à l'honneur, à la probité et aux bonnes mœurs, ou en cas d'absence non justifiée pendant plus de trois mois, le professionnel stagiaire peut faire l'objet d'une radiation du registre et de la formation.

À l'issue de sa formation, le professionnel se voit remettre un certificat de fin de formation mentionnant la durée du service effectué, la nature des fonctions occupées et les observations du maître de stage.

*Pour aller plus loin* : articles 6 à 16 du décret du 28 octobre 1991 précité.

##### Examen d'aptitude en vue de l'obtention du certificat d'aptitude à la profession d'avocat aux conseils (Capac)

Pour exercer l'activité d'avocat conseil et près la Cour de cassation, le professionnel doit avoir subi avec succès un examen d'aptitude composé d'épreuves écrites d'admissibilité et d'épreuves orales d'admission.

Cet examen est subi devant un jury composé :

- d'un conseiller d’État ;
- d'un conseiller à la Cour de cassation ;
- d'un professionnel d'université, chargé d'un enseignement juridique ;
- de trois avocats au Conseil d’État et à la Cour de cassation.

Les modalités et le programme des épreuves sont fixés à l'annexe de l'arrêté du 22 août 2016 fixant le programme et les modalités de l'examen d'aptitude à la profession d'avocat au Conseil d’État et à la Cour de cassation.

Dès lors qu'il remplit ces conditions, le professionnel doit adresser une demande d'inscription à cet examen au président du Conseil de l'Ordre des avocats au Conseil d’État et à la Cour de cassation (cf. infra « 5°. a. Demande en vue de l'inscription à l'examen d'aptitude »).

**À noter**

Certains professionnels peuvent bénéficier des dispenses fixées à l'article 17 du décret du 28 octobre 1991.

Une fois l'examen subi avec succès, le professionnel se voit délivrer un certificat d'aptitude à la profession d'avocat aux conseils (Capac).

*Pour aller plus loin* : articles 17 et 18 du décret n° 91-1125 du 28 octobre 1991 susvisé.

#### Formation professionnelle continue

L'avocat conseil près la cour de cassation est tenu d'effectuer une formation continue.

Cette formation obligatoire, d'une durée de vingt heures (sur une période d'un an) ou de quarante heures (sur une période de deux ans), peut prendre la forme soit :

- d'une participation à des actions de formation juridique ou professionnelle, effectuées au sein d'universités ;
- d'une participation à des formations dispensées par des avocats au Conseil d’État et à la Cour de cassation ou des établissements d'enseignement ;
- d'une assistance à des colloques ou des conférences juridiques en lien avec l'activité du professionnel ;
- d'une dispense d'enseignements juridiques dans un cadre universitaire ou professionnel ;
- d'une publication de travaux à caractère juridique.

**À noter**

Au cours des deux premières années d'exercice professionnel, cette formation continue doit inclure dix heures portant sur la gestion d'un office, la déontologie et le statut professionnel. Toutefois, la formation des professionnels bénéficiant de dispenses, doit porter exclusivement sur ces points.

En outre, chaque année, au plus tard le 31 janvier, le professionnel est tenu de déclarer le nombre d'heures de formation continue suivies, auprès du secrétariat du Conseil de l'Ordre des avocats au Conseil d’État et à la Cour de cassation.

*Pour aller plus loin* : articles 18-1 et 18-2 du décret du 28 octobre 1991 précité et article 13-2 de l'ordonnance du 10 septembre 1817 qui réunit, sous la dénomination d'Ordre des avocats au Conseil d’État et à la Cour de cassation, l'Ordre des avocats aux conseils et le collège des avocats à la Cour de cassation, fixe irrévocablement, le nombre des titulaires, et contient des dispositions pour la discipline intérieure de l'Ordre.

#### Nomination

Le professionnel remplissant les conditions générales d'aptitude et titulaire du Capac doit, en vue de prêter serment et d'entrer en fonction, faire l'objet d'une nomination par le ministre de la Justice.

Le professionnel remplissant les conditions générales d'aptitude à la profession d'avocat au Conseil d'État et à la Cour de cassation peuvent faire l'objet d'une nomination sur office créé dans les conditions prévues par les articles 24 et 25 du décret n° 91-1125.

Le candidat souhaitant succéder à un avocat au conseil doit solliciter un agrément auprès du ministre de la Justice (cf. infra « 5°. b. Demande d'agrément en vue d'une nomination sur présentation »).

Le professionnel souhaitant exercer en tant que salarié, dans un office créé doit adresser sa demande auprès du ministre de la Justice (cf. infra « 5. c. Demande de nomination en vue d'un exercice salarié »).

En outre, en cas d'office déclaré vacant, le ministre de la Justice procède à un appel à manifestation d'intérêt, par arrêté publié au Journal officiel de la République française et fixe le montant de l'indemnité due. Le professionnel souhaitant pourvoir cet office vacant, doit adresser une candidature au garde des sceaux, accompagnée d'un engagement à payer cette indemnité.

**À noter**

Le professionnel peut également exercer l'activité d'avocat conseil au sein d'une société, dans les conditions prévues au sein décret n° 2016-881 du 29 juin 2016 relatif à l'exercice de la profession d'avocat au Conseil d’État et à la Cour de cassation sous forme de société autre qu'une société civile professionnelle, comme une société pluri-professionnelle d'exercice.

Une fois nommé, le professionnel prend ses fonctions à compter de sa prestation de serment. Celle-ci, doit être effectuée dans le mois suivant sa nomination par le ministre de la Justice.

*Pour aller plus loin* : articles 19 à 31 du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d’État et à la Cour de cassation.

#### Coûts associés à la qualification

La formation menant au Capac est gratuite. Vous pouvez consulter le [site de l'Ordre des avocats au Conseil d’État et à la Cour de cassation](http://www.ordre-avocats-cassation.fr/), pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant européen de l’UE ou de l’EEE peut exercer en France l'activité de conseil/représentation auprès du Conseil d’État et de la Cour de cassation, sous son titre professionnel d'origine, à titre temporaire et occasionnelle, s’il remplit les conditions suivantes :

- exercer sous l’un des titres figurant à la liste fixée à l’article 31-2 II du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d’accès à la profession d’avocat au Conseil d’État et à la Cour de cassation ;
- être habilités dans l’État membre où il est établi à représenter les parties devant la ou les juridictions suprêmes et y consacrer à titre habituel une part substantielle de son activité.

Dans ce cas, le professionnel européen intéressé doit alors présenter une demande au garde des sceaux, ministre de la Justice, par voie dématérialisée. Il y joint les justificatifs de son identité et de sa nationalité, ainsi que les documents permettant de vérifier qu’il satisfait aux conditions précitées. La preuve de ces conditions est libre.

Le ministre se prononce dans un délai de quinze jours. L’éventuelle absence de décision du ministre à l’expiration de ce délai vaut autorisation. L’autorisation délivrée ne comporte aucune limitation de durée.

Une fois que l'autorisation a été délivrée, le professionnel ne pourra assister/représenter un client qu'après avoir élu domicile auprès d'un avocat au Conseil d’État et à la Cour de cassation afin que les actes de procédure lui soient régulièrement notifiés.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant européen de l’UE ou de l’EEE peut exercer en France l'activité de conseil/représentation auprès du Conseil d’État et de la Cour de cassation, sous son titre professionnel d'origine, à titre permanent, s’il remplit les conditions suivantes :

- exercer sous l’un des titres figurant à la liste fixée à l’article 31-2 II du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d’accès à la profession d’avocat au Conseil d’État et à la Cour de cassation ;
- être habilités dans l’État membre où il est établi à représenter les parties devant la ou les juridictions suprêmes et y consacrer à titre habituel une part substantielle de son activité.

Dans ce cas, le professionnel européen intéressé doit alors présenter une demande au garde des sceaux, ministre de la Justice, par voie dématérialisée. Il y joint les justificatifs de son identité et de sa nationalité, ainsi que les documents permettant de vérifier qu’il satisfait aux conditions précitées. La preuve de ces conditions est libre.

Le ministre se prononce dans un délai de deux mois. L’éventuelle absence de décision du ministre à l’expiration de ce délai vaut autorisation. L’autorisation délivrée ne comporte aucune limitation de durée.

Une fois que l'autorisation a été délivrée, le professionnel ne pourra assister/représenter un client qu'après avoir élu domicile auprès d'un avocat au Conseil d’État et à la Cour de cassation afin que les actes de procédure lui soient régulièrement notifiés.

Le professionnel européen autorisé à exercer sous son titre d’origine à titre permanent est inscrit sur une liste spéciale du tableau de l’Ordre des avocats au Conseil d’État et à la Cour de cassation.

Au terme d’une activité régulière et effective sur le territoire national d’une durée au moins égale à trois ans et s’il remplit les conditions générales d’aptitude à la profession d’avocat au Conseil d’État et à la Cour de cassation, il peut demander à être nommé dans un office d’avocat au Conseil d’État et à la Cour de cassation dans les mêmes conditions que les avocats au Conseil d’État et à la Cour de cassation nationaux.

Le titulaire ainsi nommé prêtera serment.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Déontologie

Le professionnel exerçant l'activité d'avocat conseil et près la cour de cassation est tenu au respect des règles déontologies fixées par la profession.

À ce titre il est notamment tenu :

- d'exercer en toute indépendance ;
- au respect du secret professionnel ;
- d'agir en évitant tout conflit d'intérêts de ses clients ;
- au respect des exigences de courtoisie, de délicatesse, de modération et de loyauté envers les différentes juridictions ;
- au respect du devoir de délicatesse et de solidarité vis-à-vis de ses confrères ;
- de faire preuve de modération et de délicatesse dans la fixation de ses honoraires.

**Bon à savoir**

Les honoraires du professionnel sont fixés en accord avec le client au sein d'une convention d'honoraires.

Le règlement général de déontologie est consultable sur le [site de l'Ordre des avocats au Conseil d’État et à la Cour de cassation](http://www.ordre-avocats-cassation.fr/les-avocats-aux-conseils/deontologie-et-honoraires).

*Pour aller plus loin* : article 15 de l'ordonnance du 10 septembre 1817 précitée.

### Discipline

En cas de manquement aux règles déontologiques et professionnelles, l'avocat aux conseils peut faire l'objet d'une procédure disciplinaire. À ce titre, il encourt les peines disciplinaires suivantes :

- l'avertissement ;
- le blâme ;
- l'interdiction temporaire d'exercer, qui ne peut excéder trois ans ;
- la radiation du tableau des avocats ou le retrait de l'honorariat ;
- la privation du droit de faire partie de l'Ordre des avocats du Conseil d’État et à la Cour de cassation.

*Pour aller plus loin* : décret n° 2002-76 du 11 janvier 2002 relatif à la discipline des avocats au Conseil d’État et à la Cour de cassation.

## 4°. Assurance et garantie financière

### Contrat groupe d'assurance

La responsabilité civile professionnelle des avocats au Conseil d'État et à la Cour de cassation est régi par l'article 13 de l'ordonnance du 10 septembre 1817. Le Conseil de l'Ordre a souscrit un contrat groupe d'assurance afin de garantir les activités professionnelles des avocats aux conseils.

### Obligation de justifier d'une garantie financière

L'avocat doit justifier d'une garantie financière qui sera attribuée au remboursement des fonds reçus à l’occasion de son activité. Il s’agit d’un engagement de caution pris auprès d’une banque ou d’un établissement de crédit, une entreprise d’assurances ou une société de caution mutuelle. Son montant devra être au moins égal à celui des fonds qu'il envisage de détenir.

*Pour aller plus loin* : articles 210 à 225 du décret n° 91-1197 du 27 novembre 1991 organisant la profession d'avocat.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue de l'inscription à l'examen d'aptitude

Tout ressortissant de l'UE ou de l'EEE peut être nommé avocat au Conseil d’État et à la Cour de cassation s’il justifie :

- de diplômes, certificats ou autres titres ou formations assimilées permettant l'exercice de la même profession dans un État membre de l'UE ou de l’EEE délivrés soit par l'autorité compétente de cet État et sanctionnant une formation acquise de façon prépondérante dans l'EEE, soit par un pays tiers, à condition que soit fournie une attestation émanant de l'autorité compétente de l'État membre ou partie qui a reconnu les diplômes, certificats, autres titres ou formations assimilées certifiant que leur titulaire a une expérience professionnelle de trois ans au moins dans cet État ;
- ou de l'exercice à plein temps de la même profession pendant une année au moins ou, en cas d'exercice à temps partiel, pendant une durée totale équivalente au cours des dix années précédentes dans un État membre ou partie qui ne réglemente pas l'accès ou l'exercice de cette profession à condition que cet exercice soit attesté par l'autorité compétente de cet État. Toutefois, la condition d'une expérience professionnelle d'une année n'est pas exigée lorsque le ou les titres de formation détenus par le demandeur sanctionnent une formation réglementée directement orientée vers l'exercice de la profession.

Dès lors qu'il remplit l’une de ces conditions, le ressortissant doit se soumettre à un examen d'aptitude (article 5 du décret du 28 octobre 1991 précité et pour aller plus loin voir l’arrêté du 22 novembre 1991 pris en application de l'article 5 du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d'État et à la Cour de cassation).

Le candidat peut être dispensé d'une épreuve lorsque les connaissances, aptitudes et compétences qu'il a acquises au cours de sa formation, de son expérience professionnelle antérieure ou de l'apprentissage tout au long de la vie et ayant fait l'objet, à cette fin, d'une validation en bonne et due forme par un organisme compétent, dans un État membre ou dans un pays tiers sont de nature à rendre inutile le passage de cette épreuve. Toutefois, il ne peut être dispensé d'une vérification de ses connaissances relatives à la réglementation professionnelle et à la gestion d'un office.

L’intéressé adresse son dossier au garde des sceaux, ministre de la Justice. À la réception du dossier complet, le garde des sceaux, ministre de la Justice, lui délivre un récépissé.

La liste des candidats admis à se présenter à l'examen est, après avis du Conseil de l'Ordre des avocats au Conseil d'État et à la Cour de cassation, arrêtée par le garde des sceaux, ministre de la Justice.

La décision du garde des sceaux, ministre de la Justice, doit intervenir dans un délai de quatre mois à compter de la date de la délivrance du récépissé. Elle est motivée et précise :

a) le niveau de qualifications professionnelles requis en France et le niveau des qualifications professionnelles que possède le candidat conformément à la classification figurant à l'article 11 de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 modifiée relative à la reconnaissance des qualifications professionnelles ;
b) ainsi que, le cas échéant, les matières sur lesquelles le candidat doit être interrogé compte tenu des différences substantielles entre, d'une part, les connaissances, aptitudes et compétences acquises au cours de leur formation, de leur expérience professionnelle ou de l'apprentissage tout au long de la vie et, d'autre part, les matières dont la maîtrise est essentielle à l'exercice de la profession d'avocat au Conseil d'État et à la Cour de cassation.

Nul ne peut se présenter plus de trois fois à l'examen d'aptitude.

### b. Demande d'agrément en vue d'une nomination sur présentation

#### Autorité compétente

Le candidat doit adresser une demande au ministre de la Justice.

#### Pièces justificatives

Sa demande doit comprendre l'ensemble des conventions passées entre le titulaire de l'office ou ses ayants droit et le candidat et le cas échéant, l'ensemble des éléments permettant de justifier de ses capacités financières.

#### Délai et procédure

Le ministre de la Justice peut recueillir l'avis du Conseil de l'Ordre sur l'honorabilité et les capacités financières du candidat. En l'absence d'avis émis par le Conseil au-delà d'un délai de quarante-cinq jours, il est réputé avoir émis un avis favorable. De plus, le ministre recueille l'avis motivé du vice-président du Conseil d’État, du premier président et du procureur de la Cour de cassation. En l'absence d'avis émis au-delà de quarante cinq-jours à compter de la saisine, l'avis est réputé rendu.

*Pour aller plus loin* : articles 20 à 23 du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d’État et à la Cour de cassation.

### c. Demande de nomination en vue d'un exercice salarié

#### Autorité compétente

La demande doit être adressée par le titulaire de l'office et le candidat, auprès du ministre de la Justice, dans un délai de deux mois à compter de la publication des recommandations de l'Autorité de la concurrence pour les créations d'offices d'avocats au Conseil d’État et à la Cour cassation.

#### Pièces justificatives

Sa demande doit comporter une copie du contrat de travail et l'ensemble des justificatifs nécessaires.

#### Délai et procédure

Le garde des sceaux recueille l'avis motivé du vice-président du Conseil d’État, du premier président et du procureur général de la Cour de cassation et du Conseil de l'Ordre sur l'honorabilité, les capacités professionnelles, et sur le respect du contrat de travail du candidat des règles professionnelles. En l'absence de réponse au-delà d'un délai de quarante-cinq jours à compter de leur saisine, les avis sont réputés rendus.

**À noter**

L'avocat est tenu de prêter serment dans un délai d'un mois suivant sa nomination, dans le cas contraire il sera réputé comme renonçant à sa nomination.

*Pour aller plus loin* : décret n° 2016-651 du 20 mai 2016 relatif aux avocats au Conseil d’État et à la Cour de cassation salariés.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).