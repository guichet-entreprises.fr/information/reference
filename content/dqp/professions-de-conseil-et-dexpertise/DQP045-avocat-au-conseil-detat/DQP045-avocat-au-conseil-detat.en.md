﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP045" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Lawyer at the Council of State and the Court of Cassation" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="lawyer-at-the-council-of-state-and-the-court-of-cassation" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/lawyer-at-the-council-of-state-and-the-court-of-cassation.html" -->
<!-- var(last-update)="August 2021" -->
<!-- var(url-name)="lawyer-at-the-council-of-state-and-the-court-of-cassation" -->
<!-- var(translation)="Auto" -->


Lawyer at the Council of State and the Court of Cassation
=========================================================

Latest update: <!-- begin-var(last-update) -->August 2021<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Defining the activity

The lawyer at the Council of State and the Court of Cassation is a judicial professional responsible for assisting and/or representing the parties in case of litigation before the Council of State and the Court of Cassation.

These counsel to the boards are the only ones entitled to plead before these courts when representation is mandatory.

## 2°. Professional qualifications

### a. National requirements

#### National legislation

In order to practise as a lawyer at the Council of State and the Court of Cassation, the professional must meet the following aptitude requirements:

- be a French national or a national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) agreement;
- hold a master's degree in law (B.A. 4) or a title or training recognized as equivalent for access to the profession of lawyer;
- have been on the board of a lawyer's bar for at least one year;
- have received specific training (see infra "Training in the profession of lawyer at the Council of State and the Court of Cassation");
- have successfully passed the aptitude test for the profession of lawyer at the Council of State and the Court of Cassation;
- not be the subject of any criminal conviction for acts contrary to honour, probity or good morals;
- not be subject to any disciplinary or administrative sanction of dismissal, delisting, dismissal, withdrawal of accreditation or authorization;
- not be subject to personal bankruptcy or a prohibition on running, managing, administering or controlling a commercial, artisanal or agricultural business.

As soon as he fulfils these conditions, the professional is appointed (see infra "2." a. Nomination" by the Seal Guard:

- in an existing office, on presentation of the person concerned;
- in a created office;
- in a vacant office;
- in addition to these methods of appointment to practise as a Liberal lawyer, a lawyer with counsel may be appointed as an employee in an office.

**Please note**

A professional who holds a legal office in the Council of State and the Court of Cassation may not employ more than one salaried lawyer on the same council.

*For further information*: Article 1 of Decree 91-1125 of 28 October 1991 on the conditions of access to the profession of lawyer at the Council of State and the Court of Cassation.

**Good to know: exemptions**

Exempt from obtaining a master's degree in law, registering on a bar board and specific training:

- members and former members of the Council of State, except the masters of petitions and auditors;
- magistrates and former magistrates of the Court of Cassation and the Court of Auditors, with the exception of councillors, referendum attorneys general and auditors;
- they justify four years of exercise:- university professors in charge of legal education,
  - masters and former masters of petitions to the Council of State, councillors, referendum attorneys general and former referendum general counsel to the Court of Cassation and the Court of Auditors;
- as long as they justify at least one year of professional practice with a lawyer at the Council of State and the Court of Cassation:- magistrates and former magistrates of the judicial order, other than those mentioned above and justifying having exercised for at least eight years,
  - members and former members of the Council of State and the Court of Auditors other than those mentioned above, members and former members of the body of administrative tribunals and administrative courts of appeal and members and former members of the chambers accounts with at least eight years of experience,
  - law lecturers and former assistant professors with a doctorate in law and justifying a practice of at least ten years of legal education,
  - lawyers and former lawyers registered for at least ten years on the board of a French bar or an EU member state,
  - advice and former legal advice that can be included for at least 10 years on a list of legal advice,
  - notaries with at least ten years of experience.

*For further information*: Articles 2 to 4 of Decree 91-1125 of October 28, 1991.

#### Training

**Training in the profession of lawyer at the Council of State and the Court of Cassation**

This training, lasting three years, is provided under the authority of the Council of the Order of Lawyers in the Council of State and the Court of Cassation, which sets out its terms. The professional admitted to the training is registered on a specific register maintained by the Council of the Order.

This training consists of:

- theoretical education in the form of participation in the work of the lawyers' internship conference at the Council of State and the Court of Cassation and professional practice;
- professional practice under the control of the Council of the Order and under the responsibility of a traineee master, lawyer at the Council of State and the Court of Cassation.

In the event of a criminal conviction for breaches of honour, probity and morals, or in the case of unjustified absence for more than three months, the trainee professional may be subject to a delisting and training.

At the end of his training, the professional is given a certificate of completion of training stating the duration of the service performed, the nature of the duties performed and the observations of the trainee.

*For further information*: Articles 6 to 16 of the decree of 28 October 1991 above.

**Aptitude test for the Certificate of Fitness to Advocate (Capac)**

In order to practise as a counsel and by the Court of Cassation, the professional must have successfully completed an aptitude test consisting of written eligibility tests and oral admission tests.

This examination is before a jury composed of:

- a State Councillor;
- an adviser to the Court of Cassation;
- a university professional, in charge of legal education;
- three lawyers in the Council of State and the Court of Cassation.

The modalities and programme of the tests are set out in the annex of the decree of 22 August 2016 setting out the programme and the modalities of the examination of aptitude for the profession of lawyer at the Council of State and the Court of Cassation.

Once the professional fulfils these conditions, he must apply to the President of the Council of the Order of Lawyers for this examination to the Council of State and the Court of Cassation (see infra "5o). a. Application for registration for the aptitude test").

**Please note**

Some professionals may benefit from the exemptions set out in Article 17 of the decree of 28 October 1991.

Once the examination has been successfully completed, the professional is issued a Certificate of Aptitude for the Profession of Counsel (Capac).

*For further information*: Articles 17 and 18 of Decree 91-1125 of October 28, 1991.

**Continuous vocational training**

Counsel to the Court of Cassation is required to carry out continuous training.

This mandatory training, lasting twenty hours (over a period of one year) or forty hours (over a period of two years), can take the form of:

- participation in legal or vocational training activities at universities;
- participation in training by lawyers at the Council of State and the Court of Cassation or educational institutions;
- assistance at legal conferences or conferences related to the professional's activity;
- an exemption from legal education in an academic or professional setting;
- publication of legal work.

**Please note**

During the first two years of professional practice, this continuing education must include ten hours of agency management, ethics and professional status. However, the training of professionals benefiting from exemptions, must focus exclusively on these points.

In addition, every year, no later than 31 January, the professional is required to report the number of hours of continuing education followed to the secretariat of the Council of Lawyers to the Council of State and the Court of Cassation.

*For further information*: Articles 18-1 and 18-2 of the decree of 28 October 1991 above and Article 13-2 of the ordinance of 10 September 1817 which brings together, under the name of the Order of Lawyers in the Council of State and the Court of Cassation, the Order of Counsel to Counsel and the College of Lawyers to the Court of Cassation, irrevocably sets the number of holders, and contains provisions for the internal discipline of the Order.

**Appointment**

The professional who meets the general qualifications of aptitude and holds the Capac must, in order to take an oath and take office, be appointed by the Minister of Justice.

The professional who meets the general requirements of suitability for the profession of lawyer at the Council of State and the Court of Cassation may be the subject of an appointment on an office created under the conditions provided by Articles 24 and 25 of Decree 91-1125.

The candidate wishing to succeed a lawyer on the council must apply for approval from the Minister of Justice (see infra "5°. b. Application for accreditation for a nomination upon presentation").

The professional wishing to practice as an employee, in an office created must submit his application to the Minister of Justice (cf. infra "5. c. Request for appointment for a salaried exercise").

In addition, in the event of an office declared vacant, the Minister of Justice makes a call for expressions of interest, by decree published in the Official Journal of the French Republic and sets the amount of compensation due. The professional wishing to fill this vacant office must apply to the seal keeper, accompanied by a commitment to pay this compensation.

**Please note**

The professional may also practise as a lawyer in a company, under the conditions provided for in Decree No. 2016-881 of 29 June 2016 relating to the practice of the profession of lawyer at the Council of State and the Court of Cassation in the form of society other than a professional civil society, such as a multi-professional practising society.

Once appointed, the professional takes office from the time of his swearing-in. This is to be done within one month of his appointment by the Minister of Justice.

*For further information*: Articles 19 to 31 of Decree 91-1125 of 28 October 1991 on conditions of access to the profession of lawyer at the Council of State and the Court of Cassation.

#### Costs associated with qualification

Training leading to Capac is free. You can check out the[order of lawyers at the Council of State and the Court of Cassation](http://www.ordre-avocats-cassation.fr/), for more information.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any European national of the EU or the EEA can exercise in France the activity of advice / representation before the Council of State and the Court of Cassation, under his original professional title, on a temporary and occasional basis, if it meets the following conditions:

- practice under one of the titles appearing on the list set out in article 31-2 II of decree No. 91-1125 of October 28, 1991 relating to the conditions of access to the profession of lawyer at the Council of State and at the Court of Cassation;
- be authorized in the Member State where he is established to represent the parties before the supreme court(s) and devote a substantial part of his activity to this on a regular basis.

In this case, the interested European professional must then submit a request to the Keeper of the Seals, Minister of Justice, by electronic means. He attaches proof of his identity and nationality, as well as documents to verify that he meets the aforementioned conditions. The proof of these conditions is free.

The minister makes a decision within fifteen days. The possible absence of a decision by the Minister at the end of this period constitutes authorization. The authorization issued has no time limit.

Once the authorization has been issued, the professional will only be able to assist / represent a client after having taken up residence with a lawyer at the Council of State and the Court of Cassation so that the procedural documents are regularly him. notified.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any European national of the EU or the EEA can exercise in France the activity of advice / representation before the Council of State and the Court of Cassation, under his original professional title, on a permanent basis, if it fulfills the following conditions:

- practice under one of the titles appearing on the list set out in article 31-2 II of decree No. 91-1125 of October 28, 1991 relating to the conditions of access to the profession of lawyer at the Council of State and at the Court of Cassation;
- be authorized in the Member State where he is established to represent the parties before the supreme court (s) and devote a substantial part of his activity to this on a regular basis.

In this case, the interested European professional must then submit a request to the Keeper of the Seals, Minister of Justice, by electronic means. He attaches proof of his identity and nationality, as well as documents to verify that he meets the aforementioned conditions. The proof of these conditions is free.

The minister gives his opinion within two months. The possible absence of a decision by the Minister at the end of this period constitutes authorization. The authorization issued has no time limit.

Once the authorization has been issued, the professional will only be able to assist / represent a client after having taken up residence with a lawyer at the Council of State and the Court of Cassation so that the procedural documents are regularly him. notified.

The European professional authorized to practice under his original title on a permanent basis is registered on a special list of the register of the Bar Association at the Council of State and the Court of Cassation.

At the end of a regular and effective activity in the national territory for a period of at least equal to three years and if he fulfills the general conditions of aptitude for the profession of lawyer at the Council of State and at the Court of cassation, he may request to be appointed to a lawyer's office at the Council of State and the Court of Cassation under the same conditions as lawyers at the national Council of State and the Court of Cassation.

The holder so appointed will take an oath.

## 3°. Conditions of honorability, ethical rules, ethics

**Ethics**

The professional practising the activity of counsel and near the Court of Cassation is bound to respect the rules of ethics set by the profession.

As such, it is required to:

- to exercise independently;
- respect for professional confidentiality
- act to avoid any conflict of interest of its clients;
- respect for the requirements of courtesy, delicacy, moderation and loyalty to the various jurisdictions;
- respect for the duty of delicacy and solidarity towards his colleagues;
- to exercise moderation and delicacy in setting its fees.

**Good to know**

The professional's fees are set in agreement with the client within a fee agreement.

The general rules of ethics are available on the[order of lawyers at the Council of State and the Court of Cassation](http://www.ordre-avocats-cassation.fr/les-avocats-aux-conseils/deontologie-et-honoraires).

*For further information*: Article 15 of the ordinance of September 10, 1817.

**Discipline**

In the event of a breach of ethical and professional rules, counsel may be subject to disciplinary proceedings. As such, he faces the following disciplinary penalties:

- the warning
- blame him;
- a temporary ban on practising, which may not exceed three years;
- delisting from the avocado board or removing the honorariat;
- deprivation of the right to be a member of the Council of State's Bar Association and the Court of Cassation.

*For further information*: Decree No. 2002-76 of 11 January 2002 relating to the discipline of lawyers in the Council of State and the Court of Cassation.

## 4°. Financial insurance and guarantee

**Insurance group contract**

The professional civil liability of lawyers in the Council of State and the Court of Cassation is governed by Article 13 of the ordinance of September 10, 1817. The College Council has signed an insurance group contract to ensure the professional activities of lawyers and councils.

**Obligation to justify a financial guarantee**

The lawyer must justify a financial guarantee that will be allocated to the repayment of the funds received during his activity. This is a bonding commitment made to a bank or credit institution, an insurance company or a mutual surety company. Its amount must be at least equal to that of the funds it plans to hold.

*For further information*: Articles 210 to 225 of Decree 91-1197 of November 27, 1991 organizing the profession of lawyer.

## 5°. Qualification recognition procedures and formalities

### a. Application for registration for the aptitude test

Any citizen of the EU or the EEA can be appointed lawyer at the Council of State and the Court of Cassation if he can justify:

- diplomas, certificates or other titles or similar training allowing the exercise of the same profession in a Member State of the EU or the EEA issued either by the competent authority of that State and sanctioning a training acquired in a predominant way in the EEA, or by a third country, provided that a certificate from the competent authority of the Member State or party which recognized the diplomas, certificates, other titles or similar training certifying that their holder has a at least three years' professional experience in that State;
- or the full-time exercise of the same profession for at least one year or, in the case of part-time exercise, for a total equivalent period during the previous ten years in a Member State or party which does not regulate access or exercise of this profession provided that this exercise is certified by the competent authority of that State. However, the condition of one-year professional experience is not required when the training certificate (s) held by the applicant certifies regulated training directly oriented towards the practice of the profession.

As soon as he fulfills one of these conditions, the national must undergo an aptitude test (article 5 of the aforementioned decree of October 28, 1991 and to go further see the decree of November 22, 1991 taken into application of article 5 of decree No. 91-1125 of October 28, 1991 relating to the conditions of access to the profession of lawyer at the Council of State and the Court of Cassation).

The candidate may be exempted from a test when the knowledge, skills and competences he has acquired during his training, his previous professional experience or lifelong learning and having been the subject, to this end, proper validation by a competent body, in a Member State or in a third country is such as to make it unnecessary to take this test. However, he cannot be exempt from checking his knowledge of professional regulation and the management of an office.

The person concerned sends his file to the Keeper of the Seals, Minister of Justice. Upon receipt of the complete file, the Keeper of the Seals, Minister of Justice, issues him a receipt.

The list of candidates admitted to sit for the examination is, after advice from the Council of the Bar Association, the Council of State and the Court of Cassation, adopted by the Keeper of the Seals, Minister of Justice.

The decision of the Keeper of the Seals, Minister of Justice, must be made within four months from the date of issue of the receipt. She is motivated and precise:

a) the level of professional qualifications required in France and the level of professional qualifications possessed by the candidate in accordance with the classification appearing in Article 11 of Directive 2005/36/EC of the European Parliament and of the Council of 7 September 2005 as amended relating to recognition of professional qualifications;
b) as well as, where applicable, the subjects on which the candidate must be questioned, taking into account the substantial differences between, on the one hand, the knowledge, skills and competences acquired during their training, their professional experience or the lifelong learning and, on the other hand, the subjects the mastery of which is essential for the exercise of the profession of lawyer at the Council of State and the Court of Cassation.

No one may take the aptitude test more than three times.

### b. Application for accreditation for a presentation appointment

**Competent authority**

The applicant must submit a request to the Minister of Justice.

**Supporting documents**

His application must include all the agreements between the holder of the board or his rights holder and the candidate and, if necessary, all the elements to justify his financial capacity.

**Time and procedure**

The Minister of Justice may seek the advice of the Council of the Order on the candidate's honourability and financial capabilities. In the absence of notice issued by the Council beyond a period of forty-five days, it is deemed to have issued a favourable opinion. In addition, the Minister receives the reasoned opinion of the Vice-President of the Council of State, the first President and the Prosecutor of the Court of Cassation. In the absence of notice issued beyond forty-five days from the referral, the notice is deemed rendered.

*For further information*: Articles 20 to 23 of Decree 91-1125 of 28 October 1991 on conditions of access to the profession of lawyer at the Council of State and the Court of Cassation.

### c. Request for appointment for a salaried exercise

**Competent authority**

The request must be made by the holder of the office and the candidate, to the Minister of Justice, within two months of the publication of the competition authority's recommendations for the creation of law offices in the Council State Court and the Court of Cassation.

**Supporting documents**

His application must include a copy of the employment contract and all the necessary documentation.

**Time and procedure**

The Seal Keeper receives the reasoned opinion of the Vice-President of the Council of State, the First President and the Attorney General of the Court of Cassation and the Council of the Order on honorability, professional capacity, and compliance with the employment contract candidate of the professional rules. In the absence of a response beyond forty-five days from the time they are moved, the notices are deemed to have been rendered.

**Please note**

The lawyer is required to take the oath within one month of his appointment, otherwise he will be deemed to renounce his appointment.

*For further information*: decree No. 2016-651 of 20 May 2016 relating to lawyers in the Council of State and the Court of Cassation.

### d. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- full contact details
- detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- if this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).