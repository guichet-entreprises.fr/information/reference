﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP072" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Commissaire aux comptes" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="commissaire-aux-comptes" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/commissaire-aux-comptes.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="commissaire-aux-comptes" -->
<!-- var(translation)="None" -->

# Commissaire aux comptes

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

Le commissaire aux comptes (« CAC ») est un professionnel dont la mission principale est de certifier que les comptes annuels sont réguliers et sincères et donnent une image fidèle du résultat des opérations de l'exercice écoulé ainsi que de la situation financière et du patrimoine de la société à la fin de cet exercice. Il intervient dans une entité auprès de laquelle il a été nommé volontairement ou obligatoirement Au cours de sa mission, il sera tenu de révéler au procureur de la République tous faits délictueux dont il aurait eu connaissance.

Le CAC est par ailleurs amené à établir des rapports en cas d'événements majeurs dans la vie de l'entreprise tels qu'une augmentation de capital, une transformation ou encore le paiement de dividende.

Le CAC est en principe nommé pour six ans. Toutefois des dispositions limitent dans certains secteurs la possibilité de renouveler le mandat du CAC et imposent une rotation des signataires (EIP et filiales d'EIP, personnes morales de droit privé non commerçantes ayant une activité économique et dépassant certains seuils, associations recevant des subventions dépassant un seuil : voir les articles L. 822-14 et L.823-3-1 du Code de commerce).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer sa profession, le CAC doit être inscrit sur la liste établie par le Haut Conseil du commissariat aux comptes (« H3C »).

Pour cela, il devra remplir les conditions suivantes :

- être de nationalité française ou être ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) ou d'un autre Etat étranger lorsque celui-ci admet les nationaux français à exercer le contrôle légal des comptes ;
- ne pas avoir été condamné pénalement ni avoir été l'objet de sanction disciplinaire menant à une radiation ou pour des faits de même nature ;
- ne pas avoir été condamné pour des faits contraires à l'honneur ou à la probité ;
- ne pas avoir été frappé de faillite personnelle ou de l'une des mesures d'interdiction ou de déchéance prévues au livre VI du Code de commerce.

*Pour aller plus loin* : articles L. 822-1 et L. 822-1-1 du Code du commerce.

#### Formation

Pour être inscrit sur la liste des CAC, l’intéressé devra justifier :

- soit être titulaire du certificat d'aptitude aux fonctions de commissaire aux comptes (CAFCAC) ;
- soit être titulaire du diplôme d'expertise comptable (DEC) ;
- soit avoir réussi une épreuve d'aptitude, pour le ressortissant agréé par les autorités de son État de l'UE ou de l'EEE à exercer le contrôle légal des comptes ou, bien que non agréé, réunissant les conditions de titre, de diplôme et de formation pratique permettant d'obtenir un tel agrément conformément aux dispositions de la directive 2006/43/CE ;
- soit avoir réussi une épreuve d'aptitude pour le ressortissant d'un Etat non membre de l'UE admettant les nationaux français à exercer le contrôle légal des comptes, titulaire d'un diplôme ou titre de même niveau que le CAFCAC ou le DEC et justifiant d'une expérience professionnelle de trois ans dans le domaine du contrôle légal des comptes ;
- avoir accompli un stage professionnel d'une durée fixée par voie réglementaire chez un commissaire aux comptes ou une personne agréée par un État membre de l'Union européenne pour exercer le contrôle légal des comptes.

Pour se présenter au CAFCAC, il devra justifier être titulaire :

- soit d'un diplôme de niveau master et avoir réussi les épreuves du certificat préparatoire aux fonctions de CAC, dont le programme et celui du CAFCAC sont précisés dans un arrêté du 5 mars 2013 ;
- soit d'un diplôme d'études supérieures de niveau master en comptabilité ou en finance, validant au moins quatre des sept épreuves obligatoires permettant d'obtenir le diplôme supérieur de comptabilité et gestion (DSCG) ;
- soit du DSCG.

En outre, il devra impérativement justifier être titulaire d'une attestation de fin de stage professionnel, la durée de ce dernier devant être de trois ans.

Pour se présenter au DEC, l'intéressé devra justifier :

- être titulaire du DSCG ou du diplôme d'études comptables supérieures (DECS) ;
- avoir effectué un stage dont la durée est en principe de trois ans, en cabinet d'expertise comptable ou dans des structures assimilées.

*Pour aller plus loin* : articles 63 et suivants. du décret n° 2012-432 du 30 mars 2012.

Le CAFCAC comprend une épreuve d'admissibilité et une épreuve d'admission dont les modalités sont prévues à l'article A. 822-1 du Code de commerce.

*Pour aller plus loin* : articles R. 822-2 et suivants du Code de commerce ; arrêté du 5 mars 2013 fixant le programme du certificat d'aptitude aux fonctions de commissaire aux comptes et du certificat préparatoire aux fonctions de commissaire aux comptes.

#### Coûts associés à la qualification

Les formations menant aux diplômes permettant d'exercer la fonction de CAC sont payantes. Pour plus d'informations, il est conseillé de se renseigner auprès des écoles et des établissements universitaires les dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l’Union européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer la profession de CAC en France, à titre temporaire et occasionnel.

Dès lors, seules les mesures prises pour le libre établissement des ressortissants de l'UE ou de l'EEE (cf. infra « 5. Démarches et formalités de reconnaissance de qualification ») trouveront à s'appliquer.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE qui exerce légalement l'activité de CAC dans cet État peut s'établir en France pour y exercer cette même activité de manière permanente.

Il devra demander son inscription sur la liste des CAC auprès du Haut Conseil du commissariat aux comptes, sous réserve d'avoir réussi au préalable une épreuve d'aptitude (cf. infra « 5°. a. Demander son inscription sur la liste des CAC pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : articles L. 822-1-2 et R. 822-6 du Code de commerce.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Règles déontologiques

Un ensemble de règles déontologiques est à respecter, et notamment :

- le secret professionnel des données qu'il traite ;
- l'intégrité, en justifiant notamment ne pas être l'auteur de faits incriminants ou donnant lieu à une sanction disciplinaire ;
- l'indépendance et l'impartialité, afin d'éviter tout conflit d'intérêts entre sa mission de contrôle et celle de conseil ;
- l'incompatibilité d'exercice avec :
  - une activité ou un acte portant atteinte à son indépendance,
  - son emploi de salarié,
  - une activité commerciale,
  - et toute situation d'auto-révision.

*Pour aller plus loin* : articles L. 822-10 et suivants, R. 822-20 et suivants, et annexe 8-1 du Code de commerce.

### b. Sanctions pénales

Des sanctions pénales peuvent être appliquées à l'encontre du CAC qui exercerait son activité sans être inscrit sur la liste du Haut Conseil du commissaire aux comptes ou qui ne respecterait pas les règles déontologiques visées ci-dessus. Dans ces deux cas, il encourt une peine d'un an de prison et 15 000 euros d'amende.

Des sanctions pénales sont également prévues dans les situations énoncées aux articles L.820-6 et L.820-7 du Code de commerce, notamment lorsque le professionnel s'abstient de révéler les faits délictueux dont il a connaissance à l'occasion de sa mission.

*Pour aller plus loin* : articles L. 820-5 à L.820-7 du Code de commerce.

## 4°. Assurance

### a. Obligation de suivre une formation professionnelle continue

Afin d'entretenir et de perfectionner leurs connaissances dans les domaines liés à la comptabilité, la finance et le droit, les CAC doivent suivre une formation professionnelle continue de 120 heures au cours de trois années consécutives. 20 heures au moins sont accomplies au cours d'une même année (cf. article A. 822-28-2 du Code de commerce). Le H3C définit les orientations générales et les différents domaines sur lesquels l'obligation de formation continue peut porter. Il veille au respect des obligations des commissaires aux comptes dans ce domaine.

Toute personne inscrite sur la liste et qui n'a pas exercé les fonctions de commissaire aux comptes pendant trois ans et qui n'a pas respecté durant cette période l'obligation de formation continue est tenue de suivre une formation continue particulière avant d'accepter une mission de certification.

*Pour aller plus loin* : articles L. 822-4 et R. 822-22, et articles A. 822-28-1 et suivants du Code de commerce.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

Le CAC exerçant à titre libéral a l'obligation de souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

En tout état de cause, le CAC devra impérativement présenter une garantie financière supérieure à 76 224,51 € par année et par sinistre.

*Pour aller plus loin* : articles L. 822-17, R. 822-36 et A. 822-31 du Code de commerce.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demander son inscription sur la liste des CAC pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Réussite à l'épreuve d'aptitude

Le ressortissant ne peut être soumis à l'épreuve d'aptitude qu'après en avoir fait la demande écrite auprès du ministre chargé de la justice. Celle-ci doit être accompagnée d'un dossier envoyé en double exemplaire et des pièces justificatives suivantes :

- une pièce d'identité en cours de validité ;
- tout diplôme, certificat ou titre qui atteste des qualifications professionnelles du ressortissant ;
- toute attestation de stage ou de formation suivie permettant d'apprécier le contenu et le niveau d'études postsecondaire.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

À réception du dossier, le ministre chargé de la justice délivrera un récépissé au ressortissant et disposera d'un délai de quatre mois pour se prononcer sur la demande d'inscription à l'épreuve d'aptitude. Le silence gardé dans ce délai vaudra acceptation de celle-ci.

Une fois autorisé à se présenter à l'épreuve, le ressortissant devra réussir avec succès un examen écrit et oral en langue française, en obtenant une note moyenne égale ou supérieure à 10.

*Pour aller plus loin* : articles R. 822-6, A. 822-20 et suivants du Code de commerce.

#### Inscription sur la liste des CAC

##### Autorité compétente

Une fois que le ressortissant a réussi l'épreuve d'aptitude, il devra demander son inscription définitive sur la liste des CAC au Haut Conseil du commissariat aux comptes.

##### Pièces justificatives

La demande d'inscription peut se faire par voie postale ou directement en ligne sur le [site de la Compagnie nationale des commissaires aux comptes](https://www.cncc.fr/liste-cac.html). Le ressortissant devra transmettre, en plus de sa demande motivée, l'ensemble des éléments justificatifs suivants :

- une pièce d'identité en cours de validité ;
- un curriculum vitae ;
- une attestation de réussite à l'épreuve d'aptitude ;
- une attestation de non-incompatibilité avec l'exercice de la profession ;
- une attestation sur l'honneur justifiant qu'il n'est ni frappé de faillite personnelle ni l'auteur de faits ayant donné lieu à une condamnation pénale.

##### Issue de la procédure

À réception des pièces, le Haut Conseil délivrera un récépissé au ressortissant. Si, dans un délai de quatre mois à compter de la réception de ce récépissé, l'autorité compétente ne s'est pas prononcée sur la demande, l'inscription est réputée accordée.

Le ressortissant pourra ainsi exercer légalement la fonction de CAC en France, après avoir prêté serment devant le premier président de la cour d'appel territorialement compétente.

*Pour aller plus loin* : articles L. 822-1, L. 822-1-1, L.822-1-2, R.822-9 et R. 822-12 du Code de commerce.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).