﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP072" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Statutory auditor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="statutory-auditor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/statutory-auditor.html" -->
<!-- var(last-update)="2020-04-15 17:21:01" -->
<!-- var(url-name)="statutory-auditor" -->
<!-- var(translation)="Auto" -->


Statutory auditor
=======

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Auditor ("CAC") is a professional whose main mission is to certify that the annual accounts are regular and sincere and give an accurate picture of the results of the operations of the past year as well as the financial situation and company's assets at the end of this fiscal year. He intervenes in an entity with which he was appointed voluntarily or obligatory During his mission, he will be obliged to disclose to the public prosecutor any wrongdoing of which he would have been aware.

The ACC is also required to report on major events in the life of the company such as a capital increase, a transformation or the dividend payment.

The ACC is in principle appointed for six years. However, provisions limit the possibility of renewing the mandate of the ACC in some sectors and impose a rotation of signatories (EIP and subsidiaries of EIP, non-commercial private legal entities with economic activity and exceeding certain thresholds, associations receiving grants exceeding a threshold: see articles L. 822-14 and L.823-3-1 of the Code of Commerce).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice its profession, the ACC must be listed by the High Council of the Office of the Commissioner of Auditors ("H3C").

To do so, he will have to meet the following conditions:

- be a French national or be a national of a Member State of the European Union (EU) or a party to the agreement on the European Economic Area (EEA) or another foreign state when it admits French nationals to exercise legal control of the Accounts
- not having been criminally convicted or subjected to disciplinary sanctions leading to a deletion or for similar acts;
- not having been convicted for acts contrary to honour or probity;
- not to have been subjected to personal bankruptcy or any of the prohibition or forfeiture measures in Book VI of the Code of Commerce.

*To go further* Articles L. 822-1 and L. 822-1-1 of the Trade Code.

#### Training

To be on the ACC list, the individual will have to justify:

- or hold the Certificate of Ability to Act as An Auditor (CAFCAC);
- or hold the Diploma of Accounting (DEC);
- either to have passed an aptitude test, for the national approved by the authorities of his EU state or the EEA to exercise legal control of the accounts or, although not approved, meeting the conditions of title, diploma and practical training to obtain such accreditation in accordance with the provisions of directive 2006/43/EC;
- or have passed an aptitude test for the national of a non-EU state admitting French nationals to exercise legal control of the accounts, holding a diploma or title of the same level as the CAFCAC or the DEC and justifying an experience three-year professional in the field of legal auditing;
- have completed a professional internship of a period set by regulation with an auditor or a person approved by a Member State of the European Union to exercise legal control of accounts.

To attend the CAFCAC, he will have to justify being a holder:

- a master's degree and have passed the tests of the preparatory certificate for CAC's functions, the program of which is specified in a[decree of March 5, 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027143986&categorieLien=id) ;
- i.e. a master's degree in accounting or finance, validating at least four of the seven compulsory tests for a graduate degree in accounting and management (DSCG);
- DSCG.

In addition, it is imperative that he justify holding a certificate of completion of a professional internship, the duration of the latter must be three years.

In order to attend the DEC, the person concerned will have to justify:

- Hold the DSCG or Graduate Accounting Diploma (DECS);
- have completed an internship that is in principle three years, in an accounting firm or in similar structures.

*To go further* Articles 63 and following. Decree No. 2012-432 of March 30, 2012.

The CAFCAC includes an eligibility test and an admission test, the terms and conditions of which are provided for at the[Article A. 822-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=58CC1389C7F69724803027999EB5D10B.tplgfr33s_1?idArticle=LEGIARTI000027146372&cidTexte=LEGITEXT000005634379&dateTexte=20171207) Code of Commerce.

*To go further* Articles R. 822-2 and the following of the Code of Commerce; decree of 5 March 2013 setting out the programme of the certificate of fitness for the functions of auditor and the certificate preparatory to the functions of auditor.

#### Costs associated with qualification

Training for diplomas to carry out the role of CAC is paid for. For more information, it is advisable to check with the schools and universities providing them.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

There are no regulations for a national of a Member State of the European Union (EU) or part of the European Economic Area (EEA) wishing to practise as a CAC in France, on a temporary and casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals (see below "5. Steps and procedures for recognition of qualification") will find to apply.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state who legally operates as a CAC in that state may settle in France to carry out the same activity on a permanent basis.

He will have to apply for his registration on the list of CAC from the High Council of the Office of the Commissioner of Auditors, subject to having passed a test of aptitude beforehand (see infra "5°. a. Request registration on the ACC list for EU or EEA nationals for a permanent exercise (LE)).

*To go further* Articles L. 822-1-2 and R. 822-6 of the Code of Commerce.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethical Rules

A set of ethical rules must be followed, including:

- The professional secrecy of the data it processes;
- Integrity, including the justification for not being the perpetrator of incriminating or disciplinary acts;
- independence and impartiality, in order to avoid any conflict of interest between its supervisory and advisory missions;
- exercise incompatibility with:- an activity or act that infringes on its independence,
  - his job as an employee,
  - a business activity,
  - and any self-review situation.

*To go further* Articles L. 822-10 and following, R. 822-20 and following, and Appendix 8-1 of the Code of Commerce.

### b. Criminal sanctions

Criminal sanctions may be applied against the ACC which would operate without being on the list of the High Council of the Auditors or who would not respect the ethical rules referred to above. In both cases, he faces a one-year prison sentence and a fine of 15,000 euros.

Criminal sanctions are also provided in the situations set out in Articles L.820-6 and L.820-7 of the Code of Commerce, particularly when the professional refrains from disclosing the criminal facts he is aware of during his mission.

*To go further* Articles L. 820-5 to L.820-7 of the Code of Commerce.

4°. Insurance
---------------------------------

### a. Obligation to undergo continuing vocational training

In order to maintain and improve their knowledge in the areas of accounting, finance and law, ACCs must complete 120 hours of continuing professional training for three consecutive years. At least 20 hours are completed in the same year (see Article A. 822-28-2 of the Code of Commerce). The H3C defines the general guidelines and the various areas on which the requirement for continuing education may relate. It ensures compliance with the obligations of auditors in this area.

Any person on the list who has not served as an auditor for three years and who has not complied with the continuing education requirement during this period is required to undergo special continuing education prior to to accept a certification mission.

*To go further* Articles L. 822-4 and R. 822-22, and Articles A. 822-28-1 and the following of the Code of Commerce.

### b. Obligation to take out professional liability insurance

The Liberal ACC has an obligation to purchase professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

In any event, the ACC must provide a financial guarantee of more than 76,224.51 euros per year per claim.

*To go further* Articles L. 822-17, R. 822-36 and A. 822-31 of the Code of Commerce.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request registration on the ACC list for EU or EEA nationals for a permanent exercise (LE)

#### Success in the aptitude test

The national may only be subject to the aptitude test after making a written request to the Minister for Justice. This must be accompanied by a duplicate file and the following supporting documents:

- A valid piece of identification
- any diploma, certificate or title that attests to the national's professional qualifications;
- any certificate of internship or training to assess the content and level of post-secondary education.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

Upon receipt of the file, the Minister of Justice will issue a receipt to the national and will have four months to decide on the application for the aptitude test. The silence kept within this time will be worth accepting of it.

Once allowed to take the test, the national must pass a written and oral exam in the French language, with an average score of 10 or higher.

*To go further* Articles R. 822-6, A. 822-20 and the following of the Code of Commerce.

#### Registration on the list of CAC

**Competent authority**

Once the national has passed the aptitude test, he will have to apply for his final inclusion on the list of CAC to the High Council of the Office of the Commissioner of Auditors.

**Supporting documents**

The application can be made by post or directly online on the[National Company of Auditors website](https://www.cncc.fr/liste-cac.html). In addition to his or her reasoned request, the national will be required to submit all the following supporting elements:

- A valid piece of identification
- A resume
- A certificate of success in the aptitude test;
- a certificate of non-incompatibility with the practice of the profession;
- a certificate of honour justifying that he is not subject to personal bankruptcy or the perpetrator of facts that have resulted in a criminal conviction.

**Outcome of the procedure**

Upon receipt of the coins, the High Council will issue a receipt to the national. If, within four months of receipt, the competent authority has not decided on the application, registration is deemed to be granted.

The national will thus be able to legally exercise the function of CAC in France, after having been sworn in before the first president of the territorially competent Court of Appeal.

*To go further* Articles L. 822-1, L. 822-1-1, L.822-1-2, R.822-9 and R. 822-12 of the Code of Commerce.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

