<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
| 	All Rights Reserved.
| 	Unauthorized copying of this file, via any medium is strictly prohibited
| 	Dissemination of this information or reproduction of this material
| 	is strictly forbidden unless prior written permission is obtained
| 	from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP271" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Official receiver" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="official-receiver" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/official-receiver.html" -->
<!-- var(last-update)="2021-04" -->
<!-- var(url-name)="official-receiver" -->
<!-- var(translation)="True" -->

# Official receiver [FR]

Dernière mise à jour : <!-- begin-var(last-update) -->2021-04<!-- end-var -->

## 1°. Définition de l'activité

Les mandataires judiciaires sont les mandataires, personnes physiques ou morales, chargés par décision de justice de représenter les créanciers et de procéder à la liquidation d’une entreprise dans les conditions définies par le titre II du livre VI du Code de commerce.

*Pour aller plus loin* : article L. 812-1 du Code de commerce ; site internet du [Conseil national des administrateurs et mandataires judiciaires](https://www.cnajmj.fr/fr/).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Les mandataires judiciaires sont inscrits sur une liste nationale établie par la [Commission nationale d'inscription et de discipline des administrateurs judiciaires et des mandataires judiciaires](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006242221&dateTexte=&categorieLien=cid).

Pour y être inscrit, l'intéressé devra remplir les conditions suivantes :

- être français ou ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) ;
- ne pas avoir été condamné pénalement pour des faits contraires à l'honneur ou à la probité ;
- ne pas avoir fait l'objet de sanction disciplinaire ou administrative de destitution, de radiation, de révocation, de retrait d’agrément ou d’autorisation ;
- ne pas avoir été frappé de faillite personnelle ou d’une des mesures de déchéance ou d’interdiction prévues par les législations sur les procédures collectives.

*Pour aller plus loin* : article L. 812-3 du Code de commerce.

#### Formation

Plusieurs voies permettent d’accéder à la profession de mandataire judiciaire :

- être titulaire des diplômes prévus aux articles R. 811-7 et R. 811-8 du Code de commerce et réussir l’examen d’accès au stage professionnel, exécuter un stage professionnel rémunéré de trois à six ans, puis réussir l’examen d’aptitude à l’exercice des fonctions d’administrateur judiciaire ;
- être titulaire d’un diplôme de master en administration et liquidation d’entreprises en difficulté et justifier :
  - soit d’au moins cinq ans d’expérience professionnelle en tant que collaborateur d’un administrateur judiciaire,
  - soit de huit ans au moins de pratique professionnelle comptable, juridique ou financière dans le domaine de l'administration, du financement, de la restructuration, dont les fusions-acquisitions, ou de la reprise d'entreprises, notamment en difficulté,
  - soit d'avoir accompli un stage professionnel d’une durée minimum de trente mois dans une étude d’administrateur judiciaire inscrite sur la liste de la Commission nationale d’inscription et de discipline.

*Pour aller plus loin* : articles R. 812-4, R. 812-18-1 et R. 812-18-2 du Code de commerce.

**Bon à savoir : Les dispenses d'examen et de stage**

Certaines dispenses peuvent être accordées à l'intéressé qui souhaite devenir mandataire judiciaire, selon ses qualifications, son expérience professionnelle et/ou les fonctions qu'il a pu exercer. 

Les personnes qui justifient avoir acquis dans un État membre de l’Union européenne autre que la France ou un autre État partie à l’accord sur l’Espace économique européen, une qualification suffisante pour l’exercice de la profession de mandataire judiciaire sont dispensées des conditions de diplôme, de stage et d’examen professionnel, sous réserve d’avoir subi un examen de contrôle des connaissances dans certains cas (formation portant sur des matières substantiellement différentes ou activité non réglementée dans l’État membre d’origine).

Cette qualification peut résulter :

- de la justification d’avoir suivi avec succès un cycle d'études post-secondaires d'une durée minimale de trois ans ou d'une durée équivalente à temps partiel dans une université ou un établissement d'enseignement supérieur ou dans un autre établissement d'un niveau équivalent de formation, et, le cas échéant, la formation professionnelle requise en plus de ce cycle d'études ;
- de la justification d’un exercice à plein temps de la profession pendant deux ans au moins au cours des dix années précédentes dans un État membre ou partie à l'accord sur l'EEE qui ne réglemente pas l’accès ou l’exercice de la profession d’administrateur judiciaire, à condition que cet exercice soit attesté par l'autorité compétente de cet État. Cette condition d’expérience professionnelle de deux ans n’est pas exigée lorsque le ou les titres détenus sanctionnent une formation réglementée directement orientée vers l’exercice de la profession.

*Pour aller plus loin* : articles L. 812-3 dernier alinéa, R. 812-15 et R. 812-16 du Code de commerce.

Par ailleurs sont dispensés :

- de l’examen d’accès au stage :
  - les administrateurs judiciaires ayant exercé leur profession pendant trois ans au moins,
  - les avocats, les notaires, les commissaires-priseurs judiciaires, les huissiers de justice, les greffiers des tribunaux de commerce, les experts-comptables et les commissaires aux comptes, ayant exercé leur profession pendant cinq ans au moins,
  - les personnes titulaires de l'un des titres ou diplômes mentionnés à l'article R. 811-7, justifiant de cinq ans au moins de pratique professionnelle comptable, juridique ou financière dans le domaine de l'administration, du financement, de la restructuration, dont les fusions-acquisitions, ou de la reprise d'entreprises, notamment en difficulté,
  - les personnes ayant exercé les fonctions de collaborateur d'un administrateur judiciaire pendant une durée de cinq ans ;
- du stage professionnel :
  - les administrateurs judiciaires, avocats, notaires, commissaires-priseurs judiciaires, huissiers de justice, greffiers des tribunaux de commerce, anciens avoués, experts-comptables, commissaires aux comptes, ayant exercé leur profession pendant dix ans au moins,
  - les collaborateurs d’un mandataire judiciaire ayant exercé pendant une durée de dix ans au moins,
  - les personnes titulaires des diplômes requis pour la voie d’accès par l’examen d’accès au stage professionnel ([article R. 811-7 du Code de commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) justifiant d’une pratique professionnelle de quinze ans au moins dans les domaines comptable, juridique, financier ou dans le domaine de l’administration, du financement, de la restructuration ou de la reprise d’entreprises ;
- de toutes les épreuves de l'examen d'aptitude :
  - les administrateurs judiciaires ayant exercé leur profession pendant cinq ans au moins et ayant effectué le stage professionnel ;
- de l’épreuve de procédure civile et de droit pénal des affaires de l’examen d’aptitude :
  - les avocats, notaires, commissaires-priseurs judiciaires, huissiers de justice, greffiers des tribunaux de commerce,
  - les personnes titulaires des diplômes requis pour l’accès à la profession par la voie de l’examen d’accès au stage professionnel ([article R. 811-7 du Code de commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) qui justifient de cinq ans au moins de pratique professionnelle juridique dans le domaine de l’administrateur, du financement de la restructuration ou la reprise d’entreprises ;
- de l’épreuve de l’examen d’aptitude portant sur la gestion d’un cabinet de mandataire judiciaire :
  - les personnes ayant exercé pendant une durée de cinq ans au moins les fonctions de collaborateur d’un administrateur judiciaire,
  - les experts-comptables et les commissaires aux comptes,
  - les personnes titulaires des diplômes prévus à l’article R. 811-7 du Code de commerce qui justifient de cinq ans au moins de pratique professionnelle comptable ou financière, dans le domaine de l’administration, du financement, de la restructuration, dont les fusions-acquisitions, ou de la reprise d’entreprises, notamment en difficulté.

Enfin, la durée du stage professionnel est réduite à un an pour :

- les administrateurs judiciaires, les avocats, les notaires, les commissaires-priseurs judiciaires, les huissiers de justice, les greffiers des tribunaux de commerce, les anciens avoués, les experts-comptables, les commissaires aux comptes ayant exercé leur profession pendant cinq ans au moins ;
- les personnes titulaires de l'un des titres ou diplômes mentionnés à l'[article R. 811-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid) et justifiant de dix ans au moins de pratique professionnelle comptable, juridique ou financière, dans le domaine de l'administration, du financement, de la restructuration, dont les fusions-acquisitions, ou de la reprise d'entreprises, notamment en difficulté.

*Pour aller plus loin* : voir notamment les articles R. 811-7, R. 811-8, R. 812-7, R. 812-13, R. 812-14, R. 812-15 et R. 812-16 du Code de commerce.

#### Coûts associés

Les examens d’accès au stage et d’aptitude sont gratuits.

*Pour aller plus loin* : articles R. 811-16 et R. 812-5 du Code de commerce 

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Une personne ressortissant de l’UE ou de l’EEE qui souhaite exercer temporairement et occasionnellement la profession de mandataire judiciaire doit répondre aux conditions rappelées ci-dessus et réaliser les démarches d’inscription sur la liste nationale tenue par la Commission nationale d’inscription et de discipline.

*Pour aller plus loin* : article L. 812-3 du Code de commerce.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant de l’UE ou de l’EEE peut accéder à la profession par les voies traditionnelles ouvertes aux ressortissants français.

Par ailleurs, le ressortissant d'un État de l'UE ou de l'EEE diplômé d’un cycle d’études supérieures de trois ans minimum peut accéder à la profession pour y exercer de façon permanente si :

- il est titulaire de diplômes, certificats ou autres titres acquis dans un État membre de l’UE ou de l’EEE justifiant une qualification suffisante pour l’exercice de la profession de mandataire judiciaire (cycle d’études d’une durée minimale de trois ans ou équivalent)(article R. 812-15 1° du Code de commerce) ;
- ou s’il a exercé la profession à temps plein pendant deux ans au cours des dix dernières années dans un État membre qui ne réglemente ni l’accès à la profession ni son exercice, à condition que cet exercice soit attesté par l’autorité compétente de cet État (article R. 812-15 2° du Code de commerce).

Dès lors qu'il remplit l'une de ces conditions, il pourra demander son inscription sur la liste des mandataires judiciaires auprès de la Commission nationale d'inscription et de discipline des administrateurs judiciaires et des mandataires judiciaires (cf. infra « 5°. a. Demander son inscription sur la liste des mandataires judiciaires pour le ressortissant en vue d'un exercice permanent (LE) »).

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard de celles requises pour l’accès et l'exercice de la profession en France, l’intéressé pourra être soumis à un examen de contrôle des connaissances.

*Pour aller plus loin* : articles L. 812-3, R. 812-15 et R. 812-16 du Code de commerce.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Incompatibilité d'exercice

Les mandataires judiciaires peuvent exercer leur activité à titre individuel ou sous n’importe quelle forme de société. Toutefois, la forme juridique choisie ne doit pas conférer à ses associés la qualité de commerçant.

L'exercice de la profession de mandataire judiciaire est incompatible avec l’exercice de toute autre profession. Elle est par ailleurs incompatible avec :

- toutes les activités à caractère commercial, qu’elles soient exercées directement ou par personne interposée ;
- la qualité d’associé ou de dirigeant de certaines sociétés (article L. 812-8 du Code de commerce).

*Pour aller plus loin* : article L. 812-5 et L. 812-8 du Code de commerce.

## 4°. Formation continue et assurance

### a. Obligation de suivre une formation professionnelle continue

Tout mandataire judiciaire est tenu de suivre une formation professionnelle continue. D'une durée de 20 heures au cours d'une année civile ou de 40 heures au cours de deux années consécutives, cette formation est définie par le Conseil national des administrateurs judiciaires et vise à maintenir et à actualiser les connaissances de l'administrateur dans les domaines comptable, fiscal et légal.

*Pour aller plus loin* : articles L. 814-9, L. 814-10, R. 814-28-1 et suivants du Code de commerce.

### b. Obligation de s'inscrire à la caisse de garantie

Tout mandataire judiciaire est tenu de s'inscrire à une caisse de garantie gérée par les cotisants eux-mêmes. Cette caisse doit permettre de garantir le remboursement des fonds, effets ou valeurs reçus par chaque administrateur dans le cadre de ses missions.

En revanche, lorsque le mandataire judiciaire est salarié, elle n'est que facultative et c'est à son employeur de souscrire une telle garantie.

*Pour aller plus loin* : article L. 814-3 du Code de commerce.

### c. Obligation de souscrire une assurance de responsabilité civile professionnelle

Tout mandataire judiciaire doit souscrire une assurance de responsabilité civile professionnelle le couvrant des conséquences pécuniaires résultant de son activité.

Toutefois, s'il exerce en qualité de salarié, c'est à l'employeur de souscrire une telle assurance.

*Pour aller plus loin* : article L. 814-4 du Code de commerce.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demander son inscription sur la liste des mandataires judiciaires pour le ressortissant en vue d'un exercice permanent (LE)

**Autorité compétente**

La Commission nationale d'inscription et de discipline des administrateurs judiciaires et des mandataires judiciaires est compétente pour se prononcer sur la demande d'inscription du ressortissant.

<p style="text-align:center">Commission nationale d'inscription et de discipline des administrateurs judiciaires et mandataires judiciaires<br>
13 place Vendôme<br>
75042 PARIS CEDEX 01 (FRANCE)</p>

**Pièces justificatives**

Le ressortissant devra transmettre par lettre recommandée avec avis de réception la demande d'inscription au secrétariat de la Commission. Celle-ci prend la forme d'un dossier comprenant les pièces justificatives suivantes :

- une demande d'inscription ;
- les documents établissant l'état civil et la nationalité du candidat ;
- une copie des titres et diplômes dont il entend se prévaloir ou, à défaut, une attestation des autorités habilitées à les délivrer ;
- le cas échéant, l'attestation de réussite à l'examen d'aptitude ;
- le cas échéant, les éventuelles dispenses sollicitées et les justificatifs correspondants ;
- le cas échéant, l'attestation de réussite à l'examen de contrôle de connaissance ;
- le candidat indique en outre ses activités professionnelles antérieures et le lieu où il envisage d'établir son domicile professionnel ;
- tous documents utiles indiquant que les conditions de l'article L. 811-5 du Code de commerce (absence de condamnation/sanction ou d'interdiction) sont remplies.

Les documents doivent être transmis à l'adresse postale suivante:

<p style="text-align:center">Ministère de la justice<br>
Commission nationale d'inscription et de discipline des administrateurs judiciaires et mandataires judiciaires<br>
13 place Vendôme<br>
75042 PARIS CEDEX 01 (FRANCE)</p>

**Bon à savoir**

Certains documents spécifiques sont exigés lorsque l'exercice de la profession est envisagé sous forme sociale (voir notamment les [articles R. 814-60 et suivants du Code de commerce](https://www.legifrance.gouv.fr/affichCode.do;jsessionid=235FB1B419B02405DFBE8202B5B7EE09.tplgfr24s_2?idSectionTA=LEGISCTA000006191119&cidTexte=LEGITEXT000005634379&dateTexte=20190723)).

**Procédure**

Une fois que la Commission nationale a reçu le dossier complet, elle doit demander l'avis du Conseil national des administrateurs judiciaires et des mandataires judiciaires (CNAJMJ), qui a un mois pour se prononcer.

Si à réception du dossier la Commission nationale estime qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France, elle pourra le soumettre à un contrôle de connaissance.

Dans le cas d’un ressortissant de l’UE ou de l’EEE titulaire de certificats/titres/diplômes d’un État autre que la France (article R. 811-27 du Code de commerce) la Commission doit rendre sa décision dans un délai maximum de quatre mois à compter de la date de réception du dossier complet. Dans les autres cas, la décision doit être rendue dans le délai d’un an à compter de cette date.

*Pour aller plus loin* : articles R. 812-15, R. 812-16 et R. 812-17 du Code de commerce ; décret n° 2014-1278 du 23 octobre 2014 relatif aux exceptions à l’application du délai de deux mois de naissance des décisions implicites d’acceptation sur le fondement du II de l’article 21 de la loi no 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations (ministère de la Justice).

### b. Voies de recours

Les recours contre la décision de la Commission nationale d'inscription et de discipline des administrateurs et mandataires judiciaires peuvent être exercés devant la cour d'appel de Paris dans le délai d'un mois à compter de la date de réception de la lettre de notification de la décision.
Le recours est formé soit par déclaration au greffe de la cour d'appel, soit par lettre recommandée avec demande d'avis de réception adressée au greffier en chef de cette cour.

La représentation par un avocat n'est pas obligatoire.

*Pour aller plus loin* : articles R. 812-19 et 814-2 du Code de commerce 

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

**Conditions**

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

**Procédure**

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

**Pièces justificatives**

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

**Délai**

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

**Coût**

Gratuit.

**Issue de la procédure**

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

**Informations supplémentaires**

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).