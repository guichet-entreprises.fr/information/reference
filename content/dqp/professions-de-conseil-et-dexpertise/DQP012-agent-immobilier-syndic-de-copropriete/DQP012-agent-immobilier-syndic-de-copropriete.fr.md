﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP012" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Agent immobilier - Syndic de copropriété - Administrateur de biens" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="agent-immobilier-syndic-de-copropriete" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/agent-immobilier-syndic-de-copropriete-administrateur-biens.html" -->
<!-- var(last-update)="2021-09-14" -->
<!-- var(url-name)="agent-immobilier-syndic-de-copropriete-administrateur-biens" -->
<!-- var(translation)="None" -->

# Agent immobilier - Syndic de copropriété - Administrateur de biens

Dernière mise à jour : <!-- begin-var(last-update) -->2021-09-14<!-- end-var -->

## 1°. Définition de l’activité

L’agent immobilier, l’administrateur de biens et le syndic de copropriété (nommés ci-après « professionnels de l’immobilier ») sont des professionnels dont les missions consistent à participer aux opérations sur les biens d’autrui et relatives à :

- l’achat, la vente, la recherche, l’échange, la location ou sous-location, saisonnière ou non, en nu ou en meublé, d’immeubles bâtis ou non bâtis ;
- l’achat, la vente ou la location-gérance de fonds de commerce ;
- la cession d’un cheptel (fonds de bétail) mort ou vif ;
- la souscription, l’achat, la vente d’actions ou de parts de sociétés immobilières ou de société d’habitat ;
- l’achat, la vente de parts sociales non négociables lorsque l’actif social comprend un immeuble ou un fonds de commerce ;
- la gestion immobilière ;
- la vente de documents relatifs à l’achat, la vente, la location ou sous-location, en nu ou en meublé, d’immeubles bâtis ou non, ou à la vente de fonds de commerce (à l’exception des publications par voie de presse) ;
- la conclusion de tout contrat de jouissance d’immeuble à temps partagé ;
- l’exercice des fonctions de syndic de copropriété.

*Pour aller plus loin* : articles 1er et suivants de la loi n° 70-9 du 2 janvier 1970 réglementant les conditions d’exercice des activités relatives à certaines opérations portant sur les immeubles et les fonds de commerce.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer ces activités, l'intéressé doit être titulaire d’une carte professionnelle comportant les informations suivantes :

- les activités exercées par le professionnel, à savoir, la ou les mentions suivantes :
  - « transactions sur immeubles et fonds de commerce »,
  - « gestion immobilière »,
  - « syndic de copropriété »,
  - « marchand de listes », mention exclusive,
  - « prestations touristiques »,
  - « prestation de services » pour les personnes non établies en France et n'exerçant pas dans le cadre de la libre prestation de services ;
- l’identité du titulaire et l’adresse de son établissement principal ;
- l’identité du président et le nom de la chambre de commerce et d’industrie territoriale ou départementale ayant délivré la carte ;
- le numéro et les dates de début et de fin de validité de la carte ;
- le numéro unique d’identification de l’entreprise.

Pour obtenir sa carte professionnelle, l'intéressé doit justifier :

- de son aptitude professionnelle qui peut être :
  - un diplôme ou un titre de formation (cf. infra « 2°. a. Formation »),
  - et/ou une expérience professionnelle (cf. infra « 2°. a. Expérience professionnelle »),
  - pour le ressortissant d’un autre État membre de l’Union européenne (UE) ou partie à l’Espace économique européen (EEE) qui réglemente l'activité d'agent immobilier, une attestation de compétence ou un titre de formation permettant l'accès ou l'exercice à tout ou partie de l'activité, délivré par l’autorité compétente de cet État,
  - ou pour le ressortissant d’un autre État membre de l’Union européenne (UE) ou partie à l’Espace économique européen (EEE) qui ne réglemente pas l’activité d’agent immobilier, une attestation de compétence ou un titre de formation attestant de la préparation à l’exercice de tout ou partie de l’activité. Dans ce cas, la personne doit justifier d’une expérience professionnelle à temps plein ou équivalent d’au moins 1 an au cours des 10 dernières années dans un État membre de l’Union européenne (UE) ou partie à l’Espace économique européen (EEE) qui ne réglemente pas l’activité sauf si le diplôme prépare spécifiquement à l’activité d’agent immobilier ;
- le cas échéant, d’une garantie financière ;
- d’une assurance de responsabilité civile ;
- de ne pas être frappé d’une incapacité ou d’une interdiction d’exercer.

**À noter**

Un professionnel de l’immobilier peut être habilité à exercer pour le compte d’un agent titulaire d’une carte professionnelle. Le cas échéant, il sera soumis aux conditions d’incapacité et d’assurance au même titre que ceux dotés de la carte professionnelle.

*Pour aller plus loin* : articles 3 et 4 de la loi du 2 janvier 1970.

#### Formation

Pour être qualifié de professionnel de l’immobilier, l’intéressé doit être titulaire de l’un des titres suivants ou justifier d'une expérience professionnelle :

##### Diplôme

- Diplôme d’études supérieures de niveau licence (bac +3) et sanctionnant des études juridiques, économiques ou commerciales ;
- Diplôme ou titre inscrit au Répertoire national des certifications professionnelles d’un niveau équivalent (bac +3) sanctionnant des études juridiques, économiques ou commerciales ;
- Brevet de technicien supérieur (BTS) « Professions immobilières » ;
- Diplôme de l’Institut d’études économiques et juridiques appliquées à la construction et à l’habitation.

##### Expérience professionnelle

- Exercice pendant au moins 10 ans, à temps plein ou équivalent, de l’une des activités correspondant à la mention demandée en tant que salarié. Cette durée est réduite à 4 ans (temps plein ou équivalent) lorsqu’il s’agit d’un emploi de cadre ou d’un emploi public de niveau A ou équivalent ;
- Ou exercice pendant 3 ans, à temps plein ou équivalent, de l’une des activités correspondant à la mention demandée en tant salarié lorsque la demandeur est titulaire d’un BAC ou d’un diplôme ou titre inscrit au Répertoire national des certifications professionnelles d’un niveau équivalent et sanctionnant des études juridiques, économiques ou commerciales.

**Coûts associés à la qualification**

La formation menant à la qualification professionnelle est payante et son coût varie selon le cursus envisagé. Pour plus de précisions, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissants de l’UE ou de l’EEE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d’un État membre de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE) qui est établi et exerce légalement des activités immobilières dans cet État peut exercer les mêmes activités en France de façon temporaire ou occasionnelle.

Il devra, pour cela, effectuer une déclaration préalable auprès du président de la chambre de commerce et d’industrie territoriale ou de la chambre départementale d’Île-de-France (cf. infra « 5°. a. Demander une déclaration préalable, pour le ressortissant, en vue d’un exercice temporaire ou occasionnel (LPS) »).

Toutefois, lorsque l’activité professionnelle n'est pas réglementées dans l’État de l’UE ou de l’EEE, l’intéressé peut exercer en France de manière temporaire ou occasionnelle dès lors qu’il justifie avoir exercé une telle activité pendant au moins un an au cours des dix années précédant la prestation.

En outre, le ressortissant pourra demander la délivrance d’une carte professionnelle européenne (cf. infra « 5°. c. Carte professionnelle européenne (CPE) pour le ressortissant exerçant l’activité d’agent immobilier »).

*Pour aller plus loin* : article 8-1 de la loi du 2 janvier 1970.

### c. Ressortissants de l’UE ou de l’EEE : en vue d’un exercice permanent (Libre Établissement)

Pour exercer l’une des activités de professionnel de l’immobilier à titre permanent en France, le ressortissant de l’UE ou de l’EEE devra remplir l’une des conditions énoncées plus haut (cf. supra « 2°. Qualifications professionnelles »).

Dès lors que le ressortissant remplit l’une de ces conditions, il pourra demander la délivrance d’une carte professionnelle auprès du président de la chambre de commerce et d’industrie territoriale ou de la chambre départementale d’Île-de-France dans le ressort de laquelle se trouve son principal établissement (cf. infra « 5°. b. Demander une carte professionnelle pour le ressortissant en vue d’un exercice permanent (LE) »).

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### a. Règles déontologiques 

Le professionnel de l’immobilier est soumis, durant toute la durée de son exercice, au respect des règles édictées par le Code de déontologie des agents immobiliers, des administrateurs de biens, des syndics de copropriété et des marchands de listes.

À ce titre, il doit respecter un certain nombre de règles et notamment :

- de ne commettre aucune discrimination ;
- d’agir avec ses clients en toute transparence ;
- de respecter la confidentialité des affaires qu’il traite ;
- d’éviter tout conflit d’intérêts.

*Pour aller plus loin* : annexe 1 du décret n° 2015-1090 du 28 août 2015 fixant les règles constituant le Code de déontologie applicable à certaines personnes exerçant les activités de transaction et de gestion des immeubles et des fonds de commerce.

### b Sanctions disciplinaires

En cas de sanction, le professionnel de l'immobilier peut encourir une peine disciplinaire, à savoir :

- un avertissement ;
- un blâme ;
- une interdiction temporaire d’exercer pour une durée maximale de trois ans ;
- une interdiction définitive d’exercer.

*Pour aller plus loin* : articles 13-4 et suivants de la loi du 2 janvier 1970.

### c. Sanctions pénales et administratives

Le fait pour un professionnel de l’immobilier d’exercer illégalement est puni de six mois d’emprisonnement et de 7 500 euros d’amende. Il encourt également une peine de deux ans d’emprisonnement et de 30 000 euros d’amende dès lors qu’il détient illégalement des fonds ou des biens au cours de son exercice.

Le professionnel de l’immobilier encourt également une amende administrative en cas de violation des règles de publicité prévues à l’article L111-1 du Code de la consommation.

*Pour aller plus loin* : articles 14 et suivants de la loi du 2 janvier 1970.

## 4°. Formation continue et assurances

### a. Formation continue

Une formation continue de 14 heures par an ou de 42 heures au cours de trois années consécutives d’exercice est obligatoire pour tout professionnel de l’immobilier. Le renouvellement de sa carte professionnelle est subordonné au respect de cette exigence.

*Pour aller plus loin* : articles 1er et suivants du décret n° 2016-173 du 18 février 2016 relatif à la formation continue des professionnels de l’immobilier.

### b. Garantie financière

Le professionnel de l’immobilier, titulaire de la carte professionnelle ou qui en fait la demande, doit solliciter une garantie financière d’un montant au moins égal au montant maximal des fonds qu’il envisage de détenir. Cette garantie vise à couvrir les créances nées au cours de son activité. De plus, le titulaire de la carte ne pourra recevoir de versements ou de remises que dans la limite du montant de cette garantie. Dès lors qu’il a souscrit cette garantie, l'établissement financier lui délivre une attestation de garantie.

*Pour aller plus loin* : articles 27 et suivants du décret du 20 juillet 1972.

### c. Assurance de responsabilité civile professionnelle

En qualité de professionnel de l’immobilier, le ressortissant devra être en possession d’un contrat d’assurance comportant ses coordonnées ainsi que celles de l’organisme d’assurance afin de le couvrir des risques financiers dans le cadre de son activité. La limite de la garantie ne peut être inférieure à 76 224,51 euros par an pour un même assuré.

*Pour aller plus loin* : article 49 du décret du 20 juillet 1972 ; arrêté du 1er juillet 2005 fixant les conditions d’assurance et la forme du document justificatif prévu par le décret du 20 juillet 1972.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demander une déclaration préalable pour le ressortissant en vue d’un exercice temporaire ou occasionnel (LPS)

#### Autorité compétente

Le président de la chambre de commerce et d’industrie territoriale ou de la chambre départementale d’Île-de-France dans le ressort de laquelle le ressortissant envisage de faire sa première prestation est compétent pour se prononcer sur la demande de déclaration.

#### Pièces justificatives

La demande peut être adressée par lettre recommandée ou par voie électronique et doit être accompagnée d’un dossier comprenant les pièces justificatives suivantes :

- une attestation justifiant que le professionnel de l’immobilier est légalement établi dans un État membre de l’UE ou de l’EEE et qu’il n’encourt aucune interdiction d’exercer ;
- lorsque l’État membre ne réglemente pas l’activité, tout document justifiant d’une expérience professionnelle pendant au moins un an au cours des dix années précédant la prestation ;
- un certificat de nationalité ;
- une attestation de garantie financière ;
- une attestation d’assurance de responsabilité civile professionnelle ;
- le cas échéant, une déclaration sur l’honneur qu’il ne détient aucun fonds, effet ou valeur autre que ceux représentatifs de sa rémunération.

#### Coût

La demande doit être accompagnée des frais de dossier d'instruction d'un montant de 96 euros.

*Pour aller plus loin* : article 16-6 du décret du 20 juillet 1972.

### b. Demander une carte professionnelle pour le ressortissant en vue d’un exercice permanent (LE)

#### Autorité compétente

Le président de la chambre de commerce et d’industrie territoriale ou de la chambre départementale d’Île-de-France est compétent pour délivrer la carte professionnelle au ressortissant qui souhaite exercer en France.

#### Pièces justificatives

La demande, transmise par lettre recommandée, devra être accompagnée d’un dossier comprenant les pièces justificatives suivantes :

- le formulaire [Cerfa n° 15312*01](https://www.service-public.fr/professionnels-entreprises/vosdroits/R13999) rempli et signé ;
- un justificatif d’aptitude professionnelle du demandeur ;
- le cas échéant, une attestation de garantie financière ;
- une attestation d’assurance de responsabilité civile professionnelle ;
- un extrait du registre du commerce et des sociétés (RCS) datant de moins d’un mois si la personne est immatriculée, ou du double de la demande ;
- suivant le cas :
  - soit une attestation comportant le numéro de compte délivrée par l’établissement de crédit qui a ouvert le compte,
  - soit une attestation d’ouverture au nom de chaque mandant des comptes bancaires ;
- le cas échéant, la déclaration sur l’honneur que le demandeur ne détient, directement ou indirectement, aucun autre fonds que ceux représentant sa rémunération ;
- lorsque le demandeur est ressortissant d’un État membre de l’UE, une autorisation pour la communication du casier judiciaire du pays d’origine à l’autorité française ;
- lorsque le demandeur est ressortissant d’un État non membre de l’UE, un extrait de casier judiciaire datant de moins de 3 mois délivré par l'autorité compétente de cet État.

#### Coût

La demande doit être accompagnée des [frais de dossier d’instruction]( https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier) d’un montant de 120 euros.

#### Délais

Si le dossier est incomplet, l’autorité compétente adresse au professionnel la liste des pièces manquantes dans un délai de quinze jours à compter de sa réception. Si son dossier n’est pas complété dans les deux mois suivants, sa demande de carte sera rejetée.

*Pour aller plus loin* : articles 2 et suivants du décret du 20 juillet 1972.

### c. Carte professionnelle européenne (CPE) pour le ressortissant exerçant l’activité d’agent immobilier

La carte professionnelle européenne est une procédure électronique permettant de faire reconnaître des qualifications professionnelles dans un autre État de l’UE.

La procédure CPE peut être utilisée à la fois lorsque le ressortissant souhaite exercer son activité dans un autre État de l’UE :

- à titre temporaire ou occasionnel ;
- à titre permanent.

La CPE est valide :

- indéfiniment en cas d’établissement à long terme ;
- dix-huit mois pour la prestation de services à titre temporaire.

#### Demande de carte professionnelle européenne

Pour demander une CPE, le ressortissant doit :

- créer un compte utilisateur sur le [service d’authentification de la Commission européenne](https://webgate.ec.europa.eu/cas) ;
- remplir ensuite son profil CPE (identité, coordonnées…).

**À noter**

Il est également possible de créer une demande de CPE en téléchargeant les pièces justificatives scannées.

#### Coût

Pour chaque demande de CPE, les autorités du pays d’accueil et du pays d’origine peuvent prélever des frais d’examen de dossier dont le montant varie selon les situations.

#### Délais

Dans le cas d'une demande de CPE pour une activité temporaire et occasionnelle, l’autorité du pays d’origine accuse réception de la demande de CPE dans un délai d’une semaine, signale s’il manque des documents et informe des frais éventuels. Puis, les autorités du pays d’accueil contrôlent le dossier.

Si aucune vérification n’est nécessaire auprès du pays d’accueil, l’autorité du pays d’origine examine la demande et prend une décision finale dans un délai de trois semaines.

Si des vérifications sont nécessaires au sein du pays d’accueil, l’autorité du pays d’origine a un mois pour examiner la demande et la transmettre au pays d’accueil. Le pays d’accueil prend alors une décision finale dans un délai de trois mois.

Dans le cas d'une demande de CPE pour une activité permanente, l’autorité du pays d’origine accuse réception de la demande de CPE dans un délai d’une semaine, signale s’il manque des documents et informe des frais éventuels. Le pays d’origine dispose ensuite d’un délai d’un mois pour examiner la demande et la transmettre au pays d’accueil. Ce dernier prend la décision finale dans un délai de trois mois.

Si les autorités du pays d’accueil estiment que le niveau d’éducation ou de formation ou que l’expérience professionnelle sont inférieurs aux normes exigées dans ce pays, elles peuvent demander au demandeur de passer une épreuve d’aptitude ou d’effectuer un stage d’adaptation.

#### Issue de la demande de CPE

Si la demande d’obtention de CPE est accordée, il est alors possible d’obtenir un certificat de CPE en ligne.

Si les autorités du pays d’accueil ne prennent pas de décision dans les délais impartis, les qualifications sont tacitement reconnues et une CPE est délivrée. Il est alors possible d’obtenir un certificat de CPE à partir de son compte en ligne.

Si la demande d’obtention de CPE est rejetée, la décision de refus doit être motivée et présenter les voies de recours pour contester ce refus.

*Pour aller plus loin* : articles 16-8 et suivants du décret du 20 juillet 1972.

### d. Voies de recours

#### Centre d’assistance français
 
Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.
 
#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.
 
##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.
 
##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).