﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP012" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Estate agent - Commonhold association - Property manager" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="estate-agent-commonhold-association-property-manager" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/estate-agent-commonhold-association-property-manager.html" -->
<!-- var(last-update)="2020-04-15 17:20:59" -->
<!-- var(url-name)="estate-agent-commonhold-association-property-manager" -->
<!-- var(translation)="Auto" -->


Estate agent - Commonhold association - Property manager
================================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:59<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The real estate agent, the property administrator and the condominium trustee (known as "real estate professionals") are professionals whose missions are to participate in the operations on the property of others and related to:

- buying, selling, researching, exchanging, renting or subletting, seasonally or not, in nude or furnished, of built or unded buildings;
- Buying, selling or leasing commercial funds;
- the sale of a herd (cattle fund) dead or alive;
- underwriting, buying, selling shares or shares in real estate companies or housing companies;
- The purchase, sale of non-negotiable shares where the social asset includes a building or a commercial fund;
- Property management
- the sale of documents relating to the purchase, sale, rental or subletting, naked or furnished, of buildings built or not, or the sale of commercial funds (excluding publications by press);
- the conclusion of any timeshare building enjoyment contract;
- performing the duties of condominium trustee.

*For further information*: Articles 1 and following of Act 70-9 of January 2, 1970 regulating the conditions for the conduct of activities relating to certain transactions involving real estate and commercial funds.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out these activities, the person must have a business card with the following information:

- professional activities, namely, the following specialties:- "transactions in real estate and commercial funds"
  - "real estate management"
  - "co-ownership trustee"
  - "list merchant,"
  - 'tourism services',
  - "service delivery" for people in a free-supply situation;
- The identity of the holder and the address of his main institution;
- The identity of the president and the name of the chamber of commerce and territorial or departmental industry that issued the card;
- The number and dates of the card's start and end;
- The company's unique identification number.

To obtain a business card, the person must justify:

- professional fitness, which can be:- a diploma or a training title (see infra "2." a. Training"),
  - at least three years of professional experience in an activity corresponding to the mention requested on the business card,
  - for a national of another Member State of the European Union (EU) or a party to the European Economic Area (EEA), a certificate of competence or a training certificate issued by the competent authority of that state;
- A financial guarantee
- liability insurance
- not to be incapacitated or prohibited from practising.

**Please note**

A real estate professional may be eligible to practice on behalf of an agent with a business card. If necessary, he will be subject to the conditions of disability and insurance in the same way as those with the professional card.

*For further information*: Sections 3 and 4 of the Act of January 2, 1970.

#### Training

To qualify as a real estate professional, the individual must hold one of the following securities:

- a bachelor's degree (B.A. 3) and a law degree;
- The diploma or title in the national directory of professional certifications of an equivalent level (B.A. 3);
- The Senior Technician's Patent (BTS) "Real Estate Professions";
- Institute for Economic and Legal Studies applied to construction and housing.

**Costs associated with qualification******

The training leading to professional qualification is paid for and the cost varies according to the course envisaged. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) which is established and legally carries out real estate activities in that state may carry out the same activities in France on a temporary and Occasional.

In order to do so, he will have to make a prior declaration with the president of the Chamber of Commerce and Territorial Industry or the departmental chamber of Ile-de-France (see infra "5o). a. Request a pre-declaration, for the national, for a temporary or casual exercise (LPS) ").

However, where neither professional activity nor training is regulated in the EU or EEA State, the person concerned may exercise in France on a temporary and occasional basis if he justifies having carried out such activity during the less than one year in the ten years prior to the benefit.

In addition, the national will be able to apply for a European business card (see infra "5°. c. European Professional Card (CPE) for nationals working as a real estate agent").

*For further information*: Article 8-1 of the Act of January 2, 1970.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

In order to carry out one of the activities of a real estate professional on a permanent basis in France, the EU or EEA national will have to meet one of the following conditions:

- Hold a certificate of competency or a training certificate issued by a competent authority of the Member State of origin;
- have been in business for at least one year in the previous ten years when the EU or EEA State does not regulate this occupation.

As soon as the national fulfils one of these conditions, he can apply for the issuance of a professional card from the president of the Chamber of Commerce and Territorial Industry or the departmental chamber of Ile-de-France in the jurisdiction which is its main establishment (see infra "5°. b. Ask for a business card for the national for a permanent exercise (LE) ").

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethical Rules

The real estate professional is subject, throughout his practice, to compliance with the rules of the Code of Ethics of real estate agents, property administrators, condominium trustees and list dealers.

As such, it must comply with a number of rules, including:

- not to discriminate;
- Acting with customers transparently
- Respect the confidentiality of the business it handles;
- to avoid any conflict of interest.

*For further information*: Appendix 1 of Decree No. 2015-1090 of August 28, 2015 setting out the rules for the Code of Ethics applicable to certain persons engaged in the transaction and management of buildings and commercial funds.

### c. Disciplinary sanctions

In the event of a sanction, the real estate professional may incur a disciplinary penalty, namely:

- A warning
- Blame
- a temporary ban on practising for up to three years;
- a permanent ban on practising.

*For further information*: Sections 13-4 and the following of the Act of January 2, 1970.

### d. Criminal and administrative sanctions

The fact that a real estate professional practises illegally is punishable by six months' imprisonment and a fine of 7,500 euros. He also faces a two-year prison sentence and a fine of 30,000 euros if he illegally holds funds or property during his fiscal year.

The real estate professional also incurs an administrative fine for violating the advertising rules provided for by the[Article L111-1 of the Consumer Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020625532&cidTexte=LEGITEXT000006069565).

*For further information*: Section 14 and following of the Act of January 2, 1970.

4°. Continuing education and insurance
----------------------------------------------------------

### a. Continuing education

Continuous training of 14 hours per year or 42 hours during three consecutive years of practice is mandatory for any real estate professional. The renewal of his business card is conditional on compliance with this requirement.

*For further information*: Articles 1 and following of Decree No. 2016-173 of February 18, 2016 relating to the continuing education of real estate professionals.

### b. Financial guarantee

The real estate professional, who holds or is applying for the business card, must apply for a financial guarantee of at least the maximum amount of funds he plans to hold. This guarantee is intended to cover receivables that have been designed during its activity. In addition, the cardholder will only be able to receive payments or discounts within the amount of that guarantee. Once he has taken out this guarantee, the deposit and deposit fund issues him a certificate of guarantee.

*For further information*: Articles 27 and following of the decree of July 20, 1972.

### c. Professional liability insurance

As a real estate professional, the national must be in possession of an insurance contract with his contact information as well as those of the insurance agency in order to cover him financial risks in the course of his activity. The limit of the guarantee cannot be less than 76,224.51 euros per year for the same insured.

*For further information*: Article 49 of the decree of 20 July 1972; decree of 1 July 2005 setting out the conditions of insurance and the form of the supporting document provided by the decree of 20 July 1972.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request a pre-report for the national for a temporary or casual exercise (LPS)

**Competent authority**

The president of the Chamber of Commerce and Territorial Industry or the departmental chamber of Ile-de-France in which the national plans to make his first benefit is competent to decide on the request for declaration.

**Supporting documents**

The request may be made by recommended letter or electronically and must be accompanied by a file containing the following supporting documents:

- a certificate justifying that the real estate professional is legally established in an EU or EEA Member State and that he is not prohibited from practising;
- Where the Member State does not regulate the activity, any document justifying work experience for at least one year in the ten years preceding the benefit;
- A certificate of nationality
- A financial guarantee certificate
- a certificate of professional liability insurance;
- if so, a statement on the honour that he does not hold any funds, effect or value other than those representative of his remuneration.

*For further information*: Article 16-6 of the decree of 20 July 1972.

### b. Apply for a business card for the national for a permanent exercise (LE)

**Competent authority**

The president of the Chamber of Commerce and Territorial Industry or the Departmental Chamber of Ile-de-France is responsible for issuing the professional card to a national who wishes to practice in France.

**Supporting documents**

The request, sent by recommended letter, must be accompanied by a file containing the following supporting documents:

- The form[Cerfa 15312*01](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15312.do) completed and signed;
- Proof of the applicant's professional fitness
- A financial guarantee certificate
- a certificate of professional liability insurance;
- an excerpt from the Register of Trade and Companies (RCS) less than one month old if the person is registered, or double the demand;
- depending on the case:- certificate with the account number issued by the credit institution that opened the account,
  - or a certificate of openness on behalf of each principal of bank accounts;
- if applicable, the declaration of honour that the applicant does not hold, directly or indirectly, any funds other than those representing his remuneration;
- bulletin 2 of the national criminal record or an equivalent for nationals of another EU Member State.

**Cost**

The request must be accompanied by[Investigation file fees](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier) amounting to 120 euros.

**Timeframe**

If the file is incomplete, the competent authority sends the professional a list of missing documents within a fortnight of receiving it. If his file is not completed within two months, his application for a card will be rejected.

*For further information*: Articles 2 and following of the decree of July 20, 1972.

### c. European Professional Card (CPE) for nationals working as a real estate agent

The European Professional Card is an electronic procedure for recognising professional qualifications in another EU state.

The CPE procedure can be used both when the national wishes to operate in another EU state:

- temporary and occasional;
- on a permanent basis.

The CPE is valid:

- indefinitely in the event of a long-term settlement;
- 18 months for the provision of services on a temporary basis.

**Application for a European business card**

To apply for a CPE, the national must:

- Create a user account on the[European Commission authentication service](https://webgate.ec.europa.eu/cas) ;
- then fill out his CPE profile (identity, contact information...).

**Please note**

It is also possible to create a CPE application by downloading the scanned supporting documents.

**Cost**

For each CPE application, the authorities of the host country and the country of origin may charge a file review fee, the amount of which varies depending on the situation.

**Timeframe**

In the case of a CPE application for a temporary and occasional activity, the country of origin authority acknowledges receipt of the CPE application within one week, reports if documents are missing and informs of any costs. Then, the host country's authorities check the case.

If no verification is required with the host country, the country of origin authority reviews the application and makes a final decision within three weeks.

If verifications are required within the host country, the country of origin authority has one month to review the application and forward it to the host country. The host country then makes a final decision within three months.

In the case of a CPE application for a permanent activity, the country of origin authority acknowledges receipt of the CPE application within one week, reports if documents are missing and informs of any costs. The country of origin then has one month to review the application and forward it to the host country. The latter makes the final decision within three months.

If the host country authorities believe that the level of education or training or work experience is below the standards required in that country, they may ask the applicant to take an aptitude test or to complete an internship adaptation.

**Issue of CPE application**

If the application for CPE is granted, then it is possible to obtain a CPE certificate online.

If the host country's authorities do not make a decision within the allotted time, qualifications are tacitly recognised and a CPE is issued. It is then possible to obtain a CPE certificate from your online account.

If the application for CPE is rejected, the decision to refuse must be justified and bring the remedies to challenge that refusal.

*For further information*: Articles 16-8 and following of the decree of July 20, 1972.

### d. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

