﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP080" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Conseiller en génétique" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="conseiller-en-genetique" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/conseiller-en-genetique.html" -->
<!-- var(last-update)="2020-04-15 17:21:03" -->
<!-- var(url-name)="conseiller-en-genetique" -->
<!-- var(translation)="None" -->

# Conseiller en génétique

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:03<!-- end-var -->

## 1°. Définition de l’activité

Le conseiller en génétique est un professionnel qui, au sein d'une équipe pluridisciplinaire et sous la responsabilité d'un médecin généticien, a pour missions :

- d'évaluer les risques de transmission ;
- d'informer les patients et leurs familles atteints d'une pathologie génétique ou qui sont susceptibles de l'être ;
- d'assurer leur prise en charge sur le plan médical et social.

**À noter**

Ces professionnels sont présents dans tous les services de génétique médicale hospitaliers et exercent leur activité au sein d'établissements de santé publics ou privés.

*Pour aller plus loin* : article L. 1132-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer la profession de conseiller en génétique, l'intéressé doit être titulaire soit :

- d'un diplôme national de niveau master (bac +5) en sciences de la santé mention pathologie humaine, spécialité conseil en génétique et médecine prédictive, délivré par l'Université Aix-Marseille II ;
- d'une autorisation d'exercice délivrée par le préfet régional (cf. infra «Demande d'autorisation d'exercice »).

*Pour aller plus loin* : article L. 1132-2 et suivants du Code de la santé publique.

#### Formation

##### Diplôme national spécialité conseil en génétique

Ce diplôme est accessible aux étudiants issus des :

- professions paramédicales suivantes :
  - infirmiers,
  - psychologues,
  - kinésithérapeutes ;
- professions scientifiques suivantes :
  - ingénieurs,
  - étudiants de formation en biologie d'un niveau licence ou équivalent (bac +3) ;
- professions médicales suivantes :
  - médecins, pharmaciens, dentistes,
  - sages-femmes,
  - étudiants ayant validé la troisième année de médecine, de pharmacie ou d'école dentaire.

La formation, d'une durée de deux ans, se compose de modules :

- théoriques en vue d'acquérir :
  - les connaissances générales en génétique,
  - les principes de la relation soignant-patient,
  - l'éthique de la profession,
  - la psychologie ;
- en conseil génétique et pathologies abordant les aspects génétiques et médicaux des maladies génétiques ;
- optionnels d'approfondissement en vue d'acquérir les connaissances complémentaires de la spécialité choisie.

Pour plus d'informations, il est conseillé de se reporter au site de l'[Université Aix-Marseille II](https://medecine.univ-amu.fr/).

#### Demande d'autorisation d'exercice

Lorsque l'intéressé ne justifie pas être titulaire du diplôme visé ci-avant mais a exercé des fonctions dévolues au conseil en génétique, il peut faire une demande d'autorisation d'exercice auprès du préfet de région.

##### Pièces justificatives

Pour obtenir cette autorisation, le professionnel doit adresser, par voie postale, un dossier en double exemplaire comprenant :

- ses coordonnées complètes (nom, prénom, copie d'une pièce d'identité) ;
- une lettre de demande d'autorisation d'exercice de la profession de conseiller en génétique ;
- la copie de ses diplômes.

##### Délai

Dès réception de son dossier par le préfet de région, le demandeur recevra un accusé de réception et la notification l'autorisant ou non à exercer la profession.

L'absence de réponse après un délai de deux mois vaudra rejet de sa demande.

*Pour aller plus loin* : décret n° 2007-1429 du 3 octobre 2007 relatif à la profession de conseiller en génétique et modifiant le Code de la santé publique (dispositions réglementaires).

#### Demande d'agrément en vue d'un examen des caractéristiques et des empreintes génétiques

Le professionnel, qui souhaite pratiquer un examen des caractéristiques génétiques d'une personne ou de son identification par empreintes génétiques à des fins médicales, doit effectuer une demande d'agrément auprès de [l'Agence de biomédecine](https://www.agence-biomedecine.fr/agrement-praticiens-genetique).

Cet agrément est valable pendant cinq ans.

*Pour aller plus loin* : décret n° 97-109 du 6 février 1997 relatif aux conditions d'agrément des personnes habilitées à procéder à des identifications par empreintes génétiques dans le cadre d'une procédure judiciaire ou de la procédure extrajudiciaire d'identification des personnes décédées.

#### Coûts associés à la qualification

La formation menant à l'obtention du titre de conseiller en génétique est payante et son coût varie selon le cursus envisagé. Pour plus d'informations, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), qui est établi et exerce légalement l'activité de conseiller en génétique, peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il devra, pour cela, adresser préalablement à l'exécution de la prestation de services, une déclaration préalable auprès du préfet de région (cf. infra « 5°. a. Demande de déclaration préalable en vue d'un exercice à titre temporaire et occasionnel (LPS) » ).

Toutefois, lorsque ni l’activité professionnelle ni la formation ne sont réglementées dans l’État de l’UE ou de l’EEE, l’intéressé peut exercer en France de manière temporaire et occasionnelle dès lors qu’il justifie avoir exercé une telle activité, pendant au moins un an au cours des dix dernières années précédant la prestation.

*Pour aller plus loin* : articles L. 1132-5 et R. 1132-4 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre exercice)

Pour exercer l'activité de conseiller en génétique, à titre permanent sur le territoire français, le ressortissant d'un État membre de l'UE ou de l'EEE doit être titulaire :

- soit d'un titre de formation délivré par un État membre qui réglemente l'accès à la profession ou son exercice et qui permet d'accéder à ces fonctions dans cet État ;
- soit, lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l’État de l'UE ou de l'EEE, d'une attestation de formation justifiant que l'intéressé à suivi une formation et a exercé cette activité pendant un an, à temps plein ou à temps partiel au cours des dix dernières années ;
- soit d'un titre de formation permettant d'accéder à la profession et la preuve que l'intéressé a exercé l'activité pendant trois ans à temps plein ou à temps partiel.

Le conseiller en génétique devra au préalable effectuer une demande d'autorisation d'exercice auprès du préfet de région (cf. infra « 5°. b. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : article L. 1132-3 du Code de la santé publique.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Règles professionnelles

Le professionnel est soumis aux règles professionnelles suivantes :

- respecter la vie et la personne humaine. Le professionnel doit, à ce titre, agir dans l'intérêt de ses patients et respecter leur dignité et leur intimité ;
- assurer le suivi de la prise en charge de ses patients ;
- respecter le secret professionnel ;
- respecter la prescription médicale du médecin prescripteur ;
- fournir aux personnes qui le consultent toutes les informations et explications nécessaires de manière adaptée, intelligibles, précises et loyales ;
- faire preuve de non-discrimination.

*Pour aller plus loin* : article R. 1132-7 et suivants du Code de la santé publique.

**À noter**

Le professionnel de la génétique est également soumis, lors de tout examen des caractéristiques génétiques d'une personne à des fins médicales, au respect des bonnes pratiques énoncées en annexe de l'arrêté du 27 mai 2013 définissant les règles de bonnes pratiques applicables à l'examen des caractéristiques génétiques d'une personne à des fins médicales.

### Cumul d'activités

Le conseiller en génétique ne peut exercer une autre activité professionnelle que si un tel cumul est compatible avec la dignité et la qualité qu'exige sa profession.

*Pour aller plus loin* : article R. 1132-6 du Code de la santé publique.

### Sanctions pénales

Le conseiller en génétique encourt une peine d'un an d'emprisonnement et une amende de 15 000 euros dès lors qu'il :

- procède à l'étude des caractéristiques génétiques d'une personne à des fins autres que médicales ou de recherche ou sans avoir recueilli son consentement ;
- détourne de leurs finalités médicales ou de recherche, les informations recueillies sur une personne via l'examen de ses caractéristiques génétiques ;
- recherche l'identification d'une personne par ses empreintes génétiques en dehors d'une mesure d'enquête ou d'instruction au cours d'une procédure de vérification d'un acte de l'état civil par les autorités diplomatiques ou consulaires.

*Pour aller plus loin* : article R. 1132-7 et suivants du Code de la santé publique.

## 4°. Assurances

En qualité de professionnel de la santé exerçant à titre libéral, le conseiller en génétique doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande de déclaration préalable en vue d'un exercice à titre temporaire et occasionnel (LPS)

Avant toute prestation de services, le ressortissant d'un État de l'UE ou de l'EEE, doit adresser une demande de déclaration préalable au préfet de la région dans laquelle il souhaite exercer.

Cette déclaration doit être accompagnée des pièces justificatives suivantes :

- une copie de sa pièce d'identité en cours de validité ou tout document attestant de sa nationalité ;
- une copie de son titre de formation ;
- une attestation de moins de trois mois certifiant qu'il est légalement établi dans un État membre et qu'il ne fait l'objet d'aucune interdiction d'exercice ;
- lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l’État de l'UE ou de l'EEE, la preuve qu'il a exercé cette activité pendant un an au cours des dix dernières années à temps plein ou à temps partiel ;
- lorsque le titre de formation a été délivré par un État tiers à l'UE mais reconnu dans un État membre autre que la France :
  - la reconnaissance du titre de formation établi par l’État l'ayant reconnu,
  - tout document justifiant qu'il a exercé la profession pendant trois ans à temps plein ou à temps partiel.

#### Procédure

Dans un délai d'un mois à compter de la réception de la déclaration, le préfet de la région informe le demandeur soit :

- qu'il peut débuter la prestation de services sans vérification préalable de ses qualifications professionnelles ;
- qu'il existe une différence substantielle entre ses qualifications professionnelles et la formation exigée en France, de sorte que le demandeur doit se soumettre à une épreuve d'aptitude en vue de justifier qu'il a acquis les connaissances nécessaires à l'exercice de l'activité en France ;
- qu'il ne peut pas débuter sa prestation de services.

Durant ce même délai, dès lors qu'une difficulté cause un retard dans sa prise de décision, le préfet de région en informe le professionnel et doit prendre sa décision soit :

- dans les deux mois suivants la résolution de la difficulté ;
- dans les trois mois à compter de la date à laquelle le prestataire a été informé de l'existence de cette difficulté.

#### Délais

En l'absence de réponse du préfet de région dans un délai d'un mois, le demandeur peut commencer sa prestation de services.

**À noter**

La déclaration est renouvelable tous les ans. En cas de changement de sa situation, le professionnel doit les déclarer.

*Pour aller plus loin* : articles R. 1132-4 et R. 4331-12 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestations de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### b. Demande d'autorisation d'exercice pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

Le conseiller en génétique doit adresser sa demande, par lettre recommandée, en double exemplaire au préfet de région après avis de la commission des conseillers en génétique.

#### Pièces justificatives

La demande prend la forme d'un dossier comportant les pièces justificatives suivantes :

- un [formulaire de demande d'autorisation d'exercice](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CC409C087758F57A925D6603C395546.tplgfr40s_2?idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180112), complété et signé ;
- une photocopie de la pièce d'identité du demandeur en cours de validité ;
- la copie du titre de formation permettant l'exercice de la profession ;
- tout document de moins d'un an, justifiant des formations et de l'expérience professionnelle du demandeur.

*Pour aller plus loin* : article L. 1132-3 , arrêté du 20 janvier 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de conseiller en génétique, infirmier, masseur-kinésithérapeute, pédicure-podologue, ergothérapeute, manipulateur en électroradiologie médicale et diététicien.

#### Procédure

Le professionnel doit, dans un délai de deux mois, indiquer son choix au préfet de région qui lui indiquera alors la listes des directions régionales de la jeunesse, des sports et de la cohésion sociale compétentes pour les organiser.

Le professionnel devra déposer une demande d'inscription aux épreuves sur papier libre, ainsi qu'une copie de la décision du préfet de région précisant la nature et la durée de l'épreuve.

#### Délais et issue de la procédure

Dans un délai d'un mois avant son début, le professionnel reçoit une convocation à l'épreuve d'aptitude ou au stage d'adaptation.

À l'issue de l'épreuve, le préfet de région autorise, en cas de réussite, le professionnel à exercer la profession de conseiller en génétique. À défaut, l'autorisation d'exercice lui est refusée.

#### Bon à savoir : mesures de compensation

Le préfet de région compétent pour délivrer l'autorisation d'exercice peut décider que le ressortissant accomplira au choix :

- soit une épreuve d'aptitude, composée d'une série d'interrogations écrites et notées ;
- soit un stage d'adaptation au sein d'un établissement de santé, et donnant lieu à un [rapport d'évaluation](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022023573&fastPos=2&fastReqId=1524346737&categorieLien=cid&oldAction=rechTexte) de la part du responsable pédagogique du stagiaire.

*Pour aller plus loin* : arrêté du 24 mars 2010 fixant les modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation pour l'exercice en France de conseiller en génétique, masseur-kinésithérapeute, pédicure-podologue, ergothérapeute, manipulateur d'électroradiologie médicale et diététicien par des ressortissants des États membres de l'Union européenne ou partie à l'accord sur l'Espace économique européen.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).