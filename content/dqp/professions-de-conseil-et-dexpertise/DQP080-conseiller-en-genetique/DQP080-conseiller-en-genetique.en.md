﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP080" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Genetics counsellor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="genetics-counsellor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/genetics-counsellor.html" -->
<!-- var(last-update)="2020-04-15 17:21:03" -->
<!-- var(url-name)="genetics-counsellor" -->
<!-- var(translation)="Auto" -->


Genetics counsellor
===================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:03<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The genetic counsellor is a professional who, within a multidisciplinary team and under the responsibility of a geneticist, has the mission of:

- Assess the risk of transmission
- inform patients and their families with or are likely to be diagnosed with genetic diseases;
- medically and socially.

**Please note**

These professionals are present in all hospital medical genetics services and operate in public or private health facilities.

*For further information*: Article L. 1132-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practise as a genetic counsellor, the person must hold either:

- a national master's degree (B.A. 5) in health sciences, mentioning human pathology, specialty of genetics counselling and predictive medicine, issued by Aix-Marseille II University;
- authorization issued by the regional prefect (see below "Request for Authorization to Exercise").

*For further information*: Article L. 1132-2 and the following of the Public Health Code.

#### Training

**National Specialty Diploma Consulting in Genetics**

This diploma is available to students from:

- paramedic occupations:- Nurses
  - Psychologists
  - physiotherapists;
- science professions:- Engineers
  - Biology training students at a bachelor's degree or equivalent level (B.A. 3);
- medical professions:- doctors, pharmacists, dentists,
  - Midwives
  - students who have completed the third year of medicine, pharmacy or dental school.

The two-year training consists of modules:

- to acquire:- General knowledge of genetics,
  - The principles of the carer-patient relationship,
  - the ethics of the profession,
  - Psychology
- genetic counseling and pathologies addressing the genetic and medical aspects of genetic diseases;
- to gain the additional knowledge of the chosen specialty.

For more information, it is advisable to refer to the[Aix-Marseille University II](https://medecine.univ-amu.fr/).

**Application for permission to practice**

Where the individual does not justify holding the diploma referred to above but has held positions assigned to the genetics council, he may apply for leave to practice with the regional prefect.

**Supporting documents**

To obtain this authorization, the professional must mail a duplicate file including:

- full contact information (name, first name, copy of ID)
- A letter of application for authorisation to practise as a genetic counsellor;
- copying his diplomas.

**Timeframe**

Upon receipt of his file by the regional prefect, the applicant will receive an acknowledgement and notification authorizing him or not to practice the profession.

Failure to respond after a two-month delay will result in the rejection of his application.

*For further information*: Decree No. 2007-1429 of October 3, 2007 relating to the profession of genetic counsellor and amending the Public Health Code (regulatory provisions).

**Application for certification for a dna test and fingerprinting**

The professional, who wishes to perform a review of a person's genetic characteristics or his or her DNA identification for medical purposes, must apply for accreditation with[Biomedicine Agency](https://www.agence-biomedecine.fr/agrement-praticiens-genetique).

This accreditation is valid for five years.

*For further information*: Decree 97-109 of 6 February 1997 on the conditions of approval of persons entitled to make DNA identifications in the context of judicial proceedings or the out-of-court procedure for the identification of persons Deceased.

#### Costs associated with qualification

The training leading to the title of genetic counsellor is paid and the cost varies depending on the course envisaged. For more information, it is advisable to check with the institutions concerned.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, which is established and legally practises as a genetic counsellor, may exercise in France on a temporary and occasional, the same activity.

In order to do so, it will have to send a prior declaration to the prefect of the region (see infra "5o) before the delivery of the services. a. Request for pre-reporting for a temporary and casual exercise (LPS).

However, where neither professional activity nor training is regulated in the EU or EEA State, the person concerned may exercise in France on a temporary and occasional basis if he justifies having carried out such activity, during the less than one year in the last ten years prior to the benefit.

*For further information*: Articles L. 1132-5 and R. 1132-4 of the Public Health Code.

### c. EU nationals: for a permanent exercise (Free exercise)

In order to carry out the activity of genetic counsellor, on a permanent basis on French territory, the national of an EU or EEA Member State must be the holder:

- or a training certificate issued by a Member State that regulates access to or practising the profession and allows access to these functions in that state;
- either, where neither professional activity nor training is regulated in the EU or EEA State, a certificate of training justifying that the person undergoing training and has carried out this activity for one year, full-time or part-time Over the past decade
- either a training designation to access the profession and proof that the person has been in the job for three years full-time or part-time.

The genetic counsellor must first apply to the regional prefect for an exercise permit (see below "5°). b. Application for authorization of practice for the national for a permanent exercise (LE)).

*For further information*: Article L. 1132-3 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Professional rules**

The professional is subject to the following professional rules:

- respect for life and the human person. As such, the professional must act in the best interests of his patients and respect their dignity and privacy;
- Keep track of the care of patients
- Respect professional secrecy
- comply with the prescribing physician's medical prescription
- Provide all the necessary information and explanations to the people who consult it in an appropriate, intelligible, accurate and fair manner;
- non-discrimination.

*For further information*: Article R. 1132-7 and the following of the Public Health Code.

**Please note**

The genetics professional is also subject, when examining a person's genetic characteristics for medical purposes, to the good practices set out in the[decree of May 27, 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027513617&categorieLien=id) defining the rules of good practice for examining a person's genetic characteristics for medical purposes.

**Cumulative activities**

The genetic counsellor may only engage in any other professional activity if such a combination is compatible with the dignity and quality required by his profession.

*For further information*: Article R. 1132-6 of the Public Health Code.

**Criminal sanctions**

The genetic counsellor faces a one-year prison sentence and a fine of 15,000 euros if he:

- studies a person's genetic characteristics for non-medical or research purposes or without obtaining consent;
- diverts from their medical or research purposes, the information collected about a person through the examination of his genetic characteristics;
- seeks the identification of a person by his or her DNA fingerprints outside of an investigation or investigation measure during a procedure for verifying a civil registration by the diplomatic or consular authorities.

*For further information*: Article R. 1132-7 and the following of the Public Health Code.

4°. Insurance
---------------------------------

As a liberal health professional, the genetic counsellor must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for pre-reporting for a temporary and casual exercise (LPS)

Before any provision of services, the national of an EU or EEA state must apply for a pre-declaration to the prefect of the region in which he wishes to practise.

This statement must be accompanied by the following supporting documents:

- A copy of his valid ID or any document attesting to his nationality;
- A copy of his training title
- a certificate of less than three months certifying that it is legally established in a Member State and that there is no ban on practising;
- where neither professional activity nor training is regulated in the EU or EEA State, evidence that he has been in this activity for one year in the last ten years full-time or part-time;
- where the training certificate has been issued by a third state to the EU but recognised in a Member State other than France:- recognition of the training title established by the state that recognized it,
  - any documentation justifying that he has been practising for three years full-time or part-time.

**Procedure**

Within one month of receiving the declaration, the region's prefect informs the applicant:

- that he can begin the provision of services without prior verification of his professional qualifications;
- that there is a substantial difference between his professional qualifications and the training required in France, so that the applicant must submit to an aptitude test in order to prove that he has acquired the necessary knowledge to The exercise of activity in France;
- that it cannot begin its service delivery.

During this same period, if a difficulty causes a delay in his decision-making, the regional prefect informs the professional and must make his decision:

- Within two months of resolving the difficulty;
- three months from the date the claimant was informed of the existence of this difficulty.

**Timeframe**

If the regional prefect does not respond within one month, the applicant can begin providing services.

**Please note**

The declaration is renewable every year. If the situation changes, the professional must declare them.

*For further information*: Articles R. 1132-4 and R. 4331-12 of the Public Health Code; December 8, 2017 order on the prior declaration of services for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### b. Application for authorization of exercise for the national for a permanent exercise (LE)

**Competent authority**

The genetic counsellor must submit his application, by recommended letter, in a double copy to the regional prefect after advice from the Committee of Genetic Advisors.

**Supporting documents**

The application takes the form of a file with the following supporting documents:

- A[application form for authorization to practice](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CC409C087758F57A925D6603C395546.tplgfr40s_2?idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180112), completed and signed;
- A photocopy of the applicant's valid ID
- Copying the training title to practice the profession;
- any document less than one year old, justifying training and the applicant's professional experience.

*For further information*: Article L. 1132-3 , order of 20 January 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the practice in France of the professions of genetic counsellor, nurse, massage therapist, pedicure-podologist, occupational therapist, manipulator in medical electroradiology and dietician.

**Procedure**

The professional must, within two months, indicate his choice to the prefect of the region who will then indicate to him the list of regional directorates of youth, sports and social cohesion competent to organize them.

The professional will have to submit an application for the tests on free paper, as well as a copy of the decision of the regional prefect specifying the nature and duration of the test.

**Delays and outcome of the procedure**

Within one month of starting, the professional receives a summons to the aptitude test or the adjustment course.

At the end of the test, the regional prefect authorizes, if successful, the professional to practice the profession of genetic counsellor. Failing that, he is denied permission to practice.

**Good to know: compensation measures**

The regional prefect responsible for issuing the exercise authorization may decide that the national will perform at his choice:

- or an aptitude test, consisting of a series of written and rated questions;
- an adjustment course in a health care facility, and giving rise to a[evaluation report](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022023573&fastPos=2&fastReqId=1524346737&categorieLien=cid&oldAction=rechTexte) from the trainee's teaching manager.

*For further information*: decree of 24 March 2010 setting out the modalities of the organisation of the aptitude test and the adaptation course for the exercise in France of genetic counsellor, massage therapist, pedicure-podologist, occupational therapist, manipulator medical electroradiology and dietitian by nationals of the Member States of the European Union or party to the agreement on the European Economic Area.

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

