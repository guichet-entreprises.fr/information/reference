﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP104" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Teacher in secondary education" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="teacher-in-secondary-education" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/teacher-in-secondary-education.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="teacher-in-secondary-education" -->
<!-- var(translation)="Pro" -->

# Teacher in secondary education

Latest update: <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->


<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of the profession

A teacher in secondary education is a professional responsible for imparting all his/her knowledge in one or more subjects and assessing and individually supervising pupils at lower and upper secondary level.  

The profession can be practised by fully qualified teachers (*professeurs certifiés*) and teachers having passed the highest-level competitive examination in education (*professeurs agrégés*). 

## 2°. Professional qualifications

### a. National requirements

#### National legislation

Access to the teaching profession as a state teacher under the Ministry of Education is by competitive examination, the nature of which depends on the subject taught.  

Thus, to become a fully qualified teacher (*professeur certifié*), the professional must hold one of the following:

* A *certificat d'aptitude au professorat de l'enseignement du second degré* (CAPES, secondary school teaching certificate) for general subjects
* A *certificat d'aptitude au professorat d'éducation physique et sportive* (CAPEPS, physical education teaching certificate) for physical education (PE)
* A *certificat d'aptitude au professorat de l'enseignement technique* (CAPET, technical education teaching certificate) for technical subjects
* A *certificat d'aptitude au professorat de l'enseignement du second degré agricole* (CAPESA, secondary-level agricultural education teaching certificate).  

**Please note**

A teacher wishing to teach in a state technical college (*lycée professionnel*) must pass the competitive examination to become a technical college teacher (CAPLP, *concours d'accès au corps des professeurs de lycée professionnel*).  

To become a *professeur agrégé*, the teacher must have passed the *agrégation* competitive examination (see below: “*Agrégation*”).  

#### Training

**To become a *professeur certifié* (fully qualified teacher)**

Candidates can become a *professeur certifié* by:

* passing a competitive examination to obtain a teaching certificate, or
* registering on a list of suitable candidates. Depending on requirements, up to nine appointments, decided the previous year, will be made from candidates:
  * holding a degree (three years of higher education), qualification or diploma allowing them to teach the subject required,
  * aged under forty,
  * having taught for at least ten years, five of which with tenure.

After recruitment through registration on a list of suitable candidates, the teacher will have to do a one-year placement in order to acquire tenure.  

For more information, please refer to Articles 5, 27 and 28 of Decree No. 72-581 of 4 July 1972 on the special status of *professeurs certifiés*.  

***Certificat d'aptitude au professorat de l'enseignement du second degré* (CAPES, secondary school teaching certificate)**

Any one of three competitive examinations leads to the CAPES:

* An open competitive examination for candidates with a master’s degree (four or five years of higher education) or a qualification or diploma recognised as equivalent by the Minister of Education
* A closed competitive examination for public-service employees (*fonctionnaires d’État*) with three years of professional experience in a branch of public service and with a degree (three years of higher education) or a qualification or diploma recognised as equivalent by the Minister of Education
* A third competitive examination for candidates with at least five years’ teaching experience in the private sector. 

These competitive examinations, which can be prepared for in a teacher training college (*école supérieure du professorat et de l'éducation*, ESPE), comprise at least one written eligibility test (*épreuve d’admissibilité*) and one oral admission test (*épreuve admission*) are held for different subject areas.  The list of these subject areas is laid down in [Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000032790151&cidTexte=LEGITEXT000027363503&dateTexte=20180503) of the Decree of 19 April 2013 cited below.

The nature of the eligibility and admission tests for these competitive examinations will vary according to the subject for which the candidate is applying.  The test syllabuses are laid down in Appendices [I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792126&cidTexte=LEGITEXT000027363503&dateTexte=20180503), [II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792128&cidTexte=LEGITEXT000027363503&dateTexte=20180503) and [IV](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000029052849&cidTexte=LEGITEXT000027363503&dateTexte=20180503) of the Decree of 19 April 2013.  

Having passed, the candidate must do a one-year placement with a local education authority specified by the Minister of Education in order to acquire the skills needed to practise as a teacher.  At the end of this placement the teacher is granted tenure by the chief education officer of the education authority for the area where the candidate has done the placement.  This granting of tenure gives the teacher the CAPES.  

For more information, please refer to Decree No. 72-581 of 4 July 1972 on the special status of *professeurs certifiés* and the Order of 19 April 2013 laying down the organisational arrangements for competitive examinations for the secondary school teaching certificate.  

**Specific teaching certificates**

Teachers wishing to teach specific subjects can take a competitive examination to obtain a:

* CAPEPS to teach physical education. The list of the various tests for the CAPEPS competitive examination is laid down in Appendices [I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000027363612&cidTexte=LEGITEXT000027363489&dateTexte=20180503), [II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034719692&cidTexte=LEGITEXT000027363489&dateTexte=20180503) and III of the [Order](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?cidTexte=JORFTEXT000027361487&dateTexte=20180503) of 19 April 2013 laying down the organisational arrangements for competitive examinations for the physical education teaching certificate
* CAPET to teach a technical subject. The candidate can enter for one of the subject areas laid down in [Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000032621389&cidTexte=LEGITEXT000027363387&dateTexte=20180503) of the Order of 19 April 2013 laying down the organisational arrangements for competitive examinations for the technical education teaching certificate.  The arrangements and syllabuses for the different CAPET tests are laid down in Appendices [I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792361&cidTexte=LEGITEXT000027363387&dateTexte=20180503), [II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000032621394&cidTexte=LEGITEXT000027363387&dateTexte=20180503) and [IV](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792363&cidTexte=LEGITEXT000027363387&dateTexte=20180503) of the above-mentioned Order of 19 April 2013
* CAPESA or CAPETA to teach at an agricultural college (*lycée agricole*). The list of subject areas for the competitive examination is laid down in [Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000027998111&cidTexte=LEGITEXT000022193686&dateTexte=20180503) of the Order of 14 April 2010 laying down the subject areas and organisational arrangements for competitive examinations for the secondary-level agricultural education teaching certificate and the secondary-level agricultural technical education teaching certificate. The arrangements and syllabuses for the CAPESA and CAPETA tests are laid down in the appendices to the aforesaid Order
* CAPLP to teach in a technical college. The various subject areas and options are laid down in [Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000029052855&cidTexte=LEGITEXT000027364754&dateTexte=20180503) of the Order of 19 April 2013 laying down the subject areas and organisational arrangements for competitive examinations for the technical college teaching certificate. The test syllabuses for these examinations are set out in Appendices [I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792376&cidTexte=LEGITEXT000027364754&dateTexte=20180503), [II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000036029690&cidTexte=LEGITEXT000027364754&dateTexte=20180503), III and IV of this Order.  

A teacher who has passed one of these competitive examinations must do a placement under the same conditions as those required to obtain the CAPES (see above: “*Certificat d'aptitude au professorat de l'enseignement du second degré* (CAPES, secondary school teaching certificate)”). 

**To become a *professeur agrégé***

***Agrégation* competitive examination**

A teacher wishing to become a *professeur agrégé* can take the *agrégation* competitive examination. To enter this examination the candidate must hold:

* For the open *agrégation*: a master’s degree (four or five years of higher education) or a qualification or diploma recognised as equivalent by the Minister of Education
* For the special open *agrégation*: a doctorate
* For the closed *agrégation*: a master’s degree or a qualification or diploma recognised as equivalent by the Minister of Education, and proof of having worked in public service (*la fonction publique*) for at least five years. 

The *agrégation* is awarded to a candidate having successfully completed:

* The tests of the closed, open or special open competitive examination; 
* A one-year placement in an institute of higher education. 

The various subjects that candidates can take are laid down in [Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000034264480&cidTexte=LEGITEXT000022746527&dateTexte=20180502) of the Order of 28 December 2009 establishing the subjects and organisational arrangements for the *agrégation* competitive examination. 

The test syllabuses for the various *agrégation* examinations are laid down in Appendices [I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000034792739&cidTexte=LEGITEXT000022746527&dateTexte=20180502), [I bis](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000032946014&cidTexte=LEGITEXT000022746527&dateTexte=20180502) and [II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000034792742&cidTexte=LEGITEXT000022746527&dateTexte=20180502) of the above-mentioned Order of 28 December 2009. 

For more information, please refer to Decree No. 72-580 of 4 July 1972 on the special status of *professeurs agrégés* in secondary education. 

#### Costs of qualifying as a teacher in secondary education

The cost of training varies according to the course chosen. For more details, candidates are advised to contact the establishments in question. 

### b. EU citizens: temporary and occasional practice (freedom to provide services) or permanent practice (freedom of establishment)

Any citizen of a Member State of the European Union (EU) or State party to the Agreement on the European Economic Area (EEA) can work as a teacher in secondary education in France, on either a temporary and occasional basis (freedom to provide services) or a permanent basis (freedom of establishment) under the same conditions as a French national.

For more information, please refer to Decree No. 2010-311 of 22 March 2010 on reception and recruitment arrangements for citizens of Member States of the European Union or a State party to the Agreement on the European Economic Area for an occupational branch, rank or position in the French public service (*fonction publique*). 

## 3°. Rules of professional conduct

A professional working as a teacher in secondary education will be under the authority of the chief education officer of the education authority for which he or she works. As a public-service employee, he or she must perform his or her duties in a fair, dignified, honest and ethical manner. 

In the event of failure to comply with these obligations, a teacher risks the following disciplinary measures:

* Verbal warning
* Written censure entered in teacher’s file
* Removal from promotion list
* Relegation in step
* Suspension for up to two weeks
* Demotion
* Suspension for three months to two years
* Compulsory retirement
* Revocation.

For more information, please refer to Article 37 of Decree No. 72-581 of 4 July 1972 on the special status of *professeurs certifiés*, Article 14 of Decree No. 72-580 of 4 July 1972 on the special status of *professeurs agrégés* in secondary education, and section 25 of Law 83-634 of 13 July 1983 establishing the rights and duties of public-service employees (*fonctionnaires*). 