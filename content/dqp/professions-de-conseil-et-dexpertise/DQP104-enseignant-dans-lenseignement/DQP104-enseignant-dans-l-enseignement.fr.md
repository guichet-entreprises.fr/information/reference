﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP104" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Enseignant dans l'enseignement secondaire" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="enseignant-dans-l-enseignement" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/enseignant-dans-l-enseignement-secondaire.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="enseignant-dans-l-enseignement-secondaire" -->
<!-- var(translation)="None" -->

# Enseignant dans l'enseignement secondaire

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L'enseignant dans l'enseignement secondaire est un professionnel chargé de transmettre l'ensemble de ses connaissances dans une ou plusieurs discipline(s) donnée(s), d'assurer le suivi individuel et l'évaluation des élèves de collège et lycée.

Peuvent exercer cette activité, les professeurs certifiés et les professeurs agrégés.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'accès au corps de fonctionnaires de l’Éducation nationale, se fait par voie de concours dont la nature dépend de la discipline enseignée.

Ainsi, en vue d'accéder au corps des professeurs certifiés, le professionnel doit être titulaire soit :

- d'un certificat d'aptitude au professorat de l'enseignement du second degré (CAPES) pour les disciplines générales ;
- d'un certificat d'aptitude au professorat d'éducation physique et sportive (CAPEPS) pour l'enseignement de l'éducation physique et sportive (EPS) ;
- d'un certificat d'aptitude au professorat de l'enseignement technique (CAPET) pour les disciplines techniques ;
- d'un certificat d'aptitude au professorat de l'enseignement du second degré agricole (CAPESA).

**À noter**

Le professionnel souhaitant enseigner dans un lycée professionnel de l'enseignement public doit obtenir le Concours d'accès au corps des professeurs de lycée professionnel (CAPLP).

Pour accéder au corps des professeurs agrégés, le professionnel doit satisfaire aux épreuves du concours d'agrégation (cf. infra « Concours de l'agrégation »).

#### Formation

##### En vue de l'accès au corps des professeurs certifiés

L'accès au corps des professeurs certifiés peut se faire soit :

- par la voie d'un concours en vue d'obtenir un certificat d'aptitude au professorat ;
- par voie d'inscription sur une liste d'aptitude. Le cas échéant, les candidats sont recrutés dans la limite d'une nomination pour neuf titularisations prononcées l'année précédente parmi les professionnels :
  - titulaires d'une licence (bac +3) ou d'un titre ou diplôme leur permettant d'exercer dans la discipline souhaitée,
  - âgés d'au moins quarante ans, 
  - exerçant cette activité depuis au moins dix ans, dont cinq en qualité de titulaires.

À l'issue du recrutement par voie d'inscription sur liste d'aptitude, le professionnel sera tenu d'effectuer un stage d'un an en vue d'être titularisé.

*Pour aller plus loin* : articles 5, 27 et 28 du décret n° 72-581 du 4 juillet 1972 relatif au statut particulier des professeurs certifiés.

###### Certificat d'aptitude au professorat de l'enseignement du second degré (CAPES)

Trois concours permettent d'accéder au CAPES :

- le concours externe accessible aux candidats titulaires d'un master (bac +4 ou bac +5) ou d'un titre ou diplôme reconnu comme équivalent par le ministre chargé de l'éducation ;
- le concours interne, accessible aux fonctionnaires d’État justifiant d'une expérience professionnelle de trois ans au sein d'un service public, et titulaires d'une licence (bac +3) ou d'un titre ou diplôme reconnu comme équivalent par le ministre chargé de l'éducation ;
- le troisième concours, accessible aux candidats justifiant d'au moins cinq ans d'exercice au sein du secteur privé.

Ces concours dont la préparation peut s'effectuer au sein d'une école supérieure du professorat et de l'éducation (ESPE), comportent des épreuves d'admissibilité et d'admission et sont organisés dans différentes sections. La liste de ces sections est fixée à l'article 1 de l'arrêté du 19 avril 2013 cité ci-après.

La nature des épreuves d'admissibilité et d'admission de ces concours varie selon la section au sein de laquelle le candidat est inscrit. Le programme des épreuves est fixé aux annexes I, II et IV de l'arrêté du 19 avril 2013.

Une fois admis, le candidat doit effectuer un stage d'un an, au sein d'une académie, définie par le ministre chargé de l'éducation en vue d'acquérir les compétences nécessaires à l'exercice de la fonction de professeur. À l'issue de ce stage, le professeur est titularisé par le recteur de l'académie dans le ressort de laquelle il a effectué son stage. Cette titularisation confère au professionnel le CAPES.

*Pour aller plus loin* : décret n° 72-581 du 4 juillet 1972 relatif au statut particulier des professeurs certifiés ; arrêté du 19 avril 2013 fixant les modalités d'organisation des concours du certificat d'aptitude au professorat du second degré.

###### Certificats d'aptitude spécifiques

Le professeur souhaitant exercer dans une discipline spécifique peut se présenter aux différents concours en vue d'obtenir un :

- CAPEPS pour enseigner l'éducation physique et sportive. La liste des différentes épreuves des concours du CAPEPS est fixée aux annexes I, II et III de l'arrêté du 19 avril 2013 fixant les modalités d'organisation des concours du certificat d'aptitude au professorat d'éducation physique et sportive ;
- CAPET pour l'enseignement d'une discipline technique. Le cas échéant, le candidat peut s'inscrire au sein de l'une des sections fixées à l'article 1 de l'arrêté du 19 avril 2013 fixant les modalités d'organisation des concours du certificat d'aptitude au professorat de l'enseignement technique. Les modalités et programmes des différentes épreuves du CAPET sont fixés aux annexes I, II et IV de l'arrêté du 19 avril 2013 susvisé ;
- CAPESA ou le CAPETA, pour enseigner au sein d'un lycée agricole. La liste des sections du concours est fixée à l'article 1 de l'arrêté du 14 avril 2010 fixant les sections et les modalités d'organisation des concours du certificat d'aptitude au professorat de l'enseignement du second degré agricole et du certificat d'aptitude au professorat de l'enseignement technique agricole. Les modalités et le programme des épreuves du CAPESA et CAPETA sont fixés aux annexes de l'arrêt susvisé ;
- CAPLP pour enseigner au sein d'un lycée professionnel. Les différentes sections et options possibles sont fixées à l'article 1 de l'arrêté du 19 avril 2013 fixant les sections et modalités d'organisation des concours du certificat d'aptitude au professorat de lycée professionnel. Le programme des épreuves de ces concours est fixé aux annexes I, II, III et IV dudit arrêté.

Le professionnel, une fois admis à l'un de ces concours, doit effectuer un stage dans les mêmes conditions que celles requises pour l'obtention du CAPES (cf. supra « Certificat d'aptitude au professorat de l'enseignement du second degré (CAPES) »).

##### En vue de l'accès au corps des professeurs agrégés

###### Concours de l'agrégation

Le professionnel souhaitant devenir professeur agrégé peut, accéder au concours de l'agrégation. Pour accéder à ce concours, le candidat doit être titulaire :

- en vue de se présenter au concours externe, d'un master (bac +4 ou bac +5), d'un titre, ou d'un diplôme reconnu comme équivalent par le ministre chargé de l'éducation ;
- d'un doctorat, en vue de se présenter au concours externe spécial ;
- en vue de se présenter au concours interne, d'un master, ou d'un titre ou diplôme reconnu comme équivalent et justifier avoir exercé un emploi de la fonction publique pendant au moins cinq ans.

L'agrégation est délivrée au candidat ayant effectués avec succès :

- les épreuves du concours interne, externe ou du concours externe spécifique ;
- un stage d'un an effectué au sein d'un établissement d'enseignement supérieur.

Les différentes sections au sein desquelles le candidat peut s'inscrire sont fixées à l'article 1 de l'arrêté du 28 décembre 2009 fixant les sections et les modalités d'organisation des concours de l'agrégation.

Le programme des épreuves des différents concours de l'agrégation est fixé aux annexes I, I bis et II de l'arrêté du 28 décembre 2009 susvisé.

*Pour aller plus loin* : décret n° 72-580 du 4 juillet 1972 relatif au statut particulier des professeurs agrégés de l'enseignement du second degré.

##### Coûts associés à la qualification

Le coût de la formation varie selon le cursus envisagé. Il est conseillé de se rapprocher des établissements concernés, pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services) ou permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) peut exercer en France l'activité d'enseignant dans l'enseignement secondaire, à titre temporaire et occasionnel (LPS) ou permanent (LE) dans les mêmes conditions que le ressortissant français.

*Pour aller plus loin* : décret n° 2010-311 du 22 mars 2010 relatif aux modalités de recrutements et d'accueil des ressortissants des États membres de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique dans corps, un cadre d'emplois ou un emploi de la fonction publique française.

## 3°. Règles professionnelles

Le professionnel exerçant l'activité d'enseignant de l'enseignement secondaire est placé sous l'autorité du recteur de l'académie au sein de laquelle il exerce. En qualité de fonctionnaire, il doit exercer son activité avec dignité, impartialité, intégrité et probité.

En cas de manquements à ces obligations, le professionnel encourt les sanctions disciplinaires suivantes :

- un avertissement ;
- un blâme ;
- la radiation du tableau d'avancement ;
- un abaissement d'échelon ;
- une exclusion temporaire de fonction pour une durée maximale de quinze jours ;
- une rétrogradation ;
- une exclusion temporaire de fonctions pour une durée de trois mois à deux ans ;
- une mise à la retraite d'office ;
- une révocation.

*Pour aller plus loin* : article 37 du décret n° 72-581 du 4 juillet 1972 relatif au statut particulier des professeurs certifiés et article 14 du décret n° 72-580 du 4 juillet 1972 relatif au statut particulier des professeurs agrégés de l'enseignement du second degré ; article 25 de la loi n° 83-634 du 13 juillet 1983 portant droit et obligations des fonctionnaires.