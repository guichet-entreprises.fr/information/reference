﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP238" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artisanat" -->
<!-- var(title)="Ramoneur" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artisanat" -->
<!-- var(title-short)="ramoneur" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/artisanat/ramoneur.html" -->
<!-- var(last-update)="2020-04-30 11:23:50" -->
<!-- var(url-name)="ramoneur" -->
<!-- var(translation)="None" -->

# Ramoneur

Dernière mise à jour : <!-- begin-var(last-update) -->novembre 2023<!-- end-var -->

## Définition de l'activité

Le ramoneur est un professionnel chargé d'assurer l'entretien des conduits de cheminées, des poêles à bois et des installations au fuel, mazout et gaz, dans le respect de la réglementation en vigueur. Il réalise également le nettoyage des âtres, fourneaux, incinérateurs, chaudières, gaines de ventilation et dispositifs d'évacuation des fumées.

Il établit des diagnostics pour ses clients et contrôle le bon fonctionnement des installations pour éviter tout risque d'incendie ou d'intoxication au monoxyde de carbone.

## Qualifications professionnelles

### Exigences nationales

L'intéressé souhaitant exercer l'activité de ramoneur doit disposer d'une qualification professionnelle ou exercer sous le contrôle effectif et permanent d'une personne ayant cette qualification.

Pour être considérée comme qualifiée professionnellement, la personne doit être titulaire de l'un des diplômes ou titres suivants :

- certificat d'aptitude professionnelle (CAP) ;
- brevet d'études professionnelles (BEP) ;
- diplôme ou titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au répertoire national des certifications professionnelles (RNCP).

À défaut de l'un de ces diplômes ou titres, l'intéressé doit justifier d'une expérience professionnelle de trois années effectives sur le territoire de l'Union européenne (UE) ou de l'Espace économique européen (EEE) acquise en qualité de dirigeant d'entreprise, de travailleur indépendant ou de salarié dans l'exercice du métier de ramoneur. Dans ce cas, il est conseillé à l'intéressé de s'adresser à la chambre de métiers et de l'artisanat (CMA) pour demander une attestation de qualification professionnelle.

**Pour aller plus loin :** articles L. 121-1 à L. 121-3 et R. 121-1 à R. 121-5 du code de l'artisanat.

#### Formation

Le CAP est un diplôme de niveau 3 accessible en deux ans après une classe de troisième. La sélection s'effectue le plus souvent sur dossier et sur tests. Ce diplôme est accessible après un parcours de formation sous statut d'élève ou d'étudiant, en contrat d'apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou par [validation des acquis de l'expérience](http://www.vae.gouv.fr/) (VAE).

Le BEP (diplôme intermédiaire de niveau 3 présenté pendant le cursus menant au baccalauréat professionnel obtenu en 3 ans après la classe de troisième, disparu en 2020), a été remplacé par le brevet professionnel (BP). Ce diplôme de niveau 4 est accessible soit par la voie de l’apprentissage, soit dans le cadre de la formation professionnelle continue (contrat de professionnalisation), soit par la voie de l’enseignement à distance, soit par la validation des acquis de l'expérience (VAE).

Le CTM (certificat technique des métiers – niveau 3) est un titre spécifique à l'artisanat qui se prépare à partir de la troisième. Ce diplôme est accessible après un parcours de formation sous statut d'élève ou d'étudiant, en contrat d'apprentissage, après un parcours de formation continue, en contrat de professionnalisation ou par validation des acquis de l'expérience. Le plus souvent, la formation menant à son obtention est d'une durée de deux ans.

Le BTM (brevet technique des métiers – niveau 4) est un titre spécifique à l'artisanat, ouvert au titulaire d'un diplôme de niveau 3 ou 4. Le diplôme est accessible après un parcours de formation continue, en contrat d'apprentissage, en contrat de professionnalisation, par candidature individuelle ou par validation des acquis de l'expérience. La formation dure en principe deux ans.

L’ensemble des diplômes ou titres de niveau égal ou supérieur au CAP homologués ou enregistrés lors de sa délivrance au RNCP sont disponibles sur le site [France compétences](https://www.francecompetences.fr/).

**Pour aller plus loin :** articles D. 337-1 et suivants du Code de l'éducation ; [portail de l'Artisanat](https://www.artisanat.fr/formation/devenir-apprenti).

#### Coûts associés à la qualification

La formation est le plus souvent gratuite. Pour plus de précisions, il est conseillé de se reporter au centre de formation considéré.

### Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Tout ressortissant d'un État membre de l'UE ou partie à l'accord sur l'EEE établi et exerçant légalement l'activité de ramoneur dans cet État peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Toutefois, lorsque cette activité ou la formation y conduisant ne sont pas réglementées dans l'État d'établissement, il doit l'avoir exercée dans un ou plusieurs États membres de l'UE parties à l'accord sur l'EEE pendant au moins une année à temps plein ou pendant une durée équivalente à temps partiel au cours des dix années qui précèdent la prestation qu'il entend réaliser en France.

Il doit au préalable en faire la déclaration à la CMA du lieu dans lequel il souhaite réaliser la prestation.

À réception de la déclaration complète par la CMA, le professionnel peut exercer en France, à titre temporaire et occasionnel, l’activité de ramoneur ou en assurer le contrôle effectif et permanent.

**Pour aller plus loin :** articles L. 123-2 et L. 123-3, et R. 123-14 à R. 123-18 du code de l’artisanat.

### Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Liberté d'Établissement)

Pour exercer l'activité de ramoneur en France à titre permanent, le ressortissant d'un État membre de l'UE ou partie à l'accord sur l'EEE doit remplir **l'une des conditions suivantes :**

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « Qualifications professionnelles » : diplôme/titre ou expérience professionnelle de trois ans) ;
- être titulaire d'une attestation de compétence ou d'un titre de formation requis pour l'exercice de l'activité de ramoneur dans un État, membre ou partie, lorsque cet État réglemente l'accès ou l'exercice de cette activité sur son territoire ;
- être titulaire d’une attestation de compétences ou d’un titre de formation obtenu dans un État, membre ou partie, **ET** justifier de l’exercice du métier de ramoneur à temps plein pendant une année ou à temps partiel pendant une durée équivalente au cours des dix dernières années, lorsque l’État membre ou partie ne réglemente pas l’exercice de ce métier.

Dès lors qu'il remplit l'une des conditions précitées, le ressortissant d'un État membre ou partie peut obtenir la reconnaissance de ses qualifications professionnelles (cf. infra « Effectuer une demande de reconnaissance de qualification professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) ».)

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à une mesure de compensation (cf. infra « Bon à savoir : mesures de compensation »).

**Pour aller plus loin :** articles L. 123-1, R. 123-1 à R. 123-13 et R. 123-18 du code de l’artisanat.

## Conditions d'honorabilité

Nul ne peut exercer la profession s'il fait l'objet :

- d'une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale en application de l’article L. 653-8 du code de commerce ;
- d'une peine alternative d'interdiction d'exercer une activité professionnelle ou sociale pour un délit en application du 11° de l'article 131-6 du code pénal.

**Pour aller plus loin :** article L. 123-44 du code de commerce.

## Assurance

Le ramoneur exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle. En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

## Démarches et formalités de reconnaissance de qualification

### Effectuer une déclaration préalable d'activité pour le ressortissant d'un État membre de l'UE ou d'un État partie à l'accord sur l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le professionnel adresse une déclaration écrite à la CMA dans le ressort de laquelle il envisage de réaliser une prestation de services en tant que ramoneur.

#### Procédure

À réception d’un récépissé mentionnant que le dossier est complet, le professionnel peut exercer en France, à titre temporaire et occasionnel, l’activité de ramoneur ou en assurer le contrôle effectif et permanent.

La prestation est alors réalisée sous le titre professionnel indiqué dans la langue officielle ou l'une des langues officielles de l'État dans lequel le prestataire est établi. Lorsque ce titre professionnel n'existe pas dans l'État d'établissement, le prestataire mentionne, dans la langue officielle ou l'une des langues officielles de cet État, son titre de formation et l'État membre dans lequel il a été octroyé.

En cas de doutes justifiés, la CMA peut demander à l'autorité compétente de l'État d'établissement toute information pertinente concernant la légalité de l'établissement du prestataire ainsi que l'absence de sanction disciplinaire ou pénale à caractère professionnel.

Lorsque le professionnel n’est pas en mesure de produire les pièces exigées à l’appui de sa déclaration, il peut demander à la CMA de réaliser une épreuve d’aptitude afin d’établir sa qualification professionnelle (cf. infra « Bon à savoir : mesures de compensation »).

#### Pièces justificatives

La déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une copie d'une pièce d'identité en cours de validité ;
- une attestation certifiant que le déclarant est légalement établi dans un État membre de l'UE ou partie à l'accord sur l'EEE ;
- un document justifiant de la qualification professionnelle du déclarant qui peut être, au choix :
  - un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir** : les pièces doivent être traduites en français par un traducteur agréé.

**À noter** : lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le déclarant et lui demander l'ensemble des pièces manquantes.

#### Coût

La déclaration préalable est gratuite. Cependant, si le déclarant participe à une épreuve d’aptitude, des frais d’organisation pourront lui être demandés. Pour plus d'informations, il est conseillé de se rapprocher de la CMA compétente.

**Pour aller plus loin :** articles R. 123-14 à R. 123-18 du code de l’artisanat ; arrêté du 28 octobre 2009 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### Effectuer une reconnaissance de qualification professionnelle pour le ressortissant de l'UE ou de l'EEE en cas d'exercice permanent (LE)

L'intéressé souhaitant s'établir en France et ne disposant pas d'un diplôme français ou de l'expérience professionnelle requise doit demander la reconnaissance de ses qualifications professionnelles.

#### Autorité compétente

La demande doit être adressée à la CMA dans le ressort de laquelle l'intéressé souhaite s'établir.

#### Procédure

Un récépissé de remise de demande complète est adressé au demandeur dans un délai d'un mois suivant sa réception par la CMA. Lorsque le dossier est incomplet, la CMA dispose d’un délai de quinze jours pour en informer l'intéressé et lui demander l’ensemble des pièces manquantes. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

La demande de reconnaissance de qualification professionnelle est un dossier comportant les pièces justificatives suivantes :

- une copie de la pièce d'identité du demandeur en cours de validité ;
- un justificatif de sa qualification professionnelle figurant, au choix, dans la liste suivante :
  - un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tous documents attestant de l’expérience professionnelle du ressortissant.


**À savoir** : toutes les pièces justificatives doivent être traduites en français par un traducteur agréé.

La CMA peut demander la communication d'informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l'existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. Si la CMA doit se rapprocher de France Éducation International (anciennement Centre international d'études pédagogiques) pour obtenir des informations complémentaires sur le niveau de formation d'un diplôme ou d'un certificat ou d'un titre étranger, le demandeur devra s'acquitter de frais supplémentaires.

#### Délai

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut décider de :

- reconnaître la qualification professionnelle du demandeur et lui délivrer l'attestation de qualification professionnelle ;
- soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l'attestation de qualification professionnelle.

**À savoir** : en l'absence de décision de la CMA dans le délai de trois mois à compter de la réception de la demande complète, la reconnaissance de qualification professionnelle est réputée acquise au demandeur.

#### Voies de recours

En cas de refus de reconnaissance de qualification professionnelle par la CMA, le demandeur peut contester la décision. Il peut ainsi, dans les deux mois suivant la notification du refus de la CMA, former :

- un recours administratif auprès du préfet du département dans le ressort duquel la CMA compétente a son siège ;
- un recours contentieux devant le tribunal administratif compétent.

#### Coût

Gratuit.

#### Bon à savoir : mesures de compensation

La CMA notifie au demandeur, dans un délai de trois mois à compter de la réception de la demande complète, sa décision tendant à lui faire accomplir une mesure de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit informer le CMA de son choix de :

- suivre un stage d'adaptation (dont la durée ne peut être supérieure à trois ans) ;
- ou passer une épreuve d'aptitude.

L'épreuve d'aptitude prend la forme d'un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d'opter pour cette épreuve. À défaut, la reconnaissance de qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

En cas de réussite à l’épreuve d’aptitude, la CMA délivre une attestation de qualification professionnelle dans un délai d’un mois.

En cas d’option pour le stage d’adaptation, la CMA adresse au demandeur, dans le délai d’un mois, la liste des organismes susceptibles d’organiser le stage. Passé ce délai, la reconnaissance de qualification est réputée acquise et la CMA délivre une attestation de qualification professionnelle. À l'issue du stage d'adaptation, le demandeur adresse à la CMA une attestation certifiant qu'il a valablement accompli ce stage, accompagnée d'une évaluation de l'organisme qui l'a organisé. La CMA délivre à l'intéressé, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d'un mois.

#### Vois de recours

La décision de recourir à une mesure de compensation peut être contestée par l'intéressé qui doit former un recours administratif auprès du préfet du département dans le ressort duquel la CMA a son siège dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

#### Coût

Des frais fixes couvrant l'instruction du dossier peuvent être demandés. Pour plus d'informations, il est conseillé de se rapprocher de la CMA compétente.

**Pour aller plus loin :** articles R. 123-1 à R. 123-13 et R. 123-18 du code de l’artisanat ; arrêté du 28 octobre 2009 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### Voies de recours

#### Centre d'assistance français

Le Centre ENIC-NARIC est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l'Administration nationale de chaque État membre de l'UE ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L'intéressé ne peut recourir à SOLVIT que s'il établit :

- que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
- qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

##### Délai

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l'issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
- s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).