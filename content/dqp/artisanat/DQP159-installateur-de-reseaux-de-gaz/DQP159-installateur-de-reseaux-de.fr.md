﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP159" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artisanat" -->
<!-- var(title)="Installateur de réseaux de gaz" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artisanat" -->
<!-- var(title-short)="installateur-de-reseaux-de" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/artisanat/installateur-de-reseaux-de-gaz.html" -->
<!-- var(last-update)="2020-04-30" -->
<!-- var(url-name)="installateur-de-reseaux-de-gaz" -->
<!-- var(translation)="None" -->

# Installateur de réseaux de gaz

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30<!-- end-var -->

## 1°. Définition de l’activité

L'installateur de réseaux de gaz est un professionnel chargé de réaliser l'installation et l'entretien des équipements acheminant du gaz au sein des logements collectifs et individuels.

Une fois l'installation effectuée, le professionnel est tenu de fournir un certificat de conformité aux règles d'installation et de sécurité à son client.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité d'installateur de réseaux de gaz le professionnel doit être qualifié professionnellement ou exercer sous le contrôle effectif et permanent d'une personne qualifiée.

Pour être considéré comme qualifié professionnellement, l'intéressé doit être titulaire d'un diplôme de niveau IV (bac) « Technicien réseau gaz » enregistré au Répertoire national des certifications professionnelles ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)).

À défaut, il doit justifier d'une expérience professionnelle de trois années effectives sur le territoire de l'Union européenne (UE) ou de l'Espace économique européen (EEE), acquise en qualité de dirigeant d'entreprise, de travailleur indépendant ou de salarié dans l'exercice de cette profession. Dans ce cas, il est conseillé de s'adresser à la chambre des métiers et de l'artisanat (CMA) pour demander une attestation de qualification professionnelle.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat ; article 1er du décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l'exercice des activités prévues à l'article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

#### Formation

La formation menant au diplôme de « Technicien réseaux gaz » est accessible aux candidats en contrat d'apprentissage, après un parcours de formation continue, en contrat de professionnalisation ou par validation des acquis de l'expérience (VAE). Pour plus d’informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr/).

Ce diplôme est délivré par le Groupement d'intérêt public formation tout au long de la vie (GIP -FTLV) de Nancy.

*Pour aller plus loin* : arrêté du 26 novembre 2015 portant enregistrement au Répertoire national des certifications professionnelles.

#### Coûts associés à la qualification 

La formation est, le plus souvent gratuite. Pour plus d'informations, il est conseillé de se rapprocher du centre de formation concerné.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord de l'Espace économique européen (EEE), qui est établi et exerce légalement l'activité d'installateur de réseaux de gaz dans cet État, peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Lorsque l’État ne réglemente ni l'accès à l'activité, ni son exercice, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États membres de l'UE.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à une épreuve d'aptitude.

Dès lors qu'il remplit ces conditions, le professionnel doit effectuer une déclaration préalable auprès de la CMA du lieu où il souhaite exercer (cf. infra « 5°. a. Effectuer une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

*Pour aller plus loin* : article 17-1 de la loi du 5 juillet 1996 ; article 2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Pour exercer l’activité d'installateur de réseaux de gaz en France à titre permanent, le ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité d'installateur de réseaux de gaz dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétence ou d’un titre de formation qui certifie sa préparation à l’exercice de cette activité lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès, ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité d'installateur de réseaux de gaz dans l’État qui a admis l’équivalence.

Dès lors qu'il remplit l'une des conditions précitées, le ressortissant d’un État de l’UE ou de l’EEE pourra demander une attestation de reconnaissance de qualification professionnelle (cf. infra « 5°. b. Demander une attestation de qualification professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) ».)

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à des mesures de compensation (cf. infra « 5°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 à 3-2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Nul ne peut exercer l'activité d'installateur de réseaux de gaz s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

En outre, le professionnel encourt une amende de 7 500 euros dès lors qu'il :

- exerce l'activité d'installateur de réseaux de gaz sans être qualifié professionnellement ;
- exerce son activité sans être inscrit au répertoire des métiers ou au registre des entreprises (pour les départements du Haut-Rhin, du Bas-Rhin et de la Moselle) ;
- fait usage du terme « artisan » pour la promotion de son activité sans être titulaire de la qualité d'artisan, de maître ou de maître-artisan.

*Pour aller plus loin* : articles 19 et 24 de la loi du 5 juillet 1996 précitée ; article L. 653-8 du Code de commerce.
 
## 4°. Assurances

Le professionnel exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.
 
## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le professionnel doit adresser sa déclaration à la CMA du lieu dans lequel il souhaite réaliser sa prestation.

#### Pièces justificatives

Sa demande doit comprendre les documents suivants assortis, le cas échéant, de leur traduction agréée :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant sa qualification professionnelle qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tout document attestant de son expérience professionnelle.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

#### Issue de la procédure

Dès réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de services en France.

En l'absence de réponse dans ces délais, le ressortissant peut débuter sa prestation de services.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### b. Demander une attestation de reconnaissance de qualification professionnelle pour le ressortissant de l'UE ou de l'EEE en cas d'exercice permanent (LE)

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA compétente du lieu dans lequel l'intéressé souhaite s'établir.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que ce dernier est complet.

#### Pièces justificatives

La demande d'attestation de reconnaissance de qualification professionnelle est un dossier comportant les pièces justificatives suivantes :

- une demande d’attestation de qualification professionnelle ;
- un justificatif de la qualification professionnelle sous la forme d'une attestation de compétences ou d'un diplôme ou d'un titre de formation professionnelle ;
- une photocopie de la pièce d'identité du demandeur en cours de validité ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français par un traducteur agréé.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (Ciep) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

#### Délai

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut décider de :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- soumettre le ressortissant à une mesure de compensation et lui notifie cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

**À savoir** 

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

#### Voies de recours

En cas de refus de la demande de reconnaissance de qualification professionnelle par la CMA, le demandeur peut contester la décision. Il peut ainsi, dans les deux mois suivant la notification du refus de la CMA, former :

- un recours gracieux auprès du préfet du département de la CMA compétente ;
- un recours contentieux devant le tribunal administratif compétent.

#### Coût

Gratuit.

#### Bon à savoir : mesures de compensation

La CMA notifie au demandeur sa décision tendant à lui faire accomplir l'une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans et une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À la fin du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

#### Coût

Des frais fixes couvrant l'instruction du dossier peuvent être demandés. Pour plus d'informations, il est conseillé de se rapprocher de la CMA compétente.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).