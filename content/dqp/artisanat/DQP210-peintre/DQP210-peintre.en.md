﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP210" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="House painter" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="house-painter" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/house-painter.html" -->
<!-- var(last-update)="2020-04-15 17:20:34" -->
<!-- var(url-name)="house-painter" -->
<!-- var(translation)="Auto" -->


House painter
=======

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:34<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1. Definition of activity
-------------------------

The painter is a professional who works on construction sites or renovations. It should proceed with the preparation of surfaces for painting and choose the materials and tools used to apply the final paint. This is a decorative work, but it also has technical expertise in order to choose the right materials against degradation factors (air humidity).

2. Professional .Qualifications
-------------------------------

### at. national requirements

#### national legislation

The person wishing to exercise the activity as a painter must have a professional qualification or practice under the effective control and permanent for a person with this qualification.

According to Decree No. 98-246 of 2 April 1998, a person is considered to be professionally qualified if it holds one of the following diplomas or degrees:

- a vocational certificate (CAP);
- a vocational studies certificate (BEP);
- diploma or title, or greater, licensed or registered in issuing the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)and issued for the exercise of the trade of painter.

In the case of activity as a painter, the person must hold, for example:

- CAP "coating applicator painter";
- professional patent "paint coatings";
- pro tray layout and Finishing dominant building "painting, glazing, coating" or dominant "plastering painting."

Failure of one of these diplomas or qualifications, the applicant must demonstrate professional experience of three years effective on the territory of the European Union (EU)*or the European Economic Area (EEA)*Acquired as a business executive, a self-employed or employee in the performance of the art of painting. In this case, it is advisable for the person to apply to the Chamber of Trades and Crafts (CMA) to request a certificate of professional qualification.

*For further*: Article 16 of Law No. 96-603 of 5 July 1996 on the development and promotion of trade and crafts, Decree No. 98-246 of 2 April 1998 on the professional qualifications required for the exercise planned activities in Article 16 of law No. 96-603 of 5 July 1996 cited above.

#### Training

The CAP is a level V diploma (that is to say, corresponding to an output of the second cycle and general technology before the end of year). The pro and professional patent pan are, in turn, the level IV degrees (that is to say general degree level, technological or vocational).

CAP "coating applicator painter" is accessible after a third class. Selection is usually on record and on tests. This diploma is available after a training course with the status of pupil or student, apprenticeship, after a training course, professional contract by individual candidates or validation of acquired experience ( VAE). For more information, you can consult[official site](http://www.vae.gouv.fr/)VAE. The training lasts two years and normally takes place in a vocational school.

The professional certificate "paint coatings" is available on apprenticeship contracts, after a training course, professional contract by individual candidates or accreditation of prior learning (APL). For more information, you can consult[official site](http://www.vae.gouv.fr/)VAE. The training leading to the award of this professional patent lasts in principle two years.

Tray Pro "development and building finishing" is available after a student status under training courses or studying, apprenticeship, after a training course, professional contract by individual candidates or accreditation of prior learning (APL). For more information, you can consult[official site](http://www.vae.gouv.fr/)VAE.

*For further*: Articles D. 337-1 et seq of the Education Code.

#### Costs associated with the qualification

Training is, in most cases, free. For more information, you are advised to refer to the particular training center.

### b. EU nationals: for a temporary and occasional exercise (Freedom of Services)

Professional EU national or EEA may exercise in France on a temporary and occasional basis, the effective and permanent control of the activity of painter on the condition to be legally established in one of these states to exercise it the same activity.

If neither the activity nor the training leading thereto is regulated in the State of establishment, the person concerned must also prove that he practiced painter activity in that State for at least the equivalent of two years full-time during the ten years preceding the provision that he wants to perform in France.

*For further*: Article 17-1 of Law No. 96-603 of 5 July 1996 cited above.

### vs. EU nationals: for a permanent exercise (Free Country)

To exercise in France, permanent, effective and permanent control of painter activity, professional EU national or EEA must meet one of the following conditions:

- have the same qualifications as those required for French nationals (see above: ". .a 2. Professional qualifications");
- hold a certificate of competence or qualification of training required for the exercise of painter activity in a State of the EU or EEA when regulating access or the exercise of this activity in its territory;
- have a certificate of competency or a formal qualification that certifies his preparation for the exercise of activity as a painter, where such a certificate or title has been obtained in a State of the EU or EEA which regulates neither the access nor the exercise of this activity;
- hold a diploma or certificate acquired in a third country and recognized as equivalent by a State of the EU or EEA to the additional condition that the person concerned has served for three years painter activity in the State admitted equivalence.

**To note**

A national of a State of the EU or the EEA who meets one of the above conditions can apply for a certificate of qualification to exercise effective and permanent control of the activity as a painter. For more information, you are advised to refer below under "5 °. b. If necessary, seek professional qualification recognition certificate ".

If the person does not meet any of the above conditions, seizure CMA can ask it to perform a compensation measure in the following cases:

- if the duration of the certified training is lower by at least a year to that required for one of the professional qualifications required in France to practice painter of the activity;
- if the training received covers substantially different matters from those covered by one of the required qualifications to practice in France painter of the activity;
- if the effective and permanent control of the painter activity requires, for the exercise of some of its functions, specific training is not provided in the home Member State and covers substantially different from those covered by the certificate of skills or the qualifications possessed by the applicant state.

*For further*: Articles 17 and 17-1 of Law No. 96-603 of 5 July 1996 cited above, Articles 3 3-2 of Decree No. 98-246 of 2 April 1998 cited above.

**Good to know: compensation measures**

The CMA, received a request for issuance of a certificate of professional qualification, notify the applicant of its decision to make him accomplish compensation measures. The decision lists the areas not yet covered by the qualifications attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose a course of adaptation for a maximum of three years and an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organized within six months from the receipt by the CMA plaintiff's decision to opt for this event. Otherwise, the qualification is deemed granted and CMA issues a certificate of qualification.

At the end of the adaptation period, the applicant address to the CMA a certificate stating that he validly completed the course, accompanied by an assessment of the organization that oversaw. The CMA shall, on the basis of this certificate, a certificate of qualification in a period of one month.

The decision to use a compensation measure may be challenged by the person who has an administrative appeal to the prefect within two months of notification of the decision. In case of rejection of its application, it can initiate a judicial appeal.

*For further*: Articles 3 and 3-2 of Decree No. 98-246 of 2 April 1998 cited above, Article 6-1 of Decree No 83-517 of 24 June 1983 laying down the conditions of application of the law of 23 December 82-1091 1982 on vocational training for craftsmen.

3 °. Terms of repute, professional ethics, ethics
-------------------------------------------------

No one can practice as a painter is the subject:

- prohibited from directing, managing, administering or directly or indirectly control a commercial or craft business;
- a penalty of disqualification from professional or social activity for one of the crimes or offenses provided for in Article 11 of the Penal Code 131-6.

*For further*: Article 19 III of Law No. 96-603 of 5 July 1996 cited above.

4 °. insurance
--------------

The professional is not subject to the obligation to take out professional liability insurance. However, it is advisable to take out this insurance because it allows to be covered for damage caused to others, the professional is the direct cause or whether the fact of its employees, premises or his material.

In addition, the law requires the artisans involved in the work of structural works and building purchase decennial liability assistance. This warranty is applicable when the work done are likely to undermine the strength of a book or make it unsuitable for its purpose.

However,[national jurisprudence](https://www.legifrance.gouv.fr/affichJuriJudi.do?oldAction=rechJuriJudi&idTexte=JURITEXT000007045775&fastPos=1)considers that the person practicing as a painter is not subject to the obligation of getting a ten-year insurance. According to the analysis of the Court of Cassation, the painting does not amount to a book, an item of equipment or a component of work within the meaning of Article 1792-3 of the Civil Code.

5 °. Approach and qualifications recognition procedures
-------------------------------------------------------

### at. If necessary, seek professional qualification recognition certificate

The person wishing to recognize his degree, other than that required in France, or professional experience may apply for a certificate of qualification.

#### Competent authority

The request must be sent to the CMA territorial jurisdiction.

#### Procedure

A request for postponement of receipt is sent to the applicant within one month of receipt by the CMA. If the file is incomplete, CMA asks the applicant to complete it within 15 days of the filing of the dossier. A receipt is issued as soon as the file is complete.

#### Vouchers

The professional qualification certificate application must contain:

- the demand for professional qualification certificate;
- the proof of professional qualifications: a certificate of competency or diploma or vocational training;
- proof of the nationality of the applicant;
- whether professional experience was acquired in the territory of a State of the EU or the EEA, a certificate concerning the nature and duration of the activity issued by the competent authority in the Member State original;
- whether professional experience was acquired in France, supporting the exercise of the activity for three years.

The CMA may ask for additional information regarding training or professional experience to determine the possible existence of substantial differences with the professional qualifications required in France. Also, if the MAC must approach the International Center for Pedagogical Studies (CIEP) for more information on the level of training of a diploma, a certificate or a foreign title, the applicant must s' pay an additional surcharge.

All supporting documents must be translated into French (certified translation), if applicable.

#### Response time

Within three months of the issue of the receipt, the CMA can either:

- recognize the professional qualifications and issue the certificate of professional qualification;
- decide to refer the applicant to a compensation and notify him of this decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the demand for professional qualification certificate is deemed granted.

#### Remedies

If the CMA refuses to issue the recognition of professional qualifications, the applicant may initiate an appeal with the competent administrative court within two months of notification of the refusal of the CMA. Similarly, if the person wants to challenge the decision of the CMA to undergo a compensation measure, it must first initiate an administrative appeal to the prefect of the department where the CMA has its headquarters, in the two months following the notification of the decision of the CMA. If he does not is successful, it will then opt for a settlement before the competent administrative court.

*For further*: Articles 3 3-2 of Decree No. 98-246 of 2 April 1998 cited above, decree of 28 October 2009 adopted under decree No. 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 on the recognition of professional qualifications procedure professional a national member State of the European Community or another State party to the agreement on the European economic Area.

### b. Perform a prior declaration of activity for European citizens exercising time activity (Freedom of Services)

Nationals of EU or EEA wishing to practice in France occasionally and punctual are subject to a requirement of notification to their first appearance on French soil.

This preliminary activity declaration must be renewed every year if the person intends to exercise again in France.

#### Competent authority

The prior declaration of activity is addressed to the CMA in which the declarant spring plans to realize its benefits.

#### Receipt

CMA delivers to declaring a receipt stating the date on which it received the complete record of prior declaration of activity. If the file is incomplete, CMA notify within 15 days the list of missing pieces to the declarant. It delivers the receipt as soon as the file is complete.

#### Vouchers

The statement of activities shall include:

- the statement signed and dated, stating information relating to mandatory professional civil insurance;
- proof of the nationality of the declarant;
- evidence of professional qualifications of declaring formal qualification, competence certificate issued by the competent authority of the State of establishment, documents evidencing professional experience indicating its nature and duration, etc. ;
- if any, evidence that the registrant has practiced for at least the equivalent of two years full-time painter activity over the last ten years;
- establishment of a certificate in a State of the EU or EEA to exercise painter of the activity;
- a certificate of non conviction to a ban, even temporarily, to exercise.

All documents must be translated into French (by a certified translator) if they are not established in French.

#### Time limit

Within one month of receipt of the complete file prior declaration of activity, the CMA delivers to declaring a professional qualification certificate or notify him of the need for additional examination. In the latter case, the CMA shall notify the final decision within two months from the receipt of the complete file of preliminary activity statement. Failing notification within this period, the statement is deemed granted and the provision of services can therefore begin.

In making its decision, the CMA may apply to the State authority of establishment of the declarant to obtain any information about the legality of the institution and its lack of disciplinary or criminal sanctions of a professional nature.

**Good to know**

If the CMAs substantial difference between the professional qualifications required to operate in France and as reported by the provider and this difference is likely to harm the health or safety of the service recipient, the registrant must submit a aptitude test. If he refuses, the provision of services can not be achieved. The aptitude test must be organized within three months from the submission of complete documentation of prior declaration of activity. If this deadline is not met, the recognition of professional qualifications is deemed granted and the provision of services can begin. For more information on the aptitude test, it is advisable to see above under "Good to know: compensation measures."

#### recourse

Any decision of the CMA submit declaring an aptitude test may be an administrative appeal to the prefect of the department in which the CMA has its headquarters, within three months of notification of the decision of the CMA. If the appeal is unsuccessful, the declarant may then initiate a judicial appeal.

#### Cost

The statement is free. However, if the declarant part in an aptitude test, organizational expenses will be requested it.

*For further*: Article 17-1 of Law No. 96-603 of 5 July 1996 supra, 3 and following of Decree No. 98-246 of 2 April 1998 cited above, Articles 1 and 6 of the Decree of 28 October 2009 cited above.

### vs. Remedies

#### French Support Center

The NARIC Center is the French information center on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided by the national administration of each Member State of the European Union or party to the EEA Agreement. Its goal is to find a solution to a dispute between an EU citizen to the administration of another of those States. SOLVIT including the recognition of professional qualifications.

##### terms

The person may not use SOLVIT if it establishes:

- that the public administration of an EU state has not respected the rights that EU law gives it as a citizen or company from another EU state;
- and has not already initiated legal action (the administrative appeal is not considered as such).

##### Procedure

The citizen must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once transmitted sound file, contact the SOLVIT within a week to ask, if necessary, additional information and to verify that the problem is within its jurisdiction.

##### Vouchers

To enter SOLVIT, the national shall communicate:

- full contact details;
- Detailed description of the problem;
- all evidence of the folder (for example, correspondence received and the decisions of the administrative authority concerned).

##### Time limit

SOLVIT is committed to finding a solution within ten weeks from the date of the management of the file by the SOLVIT center of the country where the problem occurred.

##### Cost

Free.

##### Following the procedure

At the end of ten weeks, the SOLVIT has a solution:

- If this solution resolves the dispute on the application of European law, the solution is accepted and the file is closed;
- if there is no solution, the case is closed as unresolved and forwarded to the European Commission.

**Additional Information**: SOLVIT France: General Secretariat for European Affairs, 68 Rue de Bellechasse, 75700 Paris,[official site](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

