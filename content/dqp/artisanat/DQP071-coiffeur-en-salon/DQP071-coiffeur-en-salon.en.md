﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP071" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="Hairdresser in a salon" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="hairdresser-in-a-salon" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/hairdresser-in-a-salon.html" -->
<!-- var(last-update)="2020-04-15 17:20:28" -->
<!-- var(url-name)="hairdresser-in-a-salon" -->
<!-- var(translation)="Auto" -->


Hairdresser in a salon
====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The salon hairdresser is a hair professional. In this capacity, he advises his clients in the choice of their hairstyles according to their morphology and tastes. It makes cuts as well as the application of temporary or durable care and colors.

In addition to this creative role, the hairdresser in the salon can also sell hair or beauty products.

When the hairdresser is in charge of the salon, he or she must take care of the recruitment and coordination of his team.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The person concerned wishing to work as a hairdresser in the salon must have a professional qualification or exercise under the effective and permanent control of a person with this qualification.

To be considered professionally qualified, the person must hold one of the following diplomas or titles:

- professional patent (BP) specialty "hairstyle";
- Master's certificate (BM) specialty "hairstyle";
- certified or registered at the time of its issuance at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

*For further information*: :[Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1AC41CCDC69D007EC85FD499D4D6CEA2.tplgfr29s_3?cidTexte=JORFTEXT000000388449&idArticle=LEGIARTI000034624936&dateTexte=20170506&categorieLien=id#LEGIARTI000034624936) Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

#### Training

The professional "hairdressing" certificate is available in an apprenticeship contract, after a continuous training course, in a professionalisation contract, by individual application or by[validation of experience](http://www.vae.gouv.fr/) (VAE). Most often, the training leading to this professional certificate is for two years.

The master's degree "hairdressing" is issued by the Chamber of Trades and Crafts (CMA). It is accessible after a continuous training course, in an apprenticeship contract, in a professionalization contract or by VAE. It lasts two years.

*For further information*: Article D. 337-95 of the Education Code;[September 21, 2010](http://www.journal-officiel.gouv.fr/publications/bocc/pdf/2010/0045/CCO_20100045_0045_0006.pdf) on the creation of the Master's degree, Level III.

#### Cost associated with qualification

The training is usually free. For more details, it is advisable to refer to the training centre in question.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or the European Economic Area (EEA) established and legally practising the activity of hairdresser in salon in that State may exercise the same activity in France, temporarily and occasionally.

He must first make the request by declaration to the CMA of the place in which he wishes to carry out the service.

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the country in which the professional is legally established, he must have carried out this activity for at least one year during the ten years before the benefit, in one or more EU Member States.

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent CMA may require that the person concerned submit to an aptitude test.

*For further information*: Article 17-1 of the Act of 5 July 1996; Article 2 of the[decree of April 2, 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid) modified by the[decree of May 4, 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7D50D037AAEEDE8367FE375890CB8510.tplgfr39s_2?cidTexte=JORFTEXT000034598573&dateTexte=20170506).

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

In order to carry out the activity of hairdresser in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above "2 degrees). a. Professional qualifications");
- Have held the business for six consecutive years independently or as a manager;
- have held the activity for three consecutive years independently or as an officer, as long as it justifies a certificate obtained in a Member State and attesting to the follow-up of a pre-training of at least three years;
- having been in the business for three consecutive years independently as long as he justifies having previously carried out the activity for a minimum of five years as an employee;
- be the holder of a diploma, title or certificate acquired in a third country and admitted in equivalency by an EU or EEA state on the additional condition that the person has worked as a hairdresser in an independent salon for three years, executive or employee status.

**Please note**

The national must have carried out the activity within ten years of applying for a permanent settlement in France.

Once the national of an EU or EEA state fulfils one of the above conditions, he or she will be able to apply for a certificate of recognition of professional qualification (see below "5o). b. Request a certificate of professional qualification for the EU or EEA national for a permanent exercise (LE))

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent CMA may require that the person concerned submit to compensation measures (see infra 5. a. Good to know: compensation measures").

*For further information*: Article 17 of Law 96-603 of 5 July 1996 above; Articles 1 paragraph II, 3 paragraph III to 3-2 of the decree of 2 April 1998 amended by the decree of 4 May 2017.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

No one may practise the profession if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of July 5, 1996.

4°. Insurance
---------------------------------

A hairdresser in a salon practising on a liberal basis must take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The CMA of the place in which the national wishes to carry out the benefit, is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to carry out this compensation measure or in the event of failure in its execution, the national will not be able to carry out the provision of services in France.

The silence kept by the competent authority in these times is worth authorisation to begin the provision of services.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the[October 17, 2017](https://www.legifrance.gouv.fr/eli/arrete/2017/10/17/ECOI1719273A/jo/texte/fr) regarding the submission of the declaration and the requests provided for by Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998.

### b. Request a certificate of recognition of professional qualification for the EU or EEA national in the event of a permanent exercise (LE)

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the appropriate CMA of the place in which the person wishes to settle.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as it is complete.

**Supporting documents**

The application for certification of professional qualification is a file with the following supporting documents:

- An application for a certificate of professional qualification
- a proof of professional qualification in the form of a certificate of competency or a diploma or a vocational training certificate;
- A photocopy of the applicant's valid ID
- if work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin.

**What to know**

If necessary, all supporting documents must be translated into French by a certified translator.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (Ciep) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**Timeframe**

Within three months of the receipt, the CMA may decide to:

- Recognise professional qualification and issue certification of professional qualification;
- submit the national to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

**What to know**

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses the CMA's application for professional qualification, the applicant may challenge the decision. It can thus, within two months of notification of the CMA's refusal, form:

- a legal challenge to the relevant administrative court;
- a graceful appeal to the prefect of the relevant CMA department;
- a legal challenge before the relevant administrative court.

**Cost**

Free.

**Good to know: compensation measures**

The CMA notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

**Cost**

A fixed fee covering the investigation of the case may be charged. For more information, it is advisable to get closer to the relevant CMA.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

