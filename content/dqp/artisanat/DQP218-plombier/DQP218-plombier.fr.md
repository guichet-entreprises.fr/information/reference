﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP218" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artisanat" -->
<!-- var(title)="Plombier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artisanat" -->
<!-- var(title-short)="plombier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/artisanat/plombier.html" -->
<!-- var(last-update)="2020-04-30 11:23:49" -->
<!-- var(url-name)="plombier" -->
<!-- var(translation)="None" -->

# Plombier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30 11:23:49<!-- end-var -->

## 1°. Définition de l’activité

Professionnel chargé de l’installation et de la réparation des canalisations d’eau et de gaz. Il prépare notamment la pose et la mise en service d’une installation sanitaire neuve (salle de bain, évier, piscine, etc.). Le plombier effectue également des remises en état sur des installations sanitaires anciennes et leurs équipements.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer en qualité de plombier, l’intéressé doit être qualifié professionnellement, ou exercer sous le contrôle effectif et permanent d’une telle personne.

Selon le décret n°98-246 du 2 avril 1998, une personne est considérée comme qualifiée professionnellement à la condition qu’elle soit titulaire de l’un des diplômes ou titres suivants :

- un certificat d’aptitude professionnelle (CAP) ;
- un brevet d’études professionnelles (BEP) ;
- un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) et délivré pour l’exercice du métier de plombier.

Dans le cas de l’activité de plombier, la personne intéressée peut notamment être titulaire, au choix :

- du CAP « installateur sanitaire » ou « installateur thermique » ;
- du bac pro « technicien du froid et du conditionnement de l’air » ou « technicien de maintenance des systèmes énergétiques et climatiques » ;
- du brevet professionnel « équipements sanitaires », « monteur en installation du génie climatique et sanitaire » ou « métiers de la piscine ».

À défaut de l’un de ces diplômes ou titres, l’intéressé doit justifier d’une expérience professionnelle de trois années effectives sur le territoire de l’Union européenne (UE) *ou de l’Espace économique européen (EEE)*, acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier de plombier. Dans ce cas, il est conseillé à l’intéressé de s’adresser à la chambre des métiers et de l’artisanat (CMA) pour demander une attestation de qualification professionnelle.

**À noter**

Une personne non qualifiée peut tout de même exercer la profession à la condition qu’elle soit placée sous le contrôle effectif et permanent d’une personne elle-même qualifiée, au sens du décret du 2 avril 1998 précité.

*Pour aller plus loin* : article 16 de la loi n°96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat, décret n°98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n°96-603 du 5 juillet 1996 précitée.

#### Formation

Le CAP est un diplôme de niveau V (c’est-à-dire correspondant à une sortie de second cycle général et technologique avant l’année de terminale). Les bac pro et brevet professionnel sont, quant à eux, des diplômes de niveau IV (c’est-à-dire niveau baccalauréat général, technologique ou professionnel).

Les CAP « installateur sanitaire » et « installateur thermique » sont accessibles après une classe de troisième générale, SEGPA ou prépa pro ou après un CAP d’un secteur proche. La sélection s’effectue le plus souvent sur dossier et sur tests. Ces diplômes sont accessibles après un parcours de formation sous statut d’élève ou d’étudiant, en contrat d’apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou par validation des acquis de l’expérience (VAE). Pour plus d’informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr/). La formation menant à l’obtention de l’un de ces deux CAP est d’une durée de deux ans et s’effectue dans un lycée professionnel.

Le bac pro « technicien du froid et du conditionnement de l’air » ou « technicien de maintenance des systèmes énergétiques et climatiques » est accessible après une classe de troisième. La formation menant à l’obtention de l’un de ces bacs pro dure trois ans. La sélection s’effectue le plus souvent sur dossier et sur tests. Ces diplômes sont accessibles après un parcours de formation sous statut d’élève ou d’étudiant, en contrat d’apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou par validation des acquis de l’expérience (VAE). Pour plus d’informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr/).

Les brevets professionnels « équipements sanitaires », « monteur en installation du génie climatique et sanitaire », « métiers de la piscine » sont accessibles en contrat d’apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou par validation des acquis de l’expérience (VAE). Pour plus d’informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr/). Le plus souvent, la formation menant à l’obtention de l’un de ces brevets professionnels est d’une durée de deux ans.

*Pour aller plus loin* : articles D. 337-1 et suivants du Code de l'éducation.

#### Coûts associés à la qualification

La formation est, le plus souvent, gratuite. Pour plus de précisions, il est conseillé de se reporter au centre de formation considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France, à titre temporaire et occasionnel, le contrôle effectif et permanent de l’activité de plombier à la condition d’être légalement établi dans un de ces États pour y exercer la même activité.

Si ni l’activité ni la formation y conduisant n’est réglementée dans l’État d’établissement, l’intéressé doit en outre prouver qu’il a exercé l’activité de plombier dans cet État pendant au moins l’équivalent de deux ans à temps plein au cours des dix dernières années précédant la prestation qu’il veut effectuer en France.

*Pour aller plus loin* : article 17-1 de la loi n°96-603 du 5 juillet 1996 précitée.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Pour exercer en France, à titre permanent, le contrôle effectif et permanent de l’activité de plombier, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un ressortissant français (cf. supra « 2°.a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité de plombier dans un État de l’UE ou de l’EEE lorsqu’il réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétence ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité de plombier lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité de plombier dans l’État qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de qualification professionnelle à exercer le contrôle effectif et permanent de l’activité de plombier. Pour plus d’informations, il est conseillé de se reporter infra au paragraphe « 5°. b. Demander une attestation de reconnaissance de qualification professionnelle, le cas échéant ».

Si l’intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité de plombier ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité de plombier ;
- si le contrôle effectif et permanent de l’activité de plombier nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l'attestation de compétences ou le titre de formation dont le demandeur fait état.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n°96-603 du 5 juillet 1996 précitée, articles 3 à 3-2 du décret n°98-246 du 2 avril 1998 précité.

#### Bon à savoir : mesures de compensation

La CMA, saisie d’une demande de délivrance d’une attestation de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans et une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À la fin du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin* : articles 3 et 3-2 du décret n°98-246 du 2 avril 1998 précité, article 6-1 du décret n°83-517 du 24 juin 1983 fixant les conditions d’application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Nul ne peut exercer la profession de plombier s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du code pénal.

*Pour aller plus loin* : article 19 III de la loi n°96-603 du 5 juillet 1996 précitée.

## 4°. Assurance de responsabilité civile obligatoire

Le professionnel doit souscrire une assurance responsabilité civile professionnelle. Elle lui permet d’être couvert pour les dommages causés à autrui, qu’il en soit directement à l’origine ou qu’ils soient le fait de ses salariés, de ses locaux ou de son matériel.

Les artisans intervenant dans les travaux de gros œuvre et de construction sont soumis à l’obligation de souscrire une assistance de responsabilité civile décennale.

Les références du contrat d’assurance doivent apparaître sur les devis et factures de l’artisan.

*Pour aller plus loin* : article L. 241-1 du code des assurances et article 22-2 de la loi n°96-603 du 5 juillet 1996 précitée.

## 5°. Démarches et formalités de reconnaissance de qualifications

### a. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître son diplôme, autre que celui exigé en France, ou son expérience professionnelle peut demander une attestation de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les 15 jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier de demande d’attestation de qualification professionnelle doit contenir :

- la demande d’attestation de qualification professionnelle ;
- le justificatif de la qualification professionnelle : une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme, d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

Toutes les pièces justificatives doivent être traduites en français (traduction certifiée), le cas échéant.

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut, au choix :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifie cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif compétent dans les deux mois suivant la notification du refus de la CMA. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département où la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra alors opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n°98-246 du 2 avril 1998 précité, arrêté du 28 octobre 2009 pris en application des décrets n°97-558 du 29 mai 1997 et n°98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### b. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Services)

Les ressortissants de l’UE ou de l’EEE souhaitant exercer en France de manière occasionnelle et ponctuelle sont soumis à une obligation de déclaration préalable à leur première prestation sur le sol français.

Cette déclaration préalable d’activité doit être renouvelée tous les ans si l’intéressé entend exercer de nouveau en France.

#### Autorité compétente

La déclaration préalable d’activité est adressée à la CMA dans le ressort de laquelle le déclarant envisage de réaliser sa prestation.

#### Récépissé

La CMA délivre au déclarant un récépissé mentionnant la date à laquelle elle a reçu le dossier complet de déclaration préalable d’activité. Si le dossier est incomplet, la CMA notifie dans les 15 jours la liste des pièces manquantes au déclarant. Elle délivre le récépissé dès que le dossier est complet.

#### Pièces justificatives

La déclaration préalable d’activités doit comporter :

- la déclaration datée et signée, mentionnant les informations relatives aux assurances civiles professionnelles obligatoires ;
- la preuve de la nationalité du déclarant ;
- la preuve des qualifications professionnelles du déclarant : titre de formation, attestation de compétence délivrée par l’autorité compétente dans l’État d’établissement, tout document attestant d’une expérience professionnelle indiquant sa nature et sa durée, etc. ;
- le cas échéant, la preuve que le déclarant a exercé pendant au moins deux ans à temps complet l’activité de plombier au cours des dix dernières années ;
- une attestation d’établissement dans un État de l’UE ou de l’EEE pour exercer l’activité de plombier ;
- une attestation de non condamnation à une interdiction, même temporaire, d’exercer.

Tous les documents doivent être traduits en français (par un traducteur certifié) s’ils ne sont pas établis en français.

#### Délai

Dans un délai d’un mois à compter de la réception du dossier complet de déclaration préalable d’activité, la CMA délivre au déclarant une attestation de qualification professionnelle ou lui notifie la nécessité de procéder à un examen complémentaire. Dans ce dernier cas, la CMA notifie sa décision finale dans les deux mois à compter de la réception du dossier complet de déclaration préalable d’activité. À défaut de notification dans ce délai, la déclaration préalable est réputée acquise et la prestation de services peut donc débuter.

Pour prendre sa décision, la CMA peut s’adresser à l’autorité compétente de l’État d’établissement du déclarant pour obtenir toute information concernant la légalité de l’établissement et son absence de sanction disciplinaire ou pénale à caractère professionnel.

**Bon à savoir** : si la CMA relève une différence substantielle entre la qualification professionnelle requise pour exercer en France et celle déclarée par le prestataire et que cette différence est de nature à nuire à la santé ou à la sécurité du bénéficiaire du service, le déclarant est invité à se soumettre à une épreuve d’aptitude. S’il s’y refuse, la prestation de services ne peut être réalisée. L’épreuve d’aptitude doit être organisée dans un délai de trois mois à compter du dépôt du dossier complet de déclaration préalable d’activité. Si ce délai n’est pas respecté, la reconnaissance de qualification professionnelle est réputée acquise et la prestation de services peut débuter. Pour plus d’informations sur l’épreuve d’aptitude, il est conseillé de se reporter supra au paragraphe « Bon à savoir : les mesures de compensation ».

#### Recours

Toute décision de la CMA de soumettre le déclarant à une épreuve d’aptitude peut faire l’objet d’un recours administratif auprès du préfet du département dans lequel la CMA a son siège, dans les trois mois suivant la notification de la décision de la CMA. Si le recours est infructueux, le déclarant peut alors initier un recours contentieux.

#### Coût

La déclaration préalable est gratuite. Cependant, si le déclarant participe à une épreuve d’aptitude, des frais d’organisation pourront lui être demandés.

*Pour aller plus loin* : article 17-1 de la loi n°96-603 du 5 juillet 1996 précitée, articles 3 et suivants du décret n°98-246 du 2 avril 1998 précité, articles 1er et 6 de l’arrêté du 28 octobre 2009 précité.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’informations sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- et qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris, [site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).