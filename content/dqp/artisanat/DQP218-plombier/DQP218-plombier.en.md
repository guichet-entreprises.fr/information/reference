﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP218" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="Plumber" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="plumber" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/plumber.html" -->
<!-- var(last-update)="2020-04-15 17:20:36" -->
<!-- var(url-name)="plumber" -->
<!-- var(translation)="Auto" -->




Plumber
=======

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:36<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

Professional in charge of the installation and repair of water and gas pipes. In particular, he prepares the installation and commissioning of a new sanitary facility (bathroom, sink, swimming pool, etc.). The plumber also performs refurbishments on old sanitary facilities and their equipment.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice as a plumber, the person must be qualified professionally, or exercise under the effective and permanent control of such a person.

According to Decree 98-246 of 2 April 1998, a person is considered to be professionally qualified on the condition that he or she holds one of the following diplomas or titles:

- A Certificate of Professional Qualification (CAP)
- A Professional Studies Patent (BEP);
- a title of equal or higher level approved or registered when it was issued at the [national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) and issued for the job of plumber.

In the case of plumber's activity, the person concerned may, among other things, be the holder, of the choice:

- CAP "sanitary installer" or "thermal installer";
- the pro "cold and air conditioning technician" or "energy and climate system maintenance technician";
- "sanitary equipment," "climate and health engineering installation fitter" or "pool trades."

In the absence of one of these diplomas or titles, the person concerned must justify an effective three years of professional experience in the territory of the European Union (EU)*European Economic Area (EEA)*, acquired as a company manager, self-employed or salaried in the job of plumber. In this case, the person concerned is advised to contact the Chamber of Trades and Crafts (CMA) to request a certificate of professional qualification.

**Please note**
An unqualified person may still practise the profession on the condition that he or she be placed under the effective and permanent control of a qualified person, within the meaning of the decree of 2 April 1998 above.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts, Decree No.98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of The Act No. 5 July 1996.

#### Training

The CAP is a Level V diploma (i.e. corresponding to a general and technological graduate before the senior year). The pro and professional certificates are level IV diplomas (i.e. general, technological or professional bachelor's degree).

The "sanitary installer" and "thermal installer" CAPS are accessible after a third-general class, SEGPA or prep pro or after a CAP from a nearby area. The selection is usually done on file and on tests. These diplomas are accessible after a training course under student status, apprenticeship contract, after a continuing education course, professionalization contract, individual application or validation of achievements. (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE. The training leading to one of these two PDCs is for two years and is carried out in a vocational high school.

The pro "cold and air conditioning technician" or "energy and climate system maintenance technician" is available after a third-grade class. The training leading to one of these pro bacs lasts three years. The selection is usually done on file and on tests. These diplomas are accessible after a training course under student status, apprenticeship contract, after a continuing education course, professionalization contract, individual application or validation of achievements. (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

Professional patents "sanitary equipment," "climate and health engineering installation fitter," "pool trades" are available in an apprenticeship contract, after a continuous training course, in a contract of professionalization, by individual application or by validation of experience (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE. Most often, the training to obtain one of these professional patents is for two years.

*For further information*: Articles D. 337-1 and the following from the Education Code.

#### Costs associated with qualification

Training is, more often than not, free. For more details, it is advisable to refer to the training centre in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

The professional who is a member of the EU or the EEA may exercise effective and permanent control of plumber's activity in France on a temporary and casual basis, provided that he or she is legally established in one of these states to carry out the same activity.

If neither the activity nor the training leading to it is regulated in the State of The Establishment, the person must also prove that he has been a plumber in that state for at least the equivalent of two years full-time in the last ten years. years before the performance he wants to perform in France.

*For further information*: Article 17-1 of Act 96-603 of 5 July 1996.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

In order to carry out on a permanent basis in France the effective and permanent control of plumber's activity, the professional who is an EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as a French national (see above: "2.a. Professional qualifications");
- hold a certificate of competency or training certificate required for the exercise of plumber's activity in an EU or EEA state when regulating access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its readiness to carry out plumber's activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or exercise of this Activity
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been a plumber in the state that has admitted for three years equivalence.

**Please note**

A national of an EU or EEA state that fulfils one of the above conditions may apply for a certificate of professional qualification to exercise effective and permanent control over plumber's activity. For more information, it is advisable to refer to paragraph "5o" below. b. Request a certificate of recognition of professional qualification, if necessary."

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the plumber's activity;
- If the training received covers subjects substantially different from those covered by one of the titles or diplomas required to carry out plumber activity in France;
- If the effective and permanent control of plumber's activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and covers substantially different subjects of those covered by the certificate of competency or training designation referred to by the applicant.

*For further information*: Articles 17 and 17-1 of Act 96-603 of 5 July 1996, Articles 3 to 3-2 of Decree No. 98-246 of 2 April 1998.

**Good to know: compensation measures**

The CMA, which is applying for a certificate of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years and an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree No.98-246 of 2 April 1998, Article 6-1 of Decree No.83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

No one may practise as a plumber if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of 5 July 1996.

4°. Compulsory liability insurance
------------------------------------------------------

The professional must take out professional liability insurance. It allows it to be covered for damage caused to others, whether directly caused or by its employees, premises or equipment.

Craftsmen involved in the construction and construction work are obliged to take out a ten-year civil liability assistance.

The insurance contract references must appear on the craftsman's quotes and invoices.

*For further information*: Article L. 241-1 of the Insurance Code and Section 22-2 of Act 96-603 of July 5, 1996.

5°. Qualifications process and formalities
----------------------------------------------------

### a. If necessary, apply for a certificate of recognition of professional qualification

The person concerned wishing to have his diploma recognized, other than that required in France, or his professional experience may apply for a certificate of professional qualification.

#### Competent authority

The request must be addressed to the territorially competent CMA.

#### Procedure

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the applicant to complete it within 15 days of filing the file. A receipt is issued as soon as the file is complete.

#### Supporting documents

The application for a certificate of professional qualification must include:

- Applying for a certificate of professional qualification
- Proof of professional qualification: a certificate of competency or a diploma or vocational training certificate;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma, certificate or foreign designation, the applicant will have to pay a fee Additional.

All supporting documents must be translated into French, if required.

#### Response time

Within three months of the receipt, the CMA may, at its choice:

- Recognise professional qualification and issue certification of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

#### Remedies

If the CMA refuses to issue the recognition of professional qualification, the applicant can initiate a legal action before the relevant administrative court within two months of notification of the refusal of the CMA. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department where the CMA is based, within two months of notification of the CMA's decision. If he does not succeed, he can then opt for a litigation before the competent administrative court.

*For further information*: Articles 3 to 3-2 of Decree No.98-246 of 2 April 1998, order of 28 October 2009 taken in according decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 and relating to the procedure for the recognition of professional qualifications of a a member state of the European Community or another state party to the European Economic Area agreement.

### b. Make a prior declaration of activity for EU nationals engaged in one-off activity (Freedom to provide services)

EU or EEA nationals wishing to practice in France on an occasional and ad hoc basis are subject to a pre-reporting obligation on their first service on French soil.

This prior declaration of activity must be renewed every year if the person concerned intends to practice again in France.

#### Competent authority

The prior declaration of activity is addressed to the CMA in the jurisdiction from which the registrant plans to carry out his performance.

#### Receipt

The CMA issues a receipt to the registrant stating the date on which it received the full pre-report of activity file. If the file is incomplete, the CMA notifies the list of missing documents to the registrant within 15 days. It issues the receipt as soon as the file is complete.

#### Supporting documents

Pre-reporting of activities must include:

- The dated and signed declaration, mentioning information on compulsory professional civil insurance;
- Proof of the nationality of the registrant;
- proof of the registrant's professional qualifications: training title, certificate of competence issued by the competent authority in the State of establishment, any document attesting to professional experience indicating its nature and nature Duration, etc.
- if applicable, evidence that the registrant has been a plumber for at least two years on a full-time basis for the past ten years;
- A certificate of establishment in an EU or EEA state to carry out plumber activity;
- a certificate of non-condemnation to a ban, even temporary, from practising.

All documents must be translated into French (by a certified translator) if they are not drawn up in French.

#### Time

Within one month of receiving the full pre-report of activity file, the CMA issues a certificate of professional qualification to the declarant or advises the declarate of the need for further examination. In the latter case, the CMA notifies its final decision within two months of receipt of the full pre-report of activity file. In the absence of notification within this time frame, the pre-declaration is deemed to have been acquired and the service delivery may therefore begin.

In making its decision, the CMA may contact the competent authority of the State of Settlement of the registrant for any information regarding the legality of the institution and its absence of disciplinary or criminal sanction of a nature. Professional.

**Good to know** If the CMA finds a substantial difference between the professional qualification required to practise in France and that declared by the claimant and this difference is likely to adversely affect the health or safety of the service recipient, the registrant is invited to submit to an aptitude test. If it refuses to do so, the provision of services cannot be carried out. The aptitude test must be held within three months of filing the full pre-report activity file. If this deadline is not met, recognition of professional qualification is deemed to have been acquired and service delivery may begin. For more information on the aptitude test, it is advisable to refer to the paragraph "Good to know: compensation measures".

#### Use

Any decision by the CMA to subject the registrant to an aptitude test may be the subject of an administrative appeal to the prefect of the department in which the CMA is based, within three months of notification of the CMA's decision. If the appeal is unsuccessful, the registrant can then initiate a litigation.

#### Cost

Pre-reporting is free of charge. However, if the registrant participates in an aptitude test, he or she may be charged an organizational fee.

*For further information*: Article 17-1 of Act No.96-603 of 5 July 1996, Articles 3 and following of Decree No.98-246 of 2 April 1998, Articles 1 and 6 of the Order of 28 October 2009 above.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- and that he has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

###### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information** : SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris,[official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

