﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP100" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="Electrician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="electrician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/electrician.html" -->
<!-- var(last-update)="2020-04-15 17:20:29" -->
<!-- var(url-name)="electrician" -->
<!-- var(translation)="Auto" -->


Electrician
===========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:29<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1. Definition of activity
-------------------------

The electrician is a professional responsible for the implementation, maintenance and modification of electrical installations.

He may have to ask cable lines to which it connects the electrical equipment, identify the locations of future electrical breakers and tables, or to undertake network commissioning at customer.

2 °. Professional qualifications
--------------------------------

### at. national requirements

#### national legislation

The person wishing to exercise the activity of electrician must have a professional qualification or practice under the effective control and permanent for a person with this qualification.

To be considered professionally qualified, the person must hold one of the following diplomas or degrees:

- a certificate of vocational aptitude (CAP) specialty "preparation and execution of electrical works";
- professional bachelor "electrical trades and its connected environment";
- a professional certificate (BP) specialty "electrician";
- a technical patent in the art (BTM) "installer electrical equipment";
- a diploma or a title or approved equal or greater recorded in issuing the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)(RNCP).

Failure of one of these diplomas or qualifications, the applicant must demonstrate professional experience of three years effective on the territory of the European Union (EU) or the European Economic Area (EEA) gained as corporate executive, independent or employed in the exercise of an electrician. In this case, it is advisable for the person to turn to the business chamber and Crafts (CMA) to request a certificate of professional qualification.

*For further*: Article 16 of Law No. 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree No. 98-246 of 2 April 1998 on the professional qualifications required for the performance of activities provided for in Article 16 of Law No. 96-603 of 5 July 1996 cited above.

#### Training

The CAP is a level V diploma (that is to say, corresponding to an output of the second cycle and general technology before the end of year). The pro tray, BP and BTM are, in turn, the level IV degrees (that is to say general degree level, technological or vocational).

The CAP is accessible after a third class. Selection is usually on record and on tests. This diploma is available after a training course for student or student under statute, in apprenticeship, after a training course, professional contract by individual candidates or[validation of acquired experience](http://www.vae.gouv.fr/)(VAE). The training lasts two years and normally takes place in a vocational school.

The professional certificate is available on apprenticeship contracts, after a training course, professional contract by individual candidates or validation of prior experience. Most often, the training leading to the award of one of these professional patent is for two years.

Tray Pro is available after a third class. The training leading to his obtaining lasts three years. Selection is usually on record and on tests. These degrees are available after a training course with the status of pupil or student, apprenticeship, after a training course, professional contract by individual candidates or validation of prior experience.

The BTM is a specific title crafts open to the holder of a diploma level IV or V. The diploma is available after a training course in apprenticeship, professional contract by individual candidates or validation of acquired experience. The training lasts two years in principle.

*For further*: Articles D. 337-1 et seq of the Education Code.

#### Costs associated with the qualification

Training is usually free. For more information, you are advised to refer to the particular training center.

### b. EU national or EEA: for a temporary and occasional exercise (Freedom of Services)

All of a national EU Member State or EEA established and legally employed electrician business in this State may exercise in France, temporary and occasional basis, the same activity.

It must first be requested by the CMA statement of the place in which he wishes to make the delivery.

If the profession is not regulated, either as part of the activity, either as part of the training, in the country where the trader is legally established, it must have practiced that activity for at least one year over the ten years preceding the provision, in one or more EU Member States.

Where there are substantial differences between the professional qualification of the national and the training required in France, the relevant CMA may require the applicant to undergo an aptitude test.

*For further*:[Article 17-1](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000193678&idArticle=LEGIARTI000018891027&dateTexte=&categorieLien=cid)the law of 5 July 1996; Article 2[Decree of 2 April 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid)modified by the decree of May 4, 2017.

### vs. EU national or EEA: for a permanent exercise (Free Country)

To carry on business as an electrician in France permanently, a national of the EU or EEA must meet one of the following conditions:

- have the same qualifications as those required for French (see above "2. Professional qualifications have..");
- hold a certificate of competency or qualification training required for the exercise of the electrical activity in a State of the EU or EEA where that State regulates access or exercise this activity in its territory;
- have a certificate of competency or a formal qualification that certifies his preparation for the exercise of the electrical activity when the certificate or title has been obtained in a State of the EU or EEA which regulates neither the access nor the exercise of this activity;
- hold a diploma or certificate acquired in a third country and recognized as equivalent by a State of the EU or EEA to the additional condition that the person concerned has served for three years the electrical activity in the state that acknowledged equivalence.

Once it meets one of the above conditions to the national rule of the EU or EEA may require a professional qualification recognition certificate (see below "5 °. B. Request a certificate of qualification in the EU national or EEA for a permanent exercise (LE) ".)

Where there are substantial differences between the professional qualification of the national and the training required in France, the relevant CMA may require the applicant to undergo compensation measures (see below "5 ° was good to know..: compensatory measures').

*For further*: Articles 17 and 17-1 of Law No. 96-603 of 5 July 1996 mentioned above; Articles 3 to 3-2 of the Decree of 2 April 1998 as amended by Decree of May 4, 2017.

3 °. Terms of repute, professional ethics, ethics
-------------------------------------------------

No one may exercise the profession if the subject:

- prohibited from directing, managing, administering or directly or indirectly control a commercial or craft business;
- a penalty of disqualification from professional or social activity for one of the crimes or offenses provided for in Article 11 of the Penal Code 131-6.

*For further*: Article 19 III of Law No. 96-603 of 5 July 1996 cited above.

4 °. Insurance
--------------

The electrician in private practice must take out professional liability insurance. However, if he exercises as an employee, this insurance is only optional. In this case, it is the employer to subscribe to its employees such insurance for acts performed on the occasion of their professional activity.

5 °. Approaches and qualification recognition procedures
--------------------------------------------------------

### at. Ask a prior declaration of activity for the EU national or EEA for a temporary and occasional exercise (LPS)

**Competent authority**

The CMA place in which he wishes to perform the service is competent to issue the prior declaration of activity.

**Vouchers**

The prior declaration of activity request is accompanied by a complete file containing the following documents:

- a photocopy of an ID valid;
- a certificate proving that the national is legally established in a State of the EU or the EEA;
- a document proving the professional qualification of nationals who may be either:- a copy of a diploma or certificate,
  - a certificate of competence,
  - documents evidencing the professional experience of the national.

**To know**

If necessary, the parts must be translated into French by a certified translator.

**To note**

When the file is incomplete, the CMA has a period of fifteen days to inform the citizen and ask all the missing pieces.

**Following the procedure**

Upon receipt of all the documents, the CMA has a period of one month to decide:

- or to authorize the provision when the citizen has professional experience of three years in a State of the EU or EEA. The CMA then join this decision a certificate of professional qualification;
- or to authorize the provision when the national professional qualifications are considered sufficient;
- or impose a when aptitude test are substantial differences between the professional qualifications of the nationals and those required in France. In case of refusal to perform this compensation measure or if it fails in its execution, the citizen can not perform the service in France.

The silence of the competent authority within that time is authorized to begin providing services.

*For further*: Article 2 of Decree of 2 April 1998; Article 2 of the decree of 17 October 2017 concerning the presentation of the declaration and applications provided by Decree No. 98-246 of 2 April 1998 and Title I of Decree No. 98-247 of 2 April 1998.

### b. Ask a professional qualification recognition certificate for EU national or EEA in case of permanent exercise (LE)

The person wishing to recognize a diploma other than that required in France or work experience may apply for a professional qualification recognition certificate.

**Competent authority**

The request must be sent to the appropriate CMA venue in which the applicant wishes to settle.

**Procedure**

A request for postponement of receipt is sent to the applicant within one month of receipt by the CMA. If the file is incomplete, CMA asks the applicant to complete it within fifteen days of the filing of the dossier. A receipt is issued as soon as it is complete.

**Vouchers**

The demand for professional qualification recognition certificate is a file containing the following documents:

- a professional qualification certificate application;
- proof of the professional qualification in the form of a certificate of competency or a degree or title of vocational training;
- a copy of the applicant's identity document valid;
- whether professional experience was acquired in the territory of a State of the EU or the EEA, a certificate concerning the nature and duration of the activity issued by the competent authority in the Member State original;
- whether professional experience was acquired in France, supporting the exercise of the activity for three years.

**To know**

If necessary, all supporting documents must be translated into French by a certified translator.

The CMA may ask for additional information regarding training or professional experience to determine the possible existence of substantial differences with the professional qualifications required in France. Also, if the MAC must approach the International Center for Pedagogical Studies (CIEP) for more information on the level of training a diploma or certificate or a foreign title, the applicant must s' pay an additional surcharge.

**Time limit**

Within three months of the issue of the receipt, the MAC may decide to:

- recognize the professional qualifications and issue the certificate of professional qualification;
- submit the citizen to a compensation and notify that decision;
- refuse to issue the certificate of professional qualification.

**To know**

In the absence of a decision within four months, the demand for professional qualification certificate is deemed granted.

**Remedies**

In case of refusal of the professional qualification recognition application by the CMA, the applicant may appeal the decision. He can, within two months of notification of the refusal of the CMA, form:

- gracious appeal to the prefect of the department of competent CMA;
- an appeal with the competent administrative court.

**Cost**

Free.

**Good to know: compensation measures**

CMA notify the applicant of its decision to make him accomplish compensation measures. The decision lists the areas not yet covered by the qualifications attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an internship matching up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organized within six months from the receipt by the CMA plaintiff's decision to opt for this event. Otherwise, the qualification is deemed granted and CMA issues a certificate of qualification.

At the end of the adaptation period, the applicant address to the CMA a certificate stating that he validly completed the course, accompanied by an assessment of the organization that oversaw. The CMA shall, on the basis of this certificate, a certificate of qualification in a period of one month.

The decision to use a compensation measure may be challenged by the person who has an administrative appeal to the prefect within two months of notification of the decision. In case of rejection of its application, it can initiate a judicial appeal.

**Cost**

Fixed costs covering the investigation of the case can be requested. For more information, it is advisable to approach the competent CMA.

*For further*: Articles 3 3-2 of Decree No. 98-246 of 2 April 1998 cited above; decree of 28 October 2009 adopted under decree No. 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 and on the procedure of recognition of professional qualifications of a professional national Member State the European Community or another State party to the agreement on the European economic Area.

### vs. Remedies

#### French Support Center

The NARIC Center is the French information center on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided by the State Administration of each EU Member State or party to the EEA Agreement. Its goal is to find a solution to a dispute between an EU citizen to the administration of another of those States. SOLVIT including the recognition of professional qualifications.

**terms**

The person may not use SOLVIT if it establishes:

- that the public administration of an EU state has not respected the rights that EU law gives it as a citizen or company from another EU state;
- he has not already initiated legal action (the administrative appeal is not considered as such).

**Procedure**

The citizen must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once transmitted sound file, contact the SOLVIT within a week to ask, if necessary, additional information and to verify that the problem is within its jurisdiction.

**Vouchers**

To enter SOLVIT, the national shall communicate:

- full contact details;
- Detailed description of the problem;
- all evidence of the folder (for example, correspondence received and the decisions of the administrative authority concerned).

**Time limit**

SOLVIT is committed to finding a solution within ten weeks from the date of the management of the file by the SOLVIT center of the country where the problem occurred.

**Cost**

Free.

**Following the procedure**

At the end of ten weeks, the SOLVIT has a solution:

- If this solution resolves the dispute on the application of European law, the solution is accepted and the file is closed;
- if there is no solution, the case is closed as unresolved and forwarded to the European Commission.

**Additional Information**

SOLVIT France: General Secretariat for European Affairs, 68 Rue de Bellechasse, 75700 Paris ([official site](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

