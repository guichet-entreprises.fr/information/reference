﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP235" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="Chief Pyrotechnician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="chief-pyrotechnician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/chief-pyrotechnician.html" -->
<!-- var(last-update)="2020-04-15 17:20:37" -->
<!-- var(url-name)="chief-pyrotechnician" -->
<!-- var(translation)="Auto" -->


Chief Pyrotechnician
====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:37<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The pyrotechnician or artificer is a professional whose activity consists of staging and performing pyrotechnic shows (fireworks) composed of different explosives.

Fireworks are classified according to their type of use, the level of risk and the level of noise produced during their use:

- Categories 1 to 4 represent entertainment items ranked according to the size of the hazard represented and the noise level produced when they are used;
- categories T1 and T2 relate to pyrotechnics intended for staging, classified according to the nature of the hazard presented;
- categories P1 and P2 refers to pyrotechnics other than entertainment and theatre items and also classified according to their dangerousness.

*For further information*: Article 13 of Decree No. 2010-455 of 4 May 2010 on the marketing and control of explosive products.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of pyrotechnician, the professional must:

- Be certified when using Category 4, T2 and P2 devices;
- when he wishes to perform a pyrotechnic show, he must first make a declaration to the town hall where he wishes to perform it (see infra "Pre-declaration for a pyrotechnic show").

*For further information*: Article 28 of the decree of 4 May 2010 above.

#### Training

**Qualification certification**

In order to obtain the Certificate of Qualification, the professional must complete training with an accredited body, with a minimum duration of two days for Level 1 and three days for Level 2. The content of this training is determined by the approved body within its specifications.

The first level allows the professional to carry out the assembly, shooting and cleaning operations of the area with category 4 or T2 pyrotechnics except the artifices:

- nautical water;
- with an active ingredient of no more than 500 g per product;
- whose diameter of the launch mortar is less than 50 mm (in case of air chestnuts) or less than 150 mm for other pyrotechnics fired by a mortar;
- with aperture angles of less than 30 degrees.

The Level 2 certificate allows the professional to carry out the assembly, shooting and cleaning operations of the shooting area with all categories of pyrotechnics.

At the end of his training and after obtaining a certificate of completion of the internship, the professional is the subject of an evaluation of his knowledge and aptitude, and if necessary, receives a certificate of success in the internship.

**Competent authority**

The professional must then apply for a certificate of qualification to the prefecture of the home department.

**Supporting documents**

This request must include:

- a certificate of completion of the internship and a certificate of success in the assessment of knowledge, both dating back less than five years and corresponding to the level requested;
- when the professional wishes to obtain Level 1, proof that he or she has participated in the editing or shooting of three shows in the past five years involving Category 4, K4 (accreditation) or T2 pyrotechnics and supervised by a artificer holding the certificate of qualification;
- when the professional wishes to obtain Level 2, his Level 1 certificate of qualification dating back at least one year, and proof that he has participated in the assembly or shooting of three pyrotechnic shows in the last two years. These shows must meet the same conditions as those mentioned above.

**Procedure**

Upon receipt of the full application, the prefect issues the certificate of qualification which includes information relating to the identity of the professional and his level of qualification. The Level 1 certificate is valid for five years, and the Level 2 certificate is valid for two years.

**Please note**

The application for renewal of the certificate must be made by the professional before its expiry date.

*For further information*: Articles 28 to 39 of the May 31, 2010 order taken under Articles 3, 4 and 6 of Decree No. 2010-580 of May 31, 2010 relating to the acquisition, possession and use of entertainment devices and pyrotechnics for theatre.

#### Pre-declaration for a pyrotechnic show

**Competent authority**

The professional must apply, at least one month before the scheduled date of the shooting, to the town hall of the commune or to the prefecture of the department where he wishes to perform the show.

**Supporting documents**

His application must include the[Form Cerfa 14098*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14098.do) completed and signed and the following documents:

- Photocopying of the valid certificate of qualification in the case of the use of Category 4 fireworks;
- an implementation scheme that includes a plan of the firing range, the security perimeter, water points that can be used in the event of a fire and emergency reception points;
- All the measures designed to limit the risks to the public and the neighbourhood;
- In the case of the use of fireworks to be launched with a mortar (category 1 and 2), the copy of the valid prefectural accreditation;
- The list of products implemented during the show;
- The civil liability certificate covering him against the risks associated with carrying out this activity;
- when it temporarily stores its equipment before the show, the presentation of the storage conditions of the products (total mass of active ingredient, the description of the installation and its environment, as well as the distances of isolation).

**Procedure**

Upon receipt of the request, the mayor and the prefect each issue a receipt of the file.

**Please note**

On the day of the show, the organizer of the show must send them a list of the people who will be required to handle the pyrotechnics, this list should mention:

- Their identity and date of birth
- their level of certification as well as the receipt number of the show's declaration form.

In addition, at the end of the show, its organizer must give this list to the prefecture of the department of the shooting place.

**Cost**

Free.

*For further information*: Articles 20 to 22 of the order of 31 May 2010 above.

#### Costs associated with qualification

It is advisable to get closer to the relevant agencies for more information.

3°.Professional rules
---------------------------------

**Conditions for storing fireworks**

The professional storing pyrotechnic devices must comply with all provisions relating to stored equipment, as well as the site and storage room.

The temporary possession of entertainment devices or pyrotechnics is permitted for up to fifteen days before the date of the show.

For example, the devices must be stored within their original packaging, in an enclosed room, not accessible to the public and under the supervision of a guard or an electronic monitoring system. The presence of such products must be indicated within this premises. In addition, no public dwelling or establishment should be within 50m of this premises or even a high-rise building within 100m.

This storage must be placed under the control of a person designated by the organizer. The town hall of the venue where the show is to take place also verifies the compliance of this storage with the rules of safety and security.

*For further information*: Articles 3 to 18 of the order of 31 May 2010 above.

**Safety and security obligations**

The professional must ensure compliance with the safety and security rules of the pyrotechnic show and must include:

- Ensure that the firing range is demarcated, not accessible to the public and has firefighting resources;
- provide an emergency reception point within the firing range materialized by a sign marked "emergency reception point";
- Clean up the shooting area at the end of the show and collect all the waste.

The town hall of the town of the shooting place must ensure that all of these requirements are met.

*For further information*: Articles 23 to 27 of the order of 31 May 2010 as a result.

4°. Insurance and criminal sanctions
--------------------------------------------------------

**Insurance**

The pyrotechnician, as a professional, must take out a certificate of liability insurance for the risks incurred during his professional activity.

On the other hand, when he is an employee, it is up to the employer to take out this insurance for his employees.

**Criminal sanctions**

In case of breach of the obligations of safety, security and storage, the professional incurs a fine of 750 euros as well as a possible additional penalty of confiscation of the artifices.

In addition, a professional who:

- organizes a pyrotechnic show without first making a declaration to the town hall;
- does not hold a certificate of qualification.

*For further information*: Articles 8 to 10 of the May 31, 2010 decree.

