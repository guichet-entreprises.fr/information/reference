﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP235" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artisanat" -->
<!-- var(title)="Pyrotechnicien en chef" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artisanat" -->
<!-- var(title-short)="pyrotechnicien-en-chef" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/artisanat/pyrotechnicien-en-chef.html" -->
<!-- var(last-update)="2020-04-30 11:23:49" -->
<!-- var(url-name)="pyrotechnicien-en-chef" -->
<!-- var(translation)="None" -->

# Pyrotechnicien en chef

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30 11:23:49<!-- end-var -->

## 1°. Définition de l’activité

Le pyrotechnicien ou artificier est un professionnel dont l'activité consiste à mettre en scène et réaliser des spectacles pyrotechniques (feux d'artifice) composés de différents explosifs.

Les artifices font l'objet d'une classification selon leur type d'utilisation, le niveau de risque et le niveau sonore produit lors de leur utilisation :

- les catégories 1 à 4 représentent les articles de divertissement classés selon l'importance du danger représenté et du niveau sonore produit lors de leur utilisation ;
- les catégories T1 et T2 est relative aux articles pyrotechniques destinés à être mis en scène, classés selon la nature du danger présenté ;
- les catégories P1 et P2 concerne les articles pyrotechniques autres que les articles de divertissement et ceux destinés au théâtre et classés également selon leur dangerosité.

*Pour aller plus loin* : article 13 du décret n° 2010-455 du 4 mai 2010 relatif à la mise sur le marché et au contrôle des produits explosifs.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de pyrotechnicien, le professionnel doit :

- être titulaire d'une certification dès lors qu'il utilise des artifices de catégorie 4, T2 et P2 ;
- lorsqu'il souhaite effectuer un spectacle pyrotechnique, il doit au préalable effectuer une déclaration auprès de la mairie où il souhaite l'effectuer (cf. infra « Déclaration préalable en vue d'effectuer un spectacle pyrotechnique »).

*Pour aller plus loin* : article 28 du décret du 4 mai 2010 précité.

#### Formation

##### Certification de qualification

En vue d'obtenir le certificat de qualification, le professionnel doit effectuer une formation auprès d'un organisme agréé, d'une durée minimale de deux jours pour le niveau 1 et de trois jours pour le niveau 2. Le contenu de cette formation est fixé par l'organisme agréé au sein de son cahier des charges.

Le premier niveau permet au professionnel de réaliser les opérations de montage, de tir et de nettoyage de la zone avec les articles pyrotechniques de catégorie 4 ou T2 sauf les artifices :

- nautiques ;
- présentant une quantité de matière active ne dépassant pas 500 g par produit ;
- dont le diamètre du mortier de lancement est inférieur à 50 mm (en cas de marrons d'air) ou inférieur à 150 mm pour les autres articles pyrotechniques tirés par un mortier ;
- dont les angles d'ouverture sont inférieurs à 30 degrés.

Le certificat de niveau 2 permet au professionnel de réaliser les opérations de montage, de tir et de nettoyage de la zone de tir avec toutes les catégories d'articles pyrotechniques.

À l'issue de sa formation et après avoir obtenu une attestation de fin de stage, le professionnel fait l'objet d'une évaluation de ses connaissances et de son aptitude, et le cas échéant, reçoit une attestation de réussite au stage.

###### Autorité compétente

Le professionnel doit ensuite adresser à la préfecture du département de son domicile une demande de certificat de qualification.

###### Pièces justificatives

Cette demande doit comporter :

- une attestation de fin de stage et une attestation de réussite à l'évaluation des connaissances, datant toutes les deux, de moins de cinq ans et correspondant au niveau sollicité ;
- lorsque le professionnel souhaite obtenir le niveau 1, la preuve qu'il a participé au montage ou au tir de trois spectacles au cours des cinq dernières années comportant des articles pyrotechniques de catégorie 4, K4 (nécessitant un agrément) ou T2 et encadré par un artificier titulaire du certificat de qualification ;
- lorsque le professionnel souhaite obtenir le niveau 2, son certificat de qualification de niveau 1 datant d'au moins un an, et la preuve qu'il a participé au montage ou au tir de trois spectacles pyrotechniques durant les deux dernières années. Ces spectacles doivent remplir les mêmes conditions que ceux cités ci-dessus.

###### Procédure

Dès réception de la demande complète, le préfet délivre le certificat de qualification qui comporte les informations relatives à l'identité du professionnel et son niveau de qualification. Le certificat de niveau 1 est valable cinq ans, et le certificat de niveau 2 est valable deux ans.

**À noter**

La demande de renouvellement du certificat doit être adressée par le professionnel avant sa date d'expiration.

*Pour aller plus loin* : articles 28 à 39 de l'arrêté du 31 mai 2010 pris en application des articles 3, 4 et 6 du décret n° 2010-580 du 31 mai 2010 relatif à l'acquisition, la détention et l'utilisation des artifices de divertissement et des articles pyrotechniques destinés au théâtre.

#### Déclaration préalable en vue d'effectuer un spectacle pyrotechnique

##### Autorité compétente

Le professionnel doit adresser sa demande, au moins un mois avant le date prévue du tir, à la mairie de la commune ou à la préfecture du département où il souhaite réaliser le spectacle.

##### Pièces justificatives

Sa demande doit comporter le [formulaire Cerfa 14098](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14323) de déclaration préalable complété et signé ainsi que les documents suivants :

- la photocopie du certificat de qualification en cours de validité en cas d'utilisation d'artifices de catégorie 4 ;
- un schéma de mise en œuvre comportant un plan de la zone de tir, le périmètre de sécurité, les points d'eau utilisables en cas d'incendie et les points d'accueil de secours ;
- l'ensemble des dispositifs prévus pour limiter les risques pour le public et le voisinage ;
- en cas d'utilisation d'artifices destinés à être lancés avec un mortier (catégorie 1 et 2), la copie de l'agrément préfectoral en cours de validité ;
- la liste des produits mis en œuvre lors du spectacle ;
- l'attestation de responsabilité civile le couvrant contre les risques liés à l'exercice de cette activité ;
- lorsqu'il stocke momentanément son matériel avant le spectacle, la présentation des conditions de stockage des produits (masse totale de matière active, la description de l'installation et de son environnement, ainsi que les distances d'isolement).

##### Procédure

Dès réception de la demande, le maire et le préfet délivrent chacun un récépissé du dossier.

**À noter**

Le jour du spectacle, l'organisateur du spectacle doit leur adresser la liste des personnes qui seront amenées à manipuler les articles pyrotechniques, cette liste doit mentionner :

- leur identité et date de naissance ;
- leur niveau de certification ainsi que le numéro de récépissé du formulaire de déclaration du spectacle.

De plus, à l'issue du spectacle, son organisateur doit remettre cette liste à la préfecture du département du lieu de tir.

##### Coût

Gratuit.

*Pour aller plus loin* : articles 20 à 22 de l'arrêté du 31 mai 2010 précité.

#### Coûts associés à la qualification

Il est conseillé de se rapprocher des organismes concernés pour de plus amples informations.

## 3°. Règles professionnelles

### Conditions relatives au stockage des artifices

Le professionnel stockant des artifices pyrotechniques doit respecter l'ensemble des dispositions relatives au matériel stocké, ainsi qu'au site et au local de stockage.

La détention momentanée des artifices de divertissement ou des articles pyrotechniques est autorisée pendant une durée maximale de quinze jours avant la date du spectacle.

Ainsi, les artifices doivent notamment être stockés au sein de leur emballage d'origine, dans un local clos, non accessible au public et sous la surveillance d'un gardien ou un système de surveillance électronique. La présence de tels produits doit être indiquée au sein de ce local. En outre, aucune habitation ou établissement recevant du public, ne doit se situer à moins de 50 m de ce local ni même un immeuble de grande hauteur à moins de 100m.

Ce stockage doit être placé sous le contrôle d'une personne désignée par l'organisateur. La mairie du lieu où doit se dérouler le spectacle vérifie également la conformité de ce stockage aux règles de sécurité et de sûreté.

*Pour aller plus loin* : articles 3 à 18 de l'arrêté du 31 mai 2010 précité.

### Obligations de sécurité et de sûreté

Le professionnel doit veiller au respect des règles de sécurité et de sûreté du spectacle pyrotechniques et doit notamment :

- s'assurer que la zone de tir est délimitée, non accessible au public et dispose de moyens de lutte contre les incendies ;
- prévoir un point d'accueil de secours au sein de la zone de tir matérialisé par une affiche portant la mention « point d'accueil des secours » ;
- nettoyer la zone de tir à l'issue du spectacle et collecter l'ensemble des déchets.

La mairie de la commune du lieu de tir doit s'assurer du respect de l'ensemble de ces exigences.

*Pour aller plus loin* : articles 23 à 27 de l'arrêté du 31 mai 2010 susvisé.

## 4°. Assurance et sanctions pénales

### Assurance

Le pyrotechnicien, en qualité de professionnel, doit souscrire une attestation d'assurance de responsabilité civile pour les risques encourus au cours de son activité professionnelle.

En revanche, lorsqu'il est salarié, c'est à l'employeur de souscrire cette assurance pour ses employés.

### Sanctions pénales

En cas de manquement aux obligations de sécurité, de sûreté et de stockage, le professionnel encourt une amende de 750 euros ainsi qu'une éventuelle peine complémentaire de confiscation des artifices.

De plus, encourt une amende de 1 500 euros le professionnel qui :

- organise un spectacle pyrotechnique sans avoir au préalable effectué une déclaration auprès de la mairie ;
- n'est pas titulaire d'un certificat de qualification.

*Pour aller plus loin* : articles 8 à 10 du décret du 31 mai 2010.