﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP041" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Social" -->
<!-- var(title)="Assistant de service social" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="social" -->
<!-- var(title-short)="assistant-de-service-social" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/social/assistant-de-service-social.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="assistant-de-service-social" -->
<!-- var(translation)="None" -->

# Assistant de service social

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

L'assistant de service social (ASS) apporte son soutien aux personnes, familles ou groupes en difficulté en les aidant à s'insérer socialement ou professionnellement.

Travaillant en collaboration avec des médecins, des éducateurs spécialisées et des services de mairies, il les accompagne et les informe sur leurs droits, leur indique les démarches à suivre en matière de logement, de scolarité des enfants ou de santé.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'accès à la profession d'ASS est réservé au titulaire du diplôme d'assistant de service social (DEASS). Une fois en possession de ce titre, le professionnel devra le faire enregistrer sur le [répertoire Adeli](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33005) (« Automatisation Des Listes »).

*Pour aller plus loin* : articles L. 411-1 et L. 411-2 du Code de l'action sociale et des familles.

#### Formation

Les candidats au DEASS doivent être au préalable, titulaires d'un diplôme au moins de niveau IV (baccalauréat, brevet de technicien ou brevet professionnel).

Dès lors qu'ils justifient d'un de ces titres, ils seront soumis à un concours leur permettant d'intégrer la formation dans un établissement formateur. Le candidat devra passer avec succès une épreuve écrite d'admissibilité puis deux épreuves d'admission.

Une fois qu'il aura intégré la formation, l'intéressé suivra un enseignement théorique de 1 740 heures et douze mois de stage dont 110 heures seront consacrées aux relations entre les établissements de formation et les sites qualifiants.

**À savoir**

Le DEASS peut également être obtenu par le système de validation des acquis de l'expérience ([VAE](https://www.pole-emploi.fr/file/mmlelement/pj/bb/7f/de/a3/vae_ministere_affaires_sociales54618.pdf)).

*Pour aller plus loin* : articles D. 451-29 et suivants du Code de l'action sociale et des familles ; arrêté du 29 juin 2004 relatif au diplôme d’État d'assistant de service social.

#### Coûts associés à la qualification

La formation menant à la délivrance du DEASS est payante. Pour plus d'informations, il est conseillé de se rapprocher des établissements la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE), exerçant légalement l’activité d'ASS dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra faire une demande préalable d'activité auprès du ministre chargé des affaires sociales (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

**À savoir**

L'exercice d'ASS, à titre temporaire ou occasionnel, requiert que le ressortissant possède toutes les connaissances linguistiques nécessaires.

*Pour aller plus loin* : article L. 411-1-1 du Code de l'action sociale et des familles.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente :

- s'il est titulaire d'un titre de formation délivré par une autorité compétente d'un autre État membre, qui réglemente l'accès à la profession ou son exercice ;
- s'il est titulaire d'un titre de formation délivré par un État tiers mais reconnu par un État de l'UE ou de l'EEE ;
- s'il a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un État de l'UE ou de l'EEE qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une attestation de capacité auprès du ministre chargé des affaires sociales (cf. infra « 5°. b. Obtenir une attestation de capacité pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE) »).

Si, lors de l'examen de la demande, le ministre chargé des affaires sociales constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées en France, des mesures de compensation pourront être prises (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

**À savoir**

L'exercice d'ASS, à titre permanent, requiert que le ressortissant possède toutes les connaissances linguistiques nécessaires.

*Pour aller plus loin* : article L. 411-1 du Code de l'action sociale et des familles.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

L'ASS est tenu au secret professionnel dans le cadre de ses missions. En cas de violation de cette obligation, il pourra être poursuivi et puni d'une peine d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : articles L. 411-3 du Code de l'action sociale et des familles et L 226-13 du Code pénal.

## 4°. Enregistrement et assurance

### a. Enregistrement au répertoire Adeli

L'ASS est tenu de faire enregistrer son diplôme au répertoire Adeli (« Automatisation Des Listes »).

#### Autorité compétente

L’enregistrement au répertoire Adeli se fait auprès de l’agence régionale de santé (ARS) du lieu d’exercice.

#### Délai

La demande d’enregistrement est présentée dans le mois suivant la prise de fonction de l'ASS, quel que soit le mode d’exercice (libéral, salarié, mixte).

#### Pièces justificatives

À l'appui de sa demande d'enregistrement, l'ASS doit fournir un dossier comportant :

- le diplôme original ou titre attestant de la formation en psychologie délivré par l'État de l'UE ou de l'EEE (traduit en français par un traducteur agréé, le cas échéant) ;
- une pièce d’identité ;
- le formulaire [Cerfa 12269*02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33005) complété, daté et signé.

#### Issue de la procédure

Le numéro Adeli du ressortissant sera directement mentionné sur le récépissé du dossier, délivré par l'ARS.

#### Coût

Gratuit.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

En cas d’exercice libéral, l'ASS a l’obligation de souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. Dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le ministre chargé des affaires sociales est compétent pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

Le ressortissant devra transmettre par tout moyen un dossier comprenant les pièces justificatives suivantes :

- le formulaire de l'annexe V de l'arrêté du 31 mars 2009 ;
- une pièce d'identité en cours de validité ;
- tout document justifiant ses qualifications professionnelles ;
- tout document justifiant une assurance de responsabilité civile professionnelle.

#### Procédure

Une fois que le ministre chargé des affaires sociales aura récupéré les pièces justificatives, il se prononcera sur la demande de déclaration et enregistrera le ressortissant sur la liste des ASS auprès du répertoire Adeli.

*Pour aller plus loin* : articles L. 411-1-1, R. 411-7 et R. 411-8 du Code de l'action sociale et des familles.

### b. Obtenir une attestation de capacité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ministre chargé des affaires sociales est compétent pour délivrer l'attestation de capacité à exercer pour le ressortissant qui souhaite s'établir en France.

#### Pièces justificatives

Le ressortissant transmettra, par courrier en deux exemplaires, un dossier à la direction régionale d'un centre d'examen interrégional délivrant DEASS, comprenant les pièces justificatives suivantes :

- une copie de la pièce d'identité en cours de validité ;
- une copie du titre de formation obtenu et sa traduction en français ;
- le cas échéant, une attestation justifiant qu'il a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un État de l'UE ou de l'EEE qui ne réglemente ni la formation, ni l'exercice de la profession ;
- un document décrivant la formation suivie et le contenu des études et des stages effectués ;
- un descriptif des principales caractéristiques du titre de formation en annexe I de l'arrêté du 31 mars 2009 ;
- un curriculum vitae manuscrit ;
- un courrier du ressortissant par lequel il désigne un établissement de formation qui émettra un avis technique sur la comparaison de la formation et des compétences attestées par le DEASS et du contenu de la formation suivie par l'intéressé.

Le directeur régional accusera réception du dossier dans un délai d'un mois et se prononcera sur la nécessité d'avoir recours ou non à une mesure de compensation dans les quatre mois suivant la réception du dossier.

Si l'avis technique est favorable, le directeur régional adressera au ressortissant un récépissé de complétude de sa demande d'attestation de capacité à exercer.

#### Bon à savoir : mesures de compensation

En cas de différences substantielles entre la formation et l'exercice de la profession par le ressortissant et ce qui est exigé en France, l'autorité compétente pourra le soumettre à une mesure de compensation qui peut être une épreuve d'aptitude ou un stage d'adaptation.

#### Épreuve d'aptitude

Elle comporte des interrogations écrites et orales ainsi que des exercices pratiques, portant sur les connaissances essentielles de l'exercice de la profession, notamment en matière de politiques sociales, et sur la réglementation relative à l'accès au droit.

#### Stage d'adaptation

Il se déroulera sous la responsabilité d'un professionnel qualifié.

*Pour aller plus loin* : articles R. 411-3 et suivants du Code de l'action sociale et des familles ; arrêté du 31 mars 2009 relatif aux conditions d'accès à la profession d'assistant de service social pour les titulaires de diplômes étrangers.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).