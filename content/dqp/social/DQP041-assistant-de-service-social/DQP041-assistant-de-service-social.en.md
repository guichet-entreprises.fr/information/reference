﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP041" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Social sector" -->
<!-- var(title)="Social worker" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="social-sector" -->
<!-- var(title-short)="social-worker" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/social-sector/social-worker.html" -->
<!-- var(last-update)="2020-04-15 17:22:30" -->
<!-- var(url-name)="social-worker" -->
<!-- var(translation)="Auto" -->


Social worker
========================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:30<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Social Service Assistant (SSA) supports individuals, families or groups in difficulty by helping them to integrate socially or professionally.

Working in collaboration with doctors, specialized educators and town hall services, he accompanies them and informs them about their rights, tells them what to do about housing, children's education or health.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Access to the SSA profession is reserved for the holder of the Social Service Assistant Diploma (DEASS). Once in possession of this title, the professional will have to register it on the[Adeli directory](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33005) ("Automation of Lists").

*To go further* Articles L. 411-1 and L. 411-2 of the Code of Social Action and Families.

#### Training

Candidates for THE DEASS must be pre-qualified with a degree at least level IV (bachelor's degree, technician's certificate or professional certificate).

As soon as they justify one of these titles, they will be subject to a competition allowing them to integrate the training into a training institution. The candidate will have to pass a written eligibility test and then two entrance exams.

Once he has integrated the training, he will follow a theoretical teaching of 1,740 hours and twelve months of internship, of which 110 hours will be devoted to the relations between training institutions and qualifying sites.

**What to know**

The DEASS can also be obtained through the experience validation system ([Vae](https://www.pole-emploi.fr/file/mmlelement/pj/bb/7f/de/a3/vae_ministere_affaires_sociales54618.pdf)).

*To go further* Articles D. 451-29 and the following articles of the Code of Social Action and Families; order of 29 June 2004 relating to the State Diploma of Social Service Assistant.

#### Costs associated with qualification

Training leading to the issuance of DEASS is paid for. For more information, it is advisable to get closer to the dispensing establishments.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

A national of a European Union (EU) or European Economic Area (EEA) state, legally operating as an SSA in one of these states, may use his or her professional title in France on a temporary or casual basis.

He will have to make a prior request for activity with the Minister responsible for social affairs (see infra "5°. a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)").

**What to know**

The exercise of SSA, on a temporary or casual basis, requires that the national possess all the necessary language skills.

*To go further* Article L. 411-1-1 of the Code of Social Action and Families.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state may settle in France to practice permanently:

- if he holds a training certificate issued by a competent authority in another Member State, which regulates access to the profession or its exercise;
- Whether it holds a training certificate issued by a third state but recognised by an EU or EEA state;
- whether he has worked full-time or part-time for one year in the last ten years in an EU or EEA state that does not regulate training or practising.

Once the national fulfils one of these conditions, he or she will be able to apply for a certificate of capacity from the Minister responsible for social affairs (see below "5°). b. Obtain a certificate of capacity for the EU or EEA national for a permanent exercise (LE)).

If, during the examination of the application, the Minister responsible for social affairs finds that there are substantial differences between the training and professional experience of the national and those required in France, compensation measures can be taken (see infra "5°. b. Good to know: compensation measures").

**What to know**

The exercise of SSA, on a permanent basis, requires that the national possess all the necessary language knowledge.

*To go further* Article L. 411-1 of the Code of Social Action and Families.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The SSA is bound by professional secrecy as part of its missions. If he violates this obligation, he may be prosecuted and punished with a one-year prison sentence and a fine of 15,000 euros.

*To go further* Articles L. 411-3 of the Code of Social Action and Families and L 226-13 of the Penal Code.

4°. Registration and insurance
--------------------------------------------------

### a. Registration with the Adeli directory

The SSA is required to register its diploma with the Adeli directory ("Automation of Lists").

**Competent authority**

Registration in the Adeli directory is done with the regional health agency (ARS) of the place of practice.

**Timeframe**

The application for registration is submitted within one month of taking office of the SSA, regardless of the mode of practice (liberal, salaried, mixed).

**Supporting documents**

In support of its application for registration, the SSA must provide a file containing:

- the original diploma or title attesting to the training in psychology issued by the EU State or the EEA (translated into French by a certified translator, if applicable);
- ID
- The form[Cerfa 12269*02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33005) completed, dated and signed.

**Outcome of the procedure**

The national's Adeli number will be directly mentioned on the receipt of the file, issued by the ARS.

**Cost**

Free.

### b. Obligation to take out professional liability insurance

In the event of a liberal exercise, the SSA is required to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during this activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)

**Competent authority**

The Minister responsible for social affairs is responsible for issuing the pre-declaration of activity.

**Supporting documents**

The national will be required to submit by any means a file containing the following supporting documents:

- Appendix V form of the[decree of March 31, 2009](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000020506757) ;
- A valid piece of identification
- any document justifying his professional qualifications;
- any documents justifying professional liability insurance.

**Procedure**

Once the Minister responsible for social affairs has recovered the supporting documents, he will decide on the request for declaration and register the national on the list of SSA with the Adeli directory.

*To go further* Articles L. 411-1-1, R. 411-7 and R. 411-8 of the Code of Social Action and Families.

### b. Obtain a certificate of capacity for the EU or EEA national for a permanent exercise (LE)

**Competent authority**

The Minister responsible for social affairs is responsible for issuing the certificate of ability to practise for the national who wishes to settle in France.

**Supporting documents**

The national will send, by mail in two copies, a file to the regional management of an inter-regional examination centre delivering DEASS, including the following supporting documents:

- A copy of the valid ID
- A copy of the training title obtained and its translation into French;
- if necessary, a certificate justifying that he has worked full-time or part-time for one year in the last ten years in an EU or EEA state that does not regulate training or the practice of the profession;
- A document outlining the training and the content of the studies and internships carried out;
- A description of the main features of the Schedule I training title of the March 31, 2009 decree;
- A handwritten resume
- a mail from the national in which he designates a training institution that will issue a technical opinion on the comparison of the training and skills attested by the DEASS and the content of the training followed by the person concerned.

The Regional Director will acknowledge receipt of the file within one month and decide whether or not to use a compensation measure within four months of receipt of the file.

If the technical opinion is favourable, the regional director will send the national a receipt of completeness of his application for certification of ability to practise.

**Good to know: compensation measures**

In the event of substantial differences between the national's training and the practice of the profession and what is required in France, the competent authority may subject him to a compensation measure which may be an aptitude test or an internship. adaptation.

**Aptitude test**

It includes written and oral questions as well as practical exercises, dealing with essential knowledge of the practice of the profession, including social policy, and regulations on access to the law.

**Adaptation course**

It will take place under the responsibility of a qualified professional.

*To go further* Articles R. 411-3 and the following articles of the Code of Social Action and Families;[decree of March 31, 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020506757&dateTexte=20180112) conditions of access to the profession of social service assistant for holders of foreign diplomas.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

