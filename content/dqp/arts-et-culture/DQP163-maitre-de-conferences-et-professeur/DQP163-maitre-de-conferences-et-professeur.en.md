﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP163" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts and culture" -->
<!-- var(title)="Lecturer and university professor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-and-culture" -->
<!-- var(title-short)="lecturer-and-university-professor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/arts-and-culture/lecturer-and-university-professor.html" -->
<!-- var(last-update)="2020-04-15 17:20:42" -->
<!-- var(url-name)="lecturer-and-university-professor" -->
<!-- var(translation)="Auto" -->


Lecturer and university professor
=================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:42<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

University lecturers and professors are professor-researchers whose activity is to pass on to students knowledge in one or more subjects given during their initial and continuing training.

During this activity, they are responsible for developing and organizing their teachings and providing advice and guidance to students.

*For further information*: Article 3 of Decree 84-431 of 6 June 1984 setting out the common statutory provisions applicable to teacher-researchers and bearing special status of the body of university professors and the body of lecturers.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The functions of lecturer and professor of universities are available to candidates:

- Professionally qualified;
- on a qualification list drawn up by the National Council of Universities (UNC).

**Please note**

Professionals who have been teaching and studying for less than 18 months in a higher education institution in a European Union (EU) member state or who are a party to the European Economic Area (EEA) agreement ) other than France, are exempt from being included on the qualification list for the duties of lecturer.

*For further information*: decree of 6 June 1984.

#### Training

**Registration on a qualifying list**

**Lecturer duties**

In order to be included on the UNC's qualification list for lecturer duties, the professional must either:

- have a PhD or an authorization to lead research. The State Doctorate, the Postgraduate Doctorate and the Doctor-Engineer Diploma are admitted in equivalence;
- justify at least three years of professional experience, acquired over the past six years;
- Be a full-time associate teacher
- Be detached from the body of lecturers;
- belong to a body of researchers.

Once they meet these conditions, the person must make an online declaration of application on the portal[Galaxy](https://www.galaxie.enseignementsup-recherche.gouv.fr/) higher education staff.

*For further information*: Article 23 of the decree of 6 June 1984 mentioned above; decree of 5 July 2017 relating to the procedure for registering on the qualification lists for the functions of lecturer or professor of universities.

**University professorships**

In order to be included in the qualification list for the positions of professor of universities, the professional must either:

- be empowered to conduct research. However, the holder of a university degree, a qualification or a equivalent level of qualification may be exempted from this requirement by the UNC;
- justify having carried out an effective professional activity of at least five years on the first of January of its registration year, in the last eight years. However, teacher, research and incidental activities in public institutions of a scientific and technological nature are not taken into account;
- Be a full-time associate teacher
- Be detached from the body of university professors;
- belong to a body of researchers assimilated to university professors.

As for lecturers, when the candidate meets these requirements, he must make an online application statement on the Galaxy portal, higher education staff.

**Good to know**

Offers for positions as a lecturer and professor of universities are published on the Galaxy portal:

- in a common calendar, offered each year on the site;
- over the water, by the institutions that publish the offers themselves as well as the timing of the application process specific to each position.

*For further information*: Article 43 and 44 of the decree of 6 June 1984.

**Recruitment competition**

The recruitment of lecturers and professors from universities is carried out, through various competitions organised by the institutions, the nature of which depends on the qualifications of the candidate.

As long as the applicant holds one of the qualifications required to attend at least one of these competitions, he must apply to the director of the institution concerned, in order to fill the vacancy (see infra "5°. a. Application for the recruitment competition for the positions of lecturer and university professor").

*For further information*: Articles 26 and 46 of the decree of 6 June 1984.

**Appointment**

**Lecturers' Provisions**

The admitted lecturers are appointed as interns for one year and must carry out training during this period in order to deepen their teaching skills necessary to carry out their duties.

In addition, within five years of tenure, the professional may apply for additional training in order to deepen his teaching skills. If necessary, it will benefit from a discharge of teaching activity.

*For further information*: Articles 32 and 32-1 of the decree of 6 June 1984 mentioned above; decree of 8 February 2018 setting out the national framework for training aimed at deepening the pedagogical skills of trainee lecturers.

**University Professors' Provisions**

University professors are appointed by decree of the President of the Republic.

In addition, candidates admitted to recruitment competitions (lecturers and university professors) must commit to the corresponding job in order to be appointed.

*For further information*: Articles 50 and 58-5 of the decree of 6 June 1984 above.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

There is no provision for the national of an EU or EEA state to work on a temporary and casual basis as a lecturer and professor of universities in France.

As such, the national is subject to the same requirements as the French national (see infra "4°. Qualification recognition procedures and formalities").

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, legally established and practising as a lecturer or university professor, may carry out the same activity in France.

For this, the national can apply to the various recruitment competitions under the same conditions as the French national (see above "4o. a. Recruitment competition").

*For further information*: Articles 27 and 42 of the decree of 6 June 1984 mentioned above.

3°. Conditions of honorability, ethical rules
--------------------------------------------------------

**Activity report**

Every five years, the lecturer or university professor must provide the president or director of the institution with a report outlining all of his activities and their possible changes. The director of the school must then submit this report to the UNC.

*For further information*: Article 7-1 of the decree of 6 June 1984.

**Cumulative activity**

The professional is bound by the provisions of state officials and, as such, may be allowed to combine an incidental activity with his main activity. If so, it must not interfere with the normal functioning, independence or neutrality of the public service.

*For further information*: Decree No. 2017-105 of January 27, 2017 relating to the exercise of private activities by public officials and certain contract officials under private law who have ceased their duties, the accumulation of activities and the ethics commission of the civil service.

**Advancement**

Lecturers can benefit from the step-up process because of their seniority. This progress is pronounced by order of the president or director of the establishment according to the provisions of Articles 36 to 40-1-1 of the decree of June 6, 1984 aforementioned.

In addition, university professors may also benefit from step or class advancement under the conditions set out in Articles 52 to 57 of the decree.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Application for recruitment competitions for the position of lecturer or university professor

**Competent authority**

The applicant must submit his application in three copies, intended for the president or director of the institution in which the job is vacant and to its rapporteurs. This request must be made on paper or electronically, within at least thirty days of the date of the opening of the application registers.

**Supporting documents**

Its application file must include the following documents, if any, with their approved translation into French:

- a printed application statement from the website[Galaxy](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_doc_dossier_qualif.htm) Superior's staff;
- A photocopy of his photo ID with a photograph;
- a document attesting to his professional qualifications (see supra "2. a. National legislation");
- A resume of work, books, articles and achievements;
- a copy of the report of the diploma's defence produced.

**Outcome of the procedure**

At the end of the procedure, the list of recruited candidates is published on the portal[Galaxy](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_recrutement.htm) Ministry of Higher Education, Research and Innovation.

*For further information*: Articles 9 to 16 of the February 13, 2015 orders relating to the general terms of transfer, secondment and competition recruitment of university lecturers and professors.

### b. Remedies

**French Assistance Centre****

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

