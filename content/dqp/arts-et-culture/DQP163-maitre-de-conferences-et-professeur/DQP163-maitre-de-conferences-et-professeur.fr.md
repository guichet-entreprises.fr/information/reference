﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP163" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts et culture" -->
<!-- var(title)="Maître de conférences et professeur des universités" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-et-culture" -->
<!-- var(title-short)="maitre-de-conferences-et-professeur" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/arts-et-culture/maitre-de-conferences-et-professeur-des-universites.html" -->
<!-- var(last-update)="2020-04-30" -->
<!-- var(url-name)="maitre-de-conferences-et-professeur-des-universites" -->
<!-- var(translation)="None" -->

# Maître de conférences et professeur des universités

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30<!-- end-var -->

## 1°. Définition de l'activité

Les maîtres de conférences et les professeurs des universités sont des enseignants-chercheurs dont l'activité consiste à transmettre aux étudiants des connaissances dans une ou plusieurs matières données au cours de leur formation initiale et continue.

Au cours de cette activité, ils sont notamment chargés d'élaborer et d'organiser leurs enseignements et d'assurer le conseil et l'orientation des étudiants.

*Pour aller plus loin* : article 3 du décret n° 84-431 du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Les fonctions de maître de conférences et de professeur des universités sont accessibles aux candidats :

- qualifiés professionnellement ;
- inscrits sur une liste de qualification établie par le Conseil national des universités (CNU).

**À noter**

Les professionnels justifiant avoir exercé depuis moins de dix-huit mois une fonction d'enseignant-chercheur dans un établissement d'enseignement supérieur d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) autre que la France, sont dispensés de l'inscription sur la liste de qualification aux fonctions de maître de conférences.

*Pour aller plus loin* : décret du 6 juin 1984 susvisé.

#### Formation

##### Inscription sur une liste de qualification

###### Fonctions de maître de conférences

En vue de son inscription sur la liste de qualification pour les fonctions de maître de conférences établie par le CNU, le professionnel doit soit :

- être titulaire d'un doctorat ou d'une habilitation à diriger des recherches. Le doctorat d'État, le doctorat de troisième cycle et le diplôme de docteur-ingénieur sont admis en équivalence ;
- justifier d'une expérience professionnelle d'au moins trois ans, acquise durant les six dernières années ;
- être enseignant associé à temps plein ;
- être détaché dans le corps des maîtres de conférences ;
- appartenir à un corps de chercheurs.

Dès lors qu'il remplit ces conditions, l'intéressé doit effectuer une déclaration de candidature en ligne sur le [portail Galaxie](https://www.galaxie.enseignementsup-recherche.gouv.fr/) des personnels de l'enseignement supérieur.

*Pour aller plus loin* : article 23 du décret du 6 juin 1984 susvisé ; arrêté du 5 juillet 2017 relatif à la procédure d'inscription sur les listes de qualification aux fonctions de maître de conférences ou de professeur des universités.

###### Fonctions de professeur des universités

En vue de son inscription au sein de la liste de qualification pour les fonctions de professeur des universités, le professionnel doit soit :

- être titulaire d'une habilitation à diriger des recherches. Toutefois, le titulaire d'un diplôme universitaire, d'une qualification ou d'un titre de niveau équivalent, peut être dispensé de cette exigence par le CNU ;
- justifier avoir exercé une activité professionnelle effective d'au moins cinq ans au premier janvier de son année d'inscription, au cours des huit dernières années. Toutefois, ne sont pas prises en compte, les activités d'enseignant, de chercheur et les activités à titre accessoire, exercées dans les établissements publics à caractère scientifique et technologique ;
- être enseignant associé à temps plein ;
- être détaché dans le corps des professeurs des universités ;
- appartenir à un corps de chercheurs assimilé aux professeurs des universités.

De même que pour les maîtres de conférences, lorsque le candidat remplit ces conditions, il doit effectuer une déclaration de candidature en ligne sur le portail Galaxie, des personnels de l'enseignement supérieur.

**Bon à savoir**

Les offres de postes aux fonctions de maître de conférences et de professeur des universités sont publiées sur le portail Galaxie soit :

- dans un calendrier commun, proposé chaque année sur le site ;
- au fil de l'eau, par les établissements qui publient eux-mêmes les offres ainsi que le calendrier de la procédure de candidature propre à chaque poste.

*Pour aller plus loin* : article 43 et 44 du décret du 6 juin 1984 susvisé.

##### Concours de recrutement

Le recrutement des maîtres de conférences et des professeurs des universités est effectué, par voie de différents concours organisés par les établissements, dont la nature dépend des qualifications du candidat.

Dès lors que le candidat est titulaire de l'une des qualifications exigées en vue de se présenter à l'un au moins de ces concours, il doit adresser une demande auprès du directeur de l'établissement concerné, en vue de pourvoir au poste vacant (cf. infra « 5°. a. Dossier de candidature au concours de recrutement aux fonctions de maître de conférences et de professeur des universités »).

*Pour aller plus loin* : articles 26 et 46 du décret du 6 juin 1984 susvisé.

##### Nomination

###### Dispositions relatives aux maîtres de conférences

Les maîtres de conférences admis, sont nommés en qualité de stagiaire, pour une durée d'un an et doivent effectuer au cours de cette période, une formation en vue d'approfondir leurs compétences pédagogiques nécessaires à l'exercice de leurs fonctions.

En outre, au cours des cinq années suivant leur titularisation, le professionnel peut demander à bénéficier d'une formation complémentaire en vue d'approfondir ses compétences pédagogiques. Le cas échéant, il bénéficiera d'une décharge d'activité d'enseignement.

*Pour aller plus loin* : articles 32 et 32-1 du décret du 6 juin 1984 précité ; arrêté du 8 février 2018 fixant le cadre national de la formation visant à l'approfondissement des compétences pédagogiques des maîtres de conférences stagiaires.

###### Dispositions relatives aux professeurs des université

Les professeurs des universités sont nommés par décret du Président de la république.

En outre, les candidats admis aux concours de recrutement (des maîtres de conférence et de professeur des universités) doivent, en vue de leur nomination, s'engager à occuper l'emploi correspondant.

*Pour aller plus loin* : articles 50 et 58-5 du décret du 6 juin 1984 précité.

### b. Ressortissants UE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Aucune disposition n'est prévue pour le ressortissant d'un État de l'UE ou de l'EEE en vue d'exercer à titre temporaire et occasionnel, l'activité de maître de conférences et de professeur des universités, en France.

À ce titre, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. infra « 4°. Démarches et formalités de reconnaissance de qualification »).

### c. Ressortissants UE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, légalement établi et exerçant l'activité de maître de conférences ou de professeur des universités, peut exercer en France, la même activité.

Pour cela, le ressortissant peut se présenter aux différents concours de recrutement dans les mêmes conditions que le ressortissant français (cf. supra « 4°. a. Concours de recrutement »).

*Pour aller plus loin* : articles 27 et 42 du décret du 6 juin 1984 précité.

## 3°. Conditions d'honorabilité, règles déontologiques

### Rapport d'activité

Tous les cinq ans, l'enseignant chercheur maître de conférences ou professeur des universités, doit remettre au président ou directeur de l'établissement un rapport décrivant l'ensemble de ses activités et leurs éventuelles évolutions. Le directeur de l'établissement doit ensuite remettre ce rapport au CNU.

*Pour aller plus loin* : article 7-1 du décret du 6 juin 1984.

### Cumul d'activité

Le professionnel est tenu au respect des dispositions propres aux fonctionnaires d'État et, à ce titre, peut être autorisé à cumuler une activité accessoire avec son activité principale. Le cas échéant, celle-ci ne doit pas porter atteinte au fonctionnement normal, à l'indépendance ou à la neutralité du service public.

*Pour aller plus loin* : décret n° 2017-105 du 27 janvier 2017 relatif à l'exercice d'activités privées par des agents publics et certains agents contractuels de droit privé ayant cessé leurs fonctions, aux cumuls d'activités et à la commission de déontologie de la fonction publique.

### Avancement

Les maîtres de conférences peuvent bénéficier de la procédure d'avancement d'échelon à raison de leur ancienneté. Cet avancement est prononcé par arrêté du président ou directeur de l'établissement selon les dispositions des articles 36 à 40-1-1 du décret du 6 juin 1984 précité.

En outre, les professeurs des universités peuvent également bénéficier d'un avancement d'échelon ou de classe dans les conditions fixées aux articles 52 à 57 dudit décret.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Dossier de candidature aux concours de recrutement aux fonctions de maître de conférences ou de professeur des universités

#### Autorité compétente

Le candidat doit adresser sa demande en trois exemplaires, destinés au président ou au directeur de l'établissement dans lequel l'emploi est vacant et à ses rapporteurs. Cette demande doit être effectuée sur support papier ou par voie électronique, dans un délai d'au moins trente jours à partir de la date d'ouverture des registres des candidatures.

#### Pièces justificatives

Son dossier de candidature doit comporter les documents suivants le cas échéant, assortis de leur traduction agréée en français :

- une déclaration de candidature imprimée depuis le [site Galaxie](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_doc_dossier_qualif.htm) des personnels du supérieur ;
- une photocopie de sa pièce d'identité avec photographie ;
- une pièce attestant de ses qualifications professionnelles (cf. supra « 2°. a. Législation nationale ») ;
- un curriculum vitae mentionnant les travaux, ouvrages, articles et réalisations effectués ;
- un copie du rapport de soutenance du diplôme produit.

#### Issue de la procédure

A l'issue de la procédure, la liste des candidats recrutés est publiée sur le [portail Galaxie](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_recrutement.htm) du ministère de l'Enseignement supérieur, de la Recherche et de l'Innovation.

*Pour aller plus loin* : articles 9 à 16 des arrêtés du 13 février 2015 relatifs aux modalités générales des opérations de mutation, de détachement et de recrutement par concours des maîtres de conférences et des professeurs des universités.

### b. Voies de recours

#### Centre d'assistance Français

Le Centre ENIC-NARIC est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l'Administration nationale de chaque État membre de l'Union européenne ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L'intéressé ne peut recourir à SOLVIT que s'il établit :

- que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
- qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

##### Délai

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit 

##### Issue de la procédure

A l'issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
- s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).