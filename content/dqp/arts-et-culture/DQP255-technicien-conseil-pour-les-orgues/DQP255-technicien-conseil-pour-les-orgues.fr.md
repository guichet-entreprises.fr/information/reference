﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP255" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts et culture" -->
<!-- var(title)="Technicien-conseil pour les orgues protégés" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-et-culture" -->
<!-- var(title-short)="technicien-conseil-pour-les-orgues" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/arts-et-culture/technicien-conseil-pour-les-orgues-proteges.html" -->
<!-- var(last-update)="2020-04-15 17:20:43" -->
<!-- var(url-name)="technicien-conseil-pour-les-orgues-proteges" -->
<!-- var(translation)="None" -->

# Technicien-conseil pour les orgues protégés

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:43<!-- end-var -->

## 1°. Définition de l’activité 

Le technicien-conseil pour les orgues protégés est un professionnel qui apporte son concours au ministère chargé de la culture pour exercer des missions de service public relatives à la protection des orgues au titre des monuments historiques (recensement, surveillance de l'état, propositions de mesures de conservation, participation à l'instruction des projets de travaux autres que ceux pour lesquels il a fait acte de candidature pour des missions de maîtrise d'œuvre, suivi de l'entretien des orgues appartenant à l'État). Il peut également être amené à participer à des programmes de recherche et d'enseignement dans le domaine du patrimoine instrumental.

*Pour aller plus loin* : chapitre II du décret 2016-831 du 22 juin 2016 relatif aux techniciens-conseils agréés pour les orgues protégés au titre des monuments historiques.

Le Code du patrimoine dispose que le propriétaire ou l'affectataire d'un orgue classé ou inscrit au titre des monuments historiques est tenu de confier la maîtrise d'œuvre des travaux (réparation, relevage et restauration) :

- soit à un technicien-conseil agréé ;
- soit sur une opération donnée, à un ressortissant français ou un ressortissant d'un autre État membre de l'Union européenne (UE) dont la formation, l'expérience professionnelle acquises sur des opérations récentes sur des orgues à caractère patrimonial attestent des connaissances nécessaires à la conduite des travaux.

*Pour aller plus loin* : chapitre II du décret 2016-831 du 22 juin 2016 relatif aux techniciens-conseils agréés pour les orgues protégés au titre des monuments historiques.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Le titre de technicien-conseil pour les orgues protégés est réservé aux professionnels ayant reçu un agrément délivré par le ministre chargé de la culture. L'agrément est délivré par une procédure d'appel à candidatures, pour une durée de 5 and renouvelables.

*Pour aller plus loin* : arrêté du 23 février 2017 relatif aux conditions requises pour l'agrément des techniciens-conseils pour les orgues protégés au titre des monuments historiques et aux condition relatives à la déclaration visant à exercer l'activité à titre temporaire et occasionnel.

#### Formation

Afin d'obtenir l'agrément du ministère chargé de la culture, le professionnel qui souhaite obtenir la qualité de technicien-conseil pour les orgues protégés doit remplir les conditions suivantes :

- être titulaire d'un diplôme de second cycle d'études supérieures ou équivalent dans les domaines de la musicologie, de l'organologie et de la pratique instrumentale ;
- avoir acquis une expérience professionnelle d'au moins un an dans ces domaines.

Lorsque la formation suivie est différente de celle précédemment citée, le candidat doit justifier avoir acquis une expérience professionnelle d'au moins six ans à temps plein ou à temps partiel au cours des dix dernières années à compter de la date du dépôt de la candidature, dans le domaine de la facture d'orgues en qualité d'organologue ou de maître d'œuvre.

#### Coûts associés à la qualification

La formation menant à l’obtention des diplômes requis pour exercer la profession de technicien-conseil pour orgues protégés est payante. Son coût varie selon les établissements (universités, conservatoires ou écoles privées) qui dispensent les enseignements dans le domaine de la musicologie, de l'organologie et de la pratique instrumentale.

### b. Ressortissants UE : en vue d’un exercice temporaire ou occasionnel (Libre Prestation de Services(LPS))

Le ressortissant d’un État membre de l’Union européenne (UE) ou de l’Espace économique européen (EEE) exerçant l’activité de technicien-conseil pour orgues protégés peut faire usage de son titre professionnel en France, à titre temporaire et occasionnel, dès lors qu’il en fait la demande, préalablement à sa première prestation, par déclaration adressée au ministre chargé de la culture (cf. infra « 5° .a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an au cours des dix années précédant la prestation, dans un ou plusieurs États membres de l'Union européenne.

Par ailleurs, le ressortissant européen désireux d’exercer en France de manière temporaire et occasionnelle doit posséder les connaissances linguistiques nécessaires à l’exercice de l’activité.

*Pour aller plus loin* : article R. 622-59 du Code du patrimoine.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Pour exercer en France à titre permanent la maîtrise d'œuvre sur les travaux sur les orgues protégés au titre des monuments historiques, le ressortissant d’un État membre de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un État membre de l’UE ou de l’EEE qui réglemente l'accès à cette activité ou son exercice ;
- attester de l’exercice de cette activité pendant au moins une année à temps plein ou à temps partiel au cours des dix dernières années dans un État membre de l’UE ou de l’EEE qui ne réglemente ni l’accès à la profession ni l’exercice de l’activité ;
- être titulaire d'un titre de formation délivré par un État tiers à l'Union européenne, reconnu par un État membre de l’UE ou de l’EEE qui lui a permis d’exercer l’activité de technicien-conseil dans cet État pendant trois années minimum.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions pré-citées pourra demander un agrément auprès des services chargés des monuments historique au sein du ministère chargé de la culture . Pour plus d’informations, il est conseillé de se reporter au paragraphe « 5°. b. Obtenir un agrément pour les ressortissants de l’UE en vue d’un exercice permanent (LE) ».

En cas de différences substantielles entre les qualifications professionnelles acquises et celles requises en France, le ressortissant pourra être soumis à des mesures de compensation (cf. infra « Bon à savoir : mesure de compensation »).

Le ressortissant qui souhaite obtenir l’agrément pour exercer son activité en France doit posséder les connaissances linguistiques nécessaires à l’exercice de la fonction.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Bien que non codifiés, l’ensemble des devoirs s’imposant aux techniciens-conseils pour les orgues protégés s’applique aux ressortissants souhaitant exercer la profession en France.

## 4°. Assurance

En tant que maître d'œuvre, l’intéressé a l’obligation de souscrire une assurance de responsabilité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualifications

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

La déclaration préalable d’activité doit être adressée au ministre chargé de la culture avant la première prestation de services.

#### Renouvellement de la déclaration préalable

La déclaration préalable doit être renouvelée une fois par an si le ressortissant souhaite effectuer une nouvelle prestation de services en France.

Cette déclaration est adressée par lettre recommandée avec demande d'accusé de réception, ou déposée, contre récépissé, à la sous-direction des monuments historiques et des espaces protégés de la direction générale des patrimoines.

Elle comprend les pièces suivantes accompagnées, le cas échéant, de leur traduction en français :

- la copie d'une pièce d'identité en cours de validité ;
- l'attestation certifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE pour y exercer l'activité de technicien-conseil pour les orgues protégés au titre des monuments historiques et qu'il n'encourt aucune interdiction, même temporaire, d'exercer ;
- la preuve de ses qualifications professionnelles ;
- si l'État ne réglemente ni l'accès ni l'exercice de l'activité, une preuve par tout moyen que le ressortissant a exercé cette activité pendant au moins une année, à temps plein ou à temps partiel, au cours des dix années précédant le dépôt de la déclaration.

*Pour aller plus loin* : article 8 de l’arrêté du 23 février 2017.

### b. Obtenir un agrément pour les ressortissants de l’UE en vue d’un exercice permanent (LE)

Tout ressortissant de l‘UE ou de l’EEE qualifié pour exercer tout ou partie des activités mentionnées au décret 2016-831 du 22 juin 2016 relatif aux techniciens-conseils agréés pour les orgues protégés au titre des monuments historiques souhaitant s’établir en France doit préalablement obtenir un agrément du ministre chargé de la culture et des monuments historiques.

Cet agrément doit être renouvelé tous les cinq ans.

Pour obtenir cet agrément, le ressortissant peut être amené à effectuer une mesure de compensation (épreuve d’aptitude ou stage d’adaptation) s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France (cf. infra « Bon à savoir : mesures de compensation »).

#### Autorité compétente

Le dossier de candidature est transmis à la sous-direction des monuments historiques et des espaces protégés de la direction générale des patrimoines. Le dossier est ensuite soumis pour avis à la Commission nationale du patrimoine et de l'architecture qui auditionne le candidat.

#### Délai

La délivrance de l’agrément par le ministre chargé de la culture et des monuments historiques intervient dans un délai de deux mois à compter du jour de la réception du dossier complet par le candidat, par décision motivée.

#### Pièces justificatives

À l'appui de sa demande d’agrément, le ressortissant d’un État de l’UE ou de l’EEE doit produire un dossier complet adressé par lettre recommandée avec avis de réception ou déposé contre récépissé. Il doit comporter les justificatifs suivants :

- une copie d'une pièce d'identité en cours de validité ;
- un curriculum vitae détaillé ;
- les références bibliographiques des études, recherches ou publications ;
- la liste des missions réalisées par le candidat en qualité d'organologue ou de maître d'œuvre ;
- un dossier équivalent au contenu d'un dossier de protection d'un orgue ;
- un dossier équivalent à une étude préalable à la restauration d'un orgue ;
- un dossier regroupant l'ensemble des documents établis par le candidat dans l'exécution d'une mission complète de maîtrise d'œuvre, incluant un dossier équivalent au contenu d'un dossier documentaire des ouvrages exécutés.

À l’ensemble de ces pièces devront également s’ajouter d’autres éléments justificatifs selon que :

- le ressortissant exerce légalement l’activité de technicien-conseil dans un État de l’UE ou de l’EEE qui règlemente l’accès à l’activité de technicien-conseil ou son exercice. Dans ce cas, il devra produire :
  - une copie de l'attestation de compétences ou du titre de formation sanctionnant une formation dans le domaine de la musicologie, de l'organologie et de la pratique instrumentale délivré par les autorités compétentes de cet État,
  - une copie du descriptif détaillé du programme des études suivies ;
- lorsque l’accès à la profession ou l’exercice de l’activité n’est pas réglementé dans un État de l’UE ou de l’EEE, le ressortissant d’un de ces État justifie de l’exercice d’une activité pendant au moins un an, à temps partiel ou à temps complet, au cours des dix dernières années dans cet État. Dans ce cas, il devra produire :
  - une copie de toute attestation de compétences ou de tout titre de formation délivré par les autorités compétentes d'un État de l’UE ou de l’EEE et attestant de la préparation à l'exercice de l'activité de technicien-conseil pour les orgues protégés,
  - un descriptif de l'expérience professionnelle acquise ;
- le ressortissant d’un État de l’UE ou de l’EEE a obtenu un titre de formation dans un État tiers, reconnu dans un État de l’UE ou de l’EEE, et a légalement exercé la profession pendant au moins trois années. Dans ce cas, il devra produire:
  - une copie du titre de formation délivré par l’État tiers,
  - une copie du document délivré par l'État dans lequel l'expérience professionnelle a été acquise, certifiant l'exercice de l'activité de technicien conseil,
  - un descriptif de l'expérience professionnelle acquise.

**À noter**

En cas de dossier incomplet, la sous-direction des monuments historiques et des espaces protégés contactera par lettre recommandée avec accusé de réception le candidat afin de l'inviter à fournir les pièces nécessaires.

#### Pièce justificative pour un renouvellement de l’agrément

Seul un rapport d’activité établi par le candidat, traduit en français, sera nécessaire au dossier de candidature.

#### Coût

Gratuit.

*Pour aller plus loin* : articles 3 et 4 de l’arrêté du 23 février 2017.

#### Bon à savoir : mesure de compensation

Après examen des pièces justificatives, la sous-direction en charge des monuments historiques indique par décision motivée au ressortissant, candidat à l’agrément, le recours à des mesures de compensation lorsqu’il existe des différences substantielles entre la formation du ressortissant et la formation délivrée en France.

La décision motivée comportera notamment :

- le niveau de qualification professionnelle requis ;
- le niveau de qualification professionnelle du candidat à l’agrément ;
- les différences entre la formation du candidat et la formation délivrée en France ;
- les raisons pour lesquelles les différences ne peuvent pas être comblées par les connaissances, aptitudes et compétences acquises au cours de l’expérience professionnelle ou de l’apprentissage.

Ces mesures peuvent prendre la forme d’un stage d’adaptation ou d’une épreuve d’aptitude, le ressortissant disposant de deux mois pour faire connaître son choix. Les matières de l’épreuve d'aptitude et la durée du stage seront indiquées par arrêté ministériel.

##### L’épreuve d’aptitude

L’épreuve d’aptitude interviendra six mois après que le candidat a notifié le choix de la mesure de compensation choisie.

Lors de l’épreuve, le candidat se présentera devant un jury désigné par le ministre chargé des monuments historiques.

*Pour aller plus loin* : article 5 de l’arrêté du 23 février 2017.

##### Le stage d’adaptation

Le stage, qui ne peut excéder un an, doit être effectué auprès d'un technicien-conseil agréé pour les orgues protégés en activité au moment de la demande et ayant exercé cette activité pendant au moins cinq ans, dans les dix dernières années précédant le début du stage.

À l’issue du stage, le candidat stagiaire sera soumis à une évaluation par le responsable du stage qui rendra un rapport et le transmettra à la sous-direction des monuments historiques dans un délai d’un mois à compter de la fin du stage.

##### Issue de la procédure

À réception du rapport d’évaluation du stage ou des résultats obtenus à l’épreuve d’aptitude, le ministre chargé des monuments historiques aura deux mois pour se prononcer sur la décision d’agrément.

*Pour aller plus loin* : article 6 de l’arrêté du 23 février 2017.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).