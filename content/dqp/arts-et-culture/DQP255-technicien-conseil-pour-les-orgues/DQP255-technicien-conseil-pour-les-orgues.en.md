﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP255" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts and culture" -->
<!-- var(title)="Technical advisor for organs protected as French historic monuments" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-and-culture" -->
<!-- var(title-short)="technical-advisor-for-organs-protected-as-French-historic-monuments" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/arts-and-culture/technical-advisor-for-organs-protected-as-French-historic-monuments.html" -->
<!-- var(last-update)="2020-04-15 17:20:43" -->
<!-- var(url-name)="technical-advisor-for-organs-protected-as-French-historic-monuments" -->
<!-- var(translation)="Auto" -->


Technical advisor for organs protected as French historic monuments
==========================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:43<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Definition of activity
-------------------------

The protected organ consultant is a professional who assists the Ministry of Culture in carrying out public service missions relating to the protection of organs under historical monuments (census, state supervision, proposals for conservation measures, participation in the training of projects other than those for which he applied for project management missions, followed by the maintenance of organs belonging to State). He may also be required to participate in research and teaching programmes in the field of instrumental heritage.

*For further information*: :[Chapter II of The 2016-831 Decree](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032753839&categorieLien=id) June 22, 2016 on certified consulting technicians for organs protected under historical monuments.

The Heritage Code stipulates that the owner or assignee of an organ classified or registered as a historical monument is required to entrust the project 'repair, survey and restoration':

- either to a licensed consulting technician;
- either on a given operation, to a French national or a national of another Member State of the European Union (EU) whose training, professional experience gained on recent operations on heritage organs attest to the knowledge required to carry out the work.

*For further information*: :[Chapter II of The 2016-831 Decree](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032753839&categorieLien=id) June 22, 2016 on certified consulting technicians for organs protected under historical monuments.

2°. Professional qualifications
------------------------------

### a. National requirements

#### National legislation

The title of consultant technician for protected organs is reserved for professionals who have received accreditation from the Minister responsible for culture. Accreditation is issued through a 5-year and renewable application process.

*For further information*: :[23 February 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034133677&categorieLien=id) relating to the conditions required for the accreditation of consulting technicians for organs protected under historical monuments and the conditions relating to the declaration to carry out the activity on a temporary and casual basis.

#### Training

In order to obtain the approval of the Ministry of Culture, the professional who wishes to obtain the status of consultant technician for protected organs must meet the following conditions:

- hold a graduate degree or equivalent in the fields of musicology, organology and instrumental practice;
- have at least one year of professional experience in these areas.

Where the training taken is different from the one previously cited, the applicant must justify having acquired at least six years of full-time or part-time work experience in the last ten years from the date of the organ invoices as an organologist or prime contractor.

#### Costs associated with qualification

Training leading to the degree required to practice as a consultant technician for protected organs is paid for. Its cost varies according to the institutions (universities, conservatories or private schools) that provide teachings in the field of musicology, organology and instrumental practice.

### b. EU nationals: for temporary or casual exercise (Freedom to provide services)

The national of a Member State of the European Union (EU) or the European Economic Area (EEA) working as a consultant technician for protected organs may make use of his professional title in France, on a temporary and casual basis, as soon as requesting it, prior to its first performance, by declaration addressed to the Minister for Culture (see infra "5.a. Make a prior declaration of activity for EU nationals engaged in temporary and temporary activity and (LPS)).

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the country in which the professional is legally established, he must have carried out this activity for at least one year during the ten years before the benefit, in one or more Eu-Eu Member States.

In addition, a European national wishing to practise in France on a temporary and occasional basis must have the necessary language skills to carry out the activity.

*For further information*: :[Article R. 622-59](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000029691954&dateTexte=&categorieLien=cid) Heritage Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

In order to exercise permanent control in France on works on organs protected under historical monuments, the national of an EU or EEA Member State must meet one of the following conditions:

- Hold a certificate of competency or training certificate issued by the competent authority of an EU or EEA Member State that regulates access to or exercise of this activity;
- certify that this activity has been performed for at least one year full-time or part-time in the last ten years in an EU or EEA Member State that does not regulate access to the profession or the exercise of the activity;
- hold a training certificate issued by a third country to the European Union, recognised by an EU or EEA Member State which has enabled it to work as a consulting technician in that state for a minimum of three years.

**Please note**

A national of an EU or EEA state that fulfils one of the above-mentioned conditions will be able to apply for accreditation from the historical monuments services within the Ministry of Culture. For more information, it is advisable to refer to paragraph "5." b. Obtain accreditation for EU nationals for a permanent exercise (LE).

In the event of substantial differences between the professional qualifications acquired and those required in France, the national may be subject to compensation measures (see below "Good to know: compensation measure").

A national who wishes to obtain accreditation to carry out his activity in France must have the necessary language skills to carry out the function.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------

Although not codified, all the duties required of consulting technicians for protected organs apply to nationals wishing to practice the profession in France.

4°. Insurance
------------

As prime contractor, the person concerned has an obligation to take out professional liability insurance.

5°. Qualification recognition procedures and formalities
-------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The prior declaration of activity must be addressed to the Minister responsible for culture before the first service delivery.

**Renewal of pre-declaration**

The prior declaration must be renewed once a year if the national wishes to carry out a new service in France.

This declaration is addressed by letter recommended with request for acknowledgement, or filed, against receipt, to the sub-direction of historical monuments and protected areas of the Directorate General of Heritages.

It includes the following pieces accompanied, if necessary, by their translation into French:

- Copying a valid piece of identification
- certificate certifying that the national is legally established in an EU or EEA state to carry out the activity of consulting technician for organs protected under historical monuments and that he does not incur any prohibition, even temporary, exercise;
- Proof of his professional qualifications
- if the state does not regulate access or the exercise of the activity, evidence by any means that the national has engaged in this activity for at least one year, full-time or part-time, in the ten years prior to filing the return.

*For further information*: Article 8 of the Order of 23 February 2017.

### b. Obtaining accreditation for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in the 2016-831 decree of 22 June 2016 relating to certified advisory technicians for organs protected under historical monuments wishing to establish themselves in France must first obtain approval from the Minister for Culture and Historic Monuments.

This accreditation must be renewed every five years.

In order to obtain this certification, the national may be required to carry out a compensation measure (fitness test or accommodation course) if it turns out that the qualifications and work experience he uses are substantially different from those required for the practice of the profession in France (see below: "Good to know: compensation measures").

**Competent authority**

The application file is forwarded to the directorate of historical monuments and protected areas of the Directorate General of Heritage. The file is then submitted for notice to the National Heritage and Architecture Commission, which auditions the candidate.

**Timeframe**

The granting of accreditation by the Minister responsible for Culture and Historic Monuments takes place within two months of the day the candidate receives the complete file by reasoned decision.

**Supporting documents**

In support of their application for accreditation, a national of an EU or EEA state must produce a complete file sent by letter recommended with notice of receipt or filed against receipt. It must include the following proofs:

- A copy of a valid ID
- A detailed resume
- references to studies, research or publications;
- The list of assignments carried out by the candidate as an organologist or prime contractor;
- A folder equivalent to the contents of an organ protection folder
- a file equivalent to a pre-restoration study of an organ;
- a file containing all the documents prepared by the candidate in the execution of a complete project management mission, including a file equivalent to the contents of a documentary file of the works performed.

To all of these exhibits will also need to be added other supporting elements depending on whether:

- the national is legally a consultant technician in an EU or EEA state that regulates access to or exercise in the consulting technician activity. In this case, it will have to produce:- A copy of the certificate of competency or training title sanctioning training in the field of musicology, organology and instrumental practice issued by the competent authorities of that state,
  - A copy of the detailed description of the curriculum followed;
- where access to the profession or the exercise of the activity is not regulated in an EU or EEA state, the national of one of these states justifies the exercise of an activity for at least one year, part-time or full-time, during the ten in recent years in that state. In this case, it will have to produce:- a copy of any certificate of competency or training document issued by the competent authorities of an EU or EEA state attesting to the preparation for the exercise of the activity of consultant technician for protected organs,
  - A description of the professional experience gained
- the national of an EU or EEA state has obtained a training designation in a third state, recognised in an EU or EEA state, and has legally practiced the profession for at least three years. In this case, it will have to produce:- A copy of the training title issued by the third state,
  - a copy of the document issued by the state in which work experience was acquired, certifying the exercise of the consulting technician activity,
  - a description of the professional experience gained.

**Please note**

In the event of an incomplete file, the Directorate of Historic Monuments and Protected Areas will contact the applicant by letter with acknowledgement to invite them to provide the necessary documents.

**Proof of approval**

Only an activity report prepared by the candidate, translated into French, will be required for the application file.

**Cost**

Free.

*For further information*: Articles 3 and 4 of the February 23, 2017 order.

**Good to know: compensation measure**

After reviewing the supporting documents, the Directorate in charge of historical monuments indicates by reasoned decision to the national, a candidate for accreditation, the use of compensation measures where there are substantial differences between training of the national and training delivered in France.

The reasoned decision will include:

- The required level of professional qualification
- The candidate's level of professional qualification for accreditation;
- Differences between the candidate's training and the training delivered in France;
- reasons why differences cannot be bridged by the knowledge, skills and skills acquired through work experience or learning.

These measures can take the form of an adjustment course or an aptitude test, as the national has two months to make his choice known. The subjects of the aptitude test and the duration of the internship will be indicated by ministerial decree.

**The aptitude test**

The aptitude test will take place six months after the candidate has notified the choice of the chosen compensation measure.

During the event, the candidate will appear before a jury appointed by the Minister responsible for historical monuments.

*For further information*: Article 5 of the Order of 23 February 2017.

**The adaptation course**

The internship, which may not exceed one year, must be carried out with a consultant technician for protected organs active at the time of application and having carried out this activity for at least five years, in the last ten years prior to the start of the internship.

At the end of the internship, the trainee candidate will be subjected to an evaluation by the internship manager who will report it and forward it to the historical monuments sub-directorate within one month of the end of the internship.

**Outcome of the procedure**

Upon receipt of the evaluation report of the internship or the results obtained in the aptitude test, the Minister responsible for historical monuments will have two months to decide on the accreditation decision.

*For further information*: Article 6 of the Order of 23 February 2017.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

