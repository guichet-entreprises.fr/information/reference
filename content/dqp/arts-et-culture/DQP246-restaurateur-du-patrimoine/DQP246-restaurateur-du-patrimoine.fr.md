﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP246" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts et culture" -->
<!-- var(title)="Restaurateur du patrimoine" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-et-culture" -->
<!-- var(title-short)="restaurateur-du-patrimoine" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/arts-et-culture/restaurateur-du-patrimoine.html" -->
<!-- var(last-update)="2020-04-15 17:20:42" -->
<!-- var(url-name)="restaurateur-du-patrimoine" -->
<!-- var(translation)="None" -->

# Restaurateur du patrimoine

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:42<!-- end-var -->

## 1°. Définition de l'activité

Le restaurateur peut agir en conservation préventive (action sur l’environnement de l’œuvre dans le but de diminuer les risques de dégradation), en conservation curative (intervention directe sur l’objet pour stabiliser son état), en restauration (intervention directe effectuée sur l’objet pour en améliorer l’état, la connaissance, la compréhension et l’usage). Avant toute intervention, le professionnel établit un constat d’état du bien sur la base d’une observation approfondie, de la documentation et, le cas échéant, d’analyses ou d’études complémentaires. À partir de ces éléments, il formule un diagnostic, un pronostic, et des propositions d’intervention.

Le restaurateur peut exercer sous différents statuts professionnels, tels qu’artisan, salarié d’entreprise artisanale ou en exercice libéral. Il peut travailler pour des particuliers, des opérateurs du marché de l’art, des institutions privées ou publiques. 

Pour intervenir en restauration sur des biens faisant partie des collections des musées de France, des conditions particulières en matière de qualifications professionnelles sont requises. 

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession de restaurateur de patrimoine en France peut être exercée à la suite du suivi de formations diplômantes spécialisées et/ou de l’acquisition d’une expérience professionnelle dans ce domaine.

En revanche, les interventions de restauration sur les biens des collections des musées de France sont exclusivement réservées à toute personne :

- titulaire d'un diplôme français à finalité professionnelle dans le domaine de la restauration de patrimoine, délivré après cinq années de formation dans le même domaine ;
- ayant bénéficié de la validation des acquis de l'expérience (VAE) en matière de restauration de patrimoine ;
- titulaire d'un diplôme français à finalité professionnelle dans le domaine de la restauration de patrimoine, délivré après quatre années de formation dans le même domaine, obtenu avant le 29 avril 2002 ;
- qui, entre le 28 avril 1997 et le 29 avril 2002, a restauré des biens des musées ayant reçu ou ayant été susceptibles de recevoir l'appellation « musée de France » et qui a été habilitée par le ministre chargé de la culture à assurer des opérations de restauration sur les biens des musées de France ;
- ayant la qualité de fonctionnaire et ayant vocation statutaire à assurer des travaux de restauration.

*Pour aller plus loin* : article R. 452-10 du Code du patrimoine.

#### Formation

La délivrance du diplôme français permettant la restauration de biens des collections des musées de France doit intervenir à l'issue d'une formation de cinq ans au cours de laquelle l'intéressé aura suivi des enseignement théoriques et pratiques, complétés de périodes de stages en France et à l'étranger.

La formation s'achève par la réalisation d'un mémoire de fin de second cycle portant sur un travail de restauration d'un bien culturel, soutenu devant un jury de professionnels de la restauration.

Plusieurs organismes proposent des diplômes menant à la profession de restaurateur pouvant intervenir sur les biens des collections des musées de France dont notamment :

- l'Institut national de patrimoine (INP) ;
- l'université Panthéon-Sorbonne de Paris avec le master Conservation et restauration de biens culturels ;
- l’École supérieure d’art d’Avignon ;
- l’Ecole supérieure des beaux-arts TALM – Tours

*Pour aller plus loin* : articles 5 et suivants du chapitre II de l'arrêté du 3 mai 2016 relatif aux qualifications requises pour procéder à la restauration d'un bien faisant partie des collections des musées de France.

#### Coûts associés à la qualification

La formation menant à l'activité de restaurateur de patrimoine pouvant intervenir sur les biens des collections des musées de France est payante. Pour plus d'informations, il est conseillé de se rapprocher des organismes la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union européenne (UE) ou de l’Espace économique européen (EEE) exerçant légalement l’activité de restaurateur de biens faisant partie de collections de musées d’intérêt général dans l’un de ces États peut faire usage de son titre professionnel en France, à titre temporaire et occasionnel.

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, il doit l'avoir exercée dans cet État pendant au moins une année, à temps plein ou à temps partiel, au cours des dix années qui précèdent la prestation.

L'intéressé devra effectuer une déclaration, préalablement à sa première prestation, auprès du ministre chargé de la culture (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

*Pour aller plus loin* : article R. 452-12 du Code du patrimoine.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de restaurateur de biens faisant partie de collections de musées d’intérêt général dans cet État peut exercer la même activité en France sur les biens faisant partie des collections des musées de France de manière permanente si :

- il est titulaire d'un diplôme délivré par une autorité compétente d'un autre État membre ou d’un État de l’EEE qui réglemente l'accès à la profession ou son exercice et permet d’y exercer légalement cette profession ;
- il est titulaire d'un titre de formation délivré par un État tiers mais certifié par un État membre ou dans un autre État de l’EEE et que ce titre lui a permis d’exercer trois ans cette profession dans cet État ;
- il a exercé l’activité de restauration de biens de collections de musées d’intérêt général à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation ni l'exercice de la profession, et que l’intéressé détient un diplôme sanctionnant une formation de l'enseignement supérieur, délivré par les autorités compétentes d'un État membre ou d'un autre État partie à l'Espace économique européen et attestant de sa préparation à l'exercice de cette activité. La condition d'exercice à temps plein pendant un an de l'activité de restauration des biens de collections d'intérêt général n'est pas exigée lorsque le diplôme détenu par le demandeur sanctionne une formation réglementée dans l'État d'origine.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander la reconnaissance de ses qualifications professionnelles auprès du ministre chargé de la culture (cf. infra « 5°. b. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE) »).

Si, lors de l'examen de la demande, le ministre chargé de la culture constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées en France, des mesures de compensation pourront être prises.

Si l'intéressé est titulaire d'une attestation de compétence au sens du a de l'article 11 de la directive 2005/36/CE du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles, le ministre peut refuser l'accès à la profession et son exercice au titulaire

*Pour aller plus loin* : article R. 452-11 du Code du patrimoine.

Un accès partiel à une activité professionnelle relevant de la profession de restaurateur d'un bien faisant partie des collections des musées de France, peut être accordé au cas par cas aux ressortissants d'un État membre de l’UE ou de l’EEE lorsque les trois conditions suivantes sont remplies :

- le professionnel est pleinement qualifié pour exercer, dans l'État d'origine, l'activité professionnelle pour laquelle l'accès partiel est sollicité ;
- les différences entre l'activité professionnelle légalement exercée dans l'État d'origine et la profession réglementée en France de restaurateur d'un bien faisant partie des collections des musées de France sont si importantes que l'application de mesures de compensation reviendrait à imposer au demandeur de suivre le programme complet d'enseignement et de formation requis en France pour avoir pleinement accès à cette profession réglementée ;
- l'activité professionnelle est distincte de la ou des autres activités relevant de la profession réglementée, notamment dans la mesure où elle est exercée de manière autonome dans l'État d'origine.

L'accès partiel peut être refusé pour des raisons impérieuses d'intérêt général, si ce refus est proportionné à la protection de cet intérêt.

Les demandes aux fins d'accès partiel sont examinées, selon le cas, comme des demandes à fin d'établissement ou de libre prestation de services temporaire et occasionnelle de la profession concernée.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Bien que non réglementées, des obligations déontologiques et éthiques incombent aux restaurateurs du patrimoine, et notamment de :

- respecter l'intégrité physique, esthétique et historique des biens culturels qu'ils restaurent ;
- garder une discrétion professionnelle sur le bien culturel restauré ;
- prendre en compte tous les aspects de la conservation préventive avant d'intervenir directement sur les biens culturels.

Pour plus d'informations, il est conseillé de se reporter au Code de déontologie rédigé par la Confédération européenne des organisations de conservateurs-restaurateurs (ECCO).

## 4°. Assurance

En cas d’exercice libéral, le restaurateur du patrimoine a l’obligation de souscrire une assurance de responsabilité professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. Dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le ministre chargé de la culture est compétent pour se prononcer sur la demande de déclaration.

#### Pièces justificatives

La demande de déclaration s'effectue par le dépôt d'un dossier transmis par lettre recommandée au service des musées de France de la direction générale des patrimoines, comprenant l'ensemble des documents suivants :

- une copie de la pièce d'identité en cours de validité ;
- une attestation certifiant que l'intéressé exerce et est établi légalement dans un État de l'UE ou de l'EEE ;
- la preuve des qualifications professionnelles du ressortissant ;
- une preuve par tout moyen qu'il a exercé cette activité pendant un an à temps plein ou à temps partiel au cours des dix dernières années lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE.

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé, le cas échéant.

#### Renouvellement

La déclaration doit être renouvelée une fois par an si le ressortissant souhaite faire une nouvelle prestation dans l'année en cours, ainsi qu'en cas de changement de situation du ressortissant.

*Pour aller plus loin* : article R. 452-12 et article 11 de l'arrêté du 3 mai 2016 relatif aux qualifications requises pour procéder à la restauration d'un bien faisant partie des collections des musées de France.

### b. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE)

#### Autorité compétente

Le ministre chargé de la culture est compétent pour se prononcer sur la demande de reconnaissance.

#### Pièces justificatives

La demande de reconnaissance des qualifications professionnelles s'effectue par le dépôt d'un dossier transmis par lettre recommandée au service des musées de France de la direction générale des patrimoines, comprenant l'ensemble des documents suivants :

- une copie de la pièce d'identité en cours de validité ;
- lorsque l’État membre réglemente l'accès à la profession :
  - une copie des diplômes, certificats et autres titres de formation délivrés par l'autorité compétente de cet État permettant d’exercer légalement cette activité,
  - une copie du programme détaillé des études suivies ;
- lorsque le titre de formation a été délivré par un État tiers :
  - une copie de la reconnaissance par un État membre de ce titre,
  - tout document délivré par cet État justifiant que le ressortissant a exercé l'activité de restauration de biens faisant partie de collections de musées d’intérêt général pendant au moins trois ans,
  - une copie du programme détaillé des études suivies,
  - un descriptif de l'expérience professionnelle acquise ;
- lorsque l’État membre ne réglemente ni la profession ni l'activité :
  - une preuve par tout moyen que le ressortissant a exercé pendant un an l'activité de restauration de biens faisant partie de collections de musées d’intérêt général dans cet État au cours des dix dernières années,
  - une copie du programme détaillé des études suivies,
  - un descriptif de l'expérience professionnelle acquise.

#### Procédure

Le service des musées de France accusera réception du dossier et pourra demander, le cas échéant, l'envoi de pièces supplémentaires. Une fois le dossier complet, le ministre chargé de la culture aura deux mois pour se prononcer sur la demande de reconnaissance ou informer le ressortissant qu'il devra se soumettre à l'une des mesures de compensation suivantes :

- un stage d'adaptation d'une durée maximum d'un an ;
- une épreuve d'aptitude.

Dès lors que l'intéressé aura effectué soit le stage d'adaptation, soit l'épreuve d'aptitude, le ministre chargé de la culture disposera de deux mois supplémentaires pour se prononcer et rendre une décision motivée de reconnaissance ou non de ses qualifications professionnelles.

*Pour aller plus loin* : articles 5 et suivants de l'arrêté du 3 mai 2016 relatif aux qualifications requises pour procéder à la restauration d'un bien faisant partie des collections des musées de France.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne.

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).