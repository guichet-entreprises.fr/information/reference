﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP246" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts and culture" -->
<!-- var(title)="Restorer of cultural heritage" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-and-culture" -->
<!-- var(title-short)="restorer-of-cultural-heritage" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/arts-and-culture/restorer-of-cultural-heritage.html" -->
<!-- var(last-update)="2020-04-15 17:20:43" -->
<!-- var(url-name)="restorer-of-cultural-heritage" -->
<!-- var(translation)="Auto" -->


Restorer of cultural heritage
=================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:43<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The conservator can act in preventive conservation (action on the environment of the work in order to reduce the risk of degradation), curative conservation (direct intervention on the object to stabilize its condition), restoration ( direct intervention on the object to improve its condition, knowledge, understanding and use). Before any intervention, the professional establishes a statement of the condition of the property on the basis of in-depth observation, documentation and, if necessary, further analysis or studies. Based on these elements, he formulates a diagnosis, a prognosis, and proposals for intervention.

The restaurateur may practice under various professional statuses, such as craftsman, artisan or liberal company employee. He may work for individuals, art market operators, private or public institutions.

To intervene in restoration on goods that are part of the collections of museums in France, special conditions for professional qualifications are required.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of heritage restorer in France can be carried out following the follow-up of specialized degree training and/or the acquisition of professional experience in this field.

On the other hand, restoration interventions on the property of the collections of museums in France are exclusively reserved for anyone:

- Holder of a French diploma for professional purposes in the field of heritage restoration, issued after five years of training in the same field;
- having benefited from the validation of experience (VAE) in heritage restoration;
- holder of a French diploma for professional purposes in the field of heritage restoration, awarded after four years of training in the same field, obtained before 29 April 2002;
- which, between 28 April 1997 and 29 April 2002, restored the property of museums that had received or was likely to receive the name "Museum of France" and which was empowered by the Minister responsible for culture to carry out restoration operations on the museum property in France;
- having the status of civil servant and having a statutory vocation to ensure restoration work.

*To go further:* Article R. 452-10 of the Heritage Code.

#### Training

The issuance of the French diploma allowing the restoration of property from the collections of the museums of France must take place at the end of a five-year training course during which the person concerned will have followed theoretical and practical teachings, supplemented by internships in France and abroad.

The training concludes with the completion of an end-of-cycle dissertation on a work to restore a cultural property, supported before a jury of catering professionals.

Several organizations offer diplomas leading to the profession of restorer who can intervene on the assets of the collections of museums in France including:

- National Heritage Institute (INP);
- Pantheon-Sorbonne University in Paris with the master's degree in conservation and restoration of cultural property;
- Avignon Higher School of Art;
- TALM Higher School of Fine Arts - Tours

*To go further:* Articles 5 and following chapter II of the order of 3 May 2016 relating to the qualifications required to proceed with the restoration of a property that is part of the collections of the museums of France.

#### Costs associated with qualification

The training leading to the activity of heritage restorers who can intervene on the property of the collections of the museums of France is paid. For more information, it is advisable to get closer to the organizations dispensing it.

### b. EU or EEA nationals: for temporary and casual exercise (free provision of services)

A national of a European Union (EU) or European Economic Area (EEA) state legally employing the activity of restoring property in collections of museums of general interest in one of these states may make use of its title France, on a temporary and casual basis.

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, it must have been carried out in that state for at least one year, full-time or part-time, during the ten years that precede the performance.

The person concerned will have to make a declaration, prior to his first performance, to the Minister responsible for culture (see infra "5°. a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)").

*To go further:* Article R. 452-12 of the Heritage Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state that is established and legally practises the activity of restoring property in collections of museums of general interest in that state may carry out the same activity in France on property belonging to the collections of museums in France on a permanent basis if:

- it holds a diploma issued by a competent authority in another Member State or an EEA state which regulates access to or practising the profession and allows it to be legally practised in that profession;
- he holds a training certificate issued by a third state but certified by a Member State or another EEA state and that this title has enabled him to practice this profession in that state for three years;
- he has been working to restore the property of museums collections of general interest full-time or part-time for one year for the past ten years in another Member State which does not regulate the training or practice of the profession. , and that the person holds a diploma sanctioning higher education training, issued by the competent authorities of a Member State or another State party to the European Economic Area and attesting to his preparation for the exercise of this Activity. The condition of full-time exercise for one year of the restoration activity of the property of collections of general interest is not required when the diploma held by the applicant sanctions regulated training in the State of origin.

Once the national fulfils one of these conditions, he or she will be able to apply to the Minister for Culture for recognition of his professional qualifications (see infra "5°. b. Request recognition of the EU or EEA national's professional qualifications for a permanent exercise (LE) ").

If, during the review of the application, the Minister responsible for culture finds that there are substantial differences between the training and professional experience of the national and those required in France, compensation measures may be Taken.

If the person holder holds a certificate of competency within the meaning of Article 11 of directive 2005/36/EC of 7 September 2005 relating to the recognition of professional qualifications, the Minister may refuse access to the profession and its exercise to the holder

*To go further:* Article R. 452-11 of the Heritage Code.

Partial access to a professional activity under the profession of conservator of a property belonging to the collections of museums in France, may be granted on a case-by-case basis to nationals of an EU or EEA Member State when all three conditions are met:

- the professional is fully qualified to carry out, in the State of origin, the professional activity for which partial access is requested;
- the differences between the professional activity legally carried out in the State of origin and the regulated profession in France of restoring a property belonging to the collections of museums in France are so important that the application of measures of compensation would be tantamount to forcing the applicant to follow the comprehensive education and training programme required in France to have full access to this regulated profession;
- professional activity is separate from the or other activities of the regulated profession, particularly insofar as it is carried out independently in the State of origin.

Partial access may be denied for compelling reasons of public interest, if this refusal is proportionate to the protection of that interest.

Applications for partial access are considered, as appropriate, as applications for the purpose of establishing or providing temporary and occasional services to the profession concerned.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Although unregulated, ethical and ethical obligations are the responsibility of heritage conservators, including:

- respect the physical, aesthetic and historical integrity of the cultural property they restore;
- maintain professional discretion over the restored cultural property;
- take into account all aspects of preventive conservation before directly intervening on cultural property.

For more information, it is advisable to refer to the Code of Ethics drafted by the European Confederation of Conservators and Conservators' Organisations (ECCO).

4°. Insurance
---------------------------------

In the event of a liberal exercise, the heritage conservator is obliged to take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during this activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)

#### Competent authority

The Minister for Culture is responsible for deciding on the request for a declaration.

#### Supporting documents

The request for declaration is made by filing a file sent by letter recommended to the department of the museums of France of the Directorate General of Heritages, including all the following documents:

- A copy of the valid ID
- A certificate certifying that the person is practising and is legally established in an EU or EEA state;
- Proof of the national's professional qualifications;
- proof by any means that he has been in this activity for one year full-time or part-time in the last ten years when neither professional activity nor training is regulated in the EU or EEA State.

**What to know**

Supporting documents must be written in French or translated by a certified translator, if necessary.

#### Renewal

The declaration must be renewed once a year if the national wishes to make a new benefit in the current year, as well as in the event of a change in the national's situation.

*To go further:* Article R. 452-12 and Article 11 of the Order of 3 May 2016 relating to the qualifications required to proceed with the restoration of a property that is part of the collections of the museums of France.

### b. Request recognition of professional qualifications for EU or EEA nationals for permanent exercise (LE)

#### Competent authority

The Minister for Culture is competent to decide on the application for recognition.

#### Supporting documents

The application for recognition of professional qualifications is made by submitting a file sent by letter recommended to the french museums department of the Directorate General of Heritages, including all the following documents:

- A copy of the valid ID
- member state regulates access to the profession:- A copy of diplomas, certificates and other training documents issued by the competent authority of that state to legally carry out this activity,
  - A copy of the detailed curriculum of the studies followed;
- where the training certificate was issued by a third-party state:- A copy of a Member State's recognition of this title,
  - any document issued by that state justifying that the national has engaged in the restoration of property in museum collections of general interest for at least three years,
  - A copy of the detailed curriculum of the studies followed,
  - A description of the professional experience gained
- Member State does not regulate the profession or the activity:- proof by any means that the national has been engaged in the restoration of property in museum collections of general interest in that state for one year over the past ten years,
  - A copy of the detailed curriculum of the studies followed,
  - a description of the professional experience gained.

#### Procedure

The Museum Department of France will acknowledge receipt of the file and may request, if necessary, the sending of additional pieces. Once the file is complete, the Minister responsible for Culture will have two months to decide on the application for recognition or inform the national that he will have to submit to one of the following compensation measures:

- an adaptation course lasting up to one year
- an aptitude test.

Once the person has completed either the adjustment course or the aptitude test, the Minister responsible for culture will have two more months to decide and make a reasoned decision on whether or not to recognize his qualifications. Professional.

*To go further:* Articles 5 and following of the order of 3 May 2016 relating to the qualifications required to proceed with the restoration of a property belonging to the collections of the museums of France.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete an online complaint form.

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

