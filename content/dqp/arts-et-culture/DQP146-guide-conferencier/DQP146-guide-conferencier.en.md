﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP146" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts and culture" -->
<!-- var(title)="Tourist guide" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-and-culture" -->
<!-- var(title-short)="tourist-guide" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/arts-and-culture/tourist-guide.html" -->
<!-- var(last-update)="2020-04-15 17:20:41" -->
<!-- var(url-name)="tourist-guide" -->
<!-- var(translation)="Auto" -->


Tourist guide
=============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:41<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The guide-lecturer is a professional whose mission is to provide guided tours, in French or in a foreign language, in museums in France and historical monuments. Its role is to enhance heritage by designing cultural mediation actions aimed at the public in heritage territories and places.

*For further information*: Article L. 221-1 of the Tourism Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of guide-lecturer, the person concerned must justify a professional card issued to holders of a certification registered in the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

#### Training

The professional guide-speaker card is given to the holder:

- Professional license as a guide-lecturer;
- master's degree with the following teaching units:- skills of the guide-lecturers,
  - situation and professional practice,
  - a living language other than French;

**Please note**

These teaching units must have been validated by the person concerned. Proof of their validation may take the form of a certificate issued by the educational institution or a descriptive appendix attached to the diploma.

- master's degree and justifying:- at least one year of cumulative professional experience in oral inheritance mediation over the past five years,
  - level C1 to the common European framework of reference for languages ([CECRL](https://www.coe.int/T/DG4/Linguistic/Source/Framework_FR.pdf)) in a foreign living language, a regional language in France or French sign language.

*For further information*: Article 1 and appendix to the[ordered from November 9, 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024813610) relating to the skills required to issue the professional guide-speaker card to holders of a professional license or diploma conferring the master's degree.

#### Costs associated with qualification

Training leading to the profession of guide-lecturer is paid for. For more information, it is advisable to get closer to the establishments issuing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of an EU or EEA Member State who is established and legally practises the activity of guide-speaker in that state may carry out the same activity in France, on a temporary and occasional basis.

The national will simply have to indicate the mention of the professional title or the training title he holds in that state on the documents he will present in France to persons related to the tourist reception as well as to museum or managers historic monument visited.

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the state in which the professional is legally established, he must have carried out this activity for at least one year during the ten years before the benefit, in one or more EU or EEA states.

*For further information*: Articles L. 211-1, L. 221-3, L. 221-4 and R. 221-14 of the Tourism Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state established and legally practising as a guide-speaker in one of these states may carry out the same activity in France on a permanent basis.

He will be able to apply for the professional card from the prefect (see infra "5°. a. Request a business card for the EU or EEA national for a permanent exercise (LE) ), as long as it justifies:

- hold a diploma, certificate or other title allowing the exercise of the activity in a professional capacity in an EU or EEA state and issued by:- the competent authority of that state,
  - a third state, provided with certification from an EU or EEA state recognising the title and certifying that it has been in business for a minimum of three years;
- Hold a training degree acquired in your home state, specifically aimed at the practice of the profession;
- for at least one year full-time or part-time in the last ten years in an EU or EEA state that does not regulate access or the exercise of this activity.

Where there are substantial differences between the professional qualification of the national and the training in France, the competent prefect may require that the person concerned submit to a compensation measure (see infra "5°. a. Good to know: compensation measures").

*For further information*: Articles L.221-2, R. 221-12 and R. 221-13 of the Tourism Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Anyone who works as a guide-lecturer without holding a professional card is fined up to 450 euros.

*For further information*: Article R. 221-3 of the Tourism Code and Article 131-13 of the Penal Code.

4°. Insurance
---------------------------------

The Liberal guide-lecturer must purchase professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Apply for a business card for the EU or EEA national for a permanent exercise (LE)

**Competent authority**

The prefect of the department in which the national wishes to settle is competent to issue the professional guide-speaker card.

**Supporting documents**

In support of his application, the national will have to submit to the competent prefect a file containing the following supporting documents:

- A card application form, completed and signed;
- A document setting out the specific mentions, either linguistic or scientific or cultural, to be included on the map;
- A photocopy of a valid ID
- One or more photo IDs
- A copy of the diploma, certificate or title obtained in an EU or EEA state;
- a certificate justifying its activity for at least one year in the last ten years when neither training nor the practice of the profession is regulated in the EU or EEA State.

**Outcome of the procedure**

The prefect will have one month from the receipt of the supporting evidence to acknowledge it or request the sending of missing documents. The decision to issue the business card will then take place within two months.

The silence kept at the end of this period will be worth the decision to grant the professional card.

**Cost**

Free.

**Good to know: compensation measures**

Where there are substantial differences in the training provided by the national and that required in France, the competent prefect may subject the person concerned to the compensation measure of his choice, namely:

- an adaptation course that cannot last more than three years;
- an aptitude test.

The individual will then have two months to make known the measure of his choice.

In the event that the national holds a certificate of competency or a title sanctioning a high school diploma, the decision on the choice of the compensation measure will be up to the prefect within two months.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

