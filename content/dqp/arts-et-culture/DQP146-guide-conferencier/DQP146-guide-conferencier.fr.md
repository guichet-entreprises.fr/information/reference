﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP146" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts et culture" -->
<!-- var(title)="Guide-conférencier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-et-culture" -->
<!-- var(title-short)="guide-conferencier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/arts-et-culture/guide-conferencier.html" -->
<!-- var(last-update)="2020-04-30" -->
<!-- var(url-name)="guide-conferencier" -->
<!-- var(translation)="None" -->

# Guide-conférencier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30<!-- end-var -->

## 1°. Définition de l'activité

Le guide-conférencier est un professionnel dont la mission est d'assurer des visites guidées, en français ou dans une langue étrangère, dans les musées de France et les monuments historiques. Son rôle est de valoriser le patrimoine en concevant des actions de médiation culturelle à destination des publics dans les territoires et lieux patrimoniaux.

*Pour aller plus loin* : article L. 221-1 du Code du tourisme.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de guide-conférencier, l'intéressé doit justifier d'une carte professionnelle délivrée aux titulaires d'une certification inscrite au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

#### Formation

La carte professionnelle de guide-conférencier est remise au titulaire :

- de la licence professionnelle de guide-conférencier ;
- d'un master comportant les unités d'enseignement suivantes :
  - compétences des guides-conférenciers,
  - mise en situation et pratique professionnelle,
  - langue vivante autre que le français ;

**À noter**

Ces unités d'enseignements doivent impérativement avoir été validées par l'intéressé. La preuve de leur validation peut prendre la forme d'une attestation délivrée par l'établissement d'enseignement ou d'une annexe descriptive jointe au diplôme.

- d'un master et justifiant :
  - d'au moins une année cumulée d'expérience professionnelle dans la médiation orale des patrimoines au cours des cinq dernières années,
  - d'un niveau C1 au cadre européen commun de référence pour les langues ([CECRL](https://www.coe.int/T/DG4/Linguistic/Source/Framework_FR.pdf)) dans une langue vivante étrangère, une langue régionale de France ou la langue des signes française.

*Pour aller plus loin* : article 1 et annexe de l'arrêté du 9 novembre 2011 relatif aux compétences requises en vue de la délivrance de la carte professionnelle de guide-conférencier aux titulaires de licence professionnelle ou de diplôme conférant le grade de master.

#### Coûts associés à la qualification

La formation menant à la profession de guide-conférencier est payante. Pour plus d'informations, il est conseillé de se rapprocher des établissements la délivrant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de guide-conférencier dans cet État peut exercer la même activité en France, de manière temporaire et occasionnelle.

Le ressortissant devra simplement indiquer la mention du titre professionnel ou du titre de formation qu'il détient dans cet État sur les documents qu'il présentera en France aux personnes liées à l'accueil touristique ainsi qu'aux responsables de musée ou de monument historique visité.

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans l'État dans lequel le professionnel est légalement établi, il devra avoir exercé cette activité pendant au moins un an au cours des dix années précédant la prestation, dans un ou plusieurs États de l'UE ou de l'EEE.

*Pour aller plus loin* : articles L. 211-1, L. 221-3, L. 221-4 et R. 221-14 du Code du tourisme.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE établi et exerçant légalement l'activité de guide-conférencier dans l'un de ces États peut exercer la même activité en France de manière permanente.

Il pourra demander la carte professionnelle auprès du préfet (cf. infra « 5°. a. Demander une carte professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »), dès lors qu'il justifie :

- être titulaire d'un diplôme, certificat ou autre titre permettant l'exercice de l'activité à titre professionnel dans un État de l'UE ou de l'EEE et délivré par :
  - l'autorité compétente de cet État, 
  - un État tiers, fourni avec attestation d'un État de l'UE ou de l'EEE reconnaissant le titre et certifiant qu'il a exercé l'activité pendant trois années minimum ;
- être titulaire d'un titre de formation acquis dans son État d'origine, visant spécifiquement l'exercice de la profession ;
- de l'exercice de l'activité pendant au moins un an à temps plein ou temps partiel au cours des dix dernières années dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation en France, le préfet compétent peut exiger que l'intéressé se soumette à une mesure de compensation (cf. infra « 5°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles L.221-2, R. 221-12 et R. 221-13 du Code du tourisme.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Toute personne exerçant l'activité de guide-conférencier sans être titulaire de la carte professionnelle encourt une amende de troisième classe allant jusqu'à 450 euros.

*Pour aller plus loin* : article R. 221-3 du Code du tourisme et article 131-13 du Code pénal.

## 4°. Assurance

Le guide-conférencier exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demander une carte professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le préfet du département dans lequel le ressortissant souhaite s'établir est compétent pour délivrer la carte professionnelle de guide-conférencier.

#### Pièces justificatives

À l'appui de sa demande, le ressortissant devra transmettre au préfet compétent un dossier comportant les pièces justificatives suivantes :

- un formulaire de demande de carte, complété et signé ;
- un document énonçant les mentions particulières, soit linguistiques, soit scientifiques et culturelles, devant figurer sur la carte ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une ou plusieurs photos d'identité ;
- une copie du diplôme, certificat ou titre obtenu dans un État de l'UE ou de l'EEE ;
- une attestation justifiant son activité pendant au moins un an au cours des dix dernières années lorsque ni la formation ni l'exercice de la profession ne sont réglementées dans l’État de l'UE ou de l'EEE.

#### Issue de la procédure

Le préfet disposera d'un délai d'un mois à compter de la réception des éléments justificatifs pour en accuser réception ou demander l'envoi de pièces manquantes. La décision de délivrer la carte professionnelle interviendra ensuite dans un délai de deux mois.

Le silence gardé à l'expiration de ce délai vaudra décision d'octroi de la carte professionnelle.

#### Coût

Gratuit.

#### Bon à savoir : mesures de compensation

Lorsque des différences substantielles existent au niveau de la formation suivie par le ressortissant et celle exigée en France, le préfet compétent peut soumettre à l'intéressé à la mesure de compensation de son choix, à savoir :

- un stage d'adaptation dont la durée ne peut être supérieure à trois années ;
- une épreuve d'aptitude.

L'intéressé aura alors deux mois pour faire connaître la mesure de son choix.

Dans le cas où le ressortissant est titulaire d'une attestation de compétence ou d'un titre sanctionnant un cycle d'études secondaire, la décision du choix de la mesure de compensation reviendra au préfet dans un délai de deux mois.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).