﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP226" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts and culture" -->
<!-- var(title)="Dance teacher" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-and-culture" -->
<!-- var(title-short)="dance-teacher" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/arts-and-culture/dance-teacher.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="dance-teacher" -->
<!-- var(translation)="Auto" -->


Dance teacher
=============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The dance teacher is a professional in charge of teaching classical, contemporary or jazz dance. It transmits the fundamental knowledge necessary for an autonomous artistic practice of students.

Depending on the case, it provides activities of awakening, initiation and the conduct of initial learning, especially in the context of the courses leading to the certificate of choreographic studies of specialized arts educational institutions. communities.

It accompanies the development of amateur artistic practices, notably by acting as an advisor and assistance in the formulation of projects. He participates in the realization of the actions carried out by the structure that employs him and his inscription in the local cultural life.

He may also be required to intervene in pre-professional preparation or vocational training courses.

*For further information*: Article L. 362-1 of the Education Code; Appendix I of the decree of 20 July 2015 relating to the various pathways to the profession of dance teacher.

**Good to know**

The job of dance animator concerns all forms of dances outside classical, contemporary and jazz dances. He leads workshops and group sessions. It introduces audiences to dance in a process of discovery and cultural mediation that promotes both listening to a rhythm and learning from a basic step, without the question of development or validation of the skills of the practitioners. In addition, he ensures the safety of practitioners and participates in the overall project of the structure in which he practices. For more information, it is advisable to consult the[French Dance Federation website](http://ffdanse.fr/index.php/formation/cqp-animateur-danse) (FFD).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of teaching or teaching classical, contemporary or jazz dance is subject to the application of Article L. 362-1 of the Education Code. To practice as a dance teacher, the professional must hold:

- State-issued dance teacher diploma or certificate of fitness (CA) to dance teacher duties;
- a French or foreign diploma recognized as equivalent;
- an exemption granted because of the particular fame or confirmed experience in teaching dance that he can avail himself of.

In the performance of their public duties of teaching dance, agents of the State, the Paris National Opera and the Higher National Conservatories of Music as well as the agents of the local authorities are exempt from the obligation to obtain the State Diploma (DE) as a dance teacher when their special status provides for obtaining a state-issued CA.

**Please note**

People who had been teaching dance for more than three years as of July 11, 1989 may be exempt from obtaining the DANCE teacher's ED. The exemption is deemed to be acquired when no decision to the contrary has been notified to the individual at the end of a three-month period from the filing of the application.

*For further information*: Articles L. 362-1, L. 362-3 and L. 362-4 of the Education Code.

The animation of other forms of dance is not regulated. However, a Certificate of Professional Qualification (CQP) dance facilitator was created by the National Joint Employment Training Commission (CPNEF) animation in order to professionalize the speakers in dance animations. For more information, it is advisable to consult the[FFD website](http://ffdanse.fr/index.php/formation/cqp-animateur-danse).

##### Obtaining full rights from the dance teacher

Some people benefit in full of the state-issued dance teacher diploma. These are choreographic artists who have received educational training and justify a professional activity of at least three years at one of the following institutions:

- the ballet of the Paris National Opera;
- the ballets of the theatres of the meeting of the municipal opera theatres of France;
- National choreographic centres;
- companies from a Member State of the European Union (EU)* or another state party to the European Economic Area (EEA) agreement*, whose list is set at[Appendix V of the 20 July 2015 decree on the various pathways to the profession of dance teacher](http://www.esmd.fr/IMG/pdf/annexes_arrete_du_20_juillet_2015_extrait_du_bo_no_248.pdf).

To qualify as a dance teacher' DE, choreographers must produce a certificate of follow-up to educational training. This certificate is issued by the regional prefect and mentions the option in which the ED is awarded.

*For further information*: Article L. 362-1 and the following of the Education Code; Article 18 and Appendix V of the decree of 20 July 2015 relating to the various pathways to the profession of dance teacher.

#### Training

##### DANCE teacher DE

This diploma is registered in the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) and classified at level III of the certification level nomenclature.

The training is organised by the National Dance Centre (CND). For more information, it is advisable to consult the[CND website](http://www.cnd.fr/formation/diplome_etat).

It has three options: classical dance, contemporary dance and jazz dance.

This diploma can be obtained in full through the validation of experience (VAE). For more information, you can see[VAE's official website](http://www.vae.gouv.fr). In the context of an VAE, the candidate will also have to be put into a professional situation.

*For further information*: Article L. 362-1 of the Education Code; order of 20 July 2015 relating to the various pathways to the profession of dance teacher under Article L. 362-1 of the Education Code.

###### Prerogatives

The dance teacher's ED attests to the following skills:

- to have the associated knowledge necessary for the transmission of its choreographic genre. This implies:- the fundamental and specific elements of its choreographic genre,
  - anatomical and physiological knowledge of movement,
  - musical knowledge and knowledge of body rhythm,
  - develop an educational project. This means taking into account the reality of the students and the characteristics of the subject being taught;
- implement its educational project. This implies:- to build and animate a collective learning situation,
  - to conduct learning sequences in their technical and artistic dimensions,
  - to mobilize the associated knowledge,
  - to evaluate,
  - to engage in broader practices.

This diploma allows the holder to teach:

- in private dance schools or in public schools of music, dance and drama under the jurisdiction of local authorities;
- in other structures offering dance teachings, including associations, socio-cultural institutions and sports clubs;
- higher education institutions under the tutelage of the Ministry of Culture or the Ministry of Higher Education.

*For further information*: Appendix I of the Decree of 20 July 2015 on the various pathways to the profession of dance teacher under Article L. 362-1 of the Education Code.

###### Training unwinding

The training lasts at least 600 hours. It is organized into four training units (UF):

- a UF of musical training (100 hours minimum);
- a dance history teaching UF (50 hours minimum);
- an anatomy-physiology teaching UF (50 hours minimum);
- a teaching UF (400 hours minimum) with three options: classical dance, contemporary dance, jazz dance.

*For further information*: Article 10 and Appendix II of the July 20, 2015 Order on Different Pathways to the Dance Teacher profession under Article L. 362-1 of the Education Code.

###### Terms of access

Access to dance teacher ED training is conditional on the completion of a Technical Aptitude (EAT) exam with three options: classical dance, contemporary dance and jazz dance. Under certain conditions, this review may be exempted.

This exam is open to candidates who are at least 18 years old as of December 31 of the year of passage.

In order to register for the training leading to the ED of dance teacher, the person concerned must produce with the regional directorate of cultural affairs of his place of residence a file including:

- A registration application that complies with a standard form
- Two identity photographs;
- two stamped envelopes bearing the candidate's name, first name and address;
- A photocopy of the ID card
- a criminal record extract (bulletin 3) or a certificate of non-conviction issued by a competent authority of the candidate's state of origin, less than three months old;
- a certificate of non-contradictory to a dance movement practice less than three months old;
- the certificate of success of the EAT issued by the examination centre or, if necessary, the supporting documents necessary to issue the exemption from the TESTS of the EAT or to obtain the equivalencies of teaching units.

The registration file must be sent two months before the date set for entry into training.

When the file is complete, the Regional Director of Cultural Affairs issues the candidate a training booklet that mentions, if necessary, the exemption and equivalencies of teaching units granted. This booklet allows you to apply to an authorized training centre.

*For further information*: Articles 3, 7.8 and 9 of the July 20, 2015 Order on Different Pathways to the Dance Teacher profession under Article L. 362-1 of the Education Code.

##### CA to the functions of dance teacher

The CA is a graduate degree enrolled in the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) , at level I of the certification level nomenclature.

It is issued by the National Conservatory of Music and Dance (CNSMD) in Lyon. For more information, it is advisable to consult the[CNSMD website](http://www.cnsmd-lyon.fr/fr-2/les-formations/departement-de-formation-a-lenseignement-danse).

The Ca validates the general and professional knowledge and skills corresponding to the end-of-2nd cycle of graduate studies preparing for the teaching of choreographic art.

The CA can be obtained through initial training, apprenticeship, continuing vocational training or vaE.

*For further information*: Articles 1 and following of Decree No. 2016-1421 of October 20, 2016 relating to the Ca for the functions of dance teacher; Article 1 of the decree of 6 January 2017 relating to the CA for the functions of dance teacher and setting the conditions for the authorisation of higher education institutions to issue this diploma.

###### Terms of access

The person concerned must:

- Pass the entrance fee
- hold a state degree as a dance teacher, or his dispensation, or an equivalent title in the discipline concerned;
- hold a professional national diploma as a dancer in the discipline, or a diploma validating a postgraduate degree or a bachelor's degree in dance performance, in the discipline corresponding to the gender choreographic expected;
- have a bachelor's degree or a French or foreign diploma admitted in exempt or equivalency;
- justify a professional activity as a dance artist in the discipline concerned, which can be attested by 500 hours as a salaried choreographic artist, and a teaching experience lasting at least 300 Hours.

*For further information*: Article 2 of the decree of 6 January 2017 relating to the certificate of fitness for the functions of dance teacher and setting the conditions for the authorisation of higher education institutions to issue this diploma.

##### CQP dance animator

###### Training unwinding

The CQP is based on three certification units:

- Preparation of interventions: the facilitator prepares both his session and a cycle related to the structure project;
- the animation: the host welcomes the audience, supervises the group and evaluates its action;
- the inclusion in the project of the structure: the facilitator identifies himself in the organization chart of his structure and participates in the operation of its structure as well as in the preparation and conduct of a collective project.

The evaluation of the different units involves an individual interview, the production of an animation and the production of a written text of about ten pages.

###### Access terms:

- Be of age
- Hold the Level 1 Civic Prevention and Relief Certificate (PSC1);
- have the technical skills (music, movement, dance techniques practiced by the candidate) and the motivation to get involved in a professional training process;
- Be able to situate themselves in relation to the expected skills and validation arrangements in the proposed curriculum;

Finally, to define opportunities for training relief based on the diplomas or skills already acquired by the candidate, without exempting him from the evaluation tests.

For more information, it is advisable to consult the[FFD website](http://ffdanse.fr/index.php/formation/cqp-animateur-danse).

#### Costs associated with qualification

Trainings leading to the ED dance teacher, the CA and the CQP dance facilitator are paid. Their costs vary from training organization to training organization.

For more details, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

The national of a Member State of the European Union (EU) or another State party to the European Economic Area (EEA) legally established in one of these states may teach classical, contemporary or jazz dance in France on a temporary and occasionally provided that a prior declaration of activity has been sent to the Directorate General of Artistic Creation (see below "b. Make a prior declaration of activity for EU nationals engaged in temporary activity) (LPS)).

If the activity or training leading there is not regulated in the state in which it is established, the national must also justify having carried out this activity in one or more Member States or parties to the EEA agreement during the equivalent at least one full-time year in the ten years prior to the benefit.

The performance is carried out under the professional title in force in the claimant's establishment Member State where such a title exists in that State for the practice of the profession of dance teacher. This title is indicated in the official language or in one of the official languages of that state. If that professional title does not exist, it mentions its training title.

**Good to know**

If travelling, the claimant is subject to professional, regulatory or administrative rules of conduct directly related to professional qualifications such as the definition of the profession, the use of titles and misconduct. professionals who have a direct and specific link with the protection and safety of consumers as well as to the disciplinary provisions applicable in France to professionals practising the same profession.

*For further information*: Article L. 362-1-1 of the Education Code; Articles 1 to 4 of the 23 December 2008 decree on the conditions of practice of the dance teacher profession applicable to nationals of an EU Member State or another State party to the EEA agreement.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state may settle in France to work permanently as a teacher of classical, contemporary or jazz dance if he owns:

- a certificate of competency or training certificate issued by the competent authorities of a Member State or another State party to the EEA agreement which regulates access to the profession of dance teacher or its exercise, and allowing to practice legally this profession in that state;
- a training certificate issued by a third state, which has been recognised in a Member State or other State party to the EEA agreement and which has enabled it to legally practise in that state for a minimum period of three years, provided that this work experience is certified by the state in which it was acquired;
- a certificate of competency or a training certificate issued by the competent authorities of a Member State or another State party to the EEA agreement, which does not regulate the access or exercise of the profession of dance teacher and attesting to its preparation for the practice of the profession when it justifies the exercise of this activity full-time for at least one year or part-time for a total period of equivalent, in the last ten years in a Member State or party to the agreement on Eea. This justification is not required when the training leading to this profession is regulated in the Member State or party to the EEA agreement in which it has been validated.

However, if the knowledge, skills and skills acquired by the applicant during his or her work experience or lifelong learning have been properly validated by an organization for this purpose competent in a Member State or in a third country are not likely to fill, in whole or in part, substantial differences in training, the Minister responsible for culture may require that the applicant submit to compensation measures, consisting of the adjustment course or aptitude test (see below "b. Request recognition of professional qualifications for European nationals for permanent exercise (LE)").

*For further information*: Article L. 362-1-1 of the Education Code.

3°. Conditions of honorability
-----------------------------------------

The practice of teaching classical, contemporary or jazz dance in France is prohibited for persons who have been sentenced to a prison sentence without a conditional sentence of more than four months for offences:

- Rape
- Sexual Assault
- sexual assault on a minor;
- pimping.

*For further information*: Article L. 362-5 of the Education Code; Articles 222-22 to 222-33, 225-5 to 225-10 and 227-22 to 227-28 of the Penal Code.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

A national of an EU or EEA state wishing to practise as a dance teacher on a temporary and occasional basis in France must make a prior declaration.

The application must be renewed annually if the claimant plans to carry out his activity during the year concerned or in the event of a material change in his situation.

*For further information*: Articles 1 and following of the decree of 23 December 2008 on the conditions of practice of the profession of dance teacher applicable to nationals of a Member State of the European Community or another State party to the EEA agreement.

#### Competent authority

The prior declaration of activity must be addressed to the Directorate General of Artistic Creation.

#### Supporting documents

The registrant must attach to his statement a file consisting of the following documents:

- Form Cerfa 14531Completed, dated and signed;
- Copying the ID
- information about insurance coverage or other means of personal or collective protection regarding personal liability (these documents must not be more than three months old);
- a certificate certifying that the claimant is legally established in a Member State to practise as a dance teacher in one or more of the options (classical, contemporary or jazz dance) and that he does not incur any prohibition even temporary exercise;
- proof of professional qualifications in one or more of the options (classical, contemporary or jazz dance);
- where the profession of dance teacher is not regulated in the claimant's establishment member state, evidence that the claimant has been teaching for at least the equivalent of two full-time years in the ten years prior to filing the return. This justification is not required when training leading to this occupation is regulated in the EU state or EEA in which it has been validated.

#### Cost

Free.

### b. Request recognition of professional qualifications for EU nationals for permanent exercise (LE)

Nationals of an EU or EEA state who have obtained their professional qualifications in one of these states and who wish to settle in France to practise as a dance teacher, must apply for recognition of their qualifications in one or more options (classical, contemporary, jazz).

#### Competent authority

The application for recognition of professional qualifications must be addressed to the Directorate General of Artistic Creation.

#### Time

The Directorate General of Artistic Creation issues a receipt within one month after receiving the application for recognition of professional qualifications. If so, it informs them of any missing documents.

The Minister responsible for culture decides on the application by a reasoned decision within four months of receiving the full file.

After reviewing the file and if the person is recognized as qualified to serve as a dance teacher, the Minister issues him a certificate of recognition of professional qualifications.

#### Compensation measures

If the Minister believes that the knowledge acquired by the individual during his or her professional experience is not likely to fill, in whole or in part, substantial differences in training, he may propose to him to submit to an ordeal or an adjustment course in two cases:

- where it is found that the applicant's training relates to subjects substantially different from those in the dance teacher's ED program;
- where regulated training in the State of Origin does not include one or more of the options and this difference is characterized by specific training required for the dance teacher ED and on subjects substantially different from those covered by the certificate or training designation referred to.

In this case, the Minister's reasoned decision states that the individual must make his choice known within two months and sets the duration of the internship and the subjects of the aptitude test to which the person will be subjected.

On the basis of the assessment report of the internship or the result obtained in the aptitude test, the Minister of Culture decides on the application within one month and issues the person with a certificate of recognition of professional qualifications. dance teacher.

#### Supporting documents

Applicants must attach a file consisting of the Cerfa 14531 form to his application for recognition of professional qualifications.and the following supporting documents, if any, translated into French:

- A copy of the ID
- A description of the applicant's professional experience
- the content of the studies and internships taken during the initial training, indicating the number of hours per subject for the theoretical teachings, the duration of the internships, the field in which they were carried out and, if applicable, the result of the evaluations carried out and, if necessary, the record of continuing education courses indicating the content and duration of these internships. These elements are issued and attested by the training structure concerned;
- if the applicant holds a certificate of competency or a training certificate issued by the competent authorities of an EU or EEA state which regulates access to the profession or its practice and which allows the profession to be legally practised in This state, a copy of this certificate or title;
- if the applicant holds a training degree issued by a third state that has been recognised by an EU or EEA state and which has allowed the profession to be practised in that state for at least three years:- copying the recognition by an EU or EEA state of the training certificate issued by a third state,
  - a copy of the document certifying the exercise by the applicant of the profession of dance teacher for a minimum of three years in the state that has recognized the training title,
  - where the minimum period of three years has not been carried out in the state that has recognized the diploma, certificate or title, the holder must be recognized by the Minister for Culture in view of the knowledge and qualifications attested by that diploma, certificate or title and by all the training and professional experience acquired;
- if the applicant holds a certificate of competency or a training certificate issued by the competent authorities of an EU or EEA state which does not regulate access or the practice of the profession and the applicant justifies the exercise of this full-time activity for two years over the past ten years:- A copy of this certificate or title,
  - evidence by any means that the person has served as a dance teacher for at least two years full-time in the ten years prior to the application. This evidence is not required when training leading to this occupation is regulated in the EU state or EEA in which it has been validated.

When the person concerned is unable to provide these documents, he submits a recognition or certification, issued by the competent authorities of the State of origin, of the existence of these documents. In case of serious doubt, confirmation of the authenticity of the diplomas, certificates or titles issued may be requested.

#### Cost

Free.

*For further information*: Articles L. 362-1-1 and the following articles of the Education Code; Articles 5 to 11 of the order of 23 December 2008 above.

### c. If necessary, request recognition of equivalency to dance teacher ED

Recognition of equivalence is granted to holders of another degree in dance education.

The competent authority verifies that the qualification resulting from the degree held corresponds well to the level of requirement established by the dance teacher's certification repository.

The applicant must prove by all documents that there is a correspondence in terms of:

- level in dance at the time of entry into training with the level of the TEST of the EAT;
- the hourly volume and content of the teachings they followed with the hourly volume and content of the teaching units constituting the dance teacher ED.

Any piece written in a foreign language must be accompanied by a sworn translator.

#### Competent authority

The request must be addressed to the Directorate General of Artistic Creation. It issues an acknowledgement as soon as the file is complete.

The application is heard by the Inspectorate of Artistic Creation.

The granting of the exemption is pronounced by ministerial decree.

#### Time

The response to the request is notified within ten months of the date of the acknowledgement. The decision to refuse must be justified.

*For further information*: Article 25 and Appendix IV of the order of 20 July 2015 mentioned above.

### d. If necessary, request an exemption from the dance teacher's ED

An exemption may be granted because of the particular fame or confirmed experience in teaching dance.

#### Competent authority

The request must be addressed to the Directorate General of Artistic Creation. It issues an acknowledgement as soon as the file is complete.

The application is heard by the Inspectorate of Artistic Creation.

The granting of the exemption is pronounced by ministerial decree. The decision to refuse must be justified.

#### Time

The response to the request is notified within ten months of the date of the acknowledgement.

#### Special fame

The choreographic artist (performer, choreographer, assistant choreographer, rehearsalist or ballet master) must be able to justify:

- High-level training in the discipline concerned;
- Notoriety of the companies and places where it occurred;
- the breadth, diversity and uniqueness of his artistic career;
- expression of his notoriety in the media and in the professional community.

The application must be supported by significant elements such as: employment contracts, pay slips, company wafers, room programs, press clippings, audiovisual performance recordings, links to sites.

#### Confirmed experience in dance teaching

The professional must justify:

- a training course to certify the acquisition of a high level of technical mastery in the discipline concerned by the application;
- an extensive pedagogical practice:- equivalent to at least five full-time years (3,600 hours) in the ten years prior to the application,
  - conducted with diverse audiences, particularly in terms of age and technical level,
  - based on an ability to build student support in progress.

The application must be supported by supporting documents such as contracts, pay slips, diplomas, awards, educational institution programs, training sessions, certificates, letters of recommendation.

Any piece written in a foreign language must be accompanied by a sworn translator.

*For further information*: Article 25 and Appendix IV of the order of 20 July 2015 mentioned above.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

