﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP226" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arts et culture" -->
<!-- var(title)="Professeur de danse" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arts-et-culture" -->
<!-- var(title-short)="professeur-de-danse" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/arts-et-culture/professeur-de-danse.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="professeur-de-danse" -->
<!-- var(translation)="None" -->

# Professeur de danse

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

Le professeur de danse est un professionnel chargé de l'enseignement de la danse classique, contemporaine ou jazz. Il transmet les savoirs fondamentaux nécessaires à une pratique artistique autonome des élèves.

Suivant les cas, il assure des activités d'éveil, d'initiation et la conduite d'un apprentissage initial notamment dans le cadre des cursus conduisant au certificat d'études chorégraphiques des établissements d'enseignement artistique spécialisés relevant des collectivités.

Il accompagne le développement des pratiques artistiques des amateurs, notamment en tenant un rôle de conseil et d'aide à la formulation des projets. Il participe à la réalisation des actions portées par la structure qui l'emploie et à son inscription dans la vie culturelle locale.

Il peut être également amené à intervenir dans des cursus de préparation pré-professionnelle ou de formation professionnelle.

*Pour aller plus loin* : article L. 362-1 du Code de l'éducation ; annexe I de l'arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse.

**Bon à savoir**

Le métier d'animateur de danse concerne toutes les formes de danses hors danses classique, contemporaine et jazz. Il anime des ateliers et séances collectives. Il initie des publics à la danse dans une démarche de découverte et de médiation culturelle favorisant tant l'écoute d'un rythme que l'apprentissage de pas de base, sans qu'il soit question de perfectionnement, ni de validation de compétences des pratiquants. En outre, il veille à la sécurité des pratiquants et participe au projet global de la structure dans laquelle il exerce. Pour plus d'informations, il est conseillé de consulter le [site de la Fédération française de danse](http://ffdanse.fr/index.php/formation/cqp-animateur-danse) (FFD).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'exercice de l'activité d'enseignant ou professeur de danse classique, contemporaine ou jazz est soumis à l'application de l'article L. 362-1 du Code de l'éducation. Pour exercer en qualité de professeur de danse, le professionnel doit être titulaire :

- du diplôme de professeur de danse délivré par l'État ou du certificat d'aptitude (CA) aux fonctions de professeur de danse ;
- d'un diplôme français ou étranger reconnu équivalent ;
- d'une dispense accordée en raison de la renommée particulière ou de l'expérience confirmée en matière d'enseignement de la danse dont il peut se prévaloir.

Dans l'exercice de leurs fonctions publiques d'enseignement de la danse, les agents de l'État, de l'Opéra national de Paris et des conservatoires nationaux supérieurs de musique ainsi que les agents des collectivités territoriales sont dispensés de l'obligation d'obtenir le diplôme d'État (DE) de professeur de danse lorsque leurs statuts particuliers prévoient l'obtention d'un CA délivré par l'État.

**À noter**

Les personnes qui enseignaient la danse depuis plus de trois ans au 11 juillet 1989 peuvent être dispensées de l'obtention du DE de professeur de danse. La dispense est réputée acquise lorsqu'aucune décision contraire n'a été notifiée à l'intéressé à l'expiration d'un délai de trois mois à compter du dépôt de la demande.

*Pour aller plus loin* : articles L. 362-1, L. 362-3 et L. 362-4 du Code de l'éducation.

L'animation des autres formes de danses n'est pas réglementée. Toutefois, un certificat de qualification professionnelle (CQP) animateur de danse a été créé par la Commission paritaire nationale emploi formation (CPNEF) animation afin de professionnaliser les intervenants en animations dansées. Pour plus d'informations, il est conseillé de consulter le [site de la FFD](http://ffdanse.fr/index.php/formation/cqp-animateur-danse).

##### Obtention de plein droit du DE professeur de danse

Certaines personnes bénéficient de plein droit du diplôme de professeur de danse délivré par l'État. Il s'agit des artistes chorégraphiques qui ont suivi une formation pédagogique et qui justifient d'une activité professionnelle d'au moins trois ans au sein de l'un des établissements suivants :

- le ballet de l'Opéra national de Paris ;
- les ballets des théâtres de la réunion des théâtres lyriques municipaux de France ;
- les centres chorégraphiques nationaux ;
- les compagnies d'un État membre de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE), dont la liste est fixée à [l'annexe V de l'arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse](http://www.esmd.fr/IMG/pdf/annexes_arrete_du_20_juillet_2015_extrait_du_bo_no_248.pdf).

Pour bénéficier de plein droit du DE de professeur de danse, les artistes chorégraphes doivent produire une attestation de suivi d'une formation pédagogique. Cette attestation est délivrée par le préfet de région et mentionne l'option dans laquelle le DE est décerné.

*Pour aller plus loin* : article L. 362-1 et suivants du Code de l'éducation ; article 18 et annexe V de l'arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse.

#### Formation

##### DE de professeur de danse

Ce diplôme est inscrit au [RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) et classé au niveau III de la nomenclature des niveaux de certification.

La formation est organisée par le Centre national de la danse (CND). Pour plus d'informations, il est conseillé de consulter le [site du CND](http://www.cnd.fr/formation/diplome_etat).

Il comporte trois options : danse classique, danse contemporaine et danse jazz.

Ce diplôme peut être obtenu en totalité par la voie de la validation des acquis de l'expérience (VAE). Pour plus d'informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr). Dans le cadre d'une VAE, le candidat devra également faire l'objet d'une mise en situation professionnelle.

*Pour aller plus loin* : article L. 362-1 du Code de l'éducation ; arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse en application de l'article L. 362-1 du Code de l'éducation.

###### Prérogatives

Le DE de professeur de danse atteste des compétences suivantes :

- disposer des savoirs associés nécessaires à la transmission de son genre chorégraphique. Ce qui implique :
  - les éléments fondamentaux et spécifiques de son genre chorégraphique,
  - les connaissances anatomiques et physiologiques du mouvement,
  - les connaissances musicales et des savoirs en matière de rythme corporel,
  - élaborer un projet pédagogique. Ce qui implique de prendre en compte la réalité des élèves et les caractéristiques de la matière enseignée ;
- mettre en œuvre son projet pédagogique. Ce qui implique :
  - de construire et d'animer une situation d'apprentissage collectif,
  - de mener des séquences d'apprentissage dans leurs dimensions technique et artistique,
  - de mobiliser les savoirs associés,
  - d'évaluer,
  - de s'engager dans des pratiques élargies.

Ce diplôme permet à son titulaire d'enseigner :

- dans des écoles de danse privées ou au sein des établissements d'enseignement public de la musique, de la danse et de l'art dramatique relevant des collectivités territoriales ;
- dans d'autres structures proposant des enseignements en danse, notamment dans les associations, les établissements socio-culturels et les clubs sportifs ;
- dans des établissements d'enseignement supérieur sous tutelle du Ministère chargé de la culture ou du Ministère chargé de l'enseignement supérieur.

*Pour aller plus loin* : annexe I de l'arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse en application de l'article L. 362-1 du Code de l'éducation.

###### Déroulé de la formation

La formation dure au minimum 600 heures. Elle est organisée en quatre unités de formation (UF) :

- une UF de formation musicale (100 heures minimum) ;
- une UF d'enseignement d'histoire de la danse (50 heures minimum) ;
- une UF d'enseignement d'anatomie-physiologie (50 heures minimum) ;
- une UF d'enseignement de pédagogie (400 heures minimum) comportant trois options : danse classique, danse contemporaine, danse jazz.

*Pour aller plus loin* : article 10 et annexe II de l'arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse en application de l'article L. 362-1 du Code de l'éducation.

###### Conditions d'accès

L'accès à la formation au DE de professeur de danse est subordonnée à la réussite d'un examen d'aptitude technique (EAT) comportant trois options : danse classique, danse contemporaine et danse jazz. Sous certaines conditions, il est possible d'être dispensé de cet examen.

Cet examen est ouvert aux candidats âgés d'au moins dix-huit ans au 31 décembre de l'année de passage.

Pour s'inscrire à la formation menant à l'obtention du DE de professeur de danse, l'intéressé doit produire auprès de la direction régionale des affaires culturelles de son lieu de domicile un dossier comprenant :

- une demande d'inscription conforme à un formulaire type ;
- deux photographies d'identité ;
- deux enveloppes timbrées portant le nom, le prénom et l'adresse du candidat ;
- une photocopie de la carte d'identité ;
- un extrait de casier judiciaire (bulletin n° 3) ou une attestation de non-condamnation délivrée par une autorité compétente de l'État d'origine du candidat, datant de moins de trois mois ;
- un certificat de non contre-indication à une pratique du mouvement dansé datant de moins de trois mois ;
- l'attestation de réussite de l'EAT délivrée par le centre d'examen ou, le cas échéant, les pièces justificatives nécessaires à la délivrance de la dispense des épreuves de l'EAT ou à l'obtention des équivalences d'unités d'enseignement.

Le dossier d'inscription doit être envoyé deux mois avant la date fixée pour l'entrée en formation.

Lorsque le dossier est complet, le directeur régional des affaires culturelles délivre au candidat un livret de formation qui mentionne, le cas échéant, la dispense et les équivalences d'unités d'enseignement accordées. Ce livret permet de postuler auprès d'un centre de formation habilité.

*Pour aller plus loin* : articles 3, 7,8 et 9 de l'arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse en application de l'article L. 362-1 du Code de l'éducation.

##### CA aux fonctions de professeur de danse

Le CA est un diplôme de 2nd cycle d'études supérieures inscrit au [RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) , au niveau I de la nomenclature des niveaux de certification.

Il est délivré par le Conservatoire national supérieur de musique et de danse (CNSMD) de Lyon. Pour plus d'informations, il est conseillé de consulter le [site du CNSMD](http://www.cnsmd-lyon.fr/fr-2/les-formations/departement-de-formation-a-lenseignement-danse).

Le CA valide les connaissances et compétences générales et professionnelles correspondant au niveau de fin de 2nd cycle d'études supérieures préparant à l'enseignement de l'art chorégraphique.

Le CA peut être obtenu par la voie de la formation initiale, de l'apprentissage, de la formation professionnelle continue ou de la VAE.

*Pour aller plus loin* : articles 1 et suivants du décret n° 2016-1421 du 20 octobre 2016 relatif au CA aux fonctions de professeur de danse ; article 1 de l'arrêté du 6 janvier 2017 relatif au CA aux fonctions de professeur de danse et fixant les conditions d'habilitation des établissements d'enseignement supérieur à délivrer ce diplôme.

###### Conditions d'accès

L'intéressé doit :

- réussir le concours d'entrée ;
- être titulaire, dans la discipline concernée, d'un diplôme d'État de professeur de danse, ou de sa dispense, ou d'un titre équivalent ;
- être titulaire, dans la discipline concernée, d'un diplôme national supérieur professionnel de danseur, ou d'un diplôme validant un 1er cycle d'enseignement supérieur ou d'un bachelor d'interprète en danse, dans la discipline correspondant au genre chorégraphique attendu ;
- être titulaire du baccalauréat ou d'un diplôme français ou étranger admis en dispense ou en équivalence ;
- justifier d'une activité professionnelle en qualité d'artiste de la danse dans la discipline concernée, pouvant notamment être attestée par 500 heures en qualité d'artiste chorégraphique salarié, et d'une expérience d'enseignement d'une durée d'au moins 300 heures.

*Pour aller plus loin* : article 2 de l'arrêté du 6 janvier 2017 relatif au certificat d'aptitude aux fonctions de professeur de danse et fixant les conditions d'habilitation des établissements d'enseignement supérieur à délivrer ce diplôme.

##### CQP animateur de danse

###### Déroulé de la formation

Le CQP s'articule autour de trois unités de certification :

- la préparation des interventions : l'animateur prépare à la fois sa séance et un cycle en lien avec le projet de la structure ;
- la réalisation de l'animation : l'animateur accueille les publics, encadre le groupe et évalue son action ;
- l'inscription dans le projet de la structure : l'animateur se repère dans l'organigramme de sa structure et participe au fonctionnement de sa structure ainsi qu'à la préparation et au déroulement d'un projet collectif.

L'évaluation des différentes unités passe par un entretien individuel, la réalisation d'une animation et la production d'un texte écrit d'une dizaine de pages.

###### Modalités d'accès :

- être majeur ;
- être titulaire de l'attestation Prévention et secours civiques de niveau 1 (PSC1) ;
- disposer des compétences techniques (musique, mouvement, techniques de danse pratiquées par le candidat) et de la motivation à s'impliquer dans une démarche de formation professionnelle ;
- être capable de se situer au regard des compétences attendues et des modalités de validation réparties dans le cursus proposé ;

Enfin, définir des possibilités d'allègement de formation en fonction des diplômes ou des compétences déjà acquises par le candidat, sans pour autant le dispenser des épreuves d'évaluation.

Pour plus d'informations, il est conseillé de consulter le [site de la FFD](http://ffdanse.fr/index.php/formation/cqp-animateur-danse).

#### Coûts associés à la qualification

Les formations menant à l'obtention du DE professeur de danse, au CA ainsi qu'au CQP animateur de danse sont payantes. Leurs coûts varient selon les organismes de formation.

Pour plus de précisions, il est conseillé de se rapprocher de l'organisme de formation considéré.

### b. Ressortissants UE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d'un État membre de l'Union européenne (UE) ou d'un autre État partie à l'Espace économique européen (EEE) légalement établi dans un de ces États peut enseigner la danse classique, contemporaine ou jazz en France de manière temporaire et occasionnelle à la condition d'avoir adressé à la direction générale de la création artistique une déclaration préalable d'activité (cf. infra « b. Effectuer une déclaration préalable d'activité pour les ressortissants de l'UE exerçant une activité temporaire et occasionnelle (LPS) »).

Si l'activité ou la formation y conduisant n'est pas réglementée dans l'État dans lequel il est établi, le ressortissant doit également justifier avoir exercé cette activité dans un ou plusieurs États membres ou parties à l'accord sur l'EEE pendant l'équivalent d'au moins une année à temps plein au cours des dix années précédant la prestation.

La prestation est effectuée sous le titre professionnel en vigueur dans l'État membre d'établissement du prestataire lorsqu'un tel titre existe dans ledit État pour l'exercice de la profession de professeur de danse. Ce titre est indiqué dans la langue officielle ou dans l'une des langues officielles de cet État. Dans le cas où ledit titre professionnel n'existe pas, il fait mention de son titre de formation.

**Bon à savoir**

S'il se déplace, le prestataire est soumis aux règles de conduite de caractère professionnel, réglementaire ou administratif en rapport direct avec les qualifications professionnelles telles que la définition de la profession, l'usage des titres et les fautes professionnelles graves qui ont un lien direct et spécifique avec la protection et la sécurité des consommateurs ainsi qu'aux dispositions disciplinaires applicables en France aux professionnels qui y exercent la même profession.

*Pour aller plus loin* : article L. 362-1-1 du Code de l'éducation ; articles 1 à 4 de l'arrêté du 23 décembre 2008 relatif aux conditions d'exercice de la profession de professeur de danse applicables aux ressortissants d'un État membre de l'UE ou d'un autre État partie à l'accord sur l'EEE.

### c. Ressortissants UE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente l'activité de professeur de danse classique, contemporaine ou jazz s'il possède :

- une attestation de compétences ou un titre de formation délivré par les autorités compétentes d'un État membre ou d'un autre État partie à l'accord sur l'EEE qui réglemente l'accès à la profession de professeur de danse ou son exercice, et permettant d'exercer légalement cette profession dans cet État ;
- un titre de formation délivré par un État tiers, qui a été reconnu dans un État membre ou un autre État partie à l'accord sur l'EEE et qui lui a permis d'exercer légalement la profession dans cet État pendant une période minimale de trois ans, à condition que cette expérience professionnelle soit certifiée par l'État dans lequel elle a été acquise ;
- une attestation de compétences ou un titre de formation délivré par les autorités compétentes d'un État membre ou d'un autre État partie à l'accord sur l'EEE, qui ne réglemente pas l'accès ou l'exercice de la profession de professeur de danse et attestant de sa préparation à l'exercice de la profession lorsqu'il justifie de l'exercice de cette activité à temps plein pendant au moins une année ou à temps partiel pendant une durée totale équivalente, au cours des dix dernières années dans un État membre ou partie à l'accord sur l'EEE. Cette justification n'est pas requise lorsque la formation conduisant à cette profession est réglementée dans l'État membre ou partie à l'accord sur l'EEE dans lequel elle a été validée.

Toutefois, si les connaissances, aptitudes et compétences acquises par le demandeur au cours de son expérience professionnelle ou de l'apprentissage tout au long de la vie ayant fait l'objet, à cette fin, d'une validation en bonne et due forme par un organisme compétent dans un État membre ou dans un pays tiers ne sont pas de nature à combler, en tout ou partie, des différences substantielles de formation, le ministre chargé de la culture peut exiger que le demandeur se soumette à des mesures de compensation, consistant au choix, en un stage d'adaptation ou en une épreuve d'aptitude (cf. infra « b. Demander une reconnaissance des qualifications professionnelles pour les ressortissants européens en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : article L. 362-1-1 du Code de l'éducation.

## 3°. Conditions d'honorabilité

L'exercice de l'activité de professeur de danse classique, contemporaine ou jazz en France est interdit pour les personnes ayant fait l'objet d'une condamnation à une peine d'emprisonnement sans sursis supérieur à quatre mois pour les infractions :

- de viol ;
- d'agression sexuelle ;
- d'atteinte sexuelle sur un mineur ;
- de proxénétisme.

*Pour aller plus loin* : article L. 362-5 du Code de l'éducation ; articles 222-22 à 222-33, 225-5 à 225-10 et 227-22 à 227-28 du Code pénal.

## 4°. Démarches et formalités de reconnaissance de qualifications

### a. Effectuer une déclaration préalable d'activité pour les ressortissants européens exerçant une activité temporaire et occasionnelle (LPS)

Le ressortissant d'un État de l'UE ou de l'EEE souhaitant exercer en qualité de professeur de danse de manière temporaire et occasionnelle en France doit en faire la déclaration préalable.

La demande doit être renouvelée annuellement si le prestataire envisage d'exercer son activité au cours de l'année concernée ou en cas de changement matériel de sa situation.

*Pour aller plus loin* : articles 1 et suivants de l'arrêté du 23 décembre 2008 relatif aux conditions d'exercice de la profession de professeur de danse applicables aux ressortissants d'un État membre de la communauté européenne ou d'un autre État partie à l'accord sur l'EEE.

#### Autorité compétente

La déclaration préalable d'activité doit être adressée à la direction générale de la création artistique.

#### Pièces justificatives

Le déclarant doit joindre à sa déclaration un dossier constitué des pièces suivantes :

- le formulaire Cerfa 14531*01 complété, daté et signé ;
- la copie de la pièce d'identité ;
- les informations relatives aux couvertures d'assurance ou autres moyens de protection personnelle ou collective concernant la responsabilité personnelle (ces documents ne doivent pas dater de plus de trois mois) ;
- une attestation certifiant que le prestataire est légalement établi dans un État membre pour y exercer la profession de professeur de danse dans une ou plusieurs des options (danse classique, contemporaine ou jazz) et qu'il n'encourt aucune interdiction même temporaire d'exercer ;
- la preuve des qualifications professionnelles dans une ou plusieurs des options (danse classique, contemporaine ou jazz) ;
- lorsque la profession de professeur de danse n'est pas réglementée dans l'État membre d'établissement du prestataire, la preuve que ce dernier a exercé son activité d'enseignement pendant au moins l'équivalent de deux années à temps plein au cours des dix années précédant le dépôt de la déclaration. Cette justification n'est pas requise lorsque la formation conduisant à cette profession est réglementée dans l'État de l'UE ou de l'EEE dans lequel elle a été validée.

#### Coût

Gratuit.

### b. Demander une reconnaissance des qualifications professionnelles pour les ressortissants européens en vue d'un exercice permanent (LE)

Les ressortissants d'un État de l'UE ou de l'EEE ayant obtenu dans l'un de ces États leurs qualifications professionnelles et qui souhaitent s'établir en France pour exercer en tant que professeur de danse, doivent demander la reconnaissance de leurs qualifications professionnelles dans une ou plusieurs options (danse classique, contemporaine, jazz).

#### Autorité compétente

La demande de reconnaissance des qualifications professionnelles doit être adressée à la Direction générale de la création artistique.

#### Délais

La Direction générale de la création artistique délivre un récépissé dans un délai d'un mois après réception de la demande de reconnaissance des qualifications professionnelles. Le cas échéant, elle l'informe de tout document manquant.

Le ministre chargé de la culture statue sur la demande par une décision motivée dans un délai de quatre mois à compter de la réception du dossier complet.

Après examen du dossier et si l'intéressé est reconnu qualifié pour exercer les fonctions de professeur de danse, le Ministre lui délivre une attestation de reconnaissance des qualifications professionnelles.

#### Mesures de compensation

Si le ministre estime que les connaissances acquises par l'intéressé au cours de son expérience professionnelle ne sont pas de nature à combler, en tout ou partie, des différences substantielles de formation, il peut lui proposer de se soumettre à une épreuve d'aptitude ou à un stage d'adaptation dans deux cas :

- lorsqu'il est constaté que la formation du demandeur porte sur des matières substantiellement différentes de celles figurant au programme du DE de professeur de danse ;
- lorsque la formation réglementée dans l'État d'origine ne comprend pas une ou plusieurs des options et que cette différence est caractérisée par une formation spécifique requise pour le DE de professeur de danse et portant sur des matières substantiellement différentes de celles couvertes par l'attestation ou le titre de formation dont l'intéressé fait état.

Dans cette hypothèse, la décision motivée du ministre mentionne que l'intéressé doit faire connaître son choix dans un délai de deux mois et fixe la durée du stage et les matières de l'épreuve d'aptitude auxquelles sera soumis l'intéressé.

Sur la base du ou des rapports d'évaluation de stage ou du résultat obtenu à l'épreuve d'aptitude, le ministre de la culture statue sur la demande dans un délai d'un mois et délivre à l'intéressé une attestation de reconnaissance des qualifications professionnelles aux fonctions de professeur de danse.

#### Pièces justificatives

Le demandeur doit joindre à sa demande de reconnaissance de qualifications professionnelles un dossier composé du formulaire Cerfa 14531*01 et des pièces justificatives suivantes, le cas échéant, traduites en français :

- une copie de la pièce d'identité ;
- le descriptif de l'expérience professionnelle acquise par le demandeur ;
- le contenu des études et des stages suivis pendant la formation initiale, indiquant le nombre d'heures par matière pour les enseignements théoriques, la durée des stages, le domaine dans lequel ils ont été effectués ainsi que, s'il y a lieu, le résultat des évaluations réalisées et, le cas échéant, le relevé des stages de formation continue indiquant le contenu et la durée de ces stages. Ces éléments sont délivrés et attestés par la structure de formation concernée ;
- si le demandeur est titulaire d'une attestation de compétence ou d'un titre de formation délivré par les autorités compétentes d'un État de l'UE ou de l'EEE qui réglemente l'accès à la profession ou son exercice et qui permet d'exercer légalement la profession dans cet État, une copie de cette attestation ou de ce titre ;
- si le demandeur est titulaire d'un titre de formation délivré par un État tiers qui a été reconnu par un État de l'UE ou de l'EEE et qui a permis l'exercice de la profession dans cet État pendant au moins trois ans :
  - la copie de la reconnaissance par un État de l'UE ou de l'EEE du titre de formation délivré par un État tiers,
  - la copie du document certifiant de l'exercice par le demandeur de la profession de professeur de danse pendant une période minimale de trois ans dans l'État qui a reconnu le titre de formation,
  - lorsque la période minimale de trois ans n'a pas été effectuée dans l'État qui a reconnu le diplôme, certificat ou titre, le titulaire doit être reconnu qualifié par le ministre chargé de la culture au vu des connaissances et qualifications attestées par ce diplôme, certificat ou titre et par l'ensemble de la formation et de l'expérience professionnelle acquises ;
- si le demandeur est titulaire d'une attestation de compétence ou d'un titre de formation délivré par les autorités compétentes d'un État de l'UE ou de l'EEE qui ne réglemente ni l'accès ni l'exercice de la profession et que le demandeur justifie de l'exercice de cette activité à temps plein pendant deux ans au cours des dix dernières années :
  - une copie de cette attestation ou de ce titre,
  - la preuve par tout moyen que l'intéressé a exercé des fonctions de professeur de danse pendant au moins l'équivalent de deux années à temps plein au cours des dix années précédant la demande. Cette preuve n'est pas requise lorsque la formation conduisant à cette profession est réglementée dans l'État de l'UE ou de l'EEE dans lequel elle a été validée.

Lorsque l'intéressé est dans l'impossibilité matérielle de fournir ces pièces, il présente une reconnaissance ou une attestation, délivrée par les autorités compétentes de l'État d'origine, de l'existence de ces pièces. En cas de doute sérieux, il peut être demandé une confirmation de l'authenticité des diplômes, certificats ou titres délivrés.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 362-1-1 et suivants du Code de l'éducation ; articles 5 à 11 de l'arrêté du 23 décembre 2008 précité.

### c. Le cas échéant, demander la reconnaissance d'équivalence au DE de professeur de danse

La reconnaissance d'équivalence est accordée aux détenteurs d'un autre diplôme relatif à l'enseignement de la danse.

L'autorité compétente vérifie que la qualification résultant du diplôme détenu correspond bien au niveau d'exigence établi par le référentiel de certification du DE de professeur de danse.

Le demandeur doit faire la preuve par tous documents qu'il y a une correspondance en termes :

- de niveau en danse au moment de l'entrée en formation avec le niveau de l'épreuve de l'EAT ;
- de volume horaire et de contenu des enseignements qu'ils ont suivi avec le volume horaire et le contenu des unités d'enseignement constitutives du DE de professeur de danse.

Toute pièce écrite en langue étrangère doit être accompagnée de sa traduction en français par un traducteur assermenté.

#### Autorité compétente

La demande doit être adressée à la Direction générale de la création artistique. Celle-ci émet un accusé de réception dès lors que le dossier est complet.

La demande est instruite par l'Inspection de la création artistique.

L'octroi de la dispense est prononcé par arrêté ministériel.

#### Délais

La réponse à la demande est notifiée dans un délai de dix mois à compter de la date de l'accusé de réception. La décision de refus doit être motivée.

*Pour aller plus loin* : article 25 et annexe IV de l'arrêté du 20 juillet 2015 précité.

### d. Le cas échéant, demander une dispense du DE de professeur de danse

Une dispense peut être accordée en raison de la renommée particulière ou de l'expérience confirmée en matière d'enseignement de la danse.

#### Autorité compétente

La demande doit être adressée à la Direction générale de la création artistique. Celle-ci émet un accusé de réception dès lors que le dossier est complet.

La demande est instruite par l'Inspection de la création artistique.

L'octroi de la dispense est prononcé par arrêté ministériel. La décision de refus doit, quant à elle, être motivée.

#### Délais

La réponse à la demande est notifiée dans un délai de dix mois à compter de la date de l'accusé de réception.

#### Renommée particulière

L'artiste chorégraphique (interprète, chorégraphe, assistant chorégraphe, répétiteur ou maître de ballet) doit pouvoir justifier :

- une formation de haut niveau dans la discipline concernée ;
- la notoriété des compagnies et des lieux où il s'est produit ;
- l'étendue, la diversité et la singularité de son parcours artistique ;
- l'expression de sa notoriété dans les médias et auprès du milieu professionnel.

La demande doit être étayée par des éléments significatifs tels que : contrats de travail, bulletins de salaire, plaquettes de compagnie, programmes de salle, coupures de presse, enregistrements audiovisuels de prestation, liens vers des sites.

#### Expérience confirmée en matière d'enseignement de la danse

Le professionnel doit justifier :

- un parcours de formation permettant d'attester l'acquisition d'un niveau de maîtrise technique élevé dans la discipline concernée par la demande ;
- une pratique pédagogique étendue :
  - équivalant à au moins cinq ans à temps plein (soit 3 600 heures) au cours des dix années précédant la demande,
  - conduite auprès de publics diversifiées, notamment en termes d'âge et de niveau technique,
  - reposant sur une capacité à construire un accompagnement des élèves dans la progression.

La demande doit être étayée par des justificatifs probants tels que contrats, bulletins de salaire, diplômes, récompenses, programmes d'établissements d'enseignement, de sessions de formation, attestations, lettres de recommandation.

Toute pièce écrite en langue étrangère doit être accompagnée de sa traduction en français par un traducteur assermenté.

*Pour aller plus loin* : article 25 et annexe IV de l'arrêté du 20 juillet 2015 précité.

### e. Voies de recours

#### Centre d'assistance français

Le Centre ENIC-NARIC est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l'Administration nationale de chaque État membre de l'Union européenne ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L'intéressé ne peut recourir à SOLVIT que s'il établit :

- que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
- qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

À l'issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
- s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

##### Délai

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).