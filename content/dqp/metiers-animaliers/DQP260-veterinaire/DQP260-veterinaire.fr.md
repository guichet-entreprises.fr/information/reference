﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP260" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Vétérinaire" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="veterinaire" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/veterinaire.html" -->
<!-- var(last-update)="2020-04-15 17:20:57" -->
<!-- var(url-name)="veterinaire" -->
<!-- var(translation)="None" -->

# Vétérinaire

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:57<!-- end-var -->

## 1°. Définition de l’activité

Le vétérinaire est un professionnel spécialisé dans la médecine et la chirurgie des animaux.

À ce titre, il a pour mission notamment :

- de protéger et soigner les animaux ;
- de diagnostiquer les maladies physiques et comportementales, les blessures, les douleurs et les malformations des animaux ;
- de sécuriser les prescriptions de médicaments ;
- d'administrer les médicaments par voie parentérale ;
- d'assurer la sécurité sanitaire des aliments et garantir la santé publique, notamment en contribuant au contrôle de l'hygiène dans les industries agroalimentaires ;
- de préserver l’environnement ;
- de développer la recherche et la formation.

## 2°. Formation

Les études pour accéder à la profession de vétérinaire sont composées de deux cycles d'une durée totale de sept ans, qui peut être complété par un troisième cycle de spécialisation allant jusqu'à trois ans selon la spécialité suivie.

Le premier cycle d'études de deux années minimum est effectué après l'obtention du baccalauréat. L'étudiant pourra se présenter au concours d'entrée dans l'un des quatre établissements permettant d'obtenir le diplôme d’État de vétérinaire à l'issue de ce cycle.

Six concours différents sont ouverts aux étudiants selon les filières choisies lors de la formation initiale :

- le concours A destiné aux étudiants de classe préparatoire biologie, chimie, physique et sciences de la terre (BCPST) ;
- le concours A-TB destiné aux étudiants de classe préparatoire technologie et biologie, réservée aux titulaires de baccalauréats technologiques sciences et technologies de laboratoire (STL) et sciences et technologies de l’agronomie et du vivant (STAV) ;
- le concours B destiné aux étudiants inscrits en troisième année de licence dans une filière scientifique, dans les domaines liés aux sciences de la vie ;
- le concours C destiné aux étudiants titulaires d'un DUT spécialité génie biologique/génie appliquée ou issus de certaines spécialités de BTS ou de BTSA ;
- le concours D destiné aux titulaires du diplôme d’État de docteur en médecine, de docteur en pharmacie, de docteur en chirurgie dentaire ou d'un master à dominante biologique ;
- le concours E destiné aux étudiants des écoles normales supérieures de Cachan et de Lyon, admis en liste principale à la session précédente de la voie A du concours.

En France, le diplôme d’État de docteur vétérinaire est délivré à l’issue d’une formation suivie dans l'un des quatre établissements suivants et de la soutenance d’une thèse d’université :

- [École nationale vétérinaire d’Alfort (ENVA)](http://www.vet-alfort.fr/) ;
- [Institut d'enseignement supérieur et de recherche en alimentation, santé animale, sciences agronomiques et de l'environnement (VetAgro Sup)](http://www.vetagro-sup.fr/) ;
- [École nationale vétérinaire, agroalimentaire et de l'alimentation (Oniris)](http://www.oniris-nantes.fr/) ;
- [École nationale vétérinaire de Toulouse (INP-ENVT)](http://www.envt.fr/).

La formation en école nationale vétérinaire est un cursus de cinq ans composé :

- de quatre années de tronc commun pour la formation de base ;
- d'une dernière année d'approfondissement.

### Diplôme d'études fondamentales vétérinaires

La formation de base de quatre ans en école est sanctionnée par le diplôme d'études fondamentales vétérinaires (DEFV). Elle comprend huit semestres pendant lesquels l'étudiant reçoit une formation de base théorique et pratique.

Dès la troisième année, l'étudiant participe au fonctionnement des consultations cliniques. Dès la quatrième année, il est en immersion totale dans une clinique vétérinaire.

**À savoir**

L'étudiant doit effectuer un stage à l'international d’une durée minimale fixée à 4 semaines.

### Diplôme d’État

Lors de la cinquième année d'approfondissement, l'étudiant peut choisir :

- d'approfondir ses connaissances et sa pratique professionnelles en optant pour l'une des dominantes spécifiques à chaque école ;
- de poursuivre sa formation dans la recherche.

L'obtention du diplôme d’État de docteur vétérinaire est soumise à la rédaction et à la soutenance d'une thèse dont le choix du sujet est laissé à l'appréciation de l'étudiant.

Tout au long de sa cinquième année, l'étudiant doit se consacrer à la préparation d'une thèse de doctorat d'exercice vétérinaire. Le directeur de thèse est un professeur de l'une des écoles nationales. La soutenance doit intervenir au moins huit jours après le dépôt du manuscrit. Le président du jury de thèse est un professeur d’une université de médecine.

La soutenance de la thèse permet à l'étudiant d'obtenir le titre de docteur vétérinaire.

*Pour aller plus loin* : articles D. 241-1 et suivants du Code rural et de la pêche maritime.

### Formations complémentaires

Une fois diplômé d’État, le vétérinaire peut choisir un enseignement facultatif permettant d'acquérir une spécialisation dans un domaine particulier.

L'internat est une formation clinique généraliste d'une année dans une des quatre écoles nationales vétérinaires dont l'entrée se fait par voie de concours national. Le vétérinaire pourra choisir entre l'une des trois filières suivantes :

- animaux de compagnie ;
- animaux de production ;
- équidés.

Le résidanat est une formation clinique spécifique de trois ans auprès d’un docteur vétérinaire titulaire d’un diplôme complémentaire de vétérinaire spécialiste dans la spécialité dont l'admission se fait sur dossier. Elle s'accompagne d'une préparation à un concours organisé au niveau européen par les collèges européens de spécialistes.

Le certificat d’études approfondies vétérinaires (CEAV) s'obtient après une année de formation dans une ENV dont l'entrée se fait sur dossier.

Le diplôme d’études spécialisées vétérinaires (DESV) s'obtient après trois années de formation après admission faite sur dossier.

Pour en savoir plus sur les spécialités étudiées lors de l'internat, du résidanat, du CEAV ou du DESV, il est conseillé de se rapprocher des écoles nationales vétérinaires.

### Coûts associés à la formation

La formation pour exercer la profession de vétérinaire est payante. Pour plus d'informations, il est conseillé de se rapprocher de l'un des établissements de formation.

## 3 °. Qualifications professionnelles

### a. Exigences nationales

Pour exercer la profession de vétérinaire en France, l'intéressé doit :

- être titulaire du diplôme d’État de docteur vétérinaire ou d'un autre titre de formation énuméré par l'arrêté du 19 juillet 2019 (cf. infra « Bon à savoir : la reconnaissance automatique de diplôme ») ;
- être de nationalité française ou ressortissant d'un État membre de l’Union européenne (UE) ou d’un autre État partie à l'accord de l’Espace économique européen (EEE) ou suisse ;
- faire enregistrer son diplôme auprès de l'un des conseils régionaux de l’Ordre des vétérinaires préalablement à son inscription sur le tableau de l’Ordre.

*Pour aller plus loin* : articles L. 241-1 et L. 241-2 du Code rural et de la pêche maritime.

**À noter**

L'exercice illégal de la médecine ou de la chirurgie des animaux est puni de deux ans d'emprisonnement et d'une amende de 30 000 €.

*Pour aller plus loin* : articles L. 243-1 et L. 243-4 du Code rural et de la pêche maritime.

### Bon à savoir: la reconnaissance automatique des diplômes

En application de la directive n° 2005/36, de l’article L. 241-2 du Code rural et de la pêche maritime et de l’article 1er de l’arrêté du 19 juillet 2019, les ressortissants de l’UE, d’un autre État partie à l’EEE ou de la Suisse, peuvent exercer la profession de vétérinaire s’ils sont titulaires d’un des titres suivants :

- diplômes, certificats ou titres mentionnés à l'annexe de l’arrêté et délivrés, pour chaque État, postérieurement à la date éventuellement fixée par l’annexe de l’arrêté du 19 juillet 2019 ;
- diplômes, certificats ou titres délivrés par un État membre de l’UE ou un autre État partie à l’EEE qui ne répondent pas à l'ensemble des exigences de formation résultant de la législation de l'Union européenne et qui sanctionnent une formation qui a débuté avant la date de référence indiquée, pour chaque État, à l'annexe de l’arrêté s’ils sont accompagnés d’une attestation délivrée par l'autorité compétente de cet État certifiant que leurs titulaires se sont consacrés de façon effective et licite aux activités de vétérinaire pendant au moins trois années consécutives au cours des cinq années qui ont précédé la délivrance de cette attestation ;
- diplômes, certificats ou titres délivrés par l'ancienne République démocratique allemande qui ne répondent pas à l'ensemble des exigences de formation résultant de la législation de l’UE s’ils sont accompagnés d'une attestation délivrée par l'autorité compétente de l'Allemagne certifiant que leurs titulaires se sont consacrés de façon effective et licite aux activités de vétérinaire pendant au moins trois années consécutives au cours des cinq années qui ont précédé la délivrance de cette attestation ;
- diplômes, certificats ou titres délivrés par l'ancienne Tchécoslovaquie ou par la République tchèque et la Slovaquie lorsqu'ils sanctionnent une formation qui a débuté sur le territoire de l'ancienne Tchécoslovaquie, à condition que l'autorité compétente de la République tchèque ou de la Slovaquie atteste que ces diplômes, certificats ou titres ont, sur leur territoire, la même validité sur le plan juridique que les diplômes, certificats ou titres que ces États délivrent pour sanctionner une formation débutée sur leur territoire et que ces diplômes, certificats ou titres soient accompagnés d'une attestation délivrée par la même autorité certifiant que leurs titulaires se sont consacrés de façon effective et licite aux activités de vétérinaire pendant au moins trois années consécutives au cours des cinq années qui ont précédé la délivrance de cette attestation ;
- diplômes, certificats ou titres délivrés par l'ancienne Union soviétique ou par la Lituanie et la Lettonie lorsqu'ils sanctionnent une formation qui a débuté sur le territoire de l'ancienne Union soviétique, à condition que l'autorité compétente de la Lituanie ou de la Lettonie atteste que ces diplômes, certificats ou titres ont, sur leur territoire, la même validité sur le plan juridique que les diplômes, certificats ou titres que ces États délivrent pour sanctionner une formation débutée sur leur territoire et que ces diplômes, certificats ou titres soient accompagnés d'une attestation délivrée par la même autorité certifiant que leurs titulaires se sont consacrés de façon effective et licite aux activités de vétérinaire pendant au moins trois années consécutives au cours des cinq années qui ont précédé la délivrance de cette attestation ;
- diplômes, certificats ou titres délivrés par l'ancienne Union soviétique ou par l'Estonie lorsqu'ils sanctionnent une formation qui a débuté sur le territoire de l'ancienne Union soviétique, à condition que l'autorité compétente de l'Estonie atteste que ces diplômes, certificats ou titres ont, sur son territoire, la même validité sur le plan juridique que les diplômes, certificats ou titres que l'Estonie délivre pour sanctionner une formation débutée sur son territoire et que ces diplômes, certificats ou titres soient également accompagnés d'une attestation délivrée par la même autorité certifiant que leurs titulaires se sont consacrés de façon effective et licite aux activités de vétérinaire pendant au moins cinq années consécutives au cours des sept années qui ont précédé la délivrance de cette attestation ;
- titres de formation de vétérinaire délivrés par l’Estonie ou dont a formation a commencé dans cet État avant le 1er mai 2004 s’ils sont accompagnés d’une attestation déclarant que les titulaires ont effectivement et licitement exercé en Estonie les activités en cause pendant au moins cinq années consécutives au cours des sept années précédant la date de délivrance de l’attestation ;
- diplômes, certificats ou titres délivrés par l'ancienne Yougoslavie ou par la Croatie et la Slovénie lorsqu'ils sanctionnent une formation qui a débuté sur le territoire de l'ancienne Yougoslavie, à condition que l'autorité compétente de la Croatie ou de la Slovénie atteste que ces diplômes, certificats ou titres ont, sur son territoire, la même validité sur le plan juridique que les diplômes, certificats ou titres que ces États délivrent pour sanctionner une formation débutée sur leur territoire et que ces diplômes, certificats ou titres soient également accompagnés d'une attestation délivrée par la même autorité certifiant que leurs titulaires se sont consacrés de façon effective et licite aux activités de vétérinaire pendant au moins trois années consécutives au cours des cinq années qui ont précédé la délivrance de cette attestation ;
- diplômes, certificats ou titres délivrés par un État membre de l’UE ou un autre État partie à l’EEE ne répondant pas aux dénominations figurant sur la liste de l’annexe de l’arrêté du 19 juillet 2019 à condition que ces diplômes, certificats ou titres soient accompagnés d’un certificat délivré par l’autorité compétente de l’État concerné qui atteste qu’ils sont assimilés à ceux figurant sur ladite liste et qu’ils sanctionnent une formation conforme aux dispositions de la directive 2005/36 du 7 septembre 2005 ;
- diplômes, certificats ou titres n’ayant pas été délivrés par un État membre de l’UE ou un autre État partie à l’EEE dès lors qu’ils ont été reconnus par un État membre de l’UE ou un autre État partie à l’EEE et que son titulaire a acquis une expérience professionnelle de trois années au moins dans cet État et attestée par celui-ci.

*Pour aller plus loin* : article L. 241-2 du Code rural et de la pêche maritime et l'arrêté du 19 juillet 2019 fixant la liste des diplômes, certificats ou titres de vétérinaire mentionnés à l'article L. 241-2 du Code rural.

### b. Ressortissants UE ou EEE : en vue d’un exercice temporaire ou occasionnel (Libre Prestation de Services)

Les personnes physiques ressortissantes d'un des États membres de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen, ainsi que les sociétés constituées en conformité avec la législation d'un de ces États et y ayant leur siège statutaire, leur administration centrale ou leur principal établissement, qui exercent légalement leurs activités de vétérinaire dans un de ces États, autre que la France, peuvent exécuter en France à titre temporaire et occasionnel des actes professionnels. L'exécution de ces actes est toutefois subordonnée à une déclaration préalable renouvelée annuellement. Si l'urgence ne permet pas de faire cette déclaration préalablement à l'acte, elle doit être faite postérieurement dans un délai maximum de quinze jours.

Les intéressés sont tenus de respecter les règles de conduite à caractère professionnel en vigueur en France et sont soumis à la juridiction disciplinaire de l'Ordre des vétérinaires.

**À savoir**

Pour exercer à titre temporaire ou occasionnel la profession de vétérinaire, le ressortissant doit posséder les connaissances linguistiques nécessaires.

*Pour aller plus loin* : article L. 241-3 du Code rural et de la pêche maritime.

### c. Ressortissants UE ou EEE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant de l'UE ou de l'EEE souhaitant exercer à titre permanent en France relève de deux régimes distincts. Dans les deux cas, il devra posséder les connaissances linguistiques nécessaires à l'exercice de l'activité en France.

#### Le régime de reconnaissance automatique des diplômes

L'article L. 241-1 du Code rural et de la pêche maritime prévoit un régime de reconnaissance automatique en France des titres et des diplômes obtenus dans un État de l’UE ou d’un autre État de l’EEE ou de Suisse (cf. supra « 2°. a. Législation nationale »).

Il appartient au conseil régional de l’Ordre des vétérinaires compétent de vérifier la régularité des diplômes et autres titres de formation, d’en accorder la reconnaissance automatique puis de statuer sur la demande d’inscription au tableau de l’Ordre.

*Pour aller plus loin* : article L241-2 du Code rural et de la pêche maritime, et arrêté du 19 juillet 2019 fixant la liste des diplômes, certificats ou titres de vétérinaire mentionnée à l'article L. 241-2 du Code rural et de la pêche maritime.

#### Le régime dérogatoire : l'autorisation d'exercer

Pour pouvoir exercer en France, le ressortissant titulaire d'un diplôme de vétérinaire non-mentionné par l'arrêté du 19 juillet 2019 et par l'article L. 241-2 du Code rural et de la pêche maritime, doit obtenir une autorisation individuelle délivrée par le ministre chargé de l'agriculture.

Cette autorisation est soumise à la réussite à un contrôle des connaissances effectué à l’[École nationale vétérinaire, agroalimentaire et de l'alimentation (Oniris)](http://www.oniris-nantes.fr/etudes/examen-de-controle-des-connaissances/examen-de-controle-des-connaissances-pour-lexercice-du-metier-de-veterinaire-en-france/) et dont les modalités sont fixées par l'arrêté du 3 mai 2010 (cf. infra « 5°. b. Le cas échéant, demander une autorisation individuelle d'exercice »).
La demande d'autorisation d'exercice doit être adressée au ministère chargé de l'agriculture. 

#### Procédure

L'autorisation d'exercice est soumise à la vérification de l'ensemble des connaissances dont les modalités sont fixées par l'arrêté du 3 mai 2010.

L'[examen](https://www.oniris-nantes.fr/etudier-a-oniris/les-formations-veterinaires/examen-de-controle-des-connaissances-ex-concours-pays-tiers/) se compose d'une épreuve d'admissibilité sous la forme de quatre questionnaires à choix multiple (QCM) puis d'épreuves orales et pratiques.

Pour accéder aux épreuves orales et pratiques, le candidat doit obtenir une note moyenne supérieure ou égale à 10 sur 20 sur l'ensemble des QCM, et ce, sans note inférieure à 5 sur 20.

#### Pièces justificatives

L'admission à l'examen sanctionnant le contrôle des connaissances est soumis à l'envoi d'un dossier complet adressé à l'Oniris. Le dossier doit comporter les pièces justificatives suivantes :

- une fiche de renseignements dûment complétée, datée et signée du candidat ;
- une lettre de demande d'autorisation d'exercer à l'attention du ministre chargé de l'agriculture ;
- un curriculum vitae ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une copie d'un extrait du casier judiciaire (bulletin n° 3) ;
- une copie certifiée conforme du diplôme de vétérinaire et sa traduction en français par un traducteur agréé ;
- un formulaire mentionnant les disciplines choisies.

Le dossier complet doit impérativement être transmis par courrier à l'Oniris avant le 31 décembre de l'année précédant le contrôle.

#### Issue de la procédure

La réussite aux épreuves du contrôle des connaissances est rendue publique par un arrêté du ministre chargé de l'agriculture, valant autorisation d'exercer la profession de vétérinaire en France à la condition de s’inscrire auprès de l’Ordre (cf. supra « 5°. b. Demander son inscription à l’Ordre des vétérinaires »).

*Pour aller plus loin* : articles R. 241-25 et R. 241-26 du Code rural et de la pêche maritime.

#### Coût

Les frais d'inscription à l'examen sont fixés à 250 euros et à régler à l'agent comptable de l'Oniris avant le 31 décembre de l'année précédant le contrôle.

## 4°. Conditions d’honorabilité, règles déontologiques, éthique

Le vétérinaire inscrit au tableau de l’Ordre ou déclaré en LPS, doit respecter les règles de conduite à caractère professionnel en vigueur en France. Il est également soumis à la juridiction disciplinaire de l'Ordre des vétérinaires.

### a. Respect du Code de déontologie des vétérinaires

Les dispositions du Code de déontologie s’imposent aux vétérinaires exerçant en France, qu’ils soient inscrits au tableau de l’Ordre ou qu’ils exercent à titre temporaire et occasionnel.

À ce titre, le vétérinaire doit notamment respecter les principes de moralité, de probité et de dévouement indispensables à l’exercice de l'activité. Il est également soumis au secret médical, doit exercer en toute indépendance, et ne doit pas prescrire des médicaments à l'intention des humains, même sur prescription d'un médecin.

*Pour aller plus loin* : article R. 242-32 et suivants du Code rural et de la pêche maritime.

### b. Développement professionnel continu

La formation professionnelle continue permet aux vétérinaires d'actualiser leurs connaissances au cours de leur carrière. C’est une obligation déontologique dont le défaut peut être sanctionné par les chambres de discipline.

Pour plus d'informations au sujet des formations professionnelles continues, il est conseillé de se rapprocher de la commission d’exercice professionnel de l’Ordre des vétérinaires et des écoles nationales vétérinaires.

### c. Cumul d'activité

Le vétérinaire peut exercer une autre activité professionnelle si elle est compatible avec la réglementation d’une part avec les principes d’indépendance et de dignité professionnelles qui s’imposent à lui.

Le cas échéant, il ne devra pas mettre en conflit les intérêts de son activité, autre que celle de vétérinaire, avec les devoirs déontologiques incombant à la profession de vétérinaire

De la même manière, il est interdit à un vétérinaire qui remplit un mandat électif ou une fonction administrative d’en user pour accroître sa clientèle.

*Pour aller plus loin* : articles R. 242-33 du Code rural et de la pêche maritime.

## 5°. Législation sociale et assurance

### a. Obligation de souscrire une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, le docteur vétérinaire doit souscrire une assurance de responsabilité civile professionnelle.

*Pour aller plus loin* : article R. 242-48 VI du Code rural et de la pêche maritime.

### b. Inscription à la caisse autonome de retraites et de prévoyance des vétérinaires

Les vétérinaires exerçant à titre libéral, que ce soit sous le statut de collaborateur libéral ou de gérant d'une société d'exercice, doivent s'affilier à la caisse autonome de retraite et de prévoyance des vétérinaires (CARPV) et remplir le [formulaire de souscription](http://www.carpv.fr/wp-content/uploads/2017/03/formulaire-Retraite-RP-2017.pdf).

## 6°. Démarches et formalités

### a. Demander son inscription à l’Ordre des vétérinaires

L'inscription à l’Ordre des vétérinaires est obligatoire pour rendre licite l'exercice de la profession.

#### Autorité compétente

La demande d’inscription est adressée au président du conseil régional de l’Ordre des vétérinaires de la région où l’intéressé souhaite établir sa résidence professionnelle.

#### Procédure

Elle peut être directement déposée au conseil régional de l’Ordre concerné ou lui être envoyé par courrier recommandé avec avis de réception.

#### Pièces justificatives

La [demande d'inscription](https://www.veterinaire.fr/la-profession/le-metier-veterinaire/linscription-a-lordre.html) au tableau de l'Ordre des vétérinaires est accompagnée d'un dossier de pièces justificatives comportant :

1. la présentation de l'original ou la production ou l'envoi d'une photocopie lisible d'un passeport ou d'une carte nationale d'identité en cours de validité ;
2. la copie du diplôme d'État de docteur vétérinaire ou d'un diplôme, certificat ou titre de vétérinaire mentionnés à l'article L. 241-2, ainsi que, pour les vétérinaires mentionnés à l'article L. 241-2-1, de l'arrêté ministériel les habilitant à exercer en France ;
3. un extrait de casier judiciaire datant de moins de trois mois, remplacé ou complété, pour les vétérinaires originaires de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen, par une attestation délivrée depuis moins de trois mois par l'autorité compétente de l'État membre d'origine ou de provenance, certifiant que sont remplies les conditions de moralité et d'honorabilité exigées dans cet État pour l'accès aux activités de vétérinaire ;
4. une déclaration manuscrite rédigée en langue française par laquelle, sous la foi du serment, l'intéressé déclare avoir eu connaissance du Code de déontologie vétérinaire et s'engage à exercer sa profession avec conscience, honneur et probité ;
5. si le vétérinaire entend exercer sa profession en partage d'activité, une copie du contrat écrit concernant ce partage d'activité ;
6. le cas échéant, une copie du contrat établi entre le vétérinaire et son employeur ;
7. un justificatif de domicile professionnel administratif ;
8. pour l'exercice en qualité de vétérinaire responsable, ou de vétérinaire responsable intérimaire d'une entreprise mentionnée à l'article L. 5142-1 du Code de la santé publique, de vétérinaire délégué ou de vétérinaire délégué intérimaire d'un établissement de ces mêmes entreprises, la copie du contrat liant le vétérinaire à l'entreprise ou à l'établissement, elle-même accompagnée :

a. de la justification que l'intéressé satisfait aux conditions d'exercice prévues, selon le cas, aux articles R. 5142-16 à R. 5142-18 ou à l'article R. 5145-14 du Code de la santé publique,
b. de la copie de l'acte de l'organe social compétent de l'entreprise portant désignation de l'intéressé et fixant ses attributions ;

9. pour l'exercice en qualité de vétérinaire adjoint dans une entreprise ou un établissement mentionné à l'article L. 5142-1 du Code de la santé publique, toute pièce indiquant la nature, les conditions et les modalités d'exercice de cette activité ;
10. pour l'exercice en qualité de vétérinaire lié par la convention prévue aux articles R. 5142-54 et R. 5142-60 du Code de la santé publique à une entreprise dont dépend un établissement fabriquant, important ou distribuant des aliments médicamenteux, la copie de la convention liant le vétérinaire à l'entreprise.

Tous les documents produits à l'appui de la demande d'inscription sont accompagnés, s'ils ne sont pas rédigés en français, d'une traduction certifiée par un traducteur assermenté ou habilité à intervenir auprès des autorités judiciaires ou administratives d'un autre État membre de l'Union européenne, d'un État partie à l'accord sur l'Espace économique européen ou de la Confédération suisse.

Il peut être exigé du vétérinaire qui sollicite son inscription de rendre préalablement visite à un membre du conseil régional de l'Ordre spécialement désigné par le président ou le secrétaire général.

Il peut être également exigé du vétérinaire qu'il fournisse tous éléments de nature à établir qu'il possède les connaissances linguistiques nécessaires à l'exercice de la profession de vétérinaire.

#### Délai

Le silence gardé par le conseil régional de l'Ordre des vétérinaires sur une demande d'inscription au tableau de l'Ordre, formulée par une personne physique ou une société, mentionnée respectivement aux articles R. 242-85 et R. 242-86, vaut décision de rejet.

#### Issue de la procédure

Lorsque l’Ordre a validé l'inscription, le ressortissant est habilité à exercer sa profession en France.

#### Voie de recours

En cas de refus d’inscription, un recours peut être déposé auprès du Conseil national de l’Ordre dans un délai de deux mois à compter de la notification du refus d‘inscription.

*Pour aller plus loin* : articles R. 242-85 et suivants du Code rural et de la pêche maritime.

### b. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

Tout ressortissant de l’UE ou de l’EEE qui est établi et exerce légalement les activités de vétérinaire dans l’un de ces États peut exercer en France de manière temporaire ou occasionnelle s’il en fait la déclaration préalable (cf. supra 2°. b. « Ressortissants UE et EEE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services) »).

#### Autorité compétente

La déclaration doit être adressée par courrier ou par email, avant la première prestation de services, au [Conseil national de l’Ordre national des vétérinaires](https://www.veterinaire.fr/).

#### Renouvellement de la déclaration préalable

La déclaration préalable doit être renouvelée tous les ans et en cas de changement de situation professionnelle.

#### Pièces justificatives

La déclaration préalable doit être accompagnée d'un dossier complet comportant les pièces justificatives suivantes :

- une copie d’une pièce d’identité en cours de validité ou d’un document attestant la nationalité du demandeur ;
- le [formulaire de déclaration préalable de prestation de services](https://www.veterinaire.fr/la-profession/le-metier-veterinaire/les-conditions-dexercice-en-france/la-libre-prestation-de-service-lps.html), complété, daté et signé ;
- une attestation de l’autorité compétente de l’État d’établissement de l’UE ou de l’EEE certifiant que l’intéressé est légalement établi dans cet État et qu’il n’encourt aucune interdiction d’exercer, accompagnée, le cas échéant, d’une traduction en français établie par un traducteur agréé ;
- une preuve des qualifications professionnelles.

Le Conseil national de l’Ordre accuse réception de la déclaration annuelle d’exercice occasionnel et temporaire sur le territoire français dans un délai d’un mois.

#### Coût

Gratuit

#### L’inscription au tableau des sociétés d’exercice vétérinaire

Les vétérinaires exerçant dans le cadre d'une société d'exercice (art L241-17 CRPM) doivent demander leur inscription à l’Ordre des vétérinaires.

La demande d'inscription doit être présentée collectivement par les associés. Elle est adressée au conseil régional de l’Ordre du siège social de la société par lettre recommandée avec demande d'avis de réception, accompagnée des pièces suivantes :

- un exemplaire des statuts et, le cas échéant, une expédition ou une copie de l'acte constitutif ;
- un certificat d'inscription au tableau de chaque associé ;
- une attestation du greffier de la juridiction ayant statué sur le siège social de la société, constatant le dépôt au greffe de la demande et des pièces ayant servi à son immatriculation au registre du commerce et des sociétés.

**À noter**

Le conseil régional de l’Ordre se prononce dans les mêmes conditions que celles mentionnées au paragraphe « 5°. b. Demander son inscription à l’Ordre des vétérinaires ».

*Pour aller plus loin* : articles L. 241-17, R. 241-29 à R. 241-32 du Code rural et de la pêche maritime.

### c. Voies de recours

#### SOLVIT

SOLVIT est un service fourni par l’administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).