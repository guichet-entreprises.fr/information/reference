﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP260" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Veterinary surgeon" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="veterinary-surgeon" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/veterinary-surgeon.html" -->
<!-- var(last-update)="2020-04-15 17:20:57" -->
<!-- var(url-name)="veterinary-surgeon" -->
<!-- var(translation)="Auto" -->


Veterinary surgeon
==========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:57<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The veterinarian is a professional specializing in animal medicine and surgery.

Its mission includes:

- Protect and care for animals
- Diagnose physical and behavioural illnesses, injuries, pain and malformations of animals;
- Secure prescriptions for medicines
- administering medications by parenteral means;
- ensure food safety and public health, including by contributing to hygiene control in the agri-food industries;
- To preserve the environment
- to develop research and training.

2°. Training
---------------------

Studies to enter the veterinary profession consist of two cycles with a total duration of seven years, which can be completed by a third cycle of specialization of up to three years depending on the specialty followed.

The first cycle of studies of at least two years is carried out after obtaining a bachelor's degree. The student will be able to enter the entrance competition at one of the four institutions that will obtain the state diploma of veterinary at the end of this cycle.

Six different competitions are open to students depending on the courses chosen during the initial training:

- The A competition for students in the preparatory class biology, chemistry, physics and earth sciences (BCPST);
- the A-TB competition for students in the technology and biology preparatory class, reserved for holders of bachelor of technology and laboratory technologies (STL) and agronomy and life sciences and technologies (STAV);
- Competition B for students enrolled in the third year of their bachelor's degree in a scientific course, in the fields related to life sciences;
- The C competition for students holding a SPECIALised biological/applied engineering DUT or from certain BTS or BTSA specialties;
- Competition D for holders of the state diploma of doctor of medicine, doctor of pharmacy, doctor of dental surgery or master's degree predominantly biological;
- the E competition for students from the higher normal schools of Cachan and Lyon, admitted as the main list at the previous session of Track A of the competition.

In France, the state diploma of veterinary doctor is issued after a training course at one of the following four institutions and the defense of a university thesis:

- [Alfort National Veterinary School (ENVA)](http://www.vet-alfort.fr/) ;
- [Institute of Higher Education and Research in Food, Animal Health, Agricultural and Environmental Sciences (VetAgro Sup)](http://www.vetagro-sup.fr/) ;
- [National Veterinary, Food and Food School (Oniris)](http://www.oniris-nantes.fr/) ;
- [Toulouse National Veterinary School (INP-ENVT)](http://www.envt.fr/).

National Veterinary School training is a five-year course consisting of:

- four years of common core for basic training;
- last year of deepening.

#### Diploma in Basic Veterinary Studies

The basic training of four years in school is sanctioned by the diploma of Basic Veterinary Studies (DEFV). It consists of eight semesters during which the student receives basic theoretical and practical training.

From the third year, the student participates in the operation of clinical consultations. From the fourth year, he is in total immersion in a veterinary clinic.

**What to know**

The student must complete an international internship with a minimum duration of 4 weeks.

#### State diploma

In the fifth year of the study, the student can choose:

- deepen your professional knowledge and practice by opting for one of the specific dominants in each school;
- continue his training in research.

The completion of the state diploma of veterinary doctor is subject to the drafting and defense of a thesis whose choice of subject is left to the discretion of the student.

Throughout the fifth year, the student must devote himself to the preparation of a doctoral thesis of veterinary practice. The thesis director is a professor at one of the national schools. The defence must take place at least eight days after the manuscript is filed. The chair of the thesis jury is a professor at a medical university.

The thesis support allows the student to obtain the title of veterinary doctor.

*For further information*: Articles D. 241-1 and the following of the Rural Code and Marine Fisheries.

#### Additional training

Once a state graduate, the veterinarian can choose an optional education to acquire a specialization in a particular field.

The boarding school is a one-year general clinical training course in one of the four national veterinary schools that are entered through national competitions. The veterinarian will be able to choose between one of three channels:

- pets;
- Production animals;
- Equidae.

The patient is a specific three-year clinical training with a veterinary doctor with a complementary diploma as a specialist veterinarian in the specialty whose admission is on file. It is accompanied by preparation for a competition organised at European level by European specialist colleges.

The Certificate of Veterinary In-depth Studies (CEAV) is obtained after one year of training in an ENV whose entry is on file.

The Diploma of Specialized Veterinary Studies (DESV) is obtained after three years of training after admission on file.

To learn more about the specialties studied at the boarding school, the boarding school, the CEAV or the DESV, it is advisable to get closer to the national veterinary schools.

#### Costs associated with training

Training to practice as a veterinarian pays off. For more information, it is advisable to get close to one of the training establishments.

Three of us. Professional qualifications
----------------------------------------

### a. National requirements

To practise as a veterinarian in France, the person concerned must:

- hold the state diploma of veterinary doctor or another training title listed by the[decree of 19 July 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038831087&categorieLien=id) (see below "Good to know: automatic diploma recognition");
- Be a French national or a national of a Member State of the European Union (EU) or another State party to the Agreement of the European Economic Area (EEA) or Switzerland;
- register your diploma with one of the regional councils of the College of Veterinarians prior to its inclusion on the Order's board.

*For further information*: Articles L. 241-1 and L. 241-2 of the Rural Code and Marine Fisheries.

**Please note**

The illegal practice of animal medicine or surgery is punishable by two years' imprisonment and a fine of 30,000 euros.

*For further information*: Articles L. 243-1 and L. 243-4 of the Rural Code and Marine Fisheries.

**Good to know: automatic recognition of diplomas**

Under Directive 2005/36, Article L. 241-2 of the Rural and Maritime Fisheries Code and Article 1 of the 19 July 2019 Decree, NATIONALs of the EU, another EEA member state or Switzerland may practise as a veterinarian if they hold one of the following titles:

- diplomas, certificates or titles mentioned in the annex of the decree and issued, for each state, after the date possibly set by the annex of the decree of 19 July 2019;
- diplomas, certificates or titles issued by an EU Member State or another EEA Member State that do not meet all the training requirements resulting from EU legislation and which sanction training that started before the reference date indicated, for each State, to the annex of the decree if they are accompanied by a certificate issued by the competent authority of that State certifying that their holders have devoted themselves effectively and lawfully to the activities of veterinarian for at least three consecutive years in the five years prior to the issuance of this certificate;
- diplomas, certificates or titles issued by the former German Democratic Republic that do not meet all training requirements resulting from EU legislation if accompanied by a certificate issued by the Competent Germany certifying that their holders have effectively and lawfully devoted themselves to veterinary activities for at least three consecutive years in the five years prior to the issuance of this certificate;
- diplomas, certificates or titles issued by the former Czechoslovakia or by the Czech Republic and Slovakia when they sanction training that began in the territory of the former Czechoslovakia, provided that the competent authority of the Czech Republic or Slovakia attests that these diplomas, certificates or titles have, on their territory, the same legal validity as the diplomas, certificates or titles that these States issue to sanction training started on their and that these diplomas, certificates or titles be accompanied by a certificate issued by the same authority certifying that their holders have devoted themselves effectively and lawfully to veterinary activities for at least three years. consecutive in the five years prior to the issuance of this certificate;
- diplomas, certificates or titles issued by the former Soviet Union or by Lithuania and Latvia when they sanction training that began on the territory of the former Soviet Union, provided that the competent authority of Lithuania or Latvia attests that these diplomas, certificates or titles have, on their territory, the same legal validity as the diplomas, certificates or titles that these States issue to sanction training begun on their territory and that these diplomas, certificates or titles are accompanied by a certificate issued by the same authority certifying that their holders have effectively and lawfully devoted themselves to veterinary activities for at least three consecutive years during the five years prior to the issuance of this certificate;
- diplomas, certificates or titles issued by the former Soviet Union or Estonia when sanctioning training that began in the territory of the former Soviet Union, provided that Estonia's competent authority certifies that these diplomas, certificates or titles have the same legal validity on its territory as the diplomas, certificates or titles that Estonia issues to sanction training begun on its territory and that these diplomas, certificates or titles are also accompanied by a certificate issued by the same authority certifying that their holders have effectively and lawfully devoted themselves to veterinary activities for at least five consecutive years in the seven years that have preceded the issuance of this certificate;
- training certificates issued by Estonia or whose training began in that state before 1 May 2004 if they are accompanied by a certificate stating that the holders have in fact and lawfully carried out the activities in Estonia cause for at least five consecutive years in the seven years prior to the date of the certification;
- diplomas, certificates or titles issued by the former Yugoslavia or Croatia and Slovenia when sanctioning training that began on the territory of the former Yugoslavia, provided that the competent authority of Croatia or Slovenia certifies that these diplomas, certificates or titles have, on its territory, the same legal validity as the diplomas, certificates or titles that these States issue to sanction training begun on their territory and that these diplomas, certificates or titles are also accompanied by a certificate issued by the same authority certifying that their holders have effectively and lawfully devoted themselves to veterinary activities for at least three consecutive years during the five years prior to the issuance of this certificate;
- diplomas, certificates or titles issued by an EU Member State or another State party to the EEA that do not meet the names on the list of the annex to the decree of 19 July 2019 provided that these diplomas, certificates or titles are accompanied by a certificate issued by the competent authority of the State concerned which certifies that they are likened to those on that list and that they sanction training in accordance with the provisions of Directive 2005/36 of 7 September 2005;
- diplomas, certificates or titles that have not been issued by an EU Member State or another State party to the EEA as long as they have been recognised by an EU Member State or another State party to the EEA and the holder has acquired professional experience of at least three years in that state and attested by it.

*For further information*: :[Article L. 241-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071367&idArticle=LEGIARTI000006582837&dateTexte=&categorieLien=cid) of the Rural and Maritime Fisheries Code and the decree of 19 July 2019 setting out the list of diplomas, certificates or veterinary titles mentioned in Article L. 241-2 of the Rural Code.

### b. EU or EEA nationals: for temporary or casual exercise (free provision of services)

Individuals who are nationals of one of the Member States of the European Union or another State party to the Agreement on the European Economic Area, as well as companies incorporated in accordance with the legislation of one of these states and based there their statutory office or principal institution, which legally carry out their veterinary activities in one of these states, other than France, may perform professional acts in France on a temporary and casual basis. The execution of these acts is, however, subject to a pre-declaration renewed annually. If the emergency does not permit this declaration to be made prior to the act, it must be made at a later date within a maximum of fifteen days.

Those concerned are obliged to respect the rules of professional conduct in force in France and are subject to the disciplinary jurisdiction of the College of Veterinarians.

**What to know**

In order to practice the veterinary profession on a temporary or casual basis, the national must have the necessary language skills.

*For further information*: Article L. 241-3 of the Rural Code and Marine Fisheries.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

An EU or EEA national wishing to work permanently in France falls under two separate regimes. In both cases, he must have the necessary language skills to carry out the activity in France.

#### The automatic recognition scheme for diplomas

Article L. 241-1 of the Rural and Maritime Fisheries Code provides for a regime of automatic recognition in France of titles and diplomas obtained in an EU state or another EEA or Swiss state (see supra "2." a. National Legislation").

It is up to the regional council of the order of veterinarians to check the regularity of diplomas and other training titles, to grant automatic recognition and then to decide on the application for inclusion on the Order's list.

*For further information*: Article L241-2 of the Rural and Marine Fisheries Code, and ordered from 19 July 2019 setting out the list of veterinary diplomas, certificates or titles referred to in Article L. 241-2 of the Rural Code and Marine Fisheries.

#### The derogatory regime: the authorisation to practice

In order to practise in France, a national with a veterinary degree not mentioned by the decree of 19 July 2019 and Article L. 241-2 of the Rural Code and Maritime Fisheries must obtain an individual authorisation issued by the Minister responsible for agriculture.

This authorization is subject to a successful knowledge check carried out at the[National Veterinary, Food and Food School (Oniris)](http://www.oniris-nantes.fr/etudes/examen-de-controle-des-connaissances/examen-de-controle-des-connaissances-pour-lexercice-du-metier-de-veterinaire-en-france/) and the terms of which are set by the decree of 3 May 2010 (see infra "5°. b. If necessary, seek an individual authorization to exercise").
The application for leave to practice must be sent to the Ministry of Agriculture.

#### Procedure

The authorisation to exercise is subject to the verification of all knowledge, the terms of which are set out in the order of 3 May 2010.

The[Review](https://www.oniris-nantes.fr/etudier-a-oniris/les-formations-veterinaires/examen-de-controle-des-connaissances-ex-concours-pays-tiers/) consists of an eligibility test in the form of four multiple-choice questionnaires (MQCs) and oral and practical tests.

To access oral and practical tests, the candidate must obtain an average score of 10 out of 20 on all MQs, with no score of less than 5 out of 20.

#### Supporting documents

Admission to the examination sanctioning the knowledge check is subject to the sending of a complete file addressed to the Oniris. The file must include the following supporting documents:

- A completed, dated and signed fact sheet of the candidate;
- A letter of request for authorisation to practise for the Minister for Agriculture;
- A resume
- A photocopy of a valid ID
- A copy of an extract from the criminal record (bulletin 3);
- A certified copy of the veterinary degree and its translation into French by a certified translator;
- a form mentioning the disciplines chosen.

The complete file must be mailed to Oniris by 31 December of the year prior to the inspection.

#### Outcome of the procedure

The success of the knowledge control tests is made public by an order of the Minister responsible for agriculture, worth the authorization to practice the profession of veterinary in France on the condition of registering with the Order (see supra "5." b. Apply for registration with the College of Veterinarians").

*For further information*: Articles R. 241-25 and R. 241-26 of the Rural and Marine Fisheries Code.

#### Cost

The registration fee for the examination is set at 250 euros and must be paid to the Accountant of the Oniris before 31 December of the year preceding the inspection.

4°. Conditions of honorability, ethical rules, ethics
-------------------------------------------------------------------------

The veterinarian registered on the order's list or declared in LPS, must respect the rules of professional conduct in force in France. It is also subject to the disciplinary jurisdiction of the College of Veterinarians.

### a. Compliance with the Veterinary Code of Conduct

The provisions of the Code of Ethics are required for veterinarians practising in France, whether they are on the Order's board or who are practising on a temporary and casual basis.

As such, the veterinarian must respect the principles of morality, probity and dedication essential to the exercise of the activity. He is also subject to medical secrecy, must exercise independently, and must not prescribe drugs for humans, even on the prescription of a doctor.

*For further information*: Article R. 242-32 and the following of the Rural code and marine fisheries.

### b. Continuous professional development

Continuing professional training allows veterinarians to update their knowledge during their careers. It is an ethical obligation, the defect of which can be sanctioned by the disciplinary chambers.

For more information about continuing professional training, it is advisable to get closer to the Professional Practice Commission of the College of Veterinarians and National Veterinary Schools.

### c. Cumulative activity

The veterinarian may engage in another professional activity if it is compatible with the regulations on the one hand with the principles of professional independence and dignity imposed on him.

If so, he should not conflict the interests of his activity, other than that of a veterinarian, with the ethical duties of the veterinary profession.

Similarly, a veterinarian who fulfils an elective or administrative mandate is prohibited from using it to increase his or her clientele.

*For further information*: Articles R. 242-33 of the Rural Code and Marine Fisheries.

5°. Social legislation and insurance
----------------------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, the veterinary doctor must take out professional liability insurance.

*For further information*: Article R. 242-48 VI of the Rural Code and Marine Fisheries.

### b. Enrollment in the self-sustaining pension and pension fund of veterinarians

Veterinarians practising on a liberal basis, whether under the status of a liberal collaborator or manager of an exercise company, must join the independent veterinary pension and pension fund (CARPV) and complete the[subscription form](http://www.carpv.fr/wp-content/uploads/2017/03/formulaire-Retraite-RP-2017.pdf).

Six degrees. Procedures and formalities
---------------------------------------

### a. Apply for registration with the College of Veterinarians

Registration for the College of Veterinarians is mandatory to make the practice of the profession lawful.

#### Competent authority

The application for registration is addressed to the President of the Regional Council of the College of Veterinarians in the region where the person wishes to establish his professional residence.

#### Procedure

It can be filed directly with the regional council of the Order concerned or sent to it by registered mail with notice of receipt.

#### Supporting documents

The[application for registration](https://www.veterinaire.fr/la-profession/le-metier-veterinaire/linscription-a-lordre.html) on the College of Veterinarians' Table is accompanied by a file of supporting documents including:

# Presenting the original or producing or sending a readable photocopy of a valid passport or national ID card;
# a copy of the state diploma of veterinary doctor or diploma, certificate or veterinary title mentioned in Article L. 241-2, as well as, for veterinarians mentioned in Article L. 241-2-1, the ministerial decree authorizing them to practise in France;
# a criminal record extract dating back less than three months, replaced or completed, for veterinarians from the European Union or another State party to the European Economic Area agreement, by a certificate issued for less than three month by the competent authority of the Member State of origin or origin, certifying that the moral and honourability requirements required in that State for access to veterinary activities are met;
# a handwritten statement written in the French language in which, under oath, he declares that he has knowledge of the Code of Veterinary Ethics and undertakes to practice his profession with conscience, honour and probity;
# If the veterinarian intends to practice his profession in activity-sharing, a copy of the written contract concerning this sharing of activity;
# If so, a copy of the contract between the veterinarian and his employer;
# A proof of administrative professional residence;
# for the exercise as a responsible veterinarian, or as an acting veterinarian in charge of a company referred to in Article L. 5142-1 of the Public Health Code, as a delegated veterinarian or as acting veterinarian of a these same companies, the copy of the contract linking the veterinarian to the company or establishment, itself accompanied by:

(a) the justification for the individual to meet the conditions of practice provided, as appropriate, for sections R. 5142-16 at R. 5142-18 or Section R. 5145-14 of the Public Health Code,
b. a copy of the act of the competent social body of the company naming the person concerned and setting its remit;

# for the exercise as an assistant veterinarian in a company or establishment referred to in Article L. 5142-1 of the Public Health Code, any evidence indicating the nature, conditions and procedures of this activity;
# for the exercise as a veterinarian bound by the convention under Sections R. 5142-54 and R. 5142-60 of the Public Health Code to a company on which a facility manufacturing, importing or distributing medicinal food depends, the copy of the agreement between the veterinarian and the company.

All documents produced in support of the application for registration are accompanied, if not written in French, by a translation certified by a sworn translator or authorized to intervene with the judicial or administrative authorities of a another member state of the European Union, from a state party to the European Economic Area agreement or from the Swiss Confederation.

Veterinarians applying for registration may be required to visit a member of the Order's regional council specially appointed by the President or Secretary General.

The veterinarian may also be required to provide any evidence that he or she has the language skills necessary to practice the veterinary profession.

#### Time

The silence maintained by the Regional Council of the College of Veterinarians on an application for inclusion on the Order's board, made by a natural person or a company, mentioned respectively in articles R. 242-85 and R. 242-86, is earned the decision to dismiss.

#### Outcome of the procedure

Once the Order has validated the registration, the national is entitled to practice his profession in France.

#### Remedy

In the event of a refusal to register, an appeal can be lodged with the National Council of the Order within two months of notification of the refusal to register.

*For further information*: Articles R. 242-85 and the following of the Rural Code and Marine Fisheries.

### b. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally practises veterinary activities in one of these states may exercise in France on a temporary or occasional basis if he makes the prior declaration (see supra 2. b. "EU nationals and EEA: for temporary and casual (Freedom to provide services)) exercise).

#### Competent authority

The declaration must be sent by mail or email, prior to the first provision of services, to the[National Council of the National Order of Veterinarians](https://www.veterinaire.fr/).

#### Renewal of pre-declaration

The prior declaration must be renewed every year and in case of a change in employment status.

#### Supporting documents

The pre-declaration must be accompanied by a complete file with the following supporting documents:

- A copy of a valid piece of identification or a document attesting to the applicant's nationality;
- The[Advance Service Delivery Reporting Form](https://www.veterinaire.fr/la-profession/le-metier-veterinaire/les-conditions-dexercice-en-france/la-libre-prestation-de-service-lps.html), completed, dated and signed;
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and that he is not prohibited from practising, accompanied, if necessary, by a french translation Established by a certified translator
- proof of professional qualifications.

The National Council of the Order acknowledges receipt of the annual declaration of occasional and temporary exercise on French territory within one month.

#### Cost

Free

#### Listing on the list of veterinary practice companies

Veterinarians practising as part of an exercise society (Art L241-17 CPMR) must apply to the College of Veterinarians.

The application for registration must be submitted collectively by the partners. It is addressed to the Regional Council of the Order of the Company's Head Office by recommended letter with request for notice of receipt, along with the following documents:

- A copy of the statutes and, if necessary, an expedition or copy of the constitution;
- A certificate of registration on each partner's board
- a certificate from the clerk of the court who ruled on the head office of the company, noting the filing at the registry of the application and the documents used for its registration in the register of trade and companies.

**Please note**

The Order's regional council decides under the same conditions as those mentioned in paragraph "5o. b. Ask for registration with the College of Veterinarians."

*For further information*: Articles L. 241-17, R. 241-29 to R. 241-32 of the Rural Code and Marine Fisheries.

### c. Remedies

#### Solvit

SOLVIT is a service provided by the national administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of an EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

