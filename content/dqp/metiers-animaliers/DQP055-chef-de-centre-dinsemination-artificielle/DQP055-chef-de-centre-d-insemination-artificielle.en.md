﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP055" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Head of equine artificial insemination centre" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="head-of-equine-artificial-insemination" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/head-of-equine-artificial-insemination-centre.html" -->
<!-- var(last-update)="2020-04-15 17:20:50" -->
<!-- var(url-name)="head-of-equine-artificial-insemination-centre" -->
<!-- var(translation)="Auto" -->


Head of equine artificial insemination centre
=============================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:50<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The head of the equine artificial insemination centre ("centre leader") is the head of an equine seed production centre and/or an embryonic transplant team.

As part of his missions, he carries out, organizes and manages the activities of insemination, freezing or refrigeration of sperm, storage of doses and shipment of seed, and, if necessary, embryo transfer activities.

In collaboration with the teams of inseminators and labourers who work in his centre, he maintains commercial relations with the owners of the mares and stallions with which he works, while promoting his Structure.

Its mission is also to keep ad terms ahead of new insemination techniques and processes, as well as to participate in research in the field.

For more information on centre-head missions, please refer to the[site of the French Institute of Horse and Horse riding](http://www.haras-nationaux.fr/information/accueil-equipaedia/formations-et-metiers/les-metiers-de-lelevage/chef-de-centre-d-ia.html).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Anyone who wishes to become head of the centre must have declared himself to the administrative authority (the prefect of the region in which he wishes to practice), who will register him in view of the presentation of a centre head's licence insemination.

This licence may only be issued to holders of the Certificate of Fitness for Centre Manager issued by a specialized training institution.

An application for validation of academic achievements, assessed by the end-of-session exam board, is possible to:

- veterinarians specializing in equine reproduction, with a French diploma or a European college;
- nationals of the European Union or european Economic Area countries, holders of accreditation, equivalent level of training, from one of the countries of the European Union, to carry out the functions of head of the centre.

The validation of academic achievements allows the recipient to be exempted from part of the training or part of the tests for the certificate of aptitude.

*For further information*: Article L. 653-13 and Article R. 653-96 of the Rural and Marine Fisheries Code; Articles 9 and 13 of the[decree of 21 January 2014](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028535400&dateTexte=20171106) certificates of fitness for inseminator and artificial insemination centre in equine and asine species.

#### Training

The centre head's licence is subject to a certificate of aptitude issued following training at one of the following institutions:

- the mare training centre of the French Horse and Riding Institute;
- The Rambouillet Zootechnical Education Centre;
- one of the four national veterinary schools.

Access to training may be limited depending on the number of places available and comes after the decision of the director of one of these institutions.

Admission can be made:

- or after reviewing the person's file, provided that it justifies:- or state diploma as a veterinary doctor,
  - either an engineering degree from a higher education institution or a national agricultural college;
- or, after a check of the acquired knowledge, the holder of a certificate of fitness for the duties of inseminator justifying:- either a Level III degree in agriculture or biology and justifying three years of activity in equine insemination,
  - five years of professional experience in equine breeding, including at least four years in equine insemination,
  - or a Level I degree in equine reproductive biology.

**What to know**

The knowledge check covers the subjects defined in Schedule III of the decree of 21 January 2014 and may involve a motivational interview.

The duration of the training is five weeks during which the candidate will have to achieve objectives in reproductive physiology and biotechnology, diet, genetics, hygiene and prophylaxis, regulation, relations promotion and marketing.

At the end of this training, the candidate will have to pass an exam consisting of theoretical, practical, oral and legislative tests.

Admission is pronounced if the person has obtained an average score of 12 out of 20, with no score less than 10 out of 20.

**Please note**

To find out about the training leading to the veterinary profession in France, it is advisable to refer to the qualification sheet dedicated to it.

*For further information*: Articles 9 and following, and Appendix III and IV of the[decree of 21 January 2014](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028535400&dateTexte=20171106) certificates of fitness for inseminator and artificial insemination centre in equine and asine species.

#### Costs associated with qualification

Training to become a centre manager pays off. For more information on training costs, it is advisable to approach specialized institutions.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

A national of an EU or EEA Member State, acting as a centre manager in one of these states, may use his or her professional title in France on a temporary or casual basis. He must make a statement to the prefect of the region in which he wishes to practise (see below "5.a. Make a prior declaration of activity for EU nationals engaged in activity) before his first performance. (LPS)) ).

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional must have carried it out in one or more Member States for at least one year in the ten years that precede the performance.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test in a one month from the prefect's receipt of the request for declaration.

*For further information*: Articles L. 653-13, R. 653-96, L. 204-1 and R. 204-1 of the Rural code and marine fisheries.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a training certificate or certificate of competency issued by a competent authority in another Member State that regulates access to the profession or its exercise;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once he fulfils one of the two previous conditions, he will have to obtain the license of head of centre required for the practice of the profession, from the competent regional prefect. For more information, it is advisable to refer to paragraph 5.b. Obtain a licence for EU or EEA nationals for a permanent exercise (LE)".

If, during the examination of the file, the prefect finds that there are substantial differences between the training and professional experience of the national and those required to practise in France, compensation measures may be taken ("5°). b. Good to know: compensation measures").

*For further information*: Articles R. 204-5 of the Rural Code and Marine Fisheries.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Ethical obligations are the responsibility of the head of the centre, including respecting the professional confidentiality associated with seed collection, insemination and embryo transfer data, ensuring the health and maintenance of the dignity of the inseminated animal or traceability and compliance with health regulations.

4°. Insurance
---------------------------------

As part of his duties, the head of the centre is obliged to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out on occasion.

5°. Qualification recognition process and formalities
---------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

#### Competent authority

The regional prefect is responsible for receiving the pre-declaration of practice on a temporary or casual basis.

#### Supporting documents

The national's prior declaration must be forwarded by any means to the competent authority and include the following supporting documents:

- Proof of the professional's nationality
- a certificate certifying that it:- is legally established in an EU or EEA state,
  - practises one or more professions whose practice in France requires the holding of a certificate of ability,
  - and does not incur a ban on practising, even temporarily, when issuing the certificate;
- proof of his professional qualifications
- where neither professional activity nor training is regulated in the EU or EEA State, proof by any means that the national has been engaged in this activity for one year, full-time or part-time, in the last ten years;
- a declaration of temporary or casual activity commitment of less than one year.

This advance declaration includes information relating to insurance or other means of personal or collective protection underwritten by the registrant to cover his professional liability.

These documents are attached, as needed, to their translation into the French language.

#### Time

The regional prefect has one month from the time the file is received to make his decision:

- allow the claimant to perform his or her benefit.  In the event of non-compliance with this one-month period or the administration's silence, the provision of services may be carried out;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the qualifications and work experience he uses are substantially different from those required for the exercise of the profession in France (see below: "Good to know: compensation measure");
- inform them of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, from the resolution of the difficulties and in any case within a maximum of three months from the information of the person concerned as to the existence of the difficulty or difficulties. In the absence of a response from the competent authority within this time frame, service delivery may begin.

*For further information*: Articles R. 204-1 and R. 204-6 of the Rural code and marine fisheries.

### b. Obtain a licence for EU nationals engaged in permanent activity (LE)

#### Competent authority

The regional prefect of the place of practice is responsible for issuing the licence allowing the national to carry out permanently the activity of head of centre in France.

#### Procedure

The national must pass on to the prefect all the necessary documents to support his application for a licence, including:

- proof of nationality
- A training certificate or certificate of competence acquired in an EU or EEA member state;
- any evidence justifying, if any, that the national has been a centre manager for one year, full-time or part-time, in a Member State that does not regulate the profession.

The prefect has one month from receiving the supporting evidence to acknowledge it or request the sending of missing documents.

The decision to grant the licence will then take place within three months, extended by an additional month in the event of missing parts.

The silence kept in these deadlines will be worth acceptance decision.

#### Good to know: compensation measure

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice, which may be:

- an adaptation course of up to three years
- an aptitude test carried out within six months of notification to the person concerned.

However, the choice of measure will be the responsibility of the prefect if the national justifies:

- a certificate of competency acquired as a result of general training, examination or full-time or part-time exercise for three years over the past ten years;
- either a general, technical or vocational secondary school cycle completed with an internship, a professional practice or a study or training cycle.

*For further information*: Articles R. 204-2 to R. 204-6 of the Rural Code and Marine Fisheries.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

