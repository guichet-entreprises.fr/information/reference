﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP245" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Responsable d'établissement d'élevage d'animaux d'espèces non domestiques, de vente ou de location, de transit, de présentation au public de spécimens vivants de la faune française et étrangère" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="responsable-d-etablissement-d-elevage" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/responsable-d-etablissement-d-elevage-d-animaux-d-especes-domestiques.html" -->
<!-- var(last-update)="2020-04-15 17:20:56" -->
<!-- var(url-name)="responsable-d-etablissement-d-elevage-d-animaux-d-especes-domestiques" -->
<!-- var(translation)="None" -->

# Responsable d'établissement d'élevage d'animaux d'espèces non domestiques, de vente ou de location, de transit, de présentation au public de spécimens vivants de la faune française et étrangère

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:56<!-- end-var -->

## 1°. Définition de l’activité

Le responsable d'un établissement d'élevage d'animaux d'espèces non domestiques a pour mission d'assurer la vente, la location, le transit et la présentation au public de ces animaux.

Sont considérés comme des animaux d'espèces non domestiques, ceux qui ne figurent pas sur la liste des espèces, races ou variétés d'animaux domestiques fixée par l'arrêté du 11 août 2006.

De plus, les établissements d'élevage ne peuvent détenir que des animaux n'ayant subi aucune modification par sélection.

*Pour aller plus loin* : article R. 413-8 du Code de l'environnement ; arrêté du 11 août 2006 fixant la liste des espèces, races ou variétés d'animaux domestiques.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de responsable d'un tel établissement, le professionnel doit respecter les exigences suivantes :

- être qualifié professionnellement (cf. infra « Formation ») ;
- être titulaire d'un certificat de capacité pour l'entretien des animaux d'espèces non domestiques ;
- obtenir une autorisation d'ouverture d'un tel établissement (cf. infra « 4°. b. Demande d'autorisation d'ouverture ») ;
- procéder à l'identification des animaux détenus en captivité.

*Pour aller plus loin* : article L. 413-2 du Code de l'environnement.

#### Formation

En vue d'obtenir le certificat de capacité, le professionnel doit être titulaire d'un diplôme lui permettant d'exercer une activité en lien avec les animaux d'espèces non domestiques et d'une expérience professionnelle qui varie selon la nature du diplôme obtenu.

La liste des diplômes et la durée de l'expérience professionnelle requise est fixée à l'annexe I de l'arrêté du 12 décembre 2000 fixant les diplômes et les conditions d'expérience professionnelle requis par l'article R. 413-5 du Code de l'environnement pour la délivrance du certificat de capacité pour l'entretien d'animaux d'espèces non domestiques.

##### Certificat de capacité

Le responsable d'un établissement d'élevage d'animaux non domestiques qui assure leur vente, leur location, leur transit ou leur présentation au public doit obtenir un certificat de capacité pour l'entretien de ces animaux.

Pour cela, l'intéressé doit adresser une demande au préfet du département de son domicile (cf. infra « 4°. a. Demande en vue d'obtenir le certificat de capacité animaux non domestiques »).

*Pour aller plus loin* : article L. 413-2 du Code de l'environnement.

##### Procédure d'identification des animaux

Pour exercer l'activité d'élevage d'animaux, de vente ou de location, de transit ou de présentation au public des animaux non-domestiques, ces derniers doivent obligatoirement être identifiés grâce à :

- un marquage individuel ;
- leur inscription sur le fichier national d'identification des animaux d'espèces non domestiques détenus en captivité.

##### Marquage des animaux

L'ensemble des animaux détenus au sein de l'établissement doivent porter un marquage individuel dès le mois suivant leur naissance.

Les modalités de ce marquage sont fixées à l'annexe A de l'arrêté du 10 août 2004 fixant les conditions d'autorisation de détention d'animaux de certaines espèces non domestiques dans les établissements d'élevage, de vente, de location, de transit ou de présentation au public d'animaux d'espèces non domestiques.

Cette procédure effectuée par un vétérinaire, permet d'attribuer à chaque animal un numéro d'identification unique.

Le vétérinaire procédant au marquage doit remettre au propriétaire de l'animal un document attestant du marquage et doit ensuite le remettre au gestionnaire du fichier national.

**À noter**

Un délai plus long est accordé en cas d'impossibilité biologique de marquer l'animal dans le délai imparti.

##### Inscription sur le fichier national

Doivent être enregistrées sur ce fichier, les données relatives à l'animal et les informations concernant son propriétaire notamment son identité et son domicile.

*Pour aller plus loin* : article R. 413-23-1 du Code de l'environnement ; articles 6 à 11 de l'arrêté du 10 août 2004 fixant les conditions d'autorisation de détention d'animaux de certaines espèces non domestiques dans les établissements d'élevage, de vente, de location, de transit ou de présentation au public d'animaux d'espèces non domestiques.

#### c. Coûts associés à la qualification

Le coût de la formation du professionnel varie selon le cursus envisagé. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant l'activité de responsable d'un établissement d'élevage d'animaux non domestiques, peut exercer à titre temporaire et occasionnel, la même activité en France et ce, sans être titulaire du certificat de capacité.

Pour cela, l'intéressé devra effectuer une déclaration préalable auprès du préfet du département au sein duquel il souhaite exercer sa première prestation (cf. infra « 4°. c. Demande de déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel et celle requise pour exercer l'activité en France, le préfet peut décider de le soumettre à une mesure de compensation (cf. infra « 4°. c. Bon à savoir : mesure de compensation »).

*Pour aller plus loin* : article L. 413-2 du Code de l'environnement.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Etablissement (LE))

Aucune disposition spécifique n'est prévue pour le ressortissant UE en vue d'un exercice permanent en France. À ce titre, il est soumis aux mêmes exigences que le ressortissant français (cf. « 2°. Qualifications professionnelles »).

## 3°. Assurances et sanctions

Le professionnel exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

### Sanctions

Dès lors que le professionnel exploite un établissement d'élevage d'animaux d'espèces non domestiques sans posséder d'autorisation d'ouverture, le préfet le met en demeure de régulariser sa situation et peut envisager de suspendre son activité durant ce délai. Si le professionnel ne procède pas à la régularisation de sa situation, son établissement pourra être fermé dans un délai inférieur à deux ans à compter de sa mise en demeure.

*Pour aller plus loin* : article R. 413-45 à R. 413-47 du Code de l'environnement

En outre, le professionnel encourt une amende de 1 500 euros, 3 000 euros en cas de récidive, dès lors qu'il ne procède pas à l'identification et au marquage de ses animaux.

*Pour aller plus loin* : articles R. 415-4 à R. 415-5 du Code de l'environnement.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir le certificat de capacité animaux non domestiques

#### Autorité compétente

Le professionnel doit adresser une demande au préfet du département de son domicile ou au préfet de police de Paris s'il n'est pas domicilié dans un département français ni à Saint-Pierre-et-Miquelon.

#### Pièces justificatives

La demande du professionnel doit contenir les informations suivantes :

- son identité, son adresse et le type de qualification générale ou spécialisée demandée ;
- l'ensemble des diplômes, certificats ou tout document justifiant de son expérience professionnelle ;
- tout document permettant de justifier de la compétence du demandeur à exercer une activité professionnelle en lien avec des animaux domestiques et l'aménagement d'un établissement les accueillant.

#### Délais et issue de la procédure

Dès réception de la demande complète de la part du professionnel, le préfet lui délivre le certificat de capacité. Ce certificat peut être accordé pour une durée limitée ou illimitée et mentionne les espèces ou l'activité pour lesquelles il a été accordé et à titre facultatif le nombre d'animaux dont l'entretien est autorisé.

**À noter** 

Pour l'élevage de certaines catégories d'animaux, la consultation de la commission départementale de la nature, des paysages et des sites n'est pas obligatoire dès lors que le professionnel satisfait à des exigences de qualification. Les modalités de cette procédure sont fixées par l'arrêté du 2 juillet 2009 cité ci-après.

*Pour aller plus loin* : R. 413-3 à R. 413-7 du Code de l'environnement ; arrêté du 2 juillet 2009 fixant les conditions simplifiées dans lesquelles le certificat de capacité pour l'entretien des animaux d'espèces non domestiques peut être délivré.

### b. Demande d'autorisation d'ouverture

L'ouverture d'un établissement d'élevage d'animaux d'espèces non domestiques fait l'objet d'une autorisation préalable d'ouverture. Pour cela l'intéressé doit en faire la demande auprès de l'autorité compétente.

Les établissements sont classés en deux catégories :

- la première regroupe les établissements présentant des dangers ou inconvénients graves pour les espèces sauvages et les milieux naturels et la sécurité des personnes ;
- la seconde regroupe ceux qui ne présentent pas les dangers susmentionnés mais qui doivent respecter les dispositions visant à assurer la protection des espèces sauvages et des milieux naturels et la sécurité des personnes.

#### Autorité compétente

Le professionnel doit adresser une demande en sept exemplaires au préfet du département dans lequel est situé l'établissement (ou de son domicile dès lors que l'établissement est mobile).

#### Pièces justificatives

La demande doit mentionner les informations suivantes :

- l'identité du demandeur, son adresse, et si le demandeur est une personne morale, sa dénomination et sa raison sociale ;
- la nature des activités que le professionnel souhaite exercer ;
- la liste des équipements de l'établissement et le plan des installations ;
- la liste des espèces et le nombre d'animaux de chaque espèces détenus par l'établissement et leur répartition dans l'établissement ;
- une notice relative au fonctionnement de l'établissement ;
- le certificat de capacité du responsable de l'établissement.

**À noter** 

La dénomination de l'établissement ne doit pas comporter les termes suivants, en raison des dispositions spécifiques qui les régissent :

- parc national ;
- réserve naturelle ;
- conservatoire.

De plus, lorsque l'établissement comprend des installations classées pour la protection de l'environnement soumises à autorisation préalable, cette dernière vaut autorisation d'ouverture.

#### Délais et issue de la procédure

Le préfet recueille l'avis de la commission départementale de la nature des paysages et des sites qui peut interroger le demandeur. Le cas échéant, il sera informé par le préfet huit jours avant sa présentation.

Le préfet dispose d'un délai de cinq mois à compter du dépôt de la demande pour autoriser l'ouverture de l'établissement. En cas d'avis favorable il arrête l'autorisation d'ouverture fixant la liste des espèces que l'établissement peut détenir et les activités susceptibles d'être pratiquées.

**À noter**

Pour les établissements de la première catégorie, le préfet doit recueillir l'avis des collectivités territoriales qui disposent d'un délai de quarante-cinq jours pour statuer. En l'absence de réponse l'avis est réputé favorable.

En revanche pour les établissements de la seconde catégorie, le préfet examine la conformité de la demande avec les exigences en matière d'impératifs de protection des espèces et de qualité des équipements d'accueil des animaux non domestiques.

*Pour aller plus loin* : articles R. 413-10 à R. R. 413-14 du Code de l'environnement ; arrêté du 10 août 2004 précité.

### c. Demande de déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant adresse une demande au préfet du département dans lequel il souhaite exercer sa première prestation.

#### Pièces justificatives

Sa demande doit comporter les informations suivantes, le cas échéant assorties de leur traduction en français :

- son identité, sa nationalité ainsi que son adresse ;
- le type d'activité pour laquelle la demande est effectuée ;
- une attestation certifiant que le ressortissant est légalement établi dans un État de l'UE pour y exercer cette activité et qu'il n'encourt aucune interdiction temporaire ou définitive d'exercer ;
- un justificatif de ses qualifications professionnelles ;
- lorsque l’État membre d'établissement ne réglemente ni l'accès à la profession ni son exercice, la preuve que le demandeur a exercé cette activité dans un ou plusieurs État(s) membres(s) pendant au moins un an au cours des dix dernières années ;
- un document justifiant que le professionnel a souscrit à une assurance de responsabilité professionnelle.

#### Délais et issue de la procédure

Dans un délai d'un mois à compter de la réception de la demande, le préfet l'informe qu'il fera ou non l'objet d'un examen de ses qualifications professionnelles et devra se soumettre à une mesure de compensation.

À l'issue de ce délai et si la demande du ressortissant ne fait pas l'objet d'un contrôle et que la demande est complète, le préfet délivre le certificat de capacité.

#### Bon à savoir : mesure de compensation

Lorsque l'examen des qualifications professionnelles du ressortissant fait apparaître des différences substantielles entre sa formation et celle requise pour exercer l'activité en France, susceptibles de nuire à la santé ou à la sécurité du public ou des animaux, le préfet saisit une commission consultative pour la faune sauvage captive, composée de professionnels et de représentants des ministères concernés. Le cas échéant, le ressortissant devra se soumettre à une épreuve d'aptitude organisée par cette commission et composée d'interrogations écrites et orales.

À l'issue de l'épreuve d'aptitude, le préfet délivre une attestation de qualification professionnelle au ressortissant en vue d'exercer sa prestation de services en France.

**À noter** 

Le silence gardé par le préfet au delà d'un délai d'un mois vaut acceptation de la demande.

*Pour aller plus loin* : article R. 413-4 du Code de l'environnement.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne.

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).