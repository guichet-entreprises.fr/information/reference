﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP245" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Manager of a livestock-breeding establishment, or of an establishment selling, renting, displaying or serving as a transit centre for live species from the French and foreign faunas" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="manager-of-a-livestock-breeding-establishment" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/manager-of-a-livestock-breeding-establishment.html" -->
<!-- var(last-update)="2020-04-15 17:20:56" -->
<!-- var(url-name)="manager-of-a-livestock-breeding-establishment" -->
<!-- var(translation)="Auto" -->


Manager of a livestock-breeding establishment, or of an establishment selling, renting, displaying or serving as a transit centre for live species from the French and foreign faunas
=========================================================================================================================================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:56<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The head of a non-domestic animal breeding facility is responsible for the sale, rental, transit and presentation to the public of these animals.

Animals of non-domestic species are considered to be those not on the list of species, breeds or varieties of domestic animals set by the[decreed from 11 August 2006](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000789087).

In addition, breeding establishments may only hold animals that have not been modified by selection.

*For further information*: Article R. 413-8 of the Environment Code; decree of 11 August 2006 setting out the list of species, breeds or varieties of domestic animals.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of manager of such an institution, the professional must comply with the following requirements:

- be professionally qualified (see infra "Training");
- Hold a certificate of capacity for the care of animals of non-domestic species;
- to obtain an authorisation to open such an establishment (see infra "4°. b. Request for permission to open");
- identify animals held in captivity.

*For further information*: Article L. 413-2 of the Environment Code.

#### Training

In order to obtain the Certificate of Capacity, the professional must have a diploma to engage in a non-domestic animal activity and work experience that varies depending on the nature of the degree obtained.

The list of diplomas and the length of work experience required is set at the[Appendix I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7A2E4C8838C4669AE6617350AFDEF4D5.tplgfr28s_3?idArticle=LEGIARTI000021122568&cidTexte=LEGITEXT000021122566&dateTexte=20180315) of the order of 12 December 2000 setting out the diplomas and conditions of professional experience required by Article R. 413-5 of the Environment Code for the issuance of the Certificate of Capacity for the Maintenance of Animals of Non-Domestic Species.

**Capacity certificate**

The manager of a non-domestic animal breeding establishment that sells, leases, transits or presents them to the public must obtain a certificate of capacity for the maintenance of these animals.

In order to do so, the person concerned must submit an application to the prefect of the department of his home (see infra "4°. a. Request for the certificate of capacity for non-domestic animals").

*For further information*: Article L. 413-2 of the Environment Code.

**How to identify animals**

In order to carry out the activity of raising animals, selling or renting, transiting or presenting non-domestic animals to the public, they must be identified through:

- Individual marking
- their inclusion on the National Identification Index of animals of non-domestic species held in captivity.

**Marking animals**

All animals held in the facility must be individually tagged as early as the month after birth.

The terms of this marking are set at the[Appendix A](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7A2E4C8838C4669AE6617350AFDEF4D5.tplgfr28s_3?idArticle=LEGIARTI000022808149&cidTexte=LEGITEXT000018810562&dateTexte=20180315) of the order of 10 August 2004 setting out the conditions for the authorisation of the detention of certain non-domestic species in establishments for the breeding, sale, rental, transit or presentation of animals of non-domestic species to the public.

This procedure, carried out by a veterinarian, allows each animal to be assigned a unique identification number.

The veterinarian marking the animal owner must provide the animal owner with a document attesting to the marking and then hand it over to the national file manager.

**Please note**

A longer period of time is granted in case of biological impossibility to mark the animal within the allotted time.

**Registration on the national file**

The animal's data and information about its owner, including its identity and home, must be stored on this file.

*For further information*: Article R. 413-23-1 of the Environment Code; Articles 6 to 11 of the 10 August 2004 order setting out the conditions for the authorisation of animals of certain non-domestic species to be kept in establishments for the breeding, sale, rental, transit or presentation to the public of animals of non-domestic species Domestic.

#### c. Costs associated with qualification

The cost of training the professional varies according to the course envisaged. It is advisable to get close to the institutions concerned for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, legally established and acting as head of a non-domestic animal establishment, may exercise temporary and casual title, the same activity in France, without holding the certificate of ability.

In order to do so, the person concerned will have to make a prior declaration with the prefect of the department in which he wishes to perform his first performance (see infra "4°. c. Request for pre-reporting for the EU national for a temporary and casual exercise (LPS)").

Where there are substantial differences between the training received by the professional and that required to carry out the activity in France, the prefect may decide to subject him to a compensation measure (see infra "4°. c. Good to know: compensation measure").

*For further information*: Article L. 413-2 of the Environment Code.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

There are no specific provisions for the EU national for a permanent exercise in France. As such, he is subject to the same requirements as the French national (see "2.0). Professional qualifications").

3°. Insurance and sanctions
--------------------------------------

The professional practising in a liberal capacity must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

**Sanctions**

As soon as the professional operates a non-domestic animal breeding establishment without having an opening permit, the prefect requires him to regularize his situation and may consider suspending his activity during this period. If the professional does not regularize his situation, his establishment may be closed within two years of his notice.

*For further information*: Article R. 413-45 to R. 413-47 of the Environment Code

In addition, the professional faces a fine of 1,500 euros, 3,000 euros in case of reoffending, if he does not identify and mark his animals.

*For further information*: Articles R. 415-4 to R. 415-5 of the Environment Code.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Application for the non-domestic animal capacity certificate

**Competent authority**

The professional must apply to the prefect of the department of his home or to the prefect of police in Paris if he is not domiciled in a French department or in Saint-Pierre-et-Miquelon.

**Supporting documents**

The professional's request should contain the following information:

- identity, address and type of general or special qualification requested
- all diplomas, certificates or any documents justifying his professional experience;
- any documentation to justify the applicant's competence to engage in a professional activity in connection with pets and the development of an establishment that would house them.

**Delays and outcome of the procedure**

Upon receipt of the full request from the professional, the prefect issues him the certificate of capacity. This certificate may be granted for a limited or unlimited period of time and mentions the species or activity for which it was granted and, as an optional option, the number of animals authorized for maintenance.

**Please note**

For the breeding of certain categories of animals, consultation with the Departmental Commission on Nature, Landscapes and Sites is not mandatory as long as the professional meets qualification requirements. The terms of this procedure are set by the[decreed from 2 July 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020887078&dateTexte=20180315) quoted below.

*For further information*: R. 413-3 to R. 413-7 of the Environment Code; order of 2 July 2009 setting out the simplified conditions under which the certificate of capacity for the maintenance of animals of non-domestic species can be issued.

### b. Request for permission to open

The opening of a non-domestic animal breeding facility is the subject of a prior authorization to open. In order to do so, the person concerned must apply to the competent authority.

Institutions are categorized into two categories:

- the first includes establishments that pose serious dangers or disadvantages to wildlife and natural environments and the safety of people;
- the second includes those who do not present the above dangers but who must comply with provisions to ensure the protection of wildlife and natural environments and the safety of persons.

**Competent authority**

The professional must submit a seven-copy application to the prefect of the department in which the establishment is located (or from his home as long as the establishment is mobile).

**Supporting documents**

The request should include the following information:

- The applicant's identity, address, and whether the applicant is a legal entity, its name and its name;
- The nature of the activities the professional wishes to carry out;
- The list of the facility's facilities and the facility plan;
- The list of species and the number of animals of each species held by the institution and their distribution in the facility;
- A notice on how the establishment works
- The facility manager's certificate of capacity.

**Please note**

The name of the institution should not include the following terms, due to the specific provisions that govern them:

- National Park;
- Nature Reserve;
- Conservatory.

In addition, where the facility includes pre-approved facilities classified for environmental protection, the facility is subject to opening permission.

**Delays and outcome of the procedure**

The prefect receives the opinion of the Departmental Commission on the Nature of Landscapes and Sites, which may question the applicant. If necessary, he will be informed by the prefect eight days before his presentation.

The prefect has five months from the filing of the application to authorize the opening of the establishment. In the event of a favourable opinion, it decides the opening authorization setting out the list of species that the establishment may hold and the activities that may be carried out.

**Please note**

For establishments in the first category, the prefect must seek the opinion of the local authorities, which have a 45-day decision period. In the absence of an answer, the opinion is deemed favourable.

On the other hand, for establishments in the second category, the prefect examines the compliance of the application with the requirements for species protection and the quality of non-domestic animal care facilities.

*For further information*: Articles R. 413-10 to R. R. 413-14 of the Environment Code; 10 August 2004 above.

### c. Request for pre-reporting for EU national for temporary and casual exercise (LPS)

**Competent authority**

The national sends an application to the prefect of the department in which he wishes to exercise his first benefit.

**Supporting documents**

His application must include the following information, if any, with their translation into French:

- identity, nationality and address
- The type of activity for which the request is made
- a certificate certifying that the national is legally established in an EU state to carry out this activity and that he does not incur any temporary or permanent ban on practising;
- proof of his professional qualifications
- where the Member State of establishment does not regulate access to the profession or its exercise, proof that the applicant has engaged in this activity in one or more member states for at least one year in the last ten years;
- a document justifying that the professional has taken out professional liability insurance.

**Delays and outcome of the procedure**

Within one month of receiving the application, the prefect informs him that he will or will not be subject to a review of his professional qualifications and will have to submit to a compensation measure.

At the end of this period and if the national's application is not subject to review and the application is complete, the prefect issues the certificate of capacity.

**Good to know: compensation measure**

When the examination of the professional qualifications of the national reveals substantial differences between his training and that required to carry out the activity in France, which could harm the health or safety of the public or animals , the prefect referred the matter to a captive wildlife advisory commission, made up of professionals and representatives of the relevant ministries. If necessary, the national will have to submit to an aptitude test organised by this commission and composed of written and oral questions.

At the end of the aptitude test, the prefect issues a certificate of professional qualification to the national in order to carry out his services in France.

**Please note**

The silence kept by the prefect beyond a one-month period is worth accepting the application.

*For further information*: Article R. 413-4 of the Environment Code.

### d. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form.

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

