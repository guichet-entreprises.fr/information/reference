﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP150" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Identifier of domestic carnivores" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="identifier-of-domestic-carnivores" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/identifier-of-domestic-carnivores.html" -->
<!-- var(last-update)="2020-04-15 17:20:54" -->
<!-- var(url-name)="identifier-of-domestic-carnivores" -->
<!-- var(translation)="Auto" -->




Identifier of domestic carnivores
=====================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:54<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The identification of domestic carnivores, namely cats, dogs and ferrets, involves marking, by assigning the animal an exclusive and non-reusable identification number carried out by tattoo or transponder, followed by registration of the animal on the National Domestic Carnivore Identification Index (ICAD).

The household carnivore identifier's mission is to mark the animal and complete the pre-identification document for the animal, a copy of which will be given to the owner and another copy to the I-CAD.

To learn more about the steps related to the identification of domestic carnivores, see the[I-CAD](https://www.i-cad.fr/).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The title of identifier for domestic carnivores is reserved:

- Persons who have received an authorization from the Minister for Agriculture;
- veterinarians, who are entitled as of right.

*To go further* Articles L. 212-10 and R. 212-65 of the Rural and Marine Fisheries Code.

#### Training

The identification of domestic carnivores is different depending on whether it is carried out by a veterinarian or an authorized tattoo artist.

The veterinarian acting as an identifier for all domestic carnivores, performs the tattoo by dermograph or pliers and by the implantation of an electronic chip.

The qualified tattoo artist can only mark dogs less than 4 months old by tweezer tattoo.

##### Veterinary

To find out all about training and access to the veterinary profession in France, it is advisable to refer to the qualification sheet provided for this purpose.

##### Qualified tattoo artists

The person concerned, who wishes to obtain the title of qualified tattoo artist, must pass a theoretical and practical examination, the terms and conditions of which are set by the Ministry of Agriculture and are implemented by the[Canine Central Society](http://www.scc.asso.fr/La-formation-des-tatoueurs-agrees) (SCC).

Both events are open to dog breeders, hunters, hunting company officials and crew masters.

The theoretical exam is in the form of a multiple-choice questionnaire. The candidate will be able to attend a training day organized by the Central Canine Society, prior to the passage of the test.

Once the candidate is admitted to the theory test, the CCS will prepare for the practical test as part of a sponsorship contract between the candidate and an experienced tattoo artist, known as a "sponsor."  This sponsor will supervise the candidate during the completion of at least 10 tattoos.

When the candidate is deemed fit to pass the practical test, the candidate's sponsor contacts the DD (CS)PP of his department, so that the practical test is validated by an administration officer, following the observation of a tattoo, carried out by the Candidate.

Csc forwards the applicant's file to the Department of Agriculture's Food Branch.

The applicant's clearance is effective only after his file has been validated by the National Enabling Commission.

*To go further* Article 35 of the August 1, 2012 order on the identification of domestic carnivores and setting out how to implement the identification file for domestic carnivores.

#### Costs associated with training

The training of identifiers, other than veterinarians, is paid for. For more details, it is advisable to get closer to the Central Canine Society.

### b. EU or EEA nationals: for temporary or casual exercise (free provision of services (LPS))

A national of a Member State of the European Union (EU) or the European Economic Area (EEA), who acts as an identifier for domestic carnivores in one of these states, may use his professional title in France, as a temporary or occasional, as long as he makes a statement to the Directorate General of Food of the Ministry of Agriculture (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional must have carried it out in one or more Member States for at least one year, during the ten years which precede the performance.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test (cf. infra "5°. a. Good to know: compensation measure").

*To go further* Article L. 204-1 of the Rural Code and Marine Fisheries.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a training certificate or certificate of competency issued by a competent authority in another Member State that regulates access to or exercise of the profession;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once he fulfils one of the two previous conditions, he will have to obtain an authorization from the Minister responsible for agriculture (see infra "5°. b. Obtain an authorization for EU or EEA nationals for a permanent exercise (LE) ").

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test or a adaptation stage (see infra "5.0). b. Good to know: compensation measures").

*To go further* Articles R. 204-2, R. 204-3 and R. 212-65 of the Rural and Marine Fisheries Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Veterinarians who carry out a mission to identify domestic carnivores are subject to the provisions of the[Code of Ethics applicable to the profession](https://www.veterinaire.fr/la-profession/code-de-deontologie.html).

For identifiers other than veterinarians, they have an ethical obligation, including respecting the confidentiality of identification data and ensuring that the dignity of the marked animal is maintained.

4°. Insurance
---------------------------------

In the event of a liberal exercise, the identifier for domestic carnivores is required to take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out on occasion.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The prior declaration of activity must be sent, before the first benefit, to the Directorate General of Food of the Ministry of Agriculture.

**Renewal of pre-declaration**

It must be renewed once a year and with each change in employment status.

**Supporting documents**

The national's prior declaration must be forwarded by any means to the competent authority and include the following supporting documents:

- Proof of the professional's nationality
- a certificate certifying that it:- is legally established in an EU or EEA state,
  - practises one or more professions whose practice in France requires the holding of a certificate of ability,
  - does not incur a ban on practising, even if temporary, when issuing the certificate;
- proof of his professional qualifications
- where neither professional activity nor training is regulated in the EU or EEA State, proof by any means that the national has been engaged in this activity for one year, full-time or part-time, in the last ten years;

This advance declaration includes information relating to insurance or other means of personal or collective protection underwritten by the registrant to cover his professional liability.

To these documents is attached, as needed, their translation into the French language.

**Timeframe**

The Food Directorate has one month from receipt of the file to make its decision:

- Allow the provider to perform its first service;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the qualifications and work experience he uses are substantially different from those required for the exercise of the profession in France (see below: "Good to know: compensation measure");
- inform them of one or more difficulties that may delay decision-making. In this case, the Directorate General of Food will have two months to decide, as of the resolution of the difficulty or difficulties. In the absence of a response from the Food Directorate within these timeframes, service delivery may begin.

**What to know**

The person who engages in the activity of identifying domestic carnivores, on an occasional or temporary basis, without having made the prior declaration or by submitting an incomplete declaration, is liable to a penalty of the fifth class provided Articles 131-13 and the following of the Penal Code.

**Cost**

Free.

**Good to know: compensation measure**

In order to obtain permission to practise, the person concerned may be required to submit to an aptitude test if it appears that the qualifications and work experience he uses are substantially different from those required for practising the profession in France.

The aptitude test covers all or part of the exam mentioned in paragraph "2." a. Training," depending on what is needed to establish that the relevant knowledge and qualifications are mastered.

*To go further* Articles R. 204-1 and the following of the Rural Code and Marine Fisheries; Article 34 of the August 1, 2012 order on the identification of domestic carnivores and setting out the modalities for the implementation of the National Domestic Carnivore Identification Index.

### b. Obtain an authorization for EU or EEA nationals for a permanent exercise (LE)

EU or EEA nationals who perform non-veterinary functions must obtain an authorization to identify domestic carnivores.

**Competent authority**

The application for clearance is addressed to the Minister for Agriculture (General Directorate of Food) accompanied by a file that includes:

- A photocopy of the ID card or passport
- a criminal record extract
- A resume detailing its various activities;
- any indications about his profession and the framework in which he practises.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

In support of his application, the national is subjected to a theoretical and practical examination, which necessarily includes the tattoo of a dog. The Director General of Food is responsible for organizing the review.

**Outcome of the procedure**

The decision of the Minister responsible for agriculture to issue the authorization is made after the advice of a commission comprising:

- The director general of food or his representative;
- the president of the National Veterinary Order or his representative;
- the president of the Association of Tattooists of France or his representative;
- the president of the Association of Registered Pet Tattooists or his representative.

The clearance is issued for one year, renewable automatically provided that the national performs at least ten tattoos per year.

The silence of the Minister responsible for agriculture at the end of a two-month period is a decision to dismiss the application for clearance.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may have to submit to the measure of his choice, namely:

- an adaptation course of up to three years
- or an aptitude test carried out within six months of notification to the person concerned.

**Adaptation course**

The General Manager of Food sets out the terms and conditions of the adaptation course in an agreement with the intern and the host company.

The choice of the company is up to the intern among professionals proposed by the general manager of food. An evaluation of the skills acquired during this internship is carried out by the Director General of Food.

**Aptitude test**

The aptitude test covers all or part of the exam mentioned in paragraph "2." a. Training," depending on what is needed to establish that the relevant knowledge and qualifications are mastered.

*To go further* Article R. 204-2 and the following of the Rural code and marine fisheries; Article R. 212-65 and the following of the Rural Code and Marine Fisheries; Article 34 of the August 1, 2012 order.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

