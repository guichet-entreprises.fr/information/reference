﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP150" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Identificateur des carnivores domestiques" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="identificateur-des-carnivores-domestiques" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/identificateur-des-carnivores-domestiques.html" -->
<!-- var(last-update)="2020-04-15" -->
<!-- var(url-name)="identificateur-des-carnivores-domestiques" -->
<!-- var(translation)="None" -->

# Identificateur des carnivores domestiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15<!-- end-var -->

## 1°. Définition de l’activité

L’identification des carnivores domestiques, à savoir les chats, les chiens et les furets, comporte le marquage, par l’attribution à l’animal d’un numéro d’identification exclusif et non réutilisable effectuée par tatouage ou par un transpondeur, suivie de l’enregistrement de l’animal sur le fichier national d’identification des carnivores domestiques (ICAD).

L’identificateur des carnivores domestiques a pour mission d’effectuer le marquage de l’animal et de remplir le document de préidentification de l’animal dont il remettra un exemplaire au propriétaire et un autre exemplaire à l’I-CAD.

Pour en savoir plus sur les démarches relatives à l’identification des carnivores domestiques, se reporter au site de l’[I-CAD](https://www.i-cad.fr/).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Le titre d’identificateur pour carnivores domestiques est réservé :

- aux personnes ayant reçu une habilitation délivrée par le ministre chargé de l'agriculture ;
- aux vétérinaires, qui sont habilités de plein droit.

*Pour aller plus loin* : articles L. 212-10 et R. 212-65 du Code rural et de la pêche maritime.

#### Formation

L'identification des carnivores domestiques est différente selon qu'elle est réalisée par un vétérinaire ou un tatoueur habilité.

Le vétérinaire agissant à titre d’identificateur pour tous les carnivores domestiques, réalise le tatouage par dermographe ou à la pince et par l'implantation d’une puce électronique.

Le tatoueur habilité peut uniquement marquer les chiens âgés de moins de 4 mois par tatouage à la pince.

##### Vétérinaire

Pour tout savoir sur la formation et l’accès à l’exercice de la profession de vétérinaire en France, il est conseiller de se reporter à la fiche qualification prévue à cet effet.

##### Les tatoueurs habilités

L'intéressé qui souhaite obtenir le titre de tatoueur habilité, doit réussir un examen théorique et un examen pratique dont les modalités et les conditions sont fixées par le ministère chargé de l'agriculture et sont mises en œuvre par la [Société centrale canine](http://www.scc.asso.fr/La-formation-des-tatoueurs-agrees) (SCC).

Ces deux épreuves sont ouvertes aux éleveurs canins, aux chasseurs, aux responsables des sociétés de chasse et aux maîtres d’équipage.

L'examen théorique se présente sous la forme d'un questionnaire à choix multiples. Le candidat pourra suivre une journée de formation organisée par la Société centrale canine, préalablement au passage de l'épreuve.

Une fois le candidat admis à l'épreuve théorique, la SCC assurera sa préparation à l'épreuve pratique dans le cadre d'un contrat de parrainage, établi entre le candidat et un tatoueur expérimenté, appelé « parrain ».  Ce parrain encadrera le candidat pendant la réalisation d'au minimum 10 tatouages.

Lorsque le candidat est jugé apte à passer l'épreuve pratique, le parrain du candidat, contacte la DD(CS)PP de son département, afin que l'épreuve pratique soit validée par un agent de l'administration, à la suite de l'observation d'un tatouage, réalisé par le candidat.

La SCC transmet le dossier du candidat ayant réussi les deux épreuves à la direction générale de l'alimentation du ministère chargé de l'agriculture.

L’habilitation du candidat n’est effective qu’après validation de son dossier par la Commission nationale d’habilitation.

*Pour aller plus loin* : article 35 de l'arrêté du 1er aout 2012 relatif à l'identification des carnivores domestiques et fixant les modalités de mise en œuvre du fichier d'identification des carnivores domestiques.

#### Coûts associés à la formation

La formation des identificateurs, autres que les vétérinaires, est payante. Pour plus de précisions, il est conseillé de se rapprocher de la Société centrale canine.

### b. Ressortissants UE ou EEE : en vue d’un exercice temporaire ou occasionnel (Libre Prestation de Services)

Le ressortissant d’un État membre de l’Union Européenne (UE) ou de l’Espace économique européen (EEE), exerçant l’activité d’identificateur pour carnivores domestiques dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel, dès lors qu’il effectue, préalablement à sa première prestation, une déclaration adressée à la direction générale de l'alimentation du ministère chargé de l'agriculture (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude (cf. infra « 5°. a. Bon à savoir : mesure de compensation »).

*Pour aller plus loin* : article L. 204-1 du Code rural et de la pêche maritime.

### c. Ressortissants UE ou EEE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre de formation ou d'une attestation de compétence délivré(e) par une autorité compétente d'un autre État membre qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une des deux conditions précédentes, il devra obtenir une habilitation du ministre chargé de l'agriculture (cf. infra « 5°. b. Obtenir une habilitation pour les ressortissants de l’UE ou de l' EEE en vue d’un exercice permanent (LE) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude ou un stage d’adaptation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles R. 204-2, R. 204-3 et R. 212-65 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le vétérinaire qui exerce une mission d'identification des carnivores domestiques est soumis aux dispositions du [Code de déontologie applicable à la profession](https://www.veterinaire.fr/la-profession/code-de-deontologie.html).

Pour les identificateurs autres que les vétérinaires, des obligations éthiques leur incombent et notamment de respecter le secret professionnel lié aux données d'identification et de veiller au maintien de la dignité de l'animal marqué.

## 4°. Assurance

En cas d’exercice libéral, l’identificateur pour carnivores domestiques a l’obligation de souscrire une assurance de responsabilité professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

La déclaration préalable d’activité doit être adressée, avant la première prestation, à la direction générale de l'alimentation du ministère chargé de l’agriculture.

#### Renouvellement de la déclaration préalable

Elle doit être renouvelée une fois par an et à chaque changement de situation professionnelle.

#### Pièces justificatives

La déclaration préalable du ressortissant devra être transmise par tout moyen à l'autorité compétente et comprendre les pièces justificatives suivantes :

- une preuve de la nationalité du professionnel ;
- une attestation certifiant qu'il :
  - est légalement établi dans un État de l’UE ou de l’EEE,
  - exerce une ou plusieurs professions dont l'exercice en France nécessite la détention d'un certificat de capacité,
  - n'encourt pas d’interdiction d'exercer, même temporaire, lors de la délivrance de l'attestation ;
- une preuve de ses qualifications professionnelles ;
- lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE, la preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années.

Cette déclaration préalable comprend les informations relatives aux assurances ou autres moyens de protection personnelle ou collective souscrits par le déclarant pour couvrir sa responsabilité professionnelle.

A ces documents est jointe, en tant que de besoin, leur traduction en langue française.

#### Délai

La direction générale de l'alimentation dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision :

- de permettre au prestataire d’effectuer sa première prestation de service ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France (cf. infra « Bon à savoir : mesure de compensation ») ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, la direction générale de l’alimentation aura deux mois pour se décider, à compter de la résolution de la ou des difficultés. En l'absence de réponse de la direction générale de l'alimentation dans ces délais, la prestation de service peut débuter.

**À savoir**

La personne qui exerce l'activité d'identificateur des carnivores domestiques, à titre occasionnel ou temporaire, sans en avoir fait la déclaration préalable ou en ayant transmis une déclaration incomplète, est passible d'une amende de cinquième classe prévue aux articles 131-13 et suivants du Code pénal.

#### Coût

Gratuit.

#### Bon à savoir : mesure de compensation

Pour obtenir l’autorisation d’exercer, l’intéressé peut être amené à se soumettre à une épreuve d’aptitude s’il apparaît que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France.

L'épreuve d'aptitude porte sur tout ou partie de l'examen mentionné au paragraphe « 2°. a. Formation », selon ce qu'il est nécessaire pour établir que les connaissances et qualifications concernées sont maîtrisées.

*Pour aller plus loin* : articles R. 204-1 et suivants du Code rural et de la pêche maritime ; article 34 de l'arrêté du 1er août 2012 relatif à l'identification des carnivores domestiques et fixant les modalités de mise en œuvre du fichier national d'identification des carnivores domestiques.

### b. Obtenir une habilitation pour les ressortissants de l’UE ou de l' EEE en vue d’un exercice permanent (LE)

Les ressortissants de l’UE ou de l’EEE qui exercent les fonctions autres que vétérinaires, doivent obtenir une habilitation permettant l’identification des carnivores domestiques.

#### Autorité compétente

La demande d'habilitation est adressée au ministre chargé de l'agriculture, (direction générale de l’alimentation) accompagnée d'un dossier comprenant :

- une photocopie de la carte d'identité ou du passeport ;
- un extrait de casier judiciaire ;
- un curriculum vitae précisant notamment ses différentes activités ;
- toutes indications concernant sa profession et le cadre dans lequel il exerce.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

À l'appui de sa demande, le ressortissant est soumis à un examen théorique et pratique, comportant obligatoirement le tatouage d'un chien. Le directeur général de l'alimentation est chargé d'organiser l'examen.

#### Issue de la procédure

La décision du ministre chargé de l'agriculture de délivrer l'habilitation est prononcée après avis d'une commission comprenant :

- le directeur général de l'alimentation ou son représentant ;
- le président de l'Ordre national des vétérinaires ou son représentant ;
- le président de l'Association des tatoueurs de France ou son représentant ;
- le président de l'Association des tatoueurs agréés des animaux de compagnie ou son représentant.

L'habilitation est délivrée pour un an, renouvelable automatiquement à condition que le ressortissant procède à au moins dix tatouages par an.

Le silence gardé du ministre chargé de l'agriculture à l'expiration d'un délai de deux mois vaut décision de rejet de la demande d'habilitation.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de son choix, à savoir :

- un stage d'adaptation d'une durée maximale de trois ans ;
- ou une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

##### Stage d'adaptation

Le directeur général de l’alimentation établit les modalités et le déroulement du stage d'adaptation dans une convention conclue avec le stagiaire et l'entreprise d'accueil.

Le choix de l'entreprise revient au stagiaire parmi des professionnels proposés par le directeur général de l'alimentation. Une évaluation des compétences acquises lors de ce stage est réalisée par le directeur général de l'alimentation.

##### Épreuve d'aptitude

L'épreuve d'aptitude porte sur tout ou partie de l'examen mentionné au paragraphe « 2°. a. Formation », selon ce qu'il est nécessaire pour établir que les connaissances et qualifications concernées sont maîtrisées.

*Pour aller plus loin* : article R. 204-2 et suivants du Code rural et de la pêche maritime ; article R. 212-65 et suivants du Code rural et de la pêche maritime ; article 34 de l'arrêté du 1er août 2012.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

#### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).