﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP087" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Conveyor of animals (equine, bovine, ovine, porcine, caprine and poultry species)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="conveyor-of-animals-equine-bovine-ovine-porcine-caprine-poultry" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/conveyor-of-animals-equine-bovine-ovine-porcine-caprine-poultry.html" -->
<!-- var(last-update)="2020-04-15 17:20:52" -->
<!-- var(url-name)="conveyor-of-animals-equine-bovine-ovine-porcine-caprine-poultry" -->
<!-- var(translation)="Auto" -->




Conveyor of animals (equine, bovine, ovine, porcine, caprine and poultry species)
==================================================================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The live animal conveyor (defined in Article 2 of regulation (EC) 1/2005) for domestic equines and domestic animals of the bovine, goat, sheep, swine and poultry species (so-called "rent animals") intervenes in the transport of animals when the journey:

- is not subject to a veterinary emergency;
- is more than 65 km
- is carried out as part of an economic activity.

Its main tasks are:

- to keep and ensure the welfare of the animals transported;
- ensure their watering and feeding;
- provide first aid, if necessary, to animals that are injured or become ill while being transported.

During transport, the conveyor can be exclusively dedicated to these missions or, failing that, be:

- The client at the place of departure until the load is included;
- The recipient at the destination from the unloading included;
- Responsible for the stopping point, including loading and unloading;
- carrier at any other time of the trip.

**Please note**

Transportation must be carried out in accordance with the conditions of Annex I and II of the[Regulation (EC) No. 1/2005 of the Council of 22 December 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32005R0001).

*For further information*: Article R. 214-55 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of conveyor belt is reserved for the professional holding a certificate of skills issued by the Departmental Directorate in charge of population protection (DDPP).

The issuance of this certificate of competency can be obtained as long as the person is justified:

- or have undergoa specific training delivered by the [authorized body](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032743454&categorieLien=id) Minister for Agriculture and having passed the associated assessment;
- or hold one of the diplomas, titles or certificates in Part 1 of the schedule of the November 12, 2005 order.

If necessary, when the conveyor is also responsible for the transport of the animals, it must hold a permit adapted to the type of vehicle driven.

**Please note**

The carrier that employs a live animal conveyor must have a valid transport authorization. On the other hand, when the conveyor acts on his own behalf, he will have to apply for this transport authorization by filling out the form[Cerfa 15714*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15714.do) which it will refer to the territorially competent DDPP.

*For further information*: Articles 1 and 2 of the [decree of 12 November 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031521436&dateTexte=20171114) relating to the clearances or registrations of training organizations implementing the training required for persons performing a live animal conveyor function and Article 6 paragraph 5 of the[1/2005 Council Regulation of 22 December 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32005R0001) protection of animals during transport and related operations.

#### Training

The training should focus on the technical and regulatory aspects of legislation on the protection of animals in transport, including:

- Fitness, general conditions and documents for transporting live animals;
- the physiology of the animals, their food needs, watering, their behaviour and the concept of stress;
- The practicalities of handling animals
- The impact of driving on the welfare of the animals being transported and on the quality of meat;
- Emergency animal care
- Safety aspects for staff handling animals;
- long-term travel and the logbook.

The minimum duration of training is 14 hours for a category of animals. This minimum duration is added to a minimum of three hours per additional category of animals.

The Certificate of Skills is issued as a result of training and success in an assessment test.

*For further information*: Article 1 of the Order of 12 November 2015 and Annex IV of the Regulation of 22 December 2004.

#### Costs associated with qualification

Training to enter the conveyor profession is paid for. For more information, it is advisable to get closer to the training bodies authorized by the Minister responsible for agriculture.

### b. EU or EEA nationals: for temporary and casual exercise (free provision of services)

The national of an EU or EEA Member State, carrying out the activity of conveyoring animals for domestic equines and domestic animals of the cattle, goat, sheep, swine and poultry species in one of these states, may make use of his professional title in France, either on a temporary or casual basis. He must make a statement to the prefect of the region in which he wishes to practice (see below "5o). a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional must have carried it out in one or more Member States for at least one year, during the ten years which precede the performance.

When the examination of professional qualifications shows substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of a Member State of the European Union (EU) or party to the European Economic Area (EEA) who legally carries out the activity of conveyoring animals in that state may settle in France to carry out the same activity on a permanent basis.

In order to do so, he will have to obtain a certificate of competence from the territorially competent DDPP (see infra "5°. a. Obtain a certificate of competency for the EU or EEA national").

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional must have carried it out in one or more Member States for at least one year in the ten years that precede the performance.

If the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subject to compensation measures (see below "5. a. Good to know: compensation measures").

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Ethical obligations are the responsibility of the animal conveyor, including ensuring the health and dignity of the animals transported.

4°. Insurance
---------------------------------

As part of its duties, the animal conveyor is obliged to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out on occasion.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

#### a. Obtain a certificate of competency for THE EU or EEA national

**Competent authority**

The DDPP of the place of residence of the national is competent to decide on the issuance of the certificate of competence to the EU or EEA national.

**Supporting documents**

The national will mail a file to the relevant authority containing the following supporting documents:

- A photocopy of a valid ID
- A copy of a proof of residence
- The form[Cerfa No. 15715*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15715.do), duly completed and signed;
- proof of qualification.

**Procedure**

Once the DDPP has carried out the audits of the file and ensured that the individual has not been the subject of a withdrawal or deletion of a certificate in his Home State, it will be able to issue the certificate of competency.

**Please note**

The certificate must be established in the language of the issuing state as well as in English if the animal conveyor is likely to move to another state.

*For further information*: Section 17 and Chapter III of Schedule III of the December 22, 2004 settlement.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

