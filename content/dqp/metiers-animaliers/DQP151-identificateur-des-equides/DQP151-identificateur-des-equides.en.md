﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP151" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Identifier of equidae" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="identifier-of-equidae" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/identifier-of-equidae.html" -->
<!-- var(last-update)="2020-04-15 17:20:55" -->
<!-- var(url-name)="identifier-of-equidae" -->
<!-- var(translation)="Auto" -->


Identifier of equidae
=========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:55<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The identification of equines by transponder is mandatory and allows the equine to be registered in the central file of the SIRE ("Equine Information System").

As a reminder, equines form the family of mammals currently comprising three groups of species: horses, donkeys and zebras. These species have the distinction of having only one finger to each limb, finished with a hoof.

The identification is made by an identifier declared with the services of the French Institute of Horse and Riding (IFCE), in charge of implanting an electronic transponder in the neck of the equine and carrying out the report reading of equine. The person in charge of the identification thus completes a field identification form (paper or dematerialized) which he transmits to the IFCE or to an issuing body for registration and editing of the identification document.

*To go further* Article L. 212-9 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Can carry out the identification activity of equines:

- practising veterinarians, i.e. on the College of Veterinarians;
- veterinarians of active armies;
- declared veterinarians who practice in France on a temporary and casual basis;
- Veterinary teachers researchers as part of their teaching mission in national veterinary schools;
- IFCE officials or contract officers who have cumulative access to:- a certificate certifying their ability to identify equines by surveying natural marks,
  - certificate issued after training specific to active marking by the implantation of a transponder, the terms of which are set by[decreed from 24 February 2003](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000005634024&dateTexte=20150811).

*To go further* Article D. 212-58 of the Rural Code and Marine Fisheries.

#### Training

Only those who have been certified as veterinarians or IFCE officers who have been trained to do so may apply to be placed on the equine identification list.

#### Veterinary training

To find out everything about the training and access to the veterinary profession in France, it is advisable to refer to the qualification sheet provided for this purpose.

#### Training of IFCE officers

The training of IFCE officers consists of theoretical courses and practical work whose content is defined in consultation with the veterinary profession. At the end of the training, the IFCE officer will be issued a certificate of capacity by a National Electronic Identification Commission of equines, based on the trainers' reports and the summary report prepared by the training manager. to judge the officer's ability to make identifications.

### b. EU or EEA nationals: for temporary or casual exercise (free provision of services)

Veterinary nationals of an EU or EEA Member State who wish to be able, on a temporary and casual basis, to identify equines in France must first declare themselves as a veterinarian practising on a temporary and casual basis. To find out all about the modalities to be carried out, it is advisable to refer to the qualification sheet provided for this purpose.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Veterinary national of an EU or EEA Member State who wishes to be permanently entitled to identify equines in France must first take steps to:

- to practice the veterinary profession on a permanent basis. To find out all about the modalities to be carried out, it is advisable to refer to the qualification sheet provided for this purpose;
- be a public servant or contract agent of the IFCE.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The veterinarian is subject to the rules of ethics applicable to the profession (Articles R. 242-33 to R. 242-84 Rural and Marine Fisheries Code).

4°. Insurance
---------------------------------

Veterinarians in practice or in free provision of services must take out professional liability insurance whose validity is recognized on French territory.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for EU or EEA veterinary nationals engaged in temporary and occasional activity (LPS)

#### Competent authority

The Director General of IFCE is responsible for receiving the pre-declaration of temporary or occasional practice of the identifier activity. Prior to this declaration, the professional must have made his prior declaration of practice for the veterinary profession to the National Council of the College of Veterinarians.

#### Supporting documents

The pre-reporting of equine identifier activity by a veterinarian is done electronically. The professional sends an email request to the info@ifce.fr address by attaching the[application form completed and signed downloadable](https://www.ifce.fr/wp-content/uploads/2018/07/SIRE-DECLARATION-VETO-IDENTIFICATEURV2.pdf).

*To go further* : order of June 25, 2018 relating to the identification of equines.

#### Time

The Director General of IFCE has one month from receipt of the file to make his decision:

- allow the claimant to perform his or her benefit. In the event of non-compliance with this one-month period or the administration's silence, the provision of services may be carried out;
- inform them of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, from the resolution of the difficulties and in any case within a maximum of three months from the information of the person concerned as to the existence of the difficulty or difficulties. In the absence of a response from the competent authority within this time frame, service delivery may begin.

*To go further* Article R. 204-1 of the Rural Code and Marine Fisheries.

### b. Register on the equine identifier list for EU nationals engaged in permanent activity (LE)

#### Competent authority

The Director General of IFCE is responsible for authorizing those wishing to be able to permanently identify the identification activity of equines in France.

#### Procedure

The national wishing to be able to identify equines must first apply for registration with the College of Veterinarians and then register it on the list of identifiers to the Director General of the IFCE.

The application for registration on the identification list is made electronically. The professional sends an email request to the info@ifce.fr address by attaching the[application form completed and signed downloadable](https://www.ifce.fr/wp-content/uploads/2018/07/SIRE-DECLARATION-VETO-IDENTIFICATEURV2.pdf).

*To go further* : order of June 25, 2018 relating to the identification of equines.

#### Cost

Free.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on the academic and professional recognition of diplomas, except for veterinarians for whom only the Order is competent.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

