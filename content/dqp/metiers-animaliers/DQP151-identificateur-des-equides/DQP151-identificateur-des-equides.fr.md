﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP151" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Identificateur des équidés" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="identificateur-des-equides" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/identificateur-des-equides.html" -->
<!-- var(last-update)="2020-04-15" -->
<!-- var(url-name)="identificateur-des-equides" -->
<!-- var(translation)="None" -->

# Identificateur des équidés

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15<!-- end-var -->

## 1°. Définition de l’activité

Rendue obligatoire, l'identification des équidés par transpondeur permet de faire enregistrer son équidé dans le fichier central du SIRE (« Système d’information relatif aux équidés »).

Pour rappel, les équidés forment la famille de mammifères comprenant actuellement trois groupes d'espèces : les chevaux, les ânes et les zèbres. Ces espèces ont la particularité de n'avoir qu'un seul doigt à chaque membre, terminé d'un sabot.

L'identification est faite par un identificateur déclaré auprès des services de l'Institut français du cheval et de l'équitation (IFCE), en charge d'implanter un transpondeur électronique dans l'encolure de l'équidé et de réaliser le relevé de signalement de l’équidé. La personne en charge de l'identification complète ainsi un formulaire d’identification terrain (papier ou dématérialisé) qu’il transmet à l'IFCE  ou à un organisme émetteur pour enregistrement et édition du document d’identification.

*Pour aller plus loin* : article L. 212-9 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Peuvent exercer l’activité d’identificateur des équidés :

- les vétérinaires en exercice, c’est-à-dire inscrits au tableau de l’Ordre des vétérinaires ;
- les vétérinaires des armées en activité ;
- les vétérinaires déclarés qui exercent en France de manière temporaire et occasionnel ;
- les vétérinaires enseignants chercheurs dans le cadre de leur mission d’enseignement au sein des écoles nationales vétérinaires ;
- les fonctionnaires ou agents contractuels de l'IFCE qui disposent cumulativement :
  - d'une attestation certifiant leur aptitude à l'identification des équidés par relevé des marques naturelles,
  - d'une attestation délivrée à l'issue d'une formation spécifique au marquage actif par implantation d'un transpondeur dont les modalités sont fixées par arrêté du 24 février 2003.

*Pour aller plus loin* : article D. 212-58 du Code rural et de la pêche maritime.

#### Formation

Seules peuvent demander à être inscrites sur la liste des identificateurs des équidés, les personnes qui ont reçu le titre de vétérinaire ou les agents de l'IFCE qui ont suivi une formation à cet effet.

#### Formation des vétérinaires

Pour tout savoir sur la formation et l’accès à l’exercice de la profession de vétérinaire en France, il est conseillé de se reporter à la fiche qualification prévue à cet effet.

#### Formation des agents de l'IFCE

La formation des agents de l'IFCE est composée de cours théoriques et de travaux pratiques dont le contenu est défini en concertation avec la profession vétérinaire. À l'issue de la formation, l'agent de l'IFCE se verra délivrer une attestation de capacité par une Commission nationale d'identification électronique des équidés, au vu des rapports des formateurs et du rapport de synthèse établi par le responsable de la formation pour juger de l'aptitude de l'agent à procéder à des identifications.

### b. Ressortissants UE ou EEE : en vue d’un exercice temporaire ou occasionnel (Libre Prestation de Services)

Le ressortissant vétérinaire d’un État membre de l’UE ou de l’EEE qui souhaite être habilité, à titre temporaire et occasionnel, à identifier les équidés en France doit au préalable se déclarer en tant que vétérinaire exerçant à titre temporaire et occasionnel. Pour tout savoir sur les modalités à effectuer, il est conseillé de se reporter à la fiche qualification prévue à cet effet.

### c. Ressortissants UE ou EEE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant vétérinaire d’un État membre de l’UE ou de l’EEE qui souhaite être habilité, à titre permanent, à identifier les équidés en France doit au préalable effectuer les démarches pour :

- exercer à titre permanent la profession de vétérinaire. Pour tout savoir sur les modalités à effectuer, il est conseillé de se reporter à la fiche qualification prévue à cet effet ;
- être fonctionnaire ou agent contractuel de l'IFCE.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le vétérinaire est soumis aux règles déontologiques applicables à la profession (articles R. 242-33 à R. 242-84 du Code rural et de la pêche maritime).

## 4°. Assurance

Les vétérinaires en exercice ou en libre prestation de services doivent souscrire une assurance en responsabilité civile professionnelle dont la validité est reconnue sur le territoire français.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants vétérinaires de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le directeur général de l’IFCE est compétent pour recevoir la déclaration préalable d’exercice à titre temporaire ou occasionnel de l’activité d’identificateur. Avant cette déclaration, le professionnel doit avoir effectué sa déclaration préalable d’exercice pour la profession de vétérinaire auprès du Conseil national de l’Ordre des vétérinaires.

#### Pièces justificatives

La déclaration préalable à l'exercice de l'activité d'identificateur d'équidés par un vétérinaire se fait par voie électronique. Le professionnel transmet une demande par courriel à l’adresse info@ifce.fr en joignant le [formulaire de demande d’inscription complété et signé téléchargeable](https://www.ifce.fr).

*Pour aller plus loin* : arrêté du 25 juin 2018 relatif à l'identification des équidés.

#### Délais

Le directeur général de l’IFCE dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision :

- de permettre au prestataire d’effectuer sa prestation. En cas de non-respect de ce délai d’un mois ou du silence de l’administration, la prestation de services peut être effectuée ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés et en tout état de cause dans un délai maximum de trois mois à compter de l’information de l’intéressé quant à l’existence de la ou les difficultés. En l'absence de réponse de l'autorité compétente dans ce délai, la prestation de service peut débuter.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime.

### b. S’inscrire sur la liste des identificateurs des équidés pour les ressortissants de l’UE exerçant une activité permanente (LE)

#### Autorité compétente

Le directeur général de l’IFCE est compétent pour autoriser les personnes souhaitant être habilitée à identifier de manière permanente l’activité d’identificateur des équidés en France.

#### Procédure

Le ressortissant souhaitant être habilité à identifier des équidés doit en premier lieu demander son inscription à l’Ordre des vétérinaires puis son inscription sur la liste des identificateurs au directeur général de l’IFCE .

La demande d’inscription sur la liste des identifications se fait par voie électronique. Le professionnel transmet une demande par courriel à l’adresse info@ifce.fr en joignant le [formulaire de demande d’inscription complété et signé téléchargeable](https://www.ifce.fr).

*Pour aller plus loin* : arrêté du 25 juin 2018 relatif à l'identification des équidés.

#### Coût

Gratuit.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes, sauf pour les vétérinaires pour lesquels seul l’Ordre est compétent.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).