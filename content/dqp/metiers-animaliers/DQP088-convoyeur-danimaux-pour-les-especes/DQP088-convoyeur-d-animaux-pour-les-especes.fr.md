﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP088" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Convoyeur d'animaux pour les espèces autres que les équidés et animaux domestiques des espèces bovine, ovine, porcine, caprine et volailles" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="convoyeur-d-animaux-pour-les-especes" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/convoyeur-d-animaux-pour-les-especes-autres-que-les-equides-animaux.html" -->
<!-- var(last-update)="2020-04-15 17:20:52" -->
<!-- var(url-name)="convoyeur-d-animaux-pour-les-especes-autres-que-les-equides-animaux" -->
<!-- var(translation)="None" -->

# Convoyeur d'animaux pour les espèces autres que les équidés et animaux domestiques des espèces bovine, ovine, porcine, caprine et volailles

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:52<!-- end-var -->

## 1°. Définition de l'activité

Le convoyeur d'animaux vivants pour les espèces autres que les équidés domestiques et les animaux domestiques des espèces bovine, caprine, ovine, porcine et volailles (animaux dits « de rente ») intervient dans le transport d'animaux lorsque le trajet :

- n'est pas soumis à une urgence vétérinaire ;
- est supérieur à 65 km ;
- est effectué dans le cadre d'une activité économique.

Il a pour missions principales :

- de garder et de veiller au bien-être des animaux transportés ;
- d'assurer leur abreuvement et leur alimentation ;
- de prodiguer, le cas échéant, les premiers soins aux animaux qui se blessent ou tombent malades en cours de transport.

Lors du transport, le convoyeur peut être exclusivement dédié à ces missions ou, à défaut, être :

- le donneur d'ordre sur le lieu de départ jusqu'au chargement inclus ;
- le destinataire sur le lieu de destination depuis le déchargement inclus ;
- le responsable du point d'arrêt, chargement et déchargement inclus ;
- le transporteur à tout autre moment du voyage.

Le transport doit se faire dans le respect des conditions des annexes I et II du [règlement du Conseil (CE) n° 1/2005 du 22 décembre 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32005R0001).

*Pour aller plus loin* : règlement n° 1/2005 et article R. 214-55 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession de convoyeur d'animaux vivants est réservée au professionnel titulaire d'une attestation de fin de formation au transport routier des animaux vivants, délivré par un organisme de formation enregistré par le ministère chargé de l’agriculture.

La délivrance de cette attestation de fin de formation peut être obtenue dès lors que l'intéressé justifie :

- soit avoir suivi une formation spécifique délivrée par [organisme de formation enregistré](https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-957fabc5-1f76-4cab-9c6c-df6cac6eba6b) ;
- soit être titulaire de l'un des diplômes figurant dans la partie 2 de l'annexe de l'arrêté du 12 novembre 2005.

Le cas échéant, lorsque le convoyeur s'occupe également du transport des animaux, il doit justifier être titulaire d'un permis adapté au type de véhicule conduit.

**À noter**

Le transporteur qui emploie un convoyeur d'animaux doit obligatoirement être titulaire d'une autorisation de transport en cours de validité. En revanche, lorsque le convoyeur agit pour son propre compte, il devra lui-même demander cette autorisation de transport en remplissant le formulaire [Cerfa n° 15714](https://www.service-public.fr/professionnels-entreprises/vosdroits/R48105) qu'il adressera à la direction départementale en charge de la protection des populations (DDPP) territorialement compétente.

*Pour aller plus loin* : articles 1 et 2 de l'arrêté du 12 novembre 2015 relatif aux habilitations ou enregistrements des organismes de formation mettant en œuvre les formations requises pour les personnes exerçant une fonction de convoyeur d'animaux vivants ; article 6 paragraphe 4 du règlement n° 1/2005.

#### Formation

La formation doit porter sur les aspects techniques et réglementaires de la législation en matière de transport d'animaux et notamment sur :

- l’aptitude, les conditions générales et les documents du transport d'animaux vivants ;
- la physiologie des animaux, leurs besoins en nourriture, l'abreuvement, leur comportement et le concept de stress ;
- les aspects pratiques de la manipulation des animaux ;
- l'incidence du mode de conduite sur le bien-être des animaux transportés et sur la qualité des viandes ;
- les soins d'urgence aux animaux ;
- les aspects de sécurité pour le personnel manipulant des animaux.

La durée minimale de la formation est de 7 heures pour une voire deux catégories comparables d’animaux. Il est ajouté à cette durée minimale un minimum d'une heure par catégorie supplémentaire d'animaux comparable et de 3 heures par catégorie supplémentaire d'animaux substantiellement différente.

*Pour aller plus loin* : article R. 214-57 du Code rural et de la pêche maritime ; article 1 de l'arrêté du 12 novembre 2015 ; annexe IV du règlement du 22 décembre 2004.

#### Coûts associés à la qualification

La formation pour accéder à la profession de convoyeur est payante. Pour plus d'informations, il est conseillé de se rapprocher des organismes de formation enregistrés par le ministre chargé de l'agriculture.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État membre de l’UE ou de l’EEE, exerçant l’activité de convoyeur d’animaux pour les espèces autres que les équidés domestiques et les animaux domestiques des espèces bovine, caprine, ovine, porcine et des volailles dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit effectuer, préalablement à sa première prestation, une déclaration adressée au préfet de la région dans laquelle il souhaite exercer (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de convoyeur d'animaux dans cet État peut exercer en France la même activité de manière permanente.

Pour cela, il devra demander la reconnaissance de ses qualifications professionnelles auprès de la DDPP territorialement compétente (cf. infra « 5°. a. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant en vue d'un exercice permanent (LE) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an au cours des dix années qui précèdent la prestation.

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à des mesures de compensation (cf. infra « 5°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles R. 214-57 et R. 204-2 et suivants du Code rural et de la pêche maritime.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Des obligations éthiques incombent au convoyeur d'animaux et notamment de veiller à la santé et au maintien de la dignité des animaux transportés.

## 4°. Assurance

Dans le cadre de ses fonctions, le convoyeur d'animaux a l’obligation de souscrire une assurance de responsabilité professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

La DDPP du lieu du domicile est compétente pour se prononcer sur la demande de reconnaissance de qualification du ressortissant.

#### Pièces justificatives

Le ressortissant accompagnera sa demande de reconnaissance des pièces justificatives suivantes :

- une pièce d'identité en cours de validité ;
- une attestation de compétence ou un titre de formation acquis dans l’État d'origine ;
- lorsque l’État ne réglemente pas la profession, un document permettant de justifier que le ressortissant a exercé l'activité de convoyeur d'animaux pendant au moins un an au cours des dix dernières années.

#### Procédure

Une fois que la DDPP aura procédé aux vérifications du dossier et se sera assurée que l'intéressé n'avait pas fait l'objet d'un retrait ou d'une suppression de certificat dans son État d'origine, elle pourra lui  délivrer un justificatif de reconnaissance de qualifications professionnelles.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de compensation de son choix qui peut être :

- un stage d'adaptation d'une durée maximale de trois ans ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : articles R. 204-2 à R. 204-6 du Code rural et de la pêche maritime.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).