﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP088" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Conveyor of animals (except equine, bovine, ovine, porcine, caprine and poultry species)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="conveyor-of animals-except-equine-bovine-ovine-porcine-caprine-poultry" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/conveyor-of animals-except-equine-bovine-ovine-porcine-caprine-poultry.html" -->
<!-- var(last-update)="2020-04-15 17:20:52" -->
<!-- var(url-name)="conveyor-of animals-except-equine-bovine-ovine-porcine-caprine-poultry" -->
<!-- var(translation)="Auto" -->




Conveyor of animals (except equine, bovine, ovine, porcine, caprine and poultry species)
=============================================================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The live animal conveyor for species other than domestic equines and domestic animals of the cattle, goat, sheep, swine and poultry species (so-called "rent animals") intervenes in the transport of animals when the journey:

- is not subject to a veterinary emergency;
- is more than 65 km
- is carried out as part of an economic activity.

Its main tasks are:

- to keep and ensure the welfare of the animals transported;
- ensure their watering and feeding;
- provide first aid, if necessary, to animals that are injured or become ill while being transported.

During transport, the conveyor can be exclusively dedicated to these missions or, failing that, be:

- The client at the place of departure until the load is included;
- The recipient at the destination from the unloading included;
- Responsible for the stopping point, including loading and unloading;
- carrier at any other time of the trip.

Transportation must be carried out in accordance with the conditions of Annex I and II of the[Council Regulation (EC) No. 1/2005 of 22 December 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32005R0001).

*For further information*: Regulation 1/2005 and Section R. 214-55 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of conveyor of live animals is reserved for the professional who holds a certificate of completion of training in road transport of live animals, issued by a training organization registered by the Ministry of Agriculture.

The issuance of this certificate of completion of training can be obtained as long as the person is justified:

- or have undergoa specific training delivered by the[registered training organization](https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-957fabc5-1f76-4cab-9c6c-df6cac6eba6b) ;
- or hold one of the diplomas in Part 2 of the Schedule of the November 12, 2005 order.

If necessary, where the conveyor is also responsible for the transport of the animals, it must justify holding a permit adapted to the type of vehicle driven.

**Please note**

The carrier that employs an animal conveyor must have a valid transport authorization. On the other hand, when the conveyor acts on his own behalf, he will have to apply for this transport authorization by filling out the form[Cerfa 15714*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15714.do) which it will refer to the departmental directorate in charge of population protection (DDPP) territorially competent.

*For further information*: Articles 1 and 2 of the[decree of 12 November 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031521436&dateTexte=20171114) relating to the clearances or registrations of training organizations implementing the training required for persons acting as a live animal conveyor; 6 paragraph 4 of Regulation 1/2005.

#### Training

The training should focus on the technical and regulatory aspects of animal transport legislation, including:

- Fitness, terms and conditions and documents for transporting live animals;
- the physiology of the animals, their food needs, watering, their behaviour and the concept of stress;
- The practicalities of handling animals
- the impact of driving on the welfare of the animals being transported and on the quality of meat;
- Emergency animal care
- safety aspects for staff handling animals.

The minimum duration of training is 7 hours for one or even two comparable categories of animals. A minimum of one hour per additional category of comparable animals and 3 hours per additional category of animals are added to this minimum.

*For further information*: Article R. 214-57 of the Rural Code and Marine Fisheries; Article 1 of the decree of 12 November 2015; Schedule IV of the December 22, 2004 regulation.

#### Costs associated with qualification

Training to enter the conveyor profession is paid for. For more information, it is advisable to get closer to the training organizations registered by the Minister responsible for agriculture.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The national of an EU or EEA Member State, carrying animal conveyor activity for species other than domestic equines and domestic animals of the cattle, goat, sheep, swine and poultry species in one of these states, may to make use of his professional title in France, on a temporary or casual basis. He must make a statement to the prefect of the region in which he wishes to practice (see below "5o). a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional must have carried it out in one or more Member States for at least one year, during the ten years which precede the performance.

When the examination of professional qualifications shows substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any EU or EEA national who is established and legally carries out the activity of conveyoring animals in that state may carry out the same activity in France on a permanent basis.

In order to do so, he will have to apply for recognition of his professional qualifications from the territorially competent DDPP (see infra "5°. a. Request recognition of the national's professional qualifications for a permanent exercise (LE)).

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional must have carried it out in one or more Member States for at least one year in the ten years that precede the performance.

If the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subject to compensation measures (see below "5. a. Good to know: compensation measures").

*For further information*: Articles R. 214-57 and R. 204-2 and following of the Rural code and marine fisheries.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Ethical obligations are the responsibility of the animal conveyor, including ensuring the health and dignity of the animals transported.

4°. Insurance
---------------------------------

As part of its duties, the animal conveyor is obliged to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out on occasion.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request recognition of the national's professional qualifications for a permanent exercise (LE)

**Competent authority**

The DDPP of the place of residence is competent to decide on the application for recognition of qualification of the national.

**Supporting documents**

The national will accompany his application for recognition of the following supporting documents:

- A valid piece of identification
- A certificate of competency or a training certificate acquired in the State of origin;
- where the state does not regulate the profession, a document to justify that the national has been an animal conveyor for at least one year in the last ten years.

**Procedure**

Once the DDPP has carried out the checks of the file and has ensured that the person concerned has not been the subject of a withdrawal or deletion of the certificate in his Home State, it will be able to issue him a proof of recognition of qualifications.

**Good to know: compensation measures**
In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice which may be:

- an adaptation course of up to three years
- an aptitude test carried out within six months of notification to the person concerned.

*For further information*: Articles R. 204-2 to R. 204-6 of the Rural Code and Marine Fisheries.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

