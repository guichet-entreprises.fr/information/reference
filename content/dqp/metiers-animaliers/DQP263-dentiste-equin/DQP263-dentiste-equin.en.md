<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP263" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Equine dentist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="equine-dentist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/equine-dentist.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="equine-dentist" -->
<!-- var(translation)="None" -->


# Equine dentist

Latest update: <!-- begin-var(last-update) -->2020-12<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of the activity

The equine dentist is a professional whose activity consists in caring for and maintaining the teeth of horses and in particular:

- remove enamel tips and rough edges from dental tables;
- detect missing, supernumerary or disturbing teeth;
- extract milk teeth and wolf teeth.

However, the administration of sedatives and the performance of local anesthesias are the sole responsibility of a veterinarian registered on the roll of the Order.

## 2°. Professional qualifications

### a. National requirements

#### National legislation

Only the professional with the appropriate skills can exercise the activity of equine dentist. In particular, he must:

- have anatomical and physiological knowledge adapted to the odontostomatology of equines and know how to assess whether the condition of the animal allows its intervention and whether the presence of a veterinarian is required,
- master all the techniques and acts falling within their competence and use the necessary equipment in an appropriate manner while respecting the animal's well-being;
- possess knowledge relating to equine behavior enabling them to carry out an intervention while respecting the animal's well-being, its safety and that of the nursing staff;
- master the techniques of approach, handling and physical restraint of equines.

He must be able to assess whether the condition of the animal authorizes an intervention by knowing how to determine:

- whether the dental procedure is indicated or contraindicated;
- whether the procedure can be performed with or without the presence of a veterinarian.

*To go further:* Articles L. 243-3 and D. 243-5 of the Rural and Maritime Fisheries Code and decree of October 12, 2016 relating to the knowledge and know-how associated with the skills adapted to the performance of dentistry acts on equines.

#### Training

Are deemed to have the appropriate skills to perform equine dentistry, people holding either:

- a professional title "equine dental technician", issued by the public interest group for animal health and veterinary auxiliary training (GIPSA) and the French Federation of technicians equine dental (FFTDE);
- a “farrier” technical certificate for trades (BTM), issued by the Permanent Assembly of Chambers of Trades and Crafts (APCMA).

*To go further:* article 2 and annex to the decree of 12 October 2016.

#### Associated costs

The training to access the activity of equine dentist is not free. For more information, it is advisable to contact training organizations.

### b. EU or EEA nationals: for temporary and occasional exercise (freedom to provide services)

The national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA), exercising the activity of equine dentist in one of these States, can make use of his professional title in France, on a temporary or occasional basis. Prior to his first service, he must make a declaration to the National Council of the Veterinary Order (CNOV).

When neither the activity nor the training leading to this activity is regulated in the State in which he is legally established, the professional must have exercised it in one or more Member States for at least one year, during the ten years which precede the service.

When the examination of professional qualifications reveals substantial differences with regard to the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test.

*To go further:* Articles L. 204-1 and R. 204-1 of the Rural and Maritime Fisheries Code.

### c. EU or EEA nationals: for permanent exercise (Freedom of establishment)

Any EU or EEA national who is established and legally exercises the activity of equine dentist in this State can exercise the same activity in France on a permanent basis.

To do this, he must apply for recognition of his professional qualifications from the CNOV.

When neither the activity nor the training leading to this activity is regulated in the State in which he is legally established, the professional must have exercised it in one or more Member States for at least one year during the ten years which precede the service.

If the examination of professional qualifications reveals substantial differences with regard to the qualifications required for access to the profession and its exercise in France, the person concerned may be subject to compensation measures.

*To go further:* Articles R. 204-2 to R. 204-3 of the Rural and Maritime Fisheries Code.

## 3 °. Conditions of good repute, deontological rules, ethics

The equine dental technician acquires the necessary scientific and technical information necessary to his professional practice, takes it into account in the accomplishment of his mission, maintains and improves his knowledge.

The equine dental technician is required to refer the owner or keeper of the animal to a veterinarian:

- when the symptoms or lesions of the animal require a diagnosis or medical treatment;
- when symptoms or lesions persist or worsen;
- if the disorders presented exceed the scope of the acts he can perform;
- in the event of pain during handling or subsequent pain.

The equine dental technician neither undertakes nor pursues care in areas which do not fall within the scope of equine dentistry or exceed the means at his disposal.

The equine dental technician respects animal welfare, particularly in terms of restraint.

In the scope of the acts that he can perform, he provides the keeper or owner of the animal, the subject of dentistry, with fair, clear and appropriate information on its condition, and ensures that it is understood. The consent of the keeper or owner of the examined or treated animal is sought in all cases.

The equine dental technician advises and informs the keeper or owner of the animal on procedures, in a fair, scientifically supported manner and does not mislead the public, nor abuse their confidence, nor exploit their credulity. , his lack of experience or knowledge.

When called upon to perform dental procedures for the keeper or owner of an equine, he ensures that appropriate hygienic conditions are observed.

## 4 °. Insurance

As part of his duties, the equine dentist is required to take out professional liability insurance.

## 5 °. Qualification recognition process and formalities

### a. Make a prior declaration of activity for the EU or EEA national exercising a temporary and occasional activity (LPS)

#### Competent authority

The prior declaration of activity must be sent, before the first service, to the CNOV.

#### Reply duration

The CNOV has a period of one month from receipt of the file to render its decision:

- to allow the service provider to perform his service. In the event of non-compliance with this one-month period or the silence of the administration, the provision of services can be carried out;
- to submit the person concerned to a compensatory measure in the form of an aptitude test, if it turns out that the qualifications and professional experience on which he relies are substantially different from those required for the exercise the profession in France (see below: “Good to know: compensation measure”);
- inform him of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, from the resolution of the difficulty (s) and in any event within a maximum period of three months from the information of the interested party as to the existence of the difficulty (s). In the absence of a response from the competent authority within this period, the provision of services may begin.

#### Vouchers

The prior declaration of the national must be sent by any means to the competent authority and include the following supporting documents:

- proof of the professional's nationality;
- a certificate certifying that he:
  - is legally established in an EU or EEA state,
  - exercises one or more professions the exercise of which in France requires the possession of a certificate of competence,
  - does not incur any prohibition to practice, even temporarily, when the certificate is issued;
- proof of their professional qualifications;
- when neither the professional activity nor the training is regulated in the EU or EEA State, proof by any means that the national has carried out this activity for one year, full time or part time , over the past ten years;
- a declaration of commitment to activity of a temporary or occasional nature lasting less than one year.

This prior declaration includes information relating to insurance or other means of personal or collective protection taken out by the declarant to cover his professional liability.

To these documents is attached, as necessary, their translation into French.

*To go further:* Articles R. 204-1 of the Rural and Maritime Fishing Code.

### b. Steps to take with a view to Freedom of establishment

Anyone wishing to practice equine dentistry is required to sign a one-year agreement, tacitly renewed, with the president of the National Council of the Veterinary Order, after the FFTDE has ensured that the title of dental technician is held. equine by the applicant.

The President of the National Council of the Order of Veterinarians designates a veterinary doctor per region who is the point of reference for all the equine dentists signatory to the agreement established in said region.

The role of this regional veterinary doctor is to be the privileged interlocutor of equine dentists who have signed the agreement with the president of the National Council of the Order, with regard to the conditions of their interventions. In particular, the referring veterinary doctor sees to the establishment of exchanges and annual continuing training for equine dentists in his district in consultation with the French Federation of equine dental technicians and technical veterinary organizations.

### c. Remedies

#### French help center

The ENIC-NARIC Center is the French information center on the academic and professional recognition of diplomas.

#### SOLVIT

SOLVIT is a service provided by the National Administration of each EU Member State or party to the EEA Agreement. Its objective is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT is particularly involved in the recognition of professional qualifications.

**Conditions**

The interested party can only use SOLVIT if he establishes:

- that the public administration of one EU state has failed to respect its rights under EU law as a citizen or business of another EU state;
- that he has not already initiated legal action (administrative appeal is not considered to be such).

**Procedure**

The national must complete an online complaint form. Once his file has been transmitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem falls within its competence.

**Vouchers**

To enter SOLVIT, the national must communicate:

- its full contact details;
- a detailed description of the problem;
- all the evidence in the case (for example, correspondence and decisions received from the administrative authority concerned).

**Time limit**

SOLVIT undertakes to find a solution within ten weeks of the day on which the case is taken over by the SOLVIT center in the country in which the problem arose.

**Cost**

Free.

**Outcome of the procedure**

At the end of the ten week period, SOLVIT presents a solution:

- If this solution settles the dispute concerning the application of European law, the solution is accepted and the case is closed;
- If there is no solution, the case is closed as unresolved and referred to the European Commission.

**Additional Information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](http://www.sgae.gouv.fr/sites/SGAE/accueil.html)).
