﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP263" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Dentiste équin" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="dentiste-equin" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/dentiste-equin.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="dentiste-equin" -->
<!-- var(translation)="None" -->

# Dentiste équin

## 1°. Définition de l’activité

Le dentiste  équin est un professionnel dont l’activité consiste à soigner et entretenir la dentition des chevaux et notamment :

- éliminer les pointes d’émail et les aspérités des tables dentaires ;
- détecter les dents absentes, surnuméraires ou gênantes ;
- extraire les dents de lait et les dents de loup.

Toutefois l’administration de sédatif et la réalisation d'anesthésies locales relèvent exclusivement de la compétence d’un vétérinaire inscrit au tableau de l’Ordre.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Seul le professionnel détenant des compétences adaptées peut exercer l’activité de dentiste équin. Il doit notamment :

- détenir des connaissances anatomiques et physiologiques adaptées à l'odontostomatologie des équidés et savoir évaluer si l'état de l'animal autorise son intervention et si la présence d'un vétérinaire est requise ;
- maîtriser l'ensemble des techniques et des actes relevant de leurs compétences et utiliser le matériel nécessaire de façon adéquate dans le respect du bien-être de l'animal ;
- posséder des connaissances relatives au comportement de l'équidé leur permettant de mener à bien une intervention en respectant le bien-être de l'animal, sa sécurité et celle des personnels soignants ;
- maîtriser les techniques d'approche, de manipulation et de contention physique des équidés.

Il doit être en mesure d'évaluer si l'état de l'animal autorise une intervention en sachant déterminer :

- si l'intervention dentaire est indiquée ou contre-indiquée ;
- si l'intervention peut se pratiquer avec ou sans la présence d'un vétérinaire.

*Pour aller plus loin* : articles L. 243-3 et D. 243-5 du Code rural et de la pêche maritime et arrêté du 12 octobre 2016 relatif aux connaissances et savoir-faire associés aux compétences adaptées à la réalisation d'actes de dentisterie sur les équidés.

#### Formation 

Sont réputés détenir les compétences adaptées pour effectuer des actes de dentisterie équine, les personnes détenant soit :

- un titre à finalité professionnelle « technicien dentaire équin », délivré par le groupement d'intérêt public formation santé animale et auxiliaire vétérinaire (GIPSA) et la Fédération française des techniciens dentaires équins (FFTDE) ;
- un brevet technique des métiers (BTM) « maréchal-ferrant », délivré par l'Assemblée permanente des chambres de métiers et de l'artisanat (APCMA).

*Pour aller plus loin* : article 2 et annexe de l’arrêté du 12 octobre 2016.

#### Coûts associés

La formation pour accéder à l’activité de dentiste équin est payante. Pour plus d'informations, il est conseillé de se rapprocher des organismes de formation.

### b. Ressortissants de l’UE ou de l’EEE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État membre de l’Union européenne (UE) ou partie à l'accord sur l’Espace économique européen (EEE), exerçant l’activité de dentiste équin dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit effectuer, préalablement à sa première prestation, une déclaration adressée au Conseil national de l’Ordre vétérinaire (CNOV).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude.

*Pour aller plus loin* : articles L. 204-1 et R. 204-1 du Code rural et de la pêche maritime.

### c. Ressortissants de l’UE ou de l’EEE : en vue d’un exercice permanent (Libre Établissement) 

Tout ressortissant de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de dentiste équin dans cet État peut exercer en France la même activité de manière permanente. 

Pour cela, il doit demander la reconnaissance de ses qualifications professionnelles auprès du CNOV.

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel doit l’avoir exercée dans un ou plusieurs États membres pendant au moins un an au cours des dix années qui précèdent la prestation. 

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé peut être soumis à des mesures de compensation.

*Pour aller plus loin* : articles R. 204-2 à R. 204-3 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le technicien dentaire équin acquiert l’information scientifique et technique nécessaire à son exercice professionnel, en tient compte dans l’accomplissement de sa mission, entretient et perfectionne ses connaissances.

Le technicien dentaire équin est tenu d’orienter le propriétaire ou le détenteur de l’animal vers un vétérinaire :

- lorsque les symptômes ou les lésions de l’animal nécessitent un diagnostic ou un traitement médical ;
- lorsqu’il est constaté une persistance ou une aggravation de symptômes ou de lésions ;
- si les troubles présentés excèdent le champ des actes qu’il peut accomplir ;
- en cas de douleur durant les manipulations ou de douleur consécutive à ces dernières.

Le technicien dentaire équin n’entreprend ni ne poursuit des soins dans des domaines qui ne relèvent pas de la dentisterie des équidés ou dépassent les moyens dont il dispose.

Le technicien dentaire équin respecte le bien-être animal notamment en matière de contention.

Dans le champ des actes qu’il peut accomplir, il fournit au détenteur ou au propriétaire de l’animal, objet d’actes de dentisterie, une information loyale, claire et appropriée sur son état, et veille à sa compréhension. Le consentement du détenteur ou du propriétaire de l’animal examiné ou soigné est recherché dans tous les cas.

Le technicien dentaire équin conseille et informe le détenteur ou le propriétaire de l’animal sur des procédés, de façon loyale, scientifiquement étayée et n’induit pas le public en erreur, ni n’abuse de sa confiance, ni n’exploite sa crédulité, son manque d’expérience ou de connaissances.

Lorsqu’il est appelé à réaliser des actes de dentisterie chez le détenteur ou le propriétaire d’un équidé, il s’assure du respect de conditions d’hygiène adaptées.


## 4°. Assurances

Dans le cadre de ses fonctions, le dentiste équin a l’obligation de souscrire une assurance de responsabilité professionnelle. 

## 5°. Démarche et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

La déclaration préalable d’activité doit être adressée, avant la première prestation, au CNOV.

#### Délais de réponse

Le CNOV dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision :

- de permettre au prestataire d’effectuer sa prestation.  En cas de non-respect de ce délai d’un mois ou du silence de l’administration, la prestation de services peut être effectuée ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France (cf. infra « Bon à savoir : mesure de compensation ») ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés et en tout état de cause dans un délai maximum de trois mois à compter de l’information de l’intéressé quant à l’existence de la ou les difficultés. En l'absence de réponse de l'autorité compétente dans ce délai, la prestation de services peut débuter.

#### Pièces justificatives

La déclaration préalable du ressortissant devra être transmise par tout moyen à l'autorité compétente et comprendre les pièces justificatives suivantes :

- une preuve de la nationalité du professionnel ;
- une attestation certifiant qu'il :
  - est légalement établi dans un État de l’UE ou de l’EEE,
  - exerce une ou plusieurs professions dont l'exercice en France nécessite la détention d'un certificat de capacité,
  - n'encourt pas d’interdiction d'exercer, même temporaire, lors de la délivrance de l'attestation ;
- une preuve de ses qualifications professionnelles ;
- lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE, la preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années ;
- une déclaration d'engagement d'activité à caractère temporaire ou occasionnel d'une durée inférieure à un an.

Cette déclaration préalable comprend les informations relatives aux assurances ou autres moyens de protection personnelle ou collective souscrits par le déclarant pour couvrir sa responsabilité professionnelle.

À ces documents est jointe, en tant que de besoin, leur traduction en langue française.

*Pour aller plus loin* : articles R. 204-1 du Code rural et de la pêche maritime.

### b. Démarches à effectuer en vue d’un Libre Établissement

Toute personne souhaitant exercer une activité de dentisterie équine est tenue de signer une convention d’un an, reconduite tacitement, avec le président du Conseil national de l’Ordre vétérinaire, après que la FFTDE se soit assurée de la détention du titre de technicien dentaire équin par le demandeur.

Le président du Conseil national de l’Ordre des vétérinaires désigne un docteur vétérinaire par région qui est le référent pour l’ensemble des dentistes équins signataires de la convention établis dans ladite région.

Ce docteur vétérinaire référent régional a pour fonction d’être l’interlocuteur privilégié des dentistes équins signataires de la convention avec le président du Conseil national de l’Ordre, en ce qui concerne les conditions de leurs interventions. En particulier, le docteur vétérinaire référent veille à la mise en place d’échanges et de formation continue annuelle des dentistes équins de sa circonscription en concertation avec la Fédération Française des techniciens dentaires équins et les organisations vétérinaires techniques.

### c. Voies de recours 

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](http://www.sgae.gouv.fr/sites/SGAE/accueil.html)).