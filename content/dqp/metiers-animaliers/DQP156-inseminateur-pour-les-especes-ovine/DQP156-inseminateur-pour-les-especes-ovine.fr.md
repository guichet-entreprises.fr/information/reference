﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP156" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Inséminateur pour les espèces ovine, bovine et caprine" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="inseminateur-pour-les-especes-ovine" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/inseminateur-pour-les-especes-ovine-bovine-et-caprine.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="inseminateur-pour-les-especes-ovine-bovine-et-caprine" -->
<!-- var(translation)="None" -->

# Inséminateur pour les espèces ovine, bovine et caprine

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l'activité

L'inséminateur est un professionnel dont la mission principale est de réaliser l'acte d'insémination artificielle sur les espèces bovine, ovine ou caprine.

Il doit assurer la gestion et la traçabilité des doses du dépôt de semence déclaré, dont il transmet par la suite les données au système national d'information génétique spécifique à l'espèce inséminée.

*Pour aller plus loin* : annexe III de l'arrêté du 18 janvier 2007 relatif à la création de la commission chargée de l'attribution du certificat d'aptitude aux fonctions de technicien d'insémination dans les espèces bovine, caprine et ovine.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne qui souhaite devenir inséminateur doit être en possession d'un certificat d'aptitude aux fonctions de technicien d’insémination (Cafti) acquis auprès d'un [centre d'évaluation](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1718413F499D82003CA70C796608A682.tplgfr38s_2?idArticle=LEGIARTI000006610101&cidTexte=LEGITEXT000006055588&dateTexte=20171204) habilité par le ministère chargé de l'agriculture.

*Pour aller plus loin* : article R. 653-87 du Code rural et de la pêche maritime.

#### Formation

Le Cafti est délivré selon trois modalités :

- sur titre pour les espèces bovine, ovine et caprine dès lors que l’intéressé justifie être titulaire, soit :
  - du diplôme d’État de Docteur vétérinaire ou de l'un des diplômes mentionnés à l'article L. 241-2 du Code rural et de la pêche maritime, et délivrés par un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE),
  - du certificat d'aptitude aux fonctions d'inséminateur ou la licence d'inséminateur pour l'espèce concernée,
  - du certificat d'aptitude aux fonctions de chef de centre ou la licence de chef de centre pour l'espèce concernée ;
- par validation de l’expérience professionnelle si l’intéressé justifie d’au moins trois années à temps plein d’expérience professionnelle et après examen du dossier par le centre d’évaluation qui validera ou non cette expérience après entretien avec le candidat ;
- par la réussite à une évaluation précédée ou non par une formation.

**À noter**

L’évaluation est constituée de trois épreuves :

- la partie écrite comporte deux épreuves de type questionnaire à choix multiples (QCM) : l'une sur la réglementation et l'autre sur les connaissances scientifiques et techniques ; le candidat a réussi s'il obtient au moins 12/20 à chacune des épreuves ;
- la partie pratique, comportant une mise en situation professionnelle d'une durée de trente minutes, donne lieu à une évaluation par une grille ; le candidat a réussi si 75 % au moins des points visés ont été réalisés correctement. Certains points sont éliminatoires en cas d’échec. 

*Pour aller plus loin* : article 9 et annexe II de l'arrêté du 18 janvier 2007 précité.

#### Coûts associés à la qualification

Pour plus d'informations, il est conseillé de se rapprocher des organismes de formation.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le ressortissant d’un État de l'UE ou de l’EEE exerçant l’activité d’inséminateur dans l’un de ces États peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra, préalablement à sa première prestation, effectuer une déclaration d’activité auprès du centre d'évaluation (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an au cours des dix années qui précèdent la prestation.

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la demande de déclaration par le préfet.

*Pour aller plus loin* : articles R. 653-87 et R. 204-1 du Code rural et de la pêche maritime.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre de formation ou d'une attestation de compétence délivré par une autorité compétente d'un autre État membre qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation ni l'exercice de la profession.

Dès lors qu'il remplit l'une des deux conditions précédentes, il devra obtenir le Cafti requis pour l'exercice de la profession, auprès du centre d'évaluation compétent (cf. infra « 5°. b. Obtenir le Cafti pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE) »). Si, lors de l'examen du dossier, le préfet constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées pour exercer en France, des mesures de compensation pourront être prises (« 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : paragraphe II de l'article R. 653-87 du Code rural et de la pêche maritime et article 11 de l'arrêté du 18 janvier 2007.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le vétérinaire qui exerce une mission d'insémination pour les espèces bovine, ovine ou caprine est soumis aux règles déontologiques et d'éthique applicables à la profession.

Pour les inséminateurs autres que les vétérinaires, des obligations éthiques leur incombent. Ils doivent notamment respecter le secret professionnel lié aux données d'insémination et la réglementation concernant le bien-être, l'hygiène, la sécurité et le maintien de la dignité de l'animal inséminé.

## 4°. Assurance

En cas d’exercice libéral, l’inséminateur a l’obligation de souscrire une assurance de responsabilité professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le centre d'évaluation habilité par le ministre chargé de l'agriculture est compétent pour se prononcer sur la demande déclaration.

#### Renouvellement de la déclaration préalable

Elle doit être renouvelée une fois par an et à chaque changement de situation professionnelle.

#### Pièces justificatives

La déclaration préalable du ressortissant devra être transmise par tout moyen à l'autorité compétente et comprendre les pièces justificatives suivantes :

- une preuve de la nationalité du professionnel ;
- une attestation certifiant qu'il :
  - est légalement établi dans un État de l’UE ou de l’EEE,
  - exerce une ou plusieurs professions dont l'exercice en France nécessite la détention d'un certificat de capacité,
  - n'encourt pas d’interdiction d'exercer, même temporaire, lors de la délivrance de l'attestation ;
- une preuve de ses qualifications professionnelles ;
- lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE, la preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années.

Cette déclaration préalable comprend les informations relatives aux assurances ou autres moyens de protection personnelle ou collective souscrits par le déclarant pour couvrir sa responsabilité professionnelle.

À ces documents est jointe, en tant que de besoin, leur traduction en langue française. 

#### Délai

Le centre d'évaluation dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision :

- de permettre au prestataire d’effectuer sa prestation ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés.

En l'absence de réponse de l'autorité compétente dans ces délais, la prestation de service peut débuter.

*Pour aller plus loin* : articles R. 204-1 et R. 653-87 paragraphe III du Code rural et de la pêche maritime.

### b. Obtenir le Cafti pour les ressortissants de l’UE ou de l'EEE exerçant une activité permanente (LE)

#### Autorité compétente

Le centre d'évaluation habilité par le ministre chargé de l'agriculture est compétent pour délivrer le Cafti au ressortissant de l'UE ou de l'EEE qui souhaite s'établir en France et exercer la profession d'inséminateur.

#### Procédure

Le ressortissant devra transmettre au centre d'évaluation toutes les pièces nécessaires à l'appui de sa demande de certificat, et notamment :

- une preuve de sa nationalité ;
- un titre de formation ou une attestation de compétence acquis dans un État membre de l'UE ou de l'EEE ;
- toute attestation justifiant, le cas échéant, que le ressortissant a exercé l'activité d’inséminateur pendant un an à temps plein ou à temps partiel au cours des dix dernières années dans un État membre qui ne réglemente pas la profession.

Après étude du dossier, le centre se prononcera sur la recevabilité de la demande. Il pourra décider :

- soit de l'octroi du Cafti au ressortissant ;
- soit de la nécessité de recourir à une mesure de compensation en cas de différences substantielles entre la formation de l'intéressé et celle exigée en France.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de compensation, qui peut être :

- un stage d'adaptation d'une durée maximale de trois ans ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

Le centre d'évaluation pourra décider ou laisser le choix de la décision de la mesure de compensation au ressortissant dans les conditions des articles R. 204-4 et R. 204-5 du Code rural et de la pêche maritime.

*Pour aller plus loin* : articles R. 204-2 et R. 653-87 paragraphe II du Code rural et de la pêche maritime.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).