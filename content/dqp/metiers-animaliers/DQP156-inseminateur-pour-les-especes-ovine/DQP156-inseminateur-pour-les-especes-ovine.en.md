﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP156" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Sheep, cattle and goat inseminator" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="sheep-cattle-and-goat-inseminator" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/sheep-cattle-and-goat-inseminator.html" -->
<!-- var(last-update)="2020-04-15 17:20:56" -->
<!-- var(url-name)="sheep-cattle-and-goat-inseminator" -->
<!-- var(translation)="Auto" -->


Sheep, cattle and goat inseminator
==============================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:56<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The inseminator is a professional whose main mission is to carry out the act of artificial insemination on the bovine, sheep or goat species.

It must manage and trace the doses of the declared seed deposit, which it then transmits data to the national genetic information system specific to the inseminated species.

*For further information*: Appendix III of the[decreed from 18 January 2007](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000271038&dateTexte=20171204) concerning the creation of the commission responsible for the award of the Certificate of Fitness to The Functions of Insemination Technician in cattle, goat and sheep species.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Anyone who wishes to become an inseminator must be in possession of a certificate of aptitude for the duties of insemination technician (Cafti) acquired from a[assessment centre](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1718413F499D82003CA70C796608A682.tplgfr38s_2?idArticle=LEGIARTI000006610101&cidTexte=LEGITEXT000006055588&dateTexte=20171204) authorized by the Ministry of Agriculture.

*For further information*: Article R. 653-87 of the Rural Code and Marine Fisheries.

#### Training

The Cafti is issued on three terms:

- for bovine, sheep and goat species as long as the person claims to be the holder, i.e.:- the State Diploma of Veterinary Doctor or one of the diplomas mentioned in Article L. 241-2 of the Rural Code and Maritime Fisheries, issued by a Member State of the European Union (EU) or party to the European Economic Area (EEA),
  - Certificate of fitness for inseminator functions or inseminator licence for the species concerned,
  - Certificate of fitness for the functions of head of centre or the license of a centre manager for the species concerned;
- by validation of professional experience if the individual justifies at least three years of full-time work experience and after review of the file by the evaluation centre that will or will not validate this experience after interview with the candidate ;
- success in an evaluation preceded or not by training.

**Please note**

The evaluation consists of three tests:

- The written part consists of two multiple-choice questionnaire (MQ) tests: one on regulation and the other on scientific and technical knowledge; The candidate has succeeded if he or she gets at least 12/20 in each of the tests;
- The practical part, which involves a 30-minute professional situation, results in a grid evaluation; the candidate was successful if at least 75% of the target points were achieved correctly. Some points are elimination in case of failure.

*For further information*: Article 9 and Appendix II of the January 18, 2007 order.

#### Costs associated with qualification

For more information, it is advisable to get closer to the training organizations.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

A national of an EU or EEA state practising the inseminator activity in one of these states may use his or her professional title in France, either temporarily or occasionally.

Prior to his first performance, he will have to make a declaration of activity with the evaluation centre (see infra "5°. a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional must have carried it out in one or more Member States for at least one year in the ten years that precede the performance.

If the examination of professional qualifications shows substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test within a period of time. one month from the prefect's receipt of the request for declaration.

*For further information*: Articles R. 653-87 and R. 204-1 of the Rural and Marine Fisheries Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a training certificate or certificate of competence issued by a competent authority in another Member State that regulates access to or exercise of the profession;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once he fulfils one of the two previous conditions, he will have to obtain the Cafti required for the practice of the profession, from the competent evaluation centre (see infra "5°. b. Obtain Cafti for EU or EEA nationals for a permanent exercise (LE)). If, during the examination of the file, the prefect finds that there are substantial differences between the training and professional experience of the national and those required to practise in France, compensation measures may be taken ("5°). b. Good to know: compensation measures").

*For further information*: ( paragraph II of Article R. 653-87 of the Rural and Marine Fisheries Code and Article 11 of the January 18, 2007 Order.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Veterinarians who carry out an insemination mission for bovine, sheep or goat species are subject to ethical and ethical rules applicable to the profession.

For inseminators other than veterinarians, they have ethical obligations. In particular, they must respect the professional secrecy associated with insemination data and regulations concerning the welfare, hygiene, safety and maintenance of the dignity of the inseminated animal.

4°. Insurance
---------------------------------

In the event of a liberal exercise, the inseminator has an obligation to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during this activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

#### Competent authority

The assessment centre authorized by the Minister responsible for agriculture is competent to decide on the request declaration.

#### Renewal of pre-declaration

It must be renewed once a year and with each change in employment status.

#### Supporting documents

The national's prior declaration must be forwarded by any means to the competent authority and include the following supporting documents:

- Proof of the professional's nationality
- a certificate certifying that it:- is legally established in an EU or EEA state,
  - practises one or more professions whose practice in France requires the holding of a certificate of ability,
  - does not incur a ban on practising, even if temporary, when issuing the certificate;
- proof of his professional qualifications
- where neither professional activity nor training is regulated in the EU or EEA State, evidence by any means that the national has been engaged in this activity for one year, full-time or part-time, in the last ten years.

This advance declaration includes information relating to insurance or other means of personal or collective protection underwritten by the registrant to cover his professional liability.

These documents are attached, as needed, to their translation into the French language.

#### Time

The evaluation centre has one month from receipt of the file to make its decision:

- Allow the claimant to perform his or her benefit;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the qualifications and work experience he uses are substantially different from those required for the exercise of the profession in France;
- inform them of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, as of the resolution of the difficulties.

In the absence of a response from the competent authority within these timeframes, service delivery may begin.

*For further information*: Articles R. 204-1 and R. 653-87 paragraph III of the Rural code and marine fisheries.

### b. Obtain Cafti for EU or EEA nationals with permanent activity (LE)

#### Competent authority

The assessment centre authorised by the Minister for Agriculture is responsible for issuing the Cafti to the EU or EEA national who wishes to settle in France and practise as an inseminator.

#### Procedure

The national will be required to provide the assessment centre with all the necessary documents to support his application for a certificate, including:

- proof of nationality
- A training certificate or certificate of competence acquired in an EU or EEA member state;
- any evidence, if any, that the national has been an inseminator for one year full-time or part-time in the last ten years in a Member State that does not regulate the profession.

After reviewing the file, the centre will decide on the admissibility of the application. He can decide:

- or the granting of the Cafti to the national;
- or the need to resort to a compensation measure in case of substantial differences between the training of the person concerned and that required in France.

#### Good to know: compensation measures

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure, which may be:

- an adaptation course of up to three years
- an aptitude test carried out within six months of notification to the person concerned.

The evaluation centre may decide or leave the decision of the compensation measure to the national under the terms of the articles[R. 204-4](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A0EB9EE48CC8DFB61D663334A035FF06.tplgfr40s_1?idArticle=LEGIARTI000031699999&cidTexte=LEGITEXT000006071367&dateTexte=20171205&categorieLien=id&oldAction=&nbResultRech=) And[R. 204-5](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A0EB9EE48CC8DFB61D663334A035FF06.tplgfr40s_1?idArticle=LEGIARTI000034398078&cidTexte=LEGITEXT000006071367&dateTexte=20171205&categorieLien=id&oldAction=&nbResultRech=) Code of The Rural Code and Maritime Fisheries.

*For further information*: Articles R. 204-2 and R. 653-87 paragraph II of the Rural and Marine Fisheries Code.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

