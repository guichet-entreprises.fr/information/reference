﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP155" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Inséminateur équin" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="inseminateur-equin" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/inseminateur-equin.html" -->
<!-- var(last-update)="2020-04-15" -->
<!-- var(url-name)="inseminateur-equin" -->
<!-- var(translation)="None" -->

# Inséminateur équin

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15<!-- end-var -->

## 1°. Définition de l'activité

L'inséminateur équin est en charge de l'insémination artificielle des juments.

Travaillant en centre d'insémination artificielle équine ou directement chez les éleveurs, il a pour mission de collecter la semence des mâles, de la traiter en laboratoire puis de procéder à l'insémination des femelles.

Pour plus d'informations sur les missions de l'inséminateur équin, il est conseillé de se reporter au [site de l'Institut français du cheval et de l'équitation](http://www.haras-nationaux.fr/information/accueil-equipaedia/formations-et-metiers/les-metiers-de-lelevage/inseminateur-equin.html) (IFCE).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne qui souhaite devenir inséminateur équin doit s'être déclarée auprès de l'autorité administrative (le préfet de la région dans laquelle elle souhaite exercer), qui procédera à son enregistrement au vu de la présentation d’une licence d’inséminateur.

Cette licence ne peut être délivrée qu’aux titulaires :

- du certificat d’aptitude aux fonctions d’inséminateur équin délivré par un établissement de formation spécialisé ;
- d’un des diplômes de vétérinaire mentionnés à l'article L. 241-2 du Code rural et de la pêche maritime ;
- de la licence de chef de centre d'insémination artificielle pour les espèces équines ou asines.

*Pour aller plus loin* : articles L. 653-13 et R. 653-87 du Code rural et de la pêche maritime et arrêté du 21 janvier 2014 relatif aux certificats d'aptitude aux fonctions d'inséminateur et de chef de centre d'insémination artificielle dans les espèces équine et asine.

#### Formation

La licence d’inséminateur est soumise à l’obtention du certificat d'aptitude délivré à l'issue d'une formation suivie dans l'un des établissements suivants :

- le centre de formation de la jumenterie de l'Institut français du cheval et de l’équitation ;
- le centre d'enseignement zootechnique de Rambouillet ;
- l'une des quatre écoles nationales vétérinaires.

L'accès à la formation peut être limitée selon le nombre de places disponibles et intervient après décision du directeur de l'un de ces établissements.

L'admission peut se faire :

- soit après examen du dossier de l'intéressé, sous réserve qu'il justifie d’un diplôme de niveau IV ou plus du ministère chargé de l'agriculture, dans le domaine des productions animales ;
- soit après un contrôle des connaissances acquises pour :
  - les titulaires d'un certificat d'aptitude aux fonctions d'inséminateur pour une autre espèce animale,
  - les personnes ayant exercé une activité professionnelle agricole dans l'élevage équin pendant au moins trois ans.

**À savoir**

Le contrôle des connaissances porte sur les matières définies à l'annexe I de l'arrêté du 21 janvier 2014 et comporte un entretien de motivation.

La durée de la formation est de cinq semaines durant lesquelles le candidat devra atteindre des objectifs en matière de physiologie et biotechnologies de la reproduction, de zootechnie, de réglementation et de relation conseil.

À l'issue de cette formation, le candidat devra réussir un examen composé d'épreuves théoriques, pratiques et orales.

L'admission est prononcée dès lors que l'intéressé a obtenu une note moyenne de 12 sur 20, et ce, sans note inférieure à 7 sur 20 à une épreuve théorique ou 10 sur 20 à l'épreuve pratique.

**À noter**

Pour connaître les formations menant aux professions de vétérinaire et de chef de centre d'insémination artificielle équine ou asine en France, il est conseillé de se reporter aux fiches qualifications qui leurs sont consacrées.

*Pour aller plus loin* : arrêté du 21 janvier 2014.

#### Coûts associés à la qualification

La formation pour devenir inséminateur équin est payante. Pour plus de renseignements sur les frais de formation, il est conseillé de se rapprocher des établissements spécialisés.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le ressortissant d’un État membre de l’UE ou de l’EEE, exerçant l’activité d’inséminateur équin dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet de la région dans laquelle il souhaite exercer (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la demande de déclaration par le préfet.

*Pour aller plus loin* : articles L. 653-13, L. 204-1 et R. 204-1 du Code rural et de la pêche maritime.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre de formation ou d'une attestation de compétence délivrés par une autorité compétente d'un autre État membre qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une des deux conditions précédentes, il devra obtenir la licence d'inséminateur équin requise pour l'exercice de la profession, auprès du préfet de région compétent. Pour plus d'information, il est conseillé de se reporter infra au paragraphe « 5°. b. Obtenir une licence pour les ressortissants de l’UE ou de l'EEE en vue d’un exercice permanent (LE) ».

Si, lors de l'examen du dossier, le préfet constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées pour exercer en France, des mesures de compensation pourront être prises (« 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : article R. 204-2 et R. 653-96 du Code rural et de la pêche maritime.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le vétérinaire qui exerce une mission d'insémination équine est soumis aux règles déontologiques et d'éthique applicables à la profession.

Pour les inséminateurs autres que les vétérinaires, des obligations éthiques leur incombent et notamment de respecter le secret professionnel lié aux données d'insémination et de veiller à la santé et au maintien de la dignité de l'animal inséminé.

## 4°. Assurance

En cas d’exercice libéral, l’inséminateur équin a l’obligation de souscrire une assurance de responsabilité professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet de région est compétent pour se prononcer sur la déclaration préalable.

#### Pièces justificatives

La déclaration préalable du ressortissant devra être transmise par tout moyen à l'autorité compétente et comprendre les pièces justificatives suivantes :

- une preuve de la nationalité du professionnel ;
- une attestation certifiant qu'il :
  - est légalement établi dans un État de l’UE ou de l’EEE,
  - exerce une ou plusieurs professions dont l'exercice en France nécessite la détention d'un certificat de capacité,
  - n'encourt pas d’interdiction d'exercer, même temporaire, lors de la délivrance de l'attestation ;
- une preuve de ses qualifications professionnelles ;
- lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE, la preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années.

Cette déclaration préalable comprend les informations relatives aux assurances ou autres moyens de protection personnelle ou collective souscrits par le déclarant pour couvrir sa responsabilité professionnelle.

À ces documents est jointe, en tant que de besoin, leur traduction en langue française.

#### Délai

Le préfet de région dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision :

- de permettre au prestataire d’effectuer sa prestation.  En cas de non-respect de ce délai d’un mois ou du silence de l’administration, la prestation de services peut être effectuée ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés et en tout état de cause dans un délai maximum de trois mois à compter de l’information de l’intéressé quant à l’existence de la ou des difficultés. En l'absence de réponse de l'autorité compétente dans ces délais, la prestation de service peut débuter.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime

### b. Obtenir une licence pour les ressortissants de l’UE exerçant une activité permanente (LE) 

#### Autorité compétente

Le préfet de région du lieu d'exercice est compétent pour délivrer la licence permettant au ressortissant d'exercer de manière permanente l'activité d'inséminateur équin en France.

#### Procédure

Le ressortissant devra transmettre au préfet toutes les pièces nécessaires à l'appui de sa demande de licence et notamment :

- une preuve de sa nationalité ;
- un titre de formation ou une attestation de compétence acquis dans un État membre de l'UE ou de l'EEE ;
- toute attestation justifiant, le cas échéant, que le ressortissant a exercé l'activité d’inséminateur pendant un an, à temps plein ou à temps partiel, dans un État membre qui ne réglemente pas la profession.

Dès lors, le préfet disposera d'un délai d'un mois à compter de la réception des éléments justificatifs pour accuser réception ou demander l'envoi de pièces manquantes. La décision d’octroyer la licence interviendra ensuite dans un délai de trois mois, prolongé d'un mois supplémentaire en cas de pièces manquantes. Le silence gardé dans ces délais vaudra acceptation.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de de compensation de son choix, qui peut être :

- un stage d'adaptation d'une durée maximale de trois ans ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : articles R. 204-2 à R. 204-6 du Code rural et de la pêche maritime.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).