﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP155" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Horse inseminator" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="horse-inseminator" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/horse-inseminator.html" -->
<!-- var(last-update)="2020-04-15 17:20:55" -->
<!-- var(url-name)="horse-inseminator" -->
<!-- var(translation)="Auto" -->


Horse inseminator
==================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:55<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The equine inseminator is in charge of the artificial insemination of mares.

Working in an equine artificial insemination centre or directly in breeders, his mission is to collect the seed of the males, treat it in the laboratory and then proceed with the insemination of females.

For more information on the missions of the equine inseminator, it is advisable to refer to the[site of the French Institute of Horse and Horse riding](http://www.haras-nationaux.fr/information/accueil-equipaedia/formations-et-metiers/les-metiers-de-lelevage/inseminateur-equin.html) (IFCE).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Any person who wishes to become an equine inseminator must have registered with the administrative authority (the prefect of the region in which he wishes to practice), who will register him in view of the presentation of a licence. inseminator.

This license can only be issued to licensees:

- Certificate of fitness for equine inseminator functions issued by a specialized training institution;
- one of the veterinary diplomas mentioned in Article L. 241-2 of the Rural Code and Marine Fisheries;
- artificial insemination centre manager's license for equine or asine species.

*For further information*: Articles L. 653-13 and R. 653-87 of the Rural and Marine Fisheries Code and Order of January 21, 2014 relating to certificates of fitness for inseminator and artificial insemination centre in equine and asine species.

#### Training

The inseminator's licence is subject to the certificate of aptitude issued after training at one of the following institutions:

- the mare training centre of the French Horse and Riding Institute;
- The Rambouillet Zootechnical Education Centre;
- one of the four national veterinary schools.

Access to training may be limited depending on the number of places available and comes after the decision of the director of one of these institutions.

Admission can be made:

- either after reviewing the person's file, provided that he justifies a level IV diploma or more from the Ministry of Agriculture, in the field of animal production;
- or after a check of the acquired knowledge for:- Holders of a certificate of fitness to be an inseminator for another animal species,
  - people who have worked in equine farming for at least three years.

**What to know**

Knowledge control covers the subjects defined in Schedule I of the January 21, 2014 decree and includes a motivational interview.

The duration of the training is five weeks during which the candidate will have to achieve objectives in reproductive physiology and biotechnology, zootechnics, regulation and consulting relations.

At the end of this training, the candidate will have to pass an exam consisting of theoretical, practical and oral tests.

Admission is pronounced if the person has obtained an average score of 12 out of 20, with no score less than 7 out of 20 in a theoretical test or 10 out of 20 in the practical test.

**Please note**

To find out the training courses leading to the professions of veterinarian and head of artificial insemination center equine or asine in France, it is advisable to refer to the qualification sheets dedicated to them.

*For further information*: ordered from 21 January 2014.

#### Costs associated with qualification

Training to become an equine inseminator pays off. For more information on training costs, it is advisable to approach specialized institutions.

### b. EU or EEA nationals: for temporary and occasional exercise (Free Service)

The national of an EU or EEA Member State, who is engaged in equine inseminator activity in one of these states, may use his or her professional title in France on a temporary or casual basis. He must apply for it, prior to his first performance, by declaration addressed to the prefect of the region in which he wishes to practice (see infra "5°. a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional must have carried it out in one or more Member States for at least one year, during the ten years which precede the performance.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test in a one month from the prefect's receipt of the request for declaration.

*For further information*: Articles L. 653-13, L. 204-1 and R. 204-1 of the Rural and Marine Fisheries Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a training certificate or certificate of competency issued by a competent authority in another Member State that regulates access to the profession or its exercise;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once he fulfils one of the two previous conditions, he will have to obtain the equine inseminator licence required for the practice of the profession, from the competent regional prefect. For more information, it is advisable to refer to paragraph "5o" below. b. Obtain a licence for EU or EEA nationals for a permanent exercise (LE).

If, during the examination of the file, the prefect finds that there are substantial differences between the training and professional experience of the national and those required to practise in France, compensation measures may be taken ("5°). b. Good to know: compensation measures").

*For further information*: Article R. 204-2 and R. 653-96 of the Rural code and marine fisheries.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Veterinarians who carry out a mission of equine insemination are subject to the rules of ethics and ethics applicable to the profession.

For inseminators other than veterinarians, they have ethical obligations, including respect for the professional secrecy associated with insemination data and to ensure the health and dignity of the inseminated animal.

4°. Insurance
---------------------------------

In the event of a liberal exercise, the equine inseminator has an obligation to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during this activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

#### Competent authority

The regional prefect is responsible for deciding on the prior declaration.

#### Supporting documents

The national's prior declaration must be forwarded by any means to the competent authority and include the following supporting documents:

- Proof of the professional's nationality
- a certificate certifying that it:- is legally established in an EU or EEA state,
  - practises one or more professions whose practice in France requires the holding of a certificate of ability,
  - does not incur a ban on practising, even if temporary, when issuing the certificate;
- proof of his professional qualifications
- where neither professional activity nor training is regulated in the EU or EEA State, evidence by any means that the national has been engaged in this activity for one year, full-time or part-time, in the last ten years.

This advance declaration includes information relating to insurance or other means of personal or collective protection underwritten by the registrant to cover his professional liability.

These documents are attached, as needed, to their translation into the French language.

#### Time

The regional prefect has one month from the time the file is received to make his decision:

- allow the claimant to perform his or her benefit.  In the event of non-compliance with this one-month period or the administration's silence, the provision of services may be carried out;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the qualifications and work experience he uses are substantially different from those required for the exercise of the profession in France;
- inform them of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, from the resolution of the difficulties and in any case within a maximum of three months from the information of the person concerned as to the existence of the difficulty or difficulties. In the absence of a response from the competent authority within these timeframes, service delivery may begin.

*For further information*: Article R. 204-1 of the Rural Code and Marine Fisheries

### b. Obtain a licence for EU nationals engaged in permanent activity (LE)

#### Competent authority

The regional prefect of the place of practice is responsible for issuing the licence allowing the national to carry out permanently the activity of equine inseminator in France.

#### Procedure

The national must pass on to the prefect all the necessary documents to support his application for a licence, including:

- proof of nationality
- A training certificate or certificate of competence acquired in an EU or EEA member state;
- any evidence, if any, that the national has been an inseminator for one year, full-time or part-time, in a Member State that does not regulate the profession.

Therefore, the prefect will have one month from the receipt of the supporting evidence to acknowledge receipt or request the sending of missing documents. The decision to grant the licence will then take place within three months, extended by an additional month in the event of missing parts. The silence kept in these deadlines will be worth acceptance.

#### Good to know: compensation measures

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice, which may be:

- an adaptation course of up to three years
- an aptitude test carried out within six months of notification to the person concerned.

*For further information*: Articles R. 204-2 to R. 204-6 of the Rural Code and Marine Fisheries.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

