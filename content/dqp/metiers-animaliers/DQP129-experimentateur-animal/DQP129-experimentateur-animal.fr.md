﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP129" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Expérimentateur animal" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="experimentateur-animal" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/experimentateur-animal.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="experimentateur-animal" -->
<!-- var(translation)="None" -->

# Expérimentateur animal

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

L'expérimentateur animal est un professionnel qui travaille sous la responsabilité d’un concepteur d’expérimentations sur les animaux, dans un établissement ayant reçu un agrément pour utiliser des animaux à des fins scientifiques.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne exerçant la fonction d’expérimentateur a suivi une formation obligatoire et approuvée par le ministère chargé de l’agriculture. Il est ensuite supervisé dans l'accomplissement de ses tâches par un tuteur présentant les qualifications et l'expérience adéquates, jusqu'à ce qu'il ait démontré qu'il possède les compétences requises en fonction des projets mis en œuvre sur les espèces animales considérées.

Tout au long de son exercice professionnel, il suit une formation continue d’au minimum trois jours sur un pas de temps de six ans. 

De plus, lorsque le projet comprend des procédures chirurgicales, la personne qui applique ces procédures devra également suivre une formation approuvée par le ministère chargé de l’agriculture en chirurgie expérimentale.

La compétence des expérimentateurs est vérifiée par un responsable du suivi des compétences au sein de l’établissement, dûment nommée par le responsable de l’établissement. Il s’assure de l’acquisition des compétences lors de l’embauche et du maintien des compétences tout au long de l’exercice professionnel.

*Pour aller plus loin* : article 2 de l'arrêté du 1er février 2013 relatif à l'acquisition et à la validation des compétences des personnels des établissements utilisateurs, éleveurs et fournisseurs d'animaux utilisés à des fins scientifiques.

#### Formation

La qualification des personnes exerçant la fonction d’expérimentateur animal résulte de leur formation initiale et de leur participation à une formation spécifique à l'expérimentation animale, et de la formation continue.

Cette formation spécifique intervient dans l'année qui suit leur prise de poste (ce qui implique qu’aucune procédure invasive ne soit réalisée par la personne avant qu’elle n’ait acquis des compétences après cette formation).

Le programme de formation doit comprendre, au minimum, l’étude des thèmes suivants :

- règlementation française applicable à l’expérimentation animale, dont les exigences de remplacement, de réduction et de raffinement ;
- principes éthiques concernant les relations entre l’homme et l’animal, règles des 3 R, rôle et fonctionnement des comités d’éthique ;
- procédures expérimentales faiblement invasives, sans anesthésie : théorie et pratique ;
- méthodes alternatives ;
- connaissances de base en biologie des espèces, dont la physiologie, l’anatomie, l’alimentation, la reproduction, le comportement, l’entretien et les techniques d’enrichissement en rapport avec les caractéristiques physiologique, la génétique et les modifications génétiques ;
- reconnaissance des signes de détresse, de douleur et de souffrance propres aux espèces utilisées le plus couramment ;
- anesthésie et analgésie ;
- recours aux points limites adaptés ;
- méthodes d’euthanasie ;
- gestion et suivi de la santé animale et de l’hygiène ;
- méthodes de transport, maniement, contention des animaux propres à chaque espèce ;
- conception de procédures expérimentales et de projets. 

Cette formation de base est complétée par des modules complémentaires spécialisés choisis par les personnes en fonction des besoins liés à leurs fonctions, aux projets qu'elles exécutent ou supervisent, aux espèces animales sur lesquelles les projets sont réalisés et en fonction de leurs compétences acquises au préalable.

**À noter**

L'expérimentateur animale titulaire d'un titre de chirurgien, chirurgien-dentiste ou vétérinaire est exempté de ce module complémentaire.

*Pour aller plus loin* : article 3 et annexe de l'arrêté du 1er février 2013.

#### Coûts associés à la qualification

La formation menant à la fonction d'expérimentateur animal est payante. Pour plus d'informations, il est conseillé de se rapprocher des établissements la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité d’expérimentateur animal peut exercer à titre temporaire et occasionnel la même activité en France.

Lorsque l’État ne réglemente ni l'accès à l'activité, ni son exercice, le ressortissant doit justifier avoir exercé la profession d’expérimentateur animal au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, l'intéressé doit, avant sa première prestation de services, effectuer une déclaration auprès du préfet de département du lieu d’exercice (cf. infra « 5°. b. Déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis, au choix, à une épreuve d’aptitude où à un stage d’adaptation.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre de formation ou d'une attestation de compétence délivré(e) par une autorité compétente d'un autre État membre qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la fonction d’expérimentateur animal à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

En cas de différences substantielles de formation, l’accès à la profession pourra être subordonné à une mesure de compensation.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Éthique

Toute personne travaillant dans la conception et la réalisation d'expérimentations animales a l'obligation de respecter des règles éthiques et notamment la règle des 3R comprenant les points suivants :

- réduire le nombre d'animaux utilisés à des fins expérimentales ;
- raffiner la méthodologie appliquée et trouver des solutions pour réduire les souffrances des animaux par l'application de points limites ;
- remplacer les modèles d'animaux.

*Pour aller plus loin* : considérant 11 de la directive 2010/63/UE du Parlement européen et du Conseil du 22 septembre 2010 relative à la protection des animaux utilisés à des fins scientifiques.

### b. Formation professionnelle continue

L'expérimentateur animal est tenu de suivre une formation continue obligatoire de trois jours, et ce, tous les six ans.

Cette formation doit lui permettre d'actualiser ses connaissances et peut être acquise lors de formations pratiques ou de participation à des colloques dans les domaines liés à l'expérimentation animale.

Le suivi de la formation continue est justifié par la possession d'attestations de formation ou, à minima, d'attestations de présence à des colloques, consignées dans un livret de compétences comportant notamment :

- l'intitulé de la formation ;
- le mode d'acquisition ;
- la date et la durée de la formation ;
- la date de validation de la formation suivie.

*Pour aller plus loin* : articles 5 et 6 de l'arrêté du 1er février 2013.

### c. Sanctions

Tout établissement utilisateur, éleveur ou fournisseur d'animaux à des fins expérimentales doit pouvoir justifier que son personnel a suivi les formations réglementaires et continues requises.

En cas de non-conformité à cette obligation lors de la visite d'agrément, le responsable de l'établissement et son personnel non formé peuvent se voir sanctionner d'une contravention de 4e classe pouvant aller jusqu'à 750 euros. Cela peut également compromettre l'obtention ou le renouvellement de l'agrément de l'établissement.

## 4°. Assurance

En cas d'exercice libéral, l'expérimentateur animal a l'obligation de souscrire une assurance de responsabilité professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de cette activité.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).