﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP129" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Animal experimenter" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="animal-experimenter" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/animal-experimenter.html" -->
<!-- var(last-update)="2020-04-15 17:20:54" -->
<!-- var(url-name)="animal-experimenter" -->
<!-- var(translation)="Auto" -->


Animal experimenter
===================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:54<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The animal experimenter is a professional who works in an accredited facility to carry out animal experiments.

Its mission is to apply the animal testing procedures put in place, to ensure the care of animals and, in some cases, to perform surgical procedures on animals.

**Please note**

Being an animal experimenter is not a profession in the strict sense of the word but an additional function that is part of a regulated profession.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The function of animal experimenter is reserved for the holders:

- a title sanctioning a minimum level II on the nomenclature of training levels;
- a title sanctioning a level B to the European Federation of Laboratory Animal Sciences (Felasa) for nationals of a Member State of the European Union (EU) or party to the European Economic Area (EEA).

*For further information*: Article 2 of the[1 February 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027037960&categorieLien=id) relating to the acquisition and validation of the skills of staff of user, breeder and suppliers of animals used for scientific purposes.

#### Training

As long as the person is justifying one of the above titles, he or she must undergo additional training to perform the role of animal experimenter. This training takes place within one year of taking up the position and consists of a general module covering all species, and specific modules dedicated to the groups of species determined below:

- rodents;
- rent mammals;
- carnivores;
- Birds;
- cold-blooded animals;
- primates;
- wildlife.

When the project he is working on involves surgical procedures, the animal experimenter will have to be trained on aspects of surgical propedeutics (pre- and post-operative care, aseptia, anesthesia and analgesia) during a training for a minimum of 24 hours.

**Please note**

The animal experimenter with a title of surgeon, dental surgeon or veterinarian is exempt from this supplemental module.

*For further information*: Article 3 and annex to the[1 February 2013](https://www.legifrance.gouv.fr/jo_pdf.do?numJO=0&dateJO=20130207&numTexte=29&pageDebut=02210&pageFin=02212) relating to the acquisition and validation of the skills of staff of user, breeder and suppliers of animals used for scientific purposes.

#### Costs associated with qualification

Training leading to the function of animal experimenter is paid for. For more information, it is advisable to get closer to the dispensing establishments.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

In order to perform the role of animal experimenter in France on a temporary or casual basis, the national of a Member State of the European Union (EU) or part of the European Economic Area (EEA) must refer to the regulations applicable to the profession practised as a principal.

If the person concerned has undergoing specific training on animal testing in his home state, he may, if necessary, apply for recognition in France if he fulfils the following two conditions:

- justify by any means of following this training;
- to follow a module on French regulations and another on ethics in one of the establishments approved by the Ministry of Agriculture and Food.

In the event that the national does not justify the follow-up of the specific training in his State of origin, he will have to follow the one given in France (see above "2. a. Training").

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

In order to carry out the role of animal experimenter in France for a permanent exercise, the national of an EU or EEA Member State must refer to the regulations applicable to the profession practised as a principal.

If the person concerned has undergoing specific training on animal testing in his home state, he may, if necessary, apply for recognition in France if he fulfils the following two conditions:

- justify by any means of following this training;
- follow one module on French regulations and another on ethics in one of the establishments approved by the Ministry of Agriculture and Food.

In the event that the national does not justify the follow-up of the specific training in his State of origin, he will have to follow the one given in France (see above "2. a. Training").

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethics

Anyone working in the design and conduct of animal experiments has an obligation to respect ethical rules, including the 3R rule including:

- Reducing the number of animals used for experimental purposes
- refine the methodology applied and find solutions to reduce the suffering of animals by applying boundary points;
- replace animal models.

### b. Continuing vocational training

The animal experimenter is required to complete a mandatory three-day continuing education every six years.

This training should enable him to update his knowledge and can be acquired during practical training or participation in seminars in the fields related to animal experimentation.

The follow-up of continuing education is justified by the possession of training certificates or, at a minimum, certificates of attendance at seminars, recorded in a competency booklet including:

- The title of the training;
- The acquisition method
- The date and duration of the training
- validation date for the training.

*For further information*: Articles 5 and 6 of the February 1, 2013 order.

### v. Sanctions

Any user, breeder or supplier of animals for experimental purposes must be able to justify that its staff has followed the required regulatory and ongoing training.

In the event of non-compliance with this obligation during the accreditation visit, the head of the establishment and his untrained staff may be punished with a 4th class ticket of up to 750 euros. It can also jeopardize the certification or renewal of the institution.

4°. Insurance
---------------------------------

In the event of a liberal exercise, the animal experimenter is obliged to take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during this activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obligation to take training for EU or EEA nationals

The national of an EU or EEA state, who has undergostered training leading to the role of animal experimenter in that state, may apply for recognition in France provided he or she undergoes further training including a module on animal Regulations and ethics in one of the establishments approved by the Ministry of Agriculture.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

