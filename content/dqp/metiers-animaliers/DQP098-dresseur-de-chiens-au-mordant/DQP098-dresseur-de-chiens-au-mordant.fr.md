﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP098" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Dresseur de chiens au mordant" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="dresseur-de-chiens-au-mordant" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/dresseur-de-chiens-au-mordant.html" -->
<!-- var(last-update)="2020-04-15 17:20:52" -->
<!-- var(url-name)="dresseur-de-chiens-au-mordant" -->
<!-- var(translation)="None" -->

# Dresseur de chiens au mordant

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:52<!-- end-var -->

## 1°. Définition de l’activité

Le dresseur de chiens au mordant est un professionnel dont l'activité consiste à dresser et entraîner des chiens à l'attaque dans le cadre d'activités de surveillance, de gardiennage ou de transports de fonds.

*Pour aller plus loin* : article L. 211-17 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Seul le professionnel titulaire d'un certificat de capacité peut exercer l'activité de dresseur de chiens au mordant et se procurer le matériel nécessaire à ce dressage.

*Pour aller plus loin* : article L. 211-17 du Code rural et de la pêche maritime.

#### Formation

**Certificat de capacité pour le dressage des chiens au mordant**

Pour obtenir le certificat de capacité, le professionnel doit justifier soit :

- être titulaire de l'un des diplômes, titres ou certificats suivants :
  - un diplôme de dresseur ou de moniteur cynotechnicien délivré par le ministère de l'Intérieur (police nationale),
  - un certificat technique cynotechnie (du premier ou du deuxième degré) ou un brevet supérieur de technicien (BTS) de l'armée de terre cynotechnie délivré par le ministère chargé de la défense (armée de terre),
  - un brevet élémentaire ou supérieur de maître chien délivré par le ministère chargé de la défense (armée de l'air),
  - un certificat technique cynotechnie (du premier ou du deuxième degré) délivré par le ministère chargé de la défense (marine nationale),
  - un certificat technique du premier degré cynotechnie (module dresseur-chef de cyno-groupe) ou un certificat technique du deuxième degré cynotechnie (module approfondissement) délivré par le ministère de la défense (gendarmerie),
  - un brevet de moniteur de club habilité à pratiquer des disciplines incluant du mordant délivré par la société centrale canine pour l'amélioration des races de chiens en France ;
- être titulaire d'une attestation de connaissances et de compétences délivrée par le directeur régional de l'alimentation, de l'agriculture et de la forêt de la région Auvergne-Rhône-Alpes.

Dès lors qu'il remplit ces conditions, l'intéressé doit effectuer une demande auprès du préfet de département en vue d'obtenir un certificat de capacité (cf. infra « 5°. a. Demande en vue d'obtenir un certificat de capacité de dresseur de chiens au mordant »).

*Pour aller plus loin* : article R. 211-9 du Code rural et de la pêche maritime ; article 1er et annexe 1 de l'arrêté du 17 juillet 2000 relatif au certificat de capacité pour le dressage de chiens au mordant : justificatifs de connaissances et de compétences requis.

**Attestation de connaissances et de compétences**

En vue d'obtenir cette attestation, le professionnel doit se soumettre à une évaluation de ses connaissances et compétences.

Pour cela, l'intéressé doit adresser une demande d'inscription à cette évaluation auprès de l'établissement public local d'enseignement et de formation professionnelle agricole des Combrailles - Saint-Gervais-d'Auvergne qui lui adressera un dossier d'inscription précisant les pièces justificatives nécessaires ainsi que les conditions de recevabilité.

Cette évaluation est composée de deux unités portant sur la capacité du candidat à :

- unité n° 1 : effectuer en toute sécurité les tâches liées à l'exercice de l'activité de dressage de chiens au mordant. Cette unité est évaluée par une mise en situation pratique ;
- unité n° 2 : mobiliser les connaissances législatives, scientifiques et techniques relatives au dressage de chiens au mordant pour expliquer les pratiques professionnelles. Cette unité est évaluée au moyen d’une épreuve orale.

Le référentiel des connaissances et des compétences évaluées est fixé à l'annexe 4 de l'arrêté du 17 juillet 2000 susvisé.

L'attestation est délivrée par le directeur de la direction régionale de l'alimentation, de l'agriculture et de la forêt (Draaf) au professionnel ayant validé les deux unités d'épreuves.

**À noter**

Le candidat n'ayant validé qu'une seule unité doit repasser l'unité manquante. Un recours peut être formulé auprès du directeur de l'alimentation de l'agriculture et de la forêt de la région Auvergne-Rhône-Alpes, dans un délai de deux mois à compter de la date de notification des résultats.

*Pour aller plus loin* : articles 2 et suivants de l'arrêté du 17 juillet 2000 précité.

#### Coûts associés à la qualification

Les frais relatifs à l'évaluation sont à la charge du candidat et donnent lieu à la perception par l'État d'une redevance pour services rendus.

*Pour aller plus loin* : article R. 211-10 du Code rural et de la pêche maritime.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de dresseur de chiens au mordant peut exercer à titre temporaire et occasionnel la même activité en France.

Lorsque l’État d'origine du ressortissant ne réglemente ni l'accès à l'activité ni son exercice, le ressortissant doit justifier avoir exercé la profession de dresseur de chiens au mordant pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, l'intéressé doit, avant sa première prestation de services, effectuer une déclaration auprès du directeur régional de l'alimentation, de l'agriculture et de la forêt de la région Auvergne-Rhône-Alpes (cf. infra « 5°. b. Déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis, à son choix, à une épreuve d’aptitude où à un stage d’adaptation.

*Pour aller plus loin* : articles L. 211-17 alinéa 2 et L. 204-1 et R. 211-9 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre de formation ou d'une attestation de compétence délivrés par une autorité compétente d'un autre État membre qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une des deux conditions précédentes, le professionnel se verra attribuer, par le directeur régional de l'alimentation, de l'agriculture et de la forêt de la région Auvergne-Rhône-Alpes, l’attestation de connaissances et de compétences.

Lorsque ni l'accès à l'activité ni son exercice ne sont réglementés dans l’État membre de l'UE ou de l'EEE, l'intéressé doit justifier avoir exercé cette activité pendant au moins un an au cours des dix dernières années.

En cas de différences substantielles entre la formation reçue par le ressortissant et celle exigée en France pour exercer cette activité, le directeur de la Draaf d’Auvergne-Rhône-Alpes peut décider de le soumettre à une mesure de compensation (cf. infra « 5°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles R. 211-9 et R. 204-2 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Nul ne peut se procurer à titre gratuit ou onéreux des objets ou du matériel destinés au dressage de chiens au mordant sans être titulaire du certificat de capacité.

Lors de toute acquisition de ces objets ou matériels, le professionnel est tenu de présenter son certificat au vendeur. La vente est inscrite sur un registre spécifique tenu par le vendeur et mis à la disposition des autorités de police ou administratives en cas de contrôle.

**À noter**

Ces obligations ne sont pas applicables aux services de police, de gendarmerie, des armées, des douanes, ni des services publics de secours utilisant des chiens dressés au mordant.
*Pour aller plus loin* : article L. 211-17 du Code rural et de la pêche maritime.
 
## 4°. Assurances

Le dresseur de chiens au mordant exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.
 
## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir un certificat de capacité de dresseur de chiens au mordant

#### Autorité compétente

Le professionnel doit adresser sa demande auprès du préfet du département au sein duquel il est domicilié.

#### Pièces justificatives

Son dossier de demande doit comporter les documents suivants :

- son nom, prénom, date de naissance et l'adresse de son domicile ;
- un curriculum vitae permettant d'établir son expérience en matière de dressage de chiens ;
- la dénomination et l'adresse de l'établissement ou du club d’utilisation au sein duquel le professionnel exerce son activité ;
- une déclaration sur l'honneur certifiant la non-condamnation pour infraction afférente à la protection et à la santé des animaux ;
- l’un des justificatifs permettant d’établir les connaissances et les compétences requises pour l’octroi du certificat de capacité (cf. supra « 2°. a. Certificat de capacité pour le dressage des chiens au mordant ») ;
- lorsque l'activité est exercée dans le cadre d'une entreprise de surveillance, de gardiennage ou de transport de fonds, la copie de l'autorisation de fonctionnement de l'entreprise en question, prévue à l'article 7 de la loi du 12 juillet 1983 susvisée ;
- la copie de la déclaration d'activité du ou des établissements ou clubs d'utilisation concernés ;
- la copie de la carte d'identité ou de tout autre document reconnu équivalent permettant de justifier l'identité et le domicile du postulant.

#### Délais et procédure

Le certificat est délivré par le préfet du département après avis du directeur chargé de la protection des populations. Le silence gardé par le préfet vaut rejet de la demande.

*Pour aller plus loin* : article R. 211-9 du Code rural et de la pêche maritime.

#### Bon à savoir : mesures de compensation

En cas de différences substantielles entre la formation reçue par le ressortissant et celle requise pour exercer l'activité de dresseur de chiens au mordant en France, le préfet peut décider de soumettre le ressortissant à une mesure de compensation. Le cas échéant, ce dernier devra effectuer au choix un stage d'adaptation ou une épreuve d'aptitude.

L'épreuve d'aptitude doit être effectuée dans un délai de six mois maximum après la décision du préfet et doit porter sur tout ou partie de l'évaluation des connaissances en vue d'obtenir le certificat de capacité (cf. annexe 5 de l'arrêté du 17 juillet 2000 précité).

Le stage d'adaptation, d'une durée maximale de trois ans, doit être effectué au sein d'une entreprise d'accueil. Cette dernière signe avec le candidat et le centre d'évaluation une convention détaillant les modalités de ce stage.

*Pour aller plus loin* : articles R. 211-9, R. 204-3 et R. 204-5 du Code rural et de la pêche maritime ; article 7 bis de l'arrêté du 17 juillet 2000 précité.

### b. Déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit effectuer une déclaration préalable auprès du directeur régional de l'alimentation, de l'agriculture et de la forêt de la région Auvergne-Rhône-Alpes (Draaf).

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant, assortis de leur traduction en français :

- un justificatif de nationalité ;
- une attestation certifiant qu'il est légalement établi au sein d'un État membre de l'UE et exerce l'activité de dresseur de chiens au mordant ;
- un document certifiant qu'il ne fait l'objet d'aucune interdiction d'exercer ;
- un justificatif de ses qualifications professionnelles ;
- lorsque l’État membre ne réglemente ni l'accès à l'activité ni son exercice, la preuve par tout moyen que le demandeur a exercé cette activité pendant au moins un an au cours des dix dernières années.

#### Délai et procédure

En cas de différence substantielle entre les qualifications professionnelles du ressortissant et celles exigées en France pour exercer l'activité de dresseur de chiens au mordant, le directeur de la Draaf d'Auvergne-Rhône-Alpes peut décider de le soumettre à une épreuve d'aptitude dans un délai d'un mois à compter de sa décision.

**À noter**

En l'absence de réponse au delà d'un délai d'un mois, le demandeur peut débuter sa prestation de services.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

#### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).