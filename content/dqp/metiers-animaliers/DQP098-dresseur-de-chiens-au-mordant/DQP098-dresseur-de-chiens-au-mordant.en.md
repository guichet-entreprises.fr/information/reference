﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP098" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Attack-dog trainer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="attack-dog-trainer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/attack-dog-trainer.html" -->
<!-- var(last-update)="2020-04-15 17:20:53" -->
<!-- var(url-name)="attack-dog-trainer" -->
<!-- var(translation)="Auto" -->


Attack-dog trainer
=====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:53<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The biting dog trainer is a professional whose activity consists of training and training dogs to attack in the course of surveillance, guarding or transporting funds.

*For further information*: Article L. 211-17 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Only the professional with a certificate of ability can perform the activity of dog trainer in the bite and obtain the necessary equipment for this training.

*For further information*: Article L. 211-17 of the Rural Code and Marine Fisheries.

#### Training

**Certificate of ability for training dogs with bite**

To obtain the Certificate of Capacity, the professional must justify either:

- hold one of the following diplomas, titles or certificates:- a Cynotechnician trainer or instructor diploma issued by the Ministry of the Interior (national police),
  - a cynotechnic technical certificate (first or second degree) or a superior technician's certificate (BTS) from the Cynotechnic Army issued by the Ministry of Defence (Army),
  - an elementary or superior dog master certificate issued by the Ministry of Defence (Air Force),
  - a cynotechnic technical certificate (first or second degree) issued by the Ministry of Defence (National Navy),
  - a cynotechnic first-degree technical certificate (cyno-group head trainer module) or a cynotechnical second-degree technical certificate (deepening module) issued by the Ministry of Defence (gendarmerie),
  - a club instructor's certificate authorized to practice disciplines including bite issued by the central canine company for the improvement of dog breeds in France;
- hold a certificate of knowledge and skills from the regional director of food, agriculture and forestry in the Auvergne-Rhône-Alpes region.

Once the person meets these conditions, he must apply to the prefect of the department in order to obtain a certificate of capacity (see infra "5°. a. Request for a certificate of dog trainer ability to bite").

*For further information*: Article R. 211-9 of the Rural Code and Marine Fisheries; Article 1 and Appendix 1 of the July 17, 2000 Order on the Certificate of Ability for The Training of Dogs in Bite: Proof of Knowledge and Skills Required.

**Certificate of knowledge and skills**

In order to obtain this certification, the professional must undergo an assessment of his knowledge and skills.

In order to do so, the person concerned must apply for registration for this assessment with the local public agricultural education and vocational training institution of Combrailles - Saint-Gervais-d'Auvergne, which will send him a file specifying the necessary supporting documents as well as the conditions of admissibility.

This evaluation consists of two units dealing with the candidate's ability to:

- Unit 1: Safely perform the tasks associated with the exercise of dog training to bite. This unit is evaluated by a practical situation;
- Unit 2: mobilize legislative, scientific and technical knowledge of biting dog training to explain professional practices. This unit is evaluated through an oral test.

The benchmark of the knowledge and skills assessed is set at the[Appendix 4](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=430B59CA50974DC3225518ECCE5946A1.tplgfr22s_2?idArticle=LEGIARTI000020755413&cidTexte=LEGITEXT000020751384&dateTexte=20180427) 17 July 2000 above.

The certificate is issued by the Director of the Regional Directorate of Food, Agriculture and Forestry (Draaf) to the professional who has validated the two test units.

**Please note**

The candidate who has validated only one unit must retake the missing unit. An appeal can be made to the Director of Agriculture and Forest Food in the Auvergne-Rhône-Alpes region within two months of the date of notification of the results.

*For further information*: Articles 2 and following of the order of 17 July 2000 above.

#### Costs associated with qualification

The assessment fee is borne by the applicant and results in the state collecting a fee for services rendered.

*For further information*: Article R. 211-10 of the Rural Code and Marine Fisheries.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement who performs the activity of biting dog trainer may exercise the same temporary and occasional practice on a casual basis. activity in France.

Where the national's state of origin does not regulate access to the activity or its exercise, the national must justify having worked as a dog trainer for at least one year in the last ten years.

Once the person meets these conditions, the person concerned must, before his first service delivery, make a declaration to the regional director of food, agriculture and forestry of the Auvergne-Rhône-Alpes region (see infra "5o). b. Pre-declaration for EU nationals for a temporary and casual exercise (LPS)").

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected, at his choice, to a test or an adjustment course.

*For further information*: Articles L. 211-17 paragraph 2 and L. 204-1 and R. 211-9 of the Rural Code and Marine Fisheries.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a training certificate or certificate of competency issued by a competent authority in another Member State that regulates access to the profession or its exercise;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once the professional fulfils one of the two previous conditions, he will be awarded the certificate of knowledge and skills by the regional director of food, agriculture and forestry in the Auvergne-Rhône-Alpes region.

Where neither access to the activity nor its exercise is regulated in the EU or EEA Member State, the person must justify having carried out this activity for at least one year in the last ten years.

In the event of substantial differences between the training received by the national and that required in France to carry out this activity, the director of the Draaf d'Auvergne-Rhône-Alpes may decide to subject him to a compensation measure (see below "5o. a. Good to know: compensation measures").

*For further information*: Articles R. 211-9 and R. 204-2 of the Rural Code and Marine Fisheries.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

No one may obtain free or expensive items or equipment for training dogs in bite without holding the certificate of ability.

When acquiring these objects or materials, the professional is required to present his certificate to the seller. The sale is registered on a specific register kept by the seller and made available to the police or administrative authorities in case of control.

**Please note**

These obligations do not apply to police, gendarmerie, armed forces, customs, or public emergency services using bitten dogs.*For further information*: Article L. 211-17 of the Rural Code and Marine Fisheries.

4°. Insurance
---------------------------------

The dog trainer with a liberal bite must take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for a certificate of dog trainer ability to bite

#### Competent authority

The professional must apply to the prefect of the department in which he is domiciled.

#### Supporting documents

Its application file must include the following documents:

- his first name, date of birth and home address;
- A resume to establish his experience in dog training;
- The name and address of the establishment or club in which the professional operates;
- a declaration of honour certifying non-conviction for offences relating to the protection and health of animals;
- one of the proofs to establish the knowledge and skills required to grant the Certificate of Capacity (see supra "2." a. Certificate of ability for training dogs in bite");
- Where the activity is carried out in the context of a supervisory, custody or money-carrying enterprise, a copy of the operating authorization of the company in question, provided for in section 7 of the aforementioned Act of 12 July 1983;
- Copying the activity statement of the affected establishments or clubs;
- a copy of the identity card or any other equivalent recognized document to justify the identity and residence of the applicant.
Delays and procedures

The certificate is issued by the prefect of the department after notice from the director in charge of population protection. The silence kept by the prefect is worth rejecting the request.

*For further information*: Article R. 211-9 of the Rural Code and Marine Fisheries.

#### Good to know: compensation measures

In the event of substantial differences between the training received by the national and that required to carry out the activity of dog trainer in France, the prefect may decide to subject the national to a compensation measure. If necessary, the latter will have to perform an adjustment course or an aptitude test.

The aptitude test must be completed within six months of the prefect's decision and must cover all or part of the knowledge assessment in order to obtain the Certificate of Capacity (cf.[Appendix 5](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=69322FA4B23BDCE8162539EAC1AF8C64.tplgfr22s_2?idArticle=LEGIARTI000020755415&cidTexte=LEGITEXT000020751384&dateTexte=20180430) 17 July 2000).

The adaptation course, which lasts up to three years, must be carried out within a host company. The latter signs an agreement with the candidate and the evaluation centre detailing the terms of this internship.

*For further information*: Articles R. 211-9, R. 204-3 and R. 204-5 of the Rural and Marine Fisheries Code; Article 7 bis of the order of 17 July 2000 above.

### b. Pre-declaration for EU national for temporary and casual exercise (LPS)

#### Competent authority

The national must make a prior declaration to the regional director of food, agriculture and forestry in the Auvergne-Rhône-Alpes region (Draaf).

#### Supporting documents

Its application must include the following documents, if any, with their translation into French:

- Proof of nationality
- a certificate certifying that it is legally established within an EU Member State and performs the activity of biting dog trainer;
- A document certifying that he is not subject to any smoking ban;
- proof of his professional qualifications
- where the Member State does not regulate access to the activity or its exercise, evidence by any means that the applicant has been engaged in this activity for at least one year in the last ten years.

#### Time and procedure

In the event of a substantial difference between the professional qualifications of the national and those required in France to carry out the activity of dog trainer in bite, the director of the Draaf d'Auvergne-Rhône-Alpes may decide to subject him to a aptitude test within one month of the decision.

**Please note**

In the absence of a response beyond one month, the applicant may begin providing services.

*For further information*: Article R. 204-1 of the Rural Code and Marine Fisheries.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

#### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

#### Procedure

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

#### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

#### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

#### Cost

Free.

#### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

#### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

