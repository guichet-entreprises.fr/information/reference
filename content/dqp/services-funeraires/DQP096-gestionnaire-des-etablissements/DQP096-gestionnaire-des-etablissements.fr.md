﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP096" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Services funéraires" -->
<!-- var(title)="Gestionnaire des établissements funéraires" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="services-funeraires" -->
<!-- var(title-short)="gestionnaire-des-etablissements" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/services-funeraires/gestionnaire-des-etablissements-funeraires.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="gestionnaire-des-etablissements-funeraires" -->
<!-- var(translation)="None" -->

# Gestionnaire des établissements funéraires

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le gestionnaire des établissements funéraires est un professionnel chargé d'administrer un établissement proposant des services de pompes funèbres.

Il est notamment chargé, en tant que professionnel du secteur funéraire, de déterminer avec les familles, l'organisation et les modalités de la prestation funéraire et de veiller au bon déroulement des activités effectuées au sein de son établissement.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de gestionnaire des établissements funéraires, le professionnel doit :

- être titulaire d'un diplôme permettant l'exercice de la profession de conseiller funéraire ;
- solliciter une habilitation pour son établissement (cf. infra « 5°. b. Demande en vue d'obtenir une habilitation ».

*Pour aller plus loin* : articles L. 2223-23 et D. 2223-55-2 du Code général des collectivités territoriales.

#### Formation

Pour exercer légalement son activité, le gestionnaire d'un établissement funéraire doit, dans les douze mois à compter de la date de création de son établissement, obtenir un diplôme.

Ce diplôme est délivré dès lors que le candidat a effectué avec succès les formations suivantes :

- une formation théorique, d'une durée de 140 heures, portant sur les matières suivantes :
  - hygiène, sécurité et ergonomie,
  - législation et réglementation funéraire,
  - psychologie et sociologie du deuil,
  - pratiques et rites funéraires,
  - conception et animation d'une cérémonie,
  - encadrement d'une équipe,
  - produits, services et conseil à la vente,
  - réglementation commerciale ;
- une formation pratique de 70 heures, réalisée dans une entreprise, une régie ou une association habilitée, à l'issue de laquelle une évaluation écrite sera faite ;
- une formation complémentaire de 42 heures.

À l'issue de sa formation, le professionnel subi un examen, organisé par les organismes formateurs.

*Pour aller plus loin* : article D. 2223-55-3 et suivants du Code général des collectivités territoriales ; arrêté du 30 avril 2012 portant application du décret n° 2012-608 du 30 avril 2012 relatif aux diplômes dans le secteur funéraire.

#### Demande d'habilitation de l'établissement

L'habilitation n'est délivrée au gestionnaire que lorsque les conditions suivantes sont remplies :

- le gestionnaire ne doit avoir fait l'objet d'aucune condamnation définitive (cf. infra « 3°. Conditions d'honorabilité, règles déontologiques, éthique ») ;
- l'ensemble du personnel et le gestionnaire de l'établissement doivent disposer de qualifications professionnelles minimales requises ;
- les installations techniques et les véhicules assurant le transport des corps doivent être conformes aux dispositions fixées aux articles R. 2223-67 à D. 2223-121 du Code général des collectivités territoriales ;
- le gestionnaire doit être en règle au regard des impositions et des cotisations sociales.

*Pour aller plus loin* : article L. 2223-23 du Code général des collectivités territoriales.

#### Coûts associés à la qualification

La formation menant au diplôme de conseiller funéraire est payante. Pour plus de renseignements, il est conseillé de se rapprocher des organismes la dispensant.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union Européenne (UE) ou partie à l'Espace Économique Européen, peut exercer en France l'activité de conseiller funéraire, de manière temporaire et occasionnelle dès lors qu'il remplit l'une des conditions suivantes :

- être légalement établi dans cet État pour y exercer la même activité ;
- avoir exercé cette activité pendant un an au cours des dix dernières années qui précèdent la prestation, lorsque ni la formation, ni l'activité ne sont réglementées dans cet État ;
- avoir reçu une [habilitation](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000006390299&dateTexte=&categorieLien=cid) par la préfecture compétente.

*Pour aller plus loin* : articles L. 2223-23 et L. 2223-47 du Code général des collectivités territoriales.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente s'il justifie soit :

- d'une expérience professionnelle de trois années consécutives ;
- d'une attestation de compétence délivrée par une autorité compétente lorsque la profession est réglementée dans cet État ;
- de l'exercice à plein temps ou à temps partiel, pendant un an au cours des dix dernières années, lors que l’État ne réglemente ni la profession ni la formation.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander la reconnaissance de ses qualifications professionnelles au préfet territorialement compétent (cf. infra « 4°. a. Demander une reconnaissance de qualifications professionnelles pour le ressortissant en vue d'un exercice permanent (LE) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles entre la qualification du professionnel et celle requise pour exercer en France, l’intéressé pourra être soumis à une épreuve d’aptitude ou un stage d’adaptation (cf. infra « 4°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles L. 2223-48 et L. 2223-50 du Code général des collectivités territoriales.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Honorabilité

Pour diriger un établissement funéraire, le professionnel ne doit pas avoir fait l'objet :

- d'une condamnation définitive à une peine d'emprisonnement avec ou sans sursis, inscrite au bulletin n° 2 de son casier judiciaire ou d'une condamnation prononcée par une juridiction étrangère pour l'un des crimes ou délits suivants :
  - exercice illégal d'une activité professionnelle ou sociale réglementée,
  - corruption ou trafic d'influence,
  - acte d'intimidation envers une personne exerçant une fonction publique,
  - escroquerie,
  - abus de confiance,
  - violation de sépulture ou atteinte au respect dû aux morts,
  - vol ou recel,
  - attentat aux mœurs ou agression sexuelle,
  - coups et blessures volontaires ;
- d'une faillite personnelle.

*Pour aller plus loin* : article L. 2223-24 du Code général des collectivités territoriales.

### Règles professionnelles

Le professionnel est tenu au respect du règlement national des pompes funèbres.

À ce titre, il doit notamment :

- veiller à respecter les dispositions en matière d'information des familles et notamment des mentions obligatoires sur les devis et bons de commande émis, fixées aux articles R. 2223-24 à R. 2223-32-1 du Code général des collectivités territoriales (informations relatives au professionnel et à la nature des prestations proposées) ;
- proposer et respecter les exigences en matière de formules de financement en prévision d'obsèques (cf. article R. 2223-33 du Code général des collectivités territoriales).

*Pour aller plus loin* : article R. 2223-23-5 et suivants du Code général des collectivités territoriales.

## 4°. Assurances et sanctions

Le fait pour un gestionnaire des établissements funéraires, d'exercer son activité sans avoir obtenu d'habilitation, est puni d'une amende de 75 000 euros d'amende.

Le professionnel encourt la même sanction, dès lors qu'il offre des services en prévision d'obsèques ou dans un délai de deux mois suivants le décès, en vue d'obtenir la commande de fournitures ou de prestations liées à un décès. Sont également interdits, les démarchages sur la voie publique en vue d'obtenir ces mêmes prestations.

En outre, est puni d'une peine de cinq ans d'emprisonnement et de 75 000 euros d'amende le fait, pour un professionnel, de proposer directement ou indirectement des avantages quelconques (offres, dons, promesses etc.) à des personnes ayant connaissance d'un décès à l'occasion de leur activité en vue d'obtenir la conclusion de prestations liées au décès ou de recommander les services d'un professionnel.

*Pour aller plus loin* : articles L. 2223-33 et L. 2223-35 du Code général des collectivités territoriales.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande de reconnaissance de qualifications professionnelles pour le ressortissant de l'UE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le préfet du lieu d'installation du ressortissant est compétent pour se prononcer sur la demande de reconnaissance de qualifications professionnelles.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comportant les pièces justificatives suivantes :

- une pièce d’identité en cours de validité ;
- une attestation de compétence ou un titre de formation délivrés par une autorité compétente ;
- toute attestation justifiant, le cas échéant, que le ressortissant a exercé l'activité de conseiller funéraire pendant un an à temps plein ou à temps partiel, au cours des dix dernières années, dans un État membre qui ne réglemente pas la profession.

#### Procédure

Le préfet accusera réception du dossier complet dans un délai d'un mois et informera le ressortissant en cas de pièce manquante. Dès lors, il aura quatre mois pour se prononcer sur la demande de reconnaissance ou de soumettre le ressortissant à une mesure de compensation.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à une mesure de compensation, qui peut être :

- un stage d'adaptation ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : article R. 2223-133 et suivants du Code général des collectivités territoriales ; arrêté du 25 août 2009 portant mise en œuvre de la vérification des connaissances et des mesures compensatoires pour la reconnaissance des qualifications professionnelles dans le secteur funéraire.

### b. Demande en vue d'obtenir une habilitation

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet du département au sein duquel l'entreprise à son siège, ou par le préfet de police pour la ville de Paris.

#### Pièces justificatives

Sa demande doit comporter les éléments suivants :

- une déclaration mentionnant le nom de l'établissement, sa forme juridique, son activité, son siège ainsi que l'état civil et la qualité de son responsable. Le cas échéant, un extrait de son immatriculation au registre du commerce et des sociétés (RCS) ou du répertoire des métiers ;
- la liste des activités pour lesquelles l'habilitation est sollicitée ;
- un justificatif attestant que l'établissement rempli les obligations requises en matière d'imposition et de cotisations sociales ;
- un justificatif de capacité, d'exercice et de formation professionnelle du dirigeant et de l'ensemble du personnel ;
- l'état à jour du personnel employé au sein de l'établissement ;
- selon le cas :
  - une attestation de conformité des véhicules pour les activités de transport de corps avant mise en bière,
  - pour l'activité de gestion et d'utilisation d'une chambre funéraire, une attestation de conformité de cette dernière,
  - une copie du diplôme de thanatopracteur pour le personnel chargé des soins de conservation,
  - en cas d'activité liée à la gestion d'un crématorium, une attestation de conformité de celui-ci.

#### Procédure

Lorsque la demande est complète, l'habilitation est accordée pour une durée de six ans, sur l'ensemble du territoire et publiée au recueil des actes de la préfecture. Toutefois, lorsque le professionnel ne justifie pas d'une expérience professionnelle d'au moins deux ans dans l'activité pour laquelle l'habilitation est sollicitée, cette durée est limitée à un an.

**À noter**

L'habilitation de l'établissement peut être suspendue pour une durée maximum d'un an ou retirée dès lors que :

- le professionnel ne remplit plus les conditions nécessaires à sa délivrance ;
- l'activité pour laquelle elle a été délivrée n'est plus exercée au sein de l'établissement ;
- l'établissement ou ses activités portent atteintes à l'ordre public.

*Pour aller plus loin* : articles L. 2223-25 et R. 2223-56 et suivants du Code général des collectivités territoriales.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L'intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).