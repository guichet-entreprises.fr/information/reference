﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP096" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Funeral services" -->
<!-- var(title)="Funeral home manager" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="funeral-services" -->
<!-- var(title-short)="funeral-home-manager" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/funeral-services/funeral-home-manager.html" -->
<!-- var(last-update)="2020-04-15 17:22:28" -->
<!-- var(url-name)="funeral-home-manager" -->
<!-- var(translation)="Auto" -->


Funeral home manager
========================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The funeral manager is a professional who administers a funeral home.

In particular, as a funeral professional, he is responsible for determining with the families, the organization and the modalities of the funeral service and ensuring the smooth running of the activities carried out within his institution.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To be a funeral manager, the professional must:

- Hold a diploma to practice the profession of funeral counselor;
- apply for an authorization for its establishment (see infra "5°). b. Request for clearance."

*For further information*: Articles L. 2223-23 and D. 2223-55-2 of the General Code of Local Government.

#### Training

In order to carry out his or her activity legally, the manager of a funeral home must obtain a diploma within 12 months of the date of the establishment's establishment.

This diploma is issued as soon as the candidate has successfully completed the following training:

- 140-hour theoretical training on the following subjects:- hygiene, safety and ergonomics,
  - funeral legislation and regulation,
  - psychology and sociology of bereavement,
  - funeral practices and rites,
  - designing and hosting a ceremony,
  - coaching a team,
  - products, services and advice on sale,
  - Trade regulation;
- 70-hour hands-on training, carried out in an authorized company, management or association, at the end of which a written evaluation will be made;
- 42-hour additional training.

At the end of their training, the professional undergoes an examination, organized by the training organizations.

*For further information*: Article D. 2223-55-3 and following of the General Code of Local Authorities;[decreed april 30, 2012](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025789769&dateTexte=20180103) Decree No. 2012-608 of April 30, 2012 on diplomas in the funeral sector.

**Request for institution clearance**

The clearance is only issued to the manager when the following conditions are met:

- the manager must not have been the subject of any definitive conviction (see infra "3°. Conditions of honorability, ethical rules");
- All staff and the facility manager must have the minimum professional qualifications required;
- technical facilities and vehicles transporting bodies must comply with the provisions of Articles R. 2223-67 to D. 2223-121 of the General Code of Local Authorities;
- the manager must be in good standing with regard to taxes and social security contributions.

*For further information*: Article L. 2223-23 of the General Code of Local Authorities.

#### Costs associated with qualification

Training leading to the diploma of funeral counselor is paid for. For more information, it is advisable to get closer to the agencies dispensing it.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a party to the European Economic Area may work in France as a funeral counsellor, on a temporary and occasional basis if he fulfils one of the following conditions:

- Be legally established in that state to carry out the same activity;
- have been in this activity for one year in the last ten years preceding the delivery, when neither training nor activity is regulated in that state;
- have received a[Clearance](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000006390299&dateTexte=&categorieLien=cid) by the competent prefecture.

*For further information*: Articles L. 2223-23 and L. 2223-47 of the General Code of Local Government.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state may settle in France to practice permanently if he justifies either:

- Three consecutive years of professional experience
- a certificate of competency issued by a competent authority when the profession is regulated in that state;
- full-time or part-time, for one year in the last ten years, when the state does not regulate the profession or training.

Once the national fulfils one of these conditions, he or she will be able to apply to the territorially competent prefect for recognition of his professional qualifications (see infra "4°. a. Request recognition of professional qualifications for the national for a permanent exercise (LE)).

When the examination of professional qualifications reveals substantial differences between the qualification of the professional and that required to practise in France, the person concerned may be subjected to an aptitude test or an internship adaptation (see infra "4.00. a. Good to know: compensation measures").

*For further information*: Articles L. 2223-48 and L. 2223-50 of the General Code of Local Government.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Integrity**

To run a funeral home, the professional must not have been subjected to:

- a final sentence of imprisonment with or without a conditional sentence, recorded on bulletin 2 of his criminal record or a conviction handed down by a foreign court for one of the following crimes or misdemeanours:- illegally carrying out a regulated professional or social activity,
  - corruption or influence peddling,
  - intimidation of a person in a public service,
  - Scam
  - breach of trust,
  - violation of burial or violation of respect due to the dead,
  - theft or receiving,
  - moral assault or sexual assault,
  - Wilful assaults;
- personal bankruptcy.

*For further information*: Article L. 2223-24 of the General Code of Local Authorities.

**Professional rules**

The professional is bound by the national funeral regulations.

As such, it must include:

- ensure compliance with family information provisions, including mandatory mentions of quotes and purchase orders issued, as set out in Articles R. 2223-24 to R. 2223-32-1 of the General Code of Local Authorities (information relating to the professional and the nature of the services offered);
- propose and comply with funeral funding requirements (see Section R. 2223-33 of the General Code of Local Authorities).

*For further information*: Article R. 2223-23-5 and following from the General Code of Local Authorities.

4°. Insurance and sanctions
-----------------------------------------------

A manager of funeral establishments, operating without obtaining an authorization, is punishable by a fine of 75,000 euros.

The professional incurs the same sanction, if he offers services in anticipation of a funeral or within two months of death, in order to obtain the order for supplies or benefits related to a death. Public road canvassing for the same benefits is also prohibited.

In addition, a five-year prison sentence and a fine of 75,000 euros are punishable by offering any benefits directly or indirectly (offers, gifts, promises, etc.) to persons with knowledge of a death at the opportunity to obtain death-related benefits or to recommend the services of a professional.

*For further information*: Articles L. 2223-33 and L. 2223-35 of the General Code of Local Government.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Application for recognition of professional qualifications for EU nationals for permanent exercise (LE)

**Competent authority**

The prefect of the national's place of installation is competent to decide on the application for recognition of professional qualifications.

**Supporting documents**

The application is made by filing a file with the following supporting documents:

- A valid piece of identification
- A certificate of competency or training certificate issued by a competent authority;
- any evidence, if any, that the national has worked as a funeral counsellor for one year full-time or part-time in the last ten years in a Member State that does not regulate the profession.

**Procedure**

The prefect will acknowledge receipt of the full file within one month and will inform the national in case of missing documents. From then on, he will have four months to decide on the application for recognition or to subject the national to a compensation measure.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to a compensation measure, which may be:

- An adaptation course
- an aptitude test carried out within six months of notification to the person concerned.

*For further information*: Article R. 2223-133 and following from the General Code of Local Authorities; order of 25 August 2009 implementing knowledge verification and compensatory measures for the recognition of professional qualifications in the funeral sector.

### b. Request for clearance

**Competent authority**

The professional must address his request to the prefect of the department in which the company is headquartered, or by the police prefect for the city of Paris.

**Supporting documents**

His application must include:

- a statement mentioning the name of the institution, its legal form, its activity, its headquarters as well as the marital status and quality of its manager. If necessary, an extract from his registration in the register of trades and companies (RCS) or the directory of trades;
- A list of activities for which clearance is requested;
- Proof that the institution is fulfilling the required tax and social security obligations;
- Proof of the ability, exercise and professional training of the manager and all staff;
- The up-to-date status of staff employed at the facility
- depending on the case:- a certificate of vehicle compliance for body transport activities before beer is set,
  - for the management and use of a burial chamber, a certificate of compliance with the funeral board,
  - a copy of the thanatopracteur diploma for conservation care staff,
  - in the event of activity related to the management of a crematorium, a certificate of compliance with the crematorium.

**Procedure**

When the application is complete, the authorization is granted for a period of six years, throughout the territory and published in the collection of the deeds of the prefecture. However, where the professional does not justify at least two years of professional experience in the activity for which the authorization is sought, this period is limited to one year.

**Please note**

The institution's clearance may be suspended for up to one year or withdrawn if:

- The professional no longer fulfils the necessary conditions for his delivery;
- The activity for which it was issued is no longer carried out within the institution;
- institution or its activities are infringing on public order.

*For further information*: Articles L. 2223-25 and R. 2223-56 and following of the General Code of Local Authorities.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

