﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP161" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Funeral services" -->
<!-- var(title)="Funeral director" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="funeral-services" -->
<!-- var(title-short)="funeral-director" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/funeral-services/funeral-director.html" -->
<!-- var(last-update)="2020-04-15 17:22:28" -->
<!-- var(url-name)="funeral-director" -->
<!-- var(translation)="Auto" -->


Funeral director
====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Master of Ceremonies is a professional in the public funeral service whose activity is to ensure that the funeral ceremony runs smoothly until the deceased is put into beer.

In this capacity, he is responsible for the supervision of the teams (carriers, drivers, assistants) and the animation of the ceremony with the families.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of master of ceremonies, the person concerned must be qualified professionally.

However, professionals are deemed to be qualified, with either:

- carried out this activity for one year from 10 May 1995, if any, he is deemed to have taken vocational training;
- vocational training and having, on 1 January 2013, carried out this activity in France or within a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) for at least six months or so spent at least six months between January 1, 2011 and December 31, 2012.

*To go further* Articles L. 2223-25-1 and D. 2223-55-2 of the General Code of Local Authorities.

#### Training

In order to qualify professionally, the person concerned must, within one year of the signing of his contract, obtain a master of ceremonies diploma and have undergon training from an authorised body, relating to the activity of "master of ceremony, responsible for coordinating the conduct of the various ceremonies that take place from the beer to the burial or cremation of a deceased."

This 70-hour training consists of:

- theoretical training on the following subjects:- hygiene, safety and ergonomics (7 hours),
  - funeral legislation and regulations (2 p.m.),
  - Psychology and the Sociology of Grief (2 p.m.),
  - funeral practices and rites (2 p.m.),
  - Designing and hosting a ceremony (2 p.m.),
  - Coaching a team (7 hours)
- a practical assessment carried out within an institution holding a prefectural clearance issued under the terms of Articles R. 2223-56 to R. 2223-65 of the General Code of Local Authorities.

At the end of his training and after passing all the exams and evaluations, the professional is awarded his diploma, thus giving him his professional aptitude.

*To go further* Articles D. 2223-55-2 to D. 2223-55-14 of the General Code of Local Authorities; decree of 30 April 2012 enforcing Decree No. 2012-608 of 30 April 2012 relating to diplomas in the funeral sector.

#### Costs associated with qualification

Training leading to the master of ceremonies activity is paid for and the cost varies depending on the institution chosen. It is advisable to approach the establishment concerned for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any EU or EEA national, legally established and practising the activity of master of ceremonies, may occasionally and temporarily engage in the same activity in France.

In order to do so, the person concerned must:

- Where neither access to the activity nor its exercise is regulated in that Member State, justify having exercised this activity in a Member State for at least one year in the last ten years;
- practice in an institution authorized by the prefect of the department where its head office is located (see Articles R. 2223-56 to R. 2223-65 of the General Code of Local Authorities).

*To go further* Articles L. 2223-47 to L. 2223-51 of the General Code of Local Government.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU state or the EEA, which is legally established and practising the activity of master of ceremonies, may carry out the same activity in France on a permanent basis.

To do so, the individual must justify an experience acquired over the past ten years:

- either as an executive or as an independent of:- three consecutive years in this activity,
  - two years in a row, as long as it justifies prior training and holds a certificate recognised by the Member State in which it has practised,
  - two consecutive years since he justifies having held this activity for three years as an employee;
- either as an employee and justify having undergo taken prior training sanctioned by a certificate issued in that Member State.

If the national does not justify any of these experiences, he must nevertheless justify either:

- Hold a certificate of competency or a training certificate allowing him to carry out the activity of master of ceremonies issued in a Member State regulating the exercise of this activity;
- have been doing this for at least one year in the last ten years and have a certificate of competency or training title when the Member State does not regulate access or the exercise of this activity. This experience is not required if the certificate certifies regulated training.

Once the professional meets these requirements, he must apply for qualification recognition (see below "5°). a. Application for qualification recognition for a permanent exercise (LE)).

*To go further* Articles L. 2223-48 and D. 2223-55-7 of the General Code of Local Government.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The master of ceremonies is bound by the national funeral regulations.

As such, it must include:

- ensure compliance with family information provisions, including mandatory mentions on quotes and purchase orders issued. These provisions are set out in Articles R. 2223-24 to R. 2223-32-1 of the General Code of Local Authorities (information relating to the professional and the nature of the proposed services);
- propose and comply with funeral funding requirements (see Section R. 2223-33 of the General Code of Local Authorities).

*To go further* Articles R. 2223-23-5 and following of the General Code of Local Authorities.

4°. Criminal sanctions
------------------------------------------

The professional is fined 75,000 euros if he provides services in preparation for a funeral or within two months of death in order to obtain the order for supplies or benefits related to a death. Public road canvassing for these same benefits is also prohibited.

In addition, it is punishable by a five-year prison sentence and a fine of 75,000 euros for a professional to propose directly or not any benefits (offers, donations, promises etc.) to persons with knowledge of a death in the event of their activity to obtain the conclusion of death-related benefits or to recommend the services of the professional.

*To go further* Articles L. 2223-33 and L. 2223-35 of the General Code of Local Government.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Application for qualification recognition for permanent practice

**Competent authority**

The professional must submit his request to the prefect of the department where he practices.

**Supporting documents**

The application is made by filing a file with the following supporting documents:

- A valid piece of identification
- A certificate of competency or training certificate issued by a competent authority;
- any evidence, if any, that the national has worked as a funeral reception officer for one year full-time or part-time in the last ten years in a Member State that does not regulate the profession.

**Procedure**

The prefect acknowledges receipt of the request within a month and informs him in case of missing document. The prefect has the professional qualifications checked and, if necessary, may decide to submit it to a compensation measure (see "Good to know: compensation measures").

*To go further* Articles R. 2223-133 and following from the General Code of Local Authorities.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the professional and that required to carry out the activity of master of ceremonies, the department prefect may require that he submit to the choice of an aptitude test or an internship. adaptation.

The adaptation course consists of performing with a professional the duties of a master of ceremonies for a maximum of two years. The aptitude test, on the other hand, involves the verification of all the professional knowledge set out in Article 2 of the Order of 25 August 2009 implementing the knowledge audit and compensatory measures for the recognition of professional qualifications in the funeral sector.

*To go further* Article L. 2223-50 of the General Code of Local Authorities.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris, ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

