﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP082" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Services funéraires" -->
<!-- var(title)="Conseiller funéraire" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="services-funeraires" -->
<!-- var(title-short)="conseiller-funeraire" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/services-funeraires/conseiller-funeraire.html" -->
<!-- var(last-update)="2020-04-15 17:22:27" -->
<!-- var(url-name)="conseiller-funeraire" -->
<!-- var(translation)="None" -->

# Conseiller funéraire

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:27<!-- end-var -->

## 1°. Définition de l'activité

Le conseiller funéraire est un professionnel dont la mission est de conseiller et d'accompagner les familles dans l'organisation des obsèques et dans les démarches administratives (rédaction de l'avis de décès, formulaire à remplir pour les mairies, etc.) mais également pratiques.

Il s'occupe de l'organisation de la cérémonie, vend des prestations d'obsèques, gère le déplacement du convoi et veille à la coordination de tous les intervenants.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession de conseiller funéraire est réservée au titulaire d'un diplôme justifiant une aptitude professionnelle.

*Pour aller plus loin* : article D.2223-55-2 du Code des collectivités territoriales.

#### Formation

Le diplôme permettant l'exercice de la profession de conseiller funéraire est délivré dès lors que le candidat a été admis avec succès à un examen théorique et une évaluation de la formation pratique.

La formation théorique, d'une durée de 140 heures, porte sur les matières suivantes :

- hygiène, sécurité et ergonomie ;
- législation et réglementation funéraire ;
- psychologie et sociologie du deuil ;
- pratiques et rites funéraires ;
- conception et animation d'une cérémonie ;
- encadrement d'une équipe ;
- produits, services et conseil à la vente ;
- réglementation commerciale.

Elle s'accompagne d'une formation pratique de 70 heures réalisée dans une entreprise, une régie ou une association habilitée à l'issue de laquelle une évaluation écrite sera faite.

*Pour aller plus loin* : articles D. 2223-55-3 et suivants du Code des collectivités territoriales ; arrêté du 30 avril 2012 portant application du décret n° 2012-608 du 30 avril 2012 relatif aux diplômes dans le secteur funéraire.

#### Coûts associés à la qualification

La formation menant au diplôme de conseiller funéraire est payante. Pour plus de renseignements, il est conseillé de se rapprocher des organismes la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen, peut exercer en France l'activité de conseiller funéraire, de manière temporaire et occasionnelle dès lors qu'il remplit l'une des conditions suivantes :

- être légalement établi dans cet État pour y exercer la même activité ;
- avoir exercé cette activité pendant un an au cours des dix dernières années qui précèdent la prestation, lorsque ni la formation ni l'activité ne sont réglementées dans cet État ;
- avoir reçu une [habilitation](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000006390299&dateTexte=&categorieLien=cid) par la préfecture compétente.

*Pour aller plus loin* : articles L. 2223-23 et L. 2223-47 du Code des collectivités territoriales.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente s'il justifie :

- soit d'une expérience professionnelle de trois années consécutives ;
- soit d'une attestation de compétences délivrée par une autorité compétente lorsque la profession est réglementée dans cet État ;
- soit de l'exercice à plein temps ou à temps partiel, pendant un an au cours des dix dernières années, lorsque l’État ne réglemente ni la profession ni la formation.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander la reconnaissance de ses qualifications professionnelles au préfet territorialement compétent (cf. infra « 4°. a. Demander une reconnaissance de qualifications professionnelles pour le ressortissant en vue d'un exercice permanent (LE) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude ou un stage d’adaptation (cf. infra « 4°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles L. 2223-48 et L. 2223-50 du Code des collectivités territoriales.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Tout fait de nature à corrompre un tiers pour obtenir de lui qu'il recommande à la famille d'un décédé l'entreprise ou l'association du conseiller funéraire est puni de cinq ans de prison et de 75 000 euros d'amende.

Le conseiller funéraire pourra également se voir affecté d'une interdiction de ses droits civiques, civils ou familiaux, d'une interdiction d'exercer l'activité professionnelle et voir la décision être affichée ou diffusée.

*Pour aller plus loin* : article L. 2223-35 du Code des collectivités territoriales.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Demander une reconnaissance de qualifications professionnelles pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

Le préfet du lieu d'installation du ressortissant est compétent pour se prononcer sur la demande de reconnaissance de qualifications professionnelles.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comportant les pièces justificatives suivantes :

- une pièce d’identité en cours de validité ;
- une attestation de compétences ou un titre de formation délivré par une autorité compétente ;
- toute attestation justifiant, le cas échéant, que le ressortissant a exercé l'activité de conseiller funéraire pendant un an à temps plein ou à temps partiel au cours des dix dernières années dans un État membre qui ne réglemente pas la profession.

#### Procédure

Le préfet accusera réception du dossier complet dans un délai d'un mois et informera le ressortissant en cas de pièce manquante. Dès lors, il aura quatre mois pour se prononcer sur la demande de reconnaissance ou de soumettre le ressortissant à une mesure de compensation.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à une mesure de compensation, qui peut être :

- un stage d'adaptation ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : articles R. 2223-133 et suivants du Code des collectivités territoriales ; arrêté du 25 août 2009 portant mise en œuvre de la vérification des connaissances et des mesures compensatoires pour la reconnaissance des qualifications professionnelles dans le secteur funéraire.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).