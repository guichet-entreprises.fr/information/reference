﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP082" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Funeral services" -->
<!-- var(title)="Funeral arranger" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="funeral-services" -->
<!-- var(title-short)="funeral-arranger" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/funeral-services/funeral-arranger.html" -->
<!-- var(last-update)="2020-04-15 17:22:27" -->
<!-- var(url-name)="funeral-arranger" -->
<!-- var(translation)="Auto" -->


Funeral arranger
=================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:27<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The funeral counsellor is a professional whose mission is to advise and accompany families in the organization of funerals and in administrative procedures (writing the notice of death, form to be filled out for town halls, etc.) but also practical.

He organizes the ceremony, sells funeral services, manages the movement of the convoy and coordinates all stakeholders.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of funeral counselor is reserved for the holder of a diploma justifying a professional aptitude.

*To go further* Article D.2223-55-2 of the Code of Local Authorities.

#### Training

The diploma allowing the profession of funeral counselor is issued as soon as the candidate has been successfully admitted to a theoretical examination and an evaluation of the practical training.

The 140-hour theoretical training focuses on the following subjects:

- hygiene, safety and ergonomics;
- funeral legislation and regulation;
- psychology and sociology of bereavement;
- funeral practices and rites;
- Designing and hosting a ceremony;
- Coaching a team
- products, services and advice on sale;
- commercial regulation.

It is accompanied by a 70-hour hands-on training course carried out in a company, a board or an authorised association at the end of which a written evaluation will be made.

*To go further* Articles D. 2223-55-3 and following of the Code of Local Authorities;[decreed april 30, 2012](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025789769&dateTexte=20180103) Decree No. 2012-608 of April 30, 2012 on diplomas in the funeral sector.

#### Costs associated with qualification

Training leading to the diploma of funeral counselor is paid for. For more information, it is advisable to get closer to the agencies dispensing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a party to the European Economic Area may work in France as a funeral counsellor, on a temporary and occasional basis if he fulfils one of the following conditions:

- Be legally established in that state to carry out the same activity;
- have been in this activity for one year in the last ten years preceding the delivery, when neither training nor activity is regulated in that state;
- have received a[Clearance](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000006390299&dateTexte=&categorieLien=cid) by the competent prefecture.

*To go further* Articles L. 2223-23 and L. 2223-47 of the Code of Local Authorities.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state may settle in France to practice permanently if he or she justifies:

- three years in a row;
- or a certificate of competency issued by a competent authority when the profession is regulated in that state;
- either full-time or part-time, for one year in the last ten years, when the state does not regulate the profession or training.

Once the national fulfils one of these conditions, he or she will be able to apply to the territorially competent prefect for recognition of his professional qualifications (see infra "4°. a. Request recognition of professional qualifications for the national for a permanent exercise (LE)).

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test or a adaptation stage (see infra "4.00). a. Good to know: compensation measures").

*To go further* Articles L. 2223-48 and L. 2223-50 of the Code of Local Authorities.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Any fact likely to corrupt a third party in order to obtain from him that he recommends to the family of a deceased the company or association of the funeral counselor is punished with five years in prison and 75,000 euros fine.

The funeral counselor may also be affected by a ban on his civil, civil or family rights, a ban on professional activity and having the decision posted or disseminated.

*To go further* Article L. 2223-35 of the Code of Local Authorities.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Request recognition of professional qualifications for the national for a permanent exercise (LE)

**Competent authority**

The prefect of the national's place of installation is competent to decide on the application for recognition of professional qualifications.

**Supporting documents**

The application is made by filing a file with the following supporting documents:

- A valid piece of identification
- A certificate of competency or a training certificate issued by a competent authority;
- any evidence, if any, that the national has worked as a funeral counsellor for one year full-time or part-time in the last ten years in a Member State that does not regulate the profession.

**Procedure**

The prefect will acknowledge receipt of the full file within one month and will inform the national in case of missing documents. From then on, he will have four months to decide on the application for recognition or to subject the national to a compensation measure.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to a compensation measure, which may be:

- An adaptation course
- an aptitude test carried out within six months of notification to the person concerned.

*To go further* Articles R. 2223-133 and following from the Code of Local Authorities; order of 25 August 2009 implementing knowledge verification and compensatory measures for the recognition of professional qualifications in the funeral sector.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

