﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP211" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Services funéraires" -->
<!-- var(title)="Personnel de pompes funèbres" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="services-funeraires" -->
<!-- var(title-short)="personnel-de-pompes-funebres" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/services-funeraires/personnel-de-pompes-funebres.html" -->
<!-- var(last-update)="2020-04-15 17:22:29" -->
<!-- var(url-name)="personnel-de-pompes-funebres" -->
<!-- var(translation)="None" -->

# Personnel de pompes funèbres

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:29<!-- end-var -->

## 1°. Définition de l’activité

L'activité de personnel de pompes funèbres consiste à effectuer l'ensemble des prestations des pompes funèbres nécessaires à l'organisation et au bon déroulement des obsèques.

Le service public des pompes funèbres comprend diverses prestations telles que le transport du corps, l'organisation des obsèques et la vente d'accessoires.

*Pour aller plus loin* : article L. 2223-19 du Code général des collectivités territoriales.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'exigence de qualification professionnelle dépend de la nature de l'activité exercée par le professionnel au sein des pompes funèbres. Pour de plus amples informations, il est conseillé de se reporter aux fiches « Agent d'accueil funéraire », « Conseiller funéraire », « Maître de cérémonie » et « Services funéraires ».

Toutefois, pour exercer au sein des pompes funèbres, le professionnel doit avoir suivi une formation spécifique (cf. ci-après « Formation »).

*Pour aller plus loin* : article L. 2223-25 du Code général des collectivités territoriales.

#### Formation

Pour exercer, le professionnel doit avoir suivi une formation d'au moins 16 heures portant sur :

- la législation et la réglementation funéraires ;
- l'hygiène et la sécurité ;
- la psychologie et la sociologie du deuil.

Cette formation est financée par son employeur dans le cadre de la formation professionnelle continue prévue aux articles D. 6321-1 à D. 6321-3 du Code du travail.

*Pour aller plus loin* : articles R. 2223-42 et R. 2223-48 du Code général des collectivités territoriales.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant au sein du service de pompes funèbres peut exercer à titre occasionnel et temporaire la même activité en France.

Pour cela, l'intéressé doit :

- lorsque ni l'accès à l'activité ni son exercice ne sont réglementés dans cet État membre, justifier avoir exercé dans un État membre cette activité pendant au moins un an au cours des dix dernières années ;
- exercer au sein d'un établissement habilité par le préfet du département où se situe son siège social (cf. articles R. 2223-56 à R. 2223-65 du Code général des collectivités territoriales).

*Pour aller plus loin* : article L. 2223-47 du Code général des collectivités territoriales.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE légalement établi et exerçant une activité au sein des pompes funèbres, peut exercer en France, à titre temporaire, la même activité.

Pour cela, l'intéressé doit justifier d'une expérience acquise au cours des dix dernières années :

- soit en qualité de dirigeant ou d'indépendant de :
  - trois années consécutives au sein de cette activité,
  - deux années consécutives, dès lors qu'il justifie d'une formation préalable et est titulaire d'une attestation reconnue par l’État membre où il a exercé,
  - deux années consécutives, dès lors qu'il justifie avoir exercé cette activité pendant trois ans en tant que salarié ;
- soit en tant que salarié et justifier avoir suivi une formation préalable sanctionnée par une attestation délivrée dans cet État membre.

Si le ressortissant ne justifie pas de l'une de ces expériences, il doit néanmoins justifier soit :

- être titulaire d'une attestation de compétences ou d'un titre de formation lui permettant d'exercer cette activité délivré dans un État membre qui en réglemente l'exercice ;
- avoir exercé cette activité pendant au moins un an au cours des dix dernières années et être titulaire d'une attestation de compétences ou d'un titre de formation lorsque l’État membre ne réglemente ni l'accès ni l'exercice de cette activité. Cette expérience n'est pas exigée si l'attestation certifie une formation réglementée.

Dès lors qu'il remplit ces conditions le professionnel doit effectuer une demande de reconnaissance de qualification (cf. infra « 5°. a. Demande de reconnaissance de qualification en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : article L. 2223-48 du Code général des collectivités territoriales.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Le professionnel est tenu au respect du règlement national des pompes funèbres.

À ce titre, il doit notamment :

- veiller à respecter les dispositions en matière d'information des familles, et notamment des mentions obligatoires sur les devis et bons de commande émis, fixées aux articles R. 2223-24 à R. 2223-32-1 du Code général des collectivités territoriales (informations relatives au professionnel et à la nature des prestations proposées) ;
- proposer et respecter les exigences en matière de formules de financement en prévision d'obsèques (cf. article R. 2223-33 du Code général des collectivités territoriales) ;

*Pour aller plus loin* : article R. 2223-23-5 et suivants du Code général des collectivités territoriales.

## 4°. Sanctions pénales

Le professionnel encourt une amende de 75 000 euros dès lors qu'il offre des services en prévision d'obsèques ou dans un délai de deux mois suivants le décès en vue d'obtenir la commande de fournitures ou de prestations liées à un décès. Les démarchages sur la voie publique en vue d'obtenir ces mêmes prestations sont également interdits.

En outre, est puni d'une peine de cinq ans d'emprisonnement et de 75 000 euros d'amende le fait, pour un professionnel, de proposer directement ou indirectement des avantages quelconques (offres, dons, promesses, etc.) à des personnes ayant connaissance d'un décès à l'occasion de leur activité en vue d'obtenir la conclusion de prestations liées au décès ou de recommander les services d'un professionnel.

*Pour aller plus loin* : articles L. 2223-33 et L. 2223-35 du Code général des collectivités territoriales.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande de reconnaissance de qualification en vue d'un exercice permanent (LE)

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet du département où il exerce.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comportant les pièces justificatives suivantes :

- une pièce d’identité en cours de validité ;
- une attestation de compétences ou un titre de formation délivré par une autorité compétente ;
- toute attestation justifiant, le cas échéant, que le ressortissant a exercé une activité liée au service des pompes funèbres pendant un an à temps plein ou à temps partiel au cours des dix dernières années dans un État membre qui ne réglemente pas la profession.

#### Procédure

Le préfet accuse réception de la demande et l'informe dans un délai mois de tout document manquant. Le préfet fait procéder à la vérification des qualifications professionnelles et peut, le cas échéant, décider de le soumettre à une mesure de compensation (cf. ci-après « Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles R. 2223-133 et suivants du Code général des collectivités territoriales.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation reçue par le professionnel et celle requise pour exercer son activité liée au service des pompes funèbres, le préfet de département peut exiger qu'il se soumette au choix à une épreuve d'aptitude ou à un stage d'adaptation.

Le stage d'adaptation consiste pour le ressortissant à exercer auprès d'un professionnel pendant une durée maximale de deux ans. L'épreuve d'aptitude porte quant à elle sur la vérification de l'ensemble des connaissances du professionnel fixées à l'article 2 de l'arrêté du 25 août 2009 portant mise en œuvre de la vérification des connaissances et des mesures compensatoires pour la reconnaissance des qualifications professionnelles dans le secteur funéraire.

*Pour aller plus loin* : article L. 2223-50 du Code général des collectivités territoriales.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).