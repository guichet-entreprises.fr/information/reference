﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP256" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Funeral services" -->
<!-- var(title)="Embalmer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="funeral-services" -->
<!-- var(title-short)="embalmer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/funeral-services/embalmer.html" -->
<!-- var(last-update)="2020-04-15 17:22:30" -->
<!-- var(url-name)="embalmer" -->
<!-- var(translation)="Auto" -->


Embalmer
==============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:30<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The thanatopracteur is a compulsory graduate professional who has the activity of performing conservation care also called thanatopraxia care. This care is regulated funeral operations. These are invasive acts*Mortem* which are drained by fluids and gases from the body and by injecting a biocide product as a replacement. Their purpose is to delay the process of decomposition of the body and its degradation.

*For further information*: :[Family backgrounder on conservation care](https://solidarites-sante.gouv.fr/IMG/pdf/information_aux_familles_sur_les_soins_de_conservation_040118.pdf).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Access to the profession is reserved for the holder of the national diploma of thanatopracteur.

*For further information*: Articles D. 2223-123 and following from the General Code of Local Authorities; decree of 18 May 2010 setting out the conditions for organising the training and examination of access to the national diploma of thanatopracteur.

#### Training

Training leading to the profession of thanatopracteur is delivered in a training centre declared to the territorially competent Direccte.

Theoretical training in conservation care must be of a minimum of 195 hours. Practical training must be followed by the candidate over a minimum of twelve consecutive months and consists of the completion of one hundred conservation cares.

At the end of the training, the candidate will have to pass an exam to obtain the national diploma of thanatopracteur.

This exam is divided into two tests. The first, theoretical, consists of a written examination. The second, practical, takes the form of an evaluation of the candidate in the field of his internship. Access to practical training is subject to a numerus clausus set by decree of the ministers responsible for health and the interior each year.

**Please note**

It is imperative that the person in training be vaccinated against hepatitis B.

*For further information*: Article D. 2223-132 of the Code of Local Authorities and Articles L. 3111-4-1 of the Public Health Code.

#### Costs associated with qualification

Training leading to the thanatopracteur diploma is paid for. It is also mandatory to be able to register for the national thanatopracteur diploma entrance exam. For more information, it is advisable to get closer to the agencies dispensing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or party to the European Economic Area (EEA) may carry out the activity of thanatopracteur in France, on a temporary and occasional basis if he fulfils one of the following conditions:

- Be legally established in that state to carry out the same activity;
- have been in this activity for one year in the last ten years prior to the delivery when neither training nor activity is regulated in that state;
- have received recognition of his professional qualifications by the competent prefecture.

*For further information*: Articles L. 2223-23, L. 2223-47 and R. 2223-133 and following of the General Code of Local Government.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state who is established and legally practises thanatopracteur activity in that state may carry out the same activity in France on a permanent basis if:

- it holds a diploma issued by a competent authority of a Member State, which regulates access to the profession or its exercise;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

In order to do so, he will have to apply for recognition of his professional qualifications from the prefect of the place of installation (see infra "4°. a. Request recognition of the EU or EEA national's professional qualifications for a permanent exercise (LE) ").

If, during the examination of the application, the prefect finds that there are substantial differences between the training and professional experience of the national and those required in France, compensation measures can be taken (see infra "4°. a. Good to know: compensation measures").

*For further information*: Articles L. 2223-49 and R. 2223-134 of the Code of Local Authorities.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Although unregulated, ethical and ethical obligations are the responsibility of thanatopractators, including:

- Respect the dignity and physical integrity of the deceased;
- Respect professional secrecy
- practice his activity with the same awareness about all the deceased.

*For further information*: Article R. 2223-132 of the General Code of Local Authorities.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Request recognition of professional qualifications for EU or EEA nationals for permanent exercise (LE)

**Competent authority**

The prefect of the national's place of installation is competent to decide on the application for recognition of professional qualifications.

**Supporting documents**

This request is made by sending a file to the prefect including all the following documents:

- A copy of diplomas, certificates and other training documents issued by the competent authority of that state when the Member State regulates access to the profession;
- a certificate or training document justifying that the national has been practising for one year in that state, when the Member State does not regulate the profession or the activity.

**Procedure**

The prefect acknowledges receipt of the file within one month and informs the national, if necessary, of any missing documents. The parts are checked by three qualified persons, including a thanatopracteur. At the end of this audit, the prefect and the minister responsible for health may decide to subject the national to a compensation measure in case of substantial differences between his professional qualification and that required to practise in France.

The prefect will have four months to decide whether or not to grant qualification to the national. In the event of compensation, this period will be suspended until the completion of the adjustment course or the aptitude test.

**Good to know: compensation measures**

The choice of compensation measure is left to the discretion of the national who has one month to make up his mind.

The adaptation course, lasting up to two years, must be carried out in an authorized management, company or funeral association. It will be evaluated by the internship manager and then validated by the Minister responsible for health.

The verification of the national's knowledge, during the aptitude test, relates to the subjects specified in the[Article 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7F79CBC33451D825348CD8199AF82FBF.tplgfr38s_2?idArticle=LEGIARTI000020996952&cidTexte=LEGITEXT000020996948&dateTexte=20180116) of the order of 25 August 2009 implementing the knowledge verification and compensatory measures for the recognition of professional qualifications in the funeral sector. It is validated by the Minister responsible for health who will give his opinion to the prefect. The latter will inform the national of his decision to grant recognition of professional qualification.

*For further information*: Articles L. 2223-49 and following articles R. 2223-134 and following of the General Code of Local Authorities.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

