﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP256" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Services funéraires" -->
<!-- var(title)="Thanatopracteur" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="services-funeraires" -->
<!-- var(title-short)="thanatopracteur" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/services-funeraires/thanatopracteur.html" -->
<!-- var(last-update)="2020-04-15 17:22:30" -->
<!-- var(url-name)="thanatopracteur" -->
<!-- var(translation)="None" -->

# Thanatopracteur

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:30<!-- end-var -->

## 1°. Définition de l'activité

Le thanatopracteur est un professionnel obligatoirement diplômé qui a pour activité de réaliser des soins de conservation également appelés soins de thanatopraxie. Ces soins constituent des opérations funéraires réglementées. Ce sont des actes invasifs *post mortem* qui procèdent par drainage des liquides et des gaz du corps et par injection d'un produit biocide en remplacement. Ils ont pour finalité de retarder le processus de décomposition du corps et sa dégradation.

*Pour aller plus loin* : [document d'information des familles sur les soins de conservation](https://solidarites-sante.gouv.fr/IMG/pdf/information_aux_familles_sur_les_soins_de_conservation_040118.pdf).

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'accès à la profession est réservé au titulaire du diplôme national de thanatopracteur.

*Pour aller plus loin* : articles D. 2223-123 et suivants du Code général des collectivités territoriales ; arrêté du 18 mai 2010 fixant les conditions d'organisation de la formation et de l'examen d'accès au diplôme national de thanatopracteur.

#### Formation

La formation menant à la profession de thanatopracteur est délivrée dans un centre de formation déclaré auprès de la Direccte territorialement compétente.

La formation théorique aux soins de conservation doit être d'une durée minimale de 195 heures. La formation pratique doit, elle, être suivie par le candidat sur une période de douze mois consécutifs minimum et consiste en la réalisation de cent soins de conservation.

À l'issue de la formation, le candidat devra passer avec succès un examen permettant l'obtention du diplôme national de thanatopracteur.

Cet examen se divise en deux épreuves. La première, théorique, consiste en un examen écrit. La seconde, pratique, prend la forme d'une évaluation du candidat sur le terrain de son stage. L'accès à la formation pratique est soumis à un numerus clausus fixé par arrêté des ministres chargés de la santé et de l'intérieur chaque année.

**À noter**

L'intéressé en formation doit impérativement être vacciné contre l'hépatite B.

*Pour aller plus loin* : article D. 2223-132 du Code des collectivités territoriales et articles L. 3111-4-1 du Code de la santé publique.

#### Coûts associés à la qualification

La formation menant au diplôme de thanatopracteur est payante. Elle est également obligatoire pour pouvoir s'inscrire à l'examen d'accès au diplôme national de thanatopracteur. Pour plus de renseignements, il est conseillé de se rapprocher des organismes la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE), peut exercer en France l'activité de thanatopracteur, de manière temporaire et occasionnelle dès lors qu'il remplit l'une des conditions suivantes :

- être légalement établi dans cet État pour y exercer la même activité ;
- avoir exercé cette activité pendant un an au cours des dix dernières années qui précèdent la prestation lorsque ni la formation ni l'activité ne sont réglementées dans cet État ;
- avoir reçu une reconnaissance de ses qualifications professionnelles par la préfecture compétente.

*Pour aller plus loin* : articles L. 2223-23, L. 2223-47 et R. 2223-133 et suivants du Code général des collectivités territoriales.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de thanatopracteur dans cet État peut exercer la même activité en France de manière permanente si :

- il est titulaire d'un diplôme délivré par une autorité compétente d'un État membre, qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la profession, à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation ni l'exercice de la profession.

Pour cela, il devra demander la reconnaissance de ses qualifications professionnelles auprès du préfet du lieu d’installation (cf. infra « 4°. a. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE) »).

Si, lors de l'examen de la demande, le préfet constate qu'il existe des différences substantielles entre la formation et l'expérience professionnelle du ressortissant et celles exigées en France, des mesures de compensation pourront être prises (cf. infra « 4°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles L. 2223-49 et R. 2223-134 du Code des collectivités territoriales.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Bien que non réglementées, des obligations déontologiques et éthiques incombent aux thanatopracteurs, et notamment de :

- respecter la dignité et l'intégrité physique de la personne décédée ;
- respecter le secret professionnel ;
- pratiquer son activité avec la même conscience sur tous les défunts.

*Pour aller plus loin* : article R. 2223-132 du Code général des collectivités territoriales.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE)

#### Autorité compétente

Le préfet du lieu d'installation du ressortissant est compétent pour se prononcer sur la demande de reconnaissance de qualifications professionnelles.

#### Pièces justificatives

Cette demande s'effectue par l'envoi d'un dossier au préfet comprenant l'ensemble des documents suivants :

- une copie des diplômes, certificats et autres titres de formation délivrés par l'autorité compétente de cet État lorsque l’État membre réglemente l'accès à la profession ;
- une attestation ou un titre de formation justifiant que le ressortissant a exercé pendant un an l'activité dans cet État, lorsque l’État membre ne réglemente ni la profession ni l'activité.

#### Procédure

Le préfet accuse réception du dossier dans un délai d'un mois et informe le ressortissant, le cas échéant, de tout document manquant. La vérification des pièces est faite par trois personnes qualifiées dont un thanatopracteur. À l'issue de cette vérification, le préfet et le ministre chargé de la santé pourront décider de soumettre le ressortissant à une mesure de compensation en cas de différences substantielles entre sa qualification professionnelle et celle requise pour exercer en France.

Le préfet disposera d'un délai de quatre mois pour se prononcer sur la décision d'accorder ou non la reconnaissance de qualification au ressortissant. En cas de mesure de compensation, ce délai sera suspendu jusqu'à l'accomplissement du stage d'adaptation ou de l'épreuve d'aptitude.

#### Bon à savoir : mesures de compensation

Le choix de la mesure de compensation est laissé à l'appréciation du ressortissant qui dispose d'un délai d'un mois pour se décider.

Le stage d'adaptation, d'une durée maximale de deux ans, doit être réalisé dans une régie, une entreprise ou une association funéraire habilitée. Il fera l'objet d'une évaluation par le responsable du stage puis sera validée par le ministre chargé de la santé.

La vérification des connaissances du ressortissant, lors de l'épreuve d'aptitude, porte sur les matières précisées à l'article 1 de l'arrêté du 25 août 2009 portant mise en œuvre de la vérification des connaissances et des mesures compensatoires pour la reconnaissance des qualifications professionnelles dans le secteur funéraire. Elle est validée par le ministre chargé de la santé qui rendra son avis au préfet. Ce dernier informera le ressortissant de sa décision d'accorder la reconnaissance de qualification professionnelle.

*Pour aller plus loin* : articles L. 2223-49 et suivants, articles R. 2223-134 et suivants du Code général des collectivités territoriales.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).