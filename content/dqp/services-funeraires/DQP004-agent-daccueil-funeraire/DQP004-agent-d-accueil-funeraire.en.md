﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP004" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Funeral services" -->
<!-- var(title)="Funeral greeter" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="funeral-services" -->
<!-- var(title-short)="funeral-greeter" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/funeral-services/funeral-greeter.html" -->
<!-- var(last-update)="2020-04-15 17:22:26" -->
<!-- var(url-name)="funeral-greeter" -->
<!-- var(translation)="Auto" -->


Funeral greeter
====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:26<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Funeral Reception Officer is a professional in the public funeral home whose activity is to welcome, inform and advise families.

This professional can set up and sell funeral items, receive phone calls and refer families to either a master of ceremonies or a funeral counselor.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of a funeral reception agent, the professional must:

- training (see infra "2. Training") to justify his professional ability;
- practice in a prefectural clearance facility issued under the conditions prescribed by Sections R. 2223-56 to R. 2223-65 of the General Code of Local Government.

**Please note**

Professionals who have served as funeral reception officers for one year from May 10, 1995 are deemed to be justified in their professional training.

*For further information*: Articles D. 2223-35 and R. 2223-50 of the General Code of Local Government.

#### Training

To practice, the professional must complete training within six months of starting his activity. The 40-hour course covers:

- funeral legislation and regulations, as well as hygiene and safety (4 p.m.);
- The Psychology and Sociology of Grief (8 a.m.);
- the protocol of the funeral, the practices and the symbolism of the various funeral rites including the cremation (4 p.m.).

At the end of his training, the professional is given a certificate of vocational training by the organization or the national centre of the territorial public service.

This training must be provided by an accredited training organization. When speaking to public service officers, the training is provided by the National Territorial Public Service Centre.

*For further information*: Articles R. 2223-44 and R. 2223-53 of the General Code of Local Government.

#### Costs associated with qualification

Training leading to the activity of funeral reception officer is paid and its cost varies depending on the institution concerned. It is advisable to get closer to this property for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, legally established and acting as a funeral reception officer, may exercise the same occasional and temporary service on a casual and temporary basis. activity in France.

In order to do so, the person concerned must:

- Where neither access to the activity nor its exercise is regulated in that Member State, justify having exercised this activity in a Member State for at least one year in the last ten years;
- practice in an institution authorized by the prefect of the department where its head office is located (see Articles R. 2223-56 to R. 2223-65 of the General Code of Local Authorities).

*For further information*: Article L. 2223-47 of the General Code of Local Authorities.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU state or the EEA legally established and carrying out the activity of a funeral reception officer may carry out the same activity in France on a permanent basis.

To do so, the individual must justify an experience acquired over the past ten years:

- either as an executive or as an independent of:- three consecutive years in this activity,
  - two years in a row, as long as it justifies prior training and holds a certificate recognised by the Member State in which it has practised,
  - two consecutive years since he justifies having held this activity for three years as an employee;
- either as an employee and justify having undergo taken prior training sanctioned by a certificate issued in that Member State.

If the national does not justify any of these experiences he will have to justify either:

- Hold a certificate of competency or a training certificate allowing him to carry out the activity of a funeral reception officer issued in a Member State regulating the exercise of this activity;
- have been doing this for at least one year in the last ten years and have a certificate of competency or training title when the Member State does not regulate access or the exercise of this activity. This experience is not required if the certificate certifies regulated training.

Once the professional meets these requirements, he must apply for qualification recognition (see below "5°). a. Application for qualification recognition for a permanent exercise (LE)).

*For further information*: Article L. 2223-48 of the General Code of Local Authorities.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The professional is bound by the national funeral regulations.

As such, it must include:

- ensure compliance with family information provisions, including mandatory mentions of quotes and purchase orders issued in Articles R. 2223-24 to R. 2223-32-1 of the General Code of Local Authorities (information relating to the professional and the nature of the services offered);
- propose and comply with funeral funding requirements (see Section R. 2223-33 of the General Code of Local Authorities).

*For further information*: Articles R. 2223-23-5 and following of the General Code of Local Authorities.

4°. Criminal sanctions
------------------------------------------

The professional is fined 75,000 euros if he offers services in preparation for a funeral or within two months of death in order to obtain the order for supplies or benefits related to a death. Public railing for the same benefits is also prohibited.

In addition, it is punishable by a five-year prison sentence and a fine of 75,000 euros for a professional to offer directly or indirectly any benefits (offers, gifts, promises, etc.) to persons with knowledge of a death at the opportunity of their activity to obtain the conclusion of death-related benefits or to recommend the services of the professional.

*For further information*: Articles L. 2223-33 and L. 2223-35 of the General Code of Local Government.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Application for qualification for a permanent exercise (LE)

**Competent authority**

The professional must submit his request to the prefect of the department where he practices.

**Supporting documents**

The application is made by filing a file with the following supporting documents:

- A valid piece of identification
- A certificate of competency or a training certificate issued by a competent authority;
- any evidence, if any, that the national has worked as a funeral reception officer for one year full-time or part-time in the last ten years in a Member State that does not regulate the profession.

**Procedure**

The prefect acknowledges receipt of the request within one month and informs him in case of missing document. The prefect has the professional qualifications checked and, if necessary, may decide to subject him to a compensation measure (see "Good to know: compensation measures").

*For further information*: Articles R. 2223-133 and following from the General Code of Local Authorities.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the professional and that required to carry out the activity of a funeral reception officer, the prefect of the department may require that he submit to the choice of an aptitude test or a adaptation course.

The accommodation course consists of the national working with a professional as a funeral reception officer for up to two years. The aptitude test, on the other hand, involves the verification of all the professional knowledge set out in Article 2 of the Order of 25 August 2009 implementing the knowledge audit and compensatory measures for the recognition of professional qualifications in the funeral sector.

*For further information*: Article L. 2223-50 of the General Code of Local Authorities.

### b. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

