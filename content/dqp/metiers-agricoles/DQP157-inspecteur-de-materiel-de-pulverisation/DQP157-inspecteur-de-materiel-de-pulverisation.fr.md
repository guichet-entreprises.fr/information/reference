﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP157" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers agricoles" -->
<!-- var(title)="Inspecteur de matériel de pulvérisation de produits phytopharmaceutiques" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-agricoles" -->
<!-- var(title-short)="inspecteur-de-materiel-de-pulverisation" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-agricoles/inspecteur-de-materiel-de-pulverisation-de-produits-phytopharmaceutiques.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="inspecteur-de-materiel-de-pulverisation-de-produits-phytopharmaceutiques" -->
<!-- var(translation)="None" -->

# Inspecteur de matériel de pulvérisation de produits phytopharmaceutiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l’activité

L'inspecteur de matériel de pulvérisation de produits phytopharmaceutiques est un professionnel, dont l'activité consiste à vérifier, à intervalles réguliers, le matériel nécessaire à l'application des produits phytopharmaceutiques (pesticides).

Ce contrôle est effectué par un organisme d'inspection, à la demande et à la charge du propriétaire du matériel.

*Pour aller plus loin* : article L. 256-2 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale 

Pour exercer l'activité d'inspecteur de matériel de pulvérisation de produits phytopharmaceutiques, le professionnel doit être titulaire d'un certificat pour la réalisation des contrôles d'une ou plusieurs catégories de pulvérisateurs sanctionnant la réussite à un examen à l'issue de la formation assurée par un centre de formation agréé.

*Pour aller plus loin* : articles D. 256-16 et D. 256-23 du Code rural et de la pêche maritime 

#### Formation

##### Obtenir un certificat d'exercice

Pour être reconnu comme étant qualifié professionnellement et obtenir son certificat, le professionnel doit suivre une formation délivrée par l'un des centres de formation, agréés par le ministère chargé de l'agriculture.

Cette formation se compose :

- d'un premier module, d'une durée minimale de 23 heures portant sur :
  - l'ensemble des généralités liées aux pulvérisateurs comme l'entretien, les réglages, etc. (durée minimale de 3 heures),
  - une initiation au protocole de contrôle théorique et pratique (durée minimale de 14 heures),
  - la santé et la sécurité de l'inspecteur (durée minimale de 2 heures),
  - la relation entre l'inspecteur et son client (durée de 2 heures) ;
- d'un second module, d'une durée minimale de 19 heures portant sur :
  - la réglementation et le cadre de l'exercice de la profession d'inspecteur (durée minimale de 4 heures),
  - la maîtrise du protocole de contrôle du matériel et mise en pratique (durée minimale de 8 heures),
  - les équipements de contrôle (durée minimale de 4 heures).

Pour obtenir le certificat, le professionnel doit passer deux tests :

- à l'issue du premier module, d'une durée de deux heures prenant la forme d'un questionnaire à choix-multiples (QCM) de trente questions. Pour pouvoir poursuivre le parcours de formation et obtenir le certificat, le seuil de réussite est fixé à 20 bonnes réponses sur 30. En cas d'échec, un nouveau questionnaire à choix multiples doit être proposé au candidat dans les plus brefs délais ;
- à l'issue du second module, le test se compose :
  - d'une épreuve pratique au cours de laquelle il doit réaliser un contrôle de matériel et dresser un rapport de contrôle,
  - d'une épreuve orale portant sur le cadre de l'exercice de la profession, la santé et la sécurité de l'inspecteur et le bon fonctionnement des matériels de contrôle, ainsi que sur la réalisation d'un contrôle sur une autre catégorie de matériel que celle pour laquelle il a réalisé le contrôle au cours de l'épreuve pratique.

Dès lors que le candidat a atteint 2/3 des objectifs, il obtient un certificat de qualification conforme au modèle fixé à l'annexe II de l'arrêté du 18 décembre 2008 relatif aux centres de formation d'inspecteurs de pulvérisateurs pris en application de l'article D. 256-24 du Code rural et de la pêche maritime.

Ce certificat est valable pendant cinq ans renouvelable (cf. infra « Bon à savoir : renouvellement du certificat »).

##### Dispense de formation possible

Certains professionnels peuvent être dispensés du suivi de la formation du premier module, dès lors qu'ils possèdent une expérience professionnelle d'au moins trois mois comportant obligatoirement le réglage des pulvérisateurs et qu’ils sont titulaires de l'un des diplômes fixés à l'annexe III de l'arrêté du 18 décembre 2008 précité.

Dès lors qu'ils remplissent ces conditions, ils ont la possibilité de passer l'épreuve de formation du premier module, et en cas de réussite, accéder au second module de formation.

En outre, sont totalement dispensés du premier module (formation et épreuve d'accès au second module), les ressortissants d'un État membre de l’Union européenne, d'un État partie à l'Espace économique européen, mentionnés aux articles D. 256-27 et D. 256-28 du Code rural et de la pêche maritime ayant fourni au groupement d'intérêt public Pulvés les documents prévus aux mêmes articles, et dont les connaissances et les compétences ont été jugées insuffisantes pour exercer la profession d'inspecteur des pulvérisateurs.

##### Bon à savoir : renouvellement du certificat

Lors du renouvellement de son certificat, le professionnel est tenu de suivre un module de renouvellement qui vise à la mise à jour des connaissances et des pratiques professionnelles mobilisées lors du contrôle périodique obligatoire des pulvérisateurs. Ce module comprend une formation spécifique d'une durée de 21 heures minimum suivi d’une épreuve d'une durée de 2 heures. Cette épreuve est composée de trente questions de type questions à choix multiple. Pour obtenir le renouvellement du certificat, le seuil de réussite est fixé à 20 bonnes réponses sur 30.

En cas d'échec, un nouveau questionnaire à choix multiple doit être proposé au candidat dans les plus brefs délais.

Le modèle du certificat de renouvellement est fixé à l'annexe II de l'arrêté du 18 décembre 2008 précité.

*Pour aller plus loin* : [site officiel du Groupement d'intérêt public Pulvés](https://controleo.net/component/seoglossary/1-controleo/gip-pulve) ; arrêté du 18 décembre 2008 relatif aux centres de formation d'inspecteurs de pulvérisateurs pris en application de l'article D. 256-24 du Code rural et de la pêche maritime.

##### Agrément de l'organisme d'inspection

L'inspecteur titulaire d'un certificat doit exercer au sein d'un organisme d'inspection agréé par le préfet de région au sein de laquelle se trouve son siège social.

L'organisme d'inspection est réputé agréé dès lors qu'il est accrédité par le [Comité français d'accréditation](https://www.cofrac.fr/) (Cofrac), ou par tout autre organisme d'accréditation signataire de l'accord de reconnaissance multilatéral établi par la coordination européenne des organismes d'accréditation, conformément aux annexes A, B ou C de la norme NF EN ISO/ CEI 17020 pour les activités de « contrôle périodique des pulvérisateurs ».

Pour obtenir son agrément, l'organisme d'inspection doit justifier :

- exercer son activité à titre indépendant des autres activités de contrôles, de fabrication ou de distribution de produits phytopharmaceutiques ;
- que l'ensemble des inspecteurs qu'il emploie sont titulaires du certificat d'exercice ;
- que l'ensemble des équipements de contrôle qu'il possède, respectent les exigences en matière de conformité et d'étalonnage ;
- posséder un système d'archivage des documents et que ceux-ci soient tenus à jour et accessibles.

**À noter**

Depuis le 1er janvier 2018, tout organisme d'inspection qui réalise au moins trois cent cinquante inspections par an, doit être accrédité en vue d'obtenir ou renouveler son agrément.

Dès lors qu'il remplit ces conditions, l'organisme doit adresser une demande au GIP Pulvés (cf. infra. « 5°. a. Demande en vue d'obtenir un agrément pour un organisme d'inspection »).

*Pour aller plus loin* : articles D. 256-15 à D. 256-20-1 du Code rural et de la pêche maritime et arrêté du 18 décembre 2008 relatif aux organismes d'inspection des pulvérisateurs pris en application des articles D. 256-20 et D. 256-26 du Code rural et de la pêche maritime.

#### Coûts associés à la qualification

La formation menant à la qualification d'inspecteur de matériel de pulvérisation de produits phytopharmaceutiques est payante et son coût varie selon le centre de formation concerné. Il est conseillé de se rapprocher de ces établissements pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services) 

Le ressortissant d’un État membre de l’UE ou de l’EEE, exerçant l’activité d’inspecteur de matériel de pulvérisation de produits phytopharmaceutiques dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit effectuer, préalablement à sa première prestation, une déclaration adressée au préfet de la région dans laquelle il souhaite exercer (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la déclaration par le préfet. 

*Pour aller plus loin* : articles L. 2041 et R. 2041 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, légalement établi et exerçant l'activité d'inspecteur de matériel de pulvérisation de produits phytopharmaceutiques, peut exercer à titre permanent, la même activité en France.

Pour cela, le ressortissant doit justifier être titulaire d'un titre de formation ou d'une attestation de compétence délivrée par un État membre réglementant l'accès et l'exercice de la profession.

Lorsque l’État ne réglemente pas l’exercice de la profession, le ressortissant doit justifier avoir exercé la profession d'inspecteur de matériel de pulvérisation de produits phytopharmaceutiques pendant un an, au cours des dix dernières années.

Lorsque la formation menant à l'exercice de cette profession, dont le ressortissant est titulaire, est réglementée dans cet État membre, aucune expérience professionnelle n'est requise.

Dès lors qu'il remplit ces conditions, le ressortissant est réputé être titulaire du certificat d'exercice. Ce dernier est délivré pour cinq ans. Il est renouvelé après suivi d'une formation spécifique dans un centre de formation agréé.

Lorsqu'il existe des différences substantielles entre la formation reçue par le ressortissant, titulaire d'un certificat, et celle requise pour exercer en France, ce dernier sera soumis, à son choix, à une épreuve d’aptitude ou à un stage d’adaptation. 

*Pour aller plus loin* : articles R. 204-2, R. 204-3 et D. 256-27 du Code rural et de la pêche maritime.

## 3°. Protocole et modalités du contrôle

L'inspecteur est tenu de respecter les modalités relatives au protocole de contrôle des matériels de pulvérisation de produits phytopharmaceutiques.

À ce titre, il doit :

- s'assurer que le pulvérisateur possède un identifiant (une plaque ou un autocollant) de manière lisible et indélébile et conforme aux caractéristiques fixées à l'annexe I de l'arrêté du 18 décembre 2008 relatif aux modalités de contrôle des pulvérisateurs pris en application du 1° de l'article D. 256-14 du Code rural et de la pêche maritime ;
- contrôler et relever les défauts de l'ensemble des points d'inspection dont la liste est fixée à l'annexe IIde l'arrêté du 18 décembre 2008 précité ;
- effectuer une contre-visite dès lors qu'un défaut est constaté et nécessite une réparation ;
- dresser un rapport d'inspection dont le modèle est fixé à l'annexe IV de l'arrêté susvisé.

Si aucun défaut n'est constaté, lors du premier contrôle ou à l'issue de la contre-visite, l'inspecteur doit apposer sur le pulvérisateur, de manière visible, une vignette, dont le modèle est fixé à l'annexe III de l'arrêté susvisé.

*Pour aller plus loin* : arrêté du 18 décembre 2008 relatif aux modalités de contrôle des pulvérisateurs pris en application du 1° de l'article D. 256-14 du Code rural et de la pêche maritime.

## 4°. Sanctions pénales

L'organisme d'inspection qui procède à des contrôles sans être titulaire d'un agrément ou qui fait procéder à de tels contrôles par des inspecteurs non titulaires d'un certificat d'exercice, encourt une amende de 1 500 euros.

*Pour aller plus loin* : article R. 256-31 du Code rural et de la pêche maritime.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir un agrément pour un organisme d'inspection

#### Autorité compétente

L'organisme d'inspection doit adresser une demande au GIP Pulvés.

#### Pièces justificatives

Il doit, pour cela, doit fournir un dossier contenant :

- un document justifiant qu'il est titulaire d'une accréditation délivrée par le Cofrac ou un autre organisme d'accréditation signataire de l'accord de reconnaissance multilatéral. À défaut, il doit joindre le règlement prévu à l'article L. 256-2-1 du Code rural et de la pêche maritime correspondant à la première visite du GIP ;
- l'organisation de ses différentes activités ;
- les informations relatives aux inspecteurs qu'il emploie ;
- l'ensemble des procédures et des équipements de contrôle qu'il possède ainsi que leurs certificats d'étalonnage ;
- l'ensemble des procédures internes en vue d'effectuer les contrôles réguliers des appareils de mesure ;
- le descriptif de l'ensemble de ses installations ;
- le cas échéant, les diagnostics volontaires réalisés avant le 1er janvier 2009.

En outre, l'organisme doit s'engager auprès du GIP à :

- lui transmettre les résultats des contrôles des pulvérisateurs ;
- mettre en œuvre les moyens techniques en vue de faciliter sa mission et celle des agents de contrôles de conformité de l'organisme ;
- d'employer exclusivement des inspecteurs titulaire d'un certificat ;
- de s'acquitter de la redevance prévue à l'article L. 256-2-1 du Code rural et de la pêche maritime ;
- de communiquer la décision de l'organisme d'accréditation après chacun de ses audits.

#### Délais et procédure

Le GIP doit s'assurer que l'organisme exécute les obligations pour lesquels il s'est engagé. Lorsque l'organisme n'est pas accrédité, le GIP doit effectuer des visites de contrôle.

L'agrément est délivré pour une durée de cinq ans.

**À noter** 

Lorsque l'organisme justifie que les inspecteurs qu'il emploie ont réalisé au moins cinquante contrôles ou ont suivi un stage d'au moins une semaine au sein d'un organisme d'inspection agréé, le récépissé de demande d'agrément vaut agrément provisoire. Cet agrément provisoire est valable jusqu'à ce que la demande soit traitée et au plus tard six mois après la délivrance du récépissé.

Il est conseillé de s'adresser au GIP Pulvés pour de plus amples informations.

*Pour aller plus loin* : articles D. 256-17 à D. 256-20-1 ; arrêté du 18 décembre 2008 relatif aux organismes d'inspection des pulvérisateurs pris en application des articles D. 256-20 et D. 256-26 du Code rural et de la pêche maritime.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne.

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).