﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP157" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Agricultural occupations" -->
<!-- var(title)="Inspector of plant protection product spraying equipment" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="agricultural-occupations" -->
<!-- var(title-short)="inspector-of-plant-protection-product-spraying-equipment" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/agricultural-occupations/inspector-of-plant-protection-product-spraying-equipment.html" -->
<!-- var(last-update)="2020-04-15 17:20:49" -->
<!-- var(url-name)="inspector-of-plant-protection-product-spraying-equipment" -->
<!-- var(translation)="Auto" -->


Inspector of plant protection product spraying equipment
===================================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:49<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The phytopharmaceutical spray inspector is a professional, whose activity consists of checking, at regular intervals, the equipment necessary for the application of phytopharmaceuticals (pesticides).

This inspection is carried out by an inspection body, at the request and at the expense of the owner of the equipment.

*For further information*: Article L. 256-2 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of inspector of phytopharmaceutical spraying equipment, the professional must hold a certificate for carrying out the checks of one or more categories of sprayers sanctioning passing an exam after training at an accredited training centre.

*For further information*: Articles D. 256-16 and D. 256-23 of the Rural and Marine Fisheries Code

#### Training

**Get a certificate of practice**

To be recognized as a professionally qualified and to obtain a certificate, the professional must undergo training from one of the training centres, approved by the Ministry of Agriculture.

This training consists of:

- a first module, with a minimum duration of 23 hours, covering:- all the generalities related to sprayers such as maintenance, settings, etc. (minimum duration of 3 hours),
  - an introduction to the theoretical and practical control protocol (minimum duration of 14 hours),
  - the health and safety of the inspector (minimum duration of 2 hours),
  - The relationship between the inspector and his client (duration of 2 hours);
- a second module, with a minimum duration of 19 hours, covering:- regulation and the framework for the practice of the profession of inspector (minimum duration of 4 hours),
  - mastering the equipment control protocol and putting it into practice (minimum duration of 8 hours),
  - control equipment (minimum duration of 4 hours).

To obtain the certificate, the professional must pass two tests:

- at the end of the first module, which lasts two hours in the form of a 30-question multiple-choice questionnaire (MQC). In order to continue the training course and obtain the certificate, the threshold for success is set at 20 correct answers out of 30. In the event of failure, a new multiple-choice questionnaire should be offered to the candidate as soon as possible;
- At the end of the second module, the test consists of:- a practical test in which it must carry out a hardware check and draw up a control report,
  - an oral test on the framework of the practice of the profession, the health and safety of the inspector and the proper functioning of the control equipment, as well as the carrying out of a control over a category of equipment other than the one for which he carried out the check during the practical test.

Once the candidate has achieved 2/3 of the objectives, he or she obtains a certificate of qualification in accordance with the model set out in Schedule II of the December 18, 2008 order on spray inspector training centres under Article D. 256-24 of the Rural Code and Marine Fisheries.

This certificate is valid for five years renewable (see infra "Good to know: renewal of the certificate").

**Possible training exemption**

Some professionals may be exempt from the training of the first module, as long as they have at least three months of professional experience involving the proper setting of the sprayers and they hold one Schedule III of the order of 18 December 2008.

Once they meet these requirements, they have the opportunity to pass the training test of the first module, and if successful, access to the second training module.

In addition, the first module (training and access test to the second module) are completely exempt, the nationals of a Member State of the European Union, a State party to the European Economic Area, mentioned in Articles D. 256-27 and D. 256-28 of the The Rural and Marine Fisheries Code that provided the Public Interest Group Pulvés with the documents provided for in the same sections, and whose knowledge and skills were deemed insufficient to practice the profession of spray inspector.

**Good to know: certificate renewal**

When renewing the certificate, the professional is required to follow a renewal module that aims to update the knowledge and professional practices mobilized during the mandatory periodic check of sprayers. This module includes a specific training course lasting at least 21 hours followed by a 2-hour test. This test consists of thirty multiple-choice question questions. To obtain the renewal of the certificate, the threshold for success is set at 20 correct answers out of 30.

In the event of failure, a new multiple-choice questionnaire should be offered to the candidate as soon as possible.

The renewal certificate model is set in Schedule II of the Order of December 18, 2008.

*For further information*: :[official website of the Pulvés Public Interest Group](http://www.gippulves.fr) ; Order of 18 December 2008 relating to spray inspector training centres under Article D. 256-24 of the Rural Code and Marine Fisheries.

**Accreditation of the inspection body**

The certificate-holding inspector must practice in an inspection body approved by the prefect of the region within which his head office is located.

The inspection body is deemed to be accredited as long as it is accredited by the French Accreditation Committee ([Cofrac](https://www.cofrac.fr/)), or by any other accreditation body that is a signatory to the multilateral recognition agreement established by the European accreditation body coordination, in accordance with Schedules A, B or C of the NF EN ISO/CIS 17020 standard for "periodic sprayer control" activities.

To obtain approval, the inspection body must justify:

- operate independently of other control, manufacturing or distribution activities of phytopharmaceuticals;
- that all of the inspectors it employs hold the certificate of practice;
- That all the control equipment it possesses meets compliance and calibration requirements;
- have a system for archiving documents and that they are kept up-to-date and accessible.

**Please note**

As of January 1, 2018, any inspection body that conducts at least three hundred and fifty inspections per year must be accredited for certification or renewal.

Once it meets these conditions, the organization must submit an application to the Pulvés GIP (see below. "5. a. Request for approval for an inspection body").

*For further information*: Articles D. 256-15 to D. 256-20-1 of the Rural and Marine Fisheries Code and order of December 18, 2008 relating to spray inspection agencies under sections D. 256-20 and D. 256-26 of the Rural Code and Marine Fisheries.

#### Costs associated with qualification

Training leading to the qualification of phytopharmaceutical spraying equipment inspector is paid for and the cost varies depending on the training centre involved. It is advisable to get closer to these establishments for more information.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

A national of an EU or EEA Member State, who works as an inspector of phytopharmaceutical spraying equipment in one of these states, may use his or her professional title in France on a temporary or casual basis. He must make a statement to the prefect of the region in which he wishes to practice (see below "5o). a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional must have carried it out in one or more Member States for at least one year, during the ten years which precede the performance.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test in a one month from the prefect's receipt of the declaration.

*For further information*: Articles L. 2041 and R. 2041 of the Rural Code and Marine Fisheries.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, legally established and acting as an inspector of phytopharmaceutical spraying equipment, may carry out the same activity on a permanent basis in France.

In order to do so, the national must justify holding a training certificate or a certificate of competence issued by a Member State regulating access and practice of the profession.

Where the state does not regulate the practice of the profession, the national must justify having practiced as an inspector of phytopharmaceutical spraying equipment for one year, in the last ten years.

Where training leading to the practice of this profession, of which the national holds, is regulated in that Member State, no work experience is required.

Once the national fulfils these conditions, he or she is deemed to hold the certificate of exercise. The latter is issued for five years. It is renewed after followed by specific training in an accredited training centre.

Where there are substantial differences between the training received by the national, who holds a certificate, and that required to practice in France, the latter will be subjected, at his choice, to an aptitude test or an adjustment course.

*For further information*: Articles R. 204-2, R. 204-3 and D. 256-27 of the Rural and Marine Fisheries Code.

3°.Protocol and modalities of control
-------------------------------------------------

The inspector is required to comply with the procedures for the protocol for controlling spraying equipment for phytopharmaceuticals.

As such, it must:

- Ensure that the sprayer has an identifier (a plate or sticker) in a readable and indelible manner and in accordance with the characteristics set out in Schedule I of the December 18, 2008 order on the control of sprayers taken in 1st of Article D. 256-14 of the Rural Code and Maritime Fisheries;
- to check and identify the defects of all inspection points listed in Schedule IIde the order of 18 December 2008 mentioned above;
- make a counter-visit as soon as a defect is found and requires repair;
- prepare an inspection report, the model of which is attached to Schedule IV of the above-posted order.

If no defects are found, during the first check or at the end of the counter-visit, the inspector must put a sticker on the sprayer, in a visible way, the model of which is attached to the[Appendix III](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=B52089429CC279EA134E0A00ABA104FF.tplgfr34s_1?idArticle=LEGIARTI000032750781&cidTexte=LEGITEXT000032750734&dateTexte=20180418) of the aforementioned decree.

*For further information*: order of 18 December 2008 relating to the control procedures for sprayers taken under Article D. 256-14 of the Rural Code and Maritime Fisheries.

4°. Criminal sanctions
------------------------------------------

An inspection body that carries out checks without a license or has such checks carried out by inspectors who do not have a certificate of practice is liable to a fine of 1,500 euros.

*For further information*: Article R. 256-31 of the Rural Code and Marine Fisheries.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for approval for an inspection body

**Competent authority**

The inspection body must submit an application to the Pulvés GIP.

**Supporting documents**

In order to do so, it must provide a file containing:

- a document justifying that it holds accreditation from the Cofrac or another accreditation body that is a signatory to the multilateral recognition agreement. Failing that, it must attach the regulation under Article L. 256-2-1 of the Rural Code and Maritime Fisheries corresponding to the first visit of the GIP;
- The organisation of its various activities;
- Information about the inspectors it employs
- all the procedures and control equipment it has and their calibration certificates;
- All internal procedures for carrying out regular checks of measuring devices;
- The description of all of its facilities;
- voluntary diagnoses made before January 1, 2009.

In addition, the organization must commit to the IIP to:

- transmit the results of sprayer checks to them.
- Implementing the technical means to facilitate its mission and that of the organization's compliance control officers;
- employing only certificate-holder inspectors;
- to pay the levy under Section L. 256-2-1 of the Rural Code and Marine Fisheries;
- to communicate the accreditation body's decision after each of its audits.

**Delays and procedures**

The IPI must ensure that the organization is carrying out the obligations for which it has committed. When the organization is not accredited, the IPI must conduct inspection visits.

Accreditation is issued for a period of five years.

**Please note**

Where the organization justifies the inspectors it employs have completed at least fifty checks or have completed a training course of at least one week in an approved inspection body, the receipt for accreditation is owed provisional approval. This provisional approval is valid until the application is processed and no later than six months after the receipt is issued.

It is advisable to contact the[GIP Pulvés](http://www.gippulves.fr/) for more information.

*For further information*: Articles D. 256-17 to D. 256-20-1; December 18, 2008 order on sprayer inspection agencies under Sections D. 256-20 and D. 256-26 of the Rural Code and Marine Fisheries.

### b. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form.

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

