﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP097" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Agricultural occupations" -->
<!-- var(title)="Distributor of plant protection products" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="agricultural-occupations" -->
<!-- var(title-short)="distributor-of-plant-protection-products" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/agricultural-occupations/distributor-of-plant-protection-products.html" -->
<!-- var(last-update)="2020-04-15 17:20:47" -->
<!-- var(url-name)="distributor-of-plant-protection-products" -->
<!-- var(translation)="Auto" -->




Distributor of plant protection products
===================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:47<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The distributor of phytopharmaceuticals is a professional whose activity consists of ensuring the sale, sale or distribution of phytopharmaceuticals free of charge to users of these products or to individuals. or morals acting on their behalf, including buying groups.

*For further information*: Articles L. 254-1 and R. 254-1 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to be a distributor of plant protection products, the professional must:

- Hold an individual certificate of exercise called "certiphyto";
- Get approval
- once a year provide individualized advice to each of their clients who use phytopharmaceuticals;
- keep a record of their sales (see infra "3.3. Keeping a sales register").

*For further information*: Articles L. 254-1, L. 254-3, L. 254-6 and L. 254-7 of the Rural Code and Marine Fisheries.

#### Training

**Individual certificate for the activity "sale, sale of phytopharmaceuticals"**

The individual certificate is issued to the professional who is:

- underwent training that included a one-hour written test of knowledge verification;
- successfully passed a one-and-a-half hour test consisting of thirty questions on the training programme set out in Schedule II of the August 29, 2016 order;
- holds one of the diplomas on the schedule I list of the August 29, 2016 order, obtained in the five years prior to the application date.

Once the professional fulfils one of these conditions, he must apply for the certificate (see below "5°). a. Request for an individual certificate of practice").

*For further information*: decree of August 29, 2016 establishing and setting out the terms of obtaining the individual certificate for the activity "sale, sale of phytopharmaceuticals".

### b. EU nationals: for temporary and casual exercise (free provision of services (LPS))

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) holding a certificate issued by a Member State, legally established and carrying out a product distribution activity phytopharmaceuticals, may carry out the same activity on a temporary and casual basis in France.

Once it fulfils these conditions, it must, prior to its first provision of services, make a declaration to the Regional Director of Food, Agriculture and Forestry (DRAAF) of the place where the first service is delivered. services (see infra "5°. c. Pre-declaration for the national for a temporary and casual exercise (LPS)."

*For further information*: Articles L. 204-1 and R. 254-9 of the Rural Code and Marine Fisheries.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) holding a certificate issued by a Member State, legally established and acting as a distributor of products phytopharmaceuticals, can carry out the same activity on a permanent basis in France.

As long as the national holds an individual certificate of exercise issued by a Member State, he is deemed to hold the certificate required to practise in France.

In addition, the professional is subject to the same installation conditions as the French national (see infra "5°). Qualification recognition procedures and formalities").

*For further information*: II of Article R. 254-9 of the Rural Code and Marine Fisheries.

3°. Conditions of honorability, professional rules
-------------------------------------------------------------

**Obligations for the sale of phytopharmaceuticals to non-professional users**

Only products with the word "authorized use in gardens" may be listed for sale, sold or distributed to non-professional users, except if they are acting on behalf of professionals. If so, these users must justify being in possession:

- for owners of land for non-agricultural and non-forestry use, managed by a third party:- a contract or certificate of delegation to one-third of all plant protection decisions,
  - proof of the professional user quality of this third party;
- for farmers or foresters, who are not involved in the definition of the treatment strategy or the choice of products to be used and who use an organism for all their phytopharmaceutical application work. approved for service delivery application:- written evidence of the completion of the service by a licensed legal entity for the independent consulting or distribution of phytopharmaceuticals to professional users,
  - a contractual document with an approved organization for service delivery application with the name of a certified person.

In addition, in the event of a sale of products that do not carry this mention, the professional is obliged to ensure beforehand the professional quality of the future buyer. To do so, it must verify that the latter has an individual certificate of practice in one of the following categories:

- "Decision-maker in work and services";
- "Decision-maker in agricultural work";
- "Territorial Community Applicator."

At each sale, the professional is required to:

- separate products labelled "authorized use in gardens" and those that do not, using an explicit signage process;
- provide all information about the use of products and the risks they pose to health and the environment, as well as safety guidelines to prevent risks.

*For further information*: Articles R. 254-20 to -22 of the Rural Code and Marine Fisheries; January 6, 2016 order on the documentation required for the purchase of phytopharmaceuticals from the range of "professional" uses.

**Obligation to inform customers who use phytopharmaceuticals**

The professional who sells or distributes phytopharmaceuticals must provide an individualized written advice to his clients at least once a year stating:

- The active ingredient and the recommended specialty
- The target of this product, as well as the parcels and area affected by its use;
- The dose to be used and the conditions for its implementation;
- alternative methods (non-chemical methods and the use of biocontrol products).

In addition, when the professional sells these products, he or she must provide all information relating to the use of phytopharmaceuticals, potential health and environmental risks, and safety guidelines.

In addition, in the event of the transfer of products to non-professional users, the professional must provide all information relating to potential risks to human health and the environment (dangers, exposure, storage conditions and guidelines for the safe handling, application and disposal of these products and low-risk alternatives).

*For further information*: Article L. 254-7 of the Rural Code and Marine Fisheries.

**Keeping a sales register**

The professional must maintain a record of sales made mentioning product information (trade name, authorization number, quantity sold, amount of royalty).

In addition, for the distribution of phytopharmaceuticals to professional users, the distributor must also include on the register:

- The number and billing date, if applicable.
- The end-user's postcode of the product
- Documents justifying the user's professional status
- all information about the treated seed (the plant species concerned, the number and date of the billing).

Each year, the professional must take stock of the previous calendar year with all the information mentioned above during the sale of each phytopharmaceutical product.

In addition, each year before 1 April, it must transmit this information to the water agency and the water board electronically, in support of the diffuse pollution levy.

*For further information*: Articles R. 254-23 and the following of the Rural Code and Marine Fisheries.

4°. Insurance and sanctions
-----------------------------------------------

**Insurance**

The professional is required, in order to obtain his approval, to take out insurance covering his professional civil liability.

*For further information*: Article L. 254-2 of the Rural Code and Marine Fisheries.

**Criminal sanctions**

A professional who works as a distributor of phytopharmaceuticals without a license is liable to a six-month prison sentence and a fine of 15,000 euros.

In addition, the professional is fined 1,500 euros for the sale of phytopharmaceuticals to non-professionals in violation of the provisions of Article L. 254-20 of the Rural Code and Maritime Fisheries.

*For further information*: Articles L. 254-12 and R. 254-30 of the Rural code and marine fisheries.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for the individual certificate of exercise

**Competent authority**

The professional must submit his request to the Regional Directorate of Food, Agriculture and Forestry (DRAAF) of the place of his residence, or, if necessary, the place of the head office of the organization where his training was carried out.

**Supporting documents**

Applicants are required to create an online account on the website[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) to access the certificate application service.

His application must include, depending on the case:

- A proof of follow-up training and, if necessary, a pass to the test;
- copy of his diploma or training title.

**Time and procedure**

The certificate is issued within two months of filing his application. In the absence of the issuance of the certificate after this period, and unless notification of refusal, the above supporting documents are worth individual certificate for up to two months.

This certificate is valid for five years and renewable under the same conditions.

*For further information*: Articles R. 254-11 and R. 254-12 of the Rural and Marine Fisheries Code.

### b. Application for certification for the practice of the distributor activity

**Competent authority**

The professional must submit his request to the prefect of the region in which the company's head office is located. If the professional is headquartered in a Member State other than France, he will have to submit his request to the prefect of the region in which he will carry out his first service delivery.

**Supporting documents**

His application must include the[Application form](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completed and signed as well as the following documents:

- A certificate of professional liability insurance policy;
- A certificate issued by a third-party body justifying that it performs this activity in accordance with the conditions guaranteeing the protection of public health and the environment, and the good information of users;
- copying a contract with a third-party agency providing for the follow-up necessary to maintain certification.

**Please note**

Micro-distributors must submit the following documents to DRAAF:

- a certificate of professional liability insurance;
- proof of the detention of Certiphyto by all staff performing supervisory, sales or advisory functions;
- A document justifying that it is subject to the micro-enterprise tax system;

**Time and procedure**

The professional who starts his activity must apply for an interim approval which will be granted for a period of six months non-renewable. Following this, he will be able to apply for final approval.

The silence kept by the prefect of the region beyond a period of two months is worth decision of rejection.

*For further information*: Articles R. 254-15-1 to R. 254-17 of the Rural Code and Marine Fisheries; order of 27 April 2017 amending the order of 25 November 2011 relating to the certification framework provided for in Article R. 254-3 of the Rural and Maritime Fisheries Code for the activity "distribution of phytopharmaceuticals to non-users professionals."

**Please note**

Even if you apply for several activities, the accreditation obtained will be unique.

### c. Pre-declaration for the national for a temporary and casual exercise (LPS)

**Competent authority**

The national must apply by any means to the DRAAF for the place where he or she first receives services.

**Supporting documents**

Its application must include the individual certificate of exercise issued by the Member State and, if necessary, with its translation into French by a certified translator.

**Please note**

This declaration must be renewed every year and in the event of a change in employment status.

*For further information*: III of Article R. 254-9 of the Rural Code and Marine Fisheries.

### d. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

