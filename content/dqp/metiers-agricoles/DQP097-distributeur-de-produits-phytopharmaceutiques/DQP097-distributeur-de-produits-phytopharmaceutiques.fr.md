﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP097" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers agricoles" -->
<!-- var(title)="Distributeur de produits phytopharmaceutiques" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-agricoles" -->
<!-- var(title-short)="distributeur-de-produits-phytopharmaceutiques" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-agricoles/distributeur-de-produits-phytopharmaceutiques.html" -->
<!-- var(last-update)="2020-04-15 17:20:47" -->
<!-- var(url-name)="distributeur-de-produits-phytopharmaceutiques" -->
<!-- var(translation)="None" -->

# Distributeur de produits phytopharmaceutiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:47<!-- end-var -->

## 1°. Définition de l’activité

Le distributeur de produits phytopharmaceutiques est un professionnel dont l'activité consiste à assurer la mise en vente, la vente ou la distribution à titre gratuit des produits phytopharmaceutiques aux utilisateurs de ces produits ou aux personnes physiques ou morales agissant pour leur compte, y compris les groupements d’achat.

*Pour aller plus loin* : articles L. 254-1 et R. 254-1 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de distributeur de produits phytosanitaires, le professionnel doit :

- être titulaire d'un certificat individuel d'exercice dit « certiphyto » ;
- obtenir un agrément ;
- adresser une fois par an un conseil individualisé à chacun de ses leurs clients utilisateurs professionnels des produits phytopharmaceutiques ;
- tenir un registre de leurs ventes (cf. infra « 3°. Tenue d'un registre de ventes »).

*Pour aller plus loin* : articles L. 254-1, L. 254-3, L. 254-6 et L. 254-7 du Code rural et de la pêche maritime.

#### Formation

##### Certificat individuel pour l'activité « mise en vente, vente des produits phytopharmaceutiques »

Le certificat individuel est délivré au professionnel qui soit :

- a suivi une formation comprenant une épreuve écrite d'une heure de vérification des connaissances ;
- a passé avec succès un test d'une heure trente composé de trente questions portant sur le programme de formation fixé à l'annexe II de l'arrêté du 29 août 2016 ;
- est titulaire de l'un des diplômes inscrits sur la liste fixée à l'annexe I de l'arrêté du 29 août 2016, obtenu au cours des cinq ans précédant la date de la demande.

Dès lors qu'il remplit l'une de ces conditions, le professionnel doit adresser une demande en vue d'obtenir le certificat (cf. infra « 5°. a. Demande en vue d'obtenir un certificat individuel d'exercice »).

*Pour aller plus loin* : arrêté du 29 août 2016 portant création et fixant les modalités d'obtention du certificat individuel pour l'activité « mise en vente, vente des produits phytopharmaceutiques ».

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) titulaire d'un certificat délivré par un État membre, légalement établi et exerçant une activité de distribution de produits phytopharmaceutiques, peut exercer à titre temporaire et occasionnel la même activité en France.

Dès lors qu'il remplit ces conditions, il doit, préalablement à sa première prestation de services, effectuer une déclaration auprès du directeur régional de l'alimentation, de l'agriculture et de la forêt (DRAAF) du lieu d’exercice de la première prestation de services (cf. infra « 5°. c. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS) ».

*Pour aller plus loin* : articles L. 204-1 et R. 254-9 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) titulaire d'un certificat délivré par un État membre, légalement établi et exerçant l'activité de distributeur de produits phytopharmaceutiques, peut exercer à titre permanent la même activité en France.

Dès lors que le ressortissant est titulaire d'un certificat individuel d'exercice délivré par un État membre, il est réputé être titulaire du certificat exigé pour exercer en France.

En outre, le professionnel est soumis au respect des mêmes conditions d'installation que le ressortissant français (cf. infra « 5°. Démarches et formalités de reconnaissance de qualification »).

*Pour aller plus loin* : II de l'article R. 254-9 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles professionnelles

### Obligations en cas de vente de produits phytopharmaceutiques à des utilisateurs non professionnels

Seuls les produits comportant la mention « emploi autorisé dans les jardins » peuvent être mis en vente, vendus ou distribués à des utilisateurs non professionnels, excepté si ces personnes agissent pour le compte de professionnels. Le cas échéant, ces utilisateurs doivent justifier être en possession :

- pour les propriétaires de biens fonciers à usage non agricole et non forestier, dont la gestion est réalisée par un tiers :
  - d'un contrat ou d'une attestation de délégation à un tiers de l'ensemble des décisions relatives à la protection des végétaux,
  - d’un justificatif de la qualité d’utilisateur professionnel de ce tiers ;
- pour les exploitants agricoles ou forestiers, qui ne participent ni à la définition de la stratégie de traitement ni au choix des produits à utiliser et qui font appel, pour l'ensemble de leurs travaux d'application de produits phytopharmaceutiques, à un organisme agréé pour l'application en prestation de services :
  - de la preuve écrite de la réalisation de la prestation par une personne morale agréée pour  les activité de conseil indépendantou de distribution de produits phytopharmaceutiques à des utilisateurs professionnels,
  - d’un document contractuel avec un organisme agréé pour l'application en prestation de services accompagné du nom d'une personne certifiée.

En outre, en cas de vente de produits ne portant pas cette mention, le professionnel est tenu de s'assurer au préalable de la qualité de professionnel du futur acheteur. Pour cela, il doit vérifier que ce dernier dispose d'un certificat individuel d'exercice dans l'une des catégories suivantes :

- « Décideur en travaux et services » ;
- « Décideur en travaux agricoles » ;
- « Applicateur en collectivité territoriale ».

Lors de chaque vente, le professionnel est tenu :

- de séparer les produits portant la mention « emploi autorisé dans les jardins » et ceux ne la comportant pas, grâce à un procédé signalétique explicite ;
- de fournir l'ensemble des informations relatives à l'utilisation des produits et des risques qu'ils présentent pour la santé et l'environnement, ainsi que les consignes de sécurité en vue de prévenir les risques.

*Pour aller plus loin* : articles R. 254-20 à -22 du Code rural et de la pêche maritime ; arrêté du 6 janvier 2016 relatif aux justificatifs requis pour l'achat de produits phytopharmaceutiques de la gamme d'usages « professionnels ».

### Obligation d'information envers les clients utilisateurs de produits phytopharmaceutiques

Le professionnel qui met en vente ou distribue des produits phytopharmaceutiques doit au moins une fois par an effectuer auprès de ses clients un conseil individualisé par écrit mentionnant :

- la substance active et la spécialité recommandée ;
- la cible visée par ce produit ainsi que les parcelles et la superficie concernées par son utilisation ;
- la dose à utiliser ainsi que les conditions de sa mise en œuvre ;
- le cas échéant, les méthodes alternatives (méthodes non chimiques et utilisation des produits de biocontrôle).

De plus, lorsque le professionnel effectue une vente de ces produits, il doit fournir l'ensemble des informations relatives à l'utilisation des produits phytopharmaceutiques, les risques éventuels pour la santé et l'environnement et les consignes de sécurité.

En outre, en cas de cession des produits à des utilisateurs non professionnels, le professionnel doit fournir l'ensemble des informations relatives aux risques potentiels pour la santé humaine et l'environnement (dangers, exposition, conditions de stockage et consignes relatives à la manipulation, l'application et l'élimination sans danger de ces produits et les solutions de substitution présentant peu de risques).

*Pour aller plus loin* : article L. 254-7 du Code rural et de la pêche maritime.

### Tenue d'un registre de ventes

Le professionnel doit tenir à jour un registre des ventes effectuées mentionnant les informations relatives au produit (nom commercial, numéro d'autorisation, quantité vendue, montant de la redevance).

En outre, pour la distribution  de produits phytopharmaceutiques à des utilisateurs professionnels, le distributeur doit également faire figurer sur le registre :

- le numéro et la date de facturation, s’il y a lieu ;
- le code postal de l'utilisateur final du produit ;
- les documents justifiants la qualité de professionnel de l'utilisateur ;
- l'ensemble des informations relatives à la semence traitée (espèce végétale concernée, numéro et date de facturation).

Chaque année, le professionnel doit dresser un bilan de l'année civile précédente comportant l'ensemble des informations citées ci-dessus lors de la vente de chaque produit phytopharmaceutique.

De plus, il doit, chaque année avant le 1er avril, transmettre ces informations à l'agence de l'eau et l'office des eaux par voie électronique, à l’appui de la redevance pour pollutions diffuses.

*Pour aller plus loin* : articles R. 254-23 et suivants du Code rural et de la pêche maritime.

## 4°. Assurances et sanctions

### Assurance

Le professionnel est tenu, pour obtenir son agrément, de souscrire une assurance couvrant sa responsabilité civile professionnelle.

*Pour aller plus loin* : article L. 254-2 du Code rural et de la pêche maritime.

### Sanctions pénales

Le professionnel qui exerce l'activité de distributeur de produits phytopharmaceutiques sans être titulaire d'un agrément encourt une peine de six mois d'emprisonnement et de 15 000 euros d'amende.

En outre, le professionnel encourt une amende de 1 500 euros en cas de vente de produits phytopharmaceutiques à des non-professionnels en violation des dispositions de l'article L. 254-20 du Code rural et de la pêche maritime.

*Pour aller plus loin* : articles L. 254-12 et R. 254-30 du Code rural et de la pêche maritime.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir le certificat individuel d'exercice

#### Autorité compétente

Le professionnel doit adresser sa demande à la direction régionale de l'alimentation, de l'agriculture et de la forêt (DRAAF) du lieu de sa résidence, ou, le cas échéant, du lieu du siège social de l'organisme où ont été réalisées ses formations.

#### Pièces justificatives

Le demandeur est tenu de créer un compte en ligne sur le site [service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) pour accéder au service de la demande de certificat.

Sa demande doit comprendre, selon le cas :

- un justificatif attestant du suivi de la formation et le cas échéant de la réussite au test ;
- la copie de son diplôme ou titre de formation.

#### Délai et procédure

Le certificat lui est délivré dans un délai de deux mois suivant le dépôt de sa demande. En l'absence de délivrance du certificat après ce délai, et sauf notification de refus, les pièces justificatives précitées valent certificat individuel pour une durée maximale de deux mois.

Ce certificat est valable pendant cinq ans et renouvelable dans les mêmes conditions.

*Pour aller plus loin* : articles R. 254-11 et R. 254-12 du Code rural et de la pêche maritime.

### b. Demande en vue d'obtenir l'agrément pour l'exercice de l'activité de distributeur

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet de la région au sein de laquelle se trouve le siège social de l'entreprise. Si le professionnel a son siège social dans un autre État membre que la France, il devra adresser sa demande au préfet de la région dans laquelle il effectuera sa première prestation de services.

#### Pièces justificatives

Sa demande doit comporter le [formulaire de demande d'agrément](https://www.service-public.fr/professionnels-entreprises/vosdroits/R22206) complété et signé ainsi que les documents suivants :

- un certificat de police d'assurance de responsabilité civile professionnelle ;
- un certificat délivré par un organisme tiers justifiant qu'il exerce cette activité dans le respect des conditions garantissant la protection de la santé publique et de l'environnement, et de la bonne information des utilisateurs ;
- la copie d'un contrat conclu avec un organisme tiers prévoyant le suivi nécessaire au maintien de la certification.

**À noter**

Les micro-distributeurs doivent quant à eux transmettre à la DRAAF les documents suivants :

- une attestation d'assurance de responsabilité civile professionnelle ;
- un justificatif de la détention du Certiphyto par l'ensemble du personnel exerçant des fonctions d’encadrement, de vente ou de conseil ;
- un document justifiant qu'il est soumis au régime fiscal des micro-entreprises.

#### Délai et procédure

Le professionnel qui débute son activité doit solliciter un agrément provisoire qui lui sera délivré pour une durée de six mois non renouvelable. Suite à cela il pourra effectuer sa demande d'agrément définitif.

Le silence gardé par le préfet de région au delà d'un délai de deux mois vaut décision de rejet.

*Pour aller plus loin* : articles R. 254-15-1 à R. 254-17 du Code rural et de la pêche maritime ; arrêté du 27 avril 2017 modifiant l'arrêté du 25 novembre 2011 relatif au référentiel de certification prévu à l'article R. 254-3 du Code rural et de la pêche maritime pour l'activité « distribution de produits phytopharmaceutiques à des utilisateurs non professionnels ».

**À noter**

Même en cas de demande portant sur plusieurs activités, l'agrément obtenu sera unique.

### c. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande par tout moyen à la DRAAF du lieu d'exercice de sa première prestation de services.

#### Pièces justificatives

Sa demande doit comporter le certificat individuel d'exercice délivré par l’État membre et, le cas échéant, assorti de sa traduction en français par un traducteur agréé.

**À noter**

Cette déclaration doit être renouvelée chaque année et en cas de changement de situation professionnelle.

*Pour aller plus loin* : III de l'article R. 254-9 du Code rural et de la pêche maritime.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).