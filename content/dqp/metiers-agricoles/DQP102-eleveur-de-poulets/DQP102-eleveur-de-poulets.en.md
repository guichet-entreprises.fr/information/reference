﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP102" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Agricultural occupations" -->
<!-- var(title)="Chicken breeder" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="agricultural-occupations" -->
<!-- var(title-short)="chicken-breeder" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/agricultural-occupations/chicken-breeder.html" -->
<!-- var(last-update)="2020-04-15 17:20:48" -->
<!-- var(url-name)="chicken-breeder" -->
<!-- var(translation)="Auto" -->


Chicken breeder
================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:48<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1. Definition of activity
-------------------------

The breeder broiler is a responsible professional, within a farm to care for broilers (Gallus gallus of) for the production and sale of their meat.

**To note**

This sheet is on meat production only, thus laying hen breeding operations will not be addressed. In addition, only some farms are affected by the minimum standards of protection of chickens.

Are not affected by these provisions:

- farms with less than 500 chickens;
- farms comprising only breeding chickens;
- hatcheries;
- farms organic chickens, chickens reared indoors extensive system or chickens raised outdoors, in the open or released.

However, these requirements are subject to the land consisting of both breeding flocks and breeding herds.

*For further*: Decree of 28 June 2010 laying down minimum standards for the protection of chickens kept for meat production.

2 °. Professional qualifications
--------------------------------

### at. national requirements

#### national legislation

To operate as a chicken farmer, the professional must:

- be professionally qualified;
- maintain a breeding register;
- declare the density of its operations, namely the total weight of chickens therein (see below "5 °. d. Declaration of the density of farming").

In addition, certain specific obligations to exploitation must be respected as well:

- the activities of the operator must be a statement (see below "5 ° C Declaration of operating activity..");
- comply with minimum standards for the protection of broiler chickens.

*For further*: Decree of 28 June 2010 cited above; Article 4 of the Decree of 26 February 2008 on the fight against Salmonella in breeding flocks of Gallus gallus in the flesh die and fixing the avian salmonellosis reporting arrangements referred to in Article 223 D. -1 rural Code and maritime fishing in these same herds.

#### Training

**individual professional certificate broiler breeder**

To be recognized as showing a level of knowledge relating to animal welfare acquired during training, the applicant must hold an individual certificate of broiler breeder.

For this, it needs training for a minimum of seven hours bearing at least on the national and Community legislation on the protection of chickens and in particular the following points:

a) Annexes I and II to Directive 2007/43 / EC mentioned above and of this Order;

b) animal physiology, including their needs for food and water, their behavior and the concept of stress;

c) the practical aspects of the careful handling of chickens, their capture, loading and transport;

d) emergency care for chickens, procedures killing and emergency slaughter;

e) preventive biosecurity measures.

This training is delivered by an approved training organization by the Minister for Agriculture, the list is attached in Annex VII of the Order of 28 June 2010.

On completion of his training, the body shall issue a certificate of training in accordance with the model set out in Annex VI of that order.

The professional must then make an application for the issuance of professional certificates (see below "5 °. A. Application to obtain individual professional certificate chicken farmer flesh").

*For further*: Article 4 of the decree of 28 June 2010 cited above.

#### Good to know: training delivery

Any breeder installed for over a year before 30 June 2010 will be provided for monitoring the training if he makes a request to the prefect of his home (see below "5 °. B. Application for the issuance of the individual certificate of exemption ").

To do this, he proves that he practiced for a minimum of one year poultry meat. This proof can be any written document mentioning the name of the breeder and the breeding in which he is or was employed.

*For further*5 of section 4 of the Decree of 28 June 2010.

#### Requirement to maintain a breeding register

Professional keeper of animals whose flesh or products are intended for consumption must declare its activity (see below "5 °. C. Statement of Operating Activity") and shall maintain a register for each poultry mentioning:

- the number of chickens introduced;
- the usable area;
- the hybrid or breed of the chicken, if known;
- at each control, the number of chickens found dead and the causes of mortality and the number of chickens killed;
- the number of chickens remaining in the flock following the removal of volatile for sale or slaughter.

These records must be kept for at least three years by the farmer or operator and shall be made available to the control services during inspections or when they so request.

*For further*: Article L. 234-1 of the Code rural and sea fishing; Annex I to the Decree of 28 June 2010 cited above.

#### Costs associated with the qualification

The cost of training leading to exercise individual certificate varies duration and training organizations. It is advisable to approach the institutions concerned for more information.

### b. EU nationals: for a temporary and occasional exercise (Free Global Services (LPS)) or a permanent exercise (Freedom of establishment)

A national of an EU Member State or EEA exercising the activity of chicken farmer in one of these states can use the professional title in France, temporary or casual basis. It shall, before his first performance, a statement addressed to the prefect of the region in which he wishes to exercise (see below "5 °. A. Make a prior declaration of activity for EU citizens exercising an activity temporary and occasional (LPS) ").

Where neither the business nor the training leading to this activity are not regulated in the State where it is legally established, the professional will have exercised in one or more Member States for at least one year during the ten years preceding the delivery.

When the examination of professional qualifications reveals substantial differences in terms of qualifications required for access to the profession and its exercise in France, the person may be subject to an aptitude test within one month the receipt of the declaration by the prefect. The national wishing to settle in France is subject to the same provisions as the French national (see below "5 °. Approaches and qualification recognition formality").

3 °. Terms of repute, professional ethics, ethics
-------------------------------------------------

### farms Provisions

chicken breeders are obliged to respect all the provisions to breeding. As such, they must ensure that these animals:

- are not deprived of food or watering needed to meet the specific physiological needs of their species and to their degree of development, adaptation and domestication;
- are not left without care in case of illness or injury;
- be placed in a suitable environment and are not subjected to any mistreatment.

Also, is forbidden does for a breeder to administer certain substances chickens such as anabolic substances, anticatabolisantes or beta-agonists or to sell on the market chickens have been subjected to drug tests.

*For further*: Articles R. 214-17 and L. 234-2 of the Code rural and maritime fishing.

### Provisions common to all chicken farms

The breeder must ensure that his houses comply with all applicable requirements for all operations including:

- power systems animals;
- ventilation and heating systems;
- illumination of premises and the cleaning of equipment used;
- surgical procedures on chickens.

These provisions are set out in Annex I of the decree of 28 June 2010 cited above.

### professional rules

Since good practice guides are developed by the poultry trade organizations, the trader shall apply.

*For further*: Article 5 of the Decree of 28 June 2010 cited above.

### Information obligation on the food chain

The broiler breeder must send to the manager of the slaughterhouse, all information relating to the food chain (CIA) animal, and at least 24 hours before removal.

This document shall conform to[model](http://mesdemarches.agriculture.gouv.fr/spip.php?action=acceder_document&arg=426&cle=d7aab69f5c9bd2a15e38a6b3cc847bbb9cc92082&file=pdf%2FICA_Gallus_chair.pdf)and is valid for five days from its date of writing. However, its validity can be extended once the next batch of animals within the same band and are intended for the same slaughterhouse.

A copy of this document should be kept in operation.

In case of breach of these obligations, the professional liable to a fine of 1500 euros.

*For further*: Decree of 20 March 2009 on modalities for implementation of the information on the food chain for lots of poultry and lagomorphs for slaughter for human consumption.

4 °. sanctions
--------------

### Responsibility

The producer is fully responsible for the welfare of the animals of its operations.

*For further*: Article 1 of the decree of 28 June 2010 cited above.

### administrative measures

In case of failure to fulfill the provisions of illegal substances or once the products are dangerous to public health, skills Veterinarians can order the implementation of administrative policy measure, including:

- sequestration, identification and marking of farm animals;
- the health control of products before placing them on the market;
- slaughter and destruction of animals and their products;
- destruction of substances administered and the products concerned;
- the surveillance of operations in the year following the slaughter of animals;
- control of livestock operation and all institutions having been in contact with it.

*For further*: Articles L. 234-3 and L. 234-4 of the Code rural and maritime fishing.

5 °. Approaches and qualification recognition procedures
--------------------------------------------------------

### at. Application to obtain individual professional certificate broiler breeder

#### Competent authority

The professional must send his application to the department prefect of his home in the county department in charge of protecting the population (HADD).

#### Vouchers

The application shall contain:

- Reporting Form[Cerfa No. 14138* 02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14138.do)completed and signed;
- a copy of the training certificate.

#### Period and procedures

After receipt of complete application, the prefect of the department issues the professional certificate to the applicant as well as documentation regarding the regulation on the protection of broiler chickens. The professional is required to read and keep this documentation in his breeding registry.

*For further*3 and 4 of Article 4 of the decree of 28 June 2010 cited above.

### b. Application for the issuance of the individual certificate recognized equivalent

#### Competent authority

The professional must apply to the prefect of the department, the department HADD his home.

#### Vouchers

The application must include:

- Reporting Form[Cerfa No. 14144* 02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14144.do)completed and signed;
- a document indicating his identity and the name of breeding in which he served, justifying that he exercised this activity for more than a year before 30 June 2010.

*For further*5 of section 4 of the Decree of 28 June 2010 cited above.

### vs. Statement from operating activities

#### Competent authority

Since its operation has more than 250 birds of Gallus gallus, the professional operator, or failing that, the breeder must declare their activity to the HADD of the department where the workshops are located.

#### Vouchers

The application shall contain:

- the form[Cerfa No. 13989* 04](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13989.do)Attraction of an operator of poultry flocks of Gallus gallus or meleagris completed and signed gallapavo;
- Reporting Form[Cerfa No. 13990* 05](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13990.do)of establishment or release of a flock.

*For further*: Article 4 of the Decree of 26 February 2008 cited above.

### d. Declaration of the livestock density

#### Competent authority

The professional must apply to the HADD later than fifteen days before the installation of his flock.

#### Vouchers

The application must include the declaration form[Cerfa No. 14148* 02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14148.do)completed and signed.

Once the HADD required, the professional may also be asked to provide details of the operation of facilities meeting the requirements laid down in Annex I to the Order of 28 June 2010 (see above "3. provisions common to all chicken farms ").

**To note**

If the trader wishes to declare a density greater than 33 kg / m² of body weight, it must also send the same form to the HADD.

#### Following the procedure

The professional must maintain and make available documentation on facilities of its operations in accordance with the Order of 28 June 2010.

### e. Remedies

#### French Support Center

The NARIC Center is the French information center on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided by the State Administration of each EU Member State or party to the EEA Agreement. Its goal is to find a solution to a dispute between an EU citizen to the administration of another of those States. SOLVIT including the recognition of professional qualifications.

##### terms

The person may not use SOLVIT if it establishes:

- that the public administration of an EU state has not respected the rights that EU law gives it as a citizen or company from another EU state;
- he has not already initiated legal action (the administrative appeal is not considered as such).

##### Procedure

The national must fill out an online complaint form. Once transmitted sound file, contact the SOLVIT within a week to ask, if necessary, additional information and to verify that the problem is within its jurisdiction.

##### Vouchers

To enter SOLVIT, the national shall communicate:

- full contact details;
- Detailed description of the problem;
- all evidence of the folder (for example, correspondence received and the decisions of the administrative authority concerned).

##### Time limit

SOLVIT is committed to finding a solution within ten weeks from the date of the management of the file by the SOLVIT center of the country where the problem occurred.

##### Cost

Free.

##### Following the procedure

At the end of ten weeks, the SOLVIT has a solution:

- If this solution resolves the dispute on the application of European law, the solution is accepted and the file is closed;
- if there is no solution, the case is closed as unresolved and forwarded to the European Commission.

##### Additional Information

SOLVIT France: General Secretariat for European Affairs, 68 Rue de Bellechasse, 75700 Paris ([official site](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

