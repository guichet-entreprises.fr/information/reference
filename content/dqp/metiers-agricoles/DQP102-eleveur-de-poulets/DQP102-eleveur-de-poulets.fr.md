﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP102" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers agricoles" -->
<!-- var(title)="Éleveur de poulets" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-agricoles" -->
<!-- var(title-short)="eleveur-de-poulets" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-agricoles/eleveur-de-poulets.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="eleveur-de-poulets" -->
<!-- var(translation)="None" -->

# Éleveur de poulets

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L'éleveur de poulets de chair est un professionnel chargé, au sein d'une exploitation agricole, de s'occuper des poulets de chair (de l'espèce Gallus gallus) en vue de la production et de la vente de leur viande.

**À noter**

Cette fiche est relative à la production de viande uniquement, ainsi, les établissements d'élevage de poules pondeuses ne seront pas abordés. En outre, seules certaines exploitations sont concernées par les normes minimales de protection des poulets.

Ne sont pas concernées par ces dispositions :

- les exploitations de moins de 500 poulets ;
- les exploitations comprenant uniquement des poulets reproducteurs ;
- les couvoirs ;
- les exploitations de poulets biologiques, de poulets élevés en intérieur en système extensif ou encore de poulets élevés à l'extérieur, en plein air ou en liberté.

En revanche, sont soumises à ces exigences les exploitations constituées à la fois de troupeaux d'élevage et de troupeaux reproducteurs.

*Pour aller plus loin* : arrêté du 28 juin 2010 établissant les normes minimales relatives à la protection des poulets destinés à la production de viande.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité d'éleveur de poulets, le professionnel doit :

- être qualifié professionnellement ;
- tenir à jour un registre d'élevage ;
- déclarer la densité de son exploitation, à savoir le poids total de poulets s'y trouvant (cf. infra « 5°. d. Déclaration de la densité de l'élevage »).

En outre, certaines obligations propres à l'exploitation doivent être respectées, ainsi :

- l'activité de l'exploitant doit faire l'objet d'une déclaration (cf. infra « 5°. c. Déclaration de l'activité d'exploitation ») ;
- respecter les normes minimales relatives à la protection des poulets de chair.

*Pour aller plus loin* : arrêté du 28 juin 2010 précité ; article 4 de l'arrêté du 26 février 2008 relatif à la lutte contre les infections à Salmonella dans les troupeaux de reproduction de l'espèce Gallus gallus en filière chair et fixant les modalités de déclaration des salmonelloses aviaires visées à l'article D. 223-1 du Code rural et de la pêche maritime, dans ces mêmes troupeaux.

#### Formation

##### Certificat professionnel individuel d'éleveur de poulet de chair

Pour être reconnu comme justifiant d’un niveau de connaissance relatif au bien-être animal acquis lors d’une formation, l'intéressé doit être titulaire d'un certificat individuel d'éleveur de poulets de chair.

Pour cela, il doit suivre une formation d'une durée minimale de 7 heures portant au moins sur la législation nationale et communautaire relative à la protection des poulets, et en particulier sur les points suivants :

a) les annexes I et II de la directive n° 2007/43/CE susvisée et du présent arrêté ;
b) la physiologie des animaux, notamment leurs besoins en nourriture et en eau, leur comportement et le concept de stress ;
c) les aspects pratiques de la manipulation attentive des poulets, de leur capture, leur chargement et leur transport ;
d) les soins d'urgence à donner aux poulets, les procédures de mise à mort et d'abattage d'urgence ;
e) les mesures de biosécurité préventive.

Cette formation est délivrée par un organisme de formation agréé par le ministre chargé de l'agriculture dont la liste est fixée à l'annexe VII de l'arrêté du 28 juin 2010.

À l'issue de sa formation, l'organisme lui délivre une attestation de formation conformément au modèle fixé à l'annexe VI dudit arrêté.

Le professionnel doit ensuite effectuer une demande en vue d'obtenir la délivrance de son certificat professionnel (cf. infra « 5°. a. Demande en vue d'obtenir un certificat professionnel individuel d'éleveur de poulet de chair »).

*Pour aller plus loin* : article 4 de l'arrêté du 28 juin 2010 précité.

#### Bon à savoir : dispense de formation

Tout éleveur installé depuis plus d'un an avant le 30 juin 2010 pourra être dispensé du suivi de la formation s'il en fait la demande auprès du préfet du département de son domicile (cf. infra « 5°. b. Demande en vue de la délivrance du certificat individuel par dérogation »).

Pour ce faire, il fournit la preuve qu'il a pratiqué pendant une durée minimale d'un an l'élevage de volailles de chair. Cette preuve peut être tout document écrit mentionnant le nom de l'éleveur et celui de l'élevage dans lequel il exerce ou a exercé.

*Pour aller plus loin* : 5° de l'article 4 de l'arrêté du 28 juin 2010.

#### Obligation de tenir à jour un registre d'élevage

Le professionnel détenteur d'animaux dont la chair ou les produits sont destinés à la consommation est tenu de déclarer son activité (cf. infra « 5°. c. Déclaration de l'activité d'exploitation ») et doit tenir à jour un registre pour chacun des poulaillers mentionnant :

- le nombre de poulets introduits ;
- la surface utilisable ;
- l'hybride ou la race du poulet, s'il les connaît ;
- lors de chaque contrôle, le nombre de poulets retrouvés morts et les causes de mortalité ainsi que le nombre de poulets mis à mort ;
- le nombre de poulets restant dans le troupeau après l'enlèvement des volatiles destinés à la vente ou à l'abattage.

Ce registre doit être conservé pendant au mois trois ans par l'éleveur ou l'exploitant et doit être mis à la disposition des services de contrôle lors des inspections ou lorsque ceux-ci le demandent.

*Pour aller plus loin* : article L. 234-1 du Code rural et de la pêche maritime ; annexe I de l'arrêté du 28 juin 2010 précité.

#### Coûts associés à la qualification

Le coût de la formation menant au certificat individuel d'exercice varie selon la durée et les organismes de formation. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestations de Services (LPS)) ou d'un exercice permanent (Libre Établissement)

Le ressortissant d’un État membre de l’UE ou de l’EEE exerçant l’activité d’éleveur de poulets dans l’un de ces États peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit effectuer, préalablement à sa première prestation, une déclaration adressée au préfet de la région dans laquelle il souhaite exercer (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la déclaration par le préfet. Le ressortissant souhaitant s’établir en France est soumis aux mêmes dispositions que le ressortissant français (cf. infra « 5°. Démarches et formalité de reconnaissance de qualification »).

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Dispositions relatives aux élevages

Les éleveurs de poulets sont tenus au respect de l'ensemble des dispositions propres à l'élevage. À ce titre, ils doivent veiller à ce que ces animaux :

- ne soient pas privés de la nourriture ou de l'abreuvement nécessaires à la satisfaction des besoins physiologiques propres à leur espèce et à leur degré de développement, d'adaptation ou de domestication ;
- ne soient pas laissés sans soins en cas de maladie ou de blessure ;
- soient placés dans un environnement adapté et ne fassent l'objet d'aucun mauvais traitement.

En outre, est notamment interdit le fait pour un éleveur d'administrer certaines substances aux poulets telles que les substances anabolisantes, anticatabolisantes ou bêta-agonistes ou de mettre en vente sur le marché des poulets ayant été soumis à des essais médicamenteux.

*Pour aller plus loin* : articles R. 214-17 et L. 234-2 du Code rural et de la pêche maritime.

### Dispositions communes à tous les élevages de poulets

L'éleveur doit s'assurer que ses poulaillers respectent l'ensemble des exigences applicables à toutes les exploitations et notamment :

- les systèmes d'alimentation des animaux ;
- les systèmes de ventilation et de chauffage ;
- l'éclairage des locaux et le nettoyage du matériel utilisé ;
- les interventions chirurgicales pratiquées sur les poulets.

Ces dispositions sont fixées à l'annexe I de l'arrêté du 28 juin 2010 précité.

### Règles professionnelles

Dès lors que des guides de bonnes pratiques sont élaborés par les organisations professionnelles avicoles, le professionnel est tenu de les appliquer.

*Pour aller plus loin* : article 5 de l'arrêté du 28 juin 2010 précité.

### Obligation d'information sur la chaîne alimentaire

L'éleveur de poulets de chair doit transmettre au responsable de l'abattoir, l'ensemble des informations relatives à la chaîne alimentaire (ICA) des animaux, et ce, au moins 24 heures avant leur enlèvement.

Ce document doit être conforme à un [modèle](http://mesdemarches.agriculture.gouv.fr/spip.php?action=acceder_document&arg=426&cle=d7aab69f5c9bd2a15e38a6b3cc847bbb9cc92082&file=pdf%2FICA_Gallus_chair.pdf) et reste valable pendant cinq jours à compter de sa date de rédaction. Toutefois, sa durée de validité peut être prolongée dès lors que les animaux du lot suivant relèvent de la même bande et sont destinés au même abattoir.

Une copie de ce document doit être conservée au sein de l'exploitation.

En cas de manquement à ces obligations, le professionnel encourt une amende de 1 500 euros.

*Pour aller plus loin* : arrêté du 20 mars 2009 relatif aux modalités de mise en œuvre de l'information sur la chaîne alimentaire pour les lots de volailles et de lagomorphes destinés à l'abattage en vue de la consommation humaine.

## 4°. Sanctions

### Responsabilité

L'éleveur est pleinement responsable du bien-être des animaux de son exploitation.

*Pour aller plus loin* : article 1er de l'arrêté du 28 juin 2010 précité.

### Mesures de police administrative

En cas de manquement constaté aux dispositions en matière de substances illégales ou dès lors que les produits présentent un danger pour la santé publique, les vétérinaires habilités peuvent ordonner la mise en place de mesure de police administrative, et notamment :

- la séquestration, le recensement ou le marquage des animaux de l'exploitation ;
- le contrôle sanitaire des produits avant leur mise sur le marché ;
- l'abattage et la destruction des animaux ou de leurs produits ;
- la destruction des substances administrées et des produits concernés ;
- la mise sous surveillance de l'exploitation durant l'année suivant l'abattage des animaux ;
- le contrôle de l'élevage de l'exploitation et de l'ensemble des établissements ayant été en relation avec celle-ci.

*Pour aller plus loin* : articles L. 234-3 et L. 234-4 du Code rural et de la pêche maritime.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir un certificat professionnel individuel d'éleveur de poulet de chair

#### Autorité compétente

Le professionnel doit transmettre sa demande auprès du préfet de département de son domicile, à la direction départementale en charge de la protection des populations (DDPP).

#### Pièces justificatives

Sa demande doit contenir :

- le formulaire de déclaration [Cerfa n° 14138](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33060) complété et signé ;
- une copie de l'attestation de formation.

#### Délai et procédure

Après réception complète de la demande, le préfet du département délivre le certificat professionnel au demandeur ainsi qu'une documentation portant sur la réglementation relative à la protection des poulets de chair. Le professionnel est tenu de lire et de conserver cette documentation au sein de son registre d'élevage.

*Pour aller plus loin* : 3° et 4° de l'article 4 de l'arrêté du 28 juin 2010 précité.

### b. Demande en vue de la délivrance du certificat individuel admis en équivalence

#### Autorité compétente

Le professionnel doit adresser sa demande auprès de son préfet de département, à la DDPP du département de son domicile.

#### Pièces justificatives

Sa demande doit comporter :

- le formulaire de déclaration [Cerfa n° 14144](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33061) complété et signé ;
- un document mentionnant son identité et le nom de l'élevage au sein duquel il a exercé, justifiant qu'il a exercé cette activité depuis plus d'un an avant le 30 juin 2010.

*Pour aller plus loin* : 5° de l'article 4 de l'arrêté du 28 juin 2010 précité.

### c. Déclaration de l'activité d'exploitation

#### Autorité compétente

Dès lors que son exploitation compte plus de 250 volailles de l'espèce Gallus gallus, le professionnel exploitant, ou à défaut, l'éleveur, est tenu de déclarer son activité auprès de la DDPP du département où se trouvent les ateliers.

#### Pièces justificatives

Sa demande doit contenir :

- le formulaire [Cerfa n° 13989](https://www.service-public.fr/professionnels-entreprises/vosdroits/R32892) d'activité d'un exploitant de troupeaux de volailles de l'espèce Gallus gallus ou meleagris gallapavo complété et signé ;
- le formulaire de déclaration [Cerfa n° 13990](https://www.service-public.fr/professionnels-entreprises/vosdroits/R32893) de mise en place ou de sortie d'un troupeau de volailles.

*Pour aller plus loin* : article 4 de l'arrêté du 26 février 2008 précité.

### d. Déclaration de la densité de l'élevage

#### Autorité compétente

Le professionnel doit adresser sa demande auprès de la DDPP au plus tard quinze jours avant l'installation de son troupeau.

#### Pièces justificatives

Sa demande doit comporter le formulaire de déclaration [Cerfa n° 14148](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33062) complété et signé.

Dès lors que la DDPP l'exige, le professionnel peut également être amené à fournir le détail des installations de l'exploitation répondant aux exigences fixées à l'annexe I de l'arrêté du 28 juin 2010 (cf. supra « 3°. Dispositions communes à tous les élevages de poulets »).

**À noter**

Lorsque le professionnel souhaite déclarer une densité supérieure à 33 kg/m² de poids vif, il doit également adresser ce même formulaire à la DDPP.

#### Issue de la procédure

Le professionnel doit conserver et rendre accessible la documentation relative aux installations de son exploitation conformément aux dispositions de l'arrêté du 28 juin 2010.

### e. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).