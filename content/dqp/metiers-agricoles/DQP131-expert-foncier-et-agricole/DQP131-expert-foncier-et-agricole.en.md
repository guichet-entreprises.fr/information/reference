﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP131" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Agricultural occupations" -->
<!-- var(title)="Agricultural and real estate appraiser" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="agricultural-occupations" -->
<!-- var(title-short)="agricultural-and-real-estate-appraiser" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/agricultural-occupations/agricultural-and-real-estate-appraiser.html" -->
<!-- var(last-update)="2020-04-15 17:20:48" -->
<!-- var(url-name)="agricultural-and-real-estate-appraiser" -->
<!-- var(translation)="Auto" -->


Agricultural and real estate appraiser
============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:48<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The land and agricultural expert is a professional who carries out missions of expertise in land and agriculture dealing with the property of others, furniture and buildings, as well as the property and property rights related to these properties.

It is particularly active in the following areas:

- estimating buildings and property and real estate rights, both urban and rural;
- estimates of rental values and urban and rural property;
- drafting acts and legal advice;
- land use and sustainable environmental management (advice, support);
- agriculture-animal or plant production;
- Estimating the farm and its components;
- Estimating the damage
- assessment of ecological harm and the implementation of compensation measures (ERC).

*To go further* Article L. 171-1 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The professional must be placed on the national list of land and agricultural experts drawn up by the committee of the National Council of Agricultural and Forestry Land Expertise (Cnefaf).

Registration is open to the individual who meets the following conditions:

- justify a professional practice in the fields of land and agricultural expertise, either personally or under the responsibility of a trainee:- at least seven years,
  - at least three years if the professional holds a title or diploma sanctioning at least four years of post-secondary education in the agricultural, agronomic, environmental, forestry, legal or economic disciplines or in the areas of land use, landscapes or urban planning;
- not having been convicted for facts contrary to honour, probity or good morals in the past five years;
- not disciplinary or administrative sanction for dismissal, dismissal, dismissal, withdrawal of accreditation or authorization;
- have not been hit with personal bankruptcy.

*To go further* Articles L. 171-1 and R. 171-10 of the Rural Code and Marine Fisheries.

#### Training

Several courses offering titles and diplomas such as engineering degree or specialized master's degree, allow to follow a course in order to become a land and agricultural expert.

For example, the individual may be trained to obtain (non-exhaustive list):

- a degree in agricultural engineering or agricultural engineer;
- a master's degree in "land expertise" ([IHEDREA school](http://www.ihedrea.org/)).

#### Costs associated with qualification

Training leading to the profession of land and agricultural expert is paid for and the cost varies depending on the institution chosen. For more information, it is advisable to check with the institutions concerned.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The national of a European Union (EU) or European Economic Area (EEA) state may carry out expert missions in France, either on a temporary or casual basis, subject to:

- to be legally established in one of these states to practise as a land and agricultural expert;
- where neither the profession nor training is regulated in that state to have practised as a land and agricultural expert in that state for at least one year or part-time for an equivalent period of time in the last ten years;
- to be insured against the pecuniary consequences of his professional civil liability.

In order to do so, the national will have to send a declaration to the Cnefaf prior to his first performance (see infra "5°. a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)").

*To go further* Article L. 171-2 of the Rural Code and Marine Fisheries.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of a Member State of the European Union or another State party to the agreement on the European Economic Area who wishes to settle in France to practise as a land and agricultural expert will have to apply for inclusion on the list national land and agricultural experts to the Cnefaf committee (see infra "5°. b. Request inclusion on the list of land and agricultural experts for the EU or EEA national for a permanent exercise (LE)).

The Cnefaf committee will make a comparison between, on the one hand, the training required in France to be included on the list and, on the other hand, that received by the applicant, as well as the knowledge, skills and skills he acquired during his work experience or lifelong learning that has been validated by a competent body.

When this examination shows a substantial difference in training in terms of the qualifications required for access to the profession and its practice in France, than the knowledge acquired by the applicant during his professional experience are not likely to fill, in whole or in part, the committee will subject the applicant to a compensation measure (adjustment course or aptitude test).

*To go further* Articles L. 171-3, R. 171-12-1, R. 2045 and R. 171-12-2 of the Rural Code and Marine Fisheries.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Professional rules and duties

The land and agricultural expert is bound by the rules and duties of his profession during the exercise of his activity.

As such, the land and agricultural expert is committed to:

- Respect the independence necessary to carry out one's profession;
- make an impartial statement;
- Respect professional secrecy
- refrain from any unfair practice towards his colleagues.

*To go further* Articles L. 171-1, R. 172-2 and following of the Rural Code and Marine Fisheries.

### b. Incompatibilities

The profession of land and agricultural expert is incompatible:

- with the offices of public and ministerial officer;
- with any function that could impair its independence, in particular that of acquiring personal or real estate in a customary manner for resale.

*To go further* Article L. 171-1 of the Rural Code and Marine Fisheries.

4°. Insurance and penalties
-----------------------------------------------

### Insurance

The liberal land and agricultural expert must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*To go further* Article L. 171-1 paragraph 8 of the Rural Code and Marine Fisheries.

### Disciplinary sanctions

Any breach of professional rules on the part of the land and agricultural expert may be subject to disciplinary proceedings before the Cnefaf committee and a disciplinary sanction which may be:

- Blame
- A warning
- a temporary suspension from three months to three years;
- a disbarment in the event of serious professional misconduct or conviction for acts contrary to honour, probity and good morals.

*To go further* Article L. 171-1 paragraph 7 of the Rural Code and Marine Fisheries.

##### Criminal sanctions

Anyone faces a one-year prison sentence and a fine of 15,000 euros for usurping the title if they use:

- the title of land and agricultural expert without being included on the list established by the Cnefaf committee;
- a denomination that may be confusing with the title of land and agricultural expert.

*To go further* Article L. 171-1 paragraph 9 of the Rural Code and Marine Fisheries; Article 433-17 of the Penal Code.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)

**Competent authority**

The professional must make a prior declaration with the Cnefaf.

**Supporting documents**

Its statement must include the following documents:

- proof of nationality
- A certificate certifying that it is legally established in an EU or EEA state;
- proof that the national has engaged in land and agricultural expertise activities for at least one year or part-time for an equivalent period of time in the last ten years when the activity or training is not regulated in his or her state Original member;
- information on the underwriting of insurance policies detailing the name and address of the insurance company, the references and the period of validity of the contract, the extent and amount of the guarantees.

These documents are attached, as needed, to their translation into the French language.

*To go further* Articles R. 171-17- to R. 171-17-3 of the Rural Code and Marine Fisheries.

### b. Request inclusion on the list of land and agricultural experts for the EU or EEA national for a permanent exercise (LE)

**Competent authority**

The Cnefaf committee is responsible for deciding on the application for inclusion on the list of land and agricultural experts.

**Supporting documents**

The application must contain the following, if any, accompanied by their translation into French:

- All the evidence justifying the applicant's marital status;
- A copy of his titles or diplomas
- proof of work experience
- A resume detailing the professional's previous professional activities (date and place of practice);
- a proof or, failing that, a commitment to underwrrite professional liability insurance;
- a criminal record extract no. 3 less than three months old or any equivalent document issued by the competent authority of the EU State or the EEA of less than three months;
- a statement of honour or any other evidence that the person is meeting the conditions of honour;
- if necessary, a declaration of the activity envisaged in company form.

**Procedure**

Upon receipt of the file, the committee has three months to inform the national of his decision to list it.

However, in the event of substantial differences between the national's professional training and experience and those required in France, the committee may subject the national to the compensation measure of his choice, i.e. to an adjustment course, or to a fitness test.

The silence kept for a period of three months is worth accepting the decision.

The renewal of the application is subject to the production of the certificate of professional liability insurance.

*To go further* Articles R. 171-10 to R. 171-13 of the Rural Code and Marine Fisheries.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

