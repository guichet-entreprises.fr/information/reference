﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP259" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers agricoles" -->
<!-- var(title)="Utilisateur de produits phytopharmaceutiques" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-agricoles" -->
<!-- var(title-short)="utilisateur-de-produits-phytopharmaceutiques" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-agricoles/utilisateur-de-produits-phytopharmaceutiques.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="utilisateur-de-produits-phytopharmaceutiques" -->
<!-- var(translation)="None" -->

# Utilisateur de produits phytopharmaceutiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

L'utilisateur de produits phytopharmaceutiques est un professionnel qui utilise des produits phytopharmaceutiques au cours de son activité professionnelle, et notamment les opérateurs, les techniciens, les employeurs et les indépendants, tant dans le secteur agricole que dans d'autres secteurs.

*Pour aller plus loin* : article R. 254-1 du Code rural et de la pêche maritime

## 2°. Qualifications professionnelles

### a. Exigences nationales

Pour utiliser ou appliquer des produits phytopharmaceutiques, le professionnel doit :

- être titulaire d'un certificat individuel « utilisation à titre professionnel des produits phytopharmaceutiques » ;
- obtenir un agrément en cas d'exercice de cette activité par une personne morale (cf. infra « 5°. b. Demande en vue d'obtenir l'agrément pour l'application des produits phytopharmaceutiques ») ;
- tenir un registre des produits utilisés. Ce registre doit mentionner :
  - la date d'acquisition des produits ou de la prestation de traitement,
  - les quantités de produit utilisées,
  - les numéros de lots.

**À noter**

Ce registre doit être conservé pendant cinq ans.

*Pour aller plus loin* : articles L. 254-1 à L. 254-6 du Code rural et de la pêche maritime ; règlement (CE) n° 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009 concernant la mise sur le marché des produits phytopharmaceutiques.

#### Législation nationale

##### Formation

Ce certificat (appelé Certiphyto) pour l'activité « utilisation à titre professionnel des produits phytopharmaceutiques » se décline en trois catégories correspondant aux secteurs d'activités suivants :

- le décideur en entreprise non soumise à agrément ;
- le décideur en entreprise soumise à agrément ;
- l'opérateur en entreprise.

###### Certificat individuel pour l'activité d'« opérateur »

Le certificat individuel d'exercice est délivré au professionnel qui, soit :

- a suivi une formation comprenant une épreuve écrite d'une heure de vérification des connaissances ;
- a passé avec succès un test d'une heure trente, composé de vingt questions portant sur le programme de formation fixé à l'annexe II de l'arrêté du 29 août 2016 ;
- est titulaire de l'un des diplômes inscrits sur la liste fixée à l'annexe I de l'arrêté du 29 août 2016, obtenu au cours des cinq ans précédant la date de la demande. 

*Pour aller plus loin* : arrêté du 29 août 2016 portant création et fixant les modalités d'obtention du certificat individuel pour l'activité « utilisation à titre professionnel des produits phytopharmaceutiques » dans la catégorie « opérateur ».

###### Certificat individuel pour les activités de « décideur en entreprise non soumise à agrément » et « décideur en entreprise soumise à agrément »

Ces certificats sont délivrés au professionnel qui, soit :

- a suivi une formation comprenant une épreuve écrite d'une heure de vérification des connaissances ;- a passé avec succès  un test d'une heure trente, composé de trente questions portant sur le programme de formation fixé à l'annexe II de l'arrêté du 29 août 2016 ;
- est titulaire de l’un des diplômes inscrits sur la liste fixée à l'annexe I du même arrêté, au cours des cinq années précédant la demande.

*Pour aller plus loin* : arrêté du 29 août 2016 portant création et fixant les modalités d'obtention du certificat individuel pour l'activité « utilisation à titre professionnel des produits phytopharmaceutiques » dans les catégories « décideur en entreprise soumise à agrément » et « décideur en entreprise non soumise à agrément ».

**À noter**

Ces certificats sont valables pendant cinq ans, renouvelables dans les mêmes conditions.

Dès lors qu'il remplit ces conditions, le professionnel doit adresser une demande en vue d'obtenir la délivrance de ce certificat (cf. infra « 5°. a. Demande en vue d'obtenir un certificat individuel d'exercice »).

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) titulaire d'un certificat délivré par un État membre et légalement établi et exerçant une activité liée à l'utilisation de produits phytopharmaceutiques peut exercer à titre temporaire et occasionnel la même activité en France.

Pour cela, il doit, préalablement à sa première prestation de services, effectuer une déclaration auprès du directeur régional de l'alimentation, de l'agriculture et de la forêt du lieu d’exercice de la première prestation de services (cf. infra « 5°. c. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS) ».

*Pour aller plus loin* : article R. 254-9 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'UE ou de l'EEE légalement établi et exerçant une activité liée à l'utilisation de produits phytopharmaceutiques peut exercer à titre permanent la même activité en France.

Dès lors que le ressortissant est titulaire d'un certificat individuel d'exercice délivré par un État membre, il est réputé être titulaire du certificat exigé pour l'exercice en France de cette activité.

*Pour aller plus loin* : II de l'article R. 254-9 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Obligations en matière d'utilisation des produits phytopharmaceutiques

Le professionnel amené à utiliser des produits phytopharmaceutiques est soumis au respect des dispositions en matière d'utilisation de ces produits.

À ce titre, il doit notamment respecter :

- les obligations en matière de délai entre l'application des produits et la récolte ;
- la vitesse maximale du vent au-delà de laquelle il ne peut utiliser ces produits ;
- les dispositions relatives à l'épandage ou la vidange des effluents phytopharmaceutiques.

En outre, tout professionnel utilisant des produits phytopharmaceutiques est tenu de respecter le plan national fixant les objectifs et les mesures prises en vue de réduire les effets de l'utilisation de ces produits sur la santé humaine et l'environnement.

*Pour aller plus loin* : article L. 253-6 du Code rural et de la pêche maritime ; arrêté du 4 mai 2017 relatif à la mise sur le marché et à l'utilisation des produits phytopharmaceutiques et de leurs adjuvants visés à l'article L. 253-1 du Code rural et de la pêche maritime.

## 4°. Assurances et sanctions

### Assurance

Le professionnel est tenu, pour obtenir son agrément, de souscrire une assurance couvrant sa responsabilité civile professionnelle.

*Pour aller plus loin* : article L. 254-2 du Code rural et de la pêche maritime.

### Sanctions

Le professionnel encourt une peine de six mois d'emprisonnement et de 150 000 euros d'amende dès lors qu'il procède à l'utilisation des produits phytopharmaceutiques sans être titulaire de l'agrément ou du certificat individuel d'exercice.

En outre, l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail peut interdire ou encadrer l'utilisation des produits phytopharmaceutiques sur des zones protégées et notamment les zones ouvertes au public.

**À noter**

Les personnes publiques ne peuvent utiliser, pour l'entretien des espaces publics (espaces verts, établissements scolaires), des produits destinés à éliminer les nuisibles, exceptés ceux qui s'avèrent nécessaires pour lutter contre un danger sanitaire grave et ceux ne présentant aucune menace pour l'environnement ou la santé humaine.

*Pour aller plus loin* : article L. 253-7 du Code rural et de la pêche maritime.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir un certificat individuel d'exercice

#### Autorité compétente

La direction régionale de l'alimentation, de l'agriculture et de la forêt (DRAAF) du lieu de résidence du demandeur ou, le cas échéant, du lieu du siège social de l'organisme où ont été réalisées les formations, est compétente pour délivrer le certificat.

#### Pièces justificatives

Le professionnel doit créer un compte en ligne sur le site [service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) afin d'accéder au service de la demande de certificat.

Sa demande doit comprendre, selon le cas :

- un justificatif attestant du suivi de la formation et le cas échéant de la réussite au test ;
- la copie de son diplôme ou titre.

#### Délai et procédure

Le certificat est délivré au professionnel dans un délai de deux mois suivants le dépôt de sa demande. En l'absence de délivrance du certificat après ce délai, et sauf notification de refus, les pièces justificatives précitées valent certificat individuel pour une durée maximale de deux mois.

*Pour aller plus loin* : articles R. 254-11 et R. 254-12 du Code rural et de la pêche maritime.

### b. Demande en vue d'obtenir l'agrément pour l'application des produits phytopharmaceutiques

#### Autorité compétente

Le préfet de région au sein de laquelle se trouve le siège social de l’entreprise est compétent pour délivrer l'agrément.

#### Pièces justificatives

Sa demande doit comporter le [formulaire de demande d'agrément](https://www.service-public.fr/professionnels-entreprises/vosdroits/R22206) complété et signé ainsi que les documents suivants :

- un certificat de police d'assurance de responsabilité civile professionnelle ;
- un certificat délivré par un organisme tiers justifiant qu'il exerce cette activité dans le respect des conditions garantissant la protection de la santé publique et de l'environnement. Cette certification d'entreprise est réalisée par un organisme certificateur à la suite d'un audit sur la base d'un référentiel fixé à l'annexede l'arrêté du 25 novembre 2011 relatif au référentiel de certification ;
- la copie d'un contrat conclu avec un organisme tiers un contrat prévoyant le suivi nécessaire au maintien de la certification.

**À noter**

Les micro-distributeurs doivent transmettre à la DRAAF les documents suivants :

- une attestation d'assurance de responsabilité civile professionnelle ;
- un justificatif de la détention du certificat Certiphyto par l'ensemble du personnel exerçant des fonctions d’encadrement, de vente ou de conseil ;
- un document justifiant qu'il est soumis au régime fiscal des micro-entreprises.

#### Délai et procédure

Le professionnel qui débute son activité doit solliciter un agrément provisoire qui lui sera délivré pour une durée de six mois non renouvelable. Par la suite, il pourra effectuer sa demande d'agrément définitif.

Le silence gardé par le préfet de région au-delà d'un délai de deux mois, vaut décision de rejet.

En outre, le professionnel est tenu, chaque année, de fournir au préfet une copie de l'attestation de la souscription à une police d'assurance.

*Pour aller plus loin* : articles R. 254-15-1 à R. 254-17 du Code rural et de la pêche maritime.

### c. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande par tout moyen à la DRAAF du lieu d'exercice de sa première prestation de services.

#### Pièces justificatives

Sa demande doit comporter le certificat individuel d'exercice délivré par l’État membre et le cas échéant assorti de sa traduction agrée en français.

**À noter**

Cette déclaration doit être renouvelée chaque année et en cas de changement de situation professionnelle.

*Pour aller plus loin* : III de l'article R. 254-9 du Code rural et de la pêche maritime.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68, rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).