﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP259" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Agricultural occupations" -->
<!-- var(title)="User of plant protection products" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="agricultural-occupations" -->
<!-- var(title-short)="user-of-plant-protection-products" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/agricultural-occupations/user-of-plant-protection-products.html" -->
<!-- var(last-update)="2020-04-15 17:20:50" -->
<!-- var(url-name)="user-of-plant-protection-products" -->
<!-- var(translation)="Auto" -->




User of plant protection products
============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:50<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The user of phytopharmaceuticals is a professional who uses phytopharmaceuticals during his or her professional activity, including operators, technicians, employers and the self-employed, both in the agriculture than in other sectors.

*For further information*: Article R. 254-1 of the Rural Code and Marine Fisheries

2°. Professional qualifications
----------------------------------------

### a. National requirements

To use or apply phytopharmaceuticals, the professional must:

- Hold an individual certificate "professional use of phytopharmaceuticals";
- obtain approval if this activity is exercised by a legal entity (see infra "5°. b. Application for approval for the application of phytopharmaceuticals");
- keep a record of the products used. This register should mention:- The date of acquisition of the products or the treatment delivery,
  - The quantities of product used,
  - Lot numbers.

**Please note**

This register must be kept for five years.

*For further information*: Articles L. 254-1 to L. 254-6 of the Rural Code and Marine Fisheries; Regulation (EC) 1107/2009 of the European Parliament and the Council of 21 October 2009 on the marketing of phytopharmaceuticals.

#### National legislation

#### Training

This certificate (called Certiphyto) for the "professional use of phytopharmaceuticals" activity is divided into three categories corresponding to the following sectors of activity:

- The unsubstified decision-maker in the company;
- The approved decision-maker in the company;
- operator in business.

**Individual certificate for "operator" activity**

The individual certificate of practice is issued to the professional who:

- underwent training that included a one-hour written test of knowledge verification;
- successfully passed a one-and-a-half hour test, consisting of twenty questions on the training programme set out in Schedule II of the Order of 29 August 2016;
- holds one of the diplomas on the schedule I list of the August 29, 2016 order, obtained in the five years prior to the application date.

*For further information*: decree of 29 August 2016 establishing and setting out the terms of obtaining the individual certificate for the activity "professional use of phytopharmaceuticals" in the category "operator".

**Individual certificate for the activities of "unsubsted in-company decision-maker" and "accredited business decision-maker"**

These certificates are issued to the professional who:

- (a) a training course that included a one-hour written test of knowledge verification;- successfully passed a one-and-a-half hour test, consisting of thirty questions on the training programme set out in Schedule II of the August 29, 2016 order;
- holds one of the diplomas on the Schedule I list of the same order in the five years prior to the application.

*For further information*: an order of 29 August 2016 establishing and setting out the terms of obtaining the individual certificate for the activity "professional use of phytopharmaceuticals" in the categories "accredited in-house decision-maker" and " decision-maker in an unsubsated company."

**Please note**

These certificates are valid for five years, renewable under the same conditions.

Once the professional meets these conditions, he must apply for the issuance of this certificate (see below "5°). a. Request for an individual certificate of practice").

### b. EU nationals: for temporary and casual exercise (free provision of services (LPS))

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) holding a certificate issued by a Member State and legally established and engaged in an activity related to the use of products phytopharmaceuticals may carry out the same activity on a temporary and casual basis in France.

In order to do so, it must, prior to its first service delivery, make a declaration to the Regional Director of Food, Agriculture and Forestry of the place where the first service is delivered (see below "5o). c. Pre-declaration for the national for a temporary and casual exercise (LPS)."

*For further information*: Article R. 254-9 of the Rural Code and Marine Fisheries.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of a legally established EU or EEA Member State engaged in an activity related to the use of phytopharmaceuticals may carry out the same activity on a permanent basis in France.

As long as the national holds an individual certificate of exercise issued by a Member State, he is deemed to hold the certificate required for the exercise in France of this activity.

*For further information*: II of Article R. 254-9 of the Rural Code and Marine Fisheries.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Obligations on the use of phytopharmaceuticals**

The professional who uses phytopharmaceuticals is subject to compliance with the regulations regarding the use of these products.

As such, it must respect:

- the time limit between the application of the products and the harvest;
- The maximum wind speed beyond which it cannot use these products;
- provisions for the application or emptying of phytopharmaceutical effluents.

In addition, any professional using phytopharmaceuticals is required to follow the national plan setting out the objectives and measures taken to reduce the effects of the use of these products on human health and the environment.

*For further information*: Article L. 253-6 of the Rural Code and Marine Fisheries; Order of May 4, 2017 relating to the marketing and use of phytopharmaceuticals and their adjuvants covered by Article L. 253-1 of the Rural Code and Marine Fisheries.

4°. Insurance and sanctions
-----------------------------------------------

**Insurance**

The professional is required, in order to obtain his approval, to take out insurance covering his professional civil liability.

*For further information*: Article L. 254-2 of the Rural Code and Marine Fisheries.

**Sanctions**

The professional faces a sentence of six months' imprisonment and a fine of 150,000 euros if he makes the use of phytopharmaceuticals without holding the certification or the individual certificate of exercise.

In addition, the National Food, Environment and Labour Safety Agency may prohibit or regulate the use of phytopharmaceuticals in protected areas, including areas open to the public.

**Please note**

Public persons cannot use products to eliminate pests for the maintenance of public spaces (green spaces, schools), except those that are necessary to combat a serious health hazard. and those that pose no threat to the environment or human health.

*For further information*: Article L. 253-7 of the Rural Code and Marine Fisheries.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for an individual certificate of practice

**Competent authority**

The Regional Directorate of Food, Agriculture and Forestry (DRAAF) of the applicant's place of residence or, if necessary, the place of the head office of the organization where the training was carried out, is competent to issue the certificate.

**Supporting documents**

The professional must create an online account on the site[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) to access the certificate application service.

His application must include, depending on the case:

- A proof of follow-up training and, if necessary, a pass to the test;
- copy of his diploma or title.

**Time and procedure**

The certificate is issued to the professional within two months of filing his application. In the absence of the issuance of the certificate after this period, and unless notification of refusal, the above supporting documents are worth individual certificate for up to two months.

*For further information*: Articles R. 254-11 and R. 254-12 of the Rural and Marine Fisheries Code.

### b. Application for approval for phytopharmaceuticals

**Competent authority**

The prefect of the region in which the company's head office is located is competent to issue the accreditation.

**Supporting documents**

His application must include the[Application form](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completed and signed as well as the following documents:

- A certificate of professional liability insurance policy;
- a certificate issued by a third-party organization justifying that it performs this activity in accordance with the conditions guaranteeing the protection of public health and the environment. This company certification is carried out by a certifying body following an audit on the basis of a repository set out in the annex of the decree of 25 November 2011 relating to the certification repository;
- copying a contract with a third-party organization with a contract providing the necessary follow-up to maintain certification.

**Please note**

Micro-distributors must submit the following documents to THE DRAAF:

- a certificate of professional liability insurance;
- proof of the holding of the Certiphyto certificate by all staff performing supervisory, sales or consulting functions;
- a document justifying that it is subject to the micro-enterprise tax system.

**Time and procedure**

The professional who starts his activity must apply for an interim approval which will be granted for a period of six months non-renewable. He will then be able to apply for final approval.

The silence kept by the prefect of the region beyond a period of two months, is worth decision of rejection.

In addition, the professional is required, each year, to provide the prefect with a copy of the certificate of subscription to an insurance policy.

*For further information*: Articles R. 254-15-1 to R. 254-17 of the Rural Code and Marine Fisheries.

### c. Pre-declaration for the national for a temporary and casual exercise (LPS)

**Competent authority**

The national must apply by any means to the DRAAF for the place where he or she first receives services.

**Supporting documents**

Its application must include the individual certificate of exercise issued by the Member State and, if necessary, accompanied by its accredited translation into French.

**Please note**

This declaration must be renewed every year and in the event of a change in employment status.

*For further information*: III of Article R. 254-9 of the Rural Code and Marine Fisheries.

### d. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

