﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP132" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers agricoles" -->
<!-- var(title)="Expert forestier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-agricoles" -->
<!-- var(title-short)="expert-forestier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-agricoles/expert-forestier.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="expert-forestier" -->
<!-- var(translation)="None" -->

# Expert forestier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l’activité

L'expert forestier est un professionnel qui réalise des missions d'expertise en matière forestière portant sur les biens d’autrui, meubles et immeubles, ainsi que sur les droits mobiliers et immobiliers afférents à ces biens.

Ses missions portent notamment sur :

- la gestion forestière et sylviculture,
- la maitrise d’œuvre de travaux : boisement, infrastructures…
- le conseil, l'expertise ou l'évaluation du patrimoine forestier en cas d'achat, vente, succession ou litige ;
- les études d'impacts notamment environnementales et paysagères ;
- les diagnostics sanitaires d'arbres et ornements ;
- l'audit en environnement ;
- la gestion des chasses et étangs ;
- l’évaluation de préjudices écologiques et mise en place de mesures de compensations (ERC) ;
- la gestion de groupements forestiers.

*Pour aller plus loin* : article L. 171-1 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Le professionnel doit être inscrit sur la liste des experts forestiers établie par le comité du Conseil national de l'expertise foncière agricole et forestière (Cnefaf).

Pour cela, l'intéressé doit remplir les conditions suivantes :

- justifier d'une pratique professionnelle à titre personnel ou sous la responsabilité d'un maître de stage :
  - soit d'au moins sept ans,
  - soit d'au moins trois ans si le professionnel est titulaire d'un titre ou diplôme sanctionnant au moins quatre années d'études post-secondaire dans les disciplines agricole, agronomique, environnementale, forestière, juridique ou économique ou dans les domaines de l’aménagement du territoire, des paysages ou de l’urbanisme ;
- ne pas avoir fait l'objet d'une condamnation pour des faits contraires à l'honneur, la probité ou aux bonnes mœurs au cours des cinq dernières années ;
- ne pas avoir fait l'objet d'une sanction disciplinaire ou administrative de destitution, radiation, ou de retrait d'agrément ou d'autorisation ;
- ne pas avoir été frappé de faillite personnelle.

*Pour aller plus loin* : article R. 171-10 du Code rural et de la pêche maritime.

#### Formation

Plusieurs cursus délivrant des titres et diplômes de type diplôme d'ingénieur ou master spécialisé en agronomie (bac + 5), permettent de suivre un enseignement en vu de devenir expert forestier.

Par exemple,  l'intéressé peut suivre une formation en vue d'obtenir (liste non exhaustive) :

- un diplôme d'ingénieur forestier délivré par l’Institut des sciences et industries du vivant et de l'environnement [AgroParisTech](http://www.agroparistech.fr/) ;
- un diplôme d'ingénieur agronome avec une spécialisation forestière ;
- un diplôme d'Ingénieur des Ponts, des Eaux et des Frêts (IPEF) ;
- un diplôme d'ingénieur en agriculture avec une connaissance du milieu forestier ;
- un master spécialisé forêt ou sciences et technologies du vivant et de l'environnement.

*Pour aller plus loin* : il est conseillé de se rapprocher des établissements proposant ces formations pour de plus amples informations.

#### Coûts associés à la qualification

La formation menant à la profession d'expert forestier est payante et son coût varie selon l'établissement choisi. Pour plus d'informations, il est conseillé de se renseigner auprès des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE) peut faire usage de son titre professionnel en France, à titre temporaire et occasionnel, sans être inscrit sur la liste des experts forestiers, sous réserve :

- d'être légalement établi dans l'un de ces États pour y exercer la profession d'expert forestier ;
- lorsque ni la profession ni la formation ne sont réglementées dans cet État, d'avoir exercé la profession d'expert forestier dans cet État pendant au moins un an au cours des dix dernières années ;
- d'être assuré contre les conséquences pécuniaires de sa responsabilité civile professionnelle.

Pour cela, le ressortissant devra en faire la demande, préalablement à sa première prestation par déclaration adressée au Cnefaf (cf. infra « 5°. a. Déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE en vue d'une LPS »).

*Pour aller plus loin* : article L. 171-2 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d’un État membre de l’Union européenne ou d’un autre État partie à l’accord sur l’espace économique européen qui souhaite s’établir en France pour exercer la profession d’expert forestier devra demander son inscription sur la liste nationale des experts forestiers auprès du comité du Cnefaf (cf. infra « 5°. b. Demander son inscription sur la liste des experts forestiers pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE) »).

Le comité du Cnefaf procédera à une comparaison entre, d'une part, la formation requise en France pour être inscrit sur la liste et, d'autre part, celle reçue par le demandeur, ainsi que les connaissances, aptitudes et compétences qu'il a acquises au cours de son expérience professionnelle ou lors de son apprentissage tout au long de la vie ayant fait l'objet d'une validation par un organisme compétent.

Lorsque cet examen fait apparaître une différence substantielle de formation au regard des qualifications requises pour l'accès à la profession et son exercice en France, que les connaissances acquises par le demandeur au cours de son expérience professionnelle ne sont pas de nature à combler, en tout ou en partie, le comité soumettra le demandeur à une mesure de compensation (stage d'adaptation ou épreuve d'aptitude). 

*Pour aller plus loin* : articles L. 171-3, R. 171-12-1, R. 2045 et R. 171-12-2 du Code rural et de la pêche maritime.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

### Règles et devoirs professionnels

L'expert forestier est tenu au respect des règles et devoirs qui incombent à sa profession, durant l'exercice de son activité.

À ce titre, l'expert forestier s'engage à :

- respecter l'indépendance nécessaire à l'exercice de sa profession ;
- se prononcer en toute impartialité ;
- respecter le secret professionnel ;
- s'abstenir de toute pratique déloyale à l'égard de ses confrères.

*Pour aller plus loin* : articles L. 171-1, R. 172-1 et suivants du Code rural et de la pêche maritime.

### Incompatibilités

L'exercice de la profession d'expert forestier est incompatible avec :

- une charge d'officier public et ministériel ;
- toute fonction susceptible de porter atteinte à son indépendance, en particulier celles consistant à acquérir de façon habituelle des biens mobiliers ou immobiliers en vue de leur revente.

*Pour aller plus loin* : article L. 171-1 alinéa 2 du Code rural et de la pêche maritime.

## 4°. Assurance et sanctions

### Assurance

L'expert forestier exerçant à titre libéral doit souscrire une assurance de responsabilité civile personnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 171-1 alinéa 8 du Code rural et de la pêche maritime.

### Sanctions disciplinaires

Tout manquement aux règles professionnelles de la part de l'expert forestier peut faire l'objet d'une procédure disciplinaire devant le comité du Cnefaf et d'une sanction disciplinaire qui peut être :

- un blâme ;
- un avertissement ;
- une suspension temporaire de trois mois à trois ans ;
- une radiation en cas de faute professionnelle grave ou de condamnation pour faits contraires à l’honneur, à la probité et aux bonnes mœurs.

*Pour aller plus loin* : article L. 171-1 alinéa 7 du Code rural et de la pêche maritime.

### Sanctions pénales

Toute personne encourt une peine d'un an d'emprisonnement et 15 000 euros d'amende pour usurpation du titre dès lors qu'il fait usage :

- du titre d'expert forestier sans être inscrit sur la liste établie par le comité du Cnefaf ;
- d'une dénomination pouvant porter à confusion avec le titre d'expert forestier.

*Pour aller plus loin* : article L. 171-1 alinéa 9 du Code rural et de la pêche maritime ; article 433-17 du Code pénal.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'une LPS

#### Autorité compétente

Le professionnel doit effectuer une déclaration préalable auprès du Cnefaf.

#### Pièces justificatives

Sa demande doit comprendre les documents suivants :

- une preuve de sa nationalité ;
- une attestation certifiant qu'il est légalement établi dans un État de l'UE ou de l'EEE ;
- une preuve que le ressortissant a exercé les activités d'expertise forestière pendant au moins un an ou à temps partiel pendant une durée équivalente au cours des dix dernières années lorsque l’activité ou la formation n’est pas réglementée dans son État membre d’origine ;
- une information relative à la souscription de police d'assurance précisant la raison sociale et l'adresse de l'entreprise d'assurance, les références et la période de validité du contrat, l'étendue et le montant des garanties.

À ces documents est jointe, en tant que de besoin, leur traduction en langue française.

*Pour aller plus loin* : articles R. 171-17- à R. 171-17-3 du Code rural et de la pêche maritime.

### b. Demander son inscription sur la liste des experts forestiers pour le ressortissant de l’UE ou de l'EEE en vue d’un exercice permanent (LE)

#### Autorité compétente

Le comité du Cnefaf est compétent pour se prononcer sur la demande d'inscription sur la liste des experts forestiers.

#### Pièces justificatives

La demande doit contenir les éléments suivants, le cas échéant accompagnés de leur traduction en français :

- l'ensemble des éléments justifiant de l'état civil du demandeur ;
- une copie de ses titres ou diplômes ;
- un justificatif de son expérience professionnelle ;
- un curriculum vitae précisant les activités professionnelles antérieures du professionnel (date et lieu d'exercice) ;
- un justificatif ou, à défaut, un engagement de souscription de l'assurance de responsabilité civile professionnelle ;
- un extrait de casier judiciaire n° 3 datant de moins de trois mois ou tout document équivalent délivré par l'autorité compétente de l’État de l'UE ou de l'EEE de moins de trois mois ;
- une déclaration sur l'honneur ou tout autre moyen de preuve attestant que l'intéressé remplit les conditions d'honorabilité ;
- le cas échéant, une déclaration de l'activité envisagée sous forme sociétaire.

#### Procédure

À réception du dossier, le comité dispose de trois mois pour informer le ressortissant de sa décision de l'inscrire sur la liste.

Toutefois, en cas de différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées en France, le comité pourra soumettre le ressortissant à la mesure de compensation de son choix : soit un stage d'adaptation, soit une épreuve d'aptitude.

Le silence gardé pendant un délai de trois mois vaut acceptation de la décision.

Le renouvellement de la demande est soumis à la production de l'attestation de l'assurance de responsabilité civile professionnelle.

*Pour aller plus loin* : articles R. 171-10 à R. 171-13 du Code rural et de la pêche maritime.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).