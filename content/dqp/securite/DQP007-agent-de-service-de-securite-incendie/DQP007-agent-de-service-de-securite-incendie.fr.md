﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP007" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité" -->
<!-- var(title)="Agent de service de sécurité incendie et d'assistance à la personne" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite" -->
<!-- var(title-short)="agent-de-service-de-securite-incendie" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite/agent-de-service-de-securite-incendie-et-d-assistance-a-la-personne.html" -->
<!-- var(last-update)="2020-04-15 17:22:21" -->
<!-- var(url-name)="agent-de-service-de-securite-incendie-et-d-assistance-a-la-personne" -->
<!-- var(translation)="None" -->

# Agent de service de sécurité incendie et d'assistance à la personne

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:21<!-- end-var -->

## 1°. Définition de l'activité

L'agent de service de sécurité incendie et d'assistance à la personne (« agent SSIAP ») est un professionnel dont la mission est d'assurer la sécurité des personnes et la sécurité incendie des biens, la prévention des incendies, la sensibilisation des employés en matière de sécurité contre les incendies et d'assistance à personnes, l'entretien élémentaire des moyens concourant à la sécurité incendie, l'alerte et l'accueil des secours, l'évacuation du public, l'intervention précoce face aux incendies, l'assistance à personnes au sein des établissements où ils exercent et l'exploitation du PC de sécurité incendie.

Il s'occupe des rondes sur les lieux surveillés et veille à sensibiliser le personnel aux problématiques incendie.

En cas de départ de feu, il coordonnera les actions d'évacuation et de mise en sécurité des personnes présentes dans l'enceinte du site.

*Pour aller plus loin* : article 2 de l'arrêté du 2 mai 2005 relatif aux missions, à l'emploi et à la qualification du personnel permanent des services de sécurité incendie des établissements recevant du public et des immeubles de grande hauteur.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession d'agent SSIAP est réservée au titulaire :

- d'un certificat d'aptitude médicale de moins de trois mois ;
- de l'attestation de formation aux premiers secours (AFPS) ou de l'attestation de prévention de secours civiques de niveau 1 (PSC1) depuis moins de 2 ans ;
- de l'attestation de sauveteur secouriste (SST) ou de l'attestation de premier secours en équipe (PSE1) en cours de validité ;
- d'un des titres suivants :
  - le diplôme d'agent de sécurité incendie et d'assistance à personnes (SSIAP 1),
  - le baccalauréat professionnel spécialité sécurité prévention,
  - le brevet professionnel spécialité agent technique de prévention et de sécurité,
  - le certificat d'aptitude professionnel spécialité agent de prévention et de sécurité,
  - la mention complémentaire spécialité sécurité civile et d'entreprise,
  - le brevet national de jeunes sapeurs-pompiers depuis moins de trois ans avec suivi d'un [module complémentaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=87A6A1A93A66E34BCFFFFD96518CC4E2.tplgfr27s_1?idArticle=LEGIARTI000029044217&cidTexte=LEGITEXT000006051723&dateTexte=20171221),
  - ou avoir été sapeur-pompier professionnel ou volontaire, pompier militaire de l'armée de terre, pompier militaire de l'armée de l'air ou marin-pompier de la marine nationale et avoir suivi le module complémentaire visé ci-avant,
  - ou avoir été sous-officier des sapeurs-pompiers professionnels ou volontaires, des pompiers militaires de l'armée de terre, des pompiers militaires de l'armée de l'air ou des marins-pompiers de la marine nationale et titulaire de l'unité de valeur de formation des sapeurs-pompiers PRV 1 ou de l'AP 1 ou du certificat de prévention délivré par le ministre de l'Intérieur.

*Pour aller plus loin* : articles 3 et 4 de l'arrêté du 2 mai 2005.

#### Formation

Le diplôme d'agent SSIAP de niveau 1 visé ci-avant est délivré par un centre de formation ayant reçu un agrément préfectoral.

À l'issue d'une formation d'au moins 67 heures, le candidat se présentera à un examen composé d'une épreuve écrite avec un questionnaire à choix multiple, et d'une épreuve pratique pendant laquelle il réalisera une ronde avec détection des anomalies et des sinistres potentiels.

*Pour aller plus loin* : article 8 de l'arrêté du 2 mai 2005.

#### Coûts associés à la qualification

Le prix de formation menant à la délivrance du diplôme SSIAP1 varie entre 850 € et 1 300 €. Pour plus de renseignements, il est conseillé de se rapprocher des centres agréés la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen qui est établi et exerce légalement l'activité d'agent SSIAP dans cet État peut exercer en France la même activité, de manière temporaire et occasionnelle.

Il devra, au préalable, en faire la demande par déclaration écrite adressée au ministre de l'Intérieur (cf. infra « 4°. a. Obtenir une autorisation d'exercer pour le ressortissant en vue d'un exercice temporaire ou occasionnel (LPS) »).

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans l'État dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États de l'UE.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation en France, le ministre de l'Intérieur peut exiger que l'intéressé se soumette à une épreuve d'aptitude.

*Pour aller plus loin* : article 3-1 de l'arrêté du 2 mai 2005.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE, qui exerce légalement l'activité d'agent SSIAP dans cet État, peut s'établir en France pour y exercer cette même activité de manière permanente.

Il devra, dès lors, demander une attestation de reconnaissance de qualification professionnelle (cf. infra « 4°. b. Demander une attestation de qualification professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, à temps plein ou à temps partiel, au cours des dix années.

Lorsqu'il existe des différences substantielles entre sa qualification professionnelle et la formation exigée en France, le ministre de l'Intérieur pourra exiger qu'il se soumette à une mesure de compensation (cf. infra 4°. b. Bon à savoir : mesure de compensation »).

*Pour aller plus loin* : article 3-2 de l'arrêté du 2 mai 2005.

## 3°. Formation professionnelle continue

Une formation professionnelle continue doit obligatoirement être suivie par l'agent SSIAP en exercice, tous les trois ans, au plus tard à la date anniversaire de la délivrance du diplôme SSIAP1.

Elle doit permettre au professionnel de mettre à jour ses connaissances des nouvelles réglementations et de maintenir ses compétences. À l'issue du stage, le centre de formation agréé remettra à chaque participant une attestation de stage de maintien des connaissances.

**À noter**

L'agent SSIAP devra se soumettre à un stage de secourisme tous les deux ans.

*Pour aller plus loin* : article 7 et annexe XII de l'arrêté du 2 mai 2005.

## 4°. Démarches et formalités de reconnaissance de qualification

### a. Obtenir une autorisation d'exercer pour le ressortissant en vue d'un exercice temporaire ou occasionnel (LPS)

#### Autorité compétente

Le ministre de l'Intérieur est compétent pour se prononcer sur la demande d'autorisation d'exercer la prestation.

#### Pièces justificatives

À l'appui de sa demande d'autorisation, le ressortissant transmet à l'autorité compétente un dossier comportant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- une attestation justifiant son activité pendant au moins un an au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre ;
- une preuve de ses qualifications professionnelles, notamment en matière de secours à personnes, de connaissance de la réglementation française de la sécurité contre l'incendie, et de sa capacité à rédiger des comptes rendus en langue française ;
- une attestation confirmant l'absence d'interdictions temporaires ou définitives d'exercer la profession ou de condamnations pénales.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Issue de la procédure

À réception de l'ensemble des pièces du dossier, le ministre de l'Intérieur dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation ;
- soit d'imposer au ressortissant une épreuve d'aptitude lorsqu'il existe des différences substantielles entre ses qualifications professionnelles et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de services en France ;
- soit de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés.

Le silence gardé de l'autorité compétente dans ces délais vaut autorisation de débuter la prestation de services.

*Pour aller plus loin* : article 3-1 de l'arrêté du 2 mai 2015.

### b. Demander une attestation de qualification professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

La demande de reconnaissance de qualification professionnelle doit être adressée au ministre de l'Intérieur.

#### Pièces justificatives

La demande d'attestation de reconnaissance de qualification professionnelle est faite par courrier et comprend un dossier composé des pièces justificatives suivantes :

- une photocopie de la pièce d'identité du demandeur en cours de validité ;
- un justificatif de la qualification professionnelle sous la forme d'une attestation de compétences ou d'un diplôme ou d'un titre de formation professionnelle ;
- une déclaration permettant de justifier ses connaissances de la langue française ;
- une déclaration permettant de justifier ses connaissances dans la réglementation française en matière de sécurité contre l'incendie.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Bon à savoir : mesure de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à une épreuve d'aptitude dans un délai maximum de six mois à compter de la décision qui la lui impose.

*Pour aller plus loin* : article 3-2 de l'arrêté du 2 mai 2015.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).