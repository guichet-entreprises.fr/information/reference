﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP007" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Security" -->
<!-- var(title)="Fire safety and first aid officer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="security" -->
<!-- var(title-short)="fire-safety-and-first-aid-officer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/security/fire-safety-and-first-aid-officer.html" -->
<!-- var(last-update)="2020-04-15 17:22:21" -->
<!-- var(url-name)="fire-safety-and-first-aid-officer" -->
<!-- var(translation)="Auto" -->


Fire safety and first aid officer
===========================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:21<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Fire Safety and Personal Assistance Officer ("SSIAP Officer") is a professional whose mission is to ensure the safety of persons and the fire safety of property, fire prevention, employee awareness in fire safety and assistance to people, basic maintenance of fire safety assets, alerting and receiving emergency services, evacuation of the public, early response to fires, assistance to fire persons in the establishments where they operate and the operation of the fire safety PC.

He takes care of the rounds on the monitored sites and ensures that staff are made aware of fire problems.

In the event of a fire, it will coordinate the evacuation and safety of those present in the site.

*For further information*: Article 2 of the[decreed from 2 May 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000448223) related to the missions, employment and qualification of permanent fire safety personnel in public and high-rise buildings.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of SSIAP agent is reserved for the holder:

- A certificate of medical fitness of less than three months
- First Aid Training Certificate (AFPS) or Level 1 Civic Relief Prevention Certificate (PSC1) for less than 2 years;
- a valid first aid (SST) certificate or first aid team certificate (PSE1);
- one of the following titles:- The Diploma of Fire Safety and Personal Assistance Officer (SSIAP 1),
  - the professional bachelor's degree specialty safety prevention,
  - The professional patent specialty technical agent of prevention and safety,
  - The certificate of professional aptitude specialty prevention and safety officer,
  - The additional mention of civil and corporate security specialty,
  - national certificate for young firefighters for less than three years with follow-up by a[Add](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=87A6A1A93A66E34BCFFFFD96518CC4E2.tplgfr27s_1?idArticle=LEGIARTI000029044217&cidTexte=LEGITEXT000006051723&dateTexte=20171221),
  - or have been a professional or volunteer firefighter, army military firefighter, air force military firefighter or national navy firefighter and have followed the complementary module referred to above,
  - or have been a NCO of professional or volunteer firefighters, army military firefighters, air force military firefighters or National Navy firefighters and holder of the training value unit PRV 1 or AP 1 or the prevention certificate issued by the Minister of the Interior.

*For further information*: Articles 3 and 4 of the May 2, 2005 order.

#### Training

The above level 1 SSIAP officer diploma is issued by a prefecturally accredited training centre.

After a training of at least 67 hours, the candidate will take an exam consisting of a written test with a multiple-choice questionnaire, and a practical test during which he will perform a round with detection of anomalies and disasters Potential.

*For further information*: Article 8 of the May 2, 2005 order.

#### Costs associated with qualification

The training price leading to the issuance of the SSIAP1 diploma ranges from 850 to 1,300 euros. For more information, it is advisable to approach the licensed centres dispensing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or party to the European Economic Area who is established and legally practises the activity of SSIAP agent in that state may carry out the same activity in France, on a temporary and occasional basis.

He must first request it by written statement to the Minister of the Interior (see infra "4°. a. Obtain a licence to practise for the national for a temporary or casual exercise (LPS)").

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the state in which the professional is legally established, he must have carried out this activity for at least one year, in the course of the ten years before the benefit, in one or more EU states.

Where there are substantial differences between the national's professional qualification and training in France, the Minister of the Interior may require that the person submit to an aptitude test.

*For further information*: Article 3-1 of the May 2, 2005 order.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state, who is legally an SSIAP agent in that state, may settle in France to carry out the same activity on a permanent basis.

He will therefore have to apply for a certificate of recognition of professional qualification (see below: "4. b. Request a certificate of professional qualification for the EU or EEA national for a permanent exercise (LE)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least one year, full-time or at part-time, over the course of ten years.

Where there are substantial differences between his professional qualification and the training required in France, the Minister of the Interior may require him to submit to a compensation measure (see infra 4. b. Good to know: compensation measure »).

*For further information*: Article 3-2 of the May 2, 2005 order.

3°.Continuous vocational training
---------------------------------------------

Continuing vocational training must be completed by the SSIAP officer in practice every three years, no later than the anniversary of the issuance of the SSIAP1 diploma.

It should allow the professional to update his knowledge of the new regulations and maintain his skills. At the end of the internship, the accredited training centre will give each participant a certificate of training to maintain knowledge.

**Please note**

The SSIAP officer will be required to undergo a first aid course every two years.

*For further information*: Article 7 and Appendix XII of the May 2, 2005 order.

4°. Qualification recognition procedures and formalities
----------------------------------------------------------------------------

### a. Obtain a licence to practise for the national for a temporary or casual exercise (LPS)

**Competent authority**

The Minister of the Interior has the authority to decide on the application for authorisation to perform the service.

**Supporting documents**

In support of the application for leave, the national sends a file to the competent authority containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a certificate justifying its activity for at least one year in the last ten years, when neither training nor the practice of the profession is regulated in the Member State;
- proof of his professional qualifications, particularly in the area of personal rescue, knowledge of French fire safety regulations, and his ability to write reports in French;
- a certificate confirming the absence of temporary or permanent bans on practising the profession or criminal convictions.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the Minister of the Interior has one month to decide:

- either to authorize the delivery;
- or to impose an aptitude test on the national when there are substantial differences between his professional qualifications and those required in France. In the event of a refusal to carry out this compensation measure or in the event of failure in its execution, the national will not be able to carry out the provision of services in France;
- inform him of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, as of the resolution of the difficulties.

The silence kept by the competent authority in these times is worth authorisation to begin the provision of services.

*For further information*: Article 3-1 of the May 2, 2015 order.

### b. Request a certificate of professional qualification for the EU or EEA national for a permanent exercise (LE)

**Competent authority**

The application for recognition of professional qualification must be addressed to the Minister of the Interior.

**Supporting documents**

The application for certification of professional qualification is made by mail and includes a file consisting of the following supporting documents:

- A photocopy of the applicant's valid ID
- a proof of professional qualification in the form of a certificate of competency or a diploma or a vocational training certificate;
- A statement to justify his knowledge of the French language;
- a statement to justify his knowledge of French fire safety regulations.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Good to know: compensation measure**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to an aptitude test within a maximum of six months from the decision that imposes it on him.

*For further information*: Article 3-2 of the May 2, 2015 order.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

