﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP006" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité" -->
<!-- var(title)="Agent de recherches privées" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite" -->
<!-- var(title-short)="agent-de-recherches-privees" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite/agent-de-recherches-privees.html" -->
<!-- var(last-update)="2020-04-15 17:22:20" -->
<!-- var(url-name)="agent-de-recherches-privees" -->
<!-- var(translation)="None" -->

# Agent de recherches privées

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:20<!-- end-var -->

## 1°. Définition de l'activité

L'agent de recherches privées (« ARP »), aussi appelé « détective privé » a pour mission de recueillir toutes les informations ou renseignements d'ordre commercial, privé ou industriel visant à constituer un dossier qui sera recevable en cas de procédure judiciaire. Il intervient dans la matérialisation de la preuve dont dépend la résolution d’un litige. Son statut d’agent privé lui permet de se déplacer sur le territoire français et à l’étranger pour mener sa mission.

*Pour aller plus loin* : article L. 621-1 du Code de la sécurité intérieure.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Toute personne souhaitant exercer l'activité d'ARP doit justifier d'une aptitude professionnelle et être titulaire d'une carte professionnelle délivrée par la commission locale d'agrément et de contrôle (CNAC) établie au sein du Conseil national des activités privées de sécurité (Cnaps).

La carte professionnelle peut être délivrée aux conditions suivantes :

- ne pas avoir fait l'objet d'une condamnation à une peine correctionnelle ou à une peine criminelle inscrite au bulletin n° 2 du casier judiciaire ou pour les ressortissants étrangers dans un document équivalent, incompatible avec l'exercice d'une activité de sécurité privée ;
- justifier des qualifications requises pour exercer l'activité de protection physique des personnes : suivi d'une formation dans un organisme de formation autorisé par le Cnaps et détenir une certification professionnelle enregistrée au RNCP, justifier d'une équivalence selon certaines conditions (policiers, gendarmes, policiers municipaux, militaires) ;
- pour les ressortissants étrangers, être détenteur d'un titre de séjour permettant d'exercer une activité sur le territoire national ;
- ne pas être sous le coup d'un arrêté d'expulsion ou d'interdiction du territoire français en cours. 

*Pour aller plus loin* : article L. 622-19 du Code de sécurité intérieure.

#### Formation

La formation professionnelle permettant de justifier l'aptitude professionnelle d'ARP est soumise à une autorisation délivrée par le Cnaps. Cette décision peut intervenir :

- avant un recrutement dans une entreprise assurant la protection des personnes. Dans ce cas, l'intéressé recevra une **autorisation préalable d'accès à la formation** valable 6 mois, à remettre à un centre de formation (autorisé par le Cnaps). Les certifications professionnelles donnant aptitude à exercer l'activité A3P sont régulièrement mises à jour. Elles sont enregistrées au RNCP et figurent sur le site du Cnaps ;
- pendant l'embauche. L'intéressé qui a conclu un contrat de travail avec une entreprise de protection privée doit demander une **autorisation provisoire d'être employé** valable 6 mois. Cette autorisation ne lui permet pas d'occuper un poste d'A3P mais engage l'entreprise à lui assurer sans délai une formation lui permettant de justifier de son aptitude à exercer. La demande d'autorisation se fait directement [en ligne](http://www.cnaps.interieur.gouv.fr/) sur le site du Cnaps.

**À noter**

La demande d'autorisation se fait directement en ligne sur le site du Cnaps.

*Pour aller plus loin* : articles L. 622-1, L. 622-2 et R. 622-17 et suivants du Code de la sécurité intérieure.

#### Coûts associés à la qualification

La formation menant à la profession d'ARP peut être payante. Pour plus d'informations, il est conseillé de se rapprocher des organismes de formation la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l'UE ou de l’EEE exerçant l’activité d'ARP dans l’un de ces États peut faire usage de son titre professionnel en France, à titre temporaire et occasionnel.

Il devra en faire la demande préalablement à sa première prestation par déclaration adressée à la commission locale d'agrément et de contrôle (CLAC) territorialement compétente pour Paris (cf. infra « 5°. a. Effectuer une déclaration pour le ressortissant de l’UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an au cours des dix années qui précèdent la prestation.

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la demande de déclaration par la CLAC.

*Pour aller plus loin* : article R. 622-23 du Code de la sécurité intérieure.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité d'ARP dans cet État peut exercer la même activité en France de manière permanente.

Il devra procéder à une demande de carte professionnelle, instruite par les services de la délégation territoriale compétente et présentée devant la CLAC territorialement compétente (cf. infra « 5°. b. Obtenir une carte professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

Lorsque ni l'activité ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le ressortissant de l'UE ou de l'EEE, devra justifier d'une formation suivie dans cet Etat et d'une expérience professionnelle dans un ou des États de l'UE ou de l'EEE au moins égale à un an au cours des dix dernières années.

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard de celles requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une mesure de compensation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : article L. 622-19 du Code de la sécurité intérieure.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Respect des règles déontologiques

Les dispositions du Code de déontologie relatif aux activités privées de sécurité s'imposent à tous les ARP exerçant en France. Elles sont codifiées aux articles R. 631-1 à R. 631-31 du Code de la sécurité intérieure.

L'ARP devra notamment éviter les conflits d'intérêt, respecter le secret des affaires, adopter une attitude professionnelle en toute occasion, ou encore assurer la confidentialité des échanges.

*Pour aller plus loin* : articles R. 631-1 à R. 631-31 du Code de la sécurité intérieure.

### b. Incompatibilité d'exercice

Dès lors que l'intéressé exerce la fonction d'ARP, il ne peut plus :

- surveiller ou garder des personnes ou des biens meubles ou immeubles ;
- surveiller et transporter des bijoux, des fonds ou des métaux précieux ;
- protéger l'intégrité physique des personnes ;
- protéger des navires français contre des menaces d'acte terroriste ou de prise de contrôle.

*Pour aller plus loin* : articles L. 611-1 et L. 622-2 du Code de la sécurité intérieure.

## 4°. Formation continue et assurance

### a. Obligation de suivre une formation continue

Le renouvellement de la carte professionnelle est soumis au suivi d'une formation continue visant au maintien et à l'actualisation des compétences (MAC) de l'intéressé. Cette formation dispensée sous la forme d'un [stage](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=426183422AEAB5864A868A3038C21D37.tplgfr22s_2?idArticle=JORFARTI000034104623&cidTexte=JORFTEXT000034104616&dateTexte=29990101&categorieLien=id) d'une durée de 35 heures doit intervenir dans un délai de vingt-quatre mois avant la date d'expiration de la carte.

*Pour aller plus loin* : arrêté du 27 février 2017 relatif à la formation continue des agents de recherches privées.

### b. Assurance de responsabilité civile professionnelle

En sa qualité de professionnel indépendant, l'ARP doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. Dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 622-5 du Code de la sécurité intérieure.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration pour le ressortissant de l’UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La délégation territoriale de la CLAC dans laquelle se trouve Paris est compétente pour se prononcer sur la demande de déclaration.

#### Pièces justificatives

La demande de déclaration est un dossier transmis par tout moyen comprenant l'ensemble des documents suivants :

- une déclaration comportant les informations relatives à l'état civil du ressortissant ;
- une photocopie de sa pièce d'identité ;
- une attestation certifiant que l'intéressé exerce et est établi légalement dans un État de l'UE ou de l'EEE ;
- l'absence de condamnation pénale inscrite sur son casier judiciaire ;
- une preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE.

#### Délai

La CLAC dispose d'un délai d'un mois pour rendre sa décision :

- d’autoriser le ressortissant à faire sa première prestation de services ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelles dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, elle aura deux mois pour se décider, à compter de la résolution de la ou des difficultés. En l'absence de réponse de l'autorité compétente dans ces délais, la prestation de services peut débuter.

*Pour aller plus loin* : article R. 622-23 du Code de la sécurité intérieure.

### b. Obtenir une carte professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

La CLAC territorialement compétente se prononce sur la délivrance de la carte professionnelle dès lors que le ressortissant remplit les conditions d'attribution.

#### Pièces justificatives

Pour obtenir la carte professionnelle, le ressortissant transmet par voie postale un dossier complet à la CLAC territorialement compétente. Ce dossier doit comporter les documents justificatifs suivants :

- le formulaire dûment complété et signé ;
- une photocopie d'une pièce d'identité du ressortissant ;
- une attestation d'emploi délivrée par l'employeur ou le futur employeur du ressortissant ;
- un justificatif attestant de son aptitude professionnelle qui peut être :
  - un certificat professionnel enregistré au Répertoire national des certifications professionnelles,
  - un certificat de qualification professionnelle élaboré par sa branche professionnelle,
  - une attestation de compétences ou un titre de formation délivré par un État de l'UE ou de l'EEE qui réglemente l'activité d'ARP sur son territoire, et comprenant le détail et la durée des modules de la formation suivie,
  - une preuve par tout moyen que le ressortissant a exercé cette activité pendant un an à temps plein ou à temps partiel au cours des dix dernières années, lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE.

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé, le cas échéant.

#### Durée et renouvellement

La carte professionnelle est délivrée sous la forme dématérialisée d'un numéro d'enregistrement et reste valable pendant cinq ans. Tout changement de situation professionnelle devra être notifié à la CLAC mais n'entraîne pas le renouvellement obligatoire de la carte. À l'issue de ces cinq ans, le professionnel pourra demander le renouvellement trois mois avant la date d'expiration à condition de présenter une attestation de formation continue (cf. supra « 4°. a. Obligation de suivre une formation professionnelle continue »).

#### Issue de la procédure

Une fois que le ressortissant a obtenu le numéro d'enregistrement de la CLAC territorialement compétente, il devra le transmettre à son employeur qui lui délivrera la carte professionnelle définitive.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à une mesure de compensation, qui peut être :

- un stage d'adaptation d'une durée maximale de trois ans ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : articles L. 622-19 et R. 622-22 du Code de la sécurité intérieure.

#### Voies et délais de recours

Le demandeur peut contester le refus de délivrance de la carte professionnelle dans un délai de deux mois à compter de la notification de la décision de refus en formant un recours administratif préalable obligatoire auprès de la Commission nationale d’agrément et de contrôle du Conseil national des activités privées de sécurité (Cnaps), située 2-4-6, boulevard Poissonnière, 75009 Paris.

La Commission nationale statuera sur le fondement de la situation de fait et de droit prévalant à la date de sa décision.

Ce recours est obligatoire avant tout recours contentieux. Il est gratuit.

Le recours contentieux peut être exercé auprès du tribunal administratif du lieu de résidence du demandeur ou auprès du tribunal administratif de Paris pour les requérants ayant leur lieu de résidence à l’étranger dans les deux mois à compter soit de la notification de la décision expresse prise par la Commission nationale d’agrément et de contrôle, soit de l’acquisition de la décision implicite de rejet résultant du silence gardé par la Commission nationale d’agrément et de contrôle pendant deux mois à compter de la date de la réception du recours administratif préalable obligatoire.

*Pour aller plus loin* : article L. 633-3 du Code de la sécurité intérieure ; articles L. 412-1 à L. 412-8 du Code des relations entre le public et l’administration ; articles R. 421-1 à R. 421-7 du Code de justice administrative.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).