﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP006" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Security" -->
<!-- var(title)="Private detective" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="security" -->
<!-- var(title-short)="private-detective" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/security/private-detective.html" -->
<!-- var(last-update)="2020-04-15 17:22:20" -->
<!-- var(url-name)="private-detective" -->
<!-- var(translation)="Auto" -->


Private detective
========================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:20<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The private research officer ("ARP"), also known as a "private investigator" is tasked with collecting all commercial, private or industrial information or information to build a file that will be admissible in the event of a court proceeding. . It intervenes in the materialization of the evidence on which the resolution of a dispute depends. His status as a private agent allows him to travel on French territory and abroad to carry out his mission.

*For further information*: Section L. 621-1 of the Code of Homeland Security.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Anyone wishing to carry out the ARP activity must prove a professional aptitude and hold a professional card issued by the Local Accreditation and Control Board (CNAC) established within the National Council of Activities (Cnaps).

The business card can be issued under the following conditions:

- not having been sentenced to a correctional sentence or a criminal sentence recorded on the no. 2 criminal record or for foreign nationals in an equivalent document, incompatible with the exercise of a criminal activity Private security;
- justify qualifications required to carry out the physical protection activity of persons: followed by training in a training organization authorized by the Cnaps and hold a professional certification registered with the RNCP, justify a equivalence under certain conditions (police, gendarmes, municipal police, military);
- for foreign nationals, to hold a residence permit to carry out an activity on the national territory;
- not be subject to an order of expulsion or ban from the current French territory.

*For further information*: Article L. 622-19 of the Code of Homeland Security.

#### Training

Vocational training to justify ARP's professional ability is subject to an authorization issued by the Cnaps. This decision may come:

- before recruiting to a company that protects people. In this case, the person concerned will receive a**prior authorisation for access to training** 6 months, to be handed over to a training centre (authorized by the Cnaps). Professional certifications that provide a ability to carry out the A3P activity are regularly updated. They are registered with the RNCP and appear on the Cnaps website;
- during hiring. An individual who has entered into an employment contract with a private protection company must apply for a**provisional authorization to be employed** valid for 6 months. This authorization does not allow him to hold an A3P position but commits the company to provide him with immediate training to justify his fitness to practice. The application for authorisation is made directly[online](http://www.cnaps.interieur.gouv.fr/) on the Cnaps website.

**Please note**

The application for permission is made directly online on the Cnaps website.

*For further information*: Articles L. 622-1, L. 622-2 and R. 622-17 and following of the Homeland Security Code.

#### Costs associated with qualification

Training leading to the ARP profession can pay off. For more information, it is advisable to get closer to the training organizations dispensing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of an EU or EEA state operating as a PRA in one of these states may use his or her professional designation in France on a temporary and casual basis.

He will have to apply for it before his first delivery by declaration addressed to the local accreditation and control commission (CLAC) territorially responsible for Paris (see infra "5°. a. Make a declaration for the EU or EEA national for a temporary and casual exercise (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional must have carried it out in one or more Member States for at least one year in the ten years that precede the performance.

If the examination of professional qualifications shows substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test within a period of time. one month from the receipt of the CLAC's request for declaration.

*For further information*: Article R. 622-23 of the Homeland Security Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state who is established and legally carries out ARP activity in that state may carry out the same activity in France on a permanent basis.

He will have to apply for a professional card, which is being studied by the competent territorial delegation and submitted to the territorially competent CLAC (see infra "5o). b. Obtain a business card for the EU or EEA national for a permanent exercise (LE) ").

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the EU or EEA national will have to justify training in that state and work experience. one or more EU or EEA states at least one year in the last ten years.

If the examination of professional qualifications reveals substantial differences in relation to those required for access to the profession and its practice in France, the person concerned may be subject to a compensation measure (see infra "5°. b. Good to know: compensation measures").

*For further information*: Section L. 622-19 of the Code of Homeland Security.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Compliance with ethical rules

The provisions of the Code of Ethics relating to private security activities are imposed on all PRAs practising in France. They are codified to sections R. 631-1 to R. 631-31 of the Homeland Security Code.

In particular, the PRA must avoid conflicts of interest, respect business secrecy, adopt a professional attitude at all times, or ensure the confidentiality of exchanges.

*For further information*: Articles R. 631-1 to R. 631-31 of the Homeland Security Code.

### b. Exercise incompatibility

Once the person is acting as ARP, he can no longer:

- monitor or keep people or property or property
- monitor and transport jewellery, funds or precious metals;
- Protecting people's physical integrity
- protect French ships from threats of terrorist acts or takeovers.

*For further information*: Articles L. 611-1 and L. 622-2 of the Homeland Security Code.

4°. Continuing education and insurance
----------------------------------------------------------

### a. Obligation to take continuing education

The renewal of the professional card is subject to the follow-up of continuous training aimed at maintaining and updating the skills (MAC) of the person concerned. This training, delivered in the form of a[Internship](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=426183422AEAB5864A868A3038C21D37.tplgfr22s_2?idArticle=JORFARTI000034104623&cidTexte=JORFTEXT000034104616&dateTexte=29990101&categorieLien=id) 35 hours must take place within 24 months of the card's expiry date.

*For further information*: :[27 February 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034104616&categorieLien=id) ongoing training of private research officers.

### b. Professional liability insurance

As an independent professional, the PRA must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Section L. 622-5 of the Code of Homeland Security.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a declaration for the EU or EEA national for a temporary and casual exercise (LPS)

#### Competent authority

The territorial delegation of the CLAC in which Paris is located is competent to decide on the request for a declaration.

#### Supporting documents

The request for return is a file transmitted by any means including all of the following documents:

- A statement containing information about the national's marital status;
- A photocopy of his ID
- A certificate certifying that the person is practising and is legally established in an EU or EEA state;
- the absence of a criminal conviction on his criminal record;
- proof by any means that the national has been engaged in this activity, full-time or part-time, in the last ten years when neither professional activity nor training is regulated in the EU or EEA State.

#### Time

The CLAC has one month to make its decision:

- Allow the national to provide his or her first service;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the professional qualifications and experience he uses are substantially different from those required for the exercise of the profession in France;
- inform them of one or more difficulties that may delay decision-making. In this case, it will have two months to decide, as of the resolution of the difficulty or difficulties. In the absence of a response from the competent authority within these timeframes, service delivery may begin.

*For further information*: Article R. 622-23 of the Homeland Security Code.

### b. Obtain a business card for the EU or EEA national for a permanent exercise (LE)

#### Competent authority

The territorially competent CLAC decides on the issuance of the business card as long as the national meets the conditions of attribution.

#### Supporting documents

To obtain the business card, the national sends a complete file by mail to the territorially competent CLAC. This file must include the following supporting documents:

- The completed and signed form;
- A photocopy of a national's ID
- A certificate of employment issued by the national's employer or future employer;
- proof of professional fitness that can be:- a professional certificate registered in the national directory of professional certifications,
  - a certificate of professional qualification developed by its professional branch,
  - a certificate of competency or training certificate issued by an EU or EEA state that regulates ARP activity on its territory, including the details and duration of the modules of the training taken,
  - proof by any means that the national has been in this activity for one year full-time or part-time in the last ten years, when neither professional activity nor training is regulated in the EU or EEA State.

**What to know**

Supporting documents must be written in French or translated by a certified translator, if necessary.

#### Duration and renewal

The business card is issued in the dematerialized form of a registration number and remains valid for five years. Any change in employment status will have to be notified to the CLAC but does not result in the mandatory renewal of the card. At the end of these five years, the professional will be able to apply for renewal three months before the expiry date provided that he submits a certificate of continuing education (see supra "4o). (a) Obligation to undergo continuing vocational training").

#### Outcome of the procedure

Once the national has obtained the registration number of the CLAC territorially competent, he will have to pass it on to his employer who will issue him the final professional card.

#### Good to know: compensation measures

In order to carry out his activity in France or to enter the profession, the national may be required to submit to a compensation measure, which may be:

- an adaptation course of up to three years
- an aptitude test carried out within six months of notification to the person concerned.

*For further information*: Articles L. 622-19 and R. 622-22 of the Homeland Security Code.

#### Ways and deadlines for appeal

The applicant may challenge the refusal to issue the business card within two months of notification of the refusal decision by forming a mandatory pre-administrative appeal with the National Accreditation and control of the National Council of Private Security Activities (Cnaps), located 2-4-6, Boulevard Poissonnière, 75009 Paris.

The National Commission will rule on the basis of the factual and legal status prevailing on the date of its decision.

This remedy is obligatory before any litigation. It's free.

Litigation may be exercised before the administrative court of the applicant's place of residence or with the administrative court of Paris for applicants who have their place of residence abroad within two months of notification. express decision by the National Accreditation and Control Commission, i.e. the acquisition of the implicit decision to reject the silence kept by the National Accreditation and Control Commission for two months from the date of receipt of the mandatory prior administrative remedy.

*For further information*: Article L. 633-3 of the Code of Homeland Security; Articles L. 412-1 to L. 412-8 of the Code of Public-Government Relations; Articles R. 421-1 to R. 421-7 of the Code of Administrative Justice.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

