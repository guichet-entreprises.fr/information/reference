﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP266" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Security" -->
<!-- var(title)="Gunsmith" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="security" -->
<!-- var(title-short)="gunsmith" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/security/gunsmith.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="gunsmith" -->
<!-- var(translation)="None" -->

# Gunsmith

Latest update: <!-- begin-var(last-update) -->2020-12<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of the activity

The armourer's activity consists, mainly or incidentally, either in the manufacture, trade, exchange, rental, hire-purchase, loan, modification, repair or transformation, or in negotiation or the organization of operations for the purchase, sale, supply or transfer of arms, ammunition or their components.

*To go further*: Article L. 313-2 of the Internal Security Code and Article L. 2332-1 of the Defense Code.

## 2°. Professional qualifications

### a. National requirements
 
#### National legislation

People wishing to exercise the activity of gunsmith must have a document establishing their professional skills.

This document establishing the professional skills of the person concerned consists of the presentation of one of the following diplomas or titles:

* either a diploma issued by France or a diploma or equivalent title issued by another Member State of the European Union or by another State party to the agreement on the European Economic Area, attesting to professional competence in the arms industry or armament;
* or the professional qualification certificate drawn up by the professional branch of the armory and approved by order of the Minister of the Interior;
* or, for the manager of the company, a level IV diploma issued by France, by another Member State of the European Union or by another State party to the Agreement on the European Economic Area, or any document justifying professional experience of at least six years in the arms industry. In the latter case, each of the company's establishments must include in its staff at least one employee holding one of the previous diplomas, titles or qualification certificates;
* or, for the non-retail trade, of arms, ammunition and their components of categories A1 ° and B, one of the previous diplomas, titles or certificates of qualification or any document justifying professional experience of at least ten years in the arms industry.

For nationals of a member state of the European Union other than France or of another state party to the agreement on the European Economic Area, failing to produce one of the previous diplomas or titles, the interested parties must produce a document establishing their professional capacity consisting of a copy of the license or equivalent title issued by the administrative authority of that State and justifying the capacity to exercise the profession of gunsmith.

#### Training

The CAP (certificate of professional aptitude) is a national vocational diploma at level V (i.e. corresponding to a general and technological second cycle exit before the final year) issued by the Ministry of Education national. It is prepared in 2 years after the 3rd, full time or as an apprenticeship. Depending on the profile and the needs of the student, the diploma can also be prepared in 1, 2 or 3 years. Professional integration diploma, the CAP trains professional techniques and know-how.

The BMA (patent of crafts) is a French national diploma of secondary studies and vocational education level IV issued by the Ministry of National Education which aims to promote innovation, to preserve and transmit traditional techniques. It is registered in the National Directory of Professional Certifications.

The CQP (professional qualification certificate) is a qualification certificate drawn up by the professional branch of the armory which must comply with specifications. It is approved by decree of the Minister of the Interior if it respects these specifications.

*To go further*: article R. 313-4 of the Internal Security Code.

#### Associated costs

The training is most often free when the diploma is prepared in a local public educational establishment. For more details, it is advisable to refer to the training center in question.

The training leading to obtaining the professional qualification certificate is chargeable. For more information, it is advisable to contact the training organizations providing it.

*To go further*: Articles R. 313-3 and R. 313-33 of the Internal Security Code.

### b. EU nationals: for a temporary and occasional exercise (freedom to provide services)

There are no provisions for a one-off or occasional activity insofar as it is not planned to issue prefectural approvals or ministerial authorizations for short periods, except in exceptional circumstances.

### c. EU nationals: for permanent exercise (Freedom of establishment)

#### EU national already established in another Member State

Failing to produce one of the diplomas or titles required by the regulations, the interested parties must produce a document establishing their professional capacity consisting of a copy of the approval or equivalent title issued by the administrative authority of that State and justifying the ability to exercise the profession of gunsmith.

#### EU national without establishment in another Member State: general system or recognition of professional experience or automatic recognition or special cases

Any national of an EU or EEA state who is established and legally exercises the activity of a gunsmith in that State can exercise the same activity in France on a permanent basis.

##### For armourer approvals of weapons, ammunition and their components of categories C and D

In order to obtain the recognition of their professional qualifications, nationals of a Member State of the European Union other than France or of another State party to the European Economic Area should address the prefect of the department of their domicile or from the place where they plan to carry out their activity, a file including the documents attesting to their competence or professional qualification.

The Prefect acknowledges receipt within one month of receipt and informs the applicant, if applicable, of any missing document. He shall notify his duly motivated decision no later than three months after receipt of a complete file.

##### For authorizations for the manufacture, trade and intermediation of arms, ammunition and their components of categories A1 ° and B

In order to obtain the recognition of their professional qualifications, nationals of a Member State of the European Union other than France or of another State party to the European Economic Area send the Minister of the Interior a file including documents certifying their competence or professional qualification.

The Minister acknowledges receipt within one month of receipt and informs the applicant, if applicable, of any missing document. He shall notify his duly motivated decision no later than three months after receipt of a complete file.

*To go further*: Articles R. 313-3-1 and R. 313-33-1 of the Internal Security Code.

## 3 °. Conditions of good repute

### For armourers' approvals of weapons, ammunition and their components of categories C and D

The applicant must produce one or more documents establishing his good repute and consisting of:

* a declaration on the applicant's honor that he is not subject to any prohibition from exercising a commercial profession;
* for foreign nationals, a document equivalent to bulletin n ° 2 of the criminal record.

Any document written in a foreign language is accompanied by its translation into French.

Approval as a gunsmith may be refused when the applicant has been sentenced to a term of imprisonment with or without suspension of more than three months, entered in his criminal record or, for foreign nationals, in a document equivalent to bulletin no. 2 of the criminal record.

The gunsmith's approval is refused when the applicant has been or has been:

* a decision prohibiting the acquisition and possession of weapons which has become final;
* a ban on carrying on a commercial activity;
* in a State other than France, measures equivalent to those above.

### For authorizations for the manufacture, trade and intermediation of arms, ammunition and their components of categories A1 ° and B

Authorization may be refused when the applicant, a natural person, has been or is the subject:

* a decision prohibiting the acquisition and possession of weapons which has become final;
* a ban on carrying on a commercial activity;
* in a State other than France of measures equivalent to the previous measures.

The authorization may also be refused when the applicant or a person belonging to the supervisory bodies in the applicant company or economic interest group or exercising a function of administrator, management or management therein has been sentenced to a penalty of '' imprisonment with or without suspension of more than three months, appearing on bulletin n ° 2 of his criminal record or in an equivalent document for nationals of a Member State of the European Union or of another State party to the agreement on the European Economic Area.

## 4°. Incompatibilities

### For armourer approvals of weapons, ammunition and their components of categories C and D

The gunsmith's approval is refused when the applicant:

* is subject to a legal protection measure;
* has been or has been admitted to psychiatric care;
* has been or is hospitalized without his consent due to mental disorder;
* has a mental state manifestly incompatible with the possession of a weapon.

### For authorizations for the manufacture, trade and intermediation of arms, ammunition and their components of categories A1 ° and B

Authorization is refused when the applicant:

* is subject to a legal protection measure;
* has been or has been admitted to psychiatric care;
* has been or is hospitalized without his consent due to mental disorder;
* has a mental state manifestly incompatible with the possession of a weapon.

*To go further*: Articles R. 313-6 and R. 313-29 of the Internal Security Code.

## 5°. Procedures and formalities

### a. For armourers' approvals of weapons, ammunition and their components of categories C and D

The request is addressed to the prefect of the establishment's location or, failing that, the domicile of the applicant. A receipt is issued.

**Note**: The request for approval is submitted by the person who exercises the activity of gunsmith. If it is a legal person, it is presented by its legal representative and the authorization is issued to the latter.

The following documents are attached to the approval request:

* a document establishing the civil status of the person concerned as well as an extract of the birth certificate with marginal information dated less than three months;
* a document establishing the professional skills of the person concerned;
* one or more documents establishing the good repute of the applicant and consisting of:
  * a declaration on the applicant's honor that he is not prohibited from exercising a commercial profession,
  * for foreign nationals, a document equivalent to bulletin n ° 2 of the criminal record.

Any document written in a foreign language is accompanied by its translation into French.

*To go further*: Articles R. 313-1 and R. 313-3 of the Internal Security Code.

## b. For authorizations for the manufacture, trade and intermediation of arms, ammunition and their elements of categories A1 ° and B

Applications for authorization to manufacture or trade or intermediation of arms, ammunition and their elements of categories A1 ° and B are addressed to the Minister of the Interior - general secretariat - central arms service - Place Beauvau - 75800 Paris cedex 08.

### When the applicant is a natural person, this one must use the [form 15694 * 02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R47730).

### When the applicant is a legal person, he must use the [form 15693 * 02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R47729).

The following information is attached to the authorization request:

* for sole proprietorships:
  * proof of the nationality of the applicant;
* for partnerships:
  * names of all partners in name, general partners, sponsors and managers,
  * proof of the nationality of these persons;
* for joint stock companies and limited liability companies:
  * names of managers, general partners, members of the board of directors, of the management board or of the supervisory board,
  * proof of the nationality of these people, information concerning the nationality of shareholders or holders of shares and the share of the capital held by French citizens,
  * form of the securities of joint stock companies;
* for economic interest groups:
  * name of the administrator (s),
  * in the event of incorporation with capital, information concerning the nationality of holders of capital shares and the share of capital held by French holders;
* an extract of the birth certificate with marginal information dated less than three months for the applicant and for each person exercising, in the applicant company or economic interest group, a management or management function;
* where applicable, nature of the manufacturing carried out for State services and summary indication of their importance;
* nature of the activity or activities carried out;
* a document establishing the professional skills of the applicant;
* the national identity card and, for foreigners, the passport or residence permit are proof of the nationality of the applicant.

## 6°. Remedies

### National

#### For armourers' approvals of weapons, ammunition and their components of categories C and D

The applicant can contest the refusal to issue a gunsmith's license by the department of the prefecture which processes the file within two months from the notification of the refusal decision by:

* a free appeal, addressed to the department of the prefecture which handles the file;
* a hierarchical appeal, addressed to the Minister of the Interior - general secretary - central service of arms - Place Beauvau - 75800 Paris cedex 08;
* a contentious appeal, addressed to the administrative court address of the court. The administrative court can be referred by the computer application "Citizen Telerecours" accessible through the website [telerecours.fr](www.telerecours.fr).

This judicial appeal must be filed at the latest before the expiration of the second month following the date of notification of the contested decision (or the second month following the date of the rejection of your gracious or hierarchical appeal).

*To go further*: Articles R. 313.1 to R. 313-7-1 of the Internal Security Code; Articles R. 421-1 to R. 421-7 of the Code of Administrative Justice.

#### For authorizations for the manufacture, trade and intermediation of arms, ammunition and their components of categories A1 ° and B

The applicant may contest the refusal to issue the manufacturing, trade and intermediation authorization by the Minister of the Interior within two months from the notification of the refusal decision by:

* a gracious appeal, addressed to the Minister of the Interior - general secretary - central service of arms - Place Beauvau - 75800 Paris cedex 08;
* a contentious appeal, addressed to the administrative court [address of the court]. The administrative court can be referred by the computer application "Citizen Telerecours" accessible through the website [telerecours.fr](www.telerecours.fr).

This judicial appeal must be filed at the latest before the expiration of the second month following the date of notification of the contested decision (or the second month following the date of the rejection of your gracious or hierarchical appeal).

*To go further*: Articles R. 313.28 to R. 313-38-2 of the Internal Security Code; Articles R. 421-1 to R. 421-7 of the Code of Administrative Justice.

### French help center

The ENIC-NARIC Center is the French information center on the academic and professional recognition of diplomas.

### SOLVIT

SOLVIT is a service provided by the National Administration of each EU Member State or party to the EEA Agreement. Its objective is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT is particularly involved in the recognition of professional qualifications.

**Conditions**

The interested party can only use SOLVIT if he establishes:

* that the public administration of one EU state has failed to respect its rights under EU law as a citizen or business of another EU state;
* that he has not already initiated legal action (administrative appeal is not considered to be such).

**Procedure**

The national must complete an online complaint form.

Once his file has been transmitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem falls within its competence.

**Vouchers**

To enter SOLVIT, the national must communicate:

* his full contact details;
* detailed description of their problem;
* all the evidence in the case (for example, correspondence and decisions received from the administrative authority concerned).

**Time limit**

SOLVIT undertakes to find a solution within ten weeks of the day on which the case is taken over by the SOLVIT center in the country in which the problem arose.

**Costs:** free.

At the end of the ten week period, SOLVIT presents a solution:

* if this solution settles the dispute concerning the application of European law, the solution is accepted and the case is closed;
* If there is no solution, the case is closed as unresolved and referred to the European Commission.

**Additional Information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official site](https://sgae.gouv.fr/sites/SGAE/accueil.html)).