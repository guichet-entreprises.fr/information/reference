﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP281" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Pilote de dirigeable" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="pilote-de-dirigeable" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/pilote-de-dirigeable.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="pilote-de-dirigeable" -->
<!-- var(translation)="None" -->

# Pilote de dirigeable

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

Pilote de dirigeable contre rémunération, soit dans des opérations de transport aérien commercial, soit dans des opérations de travail aérien.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'activité est soumise à une règlementation européenne (Règlement (UE) n° 1178/2011 de la Commission du 3 novembre 2011 déterminant les exigences techniques et les procédures administratives applicables au personnel navigant de l'aviation civile conformément au règlement (CE) n° 216/2008 du Parlement européen et du Conseil). Pour mémoire, les exigences fixées par la Commission européenne requièrent la détention d'une licence de pilote commercial ou de pilote de ligne, des qualifications associées en cours de validité, d'un certificat médical de classe 1, et le cas échéant d'un certificat d'aptitude linguistique.

#### Formation

Les exigences fixées par la Commission européenne requièrent des candidats à une licence de pilote commercial ou de pilote de ligne de suivre une formation conforme à l'appendice 3 de l'annexe 1 du règlement, auprès d'un organisme de formation approuvé. Les formations relatives aux qualifications sont conformes aux exigences fixées dans les sous-parties G et H de l'annexe 1 du règlement ainsi qu'à l'appendice 6 de la même annexe.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

La reconnaissance des titres de pilotes délivrés dans l'Union européenne est automatique. La reconnaissance des titres délivrés hors de l'Union européenne est soumise aux exigences relatives à l'acceptation de la certification des pilotes prévues par le Règlement Délégué (UE) 2020/723 de la Commission du 4 mars 2020 établissant des règles détaillées concernant l'acceptation de la certification des pilotes par les pays tiers et modifiant le règlement (UE) n° 1178/2011.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

La reconnaissance des titres de pilotes délivrés dans l'Union européenne est automatique. La reconnaissance des titres délivrés hors de l'Union européenne est soumise aux exigences relatives à l'acceptation de la certification des pilotes prévues par le Règlement Délégué (UE) 2020/723 de la Commission du 4 mars 2020 établissant des règles détaillées concernant l'acceptation de la certification des pilotes par les pays tiers et modifiant le règlement (UE) n° 1178/2011.

## 3°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable

Licence conforme au règlement 1178/2011.

### b. Autorité compétente

Direction générale de l'aviation civile.

## 4°. Textes de référence

- Réglement n° 1178/2011 de la Commission du 3 novembre 2011 déterminant les exigences techniques et les procédures administratives applicables au personnel navigant de l'aviation civile conformément au règlement (CE) n° 216/2008 du Parlement européen et du Conseil.
- Réglement Délégué (UE) 2020/723 de la Commission du 4 mars 2020 établissant des règles détaillées concernant l'acceptation de la certification des pilotes par les pays tiers et modifiant le règlement (UE) n° 1178/2011.