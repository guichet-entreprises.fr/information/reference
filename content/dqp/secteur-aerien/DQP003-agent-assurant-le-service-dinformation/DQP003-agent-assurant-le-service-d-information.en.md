﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP003" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Aviation sector" -->
<!-- var(title)="Aerodrome flight information service officer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="aviation-sector" -->
<!-- var(title-short)="aerodrome-flight-information-service-officer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/aviation-sector/aerodrome-flight-information-service-officer.html" -->
<!-- var(last-update)="2020-04-15 17:22:14" -->
<!-- var(url-name)="aerodrome-flight-information-service-officer" -->
<!-- var(translation)="Auto" -->


Aerodrome flight information service officer
====================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The flight and alert information officer or "AFIS agent" (*Aerodrome Flight Information Service*) is a professional who operates within an airfield.

As such, its main missions are:

- Provide pilots with information on weather parameters, air traffic or the configuration of the airfield and runways;
- to provide an accident alert and to initiate emergency processes in the event of an incident.

It may also have a role in relations with civil aviation officials, and must ensure that it conveys a good image of the airfield on which it is located.

**What to know**

The AFIS agent has to differentiate from the controller: he does not issue any authorization to take off or land, his role being exclusively informative.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The AFIS profession is subject to the issuance by the Directorate of Civil Aviation Safety (DSAC) of a qualification known as "AFIS qualification" attesting to the acquisition of theoretical and practical knowledge, and bearing, if necessary, the "English AFIS." This qualification allows the holder to practice on a specific airfield.

*To go further* Article R. 135-8 of the Civil Aviation Code, and order of 16 July 2007 relating to the qualification and training of AFIS personnel.

#### Training

The AFIS qualification is obtained if the candidate has:

- earned an "AFIS" designation when required;
- followed training, which was the subject of an initial theoretical assessment and then local theoretical and practical training conducted directly in an airfield and the subject of an evaluation.

**Initial theoretical training**

If he justifies having taken training whose program is established in Appendix 1 of the decree of 16 July 2007, any candidate, aged at least 18 years, can take the initial theoretical assessment test.

Once registered on the[OCEANE platform](https://www.ecologique-solidaire.gouv.fr/sites/default/files/Guide_detaille_inscription_QCM_OCEANE.pdf), the candidate will be able to go to an examination centre to take the initial theoretical assessment test. The candidate will be subjected to a multiple-choice questionnaire (MQ) and will have to correctly answer a minimum of 48 out of 60 questions to validate the test.

The certificate of success issued by the DSAC is valid for one year and allows access to local theoretical and practical training.

*To go further* Articles 6 and 8 of the[decreed from 16 July 2007](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000022270822) qualification and training of AFIS personnel.

**Local theoretical and practical training**

Lasting from two to twenty-four weeks depending on the candidate, the local theoretical training includes a curriculum identical to that of the initial theoretical training, but adapted to the local environment of the airfield in which the candidate wants to evolve. Local theoretical and practical training also covers a complementary training programme set out in Schedule 2 of the 16 July 2007 decree.

Its evaluation, for the local theoretical assessment, is done in written form and may also be accompanied by an oral test, at the evaluator's choice. The local practical assessment is carried out on site.

After successfully performing the local theoretical and practical assessment, the candidate is awarded a certificate of success. This certificate allows him to carry out his duties for two months, pending the issuance of the AFIS qualification by the DSAC.

*To go further* Articles 7 and 9 of the July 16, 2007 order.

**Renewal**

The AFIS qualification is valid for three years. The applicant will be able to apply for renewal by submitting a written application to the territorially competent DSAC.

This request should include:

- a certificate from the AFIS provider proving that the person concerned did perform at least 24 hours or four vacations in the three months preceding the application;
- a certificate from the AFIS provider proving that the person concerned has completed continuing vocational training (see below. "4th Continuing Vocational Training").

**English mention AFIS**

In addition to the success of the various theoretical and practical tests referred to above, the AFIS officer applying for the AFIS qualification must be marked "English AFIS" when the flight and alert information service is provided in English language on the airfield where he intends to exercise.

This may be on the qualification when the officer meets the following conditions:

- have a certificate of English language proficiency[Level B1](https://www.coe.int/fr/web/common-european-framework-reference-languages/table-1-cefr-3.3-common-reference-levels-global-scale) The Common European Framework for Languages (CECRL);
- have undergone training on aeronautical themes and phraseology (all typical turns in a given environment) in accordance with the themes in Appendix 7 to the 16 July 2007 decree.

**What to know**

This is only valid for a period of three years. At each renewal, the AFIS officer will be required to make a written request to the DSAC along with the following documents:

- A B1-level English language proficiency certificate issued within six months of renewal;
- a certificate of training on aeronautical themes and phraseology issued by an AFIS provider.

*To go further* Article 2-1 and Appendixes 5, 6 and 7 of the July 16, 2007 Order.

#### Costs associated with qualification

The training costs for AFIS officers vary by training organization. For more information, it is advisable to get close to one of them.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or party to the European Economic Area, which is established and legally practises the activity of AFIS agent in that state, may carry out the same activity in France, on a temporary and occasional basis.

He must first request it by written declaration addressed to the DSAC, 50 Henry Farman Street, 75720 Paris Cedex 15 (see infra "5°. a. Obtain a licence to practise for the national for a temporary or casual exercise (LPS)").

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the state in which the professional is legally established, he must have carried out this activity for at least one year, in the course of the ten years before the benefit, in one or more EU states.

Where there are substantial differences between the professional qualification of the national and the training in France, the competent DSAC may require that the person concerned submit to an aptitude test.

*To go further* paragraph IV of Article 2 of Article 2 of the Order of 16 July 2007.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

To practise as an AFIS agent in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- Hold a certificate of competency or training certificate required for the profession of AFIS agent in an EU or EEA state when regulating the profession on its territory;
- where the EU or EEA State does not regulate this profession:- have been an AFIS agent for at least one year, full-time or part-time, for the past ten years,
  - have a certificate of competency or proof of training title justifying that the national has prepared the AFIS agent exercise.

Once the national fulfils one of these conditions, he or she will be able to apply for the AFIS recognition from the DSAC, 50 rue Henry Farman, 75720 Paris Cedex 15 (see below "5o. b. Obtain the AFIS qualification for the national for a permanent exercise (LE)).

Where there are substantial differences between the professional qualification of the national and the training in France, the competent DSAC may require that the person concerned submit to compensation measures (see infra 5. a. Good to know: measures of compensation").

*To go further* paragraph I to III of Article 2 of the Order of 16 July 2007.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

In the event of serious impact or negligence in the performance of his duties, the AFIS officer may have his AFIS qualification suspended or withdrawn, respectively, with or without formality, when the emergency and safety require it.

*To go further* Article R.135-8 of the Civil Aviation Code.

4°. Continuous vocational training
------------------------------------------------------

Continuous vocational training lasting 6 hours per year must be followed by the AFIS agent. It allows the professional to regularly update his knowledge of the new regulations and his skills, as well as to maintain his level of B1 language skills required for the "English AFIS" designation.

At the end of each continuing training, the AFIS officer is provided with a certificate justifying that he has successfully completed the skills maintenance session.

In any event, each AFIS officer will have to be able to justify, throughout his career, the follow-up of this annual continuing education by presenting an up-to-date training booklet.

In addition, where English mentions are required, the applicant must maintain a minimum of level B1 and undergo ongoing training in his or her knowledge of phraseology and aeronautical themes in Schedule 7 of the 16th order. July 2007.

*To go further* Article 11 and 11-1 of the Order of 16 July 2017.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obtain a licence to practise for the national for a temporary or casual exercise (LPS)

**Competent authority**

The Civil Aviation Safety Directorate has the authority to decide on the application for authorization to perform the service.

**Supporting documents**

In support of his application for leave, the national submits to the territorially competent DSAC a file containing the following supporting documents:

- a certificate of recognition of its competence, when the profession is regulated in the EU state or the EEA in which it is established;
- a certificate justifying his activity for at least one year, in the last ten years, when neither the training nor the practice of the profession is regulated in his state;
- if necessary, a certificate of language proficiency (see above "2. b. English mention AFIS").

**Timeframe**

Once the file is received, the competent authority will have one month to make its decision, which may be:

- allow the national to provide his or her first service;
- request more information. In this case, he will have two months after sending these documents to decide;
- in case of substantial differences between the training of the national and that in France, and given the implications for public safety, to submit him to an aptitude test within one month of his decision. If the person is successful, the competent authority will issue him the AFIS qualification to carry out the performance;
- not to authorize the delivery.

*To go further* paragraph IV of Article 2 of Article 2 of the Order of 16 July 2007.

### b. Obtain AFIS qualification for the national for a permanent exercise (LE)

**Competent authority**

The Civil Aviation Safety Directorate has the authority to decide on the AFIS qualification application.

**Supporting documents**

In support of his application for qualification, the national submits a file to the DSAC containing the following supporting documents:

- A certificate of recognition of one's competence when the profession is regulated in the EU or EEA State;
- a certificate justifying its activity for at least one year in the last ten years, when neither training nor the practice of the profession is regulated in the Member State;
- if necessary, a certificate of language proficiency (see above "2. b. English mention AFIS").

**Outcome of the procedure**

Upon receipt of all the elements of the file, the DSAC will be able to decide on the issuance of the AFIS qualification or, if necessary, on the implementation of compensation measures by the person concerned.

Once the AFIS qualification is granted, the national will be able to practise as an AFIS agent in a specific airfield.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice, which may be:

- an adaptation course, sometimes with additional training, which will be evaluated at the end of the project;
- an aptitude test carried out within six months of notification to the person concerned.

*To go further* paragraphs II and III of Article 2.3 and Articles 4, 5 and 11 of the July 16, 2007 order.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

