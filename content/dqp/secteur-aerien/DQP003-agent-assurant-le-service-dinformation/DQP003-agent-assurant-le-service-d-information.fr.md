﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP003" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Agent assurant le service d'information de vol et d'alerte" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="agent-assurant-le-service-d-information" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/agent-assurant-le-service-d-information-de-vol-et-d-alerte.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="agent-assurant-le-service-d-information-de-vol-et-d-alerte" -->
<!-- var(translation)="None" -->

# Agent assurant le service d'information de vol et d'alerte

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

L'agent assurant le service d'information de vol et d'alerte ou « agent AFIS » (*Aerodrome Flight Information Service*) est un professionnel qui intervient au sein même d'un aérodrome.

À ce titre, ses missions principales sont :

- de fournir aux pilotes les services d'information relatifs aux paramètres météorologiques, au trafic aérien ou encore à la configuration de l'aérodrome et de ses pistes ;
- d'assurer l'alerte en cas d'accident et de déclencher les processus d'urgence en cas d'incidents.

Il peut également avoir un rôle dans les relations avec les représentants de l'aviation civile, et doit veiller à véhiculer une bonne image de l'aérodrome sur lequel il se trouve.

**À savoir**

L'agent AFIS est à différencier du contrôleur : il ne délivre aucune autorisation de décoller ou d'atterrir, son rôle étant exclusivement informatif.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La profession d'agent AFIS est soumise à la délivrance par la Direction de la sécurité de l’aviation civile (DSAC) d'une qualification dite « qualification AFIS » attestant l'acquisition de connaissances théoriques et pratiques, et portant, le cas échéant, la mention « anglais AFIS ». Cette qualification permet à son titulaire d'exercer sur un aérodrome déterminé.

*Pour aller plus loin* : article R. 135-8 du Code de l'aviation civile, et arrêté du 16 juillet 2007 relatif à la qualification et à la formation des personnels AFIS.

#### Formation

La qualification AFIS s’obtient dès lors que le candidat a :

- obtenu la mention « anglais AFIS » lorsqu’elle est requise ;
- suivi une formation objet d'une première évaluation théorique initiale puis d'une formation théorique et pratique locale effectuée directement dans un aérodrome et objet d’une évaluation.

##### Formation théorique initiale

Dès lors qu'il justifie avoir suivi une formation dont le programme est établi en annexe 1 de l'arrêté du 16 juillet 2007, tout candidat, âgé d'au moins 18 ans, peut se présenter à l'épreuve d'évaluation théorique initiale .

Une fois inscrit sur la [plateforme OCEANE](https://www.ecologique-solidaire.gouv.fr/sites/default/files/Guide_detaille_inscription_QCM_OCEANE.pdf), le candidat pourra se rendre dans un centre d'examen pour passer l'épreuve d'évaluation théorique initiale. Le candidat sera soumis à un questionnaire à choix multiple (QCM) et devra répondre correctement à un minimum de 48 questions sur 60 pour valider l'épreuve.

L'attestation de réussite délivrée par la DSAC est valable pour un an et permet d'accéder à la formation théorique et pratique locale.

*Pour aller plus loin* : articles 6 et 8 de l'arrêté du 16 juillet 2007 relatif à la qualification et à la formation des personnels AFIS.

##### Formation théorique et pratique locale

D'une durée de deux à vingt-quatre semaines selon le candidat, la formation théorique locale comprend un programme d'enseignement identique à celui de la formation théorique initiale, mais adapté à l'environnement local de l'aérodrome dans lequel le candidat souhaite évoluer. La formation théorique et pratique locale porte également sur un programme de formation complémentaire fixé par l’annexe 2 de l’arrêté du 16 juillet 2007.

Son évaluation, pour l’évaluation théorique locale, est faite sous forme écrite et pourra également être accompagnée d'une épreuve orale, au choix de l'évaluateur. L’évaluation pratique locale est effectuée sur site.

Après réussite à l’évaluation théorique et pratique locale, le candidat se voit délivrer une attestation de réussite. Cette attestation lui permet d’exercer ses fonctions pendant deux mois, dans l’attente de la délivrance de la qualification AFIS par la DSAC.

*Pour aller plus loin* : articles 7 et 9 de l'arrêté du 16 juillet 2007.

##### Renouvellement

La qualification AFIS est valable pendant trois ans. Le candidat pourra demander son renouvellement en transmettant une demande écrite à la DSAC territorialement compétente.

Cette demande devra contenir les éléments suivants :

- une attestation du prestataire AFIS prouvant que l'intéressé a bien effectué au moins 24 heures ou quatre vacations dans les trois mois précédents la demande ;
- une attestation du prestataire AFIS prouvant que l'intéressé a bien suivi la formation professionnelle continue (cf. infra. « 4° Formation professionnelle continue »).

##### Mention anglais AFIS

Outre la réussite aux différentes épreuves théoriques et pratiques visées ci-dessus, l'agent AFIS qui demande la délivrance de la qualification AFIS, doit bénéficier de la mention « anglais AFIS » lorsque le service d’information de vol et d’alerte est fourni en langue anglaise sur l’aérodrome où il entend exercer.

Cette mention peut être apposée sur la qualification lorsque l'agent remplit les conditions suivantes :

- être titulaire d'une attestation de compétences linguistiques en anglais de [niveau B1](https://www.coe.int/fr/web/common-european-framework-reference-languages/table-1-cefr-3.3-common-reference-levels-global-scale) au Cadre européen commun de référence pour les langues (CECRL) ;
- avoir suivi une formation portant sur les thèmes et la phraséologie (ensemble des tournures typiques à un milieu donné) aéronautiques conformément aux thèmes figurant en annexe 7 à l’arrêté du 16 juillet 2007.

**À savoir**

Cette mention n'est valable que pour une période de trois ans. À chaque renouvellement, l'agent AFIS devra en faire la demande écrite auprès de la DSAC accompagné des documents suivants :

- une attestation de compétences linguistiques en anglais de niveau B1 délivrée dans les six mois précédents le renouvellement ;
- une attestation de formation portant sur les thèmes et la phraséologie aéronautiques délivrée par un prestataire AFIS.

*Pour aller plus loin* : article 2-1 et annexes 5, 6 et 7 de l'arrêté du 16 juillet 2007.

#### Coûts associés à la qualification

Les frais de formation des agents AFIS varient selon les organismes de formation. Pour plus d'information, il est conseillé de se rapprocher de l'un d'eux.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union Européenne (UE) ou partie à l'Espace Économique Européen, qui est établi et exerce légalement l'activité d'agent AFIS dans cet État, peut exercer en France la même activité, de manière temporaire et occasionnelle.

Il devra, au préalable, en faire la demande par déclaration écrite adressée à la DSAC, 50, rue Henry Farman, 75720 Paris Cedex 15 (cf. infra « 5°. a. Obtenir une autorisation d'exercer pour le ressortissant en vue d'un exercice temporaire ou occasionnel (LPS) »).

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans l'État dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États de l'UE.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation en France, la DSAC compétente peut exiger que l'intéressé se soumette à une épreuve d'aptitude.

*Pour aller plus loin* : paragraphe IV du 2.3 de l'article 2 de l'arrêté du 16 juillet 2007.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Pour exercer la profession d'agent AFIS en France à titre permanent, le ressortissant de l'État de l’UE ou de l’EEE devra remplir l'une des conditions suivantes :

- être titulaire d'une attestation de compétences ou d'un titre de formation requis pour l'exercice de la profession d'agent AFIS dans un État de l’UE ou de l’EEE lorsqu'il réglemente la profession sur son territoire ;
- lorsque l'État de l’UE ou de l’EEE ne réglemente pas cette profession :
  - avoir exercé l'activité d'agent AFIS pendant au moins un an, à temps plein ou à temps partiel, au cours des dix dernières années,
  - disposer d'attestation de compétences ou de preuves de titre de formation justifiant que le ressortissant a préparé l'exercice d'agent AFIS.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander la délivrance de la reconnaissance AFIS auprès de la DSAC, 50 rue Henry Farman, 75720 Paris Cedex 15 (cf. infra « 5°. b. Obtenir la qualification AFIS pour le ressortissant en vue d'un exercice permanent (LE) »).

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation en France, la DSAC compétente peut exiger que l'intéressé se soumette à des mesures de compensation (cf. infra 5°. a. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : paragraphe I à III de l'article 2 de l'arrêté du 16 juillet 2007.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

En cas d'incidence ou de négligence grave dans l'exercice de ses fonctions, l'agent AFIS peut voir sa qualification AFIS respectivement suspendue ou retirée, avec ou sans formalité, lorsque l'urgence et la sécurité l'obligent.

*Pour aller plus loin* : article R.135-8 du Code de l'aviation civile.

## 4°. Formation professionnelle continue

Une formation professionnelle continue d'une durée de 6 heures par an doit obligatoirement être suivie par l'agent AFIS. Elle permet au professionnel de régulièrement mettre à jour ses connaissances des nouvelles réglementations et ses compétences, ainsi que de maintenir son niveau de compétences linguistiques B1 requis pour la mention « anglais AFIS ».

À l'issue de chaque formation continue, l'agent AFIS se voit remettre une attestation justifiant qu’il a suivi avec succès la séance de maintien des compétences.

En tout état de cause, chaque agent AFIS devra être en mesure de justifier, tout au long de sa carrière, du suivi de cette formation continue annuelle en présentant un livret de formation à jour.

De plus, dans les cas où la mention anglais est requise, le candidat doit maintenir au minimum le niveau B1 et suivre une formation continue concernant ses connaissances relative à la phraséologie et aux thèmes aéronautiques figurant à l’annexe 7 de l’arrêté du 16 juillet 2007.

*Pour aller plus loin* : article 11 et 11-1 de l'arrêté du 16 juillet 2017.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Obtenir une autorisation d'exercer pour le ressortissant en vue d'un exercice temporaire ou occasionnel (LPS)

#### Autorité compétente

La direction de la sécurité de l’aviation civile est compétente pour se prononcer sur la demande d'autorisation d'exercer la prestation.

#### Pièces justificatives

À l'appui de sa demande d'autorisation, le ressortissant transmet à la DSAC territorialement compétente un dossier comportant les pièces justificatives suivantes :

- une attestation de reconnaissance de ses compétences, lorsque la profession est réglementée dans l’État de l'UE ou de l'EEE dans lequel il est établi ;
- une attestation justifiant son activité pendant au moins un an, au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans son État ;
- le cas échéant, une attestation de compétences linguistiques (cf. supra « 2°. b. Mention anglais AFIS »).

#### Délai

À compter de la réception du dossier, l'autorité compétente disposera d'un délai d'un mois pour rendre sa décision qui pourra être :

- d'autoriser le ressortissant à faire sa première prestation de service ;
- de demander des informations complémentaires. Dans ce cas, il aura deux mois après l'envoi de ces documents pour se prononcer ;
- en cas de différences substantielles entre la formation du ressortissant et celle en France, et compte tenu des implications en matière de sécurité publique, de le soumettre à une épreuve d'aptitude dans le mois qui suit sa décision. Si l'intéressé réussit l'épreuve, l'autorité compétente lui délivrera la qualification AFIS lui permettant de réaliser la prestation ;
- de ne pas autoriser la prestation.

*Pour aller plus loin* : paragraphe IV du 2.3 de l'article 2 de l'arrêté du 16 juillet 2007.

### b. Obtenir la qualification AFIS pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

La direction de la sécurité de l’aviation civile est compétente pour se prononcer sur la demande de qualification AFIS.

#### Pièces justificatives

À l'appui de sa demande de qualification, le ressortissant transmet à la DSAC un dossier comportant les pièces justificatives suivantes :

- une attestation de reconnaissance de ses compétences lorsque la profession est réglementée dans l’État de l'UE ou de l'EEE ;
- une attestation justifiant son activité pendant au moins un an au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre ;
- le cas échéant, une attestation de compétences linguistiques (cf. supra « 2°. b. Mention anglais AFIS »).

#### Issue de la procédure

À réception de tous les éléments du dossier, la DSAC pourra se prononcer sur la délivrance de la qualification AFIS ou, le cas échéant, sur la mise en œuvre de mesures de compensation par l'intéressé.

Une fois que la qualification AFIS est accordée, le ressortissant pourra exercer la profession d'agent AFIS dans un aérodrome déterminé.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de compensation de son choix, qui peut être :

- un stage d'adaptation, avec parfois une formation complémentaire en plus, qui fera l'objet d'une évaluation à son issue ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : paragraphes II et III du 2.3 de l'article 2 et articles 4, 5 et 11 de l'arrêté du 16 juillet 2007.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).