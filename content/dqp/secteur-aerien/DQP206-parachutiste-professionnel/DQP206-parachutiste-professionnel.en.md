﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP206" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Aviation sector" -->
<!-- var(title)="Professional parachutist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="aviation-sector" -->
<!-- var(title-short)="professional-parachutist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/aviation-sector/professional-parachutist.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="professional-parachutist" -->
<!-- var(translation)="Auto" -->


Professional parachutist
========================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The professional paratrooper is a professional aircrew in civil aviation. Its activity consists of falling from a height of a hundred meters to several thousand, usually exiting an aircraft, and then returning to earth with the help of a parachute.

After the outing, the paratrooper is in free fall for a more or less long period depending on the discipline practiced, the height at which he was dropped and the relative altitude of opening. He can perform alone, for example in aerobatics, or with other paratroopers of the figures during the fall before reaching the ground by flying his parachute so as to land at the intended place.

The professional can perform paid demonstration jumps or complete an aerial work mission.

*For further information*: Article L. 6521-1 of the Transportation Code; order of 25 August 1954 relating to the classification of professional aircrew in civil aviation; Article 1 of the order on the creation of a patent and a professional paratrooper licence and an instructor qualification.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

As a professional aircrew in civil aviation, the professional paratrooper is subject to the application of Articles L. 6521-2 of the Transportation Code, which states that they must be:

- holder of a valid aeronautical title;
- registered and in one of three categories:- essays and receptions,
  - Air transport,
  - aerial work.

In order to carry out his activity, the paratrooper must hold the patent and the license corresponding to the nature of the required required planned jump.

**Please note**

Aircrew providing air transport or work services established in a Member State of the European Union (EU) other than France or in a state party to the European Economic Area (EEA) agreement or bilateral agreements the EU with the Swiss Confederation as well as the salaried aircrew of an air transport or work service provider based in one of the aforementioned states, who are temporarily active in France, do not fall within the scope implementation of this obligation. This staff refers to the provisions of the "b. EU nationals: for a temporary and occasional exercise (Freedom to provide services)" and "c. EU nationals: for a permanent exercise (Freedom of establishment)" below.

*For further information*: Articles L. 6521-1 and the following of the Transportation Code; Article 2 of the December 3, 1956 decree on the creation of a professional paratrooper's patent and licence and an instructor qualification.

#### Training

No one may practice skydiving if he is unable to justify that he holds the licence corresponding to the nature of the required planned jump with all necessary qualifications.

The license and qualifications can only be issued to patent holders.

*For further information*: Article 2 of the December 3, 1956 decree on the creation of a professional paratrooper's patent and licence and an instructor qualification.

##### Patent and license of professional paratrooper

The patent is a title sanctioning a set of general theoretical and practical knowledge. It is issued after examination and is definitively acquired from its holder.

The licence is a 12-month title, sanctioning the suitability and right of patent holders to perform the corresponding functions.

It is renewed for a period of the same duration, provided that the applicant has completed twenty jumps in the twelve months preceding the renewal application or five jumps in the six months preceding that application. Only jumps during which the parachutes were used will be taken into account.

Failure to meet these conditions must meet an instructor's check on the practical tests required to issue the licence.

*For further information*: Articles 1, 7 and 9 of the December 3, 1956 order on the creation of a professional paratrooper's patent and licence and an instructor qualification.

###### Prerogatives 

The professional paratrooper licence allows the holder to perform all types of jumps with equipment in accordance with current regulations for remuneration.

*For further information*: Article 9 of the order of 3 December 1956 above.

###### Conditions for issuing the patent and the license

The person concerned must:

- hold a Class 1 medical certificate. For more information on this certificate, it is recommended to refer to the order of 27 January 2005 on the physical and mental fitness of the technical technical aircrew of civil aviation;
- Be eighteen years old;
- or total two hundred and fifty jumps, including at least two hundred jumps during which he used only the controlled aperture device and comprising a minimum of twenty-five free falls of a duration of 30 seconds or more;
- i.e., total two hundred jumps including at least one hundred and fifty jumps during which he used only the controlled aperture device and comprising a minimum of twenty-five free falls of a duration of 30 seconds or more, if he justifies having completely and satisfactorily followed a certified teaching;
- meet the theoretical tests on aerodynamics, parachute construction and equipment, parachute implementation technique and use, meteorology and aviation regulations;
- meet the practical tests: these are in-flight events that include a jumps test and a hardware drop test.

*For further information*: Articles 4 and 9 of the order of 3 December 1956 mentioned above; Article 3 and annex to the April 25, 1962 order on the program and examination regime for the professional paratrooper's patent and licence.

**Good to know**

To obtain the patent and the professional parachutist's license by equivalence, the candidate must:

- Be 18 years old and hold a Class 1 medical certificate;
- Be a holder:- that, for at least twelve months, a state diploma of youth, popular education and sport (DEJEPS) specialty "sports development" mention "parachuting", a professional certificate of youth, popular education and sport ( BPJEPS) specialty "parachuting" or a state certificate of sports educator (BEES) 1st degree, option "parachuting", and having satisfied the theoretical tests of the certificate of professional skydiver,
  - or, from a certificate of instructor to the delayed controlled opening jump issued by the Minister of Defence and justify having carried out full and satisfactory training on civilian aviation regulations provided by the military authority and certified by an organization of the armed forces,
  - either, the certificate of experienced aircrew or paratrooper of tests and receptions issued by the Minister of Defence and justify having carried out a complete and satisfactory training on civil aviation regulations provided by the military authority and attested by an agency of the armed forces;
- 20 jumps in the 12 months prior to the application.

*For further information*: Article 9-2 of the order of 3 December 1956.

##### Instructor Qualification

An instructor qualification is required to empower the holder of the professional paratrooper licence to give or direct the flight training required to obtain this licence.

###### Validity

The instructor qualification is valid for thirty-six months, renewable by period of the same duration, provided that the person concerned justifies having conducted the training of at least fifty jumps during the twenty-four months preceding the application.

If the individual has not conducted this number of jumps, he or she must meet a professional paratrooper instructor's aptitude test for the duties of a professional paratrooper instructor.

###### Conditions

The candidate must meet the following conditions:

- total three hundred and fifty jumps including at least three hundred jumps during which he used only the controlled aperture device and comprising a minimum of forty free falls lasting between 30 and 60 seconds and a minimum of ten free falls lasting more than 60 seconds;
- have completed a satisfactory and complete course as a paratrooper instructor.

*For further information*: Article 10 of the December 3, 1956 order on the creation of a professional paratrooper's patent and licence and an instructor qualification.

**Good to know**

To qualify as a professional parachutist instructor by equivalence, the following requirements must be met:

- Be a valid holder of a professional paratrooper's licence or meet the requirements to obtain the patent and the professional parachutist licence by equivalence;
- Be a holder:- either, for at least twelve months, a speciality DEJEPS "sports development" mention "parachuting", or a BEES 2nd degree, option "parachuting",
  - either, from a trainer qualification to the progress accompanied in a fall or a qualification of a two-seater parachute pilot trainer with passenger carry issued by the Minister of Defence,
  - either the certificate of parachutist navigating experimenter or paratrooper of tests and receptions, deputy chief of mission minimum, issued by the Minister of Defence;
- have conducted at least fifty jumps in the last 24 months prior to the application.

*For further information*: Article 10.2 of the December 3, 1956 order on the creation of a professional paratrooper's patent and licence and an instructor qualification.

##### Qualification of two-seater parachute jumps

###### Prerogatives

This qualification is required to empower the holder of a professional paratrooper licence to perform two-seater parachute jumps.

*For further information*: Article 9-1 of the December 3, 1956 decree on the creation of a professional paratrooper's patent and licence and an instructor qualification; may 30, 2011 order on the training, qualification and practice of two-seater parachute jumps by professional paratroopers.

**What to know**

Every two-seater parachute pilot must, before carrying a passenger, ensure:

- that the passenger information briefing was conducted by a paratrooper who holds the two-seater parachute jump qualification;
- Has at least one altimeter and one strap-cutter;
- under normal conditions, that the main sail opening device is implemented at a minimum height of 1,500 metres.*For further information*: Article 9 of the may 30, 2011 order.

###### Validity

The qualification is valid for twelve months. It is extended if the professional paratrooper justifies, in the twelve months preceding the end of the validity date, the performance of one hundred jumps including thirty in a two-seater parachute or a test jump with a two-seater parachute jump instructor, the latter taking Passenger seat.

In addition, the paratrooper must perform an in-flight check with a two-seater parachute jump instructor prior to a theoretical knowledge check every five years from the date of qualifying. Qualification is renewed if he justifies having completed the test jump in the previous thirty days.

###### Conditions of issuance

The person concerned must:

- Hold a professional paratrooper licence
- Have completed and satisfactorily completed theoretical and practical training;
- satisfactorily performed supervised jumps.

###### Conditions for admission to training

The person concerned must:

- Hold a certificate of fitness for practical flight tests of the exam for the valid professional paratrooper licence or a professional paratrooper licence;
- justify a minimum total of one thousand free fall jumps including at least two hundred jumps in the last twenty-four months and at least one hundred jumps in the last twelve months. At least 200 jumps must have been made using a flexible extractor aperture device;
- meet two practical selection jumps.

*For further information*: Article 2 of the may 30, 2011 order.

**Good to know**

To qualify for a two-seater parachute jump by equivalence, the holder of a valid professional paratrooper licence must meet the following requirements:

- justify a minimum total of one thousand free fall jumps including at least two hundred jumps in the last twenty-four months and at least one hundred jumps in the last twelve months. At least 200 jumps must have been made using a flexible extractor aperture device;
- justify performing at least one hundred two-seater parachute jumps;
- Be a holder:- either, for at least twelve months, a specialty "parachuting" BPJEPS, or an 1st degree BEES, a "parachuting" option, a tandem teaching specialty,
  - either a two-seater operational parachute pilot qualification in passenger transport or a two-seater parachute pilot trainer qualification with passenger carry;
- have a recent experience of 100 jumps, including thirty two-seater parachute jumps, in the twelve months prior to demand;
- have refreshed theoretical knowledge specific to the practice of two-seater parachute jumping.

*For further information*: Article 8 of the order of 30 May 2011 above.

#### Costs associated with qualification

The training leading to the patent and the license is paid for. The cost varies by training organization.

For more details, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of an EU or EEA member state who plans to provide services on a temporary and casual basis must:

- Be legally established in a Member State to work as a professional paratrooper;
- have been paratrooper in one or more Full-time Member States for at least one year or part-time for a total period equivalent to the ten years prior to the benefit when the profession or training there driving is not regulated in the establishment Member State.

*For further information*: Article 2 bis of the order of 3 December 1956 above.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA Member State can obtain a professional paratrooper's licence and, if necessary, the associated qualifications:

- whether he has the certificate of competency or training required to perform paratrooper activity in one of these states when he regulates the profession;
- or if he has one or more certificates of competency or proof of training documents issued by a competent authority of one of these Member States which does not regulate this profession, and attesting that he has been prepared for the exercise of this activity, and whether he has worked as a full-time or part-time professional paratrooper for a total period equivalent to the previous ten years in one or more of those states that do not regulate this occupation.

*For further information*: Article 2 bis of the order of 3 December 1956 above.

3°.Social legislation
---------------------------------

The Labour Code is applicable to civil aviation flight attendants and their employers, unless there is a specific provision.

### a. Employment contract

The commitment of a professional flight crew member necessarily results in the establishment of a written employment contract that specifies different mentions.

If the contract is concluded for a specific mission, it indicates the final destination of the mission and the time from which it is deemed accomplished.

Each salaried flight attendant is entitled to a guaranteed salary on a monthly basis.

### b. Working time

The provisions of the Labour Code relating to mandatory break time, night work and daily rest do not apply to civil aviation aircrew.

The annual service time of paratroopers may not exceed 2,000 hours, in which flight time is limited to 900 hours.

In addition to the periods of legal leave defined by the Labour Code, these employees benefit from at least 7 days per month and at least 96 days per calendar year free of any service and obligation. These days, notified in advance, may include rest periods and all or part of downtime determined by law or regulation.

### c. Execution of the employment contract

Unless it is a matter of providing a public service, aircrew and additional flight attendants may only be assigned to air work in areas of civil and military hostility if they are volunteers. A special contract then sets out the special conditions of work and expressly covers, apart from the usual risks, the particular risks due to the conditions of employment.

With exceptions, paratrooper activity may not be carried out in public air transport beyond the age of sixty.

*For further information*: Articles L. 6521-6 and following, L. 6523-1 and following of the Transportation Code.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Pre-registration of aircrew records

As a professional aircrew in civil aviation, a professional paratrooper practising in a habitual and principal manner, on his own behalf or on behalf of others, for profit or for a fee, must:

- Holding a valid aviation designation
- be registered on the register for their duties and in one of three categories:- essays and receptions,
  - aerial work.

In order to be registered in the register corresponding to his duties, the candidate must not have on the number 2 of his criminal record any references incompatible with the performance of the duties to which he is applying.

#### Competent authority 

The request must be addressed to the Directorate General of Civil Aviation, 50 Henry Farman Street, 75720 Paris Cedex 15.

#### Supporting documents

The supporting documents to be provided are:

- Form Cerfa 47-0049;
- A copy of an ID
- A residence permit
- a no.3 extract from the criminal record less than three months old;
- A medical certificate
- a declaration of honour (including certifying that you are a regular and principal seafarer);
- a declaration of non-membership in the public service;
- for seafarers holding a professional licence established by an EU Member State and validated by France, a copy of this validation;
- for employees, a certified copy of the employment contract explicitly mentioning your date of employment and the duties performed;
- for the self-employed, the certificate of registration on the trade register or the certificate of declaration to the tax services clearly showing the activity of self-employed seafarer;
- for career military personnel, the certificate of the military authority certifying that they are placed in a position whose duration is not taken into account in the calculation of the military pension and which does not prohibit them from a primary paid activity outside army.

*For further information*: Articles L. 6521-2 and L. 6521-3 of the Transportation Code; decree of 21 January 1998 on the procedures for registering professional aircrew in civil aviation.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

Prior to the first provision of services, the national must submit a written request to the competent authority which can order an audit of his professional qualifications.

The inspection carried out must enable the Minister in charge of civil aviation, after advice from the Council of The Professional Sailing Personnel of Civil Aeronautics or his group of experts, to ensure that the person concerned, for the exercise of the activity paratrooper does not present any professional inadequacies that could affect safety.

After all the checks, if the candidate is deemed fit, he is issued the professional paratrooper's license and, if necessary, the associated qualifications. Otherwise, the application is objected to and the applicant is not permitted to engage in professional paratrooper activity. The applicant may represent himself in the aptitude test.

#### Competent authority

The request must be addressed to the Minister for Civil Aviation (Directorate General of Civil Aviation, 50 Henry Farman Street, 75720 Paris Cedex 15).

#### Licensing conditions

The applicant must:

- meet medical standards (see supra "2 a. Training");
- justify having knowledge of national aviation regulations either through an adaptation course with an operator or by passing the 010 air law test of the theoretical examination of the commercial pilot licence (CPL);
- meet the minimum experience requirements.

#### Time 

Within one month of receiving the application and attached documents, the Minister responsible for civil aviation:

- informs the person concerned of the result of the audit;
- or requests for further information by specifying the information to be provided. In this case, it makes the decision within two months of receiving the additional information.

#### Compensation measures

The Minister for Civil Aviation may require the applicant to demonstrate that he or she has the missing knowledge, skills or skills through an aptitude test:

- in the event of a substantial difference between the applicant's professional skills and those acquired by the required theoretical and practical training to carry out the activity of professional paratrooper on French territory;
- or where these are lower, as this difference is likely to affect aviation safety and cannot be compensated by the person's work experience or by the knowledge, skills and skills acquired during the lifelong learning that has been validated by a competent body for this purpose.

*For further information*: Article 2 bis of the December 3, 1956 decree on the creation of a professional paratrooper's patent and licence and an instructor qualification.

### c. Obtaining a licence for EU nationals for a permanent exercise (LE)

In order to obtain the licence and, if necessary, the associated qualifications, the professional must apply to the competent authority.

After all the checks, if the candidate is deemed fit, he is issued the professional paratrooper's license and, if necessary, the associated qualifications.

Otherwise, the application is objected to and the applicant is not permitted to engage in professional paratrooper activity.

#### Competent authority 

The applicant must submit his application to the Minister for Civil Aviation (Civil Aviation Directorate, 50 Henry Farman Street, 75720 Paris Cedex 15).

#### Licensing conditions

The applicant must:

- Meet medical standards
- justify having knowledge of national aviation regulations either through an adaptation course with an operator or by passing the 010 air law test of the theoretical examination of the commercial pilot licence (CPL);
- meet the minimum experience requirements.

If there is serious and concrete doubt about the sufficient level of language knowledge in French of the person benefiting from the recognition of his professional qualifications in relation to the activities of professional paratrooper, the Minister may impose a control of language skills.

#### Compensation measures

Where the knowledge, skills and skills that the applicant has acquired through training and work experience and lifelong learning are substantially different or less in terms of content than those acquired by theoretical and practical training to carry out the activity of professional paratrooper in France, the Minister in charge of civil aviation can, after advice from the Council of the professional aircrew of civil aeronautics or his group expert, make the duly justified decision to impose on the person:

- an adaptation course that is being evaluated and possibly accompanied by further training;
- or an aptitude test to assess the applicant's ability to perform professional paratrooper activity.

The choice is left to the applicant. The Minister for Civil Aviation ensures that the applicant has the opportunity to present the fitness test within a maximum of six months from the initial decision requiring an applicant's aptitude test.

*For further information*: Article 2 bis of the December 3, 1956 decree on the creation of a professional paratrooper's patent and licence and an instructor qualification.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

