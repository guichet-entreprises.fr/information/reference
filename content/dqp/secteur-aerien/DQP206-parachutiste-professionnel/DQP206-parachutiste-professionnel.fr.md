﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP206" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Parachutiste professionnel" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="parachutiste-professionnel" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/parachutiste-professionnel.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="parachutiste-professionnel" -->
<!-- var(translation)="None" -->

# Parachutiste professionnel

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

Le parachutiste professionnel est un personnel navigant professionnel de l’aéronautique civile. Son activité consiste à chuter d'une hauteur allant d'une centaine de mètres à plusieurs milliers, en sortant généralement d'un avion, pour ensuite retourner sur terre avec l'aide d'un parachute.

Après la sortie, le parachutiste est en chute libre pour une durée plus ou moins longue selon la discipline pratiquée, la hauteur à laquelle il a été largué et l'altitude relative d'ouverture. Il peut effectuer seul, en voltige par exemple, ou avec d'autres parachutistes des figures durant la chute avant de rejoindre le sol en pilotant son parachute de manière à se poser au lieu prévu.

Le professionnel peut effectuer des sauts de démonstration rémunérés ou accomplir une mission de travail aérien.

*Pour aller plus loin* : article L. 6521-1 du Code des transports ; arrêté du 25 août 1954 relatif à la classification du personnel navigant professionnel de l’aéronautique civile ; article 1er de l’arrêté relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

En tant que personnel navigant professionnel de l'aéronautique civile, le parachutiste professionnel est soumis à l’application des articles L. 6521-2 du Code des transports, suivant lesquels il doit être :

- titulaire d'un titre aéronautique en état de validité ;
- inscrit sur le registre et à l'une des trois catégories suivantes :
  - essais et réceptions,
  - transport aérien,
  - travail aérien.

Pour exercer son activité, le parachutiste doit être titulaire du brevet et de la licence correspondant à la nature du saut envisagé en cours de validité et comportant toutes qualifications nécessaires.

**À noter**

Le personnel navigant prestataire de services de transport ou de travail aériens établi dans un État membre de l'Union européenne (UE) autre que la France ou dans un État partie à l'accord sur l'Espace économique européen (EEE) ou aux accords bilatéraux passés par l'UE avec la Confédération suisse ainsi que le personnel navigant salarié d'un prestataire de services de transport ou de travail aériens établi dans l'un des États précités, qui exercent temporairement leur activité en France, n'entrent pas dans le champ d'application de cette obligation. Ce personnel se réfère aux dispositions du « b. Ressortissants UE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services) » et du « c. Ressortissants UE : en vue d'un exercice permanent (Libre Établissement) » ci-dessous.

*Pour aller plus loin* : articles L. 6521-1 et suivants du Code des transports ; article 2 de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

#### Formation

Nul ne peut pratiquer le parachutisme s’il n’est pas en mesure de justifier qu’il est titulaire de la licence correspondant à la nature du saut envisagé en cours de validité et comportant toutes qualifications nécessaires.

La licence et les qualifications ne peuvent être délivrées qu’aux titulaires du brevet.

*Pour aller plus loin* : article 2 de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

##### Brevet et licence de parachutiste professionnel

Le brevet est un titre sanctionnant un ensemble de connaissances générales théoriques et pratiques. Il est délivré après examen et est définitivement acquis à son titulaire.

La licence est un titre valable douze mois, sanctionnant l’aptitude et le droit pour les titulaires du brevet de remplir les fonctions correspondantes.

Elle est renouvelée pour une période de même durée, sous réserve que l’intéressé ait accompli vingt sauts dans les douze mois qui précèdent la demande de renouvellement ou cinq sauts dans les six mois précédant cette demande. Seuls seront pris en compte les sauts au cours desquels ont été utilisés les dispositifs d’ouverture commandée des parachutes.

S'il ne remplit pas ces conditions, le demandeur doit satisfaire à un contrôle d'un instructeur portant sur les épreuves pratiques exigées pour la délivrance de la licence.

*Pour aller plus loin* : articles 1, 7 et 9 de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

###### Prérogatives 

La licence de parachutiste professionnel permet à son titulaire d’exécuter contre rémunération tous types de sauts avec du matériel conforme à la réglementation en vigueur.

*Pour aller plus loin* : article 9 de l’arrêté du 3 décembre 1956 précité.

###### Conditions de délivrance du brevet et de la licence

L'intéressé doit :

- détenir un certificat médical de classe 1. Pour plus d’informations sur ce certificat, il est recommandé de se reporter à l’arrêté du 27 janvier 2005 relatif à l’aptitude physique et mentale du personnel navigant technique professionnel de l’aéronautique civile ;
- être âgé de dix-huit ans révolus ;
- soit, totaliser deux cent cinquante sauts dont au moins deux cents sauts au cours desquels il a utilisé uniquement le dispositif d’ouverture commandée et comprenant un minimum de vingt-cinq chutes libres d’une durée supérieure ou égale à 30 secondes ;
- soit, totaliser deux cents sauts dont au moins cent cinquante sauts au cours desquels il a utilisé uniquement le dispositif d’ouverture commandée et comprenant un minimum de vingt-cinq chutes libres d’une durée supérieure ou égale à 30 secondes, s’il justifie avoir suivi de manière complète et satisfaisante un enseignement homologué ;
- satisfaire aux épreuves théoriques portant sur l’aérodynamique, sur la construction de parachutes et les équipements, sur la technique de mise en œuvre des parachutes et leur utilisation, sur la météorologie et sur la réglementation aérienne ;
- satisfaire aux épreuves pratiques : il s’agit d’épreuves en vol comprenant une épreuve de sauts et une épreuve de largage de matériel.

*Pour aller plus loin* : articles 4 et 9 de l’arrêté du 3 décembre 1956 précité ; article 3 et annexe de l’arrêté du 25 avril 1962 relatif au programme et au régime de l’examen pour l’obtention du brevet et de la licence de parachutiste professionnel.

**Bon à savoir**

Pour obtenir le brevet et la licence de parachutiste professionnel par équivalence, le candidat doit :

- être âgé de dix-huit ans révolus et détenir un certificat médical de classe 1 ;
- être titulaire :
  - soit, depuis au moins douze mois, d’un diplôme d’État de la jeunesse, de l’éducation populaire et du sport (DEJEPS) spécialité « perfectionnement sportif » mention « parachutisme », d’un brevet professionnel de la jeunesse, de l’éducation populaire et du sport (BPJEPS) spécialité « parachutisme » ou d’un brevet d’État d’éducateur sportif (BEES) 1er degré, option « parachutisme », et avoir satisfait aux épreuves théoriques du brevet de parachutiste professionnel,
  - soit, d’un brevet d’instructeur au saut à ouverture commandée retardée délivré par le ministre de la Défense et justifier avoir effectué une formation complète et satisfaisante portant sur la réglementation aérienne civile dispensée par l’autorité militaire et attestée par un organisme des forces armées,
  - soit, du certificat de parachutiste navigant expérimentateur ou de parachutiste d’essais et réceptions délivré par le ministre de la Défense et justifier avoir effectué une formation complète et satisfaisante portant sur la réglementation aérienne civile dispensée par l’autorité militaire et attestée par un organisme des forces armées ;
- avoir effectué vingt sauts dans les douze mois qui précèdent la demande.

*Pour aller plus loin* : article 9-2 de l’arrêté du 3 décembre 1956 précité.

##### Qualification d’instructeur

Une qualification d’instructeur est obligatoire pour habiliter le détenteur de la licence de parachutiste professionnel à donner ou à diriger l’instruction en vol nécessaire pour l’obtention de cette licence.

###### Durée de validité

La qualification d’instructeur est valable trente-six mois, renouvelable par période de même durée, sous réserve que l’intéressé justifie avoir dirigé l’instruction de cinquante sauts au moins pendant les vingt-quatre mois précédant la demande.

Si l’intéressé n’a pas dirigé ce nombre de sauts, il doit satisfaire à un contrôle de l’aptitude aux fonctions d’instructeur de parachutiste professionnel assuré par un instructeur.

###### Conditions d’obtention

Le candidat doit remplir les conditions suivantes :

- totaliser trois cent cinquante sauts dont au moins trois cents sauts au cours desquels il a utilisé uniquement le dispositif d’ouverture commandée et comprenant un minimum de quarante chutes libres d’une durée comprise entre 30 et 60 secondes et un minimum de dix chutes libres d’une durée supérieure à 60 secondes ;
- avoir suivi de manière satisfaisante et complète un enseignement homologué d’instructeur parachutiste.

*Pour aller plus loin* : article 10 de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

**Bon à savoir**

Pour obtenir la qualification d’instructeur de parachutiste professionnel par équivalence, les conditions suivantes doivent être remplies :

- être détenteur de la licence de parachutiste professionnel en état de validité ou satisfaire les conditions exigées pour obtenir le brevet et la licence de parachutiste professionnel par équivalence ;
- être titulaire :
  - soit, depuis au moins douze mois, d’un DEJEPS spécialité « perfectionnement sportif » mention « parachutisme », ou d’un BEES 2e degré, option « parachutisme »,
  - soit, d’une qualification de formateur de moniteur à la progression accompagnée en chute ou d’une qualification de formateur pilote de parachute biplace avec emport de passager délivrée par le ministre de la Défense,
  - soit du certificat de parachutiste naviguant expérimentateur ou de parachutiste d'essais et réceptions, sous-chef de mission minimum, délivrés par le ministre de la Défense ;
- avoir dirigé l’instruction de cinquante sauts au moins pendant les vingt-quatre derniers mois qui précèdent la demande.

*Pour aller plus loin* : article 10.2 de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

##### Qualification de sauts en parachute biplace

###### Prérogatives

Cette qualification est obligatoire pour habiliter le titulaire d’une licence de parachutiste professionnel à réaliser des sauts en parachute biplace.

*Pour aller plus loin* : article 9-1 de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur ; arrêté du 30 mai 2011 relatif à la formation, la qualification et la pratique des sauts en parachute biplace par les parachutistes professionnels.

**À savoir**

Tout pilote de parachute biplace doit, avant de transporter un passager, s’assurer :

- que le briefing d’information du passager a été effectué par un parachutiste détenteur de la qualification de saut en parachute biplace ;
- qu’il dispose d’au moins un altimètre et un coupe-sangle ;
- en conditions normales, que le dispositif d’ouverture de la voile principale est mis en œuvre à une hauteur minimale de 1 500 mètres. *Pour aller plus loin* : article 9 de l’arrêté du 30 mai 2011 précité.

###### Durée de validité

La qualification est valable douze mois. Elle est prorogée si le parachutiste professionnel justifie, dans les douze mois précédant la fin de date de validité, de l’exécution de cent sauts dont trente en parachute biplace ou d’un saut de test avec un instructeur de saut en parachute biplace, celui-ci prenant la place du passager.

En outre, le parachutiste doit effectuer tous les cinq ans à compter de la date d’obtention de la qualification un contrôle en vol avec un instructeur de saut en parachute biplace précédé d’une vérification des connaissances théoriques. La qualification est renouvelée s’il justifie avoir effectué le saut de test dans les trente jours précédents.

###### Conditions de délivrance

L'intéressé doit :

- être titulaire d’une licence de parachutiste professionnel ;
- avoir suivi de manière complète et satisfaisante une formation théorique et pratique ;
- avoir effectué de manière satisfaisante des sauts sous supervision.

###### Conditions d’admission en formation

L'intéressé doit :

- détenir un certificat d’aptitude aux épreuves pratiques en vol de l’examen pour l’obtention de la licence de parachutiste professionnel en état de validité ou une licence de parachutiste professionnel ;
- justifier d’un total minimum de mille sauts en chute libre dont au moins deux cents sauts dans les vingt-quatre derniers mois et au moins cent sauts dans les douze derniers mois. Deux cents sauts au moins doivent avoir été effectués en utilisant un dispositif d’ouverture à extracteur souple ;
- satisfaire à deux sauts de sélection pratique.

*Pour aller plus loin* : article 2 de l’arrêté du 30 mai 2011 précité.

**Bon à savoir**

Pour obtenir la qualification de saut en parachute biplace par équivalence, le titulaire d’une licence de parachutiste professionnel en état de validité doit remplir les conditions suivantes :

- justifier d’un total minimum de mille sauts en chute libre dont au moins deux cents sauts dans les vingt-quatre derniers mois et au moins cent sauts dans les douze derniers mois. Deux cents sauts au moins doivent avoir été effectués en utilisant un dispositif d’ouverture à extracteur souple ;
- justifier avoir effectué au moins cent sauts en parachute biplace ;
- être titulaire :
  - soit, depuis au moins douze mois, d’un BPJEPS spécialité « parachutisme » mention « saut en tandem », ou d’un BEES 1er degré, option « parachutisme », spécialité d’enseignement tandem,
  - soit d’une qualification de pilote de parachute biplace opérationnel version emport de passager ou d’une qualification de formateur de pilote de parachute biplace avec emport de passager ;
- avoir une expérience récente de cent sauts, dont trente sauts en parachute biplace, dans les douze mois qui précèdent la demande ;
- avoir effectué un rafraîchissement des connaissances théoriques spécifiques à la pratique du saut en parachute biplace.

*Pour aller plus loin* : article 8 de l’arrêté du 30 mai 2011 précité.

#### Coûts associés à la qualification

La formation menant à l’obtention du brevet et de la licence est payante. Le coût varie selon les organismes de formation.

Pour plus de précisions, il est conseillé de se rapprocher de l’organisme de formation considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d’un État membre de l’UE ou de l’EEE qui envisage d’effectuer de manière temporaire et occasionnelle une prestation de services doit :

- être légalement établi dans un État membre pour y exercer l’activité de parachutiste professionnel ;
- avoir exercé l’activité de parachutiste dans un ou plusieurs États membres à temps plein pendant au moins un an ou à temps partiel pendant une durée totale équivalente au cours des dix années qui précèdent la prestation lorsque la profession ou la formation y conduisant n’est pas réglementée dans l’État membre d’établissement.

*Pour aller plus loin* : article 2 bis de l’arrêté du 3 décembre 1956 précité.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d’un État membre de l’UE ou de l’EEE peut obtenir la licence de parachutiste professionnel et, le cas échéant, les qualifications associées :

- s’il possède l’attestation de compétences ou le titre de formation requis pour exercer l’activité de parachutiste dans un de ces États lorsque celui-ci réglemente la profession ;
- ou s’il possède une ou plusieurs attestations de compétences ou preuves de titres de formation délivrés par une autorité compétente d’un de ces États membres qui ne réglemente pas cette profession, et attestant qu’il a été préparé à l’exercice de cette activité, et s’il a exercé l’activité de parachutiste professionnel à temps plein pendant un an ou à temps partiel pendant une durée totale équivalente au cours des dix années précédentes dans un ou plusieurs de ces États qui ne réglementent pas cette profession.

*Pour aller plus loin* : article 2 bis de l’arrêté du 3 décembre 1956 précité.

## 3°. Législation sociale

Le Code du travail est applicable au personnel navigant de l'aéronautique civile et à leurs employeurs, sauf disposition spécifique.

### a. Contrat de travail

L'engagement d'un membre du personnel navigant professionnel donne obligatoirement lieu à l'établissement d'un contrat de travail écrit qui précise différentes mentions.

Si le contrat est conclu pour une mission déterminée, il indique le lieu de destination finale de cette dernière et le moment à partir duquel elle est réputée accomplie.

Chaque personnel navigant salarié a droit mensuellement à un salaire garanti.

### b. Durée du travail

Les dispositions du Code du travail relatives au temps de pause obligatoire, au travail de nuit et au repos quotidien ne s'appliquent pas au personnel navigant de l'aviation civile.

La durée annuelle du temps de service des salariés parachutistes ne peut excéder 2 000 heures, dans lesquelles le temps de vol est limité à 900 heures.

Outre les périodes de congé légal définies par le Code du travail, ces salariés bénéficient d'au moins 7 jours par mois et d'au moins 96 jours par année civile libres de tout service et de toute astreinte. Ces jours, notifiés à l'avance, peuvent comprendre les périodes de repos et tout ou partie des temps d'arrêt déterminés par la loi ou le règlement.

### c. Exécution du contrat de travail

Sauf s'il s'agit d'assurer un service public, les navigants et le personnel complémentaire de bord ne peuvent être assignés à un travail aérien en zone d'hostilités civiles et militaires que s'ils sont volontaires. Un contrat particulier fixe alors les conditions spéciales de travail et couvre expressément, en dehors des risques habituels, les risques particuliers dus aux conditions d'emploi.

Sauf exception, l'activité de parachutiste ne peut être exercée dans le transport aérien public au-delà de l'âge de soixante ans.

*Pour aller plus loin* : articles L. 6521-6 et suivants, L. 6523-1 et suivants du Code des transports.

## 4°. Démarches et formalités de reconnaissance de qualifications

### a. Inscription préalable aux registres du personnel navigant

A son titre de personnel navigant professionnel de l'aéronautique civile, le parachutiste professionnel exerçant de façon habituelle et principale, pour son propre compte ou pour le compte d'autrui, dans un but lucratif ou contre rémunération, doit :

- être titulaire d'un titre aéronautique en état de validité ; 
- être inscrit sur le registre correspondant à ses fonctions et à l’une des trois catégories suivantes :
  - essais et réceptions,
  - travail aérien.

Pour être inscrit au registre correspondant à ses fonctions, le candidat ne doit pas avoir au bulletin n° 2 de son casier judiciaire des mentions incompatibles avec l'exercice des fonctions auxquelles il postule.

#### Autorité compétente 

La demande doit être adressée à la Direction générale de l’aviation civile, 50 rue Henry Farman, 75720 Paris Cedex 15.

#### Pièces justificatives

Les pièces justificatives à fournir sont les suivantes :

- le formulaire Cerfa 47-0049 ;
- une copie d'une pièce d'identité ;
- un titre de séjour ;
- un extrait n° 3 du casier judiciaire datant de moins de trois mois ;
- un certificat médical ;
- une déclaration sur l'honneur (certifiant notamment que vous exercez la profession de navigant à titre habituel et principal) ;
- une déclaration de non-appartenance à la fonction publique ;
- pour les navigants titulaires d'une licence professionnelle établie par un État membre de l’UE et validée par la France, une copie de cette validation ;
- pour les salariés, une copie certifiée conforme du contrat de travail mentionnant explicitement votre date d’embauche et les fonctions exercées ;
- pour les travailleurs indépendants, l’attestation d'inscription sur le registre du commerce ou l’attestation de déclaration aux services fiscaux faisant apparaître clairement l’activité de navigant indépendant ;
- pour les militaires de carrière, le certificat de l'autorité militaire attestant qu’ils sont placés dans une position dont la durée n'est pas prise en compte pour le calcul de la pension militaire et qui ne leur interdit pas une activité principale rémunérée en dehors de l'armée.

*Pour aller plus loin* : articles L. 6521-2 et L. 6521-3 du Code des transports ; arrêté du 21 janvier 1998 relatif aux modalités d'inscription aux registres du personnel navigant professionnel de l'aéronautique civile.

### b. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS)

Préalablement à la première prestation de services, le ressortissant doit adresser une demande écrite à l’autorité compétente qui peut ordonner une vérification de ses qualifications professionnelles.

Le contrôle auquel il est procédé doit permettre au ministre chargé de l’aviation civile, après avis du Conseil du personnel naviguant professionnel de l’aéronautique civile ou de son groupe d’experts, de s’assurer que l’intéressé, pour l’exercice de l’activité de parachutiste professionnel ne présente pas d’insuffisance professionnelle susceptible de nuire à la sécurité.

Après l’ensemble des vérifications, si le candidat est jugé apte, il lui est délivré la licence de parachutiste professionnel et, le cas échéant, les qualifications associées. Dans le cas contraire, il est fait opposition à la demande et le demandeur n’est pas autorisé à exercer l’activité de parachutiste professionnel. Le demandeur peut se représenter à l’épreuve d’aptitude.

#### Autorité compétente

La demande doit être adressée au ministre chargé de l’aviation civile (Direction générale de l'aviation civile, 50 rue Henry Farman, 75720 Paris Cedex 15).

#### Conditions d’obtention de la licence

Le demandeur doit :

- satisfaire aux normes médicales (cf. supra « 2° a. Formation ») ;
- justifier avoir une connaissance de la réglementation aérienne nationale soit par un stage d’adaptation chez un exploitant, soit par la réussite à l’épreuve de droit aérien 010 de l’examen théorique de la licence de pilote professionnel (CPL) ;
- remplir les conditions d’expérience minimum.

#### Délais 

Dans un délai d’un mois à compter de la réception de la demande et des documents joints, le ministre chargé de l’aviation civile :

- informe l’intéressé du résultat de la vérification ;
- ou procède à une demande d’informations complémentaires en précisant les informations à fournir. Dans cette hypothèse, il prend la décision dans un délai de deux mois à compter de la réception du complément d’informations.

#### Mesures de compensation

Le ministre chargé de l’aviation civile peut imposer au demandeur de démontrer qu’il a les connaissances, aptitudes ou compétences manquantes, par une épreuve d’aptitude :

- en cas de différence substantielle entre les compétences professionnelles du demandeur et celles acquises par la formation théorique et pratique exigée permettant d’exercer l’activité de parachutiste professionnel sur le territoire français ;
- ou lorsque celles-ci sont inférieures, dans la mesure où cette différence est de nature à nuire à la sécurité aérienne et où elle ne peut être compensée par l’expérience professionnelle de l’intéressé ou par les connaissances, aptitudes et compétences acquises lors d’un apprentissage tout au long de la vie ayant fait l’objet, à cette fin, d’une validation par un organisme compétent.

*Pour aller plus loin* : article 2 bis de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

### c. Obtenir la licence pour les ressortissants de l’UE en vue d’un exercice permanent (LE)

Afin d’obtenir la licence et, le cas échéant, les qualifications associées, le professionnel doit en faire la demande à l’autorité compétente.

Après l’ensemble des vérifications, si le candidat est jugé apte, il lui est délivré la licence de parachutiste professionnel et, le cas échéant, les qualifications associées.

Dans le cas contraire, il est fait opposition à la demande et le demandeur n’est pas autorisé à exercer l’activité de parachutiste professionnel.

#### Autorité compétente 

Le demandeur doit adresser sa demande au ministre chargé de l’aviation civile (Direction générale de l'aviation civile, 50 rue Henry Farman, 75720 Paris Cedex 15).

#### Conditions d’obtention de la licence

Le demandeur doit :

- satisfaire aux normes médicales ;
- justifier avoir une connaissance de la réglementation aérienne nationale soit par un stage d’adaptation chez un exploitant, soit par la réussite à l’épreuve de droit aérien 010 de l’examen théorique de la licence de pilote professionnel (CPL) ;
- remplir les conditions d’expérience minimum.

S’il existe un doute sérieux et concret sur le niveau suffisant des connaissances linguistiques en français de la personne bénéficiant de la reconnaissance de ses qualifications professionnelles au regard des activités de parachutiste professionnel, le ministre peut imposer un contrôle des connaissances linguistiques.

#### Mesures de compensation

Lorsque les connaissances, aptitudes et compétences que le demandeur a acquises par la formation et l’expérience professionnelle et l’apprentissage tout au long de la vie sont substantiellement différentes ou inférieures en terme de contenu de celles acquises par la formation théorique et pratique permettant d’exercer l’activité de parachutiste professionnel en France, le ministre chargé de l’aviation civile peut, après avis du Conseil du personnel navigant professionnel de l’aéronautique civile ou de son groupe d’expert, prendre la décision dûment justifiée d’imposer à la personne :

- un stage d’adaptation qui fait l’objet d’une évaluation et est accompagné éventuellement d’une formation complémentaire ;
- ou une épreuve d’aptitude qui a pour but d’apprécier l’aptitude du demandeur à exercer l’activité de parachutiste professionnel.

Le choix est laissé au demandeur. Le ministre chargé de l’aviation civile veille à ce que le demandeur ait la possibilité de présenter l’épreuve d’aptitude dans un délai maximal de six mois à compter de la décision initiale imposant une épreuve d’aptitude au demandeur.

*Pour aller plus loin* : article 2 bis de l’arrêté du 3 décembre 1956 relatif à la création d’un brevet et d’une licence de parachutiste professionnel et d’une qualification d’instructeur.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).