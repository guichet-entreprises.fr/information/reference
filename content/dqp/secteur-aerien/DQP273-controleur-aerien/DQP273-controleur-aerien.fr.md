﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP273" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Contrôleur aérien" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="controleur-aerien" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/controleur-aerien.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="controleur-aerien" -->
<!-- var(translation)="None" -->

# Contrôleur aérien

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

L'activité couverte est le contrôle d'aérodrome, le contrôle d'approche et le contrôle régional.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'activité est soumise à une règlementation européenne (Règlement (UE) 2015/340 de la Commission du 20 février 2015 déterminant les exigences techniques et les procédures administratives applicables aux licences et certificats de contrôleur de la circulation aérienne conformément au règlement (CE) n° 216/2008 du Parlement européen et du Conseil, modifiant le règlement d'exécution (UE) n° 923/2012 de la Commission et abrogeant le règlement (UE) n° 805/2011 de la Commission). Pour mémoire, les exigences fixées par la Commission européenne requièrent la détention d'une licence de contrôleur aérien, des qualifications et mentions de qualification associées en cours de validité, d'un certificat médical de classe 3, et le cas échéant d'un certificat d'aptitude linguistique.

#### Formation

Les exigences fixées par la Commission européenne requièrent des candidats à une licence de contrôleur aérien de suivre une formation conforme à l'annexe 1 et à l'appendice 2 de l'annexe 1 du règlement, auprès d'un organisme de formation certifié. Les formations relatives aux qualifications sont conformes aux exigences fixées dans l'annexe 1 et les appendices 3 à 8 du règlement.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

La reconnaissance des titres de contrôleurs aériens délivrés dans l'Union européenne est automatique par procédure d’échange conformément au point ATCO.A.010 du règlement (UE) 2015/340. La reconnaissance des titres délivrés hors de l’Union européenne n’est pas prévue.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

La reconnaissance des titres de contrôleurs aériens délivrés dans l’Union européenne est automatique par procédure d’échange conformément au point ATCO.A.010 du règlement (UE) 2015/340. La reconnaissance des titres délivrés hors de l’Union n’est pas prévue.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le service du contrôle aérien est assuré par des agents de l'État recrutés selon les règles relatives à la fonction publique.

## 4°. Législation sociale et assurances

Les règles de la fonction publique s'appliquent à la profession.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable

Licence conforme au règlement (UE) 2015/340.

### b. Autorité compétente

Direction générale de l'aviation civile.

## 6°. Textes de référence

Règlement (UE) 2015/340 de la Commission du 20 février 2015 déterminant les exigences techniques et les procédures administratives applicables aux licences et certificats de contrôleur de la circulation aérienne conformément au règlement (CE) no 216/2008 du Parlement européen et du Conseil, modifiant le règlement d'exécution (UE) n° 923/2012 de la Commission et abrogeant le règlement (UE) n° 805/2011 de la Commission.