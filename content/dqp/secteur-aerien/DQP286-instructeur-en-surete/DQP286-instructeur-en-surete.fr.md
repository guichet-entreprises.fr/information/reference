﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP286" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Instructeur en sûreté de l'aviation civile" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="instructeur-en-surete" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/instructeur-en-surete.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="instructeur-en-surete" -->
<!-- var(translation)="None" -->

# Instructeur en sûreté de l'aviation civile

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

Dispenser la formation adaptée en matière de sureté de l'aviation civile aux agents de sûreté devant être titulaires d'une certification, à leurs superviseurs ainsi qu'aux « gestionnaires de la sûreté » dans le domaine aérien.

## 2°. Qualifications professionnelles

### a. Exigences réglementaires

#### Réglementation de l'Union européenne et nationale

- Points 11.5 de l'annexe au règlement d'exécution (UE) 2015/1998 de la Commission du 5 novembre 2015 fixant des mesures détaillées pour la mise en œuvre des normes de base communes dans le domaine de la sûreté de l'aviation civile ;
- Article R. 213-4 du Code de l'aviation civile.

*Autres textes non codifiés :*

Articles 11-5-1 à 11-5-4 de l'annexe de l'arrêté modifié du 11 septembre 2013 relatif aux mesures de sûreté de l'aviation civile.

#### Formation

**Instructeurs certifiés :**

Pour dispenser les formations prévues aux points 11.2.3.1 à 11.2.3.5 ainsi qu'aux points 11.2.4 et 11.2.5 de l'annexe au règlement (UE) 2015/1998, les instructeurs doivent obtenir la certification mentionnée à l'article R. 213-4 du Code de l'aviation civile. Le contenu de la formation et les modalités de certification sont définis par les articles 11-5-2 à 11-5-4 de l'annexe de l'arrêté du 11 septembre 2013 modifié.

Les instructeurs certifiés doivent suivre une formation délivrée par l'ENAC. Ces instructeurs doivent ensuite être certifiés selon les modules suivants : 

- module général : pédagogie, connaissances réglementaires sureté ;
- module de spécialisation du module général, relatif à l'analyse d'images et à l'exploitation des équipements de sureté ;
- module management : capacité à parrainer, à former sur le tas, à motiver ; connaissance de la gestion des conflits.

La certification est valable 5 ans.

**Instructeurs qualifiés :**

Les instructeurs délivrant les formations exigées pour effectuer les activités énumérées aux points 11.2.3.6 à 11.2.3.10, 11.2.4 (agents qui supervisent directement les agents visés aux points 11.2.3.6 à 11.2.3.10), 11.2.6.2 et 11.2.7 de l'annexe au règlement (UE) n° 2015/1998 doivent être qualifiés.

L'article 11-5-1 de l'annexe de l'arrêté du 11 septembre 2013 modifié dispose que « pour être qualifié, un instructeur doit posséder une expérience de formateur dans le domaine enseigné de la sûreté du transport aérien d'une durée d'au moins un an, ou satisfaire à chacun des 3 critères suivants :

- attester d'une expérience pratique d'au moins six mois dans les fonctions d'exécution des domaines enseignés datant de moins de cinq ans ;
- attester d'une pratique de l'enseignement de plus d'un an ou de la participation à une formation de formateur datant de moins de cinq ans ;
- attester avoir suivi la formation définie au point 11.2.2 de l'annexe au règlement (UE) 2015/1998 susvisé datant de moins de cinq ans. »

#### Coûts associés

Le coût de la formation certifiante à l'ENAC est de 12 180 € en 2022.

Le coût de la formation qualifiante peut se limiter au coût de la formation définie au point 11.2.2 de l'annexe au règlement (UE) n° 2015/1998 (1 000 € environ).

### Ressortissants UE : en vue d'un exercice temporaire et occasionnel (LPS)

Indépendamment de leurs qualifications professionnelles, et conformément à l'article L. 6342-3 du Code des transports, les instructeurs doivent être habilités. Cette habilitation qui consiste en une vérification des antécédents par les services de l'État, nécessite un temps d'instruction qui limite de fait les prestations temporaires de courte durée en France.

#### Ressortissants UE : en vue d'un exercice permanent (LE) 

En application du point 11.7.1 de l'annexe du règlement d'exécution (UE) 2015/1998 de la commission du 5 novembre 2015 fixant des mesures détaillées pour la mise en œuvre des normes de base communes dans le domaine de la sûreté de l'aviation civile : « toute compétence acquise par une personne afin de satisfaire aux exigences du règlement (CE) n° 300/2008 et de ses actes d'exécution dans un État membre doit être reconnue dans un autre État membre ».

L'article 11-7 de l'arrêté du 11 septembre 2013 modifié précise « qu'une personne désirant faire reconnaître une certification acquise dans un autre État membre de l'Union européenne doit en faire la demande auprès du ministre chargé des transports ».

Ainsi, un instructeur de sûreté aérienne dont les compétences ont été certifiées par l'autorité compétente d'un autre État membre de l'Union européenne souhaitant exercer le métier d'instructeur en France doit en faire la demande auprès de la Direction générale de l'aviation civile, direction de la sécurité de l'aviation civile (DSAC-SUR plus précisément), 50 rue Henry Farman, 75720 Paris Cedex 15.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Conformément à la règlementation européenne - point 11.5.1 de l'annexe au règlement (UE) 2015/1998 tel qu'il sera en vigueur à compter du 31 décembre 2021- , les instructeurs de sureté aérienne sont soumis à une vérification renforcée de leurs antécédents, en vue d'obtenir l'habilitation prévue mentionnée à l'article L. 6342-3 du Code des transports, qui consiste à :

a) établir l'identité de la personne sur la base de documents ;
b) prendre en considération le casier judiciaire dans tous les États de résidence au cours des cinq dernières années ; 
c) prendre en considération les emplois, les études et toute interruption de plus de vingt-huit jours dans le relevé de la formation initiale ou de la carrière au cours des cinq dernières années ;
d) prendre en considération les informations des services de renseignement et toute autre information pertinente dont les autorités nationales compétentes disposent et estiment qu'elles peuvent présenter un intérêt pour apprécier l'aptitude d'une personne à exercer une fonction qui requiert une vérification renforcée de ses antécédents. 

## 4°. Législation sociale et assurances

Droit du travail.

## 5°. Démarches et formalités de reconnaissance de qualification professionnelle

### a. Autorité compétente

La Direction générale de l'aviation civile, direction de la sécurité de l'aviation civile (DSAC-SUR), 50 rue Henry Farman, 75720 Paris Cedex 15). Si le professionnel souhaite exercer ailleurs que sur un aéroport parisien, il fera sa demande de reconnaissance mutuelle auprès de la DSAC-Interrégionale dont il dépendra (la DSAC-SUR lui indiquant les coordonnées de cette autorité régionalement compétente).

### b. Délais de réponse

Un mois après réception du dossier complet, peut être allongé en fonction du délai de réponse de l'autorité compétente de l'État dans lequel les attestations ont été émises, afin de vérifier qu'elles sont conformes à la règlementation en vigueur dans cet État. (DSAC/SUR pourra fournir le contact de l'autorité compétente si le demandeur ne l'a pas lui-même fourni).

### c. Pièces justificatives

Les dossiers de demande de reconnaissance des qualifications doivent contenir les éléments suivants :

- une lettre motivant la demande et le besoin de reconnaissance mutuelle (formulée en français ou, le cas échéant, en anglais) ;
- la copie d'un document l'identité de la personne (passeport, carte nationale d'identité, titre de séjour) ;
- la copie des attestations de formation (initiales et périodiques) pour lesquelles la reconnaissance est demandée ;
- le contact de l'autorité compétente de l'État dans lequel les attestations ont été émises (nom de l'autorité, nom et prénom du contact et de préférence son adresse email).

### d. Voies de recours nationales

Recours gracieux auprès de l'autorité compétente ayant opposé un refus de reconnaissance de la formation (refus qui ne peut se fonder que sur l'absence d'une des pièces nécessaires au dossier ou si l'autorité compétente du pays d'origine a infirmé l'existence d'une qualification dans son État). Puis recours contentieux devant le tribunal administratif compétent.

## 6°. Textes de référence

Pour la reconnaissance européenne des formations :

- le règlement (UE) 2015/1998 en son paragraphe 11.7 qui prévoit la reconnaissance mutuelle des compétences acquises par des formations reçues dans un autre État membre de l'Union européenne ;
- l'article 11-7 de l'arrêté du 11 septembre 2013 modifié.


Autres textes de référence : 

- point 11.5 de l'annexe au règlement d'exécution (UE) 2015/1998 ;
- article L. 6342-3 du Code des transports ;
- article R. 213-4 du Code de l'aviation civile ;
- annexe à l'arrêté du 11 septembre 2013 relatif aux mesures de sûreté de l'aviation civile.