﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP284" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Télépilote" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)=telepilote"" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/telepilote.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="telepilote" -->
<!-- var(translation)="None" -->

# Télépilote

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

Télépilote contre rémunération.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'activité est soumise à une règlementation européenne (Règlement d'exécution (UE) 2019/947 de la Commission du 24 mai 2019 concernant les règles et procédures applicables à l'exploitation d'aéronefs sans équipage à bord). Pour mémoire, les exigences fixées par la Commission européenne requièrent la détention d'un certificat de réussite à un examen accompagné le cas échéant d'une attestation de réussite de la formation pratique, ou d'un brevet d'aptitude des pilotes à distance.

#### Formation

Les exigences fixées par la Commission européenne requièrent des candidats télépilotes de suivre une formation conforme à l'une des formations décrites dans le règlement, en fonction des opérations conduites.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

La reconnaissance des titres de télépilotes délivrés dans l'Union européenne est automatique. La reconnaissance des titres délivrés hors de l'Union européenne n'est pas prévue.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

La reconnaissance des titres de télépilotes délivrés dans l'Union européenne est automatique. La reconnaissance des titres délivrés hors de l'Union européenne n'est pas prévue.

## 3°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable

Certificat conforme au règlement (UE) 2019/947.

### b. Autorité compétente

Direction générale de l'aviation civile.

## 4°. Textes de référence

Règlement d'exécution (UE) 2019/947 de la Commission du 24 mai 2019 concernant les règles et procédures applicables à l'exploitation d'aéronefs sans équipage à bord.