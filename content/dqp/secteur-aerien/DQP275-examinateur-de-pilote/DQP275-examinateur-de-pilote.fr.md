﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP275" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Examinateur de pilote" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="examinateur-de-pilote" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/examinateur-de-pilote.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="examinateur-de-pilote" -->
<!-- var(translation)="None" -->

# Examinateur de pilote

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

L'activité d'un examinateur consiste à conduire les examens pratiques et contrôles de compétence des pilotes.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'activité est soumise à une règlementation européenne (Règlement (UE) n° 1178/2011 de la Commission du 3 novembre 2011 déterminant les exigences techniques et les procédures administratives applicables au personnel navigant de l'aviation civile conformément au règlement (CE) n° 216/2008 du Parlement européen et du Conseil). Pour mémoire, les exigences fixées par la Commission européenne requièrent la détention d'une autorisation d'examinateur et de la licence ou qualification pour laquelle l'examinateur conduit l'examen, en cours de validité ainsi que le certificat médical associé le cas échéant. L'examinateur doit être qualifié pour agir en tant que commandant de bord pendant un examen à bord d'un aéronef et n'avoir fait l'objet d'aucune sanction au cours des trois dernières années.

#### Formation

Les exigences fixées par la Commission européenne requièrent des candidats à une autorisation d'examinateur de suivre un cours de standardisation conforme à la sous-partie K de l'annexe 1 du règlement, auprès d'un organisme de formation approuvé ou dispensée par l'autorité. 

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

La reconnaissance des titres de pilotes y compris les autorisations d'examinateur délivrés dans l'Union européenne est automatique. La reconnaissance des titres délivrés hors de l'Union européenne n'est pas prévue.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

La reconnaissance des titres de pilotes y compris les autorisations d'examinateur délivrés dans l'Union européenne est automatique. La reconnaissance des titres délivrés hors de l'Union européenne n'est pas prévue.

## 3°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable

Licence et autorisation conforme au règlement 1178/2011.

### b. Autorité compétente

Direction générale de l'aviation civile.

## 4°. Textes de référence

Règlement n° 1178/2011 de la Commission du 3 novembre 2011 déterminant les exigences techniques et les procédures administratives applicables au personnel navigant de l'aviation civile conformément au règlement (CE) n° 216/2008 du Parlement européen et du Conseil.