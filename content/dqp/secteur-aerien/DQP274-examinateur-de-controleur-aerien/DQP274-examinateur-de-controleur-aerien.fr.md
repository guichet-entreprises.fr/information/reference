﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP274" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Examinateur de contrôleur aérien" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="examinateur-de-controleur-aerien" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/examinateur-de-controleur-aerien.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="examinateur-de-controleur-aerien" -->
<!-- var(translation)="None" -->

# Examinateur de contrôleur aérien

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

L'activité d'un examinateur consiste à conduire les examens pratiques de contrôleur aérien.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'activité est soumise à une règlementation européenne (Règlement (UE) 2015/340 de la Commission du 20 février 2015 déterminant les exigences techniques et les procédures administratives applicables aux licences et certificats de contrôleur de la circulation aérienne conformément au règlement (CE) n° 216/2008 du Parlement européen et du Conseil, modifiant le règlement d'exécution (UE) n° 923/2012 de la Commission et abrogeant le règlement (UE) n° 805/2011 de la Commission). Pour mémoire, les exigences fixées par la Commission Européenne requièrent, pour conduire les examens pratiques, la détention d'une mention d'examinateur pratique et des mentions appropriées en cours de validité ainsi que le certificat médical associé le cas échéant.

#### Formation

Les exigences fixées par la Commission Européenne requièrent des candidats à une mention d'examinateur pratique de contrôleur aérien de suivre une formation conforme à la section 5 de la sous-partie D de l'annexe 1 du règlement.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

La reconnaissance des titres d'examinateur de contrôleur aérien délivrés dans l'Union européenne est automatique par procédure d'échange conformément au point ATCO.A.010 du règlement (UE) 2015/340. La reconnaissance des titres délivrés hors de l'Union européenne n'est pas prévue.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

La reconnaissance des titres d'examinateur de contrôleurs aériens délivrés dans l'Union européenne est automatique par procédure d'échange conformément au point ATCO.A.010 du règlement (UE) 2015/340 . La reconnaissance des titres délivrés hors de l'Union n'est pas prévue.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Le service du contrôle aérien ainsi que les fonctions d'examinateurs sont assurés par des agents de l'État recrutés selon les règles relatives à la fonction publique.

## 4°. Législation sociale et assurances

Les règles de la fonction publique s'appliquent à la profession.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration préalable

Licence conforme au règlement (UE) 2015/340.

### b. Autorité compétente

Direction générale de l'aviation civile.

## 6°. Textes de référence

Règlement (UE) 2015/340 de la Commission du 20 février 2015 déterminant les exigences techniques et les procédures administratives applicables aux licences et certificats de contrôleur de la circulation aérienne conformément au règlement (CE) no 216/2008 du Parlement européen et du Conseil, modifiant le règlement d'exécution (UE) n° 923/2012 de la Commission et abrogeant le règlement (UE) n° 805/2011 de la Commission.