﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP285" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Agent de sûreté aéroportuaire" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="agent-de-surete-aeroportuaire" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/agent-de-surete-aeroportuaire.html" -->
<!-- var(last-update)="Décembre 2021" -->
<!-- var(url-name)="agent-de-surete-aeroportuaire" -->
<!-- var(translation)="None" -->

# Agent de sûreté aéroportuaire

Dernière mise à jour : <!-- begin-var(last-update) -->Décembre 2021<!-- end-var -->

## 1°. Définition de l'activité

Personnes effectuant :

- l'inspection filtrage des personnes, des bagages de cabine, des objets transportés et des bagages de soute ;
- l'inspection-filtrage du fret et du courrier ;
- l'inspection-filtrage du courrier et du matériel des transporteurs aériens, des approvisionnements de bord, des fournitures d'aéroports ;
- l'inspections des véhicules ;
- le contrôle d'accès à la zone de sûreté à accès réglementé d'un aéroport.

## 2°. Qualifications professionnelles

### a. Exigences réglementaires

#### Réglementation de l'Union européenne et nationale

- Points 11.2.3.1 à 11.2.3.5 de l'annexe au règlement d'exécution (UE) 2015/1998 de la Commission du 5 novembre 2015 fixant des mesures détaillées pour la mise en œuvre des normes de base communes dans le domaine de la sûreté de l'aviation civile ;
- Article L. 6342-4 du Code des transports ;
- Article R. 213-4 du Code de l'aviation civile.

*Autres textes non codifiés :*

Articles 11-2-1-1, 11-2-1-2, 11-3-1, 11-3-2 et 11-4-1 de l'arrêté du 11 septembre 2013 modifié relatif aux mesures de sûreté de l'aviation civile. Ces dispositions définissent les modalités de certification des agents de sûreté aéroportuaire (en application du point 11.3 de l'annexe au règlement UE  2015/1998). 

Pour exercer, les agents de sureté aéroportuaires -ADS- doivent être titulaire de la carte professionnelle mentionnée à l'article L. 612-20 du Code de la sécurité intérieure.

*Autres textes pertinents (Code de déontologie, règlement intérieur, charte de la profession) :*

Articles 7 et 14 de l'arrêté du 27 juin 2017 portant cahier des charges applicable à la formation initiale aux activités privées de sécurité. Cette disposition relève du code de la sécurité intérieure.

#### Formation

Les agents de sûreté aéroportuaire sont soumis à une obligation de formation générale à la sécurité privée et à une formation spécifique aux mesures de sûreté de l'aviation civile. Ces formations, initiales et continues sont précisées dans les textes suivants :

- articles 11-2-1-1 à 11-2-1-5 et 11-4-1 à 11-4-4 de l'annexe à l'arrêté du 11 septembre 2013 relatif aux mesures de sûreté de l'aviation civile ;
- article 14 de l'arrêté du 27 juin 2017 portant cahier des charges applicable à la formation initiale aux activités privées de sécurité ;
- article 7 de l'arrêté du 27 février 2017 relatif à la formation continue des agents privés de sécurité.

La certification des agents de sûreté aéroportuaire est valable 3 ou 5 ans selon les mesures de sûreté mises en œuvre.

#### Coûts associés

Le coût d'une formation complète d'agent de sûreté aéroportuaire s'élève à un montant compris entre 2000 et 3000 euros en fonction des spécialités choisies et des organismes de formation. 

### b. Ressortissants UE : en vue d'un exercice temporaire et occasionnel (LPS)

Indépendamment de leurs qualifications professionnelles, les agents de sûreté aéroportuaire doivent être habilités et préalablement agrées par le préfet et le procureur de la République conformément aux article L. 6342-3 et L. 6242-4 du Code des transports. Ces procédures, qui consistent en une vérification des antécédents par les services de l'État nécessitent un temps d'instruction qui limite de fait les prestations temporaires de courte durée en France.

### c. Ressortissants UE : en vue d'un exercice permanent (LE)

En application du point 11.7.1 de l'annexe du règlement d'exécution (UE) 2015/1998 de la commission du 5 novembre 2015 fixant des mesures détaillées pour la mise en œuvre des normes de base communes dans le domaine de la sûreté de l'aviation civile : « toute compétence acquise par une personne afin de satisfaire aux exigences du règlement (CE) n° 300/2008 et de ses actes d'exécution dans un État membre doit être reconnue dans un autre État membre ».

L'article 11-7 de l'arrêté du 11 septembre 2013 modifié précise « qu'une personne désirant faire reconnaître une certification acquise dans un autre État membre de l'Union européenne doit en faire la demande auprès du ministre chargé des transports ».

Ainsi, un agent de sûreté aéroportuaire dont les compétences ont été certifiées par l'autorité compétente d'un autre État membre de l'Union européenne souhaitant exercer le métier d'ADS en France doit en faire la demande auprès de la Direction générale de l'aviation civile, direction de la sécurité de l'aviation civile (DSAC-SUR plus précisément), 50 rue Henry Farman, 75720 Paris Cedex 15.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Conformément à la règlementation européenne - point 11.1.3 de l'annexe au règlement (UE) 2015/1998 tel qu'il sera en vigueur à compter du 31 décembre 2021- , les agents de sureté aéroportuaire sont soumis à une vérification renforcée de leurs antécédents, en vue d'obtenir l'habilitation prévue mentionnée à l'article L.6342-3 et au V de l'article L.6342-4 du Code des transports, qui consiste à :

a) établir l'identité de la personne sur la base de documents ;
b) prendre en considération le casier judiciaire dans tous les États de résidence au cours des cinq dernières années ; 
c) prendre en considération les emplois, les études et toute interruption de plus de vingt-huit jours dans le relevé de la formation initiale ou de la carrière au cours des cinq dernières années ;
d) prendre en considération les informations des services de renseignement et toute autre information pertinente dont les autorités nationales compétentes disposent et estiment qu'elles peuvent présenter un intérêt pour apprécier l'aptitude d'une personne à exercer une fonction qui requiert une vérification renforcée de ses antécédents. 

## 4°. Législation sociale et assurances

La profession est régie par la convention collective nationale des entreprises de prévention et de sécurité. Elle contient une annexe VIII spécifique aux activités de sûreté aérienne et aéroportuaire. Cette convention collective est complétée par :

- un accord relatif aux agressions en situation de travail du 30 novembre 2011 ;
- un accord sur l'équilibre vie professionnelle/vie privée du 15 juillet 2014 ;
- un avenant à l'article 3.06 de l'annexe VIII (PPI) du 19 janvier 2018 ;
- un accord spécifique aux transferts de personnels dans le secteur de la sûreté conclu le 3 décembre 2012 qui complète le texte général du 28 janvier 2011 sur les conditions de transfert du personnel en cas de perte de marché. 

Assurances habituelles de l'employeur.

## 5°. Démarches et formalités de reconnaissance de qualification professionnelle

### a. Carte professionnelle

Carte professionnelle d'agent de sûreté aéroportuaire délivrée par le Conseil national des activités privées de sécurité (CNAPS), établissement public du ministère de l'Intérieur, régulateur de la sécurité privée.

### b. Autorité compétente

La Direction générale de l'aviation civile, direction de la sécurité de l'aviation civile (DSAC-SUR), 50 rue Henry Farman, 75720 Paris Cedex 15). Si le professionnel souhaite exercer ailleurs que sur un aéroport parisien, il fera sa demande de reconnaissance mutuelle auprès de la DSAC-Interrégionale dont il dépendra (la DSAC-SUR lui indiquant les coordonnées de cette autorité régionalement compétente).

### c. Délais de réponse

Un mois après réception du dossier complet, peut être allongé en fonction du délai de réponse de l'autorité compétente de l'État dans lequel les attestations ont été émises, afin de vérifier qu'elles sont conformes à la règlementation en vigueur dans cet État. La DSAC/SUR pourra fournir le contact de l'autorité compétente si le demandeur ne l'a pas lui-même fourni.

### d. Pièces justificatives

Les dossiers de demande de reconnaissance des qualifications doivent contenir les éléments suivants : 

- une lettre motivant la demande et le besoin de reconnaissance mutuelle (formulée en français ou, le cas échéant, en anglais), 
- la copie d'un document l'identité de la personne (passeport, carte nationale d'identité, titre de séjour), 
- la copie des attestations de formation (initiales et périodiques) pour lesquelles la reconnaissance est demandée, 
- le contact de l'autorité compétente de l'État dans lequel les attestations ont été émises (nom de l'autorité, nom et prénom du contact et de préférence son adresse email). 

### e. Voies de recours nationales

Recours gracieux auprès de l'autorité compétente ayant opposé un refus de reconnaissance de la formation (refus qui ne peut se fonder que sur l'absence d'une des pièces nécessaires au dossier ou si l'autorité compétente du pays d'origine a infirmé l'existence d'une qualification dans son État).
Puis recours contentieux devant le tribunal administratif compétent.

## 6°. Textes de référence

Pour la reconnaissance européenne des formations :

- le règlement (UE) 2015/1998 en son paragraphe 11.7 qui prévoit la reconnaissance mutuelle des compétences acquises par des formations reçues dans un autre État membre de l'Union européenne ;
- l'article 11-7 de l'arrêté du 11 septembre 2013 modifié.

Autres textes de référence :

- point 11.1 à 11.4 de l'Annexe au règlement d'exécution (UE) 2015/1998
- article L. 6342-4 du Code des transports ; 
- article R. 213-4 du Code de l'aviation civile ; 
- articles 11-2-1-1, 11-2-1-2, 11-3-1, 11-3-2 et 11-4-1 de l'arrêté du 11 septembre 2013 modifié relatif aux mesures de sûreté de l'aviation civile.