﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP222" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Aviation sector" -->
<!-- var(title)="Airport firefighter" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="aviation-sector" -->
<!-- var(title-short)="airport-firefighter" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/aviation-sector/airport-firefighter.html" -->
<!-- var(last-update)="2020-04-15 17:22:14" -->
<!-- var(url-name)="airport-firefighter" -->
<!-- var(translation)="Auto" -->


Airport firefighter
====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The airfield firefighter is a professional who works in the aircraft rescue and firefighting service at airfields (SSLIA).

As such, his mission is to:

- provide relief, protection and prevention against fires and accidents that threaten the safety of persons and property (aircraft and their environment) at a specified airfield;
- Evacuation and first aid to those injured;
- when not monitoring a flight:- SSLIA vehicles,
  - to carry out sports training to maintain good physical abilities,
  - to train and mentor new recruits.

*For further information*: Article D. 213-1-5 of the Civil Aviation Code; Section L. 6332-3 of the Transportation Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The person wishing to practice the profession of firefighter in an SSLIA must be licensed by the prefect of the place in which the airfield is located.

*For further information*: Article D. 213-1-6 of the Civil Aviation Code; Article 10 paragraph I of the[decreed from 18 January 2007](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=F2DA70ACD6746F7232B96B0021A84504.tplgfr37s_3?cidTexte=JORFTEXT000000464146&dateTexte=20170511) on the technical standards applicable to the aircraft rescue and firefighting service at airfields, amended by the Order of 22 December 2015 on the recognition of professional qualifications for the profession of airfield firefighter.

#### Approval

To operate at a specific airfield, the firefighter must apply for approval from the prefect of the airfield site as long as he meets all of the following conditions:

- Have validated core materials and fire and personal rescue modules;
- a medical certificate issued by one of the following doctors:- the doctor of the medical department of the Directorate General of Civil Aviation,
  - the doctor of the health and medical rescue department of the Department of Fire and Rescue,
  - The doctor approved by one of the two services above or by the prefect;
- Have a valid licence or permit to drive SSLIA vehicles or boats on the airfield;
- a certificate of training issued by the airfield operator certifying the follow-up of local training, the program of which is planned in paragraph 2 of Schedule II of the January 18, 2007 order.

However, on the proposal of the operator of this airfield, the person concerned will also be able to obtain validation of his previous training from then on:

- He holds the additional reference to "civil and corporate security";
- he has had for less than two years:- served in a fire and rescue service as a firefighter,
  - served in a military firefighting unit and warrants specific training,
  - trained as a volunteer in the civil service of the fire brigade,
  - obtained the National Certificate of Young Firefighters.

*For further information*: Article 10 paragraph I of the 18 January 2007 decree and Appendix II paragraph I of the 18 January 2007 decree.

#### Training

Training leading to the profession of airfield firefighter is provided by the French Airport Fire Brigade Training Centre (C2FPA).

It is open to candidates:

- valid driver's license holders for SSLIA vehicle categories;
- justifying a medical certificate of less than one year certifying their physical abilities;
- validated the initial training of a volunteer firefighter.

Only a dozen candidates per session can join the C2FPA and continue the training that includes a common core and local training.

Common core training focuses on the following objectives:

- Know general aviation rules and aircraft
- learn about SSLIA regulations, including those on fire extinguishers used, SSLIA vehicles, aircraft refueling, personnel protection, aircraft firefighting tactics and response techniques special risks.

The 105-hour core training will be validated after a written exam and a physical test.

Local training will then be provided within the airfield where the candidate wishes to practice and will be fully focused on the knowledge of the airfield.

*For further information*: Appendix II of the January 18, 2007 order.

#### Costs associated with qualification

Training leading to the profession of airfield firefighter pays off. For more information, it is advisable to contact the[C2FPA](http://www.c2fpa.fr/pages/formations-sauvetage-incendie-daeronefs-2/).

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) state or party to the European Economic Area (EEA), established and legally carrying out the activities of airfield firefighter in one of these states, may carry out the same activity in France on a temporary and Occasional.

He will have to make a declaration to the prefect of the place where the airfield is located in which he wishes to perform the service (see infra "5°. a. Obtain a licence to practise for the national for a temporary or casual exercise (LPS)").

When the examination of professional qualifications shows substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test.

**Please note**

The prefect may authorize the national to carry out only certain tasks of his activity in France, as part of partial access, as long as he fulfils the following conditions:

- justify the necessary qualifications for the exercise of its activity in the State of origin;
- where there are differences between the activity carried out in his Home State and that in France, that the compensation measures taken against him would amount to full training;
- the activity of the national may be separated from those of the airfield firefighter profession in France;
- the national's activity can be carried out independently in France.

**What to know**
The prefect may impose a control of language knowledge on the national when he deems it necessary.

*For further information*: Article 10 ter of the January 18, 2007 order.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU Member State or party to the EEA, established and legally carrying out the activities of airfield firefighter in one of these states, may carry out the same activity in France on a permanent basis.

He will have to apply for approval from the prefect of the place where the airfield is located in which he wishes to settle (see infra "5°. b. Obtain accreditation for the national for a permanent exercise (LE)).

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subject to a compensation measure (cf. infra "5°. b. Good to know: compensation measures").

**Please note**

The prefect may authorize the national to carry out only certain tasks of his activity in France, as part of partial access, as long as he fulfils the following conditions:

- justify the necessary qualifications for the exercise of its activity in the State of origin;
- where there are differences between his activity in his home state and that of France, that the compensation measures taken against him would amount to full training;
- the activity of the national may be separated from those of the airfield firefighter profession in France;
- the national's activity can be carried out independently in France.

**What to know**

The prefect may impose a control of language knowledge on the national when he deems it necessary.

*For further information*: Article 10 II of the January 18, 2007 order.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

Although not regulated, ethical rules may be the responsibility of the airfield firefighter, including respect, good character or solidarity.

4°. Continuous training
-------------------------------------------

The maintenance of the certification granted to the airfield firefighter is subject to the obligation to undergo continuous professional training by the holder.

As part of this training, the airfield firefighter will be required to carry out theoretical and practical training, as well as continuing education courses, the modalities of which are specified in paragraphs III-A and III-B of the[Appendix II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8792DE5D64EE31B4A7CF060E1B45EBB4.tplgfr24s_3?idArticle=LEGIARTI000034828830&cidTexte=LEGITEXT000021506419&dateTexte=20171127) January 18, 2007. In the event that the firefighter does not meet these two conditions, he would have his accreditation suspended until these trainings are carried out.

In addition, the licence may be withdrawn from the airfield firefighter as soon as he:

- is not in possession of a medical certificate that makes him physically fit to practice the profession;
- completed 144 hours of service or 24 vacations (for airfields with a three-month period)[level of protection](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8792DE5D64EE31B4A7CF060E1B45EBB4.tplgfr24s_3?idArticle=LEGIARTI000021506448&cidTexte=LEGITEXT000021506419&dateTexte=20171127) less than 6), except in the case of an upgrade certified by the SSLIA manager, after a six-month absence.

*For further information*: Article 12 and Appendix II of the January 18, 2007 order.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Obtain a licence to practise for the national for a temporary or casual exercise (LPS)

**Competent authority**

The prefect of the department where the airfield where the national wishes to perform the service is responsible for deciding on the declaration of authorisation to exercise.

**Supporting documents**

In order to carry out the service in France, the national must pass on to the prefect, by any means, a file containing the following supporting documents:

- a written statement informing the person exercising a service in France;
- a medical certificate issued by one of the following doctors:- the doctor of the medical department of the Directorate General of Civil Aviation,
  - the doctor of the health and medical rescue department of the Department of Fire and Rescue,
  - The doctor approved by one of the two services above or by the prefect;
- one or more photocopies of the valid licences allowing the operation of SSLIA vehicles or boats located on the airfield where the national will perform the service;
- A certificate of recognition of one's competence when the profession is regulated in the EU or EEA State;
- a certificate justifying its activity for at least one year in the last ten years, when neither training nor the practice of the profession is regulated in the Member State;
- a certificate of training issued by the airfield operator when the national has completed the local training programme provided for in paragraph A-2 of Schedule II of the January 18, 2007 order.

**Timeframe**

The regional prefect has one month from the time the file is received to make his decision, which may be:

- allow the national to provide his or her first service;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the qualifications and work experience he uses are substantially different from those required for the exercise of the profession in France. The decision on whether to authorize the benefit must take place within one month of the test;
- inform them of one or more difficulties that may delay decision-making. In this case, he will have two months to decide from the resolution of the difficulties.

*For further information*: Articles 10 ter and 13 of the January 18, 2007 order.

### b. Obtaining a licence for the national for a permanent exercise (LE)

**Competent authority**

The prefect of the department where the airfield is located, in which the national wishes to practise as a firefighter, is competent to decide on the application for accreditation.

**Supporting documents**

In order to settle in France, the national must submit to the prefect, by any means, a file containing the following supporting documents:

- a medical certificate issued by one of the following doctors:- medical officer of the Directorate General of Civil Aviation,
  - medical and health department doctor of the Department of Fire and Rescue,
  - a doctor licensed by one of the two services above or by the prefect;
- one or more photocopies of the valid licences allowing the operation of SSLIA vehicles or boats located on the airfield where the national will perform the service;
- A certificate of recognition of one's competence when the profession is regulated in the EU or EEA State;
- a certificate of training issued by the airfield operator when the national has completed the local training programme provided for in paragraph A-2 of Schedule II of the January 18, 2007 order;
- a certificate justifying its activity for at least one year in the last ten years, when neither training nor the practice of the profession is regulated in the Member State.

**Good to know: compensation measures**

In order to carry out his activity in France or to enter the profession, the national may be required to submit to the compensation measure of his choice, which may be:

- an adaptation course, sometimes with additional training, which will be evaluated at the end of the project;
- an aptitude test carried out within six months of notification to the person concerned.

*For further information*: Article 10 paragraph II of the January 18, 2007 order.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

