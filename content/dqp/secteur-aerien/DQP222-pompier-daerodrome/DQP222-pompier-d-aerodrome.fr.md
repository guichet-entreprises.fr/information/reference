﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP222" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Secteur aérien" -->
<!-- var(title)="Pompier d'aérodrome" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="secteur-aerien" -->
<!-- var(title-short)="pompier-d-aerodrome" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/secteur-aerien/pompier-d-aerodrome.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="pompier-d-aerodrome" -->
<!-- var(translation)="None" -->

# Pompier d'aérodrome

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l'activité

Le pompier d'aérodrome est un professionnel qui intervient au sein du service de sauvetage et de lutte contre l'incendie des aéronefs sur les aérodromes (SSLIA).

À ce titre, il a pour missions :

- d'assurer le secours, la protection et la prévention contre les incendies et accidents menaçant la sécurité des personnes et des biens (aéronefs et leur environnement) sur un aérodrome déterminé ;
- d'effectuer l'évacuation et les premiers soins aux personnes accidentées ;
- lorsqu'il ne surveille pas un vol :
  - d'entretenir les véhicules du SSLIA,
  - d'effectuer des entraînements sportifs pour maintenir de bonnes capacités physiques,
  - de veiller à la formation et à l'encadrement des nouvelles recrues.

*Pour aller plus loin* : article D. 213-1-5 du Code de l'aviation civile ; article L. 6332-3 du Code des transports.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

La personne souhaitant exercer la profession de pompier au sein d'un SSLIA doit être titulaire d'un agrément délivré par le préfet du lieu dans lequel se situe l'aérodrome.

*Pour aller plus loin* : article D. 213-1-6 du Code de l'aviation civile ; article 10 paragraphe I de l'arrêté du 18 janvier 2007 relatif aux normes techniques applicables au service de sauvetage et de lutte contre l'incendie des aéronefs sur les aérodromes, modifié par l'arrêté du 22 décembre 2015 relatif à la reconnaissance des qualifications professionnelles pour la profession de pompier d'aérodrome.

#### Agrément

Pour exercer sur un aérodrome déterminé, le pompier doit demander un agrément au préfet du lieu de cet aérodrome dès lors qu'il remplit l'ensemble des conditions suivantes :

- avoir validé les matières du tronc commun et les modules incendies et secours à la personne ;
- un certificat médical délivré par l'un des médecins suivants :
  - le médecin du service médical de la direction générale de l'aviation civile,
  - le médecin du service de santé et de secours médical du service départemental d'incendie et de secours,
  - le médecin agréé par l'un des deux services ci-avant ou par le préfet ;
- être titulaire d'un ou des permis, en cours de validité, permettant la conduite des véhicules du SSLIA ou des embarcations se trouvant sur l'aérodrome ;
- une attestation de formation délivrée par l'exploitant de l'aérodrome certifiant du suivi de la formation locale, dont le programme est prévu au point 2 du paragraphe I de l'annexe II de l'arrêté du 18 janvier 2007.

Toutefois, sur proposition de l'exploitant de cet aérodrome, l'intéressé pourra également obtenir la validation de sa formation antérieure dès lors :

- qu'il est titulaire de la mention complémentaire « sécurité civile et d'entreprise » ;
- qu'il a, depuis moins de deux années :
  - servi dans un service d'incendie et de secours en qualité de sapeur-pompier,
  - servi dans une unité militaire chargée de la lutte contre les incendies et qu'il justifie d'une formation spécifique,
  - reçu une formation de volontaire en service civique des sapeurs-pompiers,
  - obtenu le brevet national des jeunes sapeurs-pompiers.

*Pour aller plus loin* : article 10 paragraphe I de l'arrêté du 18 janvier 2007 et annexe II paragraphe I de l'arrêté du 18 janvier 2007.

#### Formation

La formation menant à la profession de pompier d'aérodrome est dispensée par le centre français de formation des pompiers d'aéroport (C2FPA).

Elle est ouverte aux candidats :

- titulaires du permis de conduire en cours de validité pour les catégories de véhicule du SSLIA ;
- justifiant d'un certificat médical de moins d'un an certifiant de leurs aptitudes physiques ;
- ayant validé la formation initiale de sapeur-pompier volontaire.

Seule une douzaine de candidats par session peut intégrer le C2FPA et poursuivre la formation qui comprend un tronc commun et une formation locale.

La formation en tronc commun porte sur les objectifs suivants :

- connaître les règles aéronautiques générales et les aéronefs ;
- connaître les réglementations du SSLIA, et notamment celles sur les agents extincteurs utilisés, les véhicules SSLIA, l'avitaillement des aéronefs, la protection du personnel, la tactique de lutte contre l'incendie des aéronefs et techniques d'intervention spécifiques, les risques spéciaux.

D'une durée de 105 heures, cette formation en tronc commun sera validée à l'issue d'un examen écrit et d'une épreuve physique.

Une formation locale sera ensuite dispensée au sein même de l'aérodrome dans lequel le candidat souhaitera exercer et sera entièrement axée sur les connaissances de cet aérodrome.

*Pour aller plus loin* : paragraphe I de l'annexe II de l'arrêté du 18 janvier 2007.

#### Coûts associés à la qualification

La formation menant à la profession de pompier d'aérodrome est payante. Pour plus d'informations, il est conseillé de contacter directement le [C2FPA](http://www.c2fpa.fr/pages/formations-sauvetage-incendie-daeronefs-2/).

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d'un État de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE), établi et exerçant légalement les activités de pompier d'aérodrome dans l'un de ces États, peut exercer la même activité en France de manière temporaire et occasionnelle.

Il devra en faire une déclaration auprès du préfet du lieu où se situe l'aérodrome dans lequel il souhaite exercer la prestation (cf. infra « 5°. a. Obtenir une autorisation d'exercer pour le ressortissant en vue d'un exercice temporaire ou occasionnel (LPS) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude.

**À noter**

Le préfet pourra autoriser le ressortissant à n'exercer que certaines tâches de son activité en France, dans le cadre d'un accès partiel, dès lors qu'il remplit les conditions suivantes :

- justifier des qualifications nécessaires pour l'exercice de son activité dans l'État d'origine ;
- lorsqu'il existe des différences telles entre l'activité exercée dans son État d'origine et celle en France, que les mesures de compensation prises à son égard reviendraient à lui faire suivre une formation complète ;
- l'activité du ressortissant peut être séparée de celles relevant de la profession de pompier d'aérodrome en France ;
- l'activité du ressortissant peut être exercée de manière autonome en France.

**À savoir**

Le préfet peut imposer un contrôle des connaissances linguistiques au ressortissant lorsqu'il le juge nécessaire.

*Pour aller plus loin* : article 10 ter de l'arrêté du 18 janvier 2007.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État membre de l'UE ou partie à l'EEE, établi et exerçant légalement les activités de pompier d'aérodrome dans l'un de ces États, peut exercer la même activité en France de manière permanente.

Il devra demander un agrément auprès du préfet du lieu où se situe l'aérodrome dans lequel il souhaite s'établir (cf. infra « 5°. b. Obtenir un agrément pour le ressortissant en vue d'un exercice permanent (LE) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une mesure de compensation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

**À noter**

Le préfet pourra autoriser le ressortissant à n'exercer que certaines tâches de son activité en France, dans le cadre d'un accès partiel, dès lors qu'il remplit les conditions suivantes :

- justifier des qualifications nécessaires pour l'exercice de son activité dans l'État d'origine ;
- lorsqu'il existe des différences telles entre l'activité qu'il exerce dans son État d'origine et celle de la France, que les mesures de compensation prises à son égard reviendraient à lui faire suivre une formation complète ;
- l'activité du ressortissant peut être séparée de celles relevant de la profession de pompier d'aérodrome en France ;
- l'activité du ressortissant peut être exercée de manière autonome en France.

**À savoir**

Le préfet peut imposer un contrôle des connaissances linguistiques au ressortissant lorsqu'il le juge nécessaire.

*Pour aller plus loin* : article 10 II de l'arrêté du 18 janvier 2007.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Bien que non réglementées, des règles d'éthique peuvent incomber au pompier d'aérodrome, et notamment de respect, de bonne moralité ou encore de solidarité.

## 4°. Formation continue

Le maintien de l'agrément délivré au pompier d'aérodrome est soumis à l'obligation de suivre une formation professionnelle continue par son titulaire.

Dans le cadre de cette formation, le pompier d'aérodrome devra notamment effectuer des entraînements théoriques et pratiques, ainsi que des stages de formation continue dont les modalités d'exécution sont précisées aux paragraphes III-A et III-B de l'annexe II de l'arrêté du 18 janvier 2007. Dans l'hypothèse où le pompier ne remplirait pas ces deux conditions, il verrait son agrément suspendu jusqu'à réalisation de ces formations.

En outre, l'agrément pourra être retiré au pompier d'aérodrome dès lors qu'il :

- n'est pas en possession d'un certificat médical le rendant apte physiquement à exercer la profession ;
- n'a pas accompli, sur une période de trois mois, 144 heures de service ou 24 vacations (pour les aérodromes dont le [niveau de protection](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8792DE5D64EE31B4A7CF060E1B45EBB4.tplgfr24s_3?idArticle=LEGIARTI000021506448&cidTexte=LEGITEXT000021506419&dateTexte=20171127) est inférieur à 6), sauf en cas de remise à niveau attestée par le responsable du SSLIA, après une absence de six mois.

*Pour aller plus loin* : article 12 et annexe II de l'arrêté du 18 janvier 2007.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Obtenir une autorisation d'exercer pour le ressortissant en vue d'un exercice temporaire ou occasionnel (LPS)

#### Autorité compétente

Le préfet du département où se situe l'aérodrome dans lequel le ressortissant souhaite exercer la prestation, est compétent pour se prononcer sur la déclaration d'autorisation d'exercice.

#### Pièces justificatives

Pour réaliser la prestation en France, le ressortissant doit transmettre au préfet, par tout moyen, un dossier comprenant les pièces justificatives suivantes :

- une déclaration écrite informant de l'exercice par l'intéressé d'une prestation de service en France ;
- un certificat médical délivré par l'un des médecins suivants :
  - le médecin du service médical de la direction générale de l'aviation civile,
  - le médecin du service de santé et de secours médical du service départemental d'incendie et de secours,
  - le médecin agréé par l'un des deux services ci-avant ou par le préfet ;
- une ou des photocopies des permis, en cours de validité, permettant la conduite des véhicules du SSLIA ou des embarcations se trouvant sur l'aérodrome où le ressortissant exercera la prestation ;
- une attestation de reconnaissance de ses compétences lorsque la profession est réglementée dans l’État de l'UE ou de l'EEE ;
- une attestation justifiant son activité pendant au moins un an au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre ;
- une attestation de formation délivrée par l'exploitant de l'aérodrome lorsque le ressortissant a suivi la formation locale dont le programme est prévu au point A-2 du paragraphe I de l'annexe II de l'arrêté du 18 janvier 2007.

#### Délai

Le préfet de région dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision qui peut être :

- d’autoriser le ressortissant à faire sa première prestation de service ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France. La décision d'autoriser ou non la prestation doit intervenir dans le mois qui suit l'épreuve ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider à compter de la résolution de la ou des difficultés.

*Pour aller plus loin* : articles 10 ter et 13 de l'arrêté du 18 janvier 2007.

### b. Obtenir un agrément pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

Le préfet du département où se situe l'aérodrome dans lequel le ressortissant souhaite exercer la profession de pompier, est compétent pour se prononcer sur la demande d'agrément.

#### Pièces justificatives

Pour s'établir en France, le ressortissant doit transmettre au préfet, par tout moyen, un dossier comprenant les pièces justificatives suivantes :

- un certificat médical délivré par l'un des médecins suivants :
  - médecin du service médical de la direction générale de l'aviation civile,
  - médecin du service de santé et de secours médical du service départemental d'incendie et de secours,
  - médecin agréé par l'un des deux services ci-avant ou par le préfet ;
- une ou des photocopies des permis, en cours de validité, permettant la conduite des véhicules du SSLIA ou des embarcations se trouvant sur l'aérodrome où le ressortissant exercera la prestation ;
- une attestation de reconnaissance de ses compétences lorsque la profession est réglementée dans l’État de l'UE ou de l'EEE ;
- une attestation de formation délivrée par l'exploitant de l'aérodrome lorsque le ressortissant a suivi la formation locale dont le programme est prévu au point A-2 du paragraphe I de l'annexe II de l'arrêté du 18 janvier 2007 ;
- une attestation justifiant son activité pendant au moins un an au cours des dix dernières années, lorsque ni la formation, ni l'exercice de la profession ne sont réglementées dans l’État membre.

#### Bon à savoir : mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à la mesure de compensation de son choix, qui peut être :

- un stage d'adaptation, avec parfois une formation complémentaire en plus, qui fera l'objet d'une évaluation à son issue ;
- une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : article 10 paragraphe II de l'arrêté du 18 janvier 2007.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).