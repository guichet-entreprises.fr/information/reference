﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
|                                                                             |
|         Code generated. Automatically generated file; DO NOT EDIT           |
|                                                                             |
+-------------------------------------------------------------------------- -->
<!-- begin-ref(list-reference-fr-directive-qualification-professionnelle-sante) -->
  - Aide-soignant
  - Ambulancier
  - Anatomie et cytologie pathologiques
  - Anesthésiste-réanimateur
  - Audioprothésiste
  - Auxiliaire de puériculture
  - Biologiste médicale
  - Médecine cardiovasculaire
  - Chiropracteur
  - Chirurgien-dentiste (praticien de l'art dentaire)
  - Chirurgie de la face et du cou
  - Chirurgie générale
  - Chirurgie pédiatrique
  - Chirurgie maxillo-faciale et stomatologie
  - Chirurgie orthopédique et traumatologique
  - Chirurgie plastique, reconstructrice et esthétique
  - Chirurgie thoracique et cardiovasculaire
  - Chirurgie urologique
  - Chirurgie vasculaire
  - Chirurgie viscérale et digestive
  - Dermatologie et vénérologie
  - Diététicien
  - Electroradiologie
  - Endocrinologue diabétologue
  - Epithésiste
  - Ergothérapeute
  - Formation médicale de base
  - Hépato-gastroentérologue
  - Génétique médicale
  - Gériatrie
  - Gynécologie médicale
  - Gynécologie obstétrique
  - Hématologie
  - Infirmier anesthésiste
  - Infirmier
  - Infirmier de bloc opératoire
  - Manipulateur d'électroradiologie médicale
  - Masseur-kinésithérapeute
  - Médecin qualifié en médecine générale
  - Médecine bucco-dentaire
  - Médecin du travail
  - Médecine interne
  - Médecine nucléaire
  - Médecine physique et de réadaptation
  - Néphrologie
  - Neurochirurgie
  - Neurologue
  - Neuropsychiatrie
  - Oculariste
  - Oncologie
  - Oncologie option oncologie radiothérapique
  - Ophtalmologue
  - Opticien lunetier
  - Orthopédiste-orthésiste
  - Orthophoniste
  - Orthoprothésiste
  - Orthoptiste
  - Ostéopathe
  - Oto-rhino-laryngologie et chirurgie cervico-faciale
  - Pédiatre
  - Pédicure-podologue
  - Pédopsychiatre
  - Pharmacien
  - Pharmacien spécialisé en biologie médicale
  - Pneumologie
  - Podo-orthésiste
  - Praticien de l'art dentaire spécialiste (orthodontie)
  - Préparateur en pharmacie
  - Préparateur en pharmacie hospitalière
  - Prothésiste dentaire
  - Psychiatrie
  - Psychologue
  - Psychomotricien
  - Psychothérapeute
  - Puéricultrice
  - Radiodiagnostic et imagerie médicale
  - Radiophysicien
  - Réanimation médicale
  - Rhumatologie
  - Sage-femme
  - Santé publique
  - Chirurgie orale
  - Technicien de laboratoire médical
<!-- end-ref -->