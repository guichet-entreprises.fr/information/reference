﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
|                                                                             |
|         Code generated. Automatically generated file; DO NOT EDIT           |
|                                                                             |
+-------------------------------------------------------------------------- -->
<!-- begin-ref(list-reference-fr-directive-services-tourisme-loisirs-culture) -->
  - Centre équestre et assimilé
  - Entrepreneur de spectacles
  - Guide-conférencier
  - Agent de voyages
  - Agent artistique
  - Camping
  - Chambres d’hôtes
  - Exploitant piscine – lieu de baignade
  - Gîte ou meublé de tourisme
  - Hôtel
  - Organisateur de foires et salons
  - Etablissement d’activités physiques et sportives (EAPS) à vocation commerciale
  - Agence de presse
  - Artisan d'art
  - Artiste-auteur
  - Discothèque
  - Distributeur de presse
  - Editeur
  - Entreprise de presse – Publication de périodiques
  - Guide de haute montagne
  - Vidéo-club
<!-- end-ref -->