﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
|                                                                             |
|         Code generated. Automatically generated file; DO NOT EDIT           |
|                                                                             |
+-------------------------------------------------------------------------- -->
<!-- begin-ref(list-reference-fr-directive-qualification-professionnelle-professions-de-conseil-et-dexpertise) -->
  - Administrateur judiciaire
  - Agent de voyage
  - Agent général d'assurance
  - Agent immobilier - Syndic de copropriété - Administrateur de biens
  - Avocat
  - Avocat au Conseil d'État et à la Cour de cassation
  - Commissaire aux comptes
  - Commissaire aux comptes (application subsidiaire)
  - Commissaire-priseur de ventes volontaires
  - Conseiller à l'utilisation des produits phytopharmaceutiques
  - Conseiller en génétique
  - Conseiller en investissements financiers
  - Conseil en propriété industrielle
  - Courtier en vins et spiritueux
  - Courtier en assurance et en réassurance
  - Courtier en opérations de banque et en service de paiement
  - Enseignant dans l'enseignement secondaire
  - Expert en automobile
  - Mandataire d'intermédiaire en assurance
  - Mandataire d'assurance
  - Mandataire exclusif en opérations de banque et en services de paiement
  - Mandataire non exclusif en opérations de banque et en services de paiement
  - Professeur des écoles
  - Professionnel de l'expertise comptable
<!-- end-ref -->