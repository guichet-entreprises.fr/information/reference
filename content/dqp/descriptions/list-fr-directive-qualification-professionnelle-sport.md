﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
|                                                                             |
|         Code generated. Automatically generated file; DO NOT EDIT           |
|                                                                             |
+-------------------------------------------------------------------------- -->
<!-- begin-ref(list-reference-fr-directive-qualification-professionnelle-sport) -->
  - Accompagnateur en moyenne montagne
  - Animateur d'activités aquatiques et de la natation
  - Animateur d'activités combinées
  - Animateur d'activités d'orientation
  - Animateur d'activités du cyclisme
  - Animateur d'activités gymniques, de la forme et de la force
  - Animateur d'activités nautiques
  - Animateur d'activités physiques pour tous
  - Animateur d'arts martiaux
  - Animateur d'athlétisme
  - Animateur d'équitation
  - Moniteur de golf
  - Animateur de sport adapté
  - Animateur de sports santé éducation-motricité
  - Animateur de sports collectifs
  - Animateur de sports d'adresse
  - Animateur de sports d'opposition
  - Animateur de sports de combat
  - Animateur de sports de glace
  - Animateur de sports de raquettes
  - Animateur de sports mécaniques
  - Animateur handisport
  - Educateur sportif
  - Entraîneur d'activités aquatiques et de la natation
  - Entraineur d'activités combinées
  - Entraîneur d'activités de déplacement et d'orientation
  - Entraîneur d'activités du cyclisme
  - Entraîneur d'activités gymniques, d'expression, de la forme et de la force
  - Entraîneur d'activités nautiques
  - Entraîneur d'arts martiaux
  - Entraineur d'athlétisme
  - Entraîneur d'équitation
  - Entraîneur de golf
  - Entraîneur de sport adapté
  - Entraîneur de sports collectifs
  - Entraîneur de sports d'adresse
  - Entraîneur de sports d'opposition
  - Entraîneur de sports de combat
  - Entraîneur de sports de glace
  - Entraîneur de sports de raquette
  - Entraîneur de sports mécaniques
  - Entraîneur handisport
  - Guide de haute montagne
  - Maître-nageur sauveteur
  - Moniteur d'escalade
  - Moniteur de canoë-kayak
  - Moniteur de canyonisme
  - Moniteur de parachutisme
  - Moniteur de plongée subaquatique
  - Moniteur de ski alpin
  - Moniteur de ski nordique de fond
  - Moniteur de spéléologie
  - Moniteur de surf de mer
  - Moniteur de voile
  - Moniteur de vol libre
  - Parachutiste professionnel
  - Professeur de danse
<!-- end-ref -->